/*
 * CEPrecursorIon.java
 *
 * Created on 07-may-2019, 16:49:28
 *
 * Copyright(c) 2018 Ceu Mass Mediator All Rights Reserved.
 * This software is the proprietary information of Alberto Gil de la Fuente.
 *
 */
package CE.MS;

import CE.MS.CE_Exp_properties.CE_BUFFER_ENUM;
import CE.MS.CE_Exp_properties.CE_EXP_PROP_ENUM;
import exceptions.IncorrectBufferException;
import exceptions.IncorrectIonizationModeException;
import exceptions.IncorrectPolarityException;
import exceptions.IncorrectTemperatureException;

/**
 * Class to describe the compounds
 *
 * @version $Revision: 1.1.1.1 $
 * @since Build 4.0.0.0 07-may-2019
 *
 * @author Alberto Gil de la Fuente
 */
public class CEPrecursorIon {

    private final Integer cembio_id;
    private final Integer compound_id;
    private final CE_EXP_PROP_ENUM ce_exp_prop_type; // [1-16] different codes of properties
    private final Integer ce_identification_level; // L1, L2 or L3
    private final Integer ce_sample_type; // 1: standard, 2: plasma
    private final Integer capillary_length; // capillary length in milimiters.
    private final Integer capillary_voltage; // capillary_voltage in voltios.
    private final Integer bge_compound_id; // compound id of the bge (methionine sulfone: 180838, paracetamol: 73414)
    private final Double theoretical_mz;
    private final Double experimental_mz;
    private final Double absolute_MT;
    private final Double relative_MT;
    private final Double eff_mobility; // eff mobility calculated for the compound
    private final String commercial;

    private Integer ce_eff_mob_id;

    /**
     * Creates a new instance of CECompound
     *
     * @param cembio_id
     * @param compound_id
     * @param ce_eff_mob_id
     * @param ionization_mode
     * @param polarity
     * @param buffer
     * @param temperature
     * @throws exceptions.IncorrectTemperatureException
     * @throws exceptions.IncorrectBufferException
     * @throws exceptions.IncorrectIonizationModeException
     * @throws exceptions.IncorrectPolarityException
     */
    public CEPrecursorIon(Integer cembio_id, Integer compound_id, Integer ce_eff_mob_id,
            Integer ionization_mode, Integer polarity, Integer buffer, Integer temperature)
            throws IncorrectTemperatureException, IncorrectBufferException,
            IncorrectIonizationModeException, IncorrectPolarityException {
        this(cembio_id, compound_id, ce_eff_mob_id, 0, 0,
                CE_EXP_PROP_ENUM.fromBufferTemperatureIonModeAndPolarity(CE_BUFFER_ENUM.fromBufferCode(buffer), temperature, ionization_mode, polarity),
                0, 0, 0, 0d, 0d, 0d, 0d, 0d, "");
    }

    /**
     * Creates a new instance of CECompound
     *
     * @param cembio_id
     * @param compound_id
     * @param ce_eff_mob_id
     * @param ionization_mode
     * @param polarity
     * @param ce_identification_level
     * @param ce_sample_type
     * @param buffer
     * @param temperature
     * @param capillary_length
     * @param capillary_voltage
     * @param bge
     * @param theoretical_mz
     * @param experimental_mz
     * @param commercial
     * @param absolute_MT
     * @param eff_mobility
     * @param relative_MT
     * @throws exceptions.IncorrectTemperatureException
     * @throws exceptions.IncorrectBufferException
     * @throws exceptions.IncorrectIonizationModeException
     * @throws exceptions.IncorrectPolarityException
     */
    public CEPrecursorIon(Integer cembio_id, Integer compound_id, Integer ce_eff_mob_id,
            Integer ce_identification_level, Integer ce_sample_type,
            Integer ionization_mode, Integer polarity, Integer buffer, Integer temperature,
            Integer capillary_length, Integer capillary_voltage, Integer bge,
            Double theoretical_mz, Double experimental_mz, Double absolute_MT, Double relative_MT,
            Double eff_mobility, String commercial)
            throws IncorrectTemperatureException, IncorrectBufferException,
            IncorrectIonizationModeException, IncorrectPolarityException {
        this(cembio_id, compound_id, ce_eff_mob_id, ce_identification_level, ce_sample_type,
                CE_EXP_PROP_ENUM.fromBufferTemperatureIonModeAndPolarity(CE_BUFFER_ENUM.fromBufferCode(buffer), temperature, ionization_mode, polarity),
                capillary_length, capillary_voltage, bge, theoretical_mz, experimental_mz, absolute_MT,
                relative_MT, eff_mobility, commercial);
    }

    /**
     * Creates a new instance of CECompound
     *
     * @param cembio_id
     * @param compound_id
     * @param ce_identification_level
     * @param ce_sample_type
     * @param ce_exp_prop_enum
     * @param capillary_length
     * @param capillary_voltage
     * @param bge
     * @param theoretical_mz
     * @param experimental_mz
     * @param commercial
     * @param absolute_MT
     * @param eff_mobility
     * @param relative_MT
     */
    public CEPrecursorIon(Integer cembio_id, Integer compound_id,
            Integer ce_identification_level, Integer ce_sample_type, CE_EXP_PROP_ENUM ce_exp_prop_enum,
            Integer capillary_length, Integer capillary_voltage, Integer bge,
            Double theoretical_mz, Double experimental_mz, Double absolute_MT, Double relative_MT,
            Double eff_mobility, String commercial) {
        this(cembio_id, compound_id, null, ce_identification_level, ce_sample_type,
                ce_exp_prop_enum,
                capillary_length, capillary_voltage, bge, theoretical_mz, experimental_mz, absolute_MT,
                relative_MT, eff_mobility, commercial);
    }

    /**
     * Creates a new instance of CECompound
     *
     * @param cembio_id
     * @param compound_id
     * @param ce_eff_mob_id
     * @param ce_identification_level
     * @param ce_sample_type
     * @param ce_exp_prop_enum
     * @param capillary_length
     * @param capillary_voltage
     * @param bge
     * @param theoretical_mz
     * @param experimental_mz
     * @param commercial
     * @param absolute_MT
     * @param eff_mobility
     * @param relative_MT
     */
    public CEPrecursorIon(Integer cembio_id, Integer compound_id, Integer ce_eff_mob_id,
            Integer ce_identification_level, Integer ce_sample_type, CE_EXP_PROP_ENUM ce_exp_prop_enum,
            Integer capillary_length, Integer capillary_voltage, Integer bge,
            Double theoretical_mz, Double experimental_mz, Double absolute_MT, Double relative_MT,
            Double eff_mobility, String commercial) {
        this.cembio_id = cembio_id;
        this.compound_id = compound_id;
        this.ce_eff_mob_id = ce_eff_mob_id;
        this.ce_identification_level = ce_identification_level;
        this.ce_sample_type = ce_sample_type;
        this.capillary_length = capillary_length;
        this.capillary_voltage = capillary_voltage;
        this.bge_compound_id = bge;
        this.theoretical_mz = theoretical_mz;
        this.experimental_mz = experimental_mz;
        this.absolute_MT = absolute_MT;
        this.relative_MT = relative_MT;
        this.eff_mobility = eff_mobility;
        this.commercial = commercial;
        this.ce_exp_prop_type = ce_exp_prop_enum;
    }

    @Override
    public String toString() {
        return "cembio_id: " + this.cembio_id + ", compound_id: " + this.compound_id
                + ", ionization_mode: " + this.ce_exp_prop_type.getIonization_mode()
                + ", polarity: " + this.ce_exp_prop_type.getPolarity()
                + ", ce_identification_level: " + this.ce_identification_level
                + ", ce_sample_type: " + this.ce_sample_type
                + ", buffer: " + this.ce_exp_prop_type.getBuffer()
                + ", temperature: " + this.ce_exp_prop_type.getTemperature()
                + ", capillary_length: " + this.capillary_length
                + ", bge: " + this.bge_compound_id
                + ", theoretical_mz: " + this.theoretical_mz
                + ", experimental_mz: " + this.experimental_mz
                + ", absolute_MT: " + this.absolute_MT
                + ", relative_MT: " + this.relative_MT
                + ", eff_mobility: " + this.eff_mobility
                + ", commercial: " + this.commercial;
    }

    public Integer getCembio_id() {
        return cembio_id;
    }

    public Integer getCompound_id() {
        return compound_id;
    }

    public void setCe_eff_mob_id(Integer ce_eff_mob_id) {
        this.ce_eff_mob_id = ce_eff_mob_id;
    }

    public Integer getCe_eff_mob_id() {
        return ce_eff_mob_id;
    }

    public CE_EXP_PROP_ENUM getCe_exp_prop_type() {
        return ce_exp_prop_type;
    }

    public Integer getIonization_mode() {
        return this.ce_exp_prop_type.getIonization_mode();
    }

    public Integer getPolarity() {
        return this.ce_exp_prop_type.getPolarity();
    }

    public String getBuffer() {
        return this.ce_exp_prop_type.getBuffer();
    }

    public Integer getTemperature() {
        return this.ce_exp_prop_type.getTemperature();
    }

    public Integer getCe_identification_level() {
        return ce_identification_level;
    }

    public Integer getCe_sample_type() {
        return ce_sample_type;
    }

    public Integer getCapillary_length() {
        return capillary_length;
    }

    public Integer getCapillary_voltage() {
        return capillary_voltage;
    }

    public Integer getBge_compound_id() {
        return bge_compound_id;
    }

    public Double getTheoretical_mz() {
        return theoretical_mz;
    }

    public Double getExperimental_mz() {
        return experimental_mz;
    }

    public Double getAbsolute_MT() {
        return absolute_MT;
    }

    public Double getRelative_MT() {
        return relative_MT;
    }

    public Double getEff_mobility() {
        return eff_mobility;
    }

    public String getCommercial() {
        return commercial;
    }

}
