/*
 * CEProductIon.java
 *
 * Created on 07-may-2019, 16:49:28
 *
 * Copyright(c) 2018 Ceu Mass Mediator All Rights Reserved.
 * This software is the proprietary information of Alberto Gil de la Fuente.
 *
 */
package CE.MS;

import CE.MS.CE_Exp_properties.CE_EXP_PROP_ENUM;

/**
 * Class to describe the compounds
 *
 * @version $Revision: 1.1.1.1 $
 * @since Build 4.0.0.0 07-may-2019
 *
 * @author Alberto Gil de la Fuente
 */
public class CEProductIon {

    private final Integer ce_productIon_id;
    private final Integer ion_source_voltage;

    private final Double ce_productIon_mz;
    private final Double ce_productIon_intensity;
    private final String ce_transformation_type;
    private final String ce_productIon_name;
    private final CEPrecursorIon cePrecursorIon; // the precursor ion
    private final Integer compound_id_own; // link to the compound or structure of the product ion

    /**
     * Creates a new instance of CECompound
     *
     * @param ce_productIon_id
     * @param ion_source_voltage
     * @param ce_productIon_mz
     * @param ce_productIon_intensity
     * @param ce_transformation_type
     * @param ce_productIon_name
     * @param cePrecursorIon
     * @param compound_id_own
     */
    public CEProductIon(Integer ce_productIon_id, Integer ion_source_voltage, Double ce_productIon_mz,
            Double ce_productIon_intensity, String ce_transformation_type,
            String ce_productIon_name, CEPrecursorIon cePrecursorIon, Integer compound_id_own) {
        this.ce_productIon_id = ce_productIon_id;
        this.ion_source_voltage = ion_source_voltage;
        this.ce_productIon_mz = ce_productIon_mz;
        this.ce_productIon_intensity = ce_productIon_intensity;
        this.ce_transformation_type = ce_transformation_type;
        this.ce_productIon_name = ce_productIon_name;
        this.cePrecursorIon = cePrecursorIon;
        this.compound_id_own = compound_id_own;
    }

    @Override
    public String toString() {
        return "ce_productIon_id: " + this.ce_productIon_id
                + ", ce_productIon_mz: " + this.ce_productIon_mz
                + ", ce_productIon_intensity: " + this.ce_productIon_intensity
                + ", ce_transformation_type: " + this.ce_transformation_type
                + ", ce_productIon_name: " + this.ce_productIon_name
                + ", ce_ce_compound_id_id: " + this.cePrecursorIon + ", compound_id: " + this.compound_id_own;
    }

    public Integer getCe_productIon_id() {
        return ce_productIon_id;
    }

    public Integer getIon_source_voltage() {
        return ion_source_voltage;
    }

    public Double getCe_productIon_mz() {
        return ce_productIon_mz;
    }

    public Double getCe_productIon_intensity() {
        return ce_productIon_intensity;
    }

    public String getCe_transformation_type() {
        return ce_transformation_type;
    }

    public String getCe_productIon_name() {
        return ce_productIon_name;
    }

    public CEPrecursorIon getCePrecursorIon() {
        return cePrecursorIon;
    }
    
    public Integer getCe_eff_mob_id() {
        return cePrecursorIon.getCe_eff_mob_id();
    }
    
    public CE_EXP_PROP_ENUM getCeExpProp()
    {
        return cePrecursorIon.getCe_exp_prop_type();
    }
    
    /**
     *
     * @return 1 if positive mode, 2 if negative mode
     */
    public Integer getCeIonMode()
    {
        return cePrecursorIon.getCe_exp_prop_type().getIonization_mode();
    }

    public Integer getCompound_id_own() {
        return compound_id_own;
    }

}
