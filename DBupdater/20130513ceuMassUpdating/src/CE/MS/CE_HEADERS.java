/*
 * CE_HEADERS.java
 *
 * Created on 03-may-2019, 11:53:21
 *
 * Copyright(c) 2018 Ceu Mass Mediator All Rights Reserved.
 * This software is the proprietary information of Alberto Gil de la Fuente.
 *
 */

package CE.MS;

/**
 * Description of static variables for CE Files
 * 
 * @version $Revision: 1.1.1.1 $
 * @since Build 4.0.0.0 03-may-2019
 * 
 * @author Alberto Gil de la Fuente
 */

enum CE_FILE_HEADERS{
   CMM_ID, MZ_THEORETICAL, METLIN_LINK, LM_ID, CLASS_II, METLIN_ID, CLASS_I, HMDB_ID, 
   INCHI, MONOISOTOPIC_MASS, ID_CODE, FRAGMENTS_200V_AND_ADDUCTS_100V, MZ_EXPERIMENTAL, 
   MT_PLASMA, LM_LINK, MT, HMDB_LINK, MOLECULAR_FORMULA, CAS_ID, LEVEL, NAME, SAMPLE, 
   RMT_PLASMA, RMT, COMMERCIAL;
}

// enum 

public final class CE_HEADERS {

    /**
     * Creates a new instance of HEADERS
     */
    
    
}
