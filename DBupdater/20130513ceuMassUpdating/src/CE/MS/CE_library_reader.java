/*
 * CE_library_reader.java
 *
 * Created on 26-abr-2019, 11:03:16
 *
 * Copyright(c) 2018 Ceu Mass Mediator All Rights Reserved.
 * This software is the proprietary information of Alberto Gil de la Fuente.
 *
 */
package CE.MS;

/**
 * Reader of the excel file corresponding to the CE MS library obtained in
 * CEMBIO.
 *
 * @version $Revision: 1.1.1.1 $
 * @since Build 5.0.0.0 26-abr-2019
 *
 * @author Alberto Gil de la Fuente
 */
import CE.MS.CE_Exp_properties.CE_BUFFER_ENUM;
import CE.MS.CE_Exp_properties.CE_EXP_PROP_ENUM;
import databases.CMMDatabase;
import exceptions.IncorrectBufferException;
import exceptions.IncorrectIonizationModeException;
import exceptions.IncorrectPolarityException;
import exceptions.IncorrectTemperatureException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import utilities.Checker;
import utilities.Utilities;
import static utilities.Utilities.readHeaders;

public class CE_library_reader {

    /**
     * Executes the main method for the CE_library_reader
     *
     * @param args
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {

        CMMDatabase db = new CMMDatabase();
        db.connectToDB("jdbc:mysql://localhost/compounds?useSSL=false&serverTimezone=UTC", "alberto", "alberto");

        //readPrecursorIons(db);

        readProductIons(db);
        //readPrecursorIonsFromMSMLS(db);
    }

    public static void readPrecursorIons(CMMDatabase db) throws IOException {
        String CEFilePath = "resources/CE/CE_MS_precursor_ions.xlsx";
        File excelFile = new File(CEFilePath);
        // we create an XSSF Workbook object for our XLSX Excel File
        try (FileInputStream fis = new FileInputStream(excelFile); // we create an XSSF Workbook object for our XLSX Excel File
                XSSFWorkbook workbook = new XSSFWorkbook(fis)) {

            // we get first sheet
            XSSFSheet sheet = workbook.getSheetAt(0);
            int totalRows = sheet.getPhysicalNumberOfRows();
            System.out.println("READING " + (totalRows - 1) + " compounds");

            Map<String, Integer> mapHeaders = readHeaders(CEFilePath);
            // Create the variables for the CE compound
            String name;
            int cembio_id_code;
            String molecular_formula;
            double monoisotopic_mass;
            double mz_theoretical;
            double mz_experimental;
            int ce_identification_level;
            String ce_sample_typeString;
            List<Integer> ce_sample_types;
            double mt_met;
            double rmt_met;
            double mt_par;
            double rmt_par;
            String inchi;
            double mt_met_plasma;
            double rmt_met_plasma;
            double mt_par_plasma;
            double rmt_par_plasma;
            String commercial;
            String CasId;
            int metlin_id;
            int cmm_id;
            String lm_id;
            String hmdb_id;

            // Variables not taken from the file
            int ionization_mode = 1; // positive
            int polarity = 1; // direct
            int buffer = 1; // formic acid 10% methanol = 1 or acetic acid = 2
            int temperature = 20; // methionine sulfone
            CE_EXP_PROP_ENUM ce_exp_properties = null;
            try {
                CE_BUFFER_ENUM ce_buffer_enum = CE_BUFFER_ENUM.fromBufferCode(buffer);
                ce_exp_properties = CE_EXP_PROP_ENUM.fromBufferTemperatureIonModeAndPolarity(ce_buffer_enum, temperature, ionization_mode, polarity);
            } catch (IncorrectBufferException ex) {
                System.out.println("WRONG BUFFER");
                return;
            } catch (IncorrectTemperatureException ex) {
                System.out.println("WRONG TEMPERATURE");
                return;
            } catch (IncorrectIonizationModeException ex) {
                System.out.println("WRONG IONIZATION MODE");
            } catch (IncorrectPolarityException ex) {
                System.out.println("WRONG POLARITY");
            }
            Integer ce_exp_prop_id = db.getCE_exp_prop_Id(buffer, temperature, ionization_mode, polarity);
            if (ce_exp_prop_id == null) {
                System.out.println("THE EXPERIMENTAL CONDITIONS ARE NOT SUPPORTED BY THE CMM DATABASE, "
                        + "plase contact with cmm.cembio@ceu.es");
                return;
            }

            int capillary_length = 1000; // 1000 milimiters = 1 meters
            int capillary_voltage = 30; // voltage in KV
            int bge; // methionine sulfone=180838  or paracetamol = 73414
            Double eff_mobility;  // ion mobility depending on other factors

            // Variables obtained from the database
            int compound_id;

            // we iterate on rows
            Iterator<Row> rowIt = sheet.iterator();
            // skip the header
            rowIt.next();
            while (rowIt.hasNext()) {
                Row row = rowIt.next();

                // iterate on cells for the current row
                Iterator<Cell> cellIterator = row.cellIterator();
                name = cellIterator.next().getStringCellValue();
                cembio_id_code = (int) cellIterator.next().getNumericCellValue();
                System.out.println("CEMBIO ID CODE: " + cembio_id_code);
                molecular_formula = cellIterator.next().getStringCellValue();
                monoisotopic_mass = (double) cellIterator.next().getNumericCellValue();
                mz_theoretical = (double) cellIterator.next().getNumericCellValue();
                mz_experimental = (double) cellIterator.next().getNumericCellValue();
                ce_identification_level = Character.getNumericValue(cellIterator.next().getStringCellValue().charAt(1));
                ce_sample_typeString = cellIterator.next().getStringCellValue();
                ce_sample_types = Utilities.getSampleTypes(ce_sample_typeString);
                // TODO GET THE NEW ATTRIBUTES FROM EXCEL (buffer, capilar_length, temperature, etc..) when needed
                mt_met = (double) cellIterator.next().getNumericCellValue();
                rmt_met = (double) cellIterator.next().getNumericCellValue();
                mt_par = (double) cellIterator.next().getNumericCellValue();
                rmt_par = (double) cellIterator.next().getNumericCellValue();
                Cell eff_mobilityCell = cellIterator.next();
                if (eff_mobilityCell == null || eff_mobilityCell.getCellType() == CellType.BLANK) {
                    eff_mobility = null;
                } else {
                    try {
                        eff_mobility = eff_mobilityCell.getNumericCellValue();
                    } catch (IllegalStateException ise) {
                        eff_mobility = null;
                    }
                }
                inchi = cellIterator.next().getStringCellValue();
                String formulaFromInchi = Checker.getFormulaFromInChI(inchi);
                if (!molecular_formula.equals(formulaFromInchi)) {
                    System.out.println("WRONG INCHI for compound: " + cembio_id_code
                            + "\t FORMULA: " + molecular_formula + "\t FROM INCHI: " + formulaFromInchi);
                }
                compound_id = db.getCompoundIdFromInchi(inchi.trim());
                // SKIP THE MT OF THE METHIONINE SULFONE
                cellIterator.next();
                mt_met_plasma = (double) cellIterator.next().getNumericCellValue();
                rmt_met_plasma = (double) cellIterator.next().getNumericCellValue();
                mt_par_plasma = (double) cellIterator.next().getNumericCellValue();
                rmt_par_plasma = (double) cellIterator.next().getNumericCellValue();
                commercial = cellIterator.next().getStringCellValue();
                CasId = cellIterator.next().getStringCellValue();
                if (cembio_id_code > 241) {
                    int cmmIdFromCAS = db.getCompounIdFromCAS(CasId);
                    if (cmmIdFromCAS == 0) {
                        // UPDATE CAS
                        db.updateCompoundCAS(compound_id, CasId);
                    } else if (compound_id != cmmIdFromCAS) {
                        System.out.println(cembio_id_code + " INCONSISTENT CAS " + CasId + " IN DB: " + cmmIdFromCAS
                                + "\t IN EXCEL FILE: " + compound_id);
                    }

                    // Skip the metlin_link
                    cellIterator.next();
                    metlin_id = (int) cellIterator.next().getNumericCellValue();
                    cmm_id = (int) cellIterator.next().getNumericCellValue();
                    // Skip LM_LINK
                    cellIterator.next();
                    lm_id = cellIterator.next().getStringCellValue();
                    // Skip HMDB_LINKs
                    cellIterator.next();
                    hmdb_id = cellIterator.next().getStringCellValue();

                    if (ce_identification_level > 1) {
                        for (Integer ce_sample_type : ce_sample_types) {
                            CEPrecursorIon cecompound;
                            cecompound = new CEPrecursorIon(cembio_id_code, compound_id,
                                    ce_identification_level, ce_sample_type,
                                    ce_exp_properties, capillary_length, capillary_voltage, 0,
                                    mz_theoretical, mz_experimental,
                                    mt_met, rmt_met,
                                    eff_mobility, commercial);
                            System.out.println("IDENTIFICATION LEVEL 2 or 3"
                                    + " NOT INSERTED" + cecompound);
                        }
                    } else {
                        Integer ce_eff_mob_id = 0;

                        for (Integer ce_sample_type : ce_sample_types) {
                            if (null == ce_sample_type) {
                                CEPrecursorIon cecompound = new CEPrecursorIon(cembio_id_code, compound_id,
                                        ce_identification_level, ce_sample_type,
                                        ce_exp_properties, capillary_length, capillary_voltage, 0,
                                        mz_theoretical, mz_experimental,
                                        mt_met, rmt_met,
                                        eff_mobility, commercial);
                                System.out.println("ERROR: READING A SAMPLE TYPE NOT RECOGNIZED ("
                                        + "STANDARD OR SAMPLE " + cecompound);
                            } else // the sample was a standard (1)
                            {

                                switch (ce_sample_type) {

                                    case 1: {
                                        bge = 180838;
                                        CEPrecursorIon cecompound = new CEPrecursorIon(cembio_id_code, compound_id,
                                                ce_identification_level, ce_sample_type,
                                                ce_exp_properties, capillary_length, capillary_voltage, bge,
                                                mz_theoretical, mz_experimental,
                                                mt_met, rmt_met,
                                                eff_mobility, commercial);
                                        //System.out.println("CECOMPOUND: " + cecompound);
                                        ce_eff_mob_id = db.insertCECompoundEffMob(cecompound);
                                        if (ce_eff_mob_id == 0) {
                                            System.out.println("UNEXPECTED ERROR INSERTING THE COMPOUND: " + cecompound);
                                            break;
                                        }
                                        cecompound.setCe_eff_mob_id(ce_eff_mob_id);
                                        if (rmt_met > 0) {
                                            //System.out.println("CECOMPOUND: " + cecompound);
                                            int keyMetadata = db.insertCEExperimentalPropertiesMetadata(cecompound);
                                            if (keyMetadata == 0) {
                                                System.out.println("METADATA OF: " + cecompound + " not inserted");
                                            }
                                        }
                                        if (rmt_par > 0) {
                                            bge = 73414;
                                            cecompound = new CEPrecursorIon(cembio_id_code, compound_id, ce_eff_mob_id,
                                                    ce_identification_level, ce_sample_type,
                                                    ce_exp_properties, capillary_length, capillary_voltage, bge,
                                                    mz_theoretical, mz_experimental,
                                                    mt_met, rmt_par,
                                                    eff_mobility, commercial);
                                            //System.out.println("CECOMPOUND: " + cecompound);
                                            int keyMetadata = db.insertCEExperimentalPropertiesMetadata(cecompound);
                                            if (keyMetadata == 0) {
                                                System.out.println("METADATA OF: " + cecompound + " not inserted");
                                            }

                                        }
                                        break;
                                    } // the sample was a plasma (2)
                                    case 2: {
                                        if (ce_eff_mob_id == 0) {
                                            System.out.println("NO ID FOR COMPOUND " + cembio_id_code + " INSERTING METADATA OF PLASMA ");
                                            break;
                                        } else {
                                            if (rmt_met_plasma > 0) {

                                                bge = 180838;
                                                CEPrecursorIon cecompound = new CEPrecursorIon(cembio_id_code, compound_id, ce_eff_mob_id,
                                                        ce_identification_level, ce_sample_type,
                                                        ce_exp_properties, capillary_length, capillary_voltage, bge,
                                                        mz_theoretical, mz_experimental,
                                                        mt_met_plasma, rmt_met_plasma,
                                                        eff_mobility, commercial);
                                                //System.out.println("CECOMPOUND: " + cecompound);
                                                int keyMetadata = db.insertCEExperimentalPropertiesMetadata(cecompound);
                                                if (keyMetadata == 0) {
                                                    System.out.println("METADATA OF: " + cecompound + " not inserted");
                                                }

                                            }
                                            if (rmt_par_plasma > 0) {

                                                bge = 73414;
                                                CEPrecursorIon cecompound = new CEPrecursorIon(cembio_id_code, compound_id, ce_eff_mob_id,
                                                        ce_identification_level, ce_sample_type,
                                                        ce_exp_properties, capillary_length, capillary_voltage, bge,
                                                        mz_theoretical, mz_experimental,
                                                        mt_par_plasma, rmt_par_plasma,
                                                        eff_mobility, commercial);
                                                //System.out.println("CECOMPOUND: " + cecompound);
                                                int keyMetadata = db.insertCEExperimentalPropertiesMetadata(cecompound);
                                                if (keyMetadata == 0) {
                                                    System.out.println("METADATA OF: " + cecompound + " not inserted");
                                                }

                                            }
                                        }
                                    }
                                    break;
                                    default:
                                        System.out.println("SAMPLE TYPE " + ce_sample_type + " WRONG");
                                }
                            }
                        }
                    }
                    // Update the eff mobility 
                    //db.updateEffMob(compound_id, eff_mobility);
                    //db.updateCompoundName(compound_id, name);
                }
            }
        }
    }

    public static void readProductIons(CMMDatabase db) throws IOException {
        String CEFilePath = "resources/CE/CEMS_fragments.xlsx";
        File excelFile = new File(CEFilePath);
        // we create an XSSF Workbook object for our XLSX Excel File
        FileInputStream fis = new FileInputStream(excelFile);
        try (// we create an XSSF Workbook object for our XLSX Excel File
                XSSFWorkbook workbook = new XSSFWorkbook(fis)) {

            // we get first sheet
            XSSFSheet sheet = workbook.getSheetAt(0);
            int totalRows = sheet.getPhysicalNumberOfRows();
            System.out.println("READING " + (totalRows - 1) + " product ions");

            Map<String, Integer> mapHeaders = readHeaders(CEFilePath);
            // Create the variables for the CE compound
            double monoisopotic_mass;
            double ce_product_ion_mz;
            double mt_absolute;
            double rmt;
            double eff_mob;
            String ce_product_ion_name;
            int compound_id;
            String ce_transformation_type;
            String analyst;
            int cembio_id_code; // ce_ms_id of the precursor ion
            int voltage;
            Double ce_product_ion_intensity; // from 0 to 100
            // Variables not taken from the file
            int ionization_mode = 1; // positive

            // we iterate on rows
            Iterator<Row> rowIt = sheet.iterator();
            // skip the header
            rowIt.next();
            int product_ion_id = 0;
            while (rowIt.hasNext()) {
                Row row = rowIt.next();

                // iterate on cells for the current row
                Iterator<Cell> cellIterator = row.cellIterator();

                monoisopotic_mass = (int) cellIterator.next().getNumericCellValue();
                ce_product_ion_mz = (double) cellIterator.next().getNumericCellValue();
                mt_absolute = (double) cellIterator.next().getNumericCellValue();
                rmt = (double) cellIterator.next().getNumericCellValue();
                eff_mob = (double) cellIterator.next().getNumericCellValue();
                ce_product_ion_name = cellIterator.next().getStringCellValue();
                ce_transformation_type = cellIterator.next().getStringCellValue().trim();
                analyst = cellIterator.next().getStringCellValue();
                cembio_id_code = (int) cellIterator.next().getNumericCellValue();
                voltage = (int) cellIterator.next().getNumericCellValue();
                ce_product_ion_intensity = (double) cellIterator.next().getNumericCellValue(); // from 0 to 100
                if (ce_product_ion_intensity < 0.001d) {
                    ce_product_ion_intensity = null;
                }
                if (cembio_id_code >= 242 && cembio_id_code < 243) {
                    CEPrecursorIon ceprecursorIon = db.getCEPrecursorIonFromCembioIdAndIonizationMode(cembio_id_code, ionization_mode);
                    CEProductIon ceproductIon = new CEProductIon(product_ion_id, voltage,
                            ce_product_ion_mz, ce_product_ion_intensity, ce_transformation_type,
                            ce_product_ion_name, ceprecursorIon, null);
                    //System.out.println("CEProductIon: " + ceproductIon);
                    db.insertCEProductIon(ceproductIon);
                    product_ion_id++;
                } else {
                    // System.out.println("ERROR in a PRODUCT ION. The precursor ion " + cembio_id_code + " is not present in the database");
                }

            }
        }
    }

    

    public static void readPrecursorIonsFromMSMLS(CMMDatabase db) throws IOException {
        String CEFilePath = "resources/CE/IDs_database_GVA_191111.csv";
        BufferedReader br = null;
        String line;
        String csvSplitBy = ";";

        try {
            br = new BufferedReader(new FileReader(CEFilePath));
            int lineNumber = 0;
            br.readLine();
            while ((line = br.readLine()) != null) {
                // Create the variables for the CE compound
                Integer chebId;
                String compound_name;
                String formulaFromInChI;
                String inchi;
                String inchiKey;
                Double eff_mob_pos_dir;
                Double eff_mob_neg_inv;

                // Variables not taken from the file
                int ce_identification_level = 1;
                String ce_sample_typeString = "standard";
                Integer ce_sample_type = 1;
                int ionization_mode; // 1: positive, 2: negative
                int polarity; // 1: direct, 2 inverse
                int buffer = 2; // formic acid 10% methanol = 1 or acetic acid = 2
                int temperature = 25; // methionine sulfone
                CE_EXP_PROP_ENUM ce_exp_properties;

                int capillary_length = 1000; // capilar length in milimeters
                int capillary_voltage = 30; // voltage in kV

                String commercial = "SIGMA-ALDRICH";

                // Variables obtained from the database
                int compound_id;

                if (line.endsWith(csvSplitBy)) {
                    line = line + " ;";
                }
                String[] compound = line.split(csvSplitBy);
                String internal_id = compound[0];
                try {
                    chebId = Integer.parseInt(compound[1]);
                } catch (NumberFormatException e) {
                    // if there is no chebid
                    chebId = null;
                }

                Integer cembio_id = generateCembioIdFromGeneveSR(compound[0]);
                Integer ce_eff_mob_id;
                compound_name = compound[2];
                inchi = compound[3];
                compound_id = db.getCompoundIdFromInchi(inchi.trim());
                if (compound_id == 0) {
                    System.out.println("NO COMPOUND FOUND FOR: " + compound[0]);
                } else {
                    // They are already inserted, but if new compounds are introduced, 
                    // they should be inserted in the chebi table
                    db.insertCompoundCHEBI(compound_id, chebId);

                    try {
                        eff_mob_pos_dir = Double.parseDouble(compound[4]);
                        ionization_mode = 1;
                        polarity = 1;
                        try {
                            CE_BUFFER_ENUM ce_buffer_enum = CE_BUFFER_ENUM.fromBufferCode(buffer);
                            ce_exp_properties = CE_EXP_PROP_ENUM.fromBufferTemperatureIonModeAndPolarity(ce_buffer_enum, temperature, ionization_mode, polarity);

                            Integer ce_exp_prop_id = db.getCE_exp_prop_Id(buffer, temperature, ionization_mode, polarity);
                            if (ce_exp_prop_id == null) {
                                System.out.println("THE EXPERIMENTAL CONDITIONS ARE NOT SUPPORTED BY THE CMM DATABASE, "
                                        + "plase contact with cmm.cembio@ceu.es");
                                return;
                            }
                            CEPrecursorIon cecompound = new CEPrecursorIon(cembio_id, compound_id, ce_exp_prop_id,
                                    ce_identification_level, ce_sample_type, ce_exp_properties,
                                    capillary_length, capillary_voltage, null, null, null,
                                    null, null,
                                    eff_mob_pos_dir, commercial);

                            //System.out.println(cecompound);
                            ce_eff_mob_id = db.insertCECompoundEffMob(cecompound);
                            if (ce_eff_mob_id == 0) {
                                System.out.println("UNEXPECTED ERROR INSERTING THE COMPOUND POS EFF MOB: " + cecompound);
                            } else {
                                cecompound.setCe_eff_mob_id(ce_eff_mob_id);
                                int keyMetadata = db.insertCEExperimentalPropertiesMetadata(cecompound);
                                if (keyMetadata == 0) {
                                    System.out.println("METADATA OF: " + cecompound + " not inserted");
                                }
                            }
                        } catch (IncorrectTemperatureException ex) {
                            System.out.println("ROW: " + lineNumber + " NOT INSERTED BECAUSE OF WRONG TEMPERATURE");
                        } catch (IncorrectBufferException ex) {
                            System.out.println("ROW: " + lineNumber + " NOT INSERTED BECAUSE OF WRONG BUFFER");
                        } catch (IncorrectIonizationModeException ex) {
                            System.out.println("ROW: " + lineNumber + " NOT INSERTED BECAUSE OF WRONG ION MODE");
                        } catch (IncorrectPolarityException ex) {
                            System.out.println("ROW: " + lineNumber + " NOT INSERTED BECAUSE OF WRONG POLARITY");
                        }
                    } catch (NumberFormatException ex) {
                        eff_mob_pos_dir = null;
                        //System.out.println("NO POS EFF MOB OF :" + compound[0] + compound[4]);
                    }
                    try {
                        eff_mob_neg_inv = Double.parseDouble(compound[5]);
                        ionization_mode = 2;
                        polarity = 2;
                        try {
                            CE_BUFFER_ENUM ce_buffer_enum = CE_BUFFER_ENUM.fromBufferCode(buffer);
                            ce_exp_properties = CE_EXP_PROP_ENUM.fromBufferTemperatureIonModeAndPolarity(ce_buffer_enum, temperature, ionization_mode, polarity);

                            Integer ce_exp_prop_id = db.getCE_exp_prop_Id(buffer, temperature, ionization_mode, polarity);
                            if (ce_exp_prop_id == null) {
                                System.out.println("THE EXPERIMENTAL CONDITIONS ARE NOT SUPPORTED BY THE CMM DATABASE, "
                                        + "plase contact with cmm.cembio@ceu.es");
                                return;
                            }
                            CEPrecursorIon cecompound = new CEPrecursorIon(cembio_id, compound_id, ce_exp_prop_id,
                                    ce_identification_level, ce_sample_type, ce_exp_properties,
                                    capillary_length, capillary_voltage, null, null, null,
                                    null, null,
                                    eff_mob_neg_inv, commercial);
                            //System.out.println(cecompound);
                            ce_eff_mob_id = db.insertCECompoundEffMob(cecompound);
                            if (ce_eff_mob_id == 0) {
                                System.out.println("UNEXPECTED ERROR INSERTING THE COMPOUND NEG EFF MOB: " + cecompound);

                            } else {
                                cecompound.setCe_eff_mob_id(ce_eff_mob_id);
                                int keyMetadata = db.insertCEExperimentalPropertiesMetadata(cecompound);
                                if (keyMetadata == 0) {
                                    System.out.println("METADATA OF: " + cecompound + " not inserted");
                                }
                            }
                        } catch (IncorrectTemperatureException ex) {
                            System.out.println("ROW: " + lineNumber + " NOT INSERTED BECAUSE OF WRONG TEMPERATURE");
                        } catch (IncorrectBufferException ex) {
                            System.out.println("ROW: " + lineNumber + " NOT INSERTED BECAUSE OF WRONG BUFFER");
                        } catch (IncorrectIonizationModeException ex) {
                            System.out.println("ROW: " + lineNumber + " NOT INSERTED BECAUSE OF WRONG ION MODE");
                        } catch (IncorrectPolarityException ex) {
                            System.out.println("ROW: " + lineNumber + " NOT INSERTED BECAUSE OF WRONG POLARITY");
                        }
                    } catch (NumberFormatException ex) {
                        //System.out.println("NO NEG EFF MOB OF :" + compound[0] + compound[5]);
                    }

                }
            }

            lineNumber++;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private static Integer generateCembioIdFromGeneveSR(String geneveSR) {
        Integer cembioId = 100000;
        String subGeneveSR = geneveSR.substring(2);
        try {
            cembioId = cembioId + Integer.parseInt(subGeneveSR);
        } catch (NumberFormatException ex) {
            System.out.println("Wrong SR Number from Geneve " + geneveSR);
        }

        return cembioId;
    }
}
