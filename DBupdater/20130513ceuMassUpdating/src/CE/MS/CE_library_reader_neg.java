/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CE.MS;

import databases.CMMDatabase;
import databases.PubchemRest;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Compound;
import utilities.Checker;

/**
 *
 * @author alberto.gildelafuent
 */
public class CE_library_reader_neg {

    /**
     * Executes the main method for the CE_library_reader
     *
     * @param args
     * @throws java.io.IOException
     */
    public static void main(String[] args) {

        CMMDatabase db = new CMMDatabase();
        db.connectToDB("jdbc:mysql://localhost/compounds?useSSL=false&serverTimezone=UTC", "alberto", "alberto");
        String csvInputPath = "C:\\Users\\alberto.gildelafuent\\Desktop\\temporal\\Tabla_patrones_CEMSnegnegCorregido.csv";
        String csvOutputPath = "C:\\Users\\alberto.gildelafuent\\Desktop\\temporal\\Tabla_patrones_CEMSnegnegCorregido_inchis.csv";
        getInchisFromDB(db, csvInputPath, csvOutputPath);
    }

    public static void getInchisFromDB(CMMDatabase db, String csvInputPath, String csvOutputPath) {

        File csvInputFile = new File(csvInputPath);
        File csvOutputFile = new File(csvOutputPath);
        BufferedReader br = null;
        FileWriter fileWriter = null;
        String line;
        String csvSplitBy = "\\|";
        String csvSplitByNonEscaped = "|";

        try {
            br = new BufferedReader(new FileReader(csvInputFile));
            fileWriter = new FileWriter(csvOutputFile);
            String header = "COMPOUND" + csvSplitByNonEscaped + "INCHI Structure" + csvSplitByNonEscaped + "Formula+" + csvSplitByNonEscaped + "M";
            fileWriter.write(header + "\n");
            int lineNumber = 2;
            // skip headers
            br.readLine();

            // Create the variables for finding inchi
            String compound_name;
            String formula;
            Double monoisotopic_mass;
            String formulaFromInChI;
            String inchi;
            String inchiKey;
            int foundCompoundsCMM = 0;
            int foundCompoundsWithInchiCMM = 0;
            int foundCompoundsPC = 0;
            while ((line = br.readLine()) != null) {
                String[] compound = line.split(csvSplitBy);
                compound_name = compound[0].trim();
                formula = compound[2].trim();
                try {
                    monoisotopic_mass = Double.parseDouble(compound[3].trim());
                } catch (NumberFormatException nfe) {
                    monoisotopic_mass = null;
                    System.out.println("Line " + lineNumber + " WITH NO MASS");
                }
                // System.out.println(compound_name + " , " + formula + " , " + monoisotopic_mass);

                // Try to find in DB
                int idCompound = db.getCompoundIdFromCompoundName(compound_name);
                if (idCompound > 0) {
                    inchi = db.getInChIFromCompound(idCompound);
                    if (!inchi.equals("")) {
                        foundCompoundsWithInchiCMM++;
                        formulaFromInChI = Checker.getFormulaFromInChI(inchi);
                        if (!formulaFromInChI.equals(formula)) {
                            System.out.println("Line " + lineNumber + " Compound: " + idCompound + " WITH Different formula -> Formula: " + formula + "\t FROM INCHI: " + formulaFromInChI);
                        } else {

                        }
                    } else {
                        System.out.println("Compound: " + idCompound + " WITH NO INCHI");
                    }
                    foundCompoundsCMM++;
                } else {
                    inchi = "";
                    // Try to find in pubchem
                    try {
                        Compound compoundPC = PubchemRest.getCompoundFromName(compound_name);
                        foundCompoundsPC++;
                        inchi = compoundPC.getIdentifiers().getInchi();
                        if (!inchi.equals("")) {
                            foundCompoundsWithInchiCMM++;
                            formulaFromInChI = Checker.getFormulaFromInChI(inchi);
                            if (!formulaFromInChI.equals(formula)) {
                                System.out.println("Line " + lineNumber + " Compound: " + idCompound + " WITH Different formula -> Formula: " + formula + "\t FROM INCHI: " + formulaFromInChI);
                            }
                        } else {
                            System.out.println("Compound: " + idCompound + " WITH NO INCHI");
                        }
                    } catch (org.apache.hc.client5.http.HttpResponseException nfe) {
                        // Try to find in HMDB
                    }

                }
                lineNumber++;
                String newCompoundString = compound_name + csvSplitByNonEscaped + inchi + csvSplitByNonEscaped + formula + csvSplitByNonEscaped + monoisotopic_mass;
                fileWriter.write(newCompoundString + "\n");
            }
            System.out.println("FOUND COMPOUNDS: " + foundCompoundsCMM + " WITH INCHI: " + foundCompoundsWithInchiCMM);
            System.out.println("FOUND COMPOUNDS in PUBCHEM " + foundCompoundsPC);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            if (fileWriter != null) {
                try {

                    fileWriter.close();
                } catch (IOException ex) {
                    Logger.getLogger(CE_library_reader_neg.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
