/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aspergillus;

import com.hp.hpl.jena.reasoner.rulesys.builtins.Equal;
import databases.CMMDatabase;
import databases.ChebiDatabase;
import databases.ChemSpiderRest;
import databases.PubchemRest;
import exceptions.AspergillusNotFoundException;
import exceptions.ChebiException;
import exceptions.CompoundNotClassifiedException;
import exceptions.InvalidFormulaException;
import exceptions.NodeNotFoundException;
import exceptions.OrganismNotFoundException;
import exceptions.WrongRequestException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Compound;
import model.Identifier;
import model.OrganismClassification;
import model.Reference;
import patternFinders.PatternFinder;
import uk.ac.ebi.chebi.webapps.chebiWS.model.ChebiWebServiceFault_Exception;
import static utilities.Constants.CLASSYFIRE_LIPID_NODEID;
import utilities.Utilities;

/**
 *
 * @author ceu
 */
public class AspergillusFromA2MDB {

    private static final Map<String, String[]> MAPCOMPOUNDSA2MDB = new HashMap();

    /**
     * Read the data from the file A2MDB and save the compound name and the info
     * from A2MDB with an array of 11 positions
     *
     * @param file
     * @throws IOException
     * @throws FileNotFoundException
     */
    public static void loadAspergillusCompounds(String file) throws IOException, FileNotFoundException, AspergillusNotFoundException {
        File fileAspergillus = new File(file);
        if (fileAspergillus.exists()) {
            FileInputStream fis = new FileInputStream(fileAspergillus);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            // read header and escape it
            String compoundAspergillus = br.readLine();
            int line = 2;
            compoundAspergillus = br.readLine();
            while (compoundAspergillus != null) {
                // read each compound
                String[] data = compoundAspergillus.split("\\|");
                String compound_name = data[0];

                if (data.length != 11) {
                    throw new AspergillusNotFoundException("Compound with bad structure in line " + line + " -> Check it",
                            exceptions.ErrorTypes.ERROR_TYPE.BAD_STRUCTURE);
                }
                MAPCOMPOUNDSA2MDB.put(compound_name, data);
                line++;
                compoundAspergillus = br.readLine();
            }

        } else {
            throw new FileNotFoundException("aspergillus file not found");
        }
    }

    /**
     * Read the data from the file according to the aspergillus input file. The
     * aspergillus file should contain elements with the next structure: A
     * header containing the next tags in the order here specified:
     * Secondary_metabolite;MeSH_Nomenclature;IUPAC_Classification;Formula;exact_mass;InChI;InChIKey;Smiles;LogP;PC_ID;Aspergillus_link
     * A list of elements with data *
     * Ethanol;Acyclic%20Alchol;Alchol;C2H6O;46.041865;InChI=1S/C2H6O/
     *
     * @param db the database to insert the data
     */
    public static void insertFromAspergillusTXTFile(CMMDatabase db) {
        int found_and_present = 0;
        int found_not_present = 0;
        int not_found = 0;
        int bad_structure = 0;
        for (Map.Entry<String, String[]> data : MAPCOMPOUNDSA2MDB.entrySet()) {
            // read each compound

            try {
                int insertion = insertAspergillusCompound(data.getValue(), db);

                if (insertion > 0) {
                    found_and_present++;
                } else {
                    found_not_present++;
                }
            } catch (AspergillusNotFoundException ex) {
                if (ex.getError_type().equals(exceptions.ErrorTypes.ERROR_TYPE.NOT_FOUND)) {
                    not_found++;
                } else {
                    System.out.println("bad structure in compound -> " + data.getKey() + " -> " + data.getValue());
                    bad_structure++;
                }
            }
        }
        System.out.println("FOUND PRESENT -> " + found_and_present);
        System.out.println("FOUND NOT PRESENT -> " + found_not_present);
        System.out.println("NOT FOUND -> " + not_found);
        System.out.println("BAD STRUCTURE -> " + bad_structure);

    }

    private static int insertAspergillusCompound(String[] data, CMMDatabase db)
            throws AspergillusNotFoundException {

        if (data.length != 11) {
            throw new AspergillusNotFoundException("Compound with bad structure. Check it",
                    exceptions.ErrorTypes.ERROR_TYPE.BAD_STRUCTURE);
        }
        String compound_name = data[0];
        String mesh_nomenclature = data[1];
        String iupac_classification = data[2];
        String formula = data[3];
        if (formula.equals("Not FOUND")) {
            throw new AspergillusNotFoundException("Compound not found",
                    exceptions.ErrorTypes.ERROR_TYPE.NOT_FOUND);
        }
        String exact_mass_string = data[4];
        Double exact_mass = null;
        try {
            exact_mass = Double.parseDouble(exact_mass_string);
        } catch (NumberFormatException nfe) {
            System.out.println("Mass not present" + data);
        }
        String inchi = data[5];
        String inchiKey = data[6];
        String smiles = data[7];
        String logP_string = data[8];
        Double logP = null;
        try {
            logP = Double.parseDouble(logP_string);
        } catch (NumberFormatException nfe) {
            System.out.println("logP not present" + data);
        }
        String pc_id_string = data[9];
        Integer pc_id = null;
        try {
            pc_id = Integer.parseInt(pc_id_string);
        } catch (NumberFormatException nfe) {
            System.out.println("PC_ID not present" + data);
        }
        String aspergillus_link = data[10];
        if (aspergillus_link.equals("NOT FOUND")) {
            aspergillus_link = null;
        } else {
            aspergillus_link = compound_name;
        }

        Identifier partialIdentifiers = new Identifier(inchi, inchiKey, smiles);
        Integer chebId = 0;
        try {
            chebId = ChebiDatabase.getChebiFromIdentifiers(partialIdentifiers);
        } catch (ChebiException ex) {
            // System.out.println(knapSackId + " -> CHEBI NOT FOUND");
        } catch (Exception ex) {
            // System.out.println(knapSackId + " -> ERROR OBTAINING CHEBI: " + ex.getMessage());
        }
        Integer pubChemId = 0;
        try {
            Identifier IdentifiersFromPubchem = PubchemRest.getIdentifiersFromInChIPC(inchi);
            if (inchiKey.equals("")) {
                inchiKey = IdentifiersFromPubchem.getInchi_key();
            }
            if (smiles.equals("")) {
                smiles = IdentifiersFromPubchem.getSmiles();
            }
            pubChemId = PubchemRest.getPCIDFromInchiKey(IdentifiersFromPubchem.getInchi_key());

        } catch (IOException ex) {
            // System.out.println(knapSackId + " -> ERROR OBTAINING PUBCHEM: " + ex);
        } catch (NullPointerException ex) {
            // System.out.println(knapSackId + " -> Pubchem NOT FOUND");
        }
        Identifier identifiers = new Identifier(inchi, inchiKey, smiles);

        // Insert into BBDD
        Integer compoundId = db.getCompoundIdFromInchiKey(inchiKey);
        int compoundType = 0;
        List<String> ancestorNodes = new LinkedList<String>();
        try {
            ancestorNodes = downloadersOfWebResources.DownloaderOfWebResources.getAncestorNodesCompoundClassyfireClassification(inchi, inchiKey);
            if (ancestorNodes.contains(CLASSYFIRE_LIPID_NODEID)) {
                compoundType = 1;
            }
        } catch (CompoundNotClassifiedException ex) {
        }
        String biologicalActivity = null;
        if (compoundId == 0) {
            // INCHI FROm KNAPSACK CAN BE MISSCALCULATED
            compoundId = db.getCompoundIdFromInchi(inchi);
            if (compoundId == 0) {
                compoundId = db.getCompoundIdFromCompoundName(compound_name);
            }
            if (compoundId == 0) {
                // Check if the compound does not have identifiers to add them
                String casId = null;
                // insert compound
                int[] charges = PatternFinder.getChargeFromSmiles(smiles);
                int chargeType = charges[0];
                int numCharges = charges[1];
                String formulaType = PatternFinder.getTypeFromFormula(formula);
                boolean correctFormula;
                correctFormula = !formulaType.equals("");

                Integer compoundStatus = 1;
                String knapSackId = null;
                /*
                AspergillusCompound aspergillusCompound = new AspergillusCompound(knapSackId, pubChemId, chebId, biologicalActivity, compoundId,
                        compound_name, casId, formula, exact_mass, compoundStatus, compoundType, logP, identifiers, null, null, null);
                 */
                compoundId = db.insertCompound(casId, compound_name, formula, exact_mass, chargeType, numCharges, formulaType, compoundType, compoundStatus, logP);

                db.insertIdentifiers(compoundId, inchi, inchiKey, smiles);
            }
        }
        try {
            // Insert classyfire
            db.insertFullCompoundClassyfireClassification(compoundId, ancestorNodes);

        } catch (NodeNotFoundException ex) {
            Logger.getLogger(AspergillusFromA2MDB.class
                    .getName()).log(Level.SEVERE, null, ex);
            System.out.println("NODE NOT FOUND: " + ex);
        }

        if (chebId != 0) {
            db.insertCompoundCHEBI(compoundId, chebId);
        }
        if (pubChemId != 0) {
            db.insertCompoundPC(compoundId, pubChemId);
        }
        if (db.checkAspergillusExist(compoundId)) {
            db.updateCompoundAspergillus(compoundId, mesh_nomenclature, iupac_classification);
        } else {
            db.insertCompoundAspergillus(compoundId, biologicalActivity, mesh_nomenclature, iupac_classification, aspergillus_link);
        }
        try {
            Integer aspergillusOrganismId;
            aspergillusOrganismId = db.getOrganismIdFromOrganismName("Aspergillus");
            db.insertCompoundOrganism(compoundId, aspergillusOrganismId);
        } catch (OrganismNotFoundException ex) {
            Logger.getLogger(AspergillusFromA2MDB.class.getName()).log(Level.SEVERE, null, ex);
        }

        return compoundId;

    }

    public static void classifyCompoundsfromA2MDB(String fileName, CMMDatabase db) throws IOException, FileNotFoundException {
        File fileAspergillus = new File(fileName);
        if (fileAspergillus.exists()) {
            FileInputStream fis = new FileInputStream(fileAspergillus);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            // read header and escape it
            String A2MDBLineString = br.readLine();
            int line = 2;
            A2MDBLineString = br.readLine();
            String organismName = "";
            Integer organism_id = null;
            String kingdom = "Fungi";
            String family = "Trichocomaceae";
            String species = null;
            String subspecie = null;
            Set<String> notFoundElements = new TreeSet();
            while (A2MDBLineString != null) {
                // read each compound
                Integer compoundId = 0;
                Integer pc_id = null;
                String casId = null;
                String db_link = null;
                String reference_pubmed = null;
                String[] data = A2MDBLineString.split("\\|");
                String organism_name_partial = data[0];
                organism_name_partial = organism_name_partial.replaceAll("\"", "");
                if (!organism_name_partial.equals("")) {
                    organismName = organism_name_partial;
                    try {
                        organism_id = db.getOrganismIdFromOrganismName(organismName);
                    } catch (OrganismNotFoundException ex) {
                        // Insert the organism into the db
                        /*
                        species = PatternFinder.getGenusSpecieFromGenusSpecieAndSubspecies(organismName);
                        subspecie = PatternFinder.getSubspecieFromGenusSpecieAndSubspecie(organismName);
                         */
                        OrganismClassification organismClassification;
                        try {
                            //organismClassification = new OrganismClassification(pc_id, kingdom, family, species, subspecie, null);
                            organismClassification = new OrganismClassification(0, kingdom, family, organismName, null, null);
                            organism_id = db.insertOrganism(organismClassification);
                            organismClassification.setOrganismId(organism_id);
                        } catch (Exception ex1) {
                            Logger.getLogger(AspergillusFromA2MDB.class.getName()).log(Level.SEVERE, null, ex1);
                        }

                    }
                }
                // Compound with no reference
                String compound_name = data[1];

                if (!compound_name.equalsIgnoreCase("Nil")) {
                    // If the compound has information about cas, pubchem or reference we should introduce this info
                    try {
                        casId = data[2];
                    } catch (IndexOutOfBoundsException ex) {
                        casId = null;
                    }
                    try {
                        db_link = data[3];
                        pc_id = PatternFinder.searchPCID(db_link);
                    } catch (IndexOutOfBoundsException ex) {
                        db_link = null;
                    }

                    if (pc_id != null) {
                        compoundId = db.getCompoundIdFromPCID(pc_id);
                    }
                    if (compoundId == 0) {
                        compoundId = db.getCompoundIdFromCompoundName(compound_name);
                    }

                    if (compoundId != 0) {
                        if (pc_id != null) {
                            db.insertCompoundPC(compoundId, pc_id);
                        }
                        if (casId != null && !casId.equalsIgnoreCase("")) {
                            // update CAS
                            Integer compoundIdFromCas = db.getCompounIdFromCAS(casId);
                            if (compoundIdFromCas == 0) {
                                db.updateCompoundCAS(compoundId, casId);
                            }

                        }
                    } else {
                        // Get data from pubchem and insert compound and all information related to the compound
                        Compound compound = null;
                        if (pc_id != null) {
                            try {
                                compound = PubchemRest.getCompoundFromPCID(pc_id);
                            } catch (IOException ex) {
                                System.out.println("PC_ID: " + pc_id + " NOT FOUND");
                            }
                        } else {
                            try {
                                compound = PubchemRest.getCompoundFromName(compound_name);
                            } catch (IOException ex) {
                                // System.out.println("PC compound name: " + compound_name + " NOT FOUND in line: " + line);
                            }
                        }
                        if (compound != null) {
                            Identifier identifiers = compound.getIdentifiers();
                            String inchi_key = identifiers.getInchi_key();
                            compoundId = db.getCompoundIdFromInchiKey(inchi_key);
                            try {
                                pc_id = PubchemRest.getPCIDFromInchiKey(inchi_key);
                            } catch (IOException ex) {
                                System.out.println("Inchi KEY of compound: " + compound_name + " NOT FOUND in line: " + line);
                            }
                            if (compoundId == 0) {
                                String formula = compound.getFormula();
                                Double exact_mass = compound.getMass();
                                Double logP = compound.getLogP();
                                String inchi = identifiers.getInchi();
                                String smiles = identifiers.getSmiles();
                                int[] charges = PatternFinder.getChargeFromSmiles(smiles);
                                int chargeType = charges[0];
                                int numCharges = charges[1];
                                String formulaType = PatternFinder.getTypeFromFormula(formula);
                                Integer compoundType = compound.getCompound_type();
                                Integer compoundStatus = compound.getCompound_status();
                                List<String> ancestorNodes = new LinkedList<String>();
                                try {
                                    ancestorNodes = downloadersOfWebResources.DownloaderOfWebResources.getAncestorNodesCompoundClassyfireClassification(inchi, inchi_key);
                                    if (ancestorNodes.contains(CLASSYFIRE_LIPID_NODEID)) {
                                        compoundType = 1;
                                    }
                                } catch (CompoundNotClassifiedException ex) {
                                }

                                compoundId = db.insertCompound(casId, compound_name, formula, exact_mass, chargeType, numCharges, formulaType, compoundType, compoundStatus, logP);

                                // Insert identifiers
                                db.insertIdentifiers(compoundId, inchi, inchi_key, smiles);

                                db.insertCompoundAspergillus(compoundId, null, null, null, null);
                                db.insertCompoundPC(compoundId, pc_id);
                                try {
                                    // Insert classyfire
                                    db.insertFullCompoundClassyfireClassification(compoundId, ancestorNodes);

                                } catch (NodeNotFoundException ex) {
                                    Logger.getLogger(AspergillusFromA2MDB.class
                                            .getName()).log(Level.SEVERE, null, ex);
                                    System.out.println("NODE NOT FOUND: " + ex);
                                }
                            }
                        }
                    }
                }
                if (!compound_name.equalsIgnoreCase("Nil") || data.length > 2) {
                    Set<Integer> setCompoundIdsGroup = new TreeSet();
                    if (data.length > 4) {
                        reference_pubmed = data[4];
                        String reference_texts = null;
                        String dois = null;
                        String reference_links = null;
                        try {
                            reference_texts = data[5];
                            if (reference_texts.equals("")) {
                                reference_texts = null;
                            }
                        } catch (ArrayIndexOutOfBoundsException ex) {
                        }
                        try {
                            dois = data[6];
                            if (dois.equals("")) {
                                dois = null;
                            }
                        } catch (ArrayIndexOutOfBoundsException ex) {
                        }
                        try {
                            reference_links = data[7];
                            if (reference_links.equals("")) {
                                reference_texts = null;
                            }
                        } catch (ArrayIndexOutOfBoundsException ex) {
                        }
                        Integer reference_id = null;
                        if (reference_texts != null || dois != null || reference_links != null) {
                            String[] referencesTextsArray = reference_texts.split("\\;");
                            String[] doisArray = new String[0];
                            if (dois != null) {
                                doisArray = dois.split("\\;");
                            }
                            String[] referencesLinksArray = new String[0];
                            if (reference_links != null) {
                                referencesLinksArray = reference_links.split("\\;");
                            }
                            for (int indexReference = 0; indexReference < referencesTextsArray.length; indexReference++) {
                                String reference_text = referencesTextsArray[indexReference];
                                String doi = null;
                                String reference_link = null;
                                try {
                                    doi = doisArray[indexReference];
                                } catch (IndexOutOfBoundsException ex) {
                                }
                                try {
                                    reference_link = referencesLinksArray[indexReference];
                                } catch (IndexOutOfBoundsException ex) {
                                }
                                Reference reference = new Reference(0, reference_text, doi, reference_link);
                                reference_id = db.insertReference(reference);
                                reference.setId(reference_id);
                                if (compoundId > 0) {
                                    db.insertCompoundReference(compoundId, reference_id);
                                } else {
                                    // Get group of ids 
                                    setCompoundIdsGroup = db.getCompoundIdsSetFromName(compound_name);
                                    for (Integer id_partial_from_group : setCompoundIdsGroup) {
                                        db.insertCompoundReference(id_partial_from_group, reference_id);
                                    }
                                }
                                if (organism_id != null) {
                                    db.insertOrganismReference(organism_id, reference_id);
                                }
                            }
                        }

                    }
                    if (compoundId > 0) {
                        if (organism_id != null) {
                            db.insertCompoundOrganism(compoundId, organism_id);
                        }

                    } else if (!compound_name.equalsIgnoreCase("nil")) {
                        if (setCompoundIdsGroup.isEmpty() && data.length > 4) {
                            notFoundElements.add(compound_name);
                        } else {
                            for (Integer id_partial_from_group : setCompoundIdsGroup) {
                                db.insertCompoundOrganism(id_partial_from_group, organism_id);
                            }
                        }

                    } else {
                        System.out.println("EMPTY NAME on line: " + line);
                    }
                }

                line++;
                A2MDBLineString = br.readLine();
            }

            System.out.println(notFoundElements.size() + " compounds not found -> ");
            for (String compoundNameNotFound : notFoundElements) {
                System.out.println(compoundNameNotFound);
            }

        } else {
            throw new FileNotFoundException("aspergillus file not found");
        }
    }

    public static void insertFromCuratedFile(String fileName, CMMDatabase db) throws IOException, FileNotFoundException {
        File fileAspergillus = new File(fileName);
        if (fileAspergillus.exists()) {
            FileInputStream fis = new FileInputStream(fileAspergillus);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            // read header and escape it
            String curatedLineString = br.readLine();
            int line = 2;
            curatedLineString = br.readLine();
            String organismName = "";
            Set<String> notFoundElements = new TreeSet();

            String kingdom = "Fungi";
            String family = "Trichocomaceae";

            while (curatedLineString != null) {
                System.out.println("Line: " + line);
                // read each compound
                Integer compound_id = null;
                Integer pc_id = null;
                String[] data = curatedLineString.split("\\|");
                // not used
                if (data.length > 7) {
                    String compound_name = data[1].trim();
                    String organisms = data[2].trim();
                    if (organisms.equals("")) {
                        organisms = null;
                    }
                    String biological_activity = data[3];
                    if (biological_activity.equals("")) {
                        biological_activity = null;
                    }
                    String formula = data[5].trim();
                    if (formula.equals("")) {
                        formula = null;
                    }

                    String inchi = data[7].trim();
                    if (inchi.equals("")) {
                        inchi = null;
                    }
                    String smiles;
                    try {
                        smiles = data[8].trim();
                        if (smiles.equals("")) {
                            smiles = null;
                        }
                    } catch (IndexOutOfBoundsException ex) {
                        smiles = null;
                    }
                    String inchiKey = null;
                    String dois;
                    try {
                        dois = data[10].trim();
                        if (dois.equals("")) {
                            dois = null;
                        }
                    } catch (IndexOutOfBoundsException ex) {
                        dois = null;
                    }
                    String reference_texts;
                    try {
                        reference_texts = data[11].trim();
                        if (reference_texts.equals("") || reference_texts.equals("NO INFO") || reference_texts.equals("PROTEINA")) {
                            reference_texts = null;
                        }
                    } catch (IndexOutOfBoundsException ex) {
                        reference_texts = null;
                    }

                    String reference_links = null;
                    try {
                        reference_links = data[12].trim();
                        if (reference_links.equals("")) {
                            reference_links = null;
                        }
                    } catch (ArrayIndexOutOfBoundsException ex) {
                        reference_links = null;
                    }

                    List<String> ancestorNodes = new LinkedList<String>();
                    if (inchi == null || inchi.equals("")) {
                        compound_id = db.getCompoundIdFromCompoundName(compound_name);
                    } else {
                        try {
                            inchiKey = ChemSpiderRest.getINCHIKeyFromInchi(inchi);
                        } catch (WrongRequestException ex) {
                            System.out.println("INCHI NOT FOUND FOR: " + line + " inchi: " + inchi);
                        }
                        compound_id = db.getCompoundIdFromInchi(inchi);
                        try {
                            Identifier identifiers = PubchemRest.getIdentifiersFromInChIPC(inchi);
                            inchiKey = identifiers.getInchi_key();
                            smiles = identifiers.getSmiles();

                            pc_id = PubchemRest.getPCIDFromInchiKey(identifiers.getInchi_key());
                        } catch (Exception ex) {
                            try {
                                Identifier identifiers = PubchemRest.getIdentifiersFromSMILESPC(smiles);
                                inchiKey = identifiers.getInchi_key();
                                inchi = identifiers.getInchi();

                                pc_id = PubchemRest.getPCIDFromInchiKey(identifiers.getInchi_key());
                            } catch (Exception exSmiles) {
                            }
                        }
                        if (compound_id == 0 && pc_id != null) {
                            compound_id = db.getCompoundIdFromPCID(pc_id);
                        }
                        // GET DATA TO INSERT COMPOUNDS
                        String casId = null;
                        Double exact_mass;
                        try {
                            int[] charges = PatternFinder.getChargeFromSmiles(smiles);
                            int chargeType = charges[0];
                            int numCharges = charges[1];
                            String formulaType = PatternFinder.getTypeFromFormula(formula);
                            boolean correctFormula;
                            correctFormula = !formulaType.equals("");

                            if (correctFormula) {
                                exact_mass = Utilities.getMassFromFormula(formula);

                                int compoundType = 0;

                                try {
                                    ancestorNodes = downloadersOfWebResources.DownloaderOfWebResources.getAncestorNodesCompoundClassyfireClassification(inchi, inchiKey);
                                    if (ancestorNodes.contains(CLASSYFIRE_LIPID_NODEID)) {
                                        compoundType = 1;
                                    }
                                } catch (CompoundNotClassifiedException ex) {
                                }

                                Integer compoundStatus = 1;
                                Double logP = null;

                                if (compound_id == 0) {
                                    compound_id = db.getCompoundIdFromCompoundName(compound_name);
                                    if (compound_id == 0) {
                                        // insert compound 
                                        compound_id = db.insertCompound(casId, compound_name, formula, exact_mass, chargeType, numCharges, formulaType, compoundType, compoundStatus, logP);

                                    }

                                    // Insert identifiers
                                    db.insertIdentifiers(compound_id, inchi, inchiKey, smiles);
                                }
                            }

                        } catch (InvalidFormulaException ex) {
                            System.out.println("MASS NOT CALCULATED FOR COMPOUND: " + line + " , name:" + compound_name + " formula -> " + formula);
                        }

                    }
                    if (compound_id > 0) {
                        // Insert information for compound_aspergillus
                        if (inchi != null) {
                            try {
                                Integer compound_id_from_inchi = db.getCompoundIdFromInchi(inchi);
                                if (compound_id_from_inchi > 0) {
                                    if (!compound_id.equals(compound_id_from_inchi)) {
                                        // remove duplicate compounds
                                        System.out.println("BREAK");
                                    }
                                } else {
                                    // insert identifiers
                                    inchiKey = ChemSpiderRest.getINCHIKeyFromInchi(inchi);
                                    smiles = ChemSpiderRest.getSMILESFromInchi(inchi);
                                    db.insertIdentifiers(compound_id, inchi, inchiKey, smiles);
                                }

                            } catch (WrongRequestException ex) {
                                Logger.getLogger(AspergillusFromA2MDB.class.getName()).log(Level.SEVERE, null, ex);
                            }

                        }
                        String[] dataFromA2MDB = MAPCOMPOUNDSA2MDB.get(compound_name);
                        String meshNomenclature = null;
                        String iupac_classification = null;
                        String aspergillus_link = null;
                        if (dataFromA2MDB != null) {
                            meshNomenclature = dataFromA2MDB[1];
                            iupac_classification = dataFromA2MDB[2];
                            aspergillus_link = dataFromA2MDB[10];
                            if (aspergillus_link.equals("NOT FOUND")) {
                                aspergillus_link = null;
                            } else {
                                aspergillus_link = compound_name;
                            }

                        }
                        // Update biological activity
                        if (!db.checkAspergillusExist(compound_id)) {
                            db.insertCompoundAspergillus(compound_id, biological_activity, meshNomenclature, iupac_classification, aspergillus_link);
                        } else {
                            db.updateCompoundAspergillus(compound_id, meshNomenclature, iupac_classification, aspergillus_link, biological_activity);
                        }

                        // Insert compound classification 
                        try {
                            // Insert classyfire
                            db.insertFullCompoundClassyfireClassification(compound_id, ancestorNodes);

                        } catch (NodeNotFoundException ex) {
                            Logger.getLogger(AspergillusFromA2MDB.class
                                    .getName()).log(Level.SEVERE, null, ex);
                            System.out.println("NODE NOT FOUND: " + ex);
                        }
                        // INSERT ORGANISMS
                        Set<Integer> organismIds = new TreeSet();
                        if (organisms != null) {
                            String[] organismsArray = organisms.split("\\;");
                            for (int indexOrganism = 0; indexOrganism < organismsArray.length; indexOrganism++) {
                                organismName = organismsArray[indexOrganism].trim();

                                try {
                                    Integer organism_id = db.getOrganismIdFromOrganismName(organismName);
                                    organismIds.add(organism_id);
                                    db.insertCompoundOrganism(compound_id, organism_id);
                                } catch (OrganismNotFoundException ex) {

                                    /*
                        species = PatternFinder.getGenusSpecieFromGenusSpecieAndSubspecies(organismName);
                        subspecie = PatternFinder.getSubspecieFromGenusSpecieAndSubspecie(organismName);
                                     */
                                    OrganismClassification organismClassification;
                                    try {
                                        //organismClassification = new OrganismClassification(pc_id, kingdom, family, species, subspecie, null);
                                        organismClassification = new OrganismClassification(pc_id, kingdom, family, organismName, null, null);
                                        Integer organism_id = db.insertOrganism(organismClassification);
                                        organismClassification.setOrganismId(organism_id);
                                        db.insertCompoundOrganism(compound_id, organism_id);
                                        organismIds.add(organism_id);

                                    } catch (Exception ex1) {
                                        Logger.getLogger(AspergillusFromA2MDB.class
                                                .getName()).log(Level.SEVERE, null, ex1);
                                    }
                                }
                            }
                        }
                        Set<Integer> referenceIds = new TreeSet();
                        Integer reference_id = null;
                        if (reference_texts != null || dois != null || reference_links != null) {
                            String[] referencesTextsArray = reference_texts.split("\\;");
                            String[] doisArray = new String[0];
                            if (dois != null) {
                                doisArray = dois.split("\\;");
                            }
                            String[] referencesLinksArray = new String[0];
                            if (reference_links != null) {
                                referencesLinksArray = reference_links.split("\\;");
                            }
                            for (int indexReference = 0; indexReference < referencesTextsArray.length; indexReference++) {
                                String reference_text = referencesTextsArray[indexReference].trim();
                                String doi = null;
                                String reference_link = null;
                                try {
                                    doi = doisArray[indexReference].trim();
                                } catch (IndexOutOfBoundsException ex) {
                                }
                                try {
                                    reference_link = referencesLinksArray[indexReference].trim();
                                } catch (IndexOutOfBoundsException ex) {
                                }
                                Reference reference = new Reference(0, reference_text, doi, reference_link);
                                reference_id = db.insertReference(reference);
                                db.insertCompoundReference(compound_id, reference_id);
                                reference.setId(reference_id);
                                for (Integer organism_id : organismIds) {
                                    db.insertOrganismReference(organism_id, reference_id);
                                }
                            }
                        }
                        if (pc_id != null) {
                            db.insertCompoundPC(compound_id, pc_id);
                        }

                    } else {
                        notFoundElements.add((compound_name));
                        //System.out.println("LINE: " + line + " from " + compound_name + " NOT FOUND in curated_aspergillus_metabolites.csv " + compound_id + " inchi: " + inchi);
                    }

                }

                line++;
                curatedLineString = br.readLine();
            }
            /*
            System.out.println(notFoundElements.size() + " compounds not found -> ");
            for (String compoundNameNotFound : notFoundElements) {
                System.out.println(compoundNameNotFound);
            }
             */

        } else {
            throw new FileNotFoundException("Cureated File of aspergillus: " + fileName + " not found");
        }
    }

    public static void insertFromCHEBI(String fileName, CMMDatabase db) throws IOException, FileNotFoundException {
        File fileAspergillus = new File(fileName);
        if (fileAspergillus.exists()) {
            FileInputStream fis = new FileInputStream(fileAspergillus);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            // read header and escape it
            String curatedLineString = br.readLine();
            int line = 2;
            curatedLineString = br.readLine();
            String organismName = "";
            Set<String> notFoundElements = new TreeSet();

            String kingdom = "Fungi";
            String family = "Trichocomaceae";

            while (curatedLineString != null) {
                System.out.println("Line: " + line);
                // read each compound
                Integer compound_id = null;
                Integer pc_id = null;
                String[] data = curatedLineString.split("\\|");
                // not used
                if (data.length == 6) {
                    Integer chebiId = null;
                    try {
                        chebiId = Integer.parseInt(data[0].trim());
                    } catch (NumberFormatException nfe) {

                        System.out.println("WRONG CHEBI ID -> CHECK LINE: " + line + " WITH TEXT: " + curatedLineString);
                    }
                    String compound_name = null;
                    try {
                        compound_name = ChebiDatabase.getAsciiName(chebiId);
                    } catch (ChebiWebServiceFault_Exception ex) {
                        System.out.println(" ERROR ACCESING CHEBI CLIENT FOR NAME -> " + ex);
                    }

                    String formula = null;
                    try {
                        formula = ChebiDatabase.getFormulaFromChebID(chebiId);
                    } catch (ChebiWebServiceFault_Exception ex) {
                        System.out.println(" ERROR ACCESING CHEBI CLIENT FOR FORMULA -> " + ex);
                    } catch (ChebiException ex) {
                        System.out.println(" ERROR ACCESING CHEBI CLIENT FOR FORMULA -> " + ex);
                    }

                    String inchi;
                    String smiles;
                    String inchiKey;
                    Identifier identifiers;
                    identifiers = ChebiDatabase.getIdentfiersFromChebiId(chebiId);
                    if (identifiers == null) {
                        inchi = null;
                        smiles = null;
                        inchiKey = null;
                    } else {
                        inchi = identifiers.getInchi();
                        smiles = identifiers.getSmiles();
                        inchiKey = identifiers.getInchi_key();
                    }

                    String biological_activity = data[1];
                    if (biological_activity.equals("")) {
                        biological_activity = null;
                    }
                    String organisms = data[2].trim();
                    if (organisms.equals("")) {
                        organisms = null;
                    }
                    String reference_texts;
                    try {
                        reference_texts = data[3].trim();
                        if (reference_texts.equals("") || reference_texts.equals("NO INFO") || reference_texts.equals("PROTEINA")) {
                            reference_texts = null;
                        }
                    } catch (IndexOutOfBoundsException ex) {
                        reference_texts = null;
                    }
                    String dois;
                    try {
                        dois = data[4].trim();
                        if (dois.equals("")) {
                            dois = null;
                        }
                    } catch (IndexOutOfBoundsException ex) {
                        dois = null;
                    }

                    String reference_links = null;
                    try {
                        reference_links = data[5].trim();
                        if (reference_links.equals("")) {
                            reference_links = null;
                        }
                    } catch (ArrayIndexOutOfBoundsException ex) {
                        reference_links = null;
                    }

                    List<String> ancestorNodes = new LinkedList<String>();
                    if (inchi == null || inchi.equals("")) {
                        compound_id = db.getCompoundIdFromCompoundName(compound_name);
                    } else {
                        compound_id = db.getCompoundIdFromInchi(inchi);
                        try {
                            pc_id = PubchemRest.getPCIDFromInchiKey(identifiers.getInchi_key());
                        } catch (Exception ex) {
                            System.out.println("ERROR OBTAININ PC_ID FROM CHEBI DATABASE -> " + chebiId + " IN LINE " + line);
                        }
                        if (compound_id == 0 && pc_id != null) {
                            compound_id = db.getCompoundIdFromPCID(pc_id);
                        }
                        // GET DATA TO INSERT COMPOUNDS
                        String casId = null;
                        Double exact_mass;
                        try {
                            int[] charges = PatternFinder.getChargeFromSmiles(smiles);
                            int chargeType = charges[0];
                            int numCharges = charges[1];
                            String formulaType = PatternFinder.getTypeFromFormula(formula);
                            boolean correctFormula;
                            correctFormula = !formulaType.equals("");

                            if (correctFormula) {
                                exact_mass = Utilities.getMassFromFormula(formula);

                                int compoundType = 0;

                                try {
                                    ancestorNodes = downloadersOfWebResources.DownloaderOfWebResources.getAncestorNodesCompoundClassyfireClassification(inchi, inchiKey);
                                    if (ancestorNodes.contains(CLASSYFIRE_LIPID_NODEID)) {
                                        compoundType = 1;
                                    }
                                } catch (CompoundNotClassifiedException ex) {
                                }

                                Integer compoundStatus = 1;
                                Double logP = null;

                                if (compound_id == 0) {
                                    compound_id = db.getCompoundIdFromCompoundName(compound_name);
                                    if (compound_id == 0) {
                                        // insert compound 
                                        compound_id = db.insertCompound(casId, compound_name, formula, exact_mass, chargeType, numCharges, formulaType, compoundType, compoundStatus, logP);
                                    }
                                    // Insert identifiers
                                    db.insertIdentifiers(compound_id, inchi, inchiKey, smiles);
                                }
                            }

                        } catch (InvalidFormulaException ex) {
                            System.out.println("MASS NOT CALCULATED FOR COMPOUND: " + line + " , name:" + compound_name + " formula -> " + formula);
                        }

                    }
                    if (compound_id > 0) {
                        // Insert information for compound_aspergillus
                        if (inchi != null) {
                            try {
                                Integer compound_id_from_inchi = db.getCompoundIdFromInchi(inchi);
                                if (compound_id_from_inchi > 0) {
                                    if (!compound_id.equals(compound_id_from_inchi)) {
                                        // remove duplicate compounds
                                        System.out.println("BREAK -> Compounds " + compound_id_from_inchi + " " + compound_id + " are duplicated");
                                    }
                                } else {
                                    // insert identifiers
                                    inchiKey = ChemSpiderRest.getINCHIKeyFromInchi(inchi);
                                    smiles = ChemSpiderRest.getSMILESFromInchi(inchi);
                                    db.insertIdentifiers(compound_id, inchi, inchiKey, smiles);
                                }

                            } catch (WrongRequestException ex) {
                                Logger.getLogger(AspergillusFromA2MDB.class.getName()).log(Level.SEVERE, null, ex);
                            }

                        }
                        String meshNomenclature = null;
                        String iupac_classification = null;
                        String aspergillus_link = null;

                        // Update biological activity
                        if (!db.checkAspergillusExist(compound_id)) {
                            db.insertCompoundAspergillus(compound_id, biological_activity, meshNomenclature, iupac_classification, aspergillus_link);
                        } else {
                            db.updateCompoundAspergillus(compound_id, meshNomenclature, iupac_classification, aspergillus_link, biological_activity);
                        }

                        // Insert compound classification 
                        try {
                            // Insert classyfire
                            db.insertFullCompoundClassyfireClassification(compound_id, ancestorNodes);

                        } catch (NodeNotFoundException ex) {
                            Logger.getLogger(AspergillusFromA2MDB.class
                                    .getName()).log(Level.SEVERE, null, ex);
                            System.out.println("NODE NOT FOUND: " + ex);
                        }
                        // INSERT ORGANISMS
                        Set<Integer> organismIds = new TreeSet();
                        if (organisms != null) {
                            String[] organismsArray = organisms.split("\\;");
                            for (int indexOrganism = 0; indexOrganism < organismsArray.length; indexOrganism++) {
                                organismName = organismsArray[indexOrganism].trim();
                                String species = PatternFinder.getGenusSpecieFromGenusSpecieAndSubspecies(organismName);
                                String subspecie = PatternFinder.getSubspecieFromGenusSpecieAndSubspecie(organismName);
                                try {
                                    Integer organism_id;
                                    if (subspecie != null) {
                                        organism_id = db.getOrganismIdFromOrganismName(subspecie);
                                    } else {
                                        organism_id = db.getOrganismIdFromOrganismName(species);
                                    }
                                    organismIds.add(organism_id);
                                    db.insertCompoundOrganism(compound_id, organism_id);
                                } catch (OrganismNotFoundException ex) {

                                    OrganismClassification organismClassification;
                                    try {
                                        organismClassification = new OrganismClassification(pc_id, kingdom, family, species, subspecie, null);
                                        Integer organism_id = db.insertOrganism(organismClassification);
                                        organismClassification.setOrganismId(organism_id);
                                        db.insertCompoundOrganism(compound_id, organism_id);
                                        organismIds.add(organism_id);

                                    } catch (Exception ex1) {
                                        Logger.getLogger(AspergillusFromA2MDB.class
                                                .getName()).log(Level.SEVERE, null, ex1);
                                    }
                                }
                            }
                        }
                        Set<Integer> referenceIds = new TreeSet();
                        Integer reference_id = null;
                        if (reference_texts != null || dois != null || reference_links != null) {
                            String[] referencesTextsArray = reference_texts.split("\\;");
                            String[] doisArray = new String[0];
                            if (dois != null) {
                                doisArray = dois.split("\\;");
                            }
                            String[] referencesLinksArray = new String[0];
                            if (reference_links != null) {
                                referencesLinksArray = reference_links.split("\\;");
                            }
                            for (int indexReference = 0; indexReference < referencesTextsArray.length; indexReference++) {
                                String reference_text = referencesTextsArray[indexReference].trim();
                                String doi = null;
                                String reference_link = null;
                                try {
                                    doi = doisArray[indexReference].trim();
                                } catch (IndexOutOfBoundsException ex) {
                                }
                                try {
                                    reference_link = referencesLinksArray[indexReference].trim();
                                } catch (IndexOutOfBoundsException ex) {
                                }
                                Reference reference = new Reference(0, reference_text, doi, reference_link);
                                reference_id = db.insertReference(reference);
                                db.insertCompoundReference(compound_id, reference_id);
                                reference.setId(reference_id);
                                for (Integer organism_id : organismIds) {
                                    db.insertOrganismReference(organism_id, reference_id);
                                }
                            }
                        }
                        if (pc_id != null) {
                            db.insertCompoundPC(compound_id, pc_id);
                        }

                    } else {
                        notFoundElements.add((compound_name));
                        //System.out.println("LINE: " + line + " from " + compound_name + " NOT FOUND in curated_aspergillus_metabolites.csv " + compound_id + " inchi: " + inchi);
                    }

                } else {
                    System.out.println("CHECK LINE: " + line + " WITH TEXT: " + curatedLineString);
                }

                line++;
                curatedLineString = br.readLine();
            }
            /*
            System.out.println(notFoundElements.size() + " compounds not found -> ");
            for (String compoundNameNotFound : notFoundElements) {
                System.out.println(compoundNameNotFound);
            }
             */

        } else {
            throw new FileNotFoundException("Cureated File of aspergillus: " + fileName + " not found");
        }
    }

    public static void insertFromPubChem(String fileName, CMMDatabase db) throws IOException, FileNotFoundException {
        File fileAspergillus = new File(fileName);
        if (fileAspergillus.exists()) {
            FileInputStream fis = new FileInputStream(fileAspergillus);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            // read header and escape it
            String curatedLineString = br.readLine();
            int line = 2;
            curatedLineString = br.readLine();
            String organismName = "";
            Set<String> notFoundElements = new TreeSet();

            String kingdom = "Fungi";
            String family = "Trichocomaceae";
            while (curatedLineString != null) {
                System.out.println("Line: " + line);
                // read each compound
                Integer compound_id = null;
                Integer pc_id = null;
                String[] data = curatedLineString.split("\\|");
                // not used
                if (data.length >= 21) {
                    Integer pubchemId = null;
                    try {
                        String pubchemLink = data[19];
                        pubchemId = PatternFinder.searchPCID(pubchemLink);
                    } catch (ArrayIndexOutOfBoundsException nfe) {
                        System.out.println("Wrong PUBCHEM LINK -> CHECK LINE: " + line + " WITH TEXT: " + curatedLineString);
                    }
                    if (pubchemId != null) {
                        Compound compound = PubchemRest.getCompoundFromPCID(pubchemId);

                        String compound_name = data[0].trim();

                        String formula = compound.getFormula();

                        String inchi;
                        String smiles;
                        String inchiKey;
                        Identifier identifiers;
                        identifiers = compound.getIdentifiers();
                        if (identifiers == null) {
                            inchi = null;
                            smiles = null;
                            inchiKey = null;
                        } else {
                            inchi = identifiers.getInchi();
                            smiles = identifiers.getSmiles();
                            inchiKey = identifiers.getInchi_key();
                        }
                        String biological_activity;
                        try {
                            biological_activity = data[21].trim();
                        } catch (IndexOutOfBoundsException ex) {
                            biological_activity = null;
                        }
                        String organisms = data[20].trim();
                        if (organisms.equals("")) {
                            organisms = null;
                        }
                        String reference_texts;
                        try {
                            reference_texts = data[12].trim();
                            if (reference_texts.equals("") || reference_texts.equals("NO INFO") || reference_texts.equals("PROTEINA")) {
                                reference_texts = null;
                            }
                        } catch (IndexOutOfBoundsException ex) {
                            reference_texts = null;
                        }
                        String dois;
                        try {
                            dois = data[13].trim();
                            if (dois.equals("")) {
                                dois = null;
                            }
                        } catch (IndexOutOfBoundsException ex) {
                            dois = null;
                        }

                        String reference_links = null;
                        try {
                            reference_links = data[14].trim();
                            if (reference_links.equals("")) {
                                reference_links = null;
                            }
                        } catch (ArrayIndexOutOfBoundsException ex) {
                            reference_links = null;
                        }

                        List<String> ancestorNodes = new LinkedList<String>();
                        if (inchi == null || inchi.equals("")) {
                            compound_id = db.getCompoundIdFromCompoundName(compound_name);
                        } else {
                            compound_id = db.getCompoundIdFromInchi(inchi);
                            // GET DATA TO INSERT COMPOUNDS
                            String casId = compound.getCasId();
                            Double exact_mass;
                            int chargeType = compound.getCharge_type();
                            int numCharges = compound.getCharge_number();
                            String formulaType = compound.getFormula_type();
                            boolean correctFormula;
                            correctFormula = !formulaType.equals("");

                            if (correctFormula) {
                                exact_mass = compound.getMass();

                                int compoundType = compound.getCompound_type();

                                try {
                                    ancestorNodes = downloadersOfWebResources.DownloaderOfWebResources.getAncestorNodesCompoundClassyfireClassification(inchi, inchiKey);
                                    if (ancestorNodes.contains(CLASSYFIRE_LIPID_NODEID)) {
                                        compoundType = 1;
                                    }
                                } catch (CompoundNotClassifiedException ex) {
                                }

                                Integer compoundStatus = compound.getCompound_status();
                                Double logP = compound.getLogP();

                                if (compound_id == 0) {
                                    compound_id = db.getCompoundIdFromCompoundName(compound_name);
                                    if (compound_id == 0) {
                                        // insert compound 
                                        compound_id = db.insertCompound(casId, compound_name, formula, exact_mass, chargeType, numCharges, formulaType, compoundType, compoundStatus, logP);
                                    }

                                }
                                // Insert identifiers regardless the compounds are already inserted
                                db.insertIdentifiers(compound_id, inchi, inchiKey, smiles);
                                db.updateLogP(compound_id, logP);
                                db.insertCompoundPC(compound_id, pubchemId);
                            }

                        }
                        if (compound_id > 0) {
                            // Insert information for compound_aspergillus
                            if (inchi != null) {
                                try {
                                    Integer compound_id_from_inchi = db.getCompoundIdFromInchi(inchi);
                                    if (compound_id_from_inchi > 0) {
                                        if (!compound_id.equals(compound_id_from_inchi)) {
                                            // remove duplicate compounds
                                            System.out.println("BREAK -> Compounds " + compound_id_from_inchi + " " + compound_id + " are duplicated");
                                        }
                                    } else {
                                        // insert identifiers
                                        inchiKey = ChemSpiderRest.getINCHIKeyFromInchi(inchi);
                                        smiles = ChemSpiderRest.getSMILESFromInchi(inchi);
                                        db.insertIdentifiers(compound_id, inchi, inchiKey, smiles);
                                    }

                                } catch (WrongRequestException ex) {
                                    Logger.getLogger(AspergillusFromA2MDB.class.getName()).log(Level.SEVERE, null, ex);
                                }

                            }
                            String meshNomenclature = null;
                            String iupac_classification = null;
                            String aspergillus_link = null;

                            // Update biological activity
                            if (!db.checkAspergillusExist(compound_id)) {
                                db.insertCompoundAspergillus(compound_id, biological_activity, meshNomenclature, iupac_classification, aspergillus_link);
                            } else {
                                db.updateCompoundAspergillus(compound_id, meshNomenclature, iupac_classification, aspergillus_link, biological_activity);
                            }

                            // Insert compound classification 
                            try {
                                // Insert classyfire
                                db.insertFullCompoundClassyfireClassification(compound_id, ancestorNodes);

                            } catch (NodeNotFoundException ex) {
                                Logger.getLogger(AspergillusFromA2MDB.class
                                        .getName()).log(Level.SEVERE, null, ex);
                                System.out.println("NODE NOT FOUND: " + ex);
                            }
                            // INSERT ORGANISMS
                            Set<Integer> organismIds = new TreeSet();
                            if (organisms != null) {
                                String[] organismsArray = organisms.split("\\;");
                                for (int indexOrganism = 0; indexOrganism < organismsArray.length; indexOrganism++) {
                                    organismName = organismsArray[indexOrganism].trim();
                                    String species = PatternFinder.getGenusSpecieFromGenusSpecieAndSubspecies(organismName);
                                    String subspecie = PatternFinder.getSubspecieFromGenusSpecieAndSubspecie(organismName);
                                    try {
                                        Integer organism_id;
                                        if (subspecie != null) {
                                            organism_id = db.getOrganismIdFromOrganismName(subspecie);
                                        } else {
                                            organism_id = db.getOrganismIdFromOrganismName(species);
                                        }
                                        organismIds.add(organism_id);
                                        db.insertCompoundOrganism(compound_id, organism_id);
                                    } catch (OrganismNotFoundException ex) {

                                        OrganismClassification organismClassification;
                                        try {
                                            organismClassification = new OrganismClassification(pc_id, kingdom, family, species, subspecie, null);
                                            Integer organism_id = db.insertOrganism(organismClassification);
                                            organismClassification.setOrganismId(organism_id);
                                            db.insertCompoundOrganism(compound_id, organism_id);
                                            organismIds.add(organism_id);

                                        } catch (Exception ex1) {
                                            Logger.getLogger(AspergillusFromA2MDB.class
                                                    .getName()).log(Level.SEVERE, null, ex1);
                                        }
                                    }
                                }
                            }
                            Set<Integer> referenceIds = new TreeSet();
                            Integer reference_id = null;
                            if (reference_texts != null || dois != null || reference_links != null) {
                                String[] referencesTextsArray = reference_texts.split("\\;");
                                String[] doisArray = new String[0];
                                if (dois != null) {
                                    doisArray = dois.split("\\;");
                                }
                                String[] referencesLinksArray = new String[0];
                                if (reference_links != null) {
                                    referencesLinksArray = reference_links.split("\\;");
                                }
                                for (int indexReference = 0; indexReference < referencesTextsArray.length; indexReference++) {
                                    String reference_text = referencesTextsArray[indexReference].trim();
                                    String doi = null;
                                    String reference_link = null;
                                    try {
                                        doi = doisArray[indexReference].trim();
                                    } catch (IndexOutOfBoundsException ex) {
                                    }
                                    try {
                                        reference_link = referencesLinksArray[indexReference].trim();
                                    } catch (IndexOutOfBoundsException ex) {
                                    }
                                    Reference reference = new Reference(0, reference_text, doi, reference_link);
                                    reference_id = db.insertReference(reference);
                                    db.insertCompoundReference(compound_id, reference_id);
                                    reference.setId(reference_id);
                                    for (Integer organism_id : organismIds) {
                                        db.insertOrganismReference(organism_id, reference_id);
                                    }
                                }
                            }
                            if (pc_id != null) {
                                db.insertCompoundPC(compound_id, pc_id);
                            }

                        } else {
                            notFoundElements.add((compound_name));
                            //System.out.println("LINE: " + line + " from " + compound_name + " NOT FOUND in curated_aspergillus_metabolites.csv " + compound_id + " inchi: " + inchi);
                        }

                    }
                } else {
                    System.out.println("CHECK LINE: " + line + " WITH TEXT: " + curatedLineString);
                }

                line++;
                curatedLineString = br.readLine();
            }
            /*
            System.out.println(notFoundElements.size() + " compounds not found -> ");
            for (String compoundNameNotFound : notFoundElements) {
                System.out.println(compoundNameNotFound);
            }
             */

        } else {
            throw new FileNotFoundException("Cureated File of aspergillus: " + fileName + " not found");
        }
    }

    public static void main(String[] args) {
        String fileName = "C:/Users/alberto.gildelafuent/OneDrive - Fundación Universitaria San Pablo CEU/research/aspergilius_mz/aspergillus_output.csv";
        CMMDatabase db = new CMMDatabase();
        db.connectToDB("jdbc:mysql://localhost/compounds?useSSL=false&serverTimezone=UTC", "alberto", "alberto");
        // Inserting compounds from database
        fileName = "C:/Users/alberto.gildelafuent/OneDrive - Fundación Universitaria San Pablo CEU/research/aspergilius_mz/writing/gold_standard/Gold standard_metabolites_MM.csv";
        try {
            insertFromPubChem(fileName, db);
        } catch (FileNotFoundException ex) {
            System.out.println("File " + fileName + " does not exist or you do not have permissions to read it");
        } catch (IOException ex) {
            System.out.println("I/O problem occurred");
        }
        /*
        fileName = "C:/Users/alberto.gildelafuent/OneDrive - Fundación Universitaria San Pablo CEU/research/aspergilius_mz/chebi_info_final_TEA.csv";
        try {
            insertFromCHEBI(fileName, db);
        } catch (FileNotFoundException ex) {
            System.out.println("File " + fileName + " does not exist or you do not have permissions to read it");
        } catch (IOException ex) {
            System.out.println("I/O problem occurred");
        }
        
        try {
            loadAspergillusCompounds(fileName);
            
            
//            insertFromAspergillusTXTFile(db);
            fileName = "C:/Users/alberto.gildelafuent/OneDrive - Fundación Universitaria San Pablo CEU/research/aspergilius_mz/curated_aspergillus_metabolites_2.csv";
            try {
                insertFromCuratedFile(fileName, db);
            } catch (FileNotFoundException ex) {
                System.out.println("File " + fileName + " does not exist or you do not have permissions to read it");
            } catch (IOException ex) {
                System.out.println("I/O problem occurred");
            }
            
            fileName = "C:/Users/alberto.gildelafuent/OneDrive - Fundación Universitaria San Pablo CEU/research/aspergilius_mz/curated_aspergillus_metabolites_1.csv";
            try {
                insertFromCuratedFile(fileName, db);
            } catch (FileNotFoundException ex) {
                System.out.println("File " + fileName + " does not exist or you do not have permissions to read it");
            } catch (IOException ex) {
                System.out.println("I/O problem occurred");
            }

            fileName = "C:/Users/alberto.gildelafuent/OneDrive - Fundación Universitaria San Pablo CEU/research/aspergilius_mz/a2mdb/A2MDB_TEA_with_references.csv";
            try {
                classifyCompoundsfromA2MDB(fileName, db);
            } catch (FileNotFoundException ex) {
                System.out.println("File " + fileName + " does not exist or you do not have permissions to read it");

            } catch (IOException ex) {

                Logger.getLogger(AspergillusFromA2MDB.class
                        .getName()).log(Level.SEVERE, null, ex);
                System.out.println("I/O problem occurred");
            }
             
        } catch (FileNotFoundException ex) {

            Logger.getLogger(AspergillusFromA2MDB.class
                    .getName()).log(Level.SEVERE, null, ex);
            System.out.println("File " + fileName + " does not exist or you do not have permissions to read it");

        } catch (IOException ex) {

            Logger.getLogger(AspergillusFromA2MDB.class
                    .getName()).log(Level.SEVERE, null, ex);
            System.out.println("I/O problem occurred");
        } catch (AspergillusNotFoundException ex) {
            System.out.println(ex);
        }
         */

    }
}
