/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aspergillus;

import databases.CMMDatabase;
import databases.ChebiDatabase;
import databases.PubchemRest;
import static databases.PubchemRest.getPCIDFromInchiKey;
import exceptions.ChebiException;
import exceptions.CompoundNotClassifiedException;
import exceptions.NodeNotFoundException;
import ioDevices.MyFile;
import ioDevices.UriDevice;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.AspergillusCompound;
import model.ClassyfireClassification;
import model.Identifier;
import model.OrganismClassification;
import model.Reference;
import patternFinders.PatternFinder;
import static utilities.Constants.CLASSYFIRE_LIPID_NODEID;
import static utilities.Constants.KNAPSACK_ASPERGILLUS_PATH;
import static utilities.Constants.KNAPSACK_ELEMENT_PATH;
import static utilities.Constants.KNAPSACK_ELEMENT_REFERENCE_PATH;
import static utilities.Constants.KNAPSACK_RESOURCES_PATH;

/**
 *
 * @author alberto.gildelafuent
 */
public class AspergillusFromKnapsack {

    /**
     * Download all the files from KnapSack related to Aspergillus
     *
     * @param db
     */
    public static void getHTMLsFromKnapSack(CMMDatabase db) {

        String urlListMetabolitesAspergillusFileName = KNAPSACK_ASPERGILLUS_PATH;

        // For statistics
        /*
        String fileNameInChINotFound = "C:\\Users\\alberto.gildelafuent\\OneDrive - Fundación Universitaria San Pablo CEU\\research\\aspergilius_mz\\knapsack\\knapsack_no_inchi.csv";
        File fileInChINotFound = new File(fileNameInChINotFound);
        PrintWriter printwriterInChINotFoundFile = null;
        if (fileInChINotFound.exists()) {
            fileInChINotFound.delete();
        }
        try {
            fileInChINotFound.createNewFile();
            printwriterInChINotFoundFile = new PrintWriter(fileInChINotFound);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());

        }
        printwriterInChINotFoundFile.println("Name" + "|" + "Formula" + "|" + "Mass" + "|" + "casId" + "|" + "inchi" + "|" + "inchiKey" + "|" + "smiles" + "|" + "chebId" + "|" + "pubChemId" + "|" + "KnapSackLink");
         */
        String content;
        // TO DO CREATE COMPOUNDS FROM KNAPSACK AND ASPERGILLUS>
        //Map<String, AspergillusCompound> AspergillusCompoundsFromManualCSV = loadCompoundsFromCSV(db);
        Map<String, AspergillusCompound> AspergillusCompoundsFromManualCSV = new HashMap<String, AspergillusCompound>();

        try {
            content = UriDevice.readString(urlListMetabolitesAspergillusFileName);
            if (content.length() <= 10) {
                System.out.println("Error downloading list of Aspergillus compounds from KnapSack. Check " + KNAPSACK_ASPERGILLUS_PATH);
            } else {

                String idPattern = "(C[0-9]{8})";
                //Get information from html
                Set<String> knapSackIds;
                knapSackIds = new TreeSet<>();
                knapSackIds = PatternFinder.searchSetWithoutReplacement(content, idPattern);

                // Variables to get statistics
                Integer inchi_count = 0;
                Integer inchikey_count = 0;
                Integer cas_id_count = 0;
                Integer smiles_count = 0;
                Integer chebi_count = 0;
                Integer pubchem_count = 0;
                Integer file_total = 31;
                Integer file_count = 0;

                for (String knapSackId : knapSackIds) {
                    String knapSackFileName = KNAPSACK_RESOURCES_PATH + knapSackId + ".html";
                    File knapSackIdFile = new File(knapSackFileName);
                    if (!knapSackIdFile.exists()) {
                        downloadKnapSackFile(knapSackId);
                    }
                    String contentOfKnapSackId = MyFile.obtainContentOfABigFile(knapSackFileName).toString();
                    //Get information from html
                    String compound_name = "";
                    String formula = "";
                    Double mass = 0d;
                    String biologicalActivity = null;
                    try {
                        compound_name = getNameFromKnapSackFile(contentOfKnapSackId);
                        mass = getMassFromKnapSackFile(contentOfKnapSackId);
                        formula = getFormulaFromKnapSackFile(contentOfKnapSackId);
                    } catch (NoSuchFieldException nsfe) {
                        System.out.println("ERROR -> check knapSackId: " + knapSackId + " " + nsfe.getMessage());
                    }
                    String casId = "";
                    try {
                        casId = getCASFromKnapSackFile(contentOfKnapSackId);
                        cas_id_count++;
                    } catch (NoSuchFieldException ex) {
                        // System.out.println("CAS NOT FOUND: " + knapSackId);
                    }
                    String inchi = "";
                    String inchiKey = "";
                    String smiles = "";
                    Set<OrganismClassification> setOrganisms;
                    setOrganisms = new HashSet<>();
                    try {
                        inchi = getInChIFromKnapsackFile(contentOfKnapSackId);
                        inchi_count++;
                        smiles = getSMILESFromKnapsackFile(contentOfKnapSackId);
                        try {
                            inchiKey = getInChIKeyFromKnapsackFile(contentOfKnapSackId);
                        } catch (NoSuchFieldException ex) {
                            // System.out.println("INCHIKEY NOT FOUND: " + knapSackId);
                        }

                    } catch (NoSuchFieldException ex) {
                        // Try to find it in the list of compounds
                        AspergillusCompound aspergillusCompound = AspergillusCompoundsFromManualCSV.get(knapSackId);
                        if (aspergillusCompound != null) {
                            inchi = aspergillusCompound.getIdentifiers().getInchi();
                            smiles = aspergillusCompound.getIdentifiers().getSmiles();
                            inchiKey = aspergillusCompound.getIdentifiers().getInchi_key();
                            biologicalActivity = aspergillusCompound.getBiologicalActivity();
                            file_count++;
                            inchi_count++;
                            //setOrganisms = aspergillusCompound.getOrganisms();
                            AspergillusCompoundsFromManualCSV.remove(knapSackId);
                        } else {
                            System.out.println("CRITICAL ERROR -> Compound: " + knapSackId + " NOT FOUND");
                        }
                    }

                    Identifier partialIdentifiers = new Identifier(inchi, inchiKey, smiles);
                    Integer chebId = 0;
                    try {
                        chebId = ChebiDatabase.getChebiFromIdentifiers(partialIdentifiers);
                        chebi_count++;
                    } catch (ChebiException ex) {
                        // System.out.println(knapSackId + " -> CHEBI NOT FOUND");
                    } catch (Exception ex) {
                        // System.out.println(knapSackId + " -> ERROR OBTAINING CHEBI: " + ex.getMessage());
                    }
                    Integer pubChemId = 0;
                    try {
                        Identifier IdentifiersFromPubchem = PubchemRest.getIdentifiersFromInChIPC(inchi);
                        if (inchiKey.equals("")) {
                            inchiKey = IdentifiersFromPubchem.getInchi_key();
                        }
                        if (smiles.equals("")) {
                            smiles = IdentifiersFromPubchem.getSmiles();
                        }
                        pubChemId = getPCIDFromInchiKey(IdentifiersFromPubchem.getInchi_key());

                        pubchem_count++;
                    } catch (IOException ex) {
                        // System.out.println(knapSackId + " -> ERROR OBTAINING PUBCHEM: " + ex);
                    } catch (NullPointerException ex) {
                        // System.out.println(knapSackId + " -> Pubchem NOT FOUND");
                    }

                    Identifier identifiers = new Identifier(inchi, inchiKey, smiles);

                    if (!inchiKey.equals("")) {

                        inchikey_count++;
                    }
                    if (!smiles.equals("")) {
                        smiles_count++;
                    }
                    if (inchi.equals("")) {
                        String nameOneLine = compound_name.replaceAll("\n", ", ");

                        /*
                        printwriterInChINotFoundFile.println(nameOneLine + "|" + formula + "|" + mass.toString() + "|" + casId + "|"
                                + inchi + "|" + inchiKey + "|" + smiles + "|" + chebId + "|" + pubChemId + "|" + KNAPSACK_RESOURCES_PATH + knapSackId + ".html");
                         */
                    }

                    try {
                        Set<OrganismClassification> setOrganismsFromKnapsack = getOrganisms(knapSackId, contentOfKnapSackId);
                        setOrganisms.addAll(setOrganismsFromKnapsack);
                        int compoundType = 0;
                        List<String> ancestorNodes = new LinkedList<String>();
                        try {
                            ancestorNodes = downloadersOfWebResources.DownloaderOfWebResources.getAncestorNodesCompoundClassyfireClassification(inchi, inchiKey);
                            if (ancestorNodes.contains(CLASSYFIRE_LIPID_NODEID)) {
                                compoundType = 1;
                            }
                        } catch (CompoundNotClassifiedException ex) {
                        }

                        // TODO INSERT 1-Compound 2- Organism; 3- Reference; 4-Relation Compound-organism-reference 5- CLASSYFIRE 6- Insert chebi, pubchem y aspergillus
                        Integer compoundId = db.getCompoundIdFromInchiKey(inchiKey);
                        if (compoundId == 0) {
                            // INCHI FROm KNAPSACK CAN BE MISSCALCULATED
                            compoundId = db.getCompoundIdFromInchi(inchi);
                            if (compoundId == 0) {
                                // Check if the compound does not have identifiers to add them
                                compoundId = db.getCompounIdFromCAS(casId);
                                if (compoundId == 0) {
                                    compoundId = db.getCompoundIdFromCompoundName(compound_name);
                                    if (compoundId == 0) {
                                        // insert compound
                                        int[] charges = PatternFinder.getChargeFromSmiles(smiles);
                                        int chargeType = charges[0];
                                        int numCharges = charges[1];
                                        String formulaType = PatternFinder.getTypeFromFormula(formula);

                                        Integer compoundStatus = 1;
                                        Double logP = null;
                                        AspergillusCompound aspergillusCompound = new AspergillusCompound(knapSackId, null, pubChemId, chebId, biologicalActivity, compoundId,
                                                compound_name, casId, formula, mass, compoundStatus, compoundType, logP, identifiers, null, null, setOrganisms);
                                        compoundId = db.insertCompound(casId, compound_name, formula, mass, chargeType, numCharges, formulaType, compoundType, compoundStatus, logP);
                                    }
                                }
                                db.insertIdentifiers(compoundId, inchi, inchiKey, smiles);
                            }
                        }

                        // insert knapsackId
                        db.insertCompoundKnapsack(compoundId, knapSackId);

                        // Insert organisms and references:
                        for (OrganismClassification organismClassification : setOrganisms) {
                            Integer organismId = db.insertOrganism(organismClassification);
                            Set<Reference> referencesOrganismPartial = organismClassification.getReferences();
                            db.insertCompoundOrganism(compoundId, organismId);
                            for (Reference reference : referencesOrganismPartial) {
                                Integer referenceId = db.insertReference(reference);
                                db.insertCompoundReference(compoundId, referenceId);
                                db.insertOrganismReference(organismId, referenceId);
                            }
                        }

                        try {
                            // Insert classyfire
                            db.insertFullCompoundClassyfireClassification(compoundId, ancestorNodes);
                        } catch (NodeNotFoundException ex) {
                            Logger.getLogger(AspergillusFromKnapsack.class.getName()).log(Level.SEVERE, null, ex);
                            System.out.println("NODE NOT FOUND: " + ex);
                        }

                        if (chebId != 0) {
                            db.insertCompoundCHEBI(compoundId, chebId);
                        }
                        if (pubChemId != 0) {
                            db.insertCompoundPC(compoundId, pubChemId);
                        }
                        db.insertCompoundAspergillus(compoundId, biologicalActivity, null, null, null);
                    } catch (NoSuchFieldException ex) {
                        Logger.getLogger(AspergillusFromKnapsack.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    /*
                    System.out.println(knapSackId + ": " + compound_name + " | " + formula + " | " + mass.toString()
                            + " | " + casId + " | " + inchiKey + " | " + smiles + " | " + chebId + " | " + pubChemId);
                     */
                }
                System.out.println("Compounds found: 584");
                System.out.println("CAS found: " + cas_id_count);
                System.out.println("INCHI KEY found: " + inchikey_count);
                System.out.println("INCHI found: " + inchi_count);
                System.out.println("SMILES found: " + smiles_count);
                System.out.println("CHEBI found: " + chebi_count);
                System.out.println("PUBCHEM found: " + pubchem_count);
                System.out.println("FILE found: " + file_count);
            }
            //printwriterInChINotFoundFile.close();
        } catch (FileNotFoundException | UnknownHostException ex) {
            System.out.println("Error downloading list of Aspergillus compounds from KnapSack. Check " + KNAPSACK_ASPERGILLUS_PATH);
        }

    }

    /*
public void populateFromAMetlinWebPage(String fileName, CMMDatabase db) {
        String metlin_id = fileName.replaceAll(".html", "");
        String content = MyFile.read(METLINPATH + metlin_id + ".html").toString();

        String queryFailed = PatternFinder.searchWithoutReplacement(content, "failed");
        if (!queryFailed.isEmpty()) {
            return;
        }

        String exactMass = PatternFinder.searchWithoutReplacement(content, "Mass<\\/th>\\s*<td>[0-9]+(\\.[0-9]+)?");
        exactMass = PatternFinder.searchWithReplacement(exactMass, "[0-9]+(\\.[0-9]+)?", "");
        if (exactMass.equalsIgnoreCase("")) {
            exactMass = "null";
        } else {
            exactMass = PatternFinder.searchWithoutReplacement(exactMass, "[0-9]+(\\.[0-9]+)?");
        }

        String name = PatternFinder.searchWithoutReplacement(content, "<b>Name</b>(.*?)<font color=blue><b>(.*?)</b></font>");
        name = PatternFinder.searchWithReplacement(name, "<font color=blue><b>(.*?)</b></font>",
                "<font color=blue><b>|</b></font>");
        if (name.equalsIgnoreCase("")) {
            return;
        }
        name = name.replaceAll("\"", "''");
        name = "\"" + name.substring(0, name.length() - 1) + "\"";

        String formula = PatternFinder.searchWithoutReplacement(content, "<b>Formula</b></font></td>(.*?)</script>(.*?)</td>");
        formula = PatternFinder.searchWithReplacement(formula, "</script>(.*?)</td>", "</script>|</td>|<sub>|</sub>");
        if (formula.equalsIgnoreCase("")) {
            formula = "null";
        } else {
            formula = "\"" + formula.substring(0, formula.length() - 1) + "\"";
        }

        String cas = PatternFinder.searchWithoutReplacement(content, "<b>CAS</b></font></td>(.*?)</script>(.*?)</td>");
        cas = PatternFinder.searchWithReplacement(cas, "[0-9]+-[0-9]+-[0-9]+", "");
        if (cas.equalsIgnoreCase("")) {
            cas = "null";
        } else {
            cas = "\"" + cas.substring(0, cas.length() - 1) + "\"";
        }

        String insertion1 = "INSERT INTO metlin_compounds(metlin_id, compoundName, mass, formula) VALUES("
                + metlin_id + ", " + name + ", " + exactMass + ", " + formula + ") ON DUPLICATE KEY UPDATE metlin_id=" + metlin_id;

        System.out.println(insertion1);
        //db.executeIDU(insertion1);

        if (cas.equalsIgnoreCase("null")) {
            return;
        }

        String insertion2 = "INSERT INTO metlin_compounds_cas_ids(metlin_id, casIdentifier) VALUES("
                + metlin_id + ", " + cas + ") ON DUPLICATE KEY UPDATE metlin_id=" + metlin_id + ", casIdentifier=" + cas;

        System.out.println(insertion2);
        //db.executeIDU(insertion2);
    }
     */
    public static void downloadKnapSackFile(String knapSackId) {
        String fileName = KNAPSACK_RESOURCES_PATH + knapSackId + ".html";
        File knapSackLocalFile = new File(fileName);
        if (knapSackLocalFile.exists()) {
            knapSackLocalFile.delete();
        }
        String webName = KNAPSACK_ELEMENT_PATH + knapSackId;
        try {
            String content = UriDevice.readString(webName);
            MyFile.write(content, fileName);

        } catch (FileNotFoundException | UnknownHostException ex) {
            System.out.println("Error downloading Aspergillus from KnapSack. Check " + webName);
        }
        try {
            int randomInt;
            randomInt = new Random().nextInt((10000 - 1000) + 1) + 1000;
            Thread.sleep(randomInt);                 //1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            System.out.println("Thread interrupted");
            Thread.currentThread().interrupt();
        }
    }

    public static void downloaKnapSackFileWithReference(String knapSackId, Integer numReference) {

        String fileName = KNAPSACK_RESOURCES_PATH + knapSackId + "_" + numReference.toString() + ".html";
        File knapSackLocalFile = new File(fileName);
        if (knapSackLocalFile.exists()) {
            knapSackLocalFile.delete();
        }
        String webName = KNAPSACK_ELEMENT_REFERENCE_PATH + knapSackId + "&key=" + numReference.toString();
        try {
            String content = UriDevice.readString(webName);
            MyFile.write(content, fileName);

        } catch (FileNotFoundException | UnknownHostException ex) {
            System.out.println("Error downloading list of Aspergillus compounds from KnapSack. Check " + webName);
        }
        try {
            int randomInt;
            randomInt = new Random().nextInt((10000 - 1000) + 1) + 1000;
            Thread.sleep(randomInt);                 //1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            System.out.println("Thread interrupted");
            Thread.currentThread().interrupt();
        }
    }

    /**
     *
     * @param contentKnapSackFile
     * @return the name of knapssack compound
     * @throws java.lang.NoSuchFieldException
     */
    public static String getNameFromKnapSackFile(String contentKnapSackFile) throws NoSuchFieldException {

        //Get information from html
        String idPattern = "[^\\n\\r]*";
        String preName = "<th class=\"inf\">Name<\\/th>\\n"
                + "(\\s*)<td colspan=\"4\" class=\"inf\">";
        String nameEnd = "<\\/td>";
        String name = PatternFinder.searchWithReplacement(contentKnapSackFile, preName + idPattern + nameEnd, preName + "|" + nameEnd + "|\n");
        // Remove the last \n
        name = name.replaceAll("\n", "");
        name = name.replaceAll("<br>", "\n");
        if (name.equals("")) {
            throw new NoSuchFieldException("Name not found in Knapsack ID");
        }
        return name;
    }

    /**
     *
     * @param contentKnapSackFile
     * @return the formula of knapssack compound
     * @throws java.lang.NoSuchFieldException
     */
    public static String getFormulaFromKnapSackFile(String contentKnapSackFile) throws NoSuchFieldException {

        //Get information from html
        String idPattern = "[A-Za-z0-9]*";
        String preFormula = "<th class=\"inf\">Formula<\\/th>\\n"
                + "(\\s*)<td colspan=\"4\">";
        String formulaEnd = "<\\/td>";
        String formula = PatternFinder.searchWithReplacement(contentKnapSackFile, preFormula + idPattern + formulaEnd, preFormula + "|" + formulaEnd + "|\n");
        formula = formula.replaceAll("\\s+", "");
        // Remove the last \n
        formula = formula.replaceAll("\n", "");
        if (formula.equals("")) {
            throw new NoSuchFieldException("Formula not found in Knapsack ID");
        }
        return formula;
    }

    /**
     *
     * @param contentKnapSackFile
     * @return the mass of knapssack compound
     * @throws java.lang.NoSuchFieldException
     */
    public static Double getMassFromKnapSackFile(String contentKnapSackFile) throws NoSuchFieldException {

        //Get information from html
        String idPattern = "[0-9\\.]*";
        String preMass = "<th class=\"inf\">Mw<\\/th>\\n"
                + "(\\s*)<td colspan=\"4\">";
        String massEnd = "<\\/td>";
        String massString = PatternFinder.searchWithReplacement(contentKnapSackFile, preMass + idPattern + massEnd, preMass + "|" + massEnd + "|\n");
        massString = massString.replaceAll("\\s+", "");
        // Remove the last \n
        massString = massString.replaceAll("\n", "");
        try {
            Double mass = Double.parseDouble(massString);
            return mass;
        } catch (NumberFormatException e) {
            throw new NoSuchFieldException("Mass not found in Knapsack ID");
        }
    }

    /**
     *
     * @param contentKnapSackFile
     * @return the CAS of knapssack compound
     * @throws java.lang.NoSuchFieldException
     */
    public static String getCASFromKnapSackFile(String contentKnapSackFile) throws NoSuchFieldException {

        //Get information from html
        String idPattern = "[0-9]+[-][0-9]+[-][0-9]";
        String preCAS = "<th class=\"inf\">CAS RN<\\/th>\\n"
                + "(\\s*)<td colspan=\"4\">";
        String casEnd = "<\\/td>";
        String casId = PatternFinder.searchWithReplacement(contentKnapSackFile, preCAS + idPattern + casEnd, preCAS + "|" + casEnd + "|\n");
        casId = casId.replaceAll("\\s+", "");
        // Remove the last \n
        casId = casId.replaceAll("\n", "");

        if (casId.equals("")) {
            throw new NoSuchFieldException("CAS not found in Knapsack ID");
        }

        return casId;
    }

    /**
     *
     * @param contentKnapSackFile
     * @return the InCHIKEy of knapssack compound
     * @throws java.lang.NoSuchFieldException
     */
    public static String getInChIKeyFromKnapsackFile(String contentKnapSackFile) throws NoSuchFieldException {

        //Get information from html
        String idPattern = "[A-Z]+[-][A-Z]+[-][A-Z]";
        String preInchiKey = "<th class=\"inf\">InChIKey<\\/th>\\n"
                + "(\\s*)<td colspan=\"4\">";
        String inchikeyEnd = "<\\/td>";
        String inchiKey = PatternFinder.searchWithReplacement(contentKnapSackFile, preInchiKey + idPattern + inchikeyEnd, preInchiKey + "|" + inchikeyEnd + "|\n");
        inchiKey = inchiKey.replaceAll("\\s+", "");
        // Remove the last \n
        inchiKey = inchiKey.replaceAll("\n", "");
        if (inchiKey.equals("")) {
            throw new NoSuchFieldException("INCHIKEY not found in Knapsack ID");
        }
        return inchiKey;
    }

    /**
     *
     * @param contentKnapSackFile
     * @return the InChI of knapssack compound
     * @throws java.lang.NoSuchFieldException
     */
    public static String getInChIFromKnapsackFile(String contentKnapSackFile) throws NoSuchFieldException {

        //Get information from html
        String idPattern = "[^\\n\\r]*";
        String preInchi = "<th class=\"inf\">InChICode<\\/th>\\n"
                + "(\\s*)<td colspan=\"4\">";
        String inchiEnd = "<\\/td>";
        String inchi = PatternFinder.searchWithReplacement(contentKnapSackFile, preInchi + idPattern + inchiEnd, preInchi + "|" + inchiEnd + "|\n");
        inchi = inchi.replaceAll("\\s+", "");
        // Remove the last \n
        inchi = inchi.replaceAll("\n", "");
        if (inchi.equals("")) {
            throw new NoSuchFieldException("INCHI not found in Knapsack ID");
        }
        return inchi;
    }

    /**
     *
     * @param contentKnapSackFile
     * @return the SMILES of knapssack compound
     * @throws java.lang.NoSuchFieldException
     */
    public static String getSMILESFromKnapsackFile(String contentKnapSackFile) throws NoSuchFieldException {

        //Get information from html
        String idPattern = "[^\\n\\r]*";
        String preSMILES = "<th class=\"inf\">SMILES<\\/th>\\n"
                + "(\\s*)<td colspan=\"4\">";
        String SMILESEnd = "<\\/td>";
        String SMILES = PatternFinder.searchWithReplacement(contentKnapSackFile, preSMILES + idPattern + SMILESEnd, preSMILES + "|" + SMILESEnd + "|\n");
        SMILES = SMILES.replaceAll("\\s+", "");
        // Remove the last \n
        SMILES = SMILES.replaceAll("\n", "");
        if (SMILES.equals("")) {
            throw new NoSuchFieldException("SMILES not found in Knapsack ID");
        }
        return SMILES;
    }

    /**
     * TODO
     *
     * @param knapSackId
     * @param contentKnapSackFile
     * @return the organisms of knapssack compound
     * @throws java.lang.NoSuchFieldException
     */
    public static Set<OrganismClassification> getOrganisms(String knapSackId, String contentKnapSackFile) throws NoSuchFieldException {

        Set<OrganismClassification> setOrganisms = new TreeSet<>();
        String idPattern = "(.)*";

        String preOrganisms = "<th class=\"or3\">Kingdom<\\/th>\\n"
                + "(\\s*)<th class=\"or2\">Family<\\/th>"
                + "(\\s*)<th class=\"or1\">Species<\\/th>"
                + "(\\s*)<th class=\"or4\">Reference<\\/th><tr>";
        String organismsEnd = "<\\/table>(.|\\n)*<\\/table>";

        String htmlOrganisms = PatternFinder.searchWithReplacement(contentKnapSackFile, preOrganisms + idPattern + organismsEnd, preOrganisms + "|" + organismsEnd);

        String organismHTMLPattern = "<td class=org>[^\\n\\r]*";
        List<String> organisms = PatternFinder.searchListWithReplacement(htmlOrganisms, organismHTMLPattern, "");
        for (String organismHTML : organisms) {
            try {
                OrganismClassification organismClassification = getOrganismFromHTML(knapSackId, organismHTML);
                setOrganisms.add(organismClassification);
            } catch (NoSuchFieldException nsfe) {

                Logger.getLogger(AspergillusFromKnapsack.class.getName()).log(Level.SEVERE, null, nsfe + " \n " + knapSackId);
            }

        }
        if (setOrganisms.isEmpty()) {
            throw new NoSuchFieldException("ORGANISMS not found in Knapsack ID");
        }
        return setOrganisms;
    }

    private static String getKingdomFromHTML(String organismHTML) throws NoSuchFieldException {

        String idPattern = "[^\\n\\r]*";

        String preOrganismLink = "<td class=org>";
        String organismLinkEnd = "<\\/td><td class=org>[^\\n\\r]*<td class=org2>";

        String kingdom = PatternFinder.searchWithReplacement(organismHTML, preOrganismLink + idPattern + organismLinkEnd, preOrganismLink + "|" + organismLinkEnd + "|\n");
        // Remove the last \n
        kingdom = kingdom.replaceAll("\n", "");
        if (kingdom.equals("")) {
            throw new NoSuchFieldException("ORGANISM Wrong in " + organismHTML);
        }
        return kingdom;
    }

    private static String getFamilyFromHTML(String organismHTML) throws NoSuchFieldException {

        String idPattern = "[^\\n\\r]*";

        String preOrganismLink = "<td class=org>[^\\n\\r]*<\\/td><td class=org>";
        String organismLinkEnd = "<\\/td><td class=org2>";

        String family = PatternFinder.searchWithReplacement(organismHTML, preOrganismLink + idPattern + organismLinkEnd, preOrganismLink + "|" + organismLinkEnd + "|\n");
        // Remove the last \n
        family = family.replaceAll("\n", "");
        if (family.equals("")) {
            throw new NoSuchFieldException("ORGANISM Wrong in " + organismHTML);
        }
        return family;
    }

    private static String getGenusSpecieAndSubspecieFromHTML(String organismHTML) throws NoSuchFieldException {

        String idPattern = "[^\\n\\r<]*";

        String preOrganismLink = "<td class=org>[^\\n\\r]*<td class=org2>";
        String organismLinkEnd = "<";

        String GenusAndspecie = PatternFinder.searchWithReplacement(organismHTML, preOrganismLink + idPattern + organismLinkEnd, preOrganismLink + "|" + organismLinkEnd + "|\n");
        // Remove the last \n
        GenusAndspecie = GenusAndspecie.replaceAll("\n", "");
        if (GenusAndspecie.equals("")) {
            throw new NoSuchFieldException("ORGANISM Wrong in " + organismHTML);
        }
        return GenusAndspecie;
    }

    private static Integer getReferenceNumberFromHTML(String knapSackId, String organismHTML) throws NoSuchFieldException {

        String idPattern = "[0-9]*";

        String preOrganismLink = "<td class=org>[^\\n\\r]*<\\/td><td class=org><a href=information.php\\?mode=r&word=" + knapSackId
                + "&key=";
        String organismLinkEnd = "\\stitle=";

        String numReference = PatternFinder.searchWithReplacement(organismHTML, preOrganismLink + idPattern + organismLinkEnd, preOrganismLink + "|" + organismLinkEnd + "|\n");
        numReference = numReference.replaceAll("\\s+", "");
        // Remove the last \n
        numReference = numReference.replaceAll("\n", "");
        if (numReference.equals("")) {
            throw new NoSuchFieldException("No reference in " + organismHTML);
        }
        try {
            Integer numReferenceInt = Integer.parseInt(numReference);

            return numReferenceInt;
        } catch (NumberFormatException nfe) {
            throw new NoSuchFieldException("Reference Wrong. No number obtained in " + knapSackId + " -> " + organismHTML);
        }

    }

    /**
     *
     * @param knapSackId
     * @param organismHTML
     * @return
     * @throws NoSuchFieldException
     */
    public static OrganismClassification getOrganismFromHTML(String knapSackId, String organismHTML) throws NoSuchFieldException {
        OrganismClassification organismClassification = null;

        String kingdom = getKingdomFromHTML(organismHTML);
        String family = getFamilyFromHTML(organismHTML);
        String speciesAndCells = getGenusSpecieAndSubspecieFromHTML(organismHTML);
        String genusAndSpecie = PatternFinder.getGenusSpecieFromGenusSpecieAndSubspecies(speciesAndCells);
        String subspecie = PatternFinder.getSubspecieFromGenusSpecieAndSubspecie(speciesAndCells);
        Integer numReference = getReferenceNumberFromHTML(knapSackId, organismHTML);
        // 
        Set<Reference> setReferences = getReferencesFromKnapsackIdAndRefNumber(knapSackId, numReference);

        // TODO get node_id from db
        Integer node_id = 1;

        try {
            organismClassification = new OrganismClassification(node_id, kingdom, family, genusAndSpecie, subspecie, setReferences);
        } catch (Exception ex) {
            Logger.getLogger(AspergillusFromKnapsack.class.getName()).log(Level.SEVERE, null, ex);
            throw new NoSuchFieldException("Wrong organism");
        }
        return organismClassification;
    }

    /**
     * get the reference texts from the knapsackId and the reference number
     *
     * @param knapSackId
     * @param numReference
     * @return
     * @throws NoSuchFieldException
     */
    public static Set<Reference> getReferencesFromKnapsackIdAndRefNumber(String knapSackId, Integer numReference) throws NoSuchFieldException {
        Set<Reference> setReferences = new TreeSet<Reference>();
        String fileName = KNAPSACK_RESOURCES_PATH + knapSackId + "_" + numReference.toString() + ".html";
        File knapSackIdFile = new File(fileName);
        if (!knapSackIdFile.exists()) {
            downloaKnapSackFileWithReference(knapSackId, numReference);
        }
        String contentOfKnapSackIdWithReference = MyFile.obtainContentOfABigFile(fileName).toString();

        // Open the file and get the content to extract the reference text
        String idPattern = "[^\\n\\r]*";

        String preReferenceText = "<table class=r1> <tr><th class=r1>Organism<\\/th><td class=r1>[^\\n\\r]*<\\/td><\\/tr> <tr><th class=r1>Reference<\\/th><td class=r1>";
        String referenceTextEnd = "<\\/td><\\/tr><\\/table>";

        String allReferenceText = PatternFinder.searchWithReplacement(contentOfKnapSackIdWithReference, preReferenceText + idPattern + referenceTextEnd, preReferenceText + "|" + referenceTextEnd + "|\n");
        String[] references = allReferenceText.split("<br><br>");

        // TODO get reference_id from db
        Integer reference_id = 1;
        for (int index = 0; index < references.length; index++) {
            String referenceText = references[index];
            // Remove the last \n
            referenceText = referenceText.replaceAll("\n", "");

            // TODO get reference_id from db
            Reference reference = new Reference(reference_id, referenceText);
            setReferences.add(reference);
            if (allReferenceText.equals("")) {
                throw new NoSuchFieldException("Reference Wrong. No number obtained in " + knapSackId + " -> " + numReference);
            }

            reference_id++;

        }

        return setReferences;
    }

    /**
     * Load the Aspergillus compounds from the csv knapsack_no_inchi_output.csv
     *
     * @param db
     * @return a map with the knapsack ID as a key and the corresponding
     * AspergillusCompound
     */
    private static Map<String, AspergillusCompound> loadCompoundsFromCSV(CMMDatabase db) {
        Map<String, AspergillusCompound> mapCompoundsFromCSV = new HashMap();
        String CEFilePath = "resources/knapsack/knapsack_no_inchi_output.csv";
        BufferedReader br = null;
        String line;
        String csvSplitBy = "\\|";

        try {
            br = new BufferedReader(new FileReader(CEFilePath));
            int lineNumber = 2;
            br.readLine();
            while ((line = br.readLine()) != null) {
                // Create the variables for the CE compound
                String compound_name;
                String kingdomsFull;
                String biological_activity;
                String formula;
                String DOIsFullString;
                String referencesFullString;
                String linksFullString;
                Double mass;
                String casId;
                String inchi;
                String inchiKey;
                String smiles;
                String knapSackId;

                // Variables obtained from the database
                int compound_id;

                if (line.endsWith(csvSplitBy)) {
                    line = line + " |";
                }
                String[] compound = line.split(csvSplitBy);
                compound_name = compound[0];
                kingdomsFull = compound[1];
                String[] kingdoms = kingdomsFull.split("&");

                String familiesFull = compound[2];
                String[] families = familiesFull.split("&");

                String speciesFull = compound[3];
                String[] species = speciesFull.split("&");

                String cellsFull = compound[4];
                String[] cells = cellsFull.split("&");

                biological_activity = compound[6];
                DOIsFullString = compound[8];
                referencesFullString = compound[9];
                linksFullString = compound[10];

                formula = compound[11];
                try {
                    mass = Double.parseDouble(compound[12]);
                } catch (NumberFormatException e) {
                    // if there is no chebid
                    mass = null;
                }
                casId = compound[13];
                inchi = compound[14];
                inchiKey = compound[15];
                smiles = compound[16];
                knapSackId = compound[19];

                Identifier identifiers = new Identifier(inchi, inchiKey, smiles);
                compound_id = db.getCompoundIdFromInchiKey(inchiKey.trim());
                Integer chebId = 0;
                try {
                    chebId = ChebiDatabase.getChebiFromIdentifiers(identifiers);
                } catch (ChebiException ex) {
                    // System.out.println(knapSackId + " -> CHEBI NOT FOUND");
                    // System.out.println(identifiers);
                } catch (Exception ex) {
                    System.out.println("error obtaining chebi id: " + identifiers);
                }
                Integer pubChemId = 0;
                try {
                    Identifier IdentifiersFromPubchem = PubchemRest.getIdentifiersFromInChIPC(inchi);
                    if (inchiKey.equals("")) {
                        inchiKey = IdentifiersFromPubchem.getInchi_key();
                    }
                    if (smiles.equals("")) {
                        smiles = IdentifiersFromPubchem.getSmiles();
                    }

                    pubChemId = getPCIDFromInchiKey(IdentifiersFromPubchem.getInchi_key());
                } catch (IOException ex) {
                    System.out.println(knapSackId + " -> PUBCHEM NOT FOUND: " + ex);
                    System.out.println(identifiers);
                } catch (NullPointerException ex) {
                    // System.out.println(knapSackId + " -> Pubchem NOT FOUND");
                }
                Integer compoundStatus = 1;
                Integer compoundType = 0;

                Double logP = null;
                ClassyfireClassification classyfireClassifcation = null;
                Set<Reference> references = new TreeSet<Reference>();
                // db.getReference
                String[] DOIs = DOIsFullString.split("&");
                String[] referenceTexts = referencesFullString.split("&");
                String[] linkTexts = linksFullString.split("&");
                for (int index = 0; index < DOIs.length; index++) {
                    String DOI = DOIs[index];
                    try {
                        String referenceText = referenceTexts[index];
                        String linkText = linkTexts[index];
                        if (!DOI.equals("")) {
                            Reference reference = new Reference(1, referenceText, DOI, linkText);
                            references.add(reference);
                        }
                    } catch (IndexOutOfBoundsException ex) {
                        System.out.println("ERROR IN LINE: " + lineNumber);
                    }
                }

                Set<OrganismClassification> setOrganisms = new TreeSet<OrganismClassification>();
                for (int index = 0; index < kingdoms.length; index++) {
                    String kingdom = kingdoms[index];
                    String family = null;
                    String specie = null;
                    String cell = null;
                    try {
                        family = families[index];
                        if (family.equals("")) {
                            family = null;
                        }
                    } catch (IndexOutOfBoundsException ex) {
                        System.out.println("ERROR OBTAINING family CLASSIFICATION IN LINE: " + lineNumber);
                    }
                    try {
                        specie = species[index];
                        if (specie.equals("")) {
                            specie = null;
                        }
                    } catch (IndexOutOfBoundsException ex) {
                        System.out.println("ERROR OBTAINING species CLASSIFICATION IN LINE: " + lineNumber);
                    }
                    try {
                        cell = cells[index];
                        if (cell.equals("")) {
                            cell = null;
                        }
                    } catch (IndexOutOfBoundsException ex) {
                        System.out.println("ERROR OBTAINING CELLS CLASSIFICATION IN LINE: " + lineNumber);
                    }
                    OrganismClassification organismClassification;
                    try {
                        organismClassification = new OrganismClassification(null, kingdom, family, specie, cell, references);
                        setOrganisms.add(organismClassification);
                    } catch (Exception ex) {

                    }

                }

                AspergillusCompound aspergillusCompound = new AspergillusCompound(knapSackId, null, pubChemId, chebId, biological_activity, compound_id,
                        compound_name, casId, formula, mass, compoundStatus, compoundType, logP, identifiers, classyfireClassifcation, references, setOrganisms);
                mapCompoundsFromCSV.put(knapSackId, aspergillusCompound);

                lineNumber++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return mapCompoundsFromCSV;
    }

// single insertion
    public static void main(String[] args) {
        CMMDatabase db = new CMMDatabase();
        db.connectToDB("jdbc:mysql://localhost/compounds?&allowPublicKeyRetrieval=true&useSSL=false&serverTimezone=UTC", "alberto", "alberto");
        //Map<String, AspergillusCompound> mapCompoundsFromCSV = loadCompoundsFromCSV(db);
        getHTMLsFromKnapSack(db);
    }
}
