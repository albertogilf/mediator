/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aspergillus;

import static aspergillus.AspergillusFromA2MDB.classifyCompoundsfromA2MDB;
import databases.CMMDatabase;
import databases.PubchemRest;
import exceptions.AspergillusNotFoundException;
import exceptions.CompoundNotClassifiedException;
import exceptions.NodeNotFoundException;
import exceptions.OrganismNotFoundException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Compound;
import model.Identifier;
import model.OrganismClassification;
import model.Reference;
import patternFinders.PatternFinder;
import static utilities.Constants.CLASSYFIRE_LIPID_NODEID;

/**
 *
 * @author alberto.gildelafuent
 */
public class AspergillusFromMariaStroe {

    public static Map<String, String> compoundToGroup;

    public static void FillInchisAndRefsFumigatus(CMMDatabase db, String inputFileName, String outputFileName) throws IOException, FileNotFoundException {
        Map<String, String> compoundToGroup = new HashMap();

        File fileAspergillus = new File(inputFileName);
        if (fileAspergillus.exists()) {
            FileInputStream fis = new FileInputStream(fileAspergillus);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);

            PrintWriter printWriter = null;
            printWriter = new PrintWriter(outputFileName);

            // read header and add the field inchi
            String csvLine = br.readLine();
            String[] oldData = csvLine.split("\\|");
            String[] newData = new String[oldData.length + 1];
            newData[0] = oldData[0];
            printWriter.print(newData[0] + "|");
            newData[1] = oldData[1];
            printWriter.print(newData[1] + "|");
            newData[2] = "Inchi";
            printWriter.print(newData[2] + "|");
            for (int i = 2; i < oldData.length - 1; i++) {
                newData[i + 1] = oldData[i];
                printWriter.print(newData[i + 1] + "|");
            }
            newData[oldData.length] = oldData[oldData.length - 1];
            printWriter.println(newData[oldData.length]);
            int line = 2;
            csvLine = br.readLine();

            while (csvLine != null) {

                oldData = csvLine.split("\\|");
                newData[0] = oldData[0];
                printWriter.print(newData[0] + "|");
                newData[1] = oldData[1];
                printWriter.print(newData[1] + "|");
                Integer compoundIdFromDatabase = db.getCompoundIdFromCompoundName(newData[1]);
                String inchi = db.getInChIFromCompound(compoundIdFromDatabase);
                newData[2] = inchi;
                printWriter.print(newData[2] + "|");
                for (int i = 2; i < oldData.length - 1; i++) {
                    newData[i + 1] = oldData[i];
                    printWriter.print(newData[i + 1] + "|");
                }
                newData[oldData.length] = oldData[oldData.length - 1];

                printWriter.println(newData[oldData.length]);
                line++;
                csvLine = br.readLine();
            }

            try {
                if (isr != null) {
                    isr.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(AspergillusFromMariaStroe.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (printWriter != null) {
                printWriter.close();
            }
        } else {
            throw new FileNotFoundException("aspergillus file not found");
        }

    }

    public static void FillInchisAndRefsNidulans(CMMDatabase db, String inputFileName, String outputFileName) throws IOException, FileNotFoundException {
        Map<String, String> compoundToGroup = new HashMap();

        File fileAspergillus = new File(inputFileName);
        if (fileAspergillus.exists()) {
            FileInputStream fis = new FileInputStream(fileAspergillus);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);

            PrintWriter printWriter = null;
            printWriter = new PrintWriter(outputFileName);

            // read header and add the field inchi
            String csvLine = br.readLine();
            String[] oldData = csvLine.split("\\|");
            String[] newData = new String[oldData.length + 2];
            newData[0] = "Group";
            printWriter.print(newData[0] + "|");
            newData[1] = oldData[0];
            printWriter.print(newData[1] + "|");
            newData[2] = "Inchi";
            printWriter.print(newData[2] + "|");
            for (int i = 1; i < oldData.length - 1; i++) {
                newData[i + 2] = oldData[i];
                printWriter.print(newData[i + 2] + "|");
            }
            newData[newData.length - 1] = oldData[oldData.length - 1];
            printWriter.println(newData[newData.length - 1]);
            int line = 2;
            csvLine = br.readLine();

            while (csvLine != null) {

                oldData = csvLine.split("\\|");
                newData[1] = oldData[0];
                newData[0] = compoundToGroup.getOrDefault(newData[1], "");
                printWriter.print(newData[0] + "|");
                printWriter.print(newData[1] + "|");
                Integer compoundIdFromDatabase = db.getCompoundIdFromCompoundName(newData[1]);
                String inchi = db.getInChIFromCompound(compoundIdFromDatabase);
                newData[2] = inchi;
                printWriter.print(newData[2] + "|");
                for (int i = 1; i < oldData.length - 1; i++) {
                    newData[i + 2] = oldData[i];
                    printWriter.print(newData[i + 2] + "|");
                }
                newData[newData.length - 1] = oldData[oldData.length - 1];
                printWriter.println(newData[newData.length - 1]);
                line++;
                csvLine = br.readLine();
            }

            try {
                if (isr != null) {
                    isr.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(AspergillusFromMariaStroe.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (printWriter != null) {
                printWriter.close();
            }
        } else {
            throw new FileNotFoundException("aspergillus file not found");
        }

    }

    public static Map<String, String> loadGroupFromFumigatus(String fileName) throws IOException, FileNotFoundException {
        Map<String, String> compoundToGroup = new HashMap();

        File fileAspergillus = new File(fileName);
        if (fileAspergillus.exists()) {
            FileInputStream fis = new FileInputStream(fileAspergillus);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            // read header and escape it
            String csvLine = br.readLine();
            csvLine = br.readLine();
            int line = 2;

            while (csvLine != null) {

                String[] data = csvLine.split("\\|");

                String groupName = data[0].trim();
                groupName = groupName.replaceAll("\"", "");
                if (groupName != null && groupName.equals("")) {
                    groupName = null;

                } else {
                    String compoundName = data[1].trim();
                    if (compoundToGroup.containsKey(compoundName)) {
                        System.out.println("Compound from two groups: " + compoundName);
                    }
                    compoundToGroup.put(compoundName, groupName);
                }
                line++;

                csvLine = br.readLine();
            }
        } else {
            throw new FileNotFoundException("aspergillus file not found");
        }

        return compoundToGroup;
    }

    public static void main(String[] args) {
        String inputFileNameFumigatus = "C:/Users/alberto.gildelafuent/OneDrive - Fundación Universitaria San Pablo CEU/research/aspergilius_mz/german_list/Afumigatus_SMs_MAMdatabase.csv";
        String inputFileNameNidulans = "C:/Users/alberto.gildelafuent/OneDrive - Fundación Universitaria San Pablo CEU/research/aspergilius_mz/german_list/Anidulans_SMs_MAMdatabase.csv";
        String outputFileNameFumigatus = "C:/Users/alberto.gildelafuent/OneDrive - Fundación Universitaria San Pablo CEU/research/aspergilius_mz/german_list/Afumigatus_SMs_MAMdatabase_inchis.csv";
        String outputFileNameNidulans = "C:/Users/alberto.gildelafuent/OneDrive - Fundación Universitaria San Pablo CEU/research/aspergilius_mz/german_list/Anidulans_SMs_MAMdatabase_inchis.csv";
        CMMDatabase db = new CMMDatabase();
        db.connectToDB("jdbc:mysql://localhost/compounds?useSSL=false&serverTimezone=UTC", "alberto", "alberto");
        // Inserting compounds from database

        try {
            AspergillusFromMariaStroe.compoundToGroup = loadGroupFromFumigatus(inputFileNameFumigatus);
            System.out.println("compounds with group: " + AspergillusFromMariaStroe.compoundToGroup.size());
            Set<String> groups = new HashSet(compoundToGroup.values());
            for (String group : groups) {
                System.out.println(group);
            }
            
            try {
                FillInchisAndRefsFumigatus(db, inputFileNameFumigatus, outputFileNameFumigatus);
                FillInchisAndRefsNidulans(db, inputFileNameNidulans, outputFileNameNidulans);

            } catch (FileNotFoundException ex) {
                System.out.println("File " + inputFileNameNidulans + " does not exist or you do not have permissions to read it");

            } catch (IOException ex) {

                Logger.getLogger(AspergillusFromMariaStroe.class
                        .getName()).log(Level.SEVERE, null, ex);
                System.out.println("I/O problem occurred");

            }
        } catch (FileNotFoundException ex) {

            Logger.getLogger(AspergillusFromMariaStroe.class
                    .getName()).log(Level.SEVERE, null, ex);
            System.out.println("File " + inputFileNameFumigatus + " does not exist or you do not have permissions to read it");

        } catch (IOException ex) {

            Logger.getLogger(AspergillusFromMariaStroe.class
                    .getName()).log(Level.SEVERE, null, ex);
            System.out.println("I/O problem occurred");
        }
    }
}
