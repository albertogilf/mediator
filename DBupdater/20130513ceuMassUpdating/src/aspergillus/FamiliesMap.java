/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aspergillus;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author alberto.gildelafuent
 */
public class FamiliesMap {
    public static final Map<String, String> GENUS_TO_FAMILY_MAP = new HashMap<String, String>();

    static {
        GENUS_TO_FAMILY_MAP.put("Aspergillus", "Trichocomaceae");
    }
    
}
