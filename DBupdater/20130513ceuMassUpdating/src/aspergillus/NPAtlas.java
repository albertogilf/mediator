/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aspergillus;

import static aspergillus.FamiliesMap.GENUS_TO_FAMILY_MAP;
import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import databases.CMMDatabase;
import databases.ChebiDatabase;
import databases.PubchemRest;
import static databases.PubchemRest.getPCIDFromInchiKey;
import exceptions.ChebiException;
import exceptions.CompoundNotClassifiedException;
import exceptions.NodeNotFoundException;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.AspergillusCompound;
import model.Identifier;
import model.OrganismClassification;
import model.Reference;
import patternFinders.PatternFinder;
import utilities.Constants;
import static utilities.Constants.CLASSYFIRE_LIPID_NODEID;

/**
 * Class to get information from NPAtlas. The information is provided as a JSON
 *
 * @author alberto.gildelafuent
 */
public class NPAtlas {

    public static final String PATH_NPATLAS_JSON = "C:\\Users\\alberto.gildelafuent\\OneDrive - Fundación Universitaria San Pablo CEU\\research\\aspergilius_mz\\npatlas\\NPAtlas_download.json";
    public static final String LABEL_NPATLAS_ID = "npaid";
    public static final String LABEL_NPATLAS_NAME = "name";
    public static final String LABEL_NPATLAS_FORMULA = "mol_formula";
    public static final String LABEL_NPATLAS_INCHI_KEY = "inchikey";
    public static final String LABEL_NPATLAS_INCHI = "inchi";
    public static final String LABEL_NPATLAS_SMILES = "smiles";
    public static final String LABEL_NPATLAS_EXACT_MASS = "exact_mass";
    public static final String LABEL_NPATLAS_REFERENCE = "origin_reference";
    public static final String LABEL_NPATLAS_ORGANISM = "origin_organism";

    public static void insertCompoundsFromNPAtlasJSON(CMMDatabase db) {
        try {
            FileReader fileJSON = new FileReader(PATH_NPATLAS_JSON);
            JsonReader reader = new JsonReader(fileJSON);
            JsonParser parser = new JsonParser();
            try {
                int chebi_count = 0;
                int pubchem_count = 0;
                JsonArray jsonArrayNPAtlasCompounds = parser.parse(reader).getAsJsonArray();
                for (int i = 0; i < jsonArrayNPAtlasCompounds.size(); i++) {
                    JsonObject jsonNPAtlasCompound = jsonArrayNPAtlasCompounds.get(i).getAsJsonObject();
                    int npatlas_id = jsonNPAtlasCompound.get(LABEL_NPATLAS_ID).getAsInt();
                    String compound_name = jsonNPAtlasCompound.get(LABEL_NPATLAS_NAME).getAsString().trim();
                    String formula = jsonNPAtlasCompound.get(LABEL_NPATLAS_FORMULA).getAsString().trim();
                    String inchiKey = jsonNPAtlasCompound.get(LABEL_NPATLAS_INCHI_KEY).getAsString().trim();
                    String inchi = jsonNPAtlasCompound.get(LABEL_NPATLAS_INCHI).getAsString().trim();
                    String smiles = jsonNPAtlasCompound.get(LABEL_NPATLAS_SMILES).getAsString().trim();
                    Double mass = jsonNPAtlasCompound.get(LABEL_NPATLAS_EXACT_MASS).getAsDouble();
                    String casId = null;
                    String biologicalActivity = null;
                    String knapSackId = null;
                    JsonObject npatlasReferenceJsonObject = jsonNPAtlasCompound.get(LABEL_NPATLAS_REFERENCE).getAsJsonObject();
                    Reference reference = getReferenceFromNPATlasJSonObject(npatlasReferenceJsonObject);
                    // System.out.println(npatlas_id + "\t" + reference);
                    JsonObject npatlasOrganismJsonObject = jsonNPAtlasCompound.get(LABEL_NPATLAS_ORGANISM).getAsJsonObject();
                    OrganismClassification organismClassification = null;
                    try {
                        // Insert only compounds from aspergillus by now
                        organismClassification = getOrganismFromNPATlasJSonObject(npatlasOrganismJsonObject, reference);
                        Set<OrganismClassification> setOrganisms = new TreeSet<>();
                        setOrganisms.add(organismClassification);
                        Identifier identifiers = new Identifier(inchi, inchiKey, smiles);
                        Integer chebId = 0;
                        try {
                            chebId = ChebiDatabase.getChebiFromIdentifiers(identifiers);
                            chebi_count++;
                        } catch (ChebiException ex) {
                            // System.out.println(knapSackId + " -> CHEBI NOT FOUND");
                        } catch (Exception ex) {
                            // System.out.println(knapSackId + " -> ERROR OBTAINING CHEBI: " + ex.getMessage());
                        }
                        Integer pubChemId = 0;
                        try {
                            pubChemId = getPCIDFromInchiKey(inchiKey);

                            pubchem_count++;
                        } catch (IOException ex) {
                            // System.out.println(knapSackId + " -> ERROR OBTAINING PUBCHEM: " + ex);
                        } catch (NullPointerException ex) {
                            // System.out.println(knapSackId + " -> Pubchem NOT FOUND");
                        }


                        int compoundType = 0;
                        List<String> ancestorNodes = new LinkedList<String>();
                        try {
                            ancestorNodes = downloadersOfWebResources.DownloaderOfWebResources.getAncestorNodesCompoundClassyfireClassification(inchi, inchiKey);
                            if (ancestorNodes.contains(CLASSYFIRE_LIPID_NODEID)) {
                                compoundType = 1;
                            }
                        } catch (CompoundNotClassifiedException ex) {
                        }

                        // TODO INSERT 1-Compound 2- Organism; 3- Reference; 4-Relation Compound-organism-reference 5- CLASSYFIRE 6- Insert chebi, pubchem y aspergillus
                        Integer compoundId = db.getCompoundIdFromInchiKey(inchiKey);
                        if (compoundId == 0) {
                            // INCHI FROm KNAPSACK CAN BE MISSCALCULATED
                            compoundId = db.getCompoundIdFromInchi(inchi);
                            if (compoundId == 0) {
                                compoundId = db.getCompoundIdFromCompoundName(compound_name);
                                if (compoundId == 0) {
                                    // insert compound
                                    int[] charges = PatternFinder.getChargeFromSmiles(smiles);
                                    int chargeType = charges[0];
                                    int numCharges = charges[1];
                                    String formulaType = PatternFinder.getTypeFromFormula(formula);

                                    Integer compoundStatus = 1;
                                    Double logP = null;
                                    AspergillusCompound aspergillusCompound = new AspergillusCompound(knapSackId, npatlas_id, pubChemId, chebId, biologicalActivity, compoundId,
                                            compound_name, casId, formula, mass, compoundStatus, compoundType, logP, identifiers, null, null, setOrganisms);
                                    compoundId = db.insertCompound(casId, compound_name, formula, mass, chargeType, numCharges, formulaType, compoundType, compoundStatus, logP);
                                }

                                db.insertIdentifiers(compoundId, inchi, inchiKey, smiles);
                            }
                        }

                        // insert NPAtlasID
                        db.insertCompoundNPAtlas(compoundId, npatlas_id);
                        Integer organismId = db.insertOrganism(organismClassification);
                        Set<Reference> referencesOrganismPartial = organismClassification.getReferences();
                        db.insertCompoundOrganism(compoundId, organismId);
                        for (Reference referenceFromOrganism : referencesOrganismPartial) {
                            Integer referenceId = db.insertReference(referenceFromOrganism);
                            db.insertCompoundReference(compoundId, referenceId);
                            db.insertOrganismReference(organismId, referenceId);
                        }
                        try {
                            // Insert classyfire
                            db.insertFullCompoundClassyfireClassification(compoundId, ancestorNodes);
                        } catch (NodeNotFoundException ex) {
                            System.out.println("NODE NOT FOUND: " + ex);
                        }

                        if (chebId != 0) {
                            db.insertCompoundCHEBI(compoundId, chebId);
                        }
                        if (pubChemId != 0) {
                            db.insertCompoundPC(compoundId, pubChemId);
                        }
                        db.insertCompoundAspergillus(compoundId, biologicalActivity, null, null, null);

                    } catch (Exception e) {
                        // TO DEBUG AND INSERT THE FAMILIES OF THE DIFFERENT GENUS
                        //System.out.println(e);
                    }

                }
            } catch (JsonSyntaxException jse) {
                System.out.println("Wrong syntax of the array of compounds");
                jse.printStackTrace();
            } catch (JsonIOException jioe) {
                System.out.println("Error reading the JSONFILE " + PATH_NPATLAS_JSON);
                jioe.printStackTrace();
            }
        } catch (FileNotFoundException fnfe) {
            System.out.println("Check the file: " + PATH_NPATLAS_JSON);
        }

    }

    public static void main(String[] args) {

        CMMDatabase db = new CMMDatabase();
        db.connectToDB("jdbc:mysql://localhost/compounds?&allowPublicKeyRetrieval=true&useSSL=false&serverTimezone=UTC", "alberto", "alberto");
        insertCompoundsFromNPAtlasJSON(db);
    }

    private static Reference getReferenceFromNPATlasJSonObject(JsonObject npatlasReferenceJsonObject) {
        String label_reference_doi = "doi";
        String label_reference_authors = "authors";
        String label_reference_title = "title";
        String label_reference_journal = "journal";
        String label_reference_year = "year";
        String label_reference_volume = "volume";
        String label_reference_issue = "issue";
        String label_reference_pages = "pages";
        String journal_title = npatlasReferenceJsonObject.get(label_reference_journal).getAsString();
        Integer year = null;
        if (!npatlasReferenceJsonObject.get(label_reference_year).isJsonNull()) {
            year = npatlasReferenceJsonObject.get(label_reference_year).getAsInt();
        }
        String volume = null;
        if (!npatlasReferenceJsonObject.get(label_reference_volume).isJsonNull()) {
            volume = npatlasReferenceJsonObject.get(label_reference_volume).getAsString().trim();
        }

        String issue = null;
        if (!npatlasReferenceJsonObject.get(label_reference_issue).isJsonNull()) {
            issue = npatlasReferenceJsonObject.get(label_reference_issue).getAsString().trim();
        }
        String pages = null;
        if (!npatlasReferenceJsonObject.get(label_reference_pages).isJsonNull()) {
            pages = npatlasReferenceJsonObject.get(label_reference_pages).getAsString().trim();
        }

        String reference_text = Reference.referenceTextFromData(journal_title, year, volume, issue, pages);

        String doi = npatlasReferenceJsonObject.get(label_reference_doi).getAsString().trim();
        String link = Constants.DOI_WEBPAGE + doi;
        Reference reference = new Reference(null, reference_text, doi, link);
        return reference;
    }

    private static OrganismClassification getOrganismFromNPATlasJSonObject(
            JsonObject npatlasOrganismJsonObject,
            Reference reference) throws Exception {
        String label_organism_id = "id";
        String label_organism_type = "type";
        String label_organism_genus = "genus";
        String label_organism_species = "species";
        String label_organism_taxon = "taxon";
        Integer id = null;
        if (!npatlasOrganismJsonObject.get(label_organism_id).isJsonNull()) {
            id = npatlasOrganismJsonObject.get(label_organism_id).getAsInt();
        }

        String type = null;
        if (!npatlasOrganismJsonObject.get(label_organism_type).isJsonNull()) {
            type = npatlasOrganismJsonObject.get(label_organism_type).getAsString().trim();
        }
        String genus = null;
        if (!npatlasOrganismJsonObject.get(label_organism_genus).isJsonNull()) {
            genus = npatlasOrganismJsonObject.get(label_organism_genus).getAsString().trim();
        }

        String species = null;
        if (!npatlasOrganismJsonObject.get(label_organism_species).isJsonNull()) {
            species = npatlasOrganismJsonObject.get(label_organism_species).getAsString().trim();
        }
        Set<Reference> setReferences = new TreeSet<Reference>();
        setReferences.add(reference);
        try {
            String kingdom = OrganismClassification.getKingdomFromNPAtlas(type);

            String family = GENUS_TO_FAMILY_MAP.get(genus);
            if (family != null) {
                species = genus + " " + species;
                String genusAndSpecie = PatternFinder.getGenusSpecieFromGenusSpecieAndSubspecies(species);
                String subspecie = PatternFinder.getSubspecieFromGenusSpecieAndSubspecie(species);
                OrganismClassification organismClassification = new OrganismClassification(0, kingdom, family,
                        genusAndSpecie, subspecie, setReferences);
                return organismClassification;
            }
            throw new Exception("Family not found for genus: " + genus);
        } catch (Exception ex) {
            throw ex;
        }

    }

}
