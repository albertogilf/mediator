/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aspergillus;

import databases.CMMDatabase;
import exceptions.OrganismNotFoundException;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.OrganismClassification;

/**
 *
 * @author alberto.gildelafuent
 */
public class UpdateAspergillusOrganismsProcess {

    public static void updateOrganismLevelFromOrganismTable(CMMDatabase db) {
        // STILL TO CHECK HOW TO GET THE FULL ORGANISM TREE
        /*
        Set<String> organismNames = null;
            organismNames = db.getOrganismNamesForUpdatingOrganism();
        
        System.out.println("organismNamesLength: " + organismNames.size());
        System.out.println(organismNames);
        
         //organismNames.clear()
         // CHECK BEFORE THE ORGANISM WITH THE SAME NAME
        
        for (String organismName : organismNames) {
            try {
                Integer organismIdGenusAndSpecie = db.getOrganismIdFromOrganismName(organismName);

                String[] genusAndSpecie = organismName.split(" ");
                String genus = genusAndSpecie[0].trim();
                try {
                    String specie = genusAndSpecie[1].trim();
                    Integer organismIdGenus = null;
                    try {
                        organismIdGenus = db.getOrganismIdFromOrganismName(genus);

                    } catch (OrganismNotFoundException ex) {
                        try {
                            // Genus organism does not exist, we need to create it
                            OrganismClassification oldOrganism = db.getOrganismFromOrganismName(organismName);
                            // Insert the new genus organism
                            OrganismClassification newOrganism;
                            try {
                                newOrganism = new OrganismClassification(0, oldOrganism.getKingdom(), oldOrganism.getFamily(), genus, specie, null, null);
                                Integer oldOrganismId = oldOrganism.getOrganismId();
                                // Insert new organism
                                Integer parentId = db.getParentIdFromOrganismId(oldOrganismId);

                                String queryUpdateOrganismLevelGenusAndSpecie = "INSERT IGNORE INTO organism("
                                        + "organism_name, "
                                        + "organism_level, parent_id) VALUES(\""
                                        + genus + "\", 3, "
                                        + parentId + ")";
                                organismIdGenus = db.executeNewInsertion(queryUpdateOrganismLevelGenusAndSpecie);

                                

                            } catch (Exception ex1) {
                                Logger.getLogger(UpdateAspergillusOrganismsProcess.class.getName()).log(Level.SEVERE, null, ex1);
                            }

                        } catch (OrganismNotFoundException ex1) {
                            // EPIC FAIL
                            Logger.getLogger(UpdateAspergillusOrganismsProcess.class.getName()).log(Level.SEVERE, null, ex1);
                            System.out.println("EPIC FAIL. IT CANT HAPPEN! ");
                        }
                    }

                    // The Genus exists, update the parent_id and the parent_level of the children
                    String queryUpdateOrganismLevelGenusAndSpecie = "update organism set organism_name = \"" + specie + "\", parent_id = " + organismIdGenus + ", organism_level = 4 where organism_id = " + organismIdGenusAndSpecie;
                    String queryUpdateChildren = "Update organism set organism_level = 5 where parent_id = " + organismIdGenusAndSpecie;
                    db.executeIDU(queryUpdateOrganismLevelGenusAndSpecie);
                    db.executeNewIDU(queryUpdateChildren);
                    System.out.println(queryUpdateOrganismLevelGenusAndSpecie);

                } catch (IndexOutOfBoundsException ioobe) {
                    System.out.println("The organism only contains the genus" + organismName);
                }
            } catch (OrganismNotFoundException ex1) {
                // EPIC FAIL
                Logger.getLogger(UpdateAspergillusOrganismsProcess.class.getName()).log(Level.SEVERE, null, ex1);
                System.out.println("EPIC FAIL. IT CANT HAPPEN! ");
            }

        }
        // Split the level 3 (genus + specie) in level 3 (genus) + level 4(specie)
*/
    }

    public static void main(String[] args) {
        CMMDatabase db = new CMMDatabase();
        db.connectToDB("jdbc:mysql://localhost/compounds?useSSL=false&serverTimezone=UTC", "alberto", "alberto");
        updateOrganismLevelFromOrganismTable(db);
    }
}
