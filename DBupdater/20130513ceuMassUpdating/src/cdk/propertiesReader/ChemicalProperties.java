/*
 * NewCMM_Class.java
 *
 * Created on 29-may-2018, 15:49:53
 *
 * Copyright(c) 2018 Ceu Mass Mediator All Rights Reserved.
 * This software is the proprietary information of Alberto Gil de la Fuente.
 *
 */
package cdk.propertiesReader;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.UnsupportedEncodingException;
import java.util.List;
import org.openscience.cdk.ChemFile;
import org.openscience.cdk.ChemObject;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.io.MDLV2000Reader;
import org.openscience.cdk.tools.manipulator.ChemFileManipulator;

/**
 * Class to obtain the chemical Properties of a molecule using the Chemistry
 * Development Kit (CDK)
 *
 * @version $Revision: 1.1.1.1 $
 * @since Build {insert version here} 29-may-2018
 *
 * @author Alberto Gil de la Fuente
 */
public class ChemicalProperties {

    /**
     * Creates a new instance of NewCMM_Class
     */
    public ChemicalProperties() {

    }

    /**
     * Read Molecule file (.mol) from specified Path.
     *
     * @param file the file
     *
     * @return the molecule
     *
     * @throws CDKException the CDK exception
     * @throws FileNotFoundException the file not found exception
     */
    public static IAtomContainer Read(String file) throws CDKException, FileNotFoundException {
        MDLV2000Reader reader;
        List<IAtomContainer> containersList;

        reader = new MDLV2000Reader(new FileReader(new File(file)));
        ChemFile chemFile = (ChemFile) reader.read((ChemObject) new ChemFile());
        containersList = ChemFileManipulator.getAllAtomContainers(chemFile);

        IAtomContainer mol = containersList.get(0);
        return mol;

    }

    /**
     * Read a mol file from a given string.
     *
     * @param molString the mol string
     *
     * @return the i atom container
     *
     * @throws CDKException the CDK exception
     * @throws FileNotFoundException the file not found exception
     * @throws UnsupportedEncodingException the unsupported encoding exception
     */
    public static IAtomContainer ReadFromString(String molString) 
            throws CDKException, FileNotFoundException, UnsupportedEncodingException {
        MDLV2000Reader reader;
        List<IAtomContainer> containersList;

        reader = new MDLV2000Reader(new ByteArrayInputStream(molString.getBytes("UTF-8")));
        ChemFile chemFile = (ChemFile) reader.read((ChemObject) new ChemFile());
        containersList = ChemFileManipulator.getAllAtomContainers(chemFile);

        IAtomContainer mol = containersList.get(0);
        return mol;

    }
}
