/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package checkers.cas;

/**
 *
 * @author: San Pablo-CEU, Alberto Gil de la Fuente
 * @version: 4.0, 20/07/2016
 */
import ioDevices.MyFile;
import java.io.File;
import patternFinders.PatternFinder;

import databases.CMMDatabase;
import ioDevices.IoDevice;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Random;
import static utilities.Constants.CAS_ONLINE_PATH;
import static utilities.Constants.CAS_RESOURCES_PATH;

public abstract class CheckerCas {

    static PatternFinder pf = new PatternFinder();
    static Random randomGenerator = new Random();

    /**
     * Update all the compounds already present in the database from the source
     * ChemIdPlus
     *
     * @param db
     */
    public static void downloadAllCasIds(CMMDatabase db) {
        String query = "select distinct cas_id from compounds where cas_id is not null";
        ResultSet casIds = db.getResultSet(query);
        try {
            while (casIds.next()) {
                updateCasId(db, query);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CheckerCas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * method to check if the compound is in the table compounds_cas in order to
     * insert the compound which is not present there.
     *
     * @param db CMMDatabase where it is inserted
     * @param casId String with the cas identifier to search in ChemIdPlus
     * without ""
     */
    public static void insertCasId(CMMDatabase db, String casId) {
        String cas_id;
        if (casId.equalsIgnoreCase("")) {
            return;
        } else {
            cas_id = "\"" + casId + "\"";
        }
        String query;
        query = "SELECT formula FROM compounds_cas where cas_id= " + cas_id;
        ResultSet listStrings = db.getResultSet(query);
        try {
            // If there is no compound in the database
            if (!listStrings.next()) {
                downloadCASIDFile(casId);
                String formula = getFormulaCasId(casId);
                if (formula.equals("")) {
                    formula = "NULL";
                } else {
                    formula = "\"" + formula + "\"";
                }
                double mass = getWeightCasId(casId);
                String inChi = getInChICasId(casId);
                if (inChi.equals("")) {
                    inChi = "NULL";
                } else {
                    inChi = "\"" + inChi + "\"";
                }
                String inChiKey = getInChIKeyCasId(casId);
                if (inChiKey.equals("")) {
                    inChiKey = "NULL";
                } else {
                    inChiKey = "\"" + inChiKey + "\"";
                }
                insertCompoundCas(db, cas_id, formula, mass, inChi, inChiKey);

            }
        } catch (SQLException ex) {
            Logger.getLogger(CheckerCas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * method which update the table compound_cas with the information from
     * ChemIdPlus If the compound is not in the table, is is inserted.
     *
     * @param db CMMDatabase where it is inserted
     * @param casId String with the cas identifier to search in ChemIdPlus
     * without ""
     */
    public static void updateCasId(CMMDatabase db, String casId) {
        String cas_id;
        if (casId.equalsIgnoreCase("")) {
            return;
        } else {
            cas_id = "\"" + casId + "\"";
        }
        String query;
        String startInsertion;
        String endInsertion = "";

        // Information from ChemIdPlus
        forceDownloadFile(casId);
        String formula = getFormulaCasId(casId);
        if (formula.equals("")) {
            formula = "NULL";
        } else {
            formula = "\"" + formula + "\"";
        }
        double mass = getWeightCasId(casId);
        String inChi = getInChICasId(casId);
        if (inChi.equals("")) {
            inChi = "NULL";
        } else {
            inChi = "\"" + inChi + "\"";
        }
        String inChiKey = getInChIKeyCasId(casId);
        if (inChiKey.equals("")) {
            inChiKey = "NULL";
        } else {
            inChiKey = "\"" + inChiKey + "\"";
        }

        query = "SELECT formula FROM compounds_cas where cas_id= " + cas_id;
        ResultSet listStrings = db.getResultSet(query);
        try {
            // If there is no compound in the database
            if (!listStrings.next()) {
                insertCompoundCas(db, cas_id, formula, mass, inChi, inChiKey);
            } else {
                // populate information of cas ids from chemIDplus
                startInsertion = "UPDATE compounds_cas set ";
                if (!formula.equals("NULL")) {
                    endInsertion = endInsertion + ", formula= " + formula;
                }
                if (mass != 0) {
                    endInsertion = endInsertion + ", mass= " + mass;
                }
                if (!inChi.equals("NULL")) {
                    endInsertion = endInsertion + ", inchi= " + inChi;
                }
                if (!inChiKey.equals("NULL")) {
                    endInsertion = endInsertion + ", inchi_key= " + inChiKey;
                }

                if (!endInsertion.equals("")) {
                    endInsertion = endInsertion.substring(2, endInsertion.length());
                    startInsertion = startInsertion + endInsertion + " where cas_id=" + cas_id;
                    //System.out.println("\n inserting: " + startInsertion);
                    db.executeNewIDU(startInsertion);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CheckerCas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteFilesCasIds() {

        File dir = new File(CAS_RESOURCES_PATH);
        String[] ficheros = dir.list();

        for (String cas : ficheros) {
            deleteFileCasId(cas);
        }
    }

    public void deleteFileCasId(String casId) {
        File file;
        file = new File(CAS_RESOURCES_PATH + casId + ".html");
        file.delete();
    }

    /**
     *
     * @param casId
     * @return the formula of the compound
     */
    public static String getFormulaCasId(String casId) {

        String fileName = CAS_RESOURCES_PATH + casId + ".html";

        File casIdFile = new File(fileName);
        if (casIdFile.exists()) {
        } else {
            downloadCASIDFile(casId);
        }
        String content = MyFile.obtainContentOfABigFile(fileName).toString();
        //Get information from html
        // Molecular Formula</h2><div class="ds"><ul><li id="ds1"><div>C7-H8</div>

        // String regularExpression="[A-Z][a-z]?\\d*|(?<!\\([^)]*)\\(.*\\)\\d+(?![^(]*\\))";
        String idPattern = "[a-zA-Z0-9]*";
        String preFormula = "id=\"formulas\">Molecular Formula</h2><div class=\"ds\"><ul><li id=\"" + idPattern + "\"><div>";
        String formulaEnd = "</div>";
        String formula = PatternFinder.searchWithReplacement(content, preFormula + "(.*?)(" + formulaEnd + ")",
                preFormula + "|" + formulaEnd + "|\n");
        formula = formula.replaceAll("-", "");
        formula = formula.replaceAll("\\s+", "");
        // String formula = PatternFinder.searchWithoutReplacement(content, preFormula + "(.*?)");
//        System.out.println("\n cas id: " + casId + "    formula: " + formula);
        if (formula.equals("Unspecified")) {
            formula = "";
        }
        return formula;
    }

    /**
     *
     * @param casId
     * @return a double, the average weight of the compound
     */
    public static double getWeightCasId(String casId) {
        double finalWeight;
        String fileName = CAS_RESOURCES_PATH + casId + ".html";

        File casIdFile = new File(fileName);
        if (casIdFile.exists()) {
        } else {
            downloadCASIDFile(casId);
        }
        String content = MyFile.obtainContentOfABigFile(fileName).toString();
        //Get information from html
        // Molecular Formula</h2><div class="ds"><ul><li id="ds1"><div>C7-H8</div>
        String preWeight = "<h2>Molecular Weight";
        String weightEnd = "</div>";
        String weight = PatternFinder.searchWithReplacement(content, preWeight + "(.*?)(" + weightEnd + ")",
                "FORMULA|" + weightEnd + "|\n");

        weight = weight.replaceAll("\\s+", "");
        finalWeight = PatternFinder.searchDouble(weight);
//        System.out.println("\n cas id: " + casId + "    weight: " + finalWeight);
        return finalWeight;
    }

    /**
     *
     * @param casId
     * @return the InChI of the compound. If there is no InChI returns ""
     */
    public static String getInChICasId(String casId) {
        String fileName = CAS_RESOURCES_PATH + casId + ".html";

        File casIdFile = new File(fileName);
        if (casIdFile.exists()) {
        } else {
            downloadCASIDFile(casId);
        }
        String content = MyFile.obtainContentOfABigFile(fileName).toString();

        String structDesc = "id=\"structureDescs\"";
        String inChIId = "<h3>InChI</h3>";
        String inChIIdEnd = "<br>";
        // Look the content where the InChI is written
        String inChIBlock = PatternFinder.searchWithoutReplacement(content, structDesc + "(.*?)" + inChIId + "(.*?)"
                + "(" + inChIIdEnd + ")");
        String inChI = PatternFinder.searchWithReplacement(inChIBlock, inChIId + "(.*?)" + inChIIdEnd,
                inChIId + "|" + inChIIdEnd + "|\n");
        inChI = inChI.trim();
        // Delete the special characters
        inChI = inChI.replaceAll("\\s+", "");
        //System.out.println("\n cas id: " + casId + "    InchI block: \n" + inChIBlock);
        //System.out.println("\n Inchi: " + inChI);
        // String formula = PatternFinder.searchWithoutReplacement(content, preFormula + "(.*?)");
        return inChI;
    }

    /**
     *
     * @param casId
     * @return the InChIKey of the compound. If there is no InChIKey returns ""
     */
    public static String getInChIKeyCasId(String casId) {
        String fileName = CAS_RESOURCES_PATH + casId + ".html";

        File casIdFile = new File(fileName);
        if (casIdFile.exists()) {
        } else {
            downloadCASIDFile(casId);
        }
        String content = MyFile.obtainContentOfABigFile(fileName).toString();

        String structDesc = "id=\"structureDescs\"";
        String inChIKeyId = "<h3>InChIKey</h3>";
        String inChIKeyIdEnd = "<br>";
        // Look the content where the InChIKey is written
        String inChIBlock = PatternFinder.searchWithoutReplacement(content, structDesc + "(.*?)" + inChIKeyId + "(.*?)"
                + "(" + inChIKeyIdEnd + ")");
        String inChIKey = PatternFinder.searchWithReplacement(inChIBlock, inChIKeyId + "(.*?)" + inChIKeyIdEnd,
                inChIKeyId + "|" + inChIKeyIdEnd + "|\n");
        inChIKey = inChIKey.trim();
        // Delete the special characters
        inChIKey = inChIKey.replaceAll("\\s+", "");
        //System.out.println("\n cas id: " + casId + "    InchIKey block: \n" + inChIBlock);
        //System.out.println("\n InchiKey: " + inChIKey);
        // String formula = PatternFinder.searchWithoutReplacement(content, preFormula + "(.*?)");
        return inChIKey;
    }

    /**
     * Still to do
     *
     * @param casId
     * @return
     */
    public static String getSmilesCasId(String casId) {

        //TODO!
        String fileName = CAS_RESOURCES_PATH + casId + ".html";

        File casIdFile = new File(fileName);
        if (casIdFile.exists()) {
            //casIdFile.delete();
        } else {
            downloadCASIDFile(casId);
        }
        String content = MyFile.obtainContentOfABigFile(fileName).toString();
        //TODO
        String structDesc = "id=\"structureDescs\"";
        String inChIKeyId = "<h3>InChIKey</h3>";
        String inChIKeyIdEnd = "<br>";
        // Look the content where the InChIKey is written
        String inChIBlock = PatternFinder.searchWithoutReplacement(content, structDesc + "(.*?)" + inChIKeyId + "(.*?)"
                + "(" + inChIKeyIdEnd + ")");
        String inChIKey = PatternFinder.searchWithReplacement(inChIBlock, inChIKeyId + "(.*?)" + inChIKeyIdEnd,
                inChIKeyId + "|" + inChIKeyIdEnd);
        inChIKey = inChIKey.trim();
        System.out.println("\n cas id: " + casId + "    InchI block: \n" + inChIBlock);
        System.out.println("\n InchiKey: " + inChIKey);
        // String formula = PatternFinder.searchWithoutReplacement(content, preFormula + "(.*?)");
        return inChIKey;
    }

    /**
     * Gets the formula from table compounds_cas
     *
     * @param casId
     * @param db
     * @return
     */
    public static String getFormulaFromDB(String casId, CMMDatabase db) {
        String query;
        casId = "\"" + casId + "\"";
        query = "SELECT formula FROM compounds_cas where cas_id=" + casId;
        String formulaCas = db.getString(query);
        formulaCas = formulaCas.replaceAll("\\s+", "");
        return formulaCas;
    }

    /**
     * Gets the mass from table compounds_cas
     *
     * @param casId
     * @param db
     * @return
     */
    public static double getMassFromDB(String casId, CMMDatabase db) {
        String query;
        casId = "\"" + casId + "\"";
        query = "SELECT mass FROM compounds_cas where cas_id=" + casId;
        double weightCas = db.getDouble(query);
        return weightCas;
    }

    /**
     * Gets the InChIKey from table compounds_cas
     *
     * @param casId
     * @param db
     * @return
     */
    public static String getInChIKeyFromDB(String casId, CMMDatabase db) {
        String query;
        casId = "\"" + casId + "\"";
        query = "SELECT inchi_key FROM compounds_cas where cas_id=" + casId;
        String InChIKey = db.getString(query);
        return InChIKey;
    }

    /**
     * Gets the mass from table compounds_cas
     *
     * @param casId
     * @param db
     * @param formula
     * @param mass
     * @param inChI
     * @param inChIKey
     */
    public static void insertCompoundCas(CMMDatabase db, String casId, String formula,
            double mass, String inChI, String inChIKey) {
        // populate information of cas ids from chemIDplus
        String insertion;
        insertion = "INSERT IGNORE INTO compounds_cas(cas_id, formula, mass,inchi,inchi_key) VALUES("
                + casId + ", " + formula + ", " + mass + ", " + inChI + ", " + inChIKey + ")";
//                System.out.println("\n inserting: " + insertion);
        db.executeNewIDU(insertion);
    }

    private static void downloadCASIDFile(String casId) {
        String fileName = CAS_RESOURCES_PATH + casId + ".html";
        File casIdFile = new File(fileName);
        if (casIdFile.exists()) {
            //casIdFile.delete();
        } else {
            StringBuilder content = IoDevice.read(CAS_ONLINE_PATH + casId);
            MyFile.write(content.toString(), fileName);
        }
        try {
            int randomInt;
            randomInt = randomGenerator.nextInt((10000 - 1000) + 1) + 1000;
            Thread.sleep(randomInt);                 //1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            System.out.println("Thread interrupted");
            Thread.currentThread().interrupt();
        }
    }

    private static void forceDownloadFile(String casId) {
        String fileName = CAS_RESOURCES_PATH + casId + ".html";
        File casIdFile = new File(fileName);
        if (casIdFile.exists()) {
            casIdFile.delete();
        }
        StringBuilder content = IoDevice.read(CAS_ONLINE_PATH + casId);
        MyFile.write(content.toString(), fileName);
        try {
            int randomInt;
            randomInt = randomGenerator.nextInt((10000 - 1000) + 1) + 1000;
            Thread.sleep(randomInt);                 //1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            System.out.println("Thread interrupted");
            Thread.currentThread().interrupt();
        }
    }

}
