/*
 * ClassifyFromClassyFire.java
 *
 * Created on 20-mar-2018, 11:36:54
 *
 * Copyright(c) 2018 Ceu Mass Mediator All Rights Reserved.
 * This software is the proprietary information of Alberto Gil de la Fuente.
 *
 */
package classyfire;

import com.google.gson.JsonObject;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import databases.CMMDatabase;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import utilities.Constants;

/**
 * Class to insert all nodes from classyfire and their hierarchy.
 *
 * @version $Revision: 1.1.1.1 $
 * @since Build version 4.0 20-mar-2018
 *
 * @author Alberto Gil de la Fuente
 */
public class ClassifyFromClassyFire {

    /**
     * Creates a new instance of ClassifyFromClassyFire
     */
    public ClassifyFromClassyFire() {

    }

    public static void insertNodesFromClassyfire(CMMDatabase db) {
        // DownloaderOfWebResources.downloadNodesFromClassyfireJSONFile();
        FileReader fileJSON;
        try {
            fileJSON = new FileReader(Constants.CLASSYFIRE_RESOURCES_PATH + Constants.CLASSYFIRE_FILE_NAME);
            JsonReader reader = new JsonReader(fileJSON);
            JsonParser parser = new JsonParser();
            try {
                JsonArray arrayJSONNodes = parser.parse(reader).getAsJsonArray();
                for (int n = 0; n < arrayJSONNodes.size(); n++) {
                    JsonObject nodeJSON = arrayJSONNodes.get(n).getAsJsonObject();
                    if (!nodeJSON.get(Constants.CLASSYFIRE_NODE_NAME).isJsonNull()) {
                        String nodeName = nodeJSON.get(Constants.CLASSYFIRE_NODE_NAME).getAsString();
                        String nodeId = nodeJSON.get(Constants.CLASSYFIRE_NODE_ID).getAsString();
                        if (!nodeJSON.get(Constants.CLASSYFIRE_NODE_PARENT_ID).isJsonNull()) {
                            String nodeParentId = nodeJSON.get(Constants.CLASSYFIRE_NODE_PARENT_ID).getAsString();
                            db.insertNodeCLASSYFIRE(nodeName,nodeId,nodeParentId);
                            //System.out.println("NAME, " + nodeName + "  " + nodeId + "  " + nodeParentId);
                        }
                        else{
                            //System.out.println("NAME, " + nodeName + "  " + nodeId + " PARENT NULL" );
                        }
                    }
                    else{
                        System.out.println("NAME NULL");
                    }
                }
            } catch (IllegalStateException ex) {
                System.out.println("INSERTION OF TAX NODES FROM CLASSYFIRE FAILED");
                return;
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ClassifyFromClassyFire.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
