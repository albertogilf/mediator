/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package databasePopulators;

import databases.CMMDatabase;
import ioDevices.IoDevice;
import ioDevices.MyFile;
import java.io.File;
import java.util.List;
import java.util.Random;
import patternFinders.PatternFinder;

/**
 * @Deprecated
 * @author Alberto Gild e la Fuente
 */
public class MetlinDatabasePopulator {

    private static final String METLINPATH = "/home/alberto/alberto/repo/mediator/DBupdater/20130513ceuMassUpdating/resources/metlin/";

    PatternFinder pf = new PatternFinder();

    public void populateFromAMetlinWebPage(String fileName, CMMDatabase db) {
        String metlin_id = fileName.replaceAll(".html", "");
        String content = MyFile.read(METLINPATH + metlin_id + ".html").toString();

        String queryFailed = PatternFinder.searchWithoutReplacement(content, "failed");
        if (!queryFailed.isEmpty()) {
            return;
        }

        String exactMass = PatternFinder.searchWithoutReplacement(content, "Mass<\\/th>\\s*<td>[0-9]+(\\.[0-9]+)?");
        exactMass = PatternFinder.searchWithReplacement(exactMass, "[0-9]+(\\.[0-9]+)?", "");
        if (exactMass.equalsIgnoreCase("")) {
            exactMass = "null";
        } else {
            exactMass = PatternFinder.searchWithoutReplacement(exactMass, "[0-9]+(\\.[0-9]+)?");
        }

        String name = PatternFinder.searchWithoutReplacement(content, "<b>Name</b>(.*?)<font color=blue><b>(.*?)</b></font>");
        name = PatternFinder.searchWithReplacement(name, "<font color=blue><b>(.*?)</b></font>",
                "<font color=blue><b>|</b></font>");
        if (name.equalsIgnoreCase("")) {
            return;
        }
        name = name.replaceAll("\"", "''");
        name = "\"" + name.substring(0, name.length() - 1) + "\"";

        String formula = PatternFinder.searchWithoutReplacement(content, "<b>Formula</b></font></td>(.*?)</script>(.*?)</td>");
        formula = PatternFinder.searchWithReplacement(formula, "</script>(.*?)</td>", "</script>|</td>|<sub>|</sub>");
        if (formula.equalsIgnoreCase("")) {
            formula = "null";
        } else {
            formula = "\"" + formula.substring(0, formula.length() - 1) + "\"";
        }

        String cas = PatternFinder.searchWithoutReplacement(content, "<b>CAS</b></font></td>(.*?)</script>(.*?)</td>");
        cas = PatternFinder.searchWithReplacement(cas, "[0-9]+-[0-9]+-[0-9]+", "");
        if (cas.equalsIgnoreCase("")) {
            cas = "null";
        } else {
            cas = "\"" + cas.substring(0, cas.length() - 1) + "\"";
        }

        String insertion1 = "INSERT INTO metlin_compounds(metlin_id, compoundName, mass, formula) VALUES("
                + metlin_id + ", " + name + ", " + exactMass + ", " + formula + ") ON DUPLICATE KEY UPDATE metlin_id=" + metlin_id;

        System.out.println(insertion1);
        //db.executeIDU(insertion1);

        if (cas.equalsIgnoreCase("null")) {
            return;
        }

        String insertion2 = "INSERT INTO metlin_compounds_cas_ids(metlin_id, casIdentifier) VALUES("
                + metlin_id + ", " + cas + ") ON DUPLICATE KEY UPDATE metlin_id=" + metlin_id + ", casIdentifier=" + cas;

        System.out.println(insertion2);
        //db.executeIDU(insertion2);
    }

    public Object[] getValuesFromMetlin(int metlin_id) {

        Object[] result = new Object[4];
        String metlinFileName = METLINPATH + metlin_id + ".html";
        File metlinFile = new File(metlinFileName);
        if (!metlinFile.exists()) {
            try {
                Random randomGenerator = new Random();
                int randomInt;
                randomInt = randomGenerator.nextInt((50000 - 10000) + 1) + 10000;
                Thread.sleep(randomInt);                 //1000 milliseconds is one second.
            } catch (InterruptedException ex) {
                System.out.println("Thread interrupted");
                Thread.currentThread().interrupt();
            }
            downloadMetlinWebPage(metlin_id);
        }
        String content;
        content = MyFile.read(metlinFileName).toString();
        String queryFailed;
        queryFailed = PatternFinder.searchWithoutReplacement(content, "failed");
        boolean downloadInformation;
        downloadInformation = (!queryFailed.isEmpty() || (getName(content).equals("")));
        int tries=0;
        while (downloadInformation) {
            // Wait for accessing metlin web page
            try {
                Random randomGenerator = new Random();
                int randomInt;
                randomInt = randomGenerator.nextInt((60000 - 10000) + 1) + 60000;
                Thread.sleep(randomInt);                 //1000 milliseconds is one second.
            } catch (InterruptedException ex) {
                System.out.println("Thread interrupted");
                Thread.currentThread().interrupt();
            }
            metlinFile.delete();
            downloadMetlinWebPage(metlin_id);
            content = MyFile.read(metlinFileName).toString();
            queryFailed = PatternFinder.searchWithoutReplacement(content, "failed");
            tries++;
            downloadInformation = (!queryFailed.isEmpty() || (getName(content).equals("")) || tries > 4);
        }
        if(tries>4)
        {
            System.out.println("Metlin ID: " + metlin_id + " not possible to download");
        }
        
        String exactMass = getMass(content);
        if (!exactMass.equals("")) {
            exactMass = exactMass.substring(0, exactMass.length() - 1);
        }

        String name = getName(content);
        if (!name.equals("")) {
            name = name.replaceAll("\"", "''");
            name = name.substring(0, name.length() - 1);
        }
        String formula = getFormula(content);
        if (!formula.equals("")) {
            formula = formula.substring(0, formula.length() - 1);
        }
        String cas = getCAS(content);
        if (!cas.equals("")) {
            cas = cas.substring(0, cas.length() - 1);
        }
        //System.out.println(metlin_id + "  " + exactMass + "  " + name + "  " + formula + "  " + cas);
        result[0] = exactMass;
        result[1] = formula;
        result[2] = name;
        result[3] = cas;

        return result;

    }

    private void downloadMetlinWebPage(int metlinID) {
        StringBuilder content = IoDevice.read("https://metlin.scripps.edu/metabo_info.php?molid=" + metlinID);
        MyFile.write(content.toString(), METLINPATH + metlinID + ".html");
        //System.out.println(metlinID);

    }

    private String getMass(String content) {
        String mass = "";

        String auxiliarMass = PatternFinder.searchWithoutReplacement(content, "\">Mass<\\/th>\\s*<td>[0-9]+(\\.[0-9]+)?");
        auxiliarMass = PatternFinder.searchWithReplacement(auxiliarMass, "[0-9]+(\\.[0-9]+)?", "");
        mass = PatternFinder.searchWithoutReplacement(auxiliarMass, "[0-9]+(\\.[0-9]+)?");
        return mass;
    }

    private String getName(String content) {
        String name = "";

        String auxiliarName = PatternFinder.searchWithoutReplacement(content, "\">Name<\\/th>\\s*<td>(.*?)<\\/td>");
        name = PatternFinder.searchWithReplacement(auxiliarName, "\">Name<\\/th>\\s*<td>(.*?)<\\/td>",
                "\">Name<\\/th>\\s*<td>|<\\/td>");
        return name;
    }

    private String getFormula(String content) {
        String formula = "";

        String auxiliarFormula = PatternFinder.searchWithoutReplacement(content, "\">Formula<\\/th>\\s*<td>(.*?)<\\/td>");
        formula = PatternFinder.searchWithReplacement(auxiliarFormula, "\">Formula<\\/th>\\s*<td>(.*?)<\\/td>",
                "\">Formula<\\/th>\\s*<td>|<\\/td>");
        return formula;
    }

    private String getCAS(String content) {
        String CAS = "";

        String auxiliarCAS = PatternFinder.searchWithoutReplacement(content, "\">CAS<\\/th>\\s*<td>(.*?)<\\/td>");
        CAS = PatternFinder.searchWithReplacement(auxiliarCAS, "CAS<\\/th>\\s*<td>(.*?)<\\/td>",
                "CAS<\\/th>\\s*<td>|<\\/td>");
        return CAS;
    }

    public void populateFromMetlinCompound(String metlinCompound, CMMDatabase db) {
        // Search molID
        String molid = PatternFinder.searchWithReplacement(metlinCompound, "\"molid\":\"[0-9]+\"", "(molid|:|\"|\n)");
        molid = molid.replaceAll("\n", "");
        // Search mass
        String mass = PatternFinder.searchWithReplacement(metlinCompound, "\"mass\":\"[0-9]+\\.[0-9]+\"", "(mass|:|\"|\n)");
        mass = mass.replaceAll("\n", "");
        // Search name
        String name = PatternFinder.searchWithReplacement(metlinCompound, "\"name\":\"(.*?)\"", "(name\":\"|\"|\n)");
        name = name.replaceAll("\n", "");
        //name = name.replaceAll("\\\"","\\ \"");
        name = name.replaceAll("\\\\", "aaaa"); // La expresión \\\\ es para un sólo \
        name = name.replaceAll("aaaa", "\\\\\\\\"); // Esto es un truco para que no se desborde la pila
        // con el tratamiento de la expresión regular
        if (name.equalsIgnoreCase("")) {
            return;
        }

        // Search formula
        String formula = PatternFinder.searchWithReplacement(metlinCompound, "\"formula\":\"(.*?)\"", "(formula\":\"|\"|\n)");
        formula = formula.replaceAll("\n", "");

        /*     String insertion1 = "UPDATE metlin_compounds "+
                "SET compoundName=\"" + name + "\", mass=" + mass +", formula=\"" + formula + "\", "+ 
                "insertion_date = CURDATE() " +
                " WHERE metlin_id="+molid;  */
        String insertion1 = "INSERT INTO metlin_compounds(metlin_id, compoundName, mass, formula, insertion_date)"
                + " VALUES("
                + molid + ", \"" + name + "\", " + mass + ", \"" + formula + "\", insertion_date = CURDATE()) "
                + "ON DUPLICATE KEY UPDATE metlin_id=" + molid + ", compoundName=\"" + name + "\", mass=" + mass
                + ", formula=\"" + formula + "\", " + "insertion_date = CURDATE() ";

        System.out.println(insertion1);
        db.executeIDU(insertion1);

        // why it is not filled metlin_compounds_cas_ids?

        /*    if (cas.equalsIgnoreCase("null")) return;
        
        String insertion2 = "INSERT INTO metlin_compounds_cas_ids(metlin_id, casIdentifier) VALUES("+
                molid + ", " + cas+") ON DUPLICATE KEY UPDATE metlin_id="+metlin_id+ ", casIdentifier="+cas;
                * 
        
        System.out.println(insertion2);
        db.executeIDU(insertion2); */
    }

    public void populateFromADownloadedFile(String fileName, CMMDatabase db) {
        String fileNumber = fileName.replaceAll(".txt", "");
        //adapt to unix directory
        //String content = MyFile.read(".\\resources\\metlin\\"+fileNumber+".txt").toString();
        String content = MyFile.read(METLINPATH + fileNumber + ".txt").toString();
        System.out.println("Reading " + fileNumber);

        String queryFailed = PatternFinder.searchWithoutReplacement(content, "\\[\"\"\\]");
        if (!queryFailed.isEmpty()) {
            return;
        }

        List<String> metlinCompounds = PatternFinder.searchListWithReplacement(content, "\\{(.*?)\\}", "");
        if (metlinCompounds.isEmpty()) {
            return;
        }

        System.out.println(metlinCompounds);

        for (String metlinCompound : metlinCompounds) {
            populateFromMetlinCompound(metlinCompound, db);
        }

    }

    // Why to copy the view on the table? 
    // It is not more logic copy the view from the table?
    // Actually metlin_compounds_view is a table.
    /**
     *
     * @param db
     * @deprecated
     */
    public void poblarFastMetlinCompounds(CMMDatabase db) {
        String insertion = "INSERT INTO fast_metlin_compounds(metlin_id, compound_name, mass, formula, cas_identifier)"
                + " SELECT metlin_id, compound_name, mass, formula, cas_identifier"
                + " FROM metlin_compounds_view;";

        System.out.println(insertion);
        db.executeIDU(insertion);
    }

    /**
     *
     * @param db
     * @deprecated
     */
    public void populateFromMetlin(CMMDatabase db) {
        // adapt to unix
        //File dir = new File(".\\resources\\metlin");
        File dir = new File(METLINPATH);
        String[] ficheros = dir.list();

        for (String fichero : ficheros) {
            populateFromADownloadedFile(fichero, db);
        }

        poblarFastMetlinCompounds(db);
    }

}
