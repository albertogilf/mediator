/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databases;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author alberto
 */
public class AuxQueries {

    public static void main(String[] args) {
        CMMDatabase db = new CMMDatabase();
        db.connectToDB("jdbc:mysql://localhost/compounds?useSSL=false&serverTimezone=UTC", "alberto", "alberto");

        int CMMID = 0;
        String query = "select compound_id, compound_name, formula, mass from compounds where "
                + "compound_name like '%carnitin%'";
        ResultSet prov;
        try {
            prov = db.statement.executeQuery(query);
            while (prov.next()) {
                // Build the String with the information of the compound
                CMMID = prov.getInt(1);
                String compound_name = prov.getString(2);
                String formula = prov.getString(3);
                double mass = prov.getDouble(4);
                String separator = "|";
                Pattern p = Pattern.compile("C[0-9]*");
                Matcher m = p.matcher(formula);
                while (m.find()) {
                    String carbonsString = m.group();
                    carbonsString = carbonsString.substring(1);
                    int carbonNumbers = Integer.parseInt(carbonsString);
                    if (carbonNumbers <= 23) {
                        String csvLine = "" + CMMID + separator + compound_name + separator
                                + formula + separator + mass;
                        System.out.println(csvLine);
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
