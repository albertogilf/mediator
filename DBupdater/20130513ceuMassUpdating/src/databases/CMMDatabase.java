package databases;

import CE.MS.CEPrecursorIon;
import CE.MS.CEProductIon;
import exceptions.ErrorTypes;
import exceptions.IncorrectBufferException;
import exceptions.IncorrectIonizationModeException;
import exceptions.IncorrectPolarityException;
import exceptions.IncorrectTemperatureException;
import exceptions.IonModeAdductException;
import exceptions.NodeNotFoundException;
import exceptions.NotFoundException;
import exceptions.OrganismNotFoundException;
import exceptions.ReferenceException;
import fahfa.Fahfa;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.OrganismClassification;
import model.Reference;
import patternFinders.PatternFinder;
import utilities.Utilities;

/**
 *
 * @author: San Pablo-CEU, Alberto Gil de la Fuente
 * @version: 4.0, 20/07/2016
 */
public class CMMDatabase extends Database {

    /**
     * return true if the casId exists in the database or false otherwise
     *
     * @param casID
     * @return
     * @throws SQLException
     */
    public boolean existsCasID(String casID) throws SQLException // insertar, borrar o actualizar
    {
        int CMMID = 0;
        String query = "select compound_id from compounds where cas_id = '" + casID + "'";
        ResultSet prov = statement.executeQuery(query);
        if (prov.next()) {
            // Build the String with the information of the compound
            CMMID = prov.getInt(1);
            if (CMMID > 0) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * return the compoundId if CAS exists in the database. 0 otherwise
     *
     * @param casID
     * @return
     */
    public int getCompounIdFromCAS(String casID) {
        int CMMID = 0;
        String query = "select compound_id from compounds where cas_id = '" + casID + "'";
        ResultSet prov;
        try {
            prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                CMMID = prov.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return CMMID;
    }

    /**
     * return the compoundId if the CE Compound Id exist in the database. 0
     * otherwise
     *
     * @param cembioId
     * @return the compound Id of the CEMBIO Id, 0 otherwise
     */
    public int getCompounIdFromCembioID(Integer cembioId) {
        int CMMID = 0;
        String query = "select ce_compound_id from ce_eff_mob ce_eff_mob where "
                + "cembio_id = " + cembioId;
        ResultSet prov;
        try {
            prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                CMMID = prov.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return CMMID;
    }

    /**
     *
     * @param compound_id
     * @return
     */
    public int areThereAlreadyChains(int compound_id) // check if the chains are already inserted
    {
        int CMMID = 0;
        String query = "select compound_id from compound_chain where compound_id = " + compound_id;
        try {
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                CMMID = prov.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return CMMID;
    }

    /**
     * get the chain_id. If the chain ID does not exist, it returns 0
     *
     * @param numCarbons
     * @param numDoubleBonds
     * @param oxidation
     * @return
     */
    public int getChainID(int numCarbons, int numDoubleBonds, String oxidation) // check if the chains are already inserted
    {
        int chainID = 0;
        String query = "select chain_id from chains where num_carbons = " + numCarbons + " and double_bonds = "
                + numDoubleBonds + " and oxidation = '" + oxidation + "'";
        try {
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                chainID = prov.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return chainID;
    }

    /**
     * get the repetitions of the chain chain_id in the compound compound_id.
     *
     * @param compound_id
     * @param chain_id
     * @return
     */
    public int getRepetitionsSameChain(int compound_id, int chain_id) // check if the chains are already inserted
    {
        int number = 0;
        String query = "select number_chains from compound_chain where compound_id = " + compound_id + " and chain_id = "
                + chain_id;
        try {
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                number = prov.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return number;
    }

    public void executeNewIDU(String query) {
        Statement provStatement;
        try {
            provStatement = getConnection().createStatement();
            provStatement.executeUpdate(query);
            provStatement.close();

        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void executeNewIDUWithEx(String query) throws SQLException {
        Statement provStatement;
        provStatement = getConnection().createStatement();
        provStatement.executeUpdate(query);
        provStatement.close();
    }

    public void showQueryResult(int[] columns) {
        try {
            // ResultSet listed showing results in app server log
            while (rs.next()) {
                //System.out.println (rs.getString (1) + " " + rs.getString (2)+
                //rs.getString (3)+ " " + rs.getString (4));
                System.out.println(oneRowResult(columns));

            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String queryResultInString(int[] columns) {
        StringBuilder result = new StringBuilder();
        try {
            // ResultSet listed showing returning results in a string
            while (rs.next()) {
                //System.out.println (rs.getString (1) + " " + rs.getString (2)+
                //rs.getString (3)+ " " + rs.getString (4));
                result.append(oneRowResult(columns)).append("\n");

            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
            return null;
        }

        return result.toString();
    }

    public ArrayList<Object[]> queryResultInGrid(int[] columns) {
        ArrayList<Object[]> result = new ArrayList();
        try {

            Object[] row;
            // ResultSet listed showing returning results in a list
            while (rs.next()) {
                row = new Object[columns.length];
                for (int i = 0; i < columns.length; i++) {
                    row[i] = rs.getObject(columns[i]);
                }
                result.add(row);

            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
            return null;
        }

        return result;
    }

    private String oneRowResult(int[] columns) throws SQLException {
        String result = "";
        for (int column : columns) {
            result += rs.getString(column) + " ";
        }

        return result;
    }

    public void closeConnection() {
        try {
            getConnection().close();

        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param query
     * @return the ID of the query or 0 if the result is null
     */
    public int getInt(String query) {
        int result = 0;
        try {
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                result = prov.getInt(1);
                if (prov.wasNull()) {
                    result = 0;

                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     * getString Method process a query which retrieves the first field of it as
     * a String
     *
     * @param query
     * @return the information of the compound
     */
    public String getString(String query) {
        String result = "";
        try {
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                result = prov.getString(1);
                if (prov.wasNull()) {
                    result = "";

                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     *
     * @param casId cas Id of the compound. Can be null
     * @param name
     * @param formula
     * @param mass
     * @param chargeType 0 for neutral, 1 for positive, 2 for negative
     * @param numCharges integer positive
     * @param formulaType CHNOPS, CHNOPSCL or ALL
     * @param compoundType 0 for metabolite, 1 for lipid, 2 for peptide
     * @param compoundStatus 0 for expected, 1 for detected, 2 for quantified
     * and 3 for predicted
     * @param logP
     * @return
     */
    public int insertCompound(String casId, String name, String formula, Double mass,
            Integer chargeType, Integer numCharges, String formulaType,
            Integer compoundType, Integer compoundStatus, Double logP) {
        String massString;
        String logPString;
        if (mass == null) {
            massString = "null";
        } else {
            massString = mass.toString();
        }
        if (logP == null) {
            logPString = "null";
        } else {
            logPString = logP.toString();
        }

        return this.insertCompound(casId, name, formula, massString, chargeType,
                numCharges, formulaType, compoundType, compoundStatus, logPString);
    }

    /**
     *
     * @param casId cas Id of the compound. Can be null
     * @param name
     * @param formula
     * @param mass
     * @param chargeType 0 for neutral, 1 for positive, 2 for negative
     * @param numCharges integer positive
     * @param formulaType CHNOPS, CHNOPSCL or ALL
     * @param compoundType 0 for metabolite, 1 for lipid, 2 for peptide
     * @param compoundStatus 0 for expected, 1 for detected, 2 for quantified
     * and 3 for predicted
     * @param logP
     * @return
     */
    public int insertCompound(String casId, String name, String formula, String mass,
            Integer chargeType, Integer numCharges, String formulaType,
            Integer compoundType, Integer compoundStatus, String logP) {
        if (casId == null || casId.equalsIgnoreCase("null") || casId.equals("")) {
            casId = "NULL";
        } else {
            casId = "'" + casId + "'";
        }
        if (name == null || name.equalsIgnoreCase("null") || name.equals("")) {
            name = "NULL";
        } else {
            name = "\"" + name + "\"";
        }
        if (formula == null || formula.equalsIgnoreCase("null") || formula.equals("")) {
            formula = "NULL";
        } else {
            formula = "'" + formula + "'";
        }
        if (mass == null || mass.equalsIgnoreCase("null") || mass.equals("")) {
            mass = "NULL";
        }
        if (formulaType.equalsIgnoreCase("null") || formulaType.equals("")) {
            formulaType = "ALL";
        }
        if (logP == null || logP.equalsIgnoreCase("null") || logP.equals("")) {
            logP = "NULL";
        }
        int formulaTypeInt = Utilities.getIntChemAlphabet(formulaType);
        formulaType = "'" + formulaType + "'";

        String insertion = "INSERT IGNORE INTO compounds(cas_id, compound_name, formula, mass, "
                + "charge_type, charge_number, formula_type, formula_type_int, "
                + "compound_type, compound_status, logP) "
                + "VALUES(" + casId + ", " + name + ", " + formula + ", " + mass + ", "
                + chargeType + ", " + numCharges + ", " + formulaType + ", " + formulaTypeInt
                + ", " + compoundType + ", " + compoundStatus + ", " + logP + ")";
        //System.out.println(insertion);
        return this.executeNewInsertion(insertion);
    }

    /**
     * getformulaFromCompound Method process a query which retrieves the formula
     * of compound_id
     *
     * @param compound_id
     * @return the information of the compound
     */
    public String getformulaFromCompound(int compound_id) {
        String result = "";
        try {
            String query = "select formula from compounds where compound_id = " + compound_id;
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                result = prov.getString(1);
                if (prov.wasNull()) {
                    result = "null";

                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     * getMassFromCompound Method process a query which retrieves the mass
     * (double) of compound_id
     *
     * @param compound_id
     * @return the information of the compound
     */
    public double getMassFromCompound(int compound_id) {
        double result = 0;
        try {
            String query = "select mass from compounds where compound_id = " + compound_id;
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                result = prov.getDouble(1);
                if (prov.wasNull()) {
                    result = 0.0d;

                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     * getCasFromCompound Method process a query which retrieves the cas
     * identifier of compound_id
     *
     * @param compound_id
     * @return the information of the compound
     */
    public String getCasFromCompound(int compound_id) {
        String result = "";
        try {
            String query = "select cas_id from compounds where compound_id = " + compound_id;
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                result = prov.getString(1);
                if (prov.wasNull()) {
                    result = "";

                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     *
     * @param query
     * @return
     */
    public ResultSet getResultSet(String query) {
        ResultSet prov = null;
        try {
            prov = statement.executeQuery(query);

        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return prov;
    }

    /**
     * getDouble Method process a query which retrieves the first field of it as
     * a Double
     *
     * @param query
     * @return the information of the compound
     */
    public double getDouble(String query) {
        double result = 0f;
        try {
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                result = prov.getDouble(1);

            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     * Method process the query about a compound and retrieves his information
     *
     * @param query
     * @return the information of the compound
     */
    public String getInformationCompound(String query) {
        String result = "";
        try {
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                int compound_id = prov.getInt(1);
                String cas_id = prov.getString(2);
                if (cas_id == null) {
                    cas_id = "NO CAS IDENTIFIER";
                }
                String compound_name = prov.getString(3);
                String formula = prov.getString(4);
                double mass = prov.getDouble(5);
                result = "compound_id: " + compound_id + "\ncas_id: " + cas_id
                        + "\ncompound_name: " + compound_name + "\nformula: " + formula
                        + "\nmass: " + mass;
                // Look for the compound in all databases
                // KEGG
                String newQuery;
                newQuery = "select * from compounds_kegg where compound_id=" + compound_id;
                prov = statement.executeQuery(newQuery);
                result = result + "\n" + "kegg ids: ";
                while (prov.next()) {
                    String kegg_id = prov.getString(2);
                    result = result + "\n" + kegg_id;
                }
                //METLIN
                newQuery = "select * from compounds_agilent where compound_id=" + compound_id;
                prov = statement.executeQuery(newQuery);
                result = result + "\n" + "metlin ids: ";
                while (prov.next()) {
                    int metlin_id = prov.getInt(2);
                    result = result + "\n" + metlin_id;
                }
                //LipidMaps
                newQuery = "select * from compounds_lm where compound_id=" + compound_id;
                prov = statement.executeQuery(newQuery);
                result = result + "\n" + "lipid maps ids: ";
                while (prov.next()) {
                    String lm_id = prov.getString(2);
                    result = result + "\n" + lm_id;
                }
                //HMDB
                newQuery = "select * from compounds_hmdb where compound_id=" + compound_id;
                prov = statement.executeQuery(newQuery);
                result = result + "\n" + "hmdb ids: ";
                while (prov.next()) {
                    String hmdb_id = prov.getString(2);
                    result = result + "\n" + hmdb_id;
                }
                //PUBCHEM
                newQuery = "select * from compounds_pc where compound_id=" + compound_id;
                prov = statement.executeQuery(newQuery);
                result = result + "\n" + "pub chemical ids: ";
                while (prov.next()) {
                    int pc_id = prov.getInt(2);
                    result = result + "\n" + pc_id;

                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     * This method update the compound if the name, formula or mass was not set
     * before
     *
     * @param compound_id
     * @param cas_id
     * @param compound_name
     * @param formula
     * @param mass
     */
    public void updateCompound(int compound_id, String cas_id, String compound_name,
            String formula, String mass) {
        String query = "SELECT * FROM compounds where compound_id =" + compound_id;
        try {
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                String old_cas_id = prov.getString(2);
                String old_compound_name = prov.getString(3);
                String old_formula = prov.getString(4);
                String old_mass = prov.getString(5);
                if (!cas_id.equalsIgnoreCase("NULL") && old_cas_id == null) {
                    // UPDATE cas_id
                    String update = "UPDATE compounds set cas_id =" + cas_id
                            + " where compound_id=" + compound_id;
                    executeIDU(update);
                }
                if (!compound_name.equalsIgnoreCase("NULL") && old_compound_name == null) {
                    // UPDATE compound_name
                    String update = "UPDATE compounds set compound_name =" + compound_name
                            + " where compound_id=" + compound_id;
                    executeIDU(update);
                }
                if (!formula.equalsIgnoreCase("NULL") && old_formula == null) {
                    // UPDATE formula                    
                    String update = "UPDATE compounds set formula =" + formula
                            + " where compound_id=" + compound_id;
                    executeIDU(update);
                }
                if (!mass.equalsIgnoreCase("NULL") && (old_mass == null || old_mass.length() < mass.length())) {
                    // UPDATE mass
                    String update = "UPDATE compounds set mass =" + mass
                            + " where compound_id=" + compound_id;
                    executeIDU(update);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This method update the cas of the compound compound_id
     *
     * @param compound_id
     * @param cas_id
     */
    public void updateCompoundCAS(int compound_id, String cas_id) {
        String update;
        if (cas_id == null || cas_id.equalsIgnoreCase("null")
                || cas_id.equals("")) {
            cas_id = "NULL";
        } else {
            cas_id = "'" + cas_id + "'";
        }
        update = "update compounds set cas_id = " + cas_id + " WHERE compound_id = " + compound_id;
        this.executeNewIDU(update);
    }

    /**
     * This method update the mass of the compound compound_id
     *
     * @param compound_id
     * @param mass
     */
    public void updateCompoundMass(int compound_id, Double mass) {
        if (!mass.isNaN()) {
            String massString = mass.toString();
            this.updateCompoundMass(compound_id, massString);
        }
    }

    /**
     * This method update the mass of the compound compound_id
     *
     * @param compound_id
     * @param mass
     */
    public void updateCompoundMass(int compound_id, String mass) {
        String update;
        double massDouble;
        try {
            massDouble = Double.parseDouble(mass);

            if (!Double.isNaN(Double.parseDouble(mass)) && !mass.equalsIgnoreCase("null")
                    && !mass.equals("") && Double.parseDouble(mass) > 1d) {
                update = "update compounds set mass = " + mass + " WHERE compound_id = " + compound_id;
                this.executeNewIDU(update);
            }
        } catch (NumberFormatException nfe) {
        }
    }

    /**
     *
     * @param compoundId
     * @param mesh_nomenclature
     * @param iupac_classification
     */
    public void updateCompoundAspergillus(Integer compoundId, String mesh_nomenclature, String iupac_classification) {
        this.updateCompoundAspergillus(compoundId, mesh_nomenclature, iupac_classification, null);
    }

    /**
     *
     * @param compoundId
     * @param mesh_nomenclature
     * @param iupac_classification
     * @param aspergillus_web_name
     */
    public void updateCompoundAspergillus(Integer compoundId, String mesh_nomenclature, String iupac_classification, String aspergillus_web_name) {
        this.updateCompoundAspergillus(compoundId, mesh_nomenclature, iupac_classification, aspergillus_web_name, null);
    }

    /**
     *
     * @param compoundId
     * @param mesh_nomenclature
     * @param iupac_classification
     * @param aspergillus_web_name
     * @param biological_activity
     */
    public void updateCompoundAspergillus(Integer compoundId, String mesh_nomenclature, String iupac_classification, String aspergillus_web_name, String biological_activity) {
        String query = "SELECT biological_activity, mesh_nomenclature, iupac_classification, aspergillus_web_name "
                + "FROM compounds_aspergillus where compound_id =" + compoundId;
        try {
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                String old_biological_activity = prov.getString(1);
                String old_mesh_nomenclature = prov.getString(2);
                String old_iupac_classification = prov.getString(3);
                String old_aspergillus_web_name = prov.getString(4);
                if (biological_activity != null && old_biological_activity == null) {
                    // UPDATE biological_activity
                    String update = "UPDATE compounds_aspergillus set biological_activity  = '" + biological_activity + "' "
                            + " where compound_id=" + compoundId;
                    executeIDU(update);
                }
                if (mesh_nomenclature != null && old_mesh_nomenclature == null) {
                    // UPDATE compound_name
                    // UPDATE biological_activity
                    String update = "UPDATE compounds_aspergillus set mesh_nomenclature  = '" + mesh_nomenclature + "' "
                            + " where compound_id=" + compoundId;
                    executeIDU(update);
                }
                if (iupac_classification != null && old_iupac_classification == null) {
                    // UPDATE formula                    
                    // UPDATE biological_activity
                    String update = "UPDATE compounds_aspergillus set iupac_classification  = '" + iupac_classification + "' "
                            + " where compound_id=" + compoundId;
                    executeIDU(update);
                }
                if (aspergillus_web_name != null && old_aspergillus_web_name == null) {
                    // UPDATE mass
                    // UPDATE biological_activity
                    String update = "UPDATE compounds_aspergillus set aspergillus_web_name  = '" + aspergillus_web_name + "' "
                            + " where compound_id=" + compoundId;
                    executeIDU(update);
                }
            } else {
                if (compoundId > 0) {
                    this.insertCompoundAspergillus(compoundId, biological_activity, mesh_nomenclature, iupac_classification, aspergillus_web_name);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This method update the name of the compound compound_id
     *
     * @param compound_id
     * @param name
     */
    public void updateCompoundName(int compound_id, String name) {
        String update;
        if (name == null || name.equalsIgnoreCase("null")
                || name.equals("")) {
            name = "NULL";
        } else {
            name = Utilities.escapeSQL(name);
            name = "'" + name + "'";
            update = "update compounds set compound_name = " + name + " WHERE compound_id = " + compound_id;
            //System.out.println(update);
            this.executeNewIDU(update);
        }
    }

    /**
     * This method update the charges of the compound compound_id
     *
     * @param compound_id
     * @param chargeType
     * @param chargeNumber
     */
    public void updateCompoundCharge(int compound_id, int chargeType, int chargeNumber) {
        String update;

        update = "update compounds set charge_type = " + chargeType
                + ", charge_number = " + chargeNumber
                + " WHERE compound_id = " + compound_id;
        this.executeNewIDU(update);

    }

    public void updateCompoundType(int compound_id, int compound_type) {
        String update;
        try {
            if (compound_type >= 0 && compound_type <= 2) {
                update = "update compounds set compound_type = " + compound_type + " WHERE compound_id = " + compound_id;
                this.executeNewIDU(update);
            }
        } catch (NumberFormatException nfe) {
        }
    }

    /**
     * This method update the status of the compound compound_id
     *
     * @param compound_id
     * @param status
     */
    public void updateCompoundStatus(int compound_id, int status) {
        int previousStatus = -1;
        try {
            String select = "select compound_status from compounds "
                    + " WHERE compound_id = " + compound_id;
            ResultSet prov = statement.executeQuery(select);
            if (prov.next()) {
                // Build the String with the information of the compound
                previousStatus = prov.getInt(1);
                if (prov.wasNull()) {
                    previousStatus = -1;

                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        if (status != -1 && status != previousStatus) {
            String update = "update compounds set compound_status = " + status
                    + " WHERE compound_id = " + compound_id;
            this.executeNewIDU(update);
        }
    }

    /**
     * This method update the cas of the compound compound_id
     *
     * @param compound_id
     * @param formula
     */
    public void updateCompoundFormula(int compound_id, String formula) {
        String update;
        if (formula == null || formula.equalsIgnoreCase("null")
                || formula.equals("")) {
            formula = "NULL";
        } else {
            formula = "'" + formula + "'";
            update = "update compounds set formula = " + formula + " WHERE compound_id = " + compound_id;
            this.executeNewIDU(update);
        }

    }

    /**
     * Get the compoundId of the HMDBID
     *
     * @param hmdbId
     * @return 0 if HMDBID does not exist
     */
    public Integer getCompoundIdFromHMDBID(String hmdbId) {
        int compoundId = 0;
        try {
            String query = "select compound_id from compounds_hmdb where hmdb_id = '" + hmdbId + "'";
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                compoundId = prov.getInt(1);
                if (prov.wasNull()) {
                    compoundId = 0;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return compoundId;
    }

    /**
     * Get the compoundId of the LMID.
     *
     * @param LMId
     * @return 0 if LMID does not exist
     */
    public Integer getCompoundIdFromLMId(String LMId) {
        int compoundId = 0;
        try {
            String query = "select compound_id from compounds_lm where lm_id = '" + LMId + "'";
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                compoundId = prov.getInt(1);
                if (prov.wasNull()) {
                    compoundId = 0;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return compoundId;
    }

    /**
     * Get the compoundId of the LMID.
     *
     * @param keggId
     * @return 0 if LMID does not exist
     */
    public Integer getCompoundIdFromKeggId(String keggId) {
        int compoundId = 0;
        try {
            String query = "select compound_id from compounds_kegg where kegg_id = '" + keggId + "'";
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                compoundId = prov.getInt(1);
                if (prov.wasNull()) {
                    compoundId = 0;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return compoundId;
    }

    /**
     *
     * @param compound_id
     * @return
     */
    public boolean checkAspergillusExist(Integer compound_id) {
        int compoundId = 0;
        boolean result = false;
        try {
            String query = "select compound_id from compounds_aspergillus where compound_id = " + compound_id;
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                compoundId = prov.getInt(1);
                if (prov.wasNull()) {
                    compoundId = 0;
                    return false;
                }
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    /**
     * Get the compoundId of the AgilentId.
     *
     * @param agilentId
     * @return 0 if LMID does not exist
     */
    public Integer getCompoundIdFromAgilentId(String agilentId) {
        int compoundId = 0;
        try {
            String query = "select compound_id from compounds_agilent where agilent_id = '" + agilentId + "'";
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                compoundId = prov.getInt(1);
                if (prov.wasNull()) {
                    compoundId = 0;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return compoundId;
    }

    /**
     * Insert the structures of the compound Id in the database
     *
     * @param compoundId
     * @param inChI
     * @param inChIKey
     * @param SMILES
     */
    public void insertIdentifiers(int compoundId, String inChI, String inChIKey, String SMILES) {
        if (this.getInChIKeyFromCompound(compoundId).equals("")) {
            if (inChI != null && SMILES != null && inChIKey != null
                    && !inChI.equals("") && !inChIKey.equals("") && !SMILES.equals("")) {
                try {
                    inChI = Utilities.escapeSQL(inChI);
                    SMILES = Utilities.escapeSQL(SMILES);
                    String insertion = "INSERT IGNORE INTO "
                            + "compound_identifiers(compound_id, inchi, inchi_key, smiles) VALUES("
                            + compoundId + ", '" + inChI + "', '" + inChIKey + "', \"" + SMILES + "\")";
                    this.executeNewIDUWithEx(insertion);
                    //System.out.println(insertion);
                } catch (SQLException ex) {
                    System.out.println("Identifier not inserted -> compoundId: " + compoundId
                            + " InCHI: " + inChI);
                    Logger.getLogger(CMMDatabase.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    /**
     * IUpdate the SMILES of the compound Id in the database
     *
     * @param compoundId
     * @param SMILES
     */
    public void updateSMILESIdentifier(int compoundId, String SMILES) {
        if (SMILES != null && !SMILES.equals("")) {
            SMILES = Utilities.escapeSQL(SMILES);
            String insertion = "UPDATE "
                    + "compound_identifiers set smiles = \"" + SMILES + "\" where compound_id =" + compoundId;
            this.executeNewIDU(insertion);
            //System.out.println(insertion);
        }
    }

    /**
     * Insert the classification from lipidMaps. LM Classification has 4 levels.
     *
     * @param compoundId
     * @param codeCategory
     * @param codeMainClass
     * @param codeSubClass
     * @param codeLevel4Class
     */
    public void insertLMClassification(int compoundId, String codeCategory, String codeMainClass,
            String codeSubClass, String codeLevel4Class) {
        if (codeCategory != null && !codeCategory.equals("") && !codeCategory.equalsIgnoreCase("null")) {
            String insertion = "INSERT IGNORE INTO compounds_lm_classification(compound_id, "
                    + "category, main_class, sub_class, class_level4) "
                    + "VALUES(" + compoundId + ", '" + codeCategory + "', '"
                    + codeMainClass + "', '" + codeSubClass + "', '" + codeLevel4Class + "')";
            //System.out.println("INSERTION: " + insertion);
            this.executeNewInsertion(insertion);
        }
    }

    /**
     * Insert the classification for the lipids. It save the lipidType, the num
     * of chains, the num of carbons and the num of double bonds.
     *
     * @param compoundId
     * @param lipidType
     * @param numChains
     * @param numCarbons
     * @param numDoubleBonds
     */
    public void insertLipidClassification(int compoundId, String lipidType, int numChains,
            int numCarbons, int numDoubleBonds) {
        if (lipidType != null && !lipidType.equals("") && !lipidType.equalsIgnoreCase("null")) {
            String insertion = "INSERT IGNORE INTO compounds_lipids_classification(compound_id, "
                    + "lipid_type, num_chains, number_carbons, double_bonds) "
                    + "VALUES(" + compoundId + ", '" + lipidType + "', " + numChains + ", "
                    + numCarbons + ", " + numDoubleBonds + ")";
            this.executeNewInsertion(insertion);
            //System.out.println(insertion);
        }
    }

    /**
     * Insert the classification for the lipids. It save the lipidType, the num
     * of chains, the num of carbons and the num of double bonds.
     *
     * @param compoundId
     * @param lipidType
     * @param numChains
     * @param numCarbons
     * @param numDoubleBonds
     */
    public void updateLipidClassification(int compoundId, String lipidType, int numChains,
            int numCarbons, int numDoubleBonds) {
        if (lipidType != null && !lipidType.equals("") && !lipidType.equalsIgnoreCase("null")) {
            String insertion = "UPDATE compounds_lipids_classification set "
                    + "lipid_type = '" + lipidType + "', num_chains = " + numChains
                    + ", number_carbons=" + numCarbons + ", double_bonds=" + numDoubleBonds
                    + " where compound_id =" + compoundId + ";";
            this.executeNewInsertion(insertion);
            //System.out.println(insertion);
        }
    }

    /**
     * Insert the relation between compound_id and hmdb_id
     *
     * @param compoundId
     * @param hmdbId
     */
    public void insertCompoundHMDB(int compoundId, String hmdbId) {
        if (hmdbId != null && !hmdbId.equals("") && !hmdbId.equalsIgnoreCase("null")) {
            String insertion = "INSERT IGNORE INTO "
                    + "compounds_hmdb(compound_id, hmdb_id) VALUES("
                    + compoundId + ", '" + hmdbId + "')";
            this.executeNewIDU(insertion);
            //System.out.println(insertion);
        }
    }

    /**
     * Insert the relation between compound_id and hmdb_id
     *
     * @param compoundId
     * @param chebiId
     */
    public void insertCompoundCHEBI(int compoundId, Integer chebiId) {
        if (chebiId != null) {
            String insertion = "INSERT IGNORE INTO "
                    + "compounds_chebi(compound_id, chebi_id) VALUES("
                    + compoundId + ", " + chebiId + ")";
            this.executeNewIDU(insertion);
            //System.out.println(insertion);
        }
    }

    /**
     * Insert the relation between compound_id and lm_id
     *
     * @param compoundId
     * @param lmId
     */
    public void insertCompoundLM(int compoundId, String lmId) {
        if (lmId != null && !lmId.equals("") && !lmId.equalsIgnoreCase("null")) {
            String insertion = "INSERT IGNORE INTO "
                    + "compounds_lm(compound_id, lm_id) VALUES("
                    + compoundId + ", '" + lmId + "')";
            this.executeNewIDU(insertion);
            //System.out.println(insertion);
        }
    }

    /**
     * Delete the relation between compound_id and lm_id
     *
     * @param compoundId
     * @param lmId
     */
    public void deleteCompoundLM(int compoundId, String lmId) {
        if (lmId != null && !lmId.equals("") && !lmId.equalsIgnoreCase("null")) {
            String insertion = "delete FROM "
                    + "compounds_lm where lm_id= "
                    + "'" + lmId + "' and compound_id = " + compoundId;
            this.executeNewIDU(insertion);
            //System.out.println(insertion);
        }
    }

    /**
     * Delete the relation between compound_id and hmdb_id
     *
     * @param compoundId
     * @param hmdbId
     */
    public void deleteCompoundHMDB(int compoundId, String hmdbId) {
        if (hmdbId != null && !hmdbId.equals("") && !hmdbId.equalsIgnoreCase("null")) {
            String insertion = "delete FROM "
                    + "compounds_hmdb where hmdb_id= "
                    + "'" + hmdbId + "' and compound_id = " + compoundId;
            this.executeNewIDU(insertion);
            //System.out.println(insertion);
        }
    }

    /**
     * Insert the relation between compound_id and lm_id
     *
     * @param compoundId
     * @param keggId
     */
    public void insertCompoundKegg(int compoundId, String keggId) {
        if (keggId != null && !keggId.equals("") && !keggId.equalsIgnoreCase("null")) {
            String insertion = "INSERT IGNORE INTO "
                    + "compounds_kegg(compound_id, kegg_id) VALUES("
                    + compoundId + ", '" + keggId + "')";
            this.executeNewIDU(insertion);
            //System.out.println(insertion);
        }
    }

    /**
     * Insert the relation between compound_id and lm_id
     *
     * @param compoundId
     * @param agilentId
     */
    public void insertCompoundAgilent(int compoundId, String agilentId) {
        if (agilentId != null && !agilentId.equals("") && !agilentId.equalsIgnoreCase("null")) {
            String insertion = "INSERT IGNORE INTO "
                    + "compounds_agilent(compound_id, agilent_id) VALUES("
                    + compoundId + ", '" + agilentId + "')";
            this.executeNewIDU(insertion);
            //System.out.println(insertion);
        }
    }

    /**
     * Insert the relation between compound_id and pubChemId
     *
     * @param compoundId
     * @param pubChemId
     */
    public void insertCompoundPC(int compoundId, int pubChemId) {
        if (compoundId > 0 && pubChemId > 0) {
            String insertion = "INSERT IGNORE INTO "
                    + "compounds_pc(compound_id, pc_id) VALUES("
                    + compoundId + ", " + pubChemId + ")";
            this.executeNewIDU(insertion);
            //System.out.println(insertion);
        }
    }

    /**
     * Insert the relation between compound_id and aspergillus
     *
     * @param compoundId
     * @param biological_activity
     * @param mesh_nomenclature
     * @param iupac_classification
     * @param aspergillus_link
     */
    public void insertCompoundAspergillus(int compoundId, String biological_activity,
            String mesh_nomenclature, String iupac_classification, String aspergillus_link) {
        if (compoundId > 0) {
            if (biological_activity == null || biological_activity.equalsIgnoreCase("null")
                    || biological_activity.equals("")) {
                biological_activity = "NULL";
            } else {

                biological_activity = Utilities.escapeSQL(biological_activity);
                biological_activity = "'" + biological_activity + "'";
            }
            if (aspergillus_link == null || aspergillus_link.equalsIgnoreCase("null")
                    || aspergillus_link.equals("")) {
                aspergillus_link = "NULL";
            } else {
                aspergillus_link = Utilities.escapeSQL(aspergillus_link);
                aspergillus_link = "'" + aspergillus_link + "'";
            }
            if (mesh_nomenclature == null || mesh_nomenclature.equalsIgnoreCase("null")
                    || mesh_nomenclature.equals("")) {
                mesh_nomenclature = "NULL";
            } else {
                mesh_nomenclature = Utilities.escapeSQL(mesh_nomenclature);
                mesh_nomenclature = "'" + mesh_nomenclature + "'";
            }
            if (iupac_classification == null || iupac_classification.equalsIgnoreCase("null")
                    || iupac_classification.equals("")) {
                iupac_classification = "NULL";
            } else {
                iupac_classification = Utilities.escapeSQL(iupac_classification);
                iupac_classification = "'" + iupac_classification + "'";
            }

            String insertion = "INSERT IGNORE INTO "
                    + "compounds_aspergillus(compound_id, biological_activity, "
                    + "mesh_nomenclature, iupac_classification, aspergillus_web_name) VALUES("
                    + compoundId + ", " + biological_activity + ", " + mesh_nomenclature + ", " + iupac_classification + ", " + aspergillus_link + ")";
            this.executeNewIDU(insertion);
            //System.out.println(insertion);
        }
    }

    /**
     * Insert the relation between inHouse Id and compound Id in the database
     *
     * @param compoundId
     * @param inHouseId
     * @param sourceData
     * @param description
     */
    public void insertCompoundInHouse(int compoundId, int inHouseId, String sourceData, String description) {
        if (compoundId > 0 && inHouseId > 0) {
            String insertion = "INSERT IGNORE INTO compounds_in_house(compound_id,in_house_id,"
                    + "source_data,description) VALUES("
                    + compoundId + ", " + inHouseId + ", '" + sourceData + "','" + description + "')";
            this.executeNewIDU(insertion);
            //System.out.println(insertion);
        }
    }

    /**
     * Insert the relation between compound_id and FAHFA Compounds
     *
     * @param fahfa_compound
     */
    public void insertCompoundFahfa(Fahfa fahfa_compound) {
        int compound_id = fahfa_compound.getCompound_id();
        int fahfa_id = fahfa_compound.getFahfa_id();
        int oh_position = fahfa_compound.getOh_position();
        if (compound_id > 0 && fahfa_id > 0) {
            String insertion = "INSERT IGNORE INTO "
                    + "compounds_fahfa(compound_id, fahfa_id, "
                    + "oh_position) VALUES("
                    + compound_id + ", " + fahfa_id + ", " + oh_position + ")";
            this.executeNewIDU(insertion);
            //System.out.println(insertion);
        }
    }

    /**
     * Insert the relation between inHouse Id and compound Id in the database
     * return the next id for in_house_id based on the last ID.
     *
     * @return
     */
    public int getNextInHouseID() {
        int nextInHouseId;
        String insertion = "SELECT MAX(id) from compounds_in_house_seq";
        nextInHouseId = this.getInt(insertion) + 1;
        return nextInHouseId;
    }

    /**
     * Insert a chain in the database
     *
     * @param numCarbons
     * @param numDoubleBonds
     * @param oxidation
     * @param mass
     * @param formula
     * @return
     */
    public int insertChain(int numCarbons, int numDoubleBonds, String oxidation, Double mass, String formula) {
        return this.insertChain(numCarbons, numDoubleBonds, oxidation, Double.toString(mass), formula);
    }

    /**
     * Insert a chain in the database
     *
     * @param numCarbons
     * @param numDoubleBonds
     * @param oxidation
     * @param mass
     * @param formula
     * @return
     */
    public int insertChain(int numCarbons, int numDoubleBonds, String oxidation, String mass, String formula) {
        if (!Float.isNaN(Float.parseFloat(mass)) && Float.parseFloat(mass) > 0
                && oxidation != null && !oxidation.equals("") && !oxidation.equalsIgnoreCase("null")
                && formula != null && !formula.equals("") && !formula.equalsIgnoreCase("null")) {
            String insertion = "INSERT IGNORE INTO chains(num_carbons, "
                    + "double_bonds, oxidation, mass, formula) "
                    + "VALUES(" + numCarbons + ", " + numDoubleBonds + ", '"
                    + oxidation + "', " + mass + ",'" + formula + "')";
            return this.executeNewInsertion(insertion);
            //System.out.println(insertion);
        }
        return 0;
    }

    /**
     * Insert the relation between the Chain Id and compound Id in the database
     *
     * @param compoundId
     * @param chainId
     * @param numChains
     */
    public void insertCompoundChainRelation(int compoundId, int chainId, int numChains) {
        if (compoundId > 0 && chainId > 0) {
            String insertion = "INSERT IGNORE INTO compound_chain(compound_id, "
                    + "chain_id, number_chains) "
                    + "VALUES(" + compoundId + ", " + chainId + ", " + numChains + " )";
            this.executeNewInsertion(insertion);
            //System.out.println(insertion);
        }
    }

    /**
     * Delete the relation between the compound and the chains
     *
     * @param compoundId
     */
    public void deleteCompoundChainRelationCompoundId(int compoundId) {
        if (compoundId > 0) {
            String insertion = "DELETE FROM compound_chain where compound_id = " + compoundId;
            this.executeNewInsertion(insertion);
            //System.out.println(insertion);
        }
    }

    /**
     * Insert the relation between the Chain Id and compound Id in the database
     *
     * @param compoundId
     * @param chainId
     * @param numChains
     */
    public void updateCompoundChainRepetitions(int compoundId, int chainId, int numChains) {
        if (compoundId > 0 && chainId > 0) {
            String update = "UPDATE compound_chain set number_chains = " + numChains
                    + " WHERE compound_id = " + compoundId + " and chain_id = " + chainId;
            this.executeNewIDU(update);
        }
    }

    /**
     * getInChIFromCompound Method process a query which retrieves the INCHI
     * identifier of compound_id
     *
     * @param compound_id
     * @return the inchikey
     */
    public String getInChIFromCompound(int compound_id) {
        String result = "";
        try {
            String query = "select inchi from compound_identifiers where compound_id = " + compound_id;
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                result = prov.getString(1);
                if (prov.wasNull()) {
                    result = "";

                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     * getInChIKeyFromCompound Method process a query which retrieves the INCHI
     * KEY identifier of compound_id
     *
     * @param compound_id
     * @return the inchikey
     */
    public String getInChIKeyFromCompound(int compound_id) {
        String result = "";
        try {
            String query = "select inchi_key from compound_identifiers where compound_id = " + compound_id;
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                result = prov.getString(1);
                if (prov.wasNull()) {
                    result = "";

                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     * retrieve the smiles from the database
     *
     * @param compound_id
     * @return the information of the compound
     */
    public String getSMILESFromCompound(int compound_id) {
        String result = "";
        try {
            String query = "select smiles from compound_identifiers where compound_id = " + compound_id;
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                result = prov.getString(1);
                if (prov.wasNull()) {
                    result = "null";

                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     * retrieve the smiles from the database based on the inchi
     *
     * @param inchi
     * @return the information of the compound
     */
    public String getSMILESFromInChI(String inchi) {
        String result = "";
        try {
            String query = "select smiles from compound_identifiers where inchi = '" + inchi
                    + "'";
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                result = prov.getString(1);
                if (prov.wasNull()) {
                    result = "null";
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     * retrieve the compound Id from the inchikey
     *
     * @param inchiKey of the compound to search
     * @return the compound id of the inchikey. 0 if the compound was not found
     */
    public int getCompoundIdFromInchiKey(String inchiKey) {
        int result = 0;
        try {
            String query = "select compound_id from compound_identifiers where inchi_key = '" + inchiKey + "'";
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                result = prov.getInt(1);
                if (prov.wasNull()) {
                    result = 0;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     * retrieve the compoundId from the inchi
     *
     * @param inchi of the compound to search, 0 otherwise
     * @return the information of the compound
     */
    public int getCompoundIdFromInchi(String inchi) {
        int result = 0;
        try {
            String query = "select compound_id from compound_identifiers where inchi = '" + inchi + "'";
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                result = prov.getInt(1);
                if (prov.wasNull()) {
                    result = 0;

                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     * retrieve the compoundId from the compound_name
     *
     * @param compoundName of the compound to search
     * @return the information of the compound
     */
    public int getCompoundIdFromCompoundName(String compoundName) {
        int result = 0;

        String query = "select compound_id from compounds where compound_name = \"" + compoundName + "\"";
        try {
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                result = prov.getInt(1);
                if (prov.wasNull()) {
                    result = 0;

                }
            }
        } catch (SQLSyntaxErrorException ex) {
            System.out.println(ex);
            System.out.println(query);

        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     * retrieve the compoundId from the compound_name
     *
     * @param compoundName of the compound to search
     * @return the information of the compound
     */
    public Set<Integer> getCompoundIdsSetFromName(String compoundName) {
        Set<Integer> idsFromGroup = new TreeSet();
        Integer id;
        String query = "select compound_id from compounds where compound_name like \"" + compoundName + "%\"";
        try {
            ResultSet prov = statement.executeQuery(query);
            while (prov.next()) {
                // Build the String with the information of the compound
                id = prov.getInt(1);
                if (!prov.wasNull()) {
                    idsFromGroup.add(id);

                }
            }
        } catch (SQLSyntaxErrorException ex) {
            System.out.println(ex);
            System.out.println(query);

        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return idsFromGroup;
    }

    /**
     * retrieve the compoundId from the compound_name
     *
     * @param PC_ID of the compound to search
     * @return the information of the compound
     */
    public int getCompoundIdFromPCID(Integer PC_ID) {
        int result = 0;
        try {
            String query = "select compound_id from compounds_pc where pc_id = " + PC_ID + "";
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                result = prov.getInt(1);
                if (prov.wasNull()) {
                    result = 0;

                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     * retrieve the compoundId from the compound_name
     *
     * @param chebi_id of the compound to search
     * @return the information of the compound
     */
    public int getCompoundIdFromChebiID(Integer chebi_id) {
        int result = 0;
        try {
            String query = "select compound_id from compounds_chebi where chebi_id = '" + chebi_id + "'";
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                result = prov.getInt(1);
                if (prov.wasNull()) {
                    result = 0;

                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     * Update the structures of the compound Id in the database
     *
     * @param compoundId
     * @param InChI
     * @param InChIKey
     * @param SMILES
     */
    public void updateIdentifiers(int compoundId, String InChI, String InChIKey, String SMILES) {
        String update;
        if (!InChI.equals("") && !InChIKey.equals("") && !SMILES.equals("")) {
            update = "update compound_identifiers set inchi = '" + InChI
                    + "', inchi_key = '" + InChIKey
                    + "', smiles = '" + SMILES + "' WHERE compound_id = " + compoundId;
            this.executeNewIDU(update);
        }
    }

    /**
     * retrieve the pathway Id of the pathway pathwayMap
     *
     * @param pathwayMap of the compound to search
     * @return the information of the compound
     */
    public int getPathwayId(String pathwayMap) {
        int result = 0;
        try {
            String query = "SELECT pathway_id FROM pathways where pathway_map= '"
                    + pathwayMap + "'";
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                result = prov.getInt(1);
                if (prov.wasNull()) {
                    result = 0;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     * Insert a pathway and return the pathwayId of the insertion
     *
     * @param pathwayMap
     * @param pathwayName
     * @return
     */
    public int insertPathway(String pathwayMap, String pathwayName) {
        int pathwayId = 0;
        String insertion;

        if (pathwayMap != null && !pathwayMap.equals("") && !pathwayMap.equalsIgnoreCase("null")
                && pathwayName != null && !pathwayName.equals("") && !pathwayName.equalsIgnoreCase("null")) {
            pathwayName = Utilities.escapeSQL(pathwayName);
            insertion = "INSERT IGNORE INTO pathways(pathway_map, pathway_name) VALUES(\""
                    + pathwayMap + "\", \"" + pathwayName + "\")";
            //System.out.println("INSERTING PATHWAY FROM HMDB: " + insertion);
            pathwayId = this.executeNewInsertion(insertion);

        }
        return pathwayId;
    }

    /**
     * Insert a pathway and return the pathwayId of the insertion
     *
     * @param compound_id
     * @param pathway_id
     * @return
     */
    public int getCompoundIDFromCompoundsPathways(int compound_id, int pathway_id) {
        int isThereRelation = 0;
        String query;
        if (compound_id > 0 && pathway_id > 0) {
            query = "SELECT compound_id FROM compounds_pathways where compound_id= "
                    + compound_id + " and pathway_id = " + pathway_id;
            //System.out.println("INSERTING PATHWAY FROM HMDB: " + insertion1);
            isThereRelation = this.getInt(query);
        }
        return isThereRelation;
    }

    /**
     * Insert a pathway and return the pathwayId of the insertion
     *
     * @param compound_id
     * @param pathway_id
     */
    public void insertCompoundPathway(int compound_id, int pathway_id) {
        String insertion;
        if (compound_id > 0 && pathway_id > 0) {
            insertion = "INSERT IGNORE INTO compounds_pathways(compound_id, pathway_id) VALUES("
                    + compound_id + ", " + pathway_id + ")";

            this.executeIDU(insertion);
        }
    }

    /**
     * Insert the node node_id with name node_name and parent parent_node_id
     * into the database, saving the hierarchy
     *
     * @param node_name
     * @param node_id
     * @param parent_node_id
     */
    public void insertNodeCLASSYFIRE(String node_name, String node_id, String parent_node_id) {
        String kingdom = "";
        String super_class = "";
        String main_class = "";
        String sub_class = "";
        String direct_parent = "";
        String level_7 = "";
        String level_8 = "";
        String level_9 = "";
        String level_10 = "";
        String level_11 = "";
        String insertion;
        if (node_name != null && !node_name.equals("")) {
            node_name = Utilities.escapeSQL(node_name);

            insertion = "INSERT IGNORE INTO classyfire_classification_dictionary(node_id, node_name) VALUES('"
                    + node_id + "', '" + node_name + "')";
            this.executeNewIDU(insertion);
        }

        switch (node_id) {
            case "CHEMONTID:9999999":
                return;
            case "CHEMONTID:0000000":
            case "CHEMONTID:0000001":
                kingdom = node_id;
                break;
            default:
                kingdom = getFieldCLASSYFIRE(parent_node_id, "kingdom");
                super_class = getFieldCLASSYFIRE(parent_node_id, "super_class");
                if (super_class.equals("")) {
                    super_class = node_id;
                    break;
                }
                main_class = getFieldCLASSYFIRE(parent_node_id, "main_class");
                if (main_class.equals("")) {
                    main_class = node_id;
                    break;
                }
                sub_class = getFieldCLASSYFIRE(parent_node_id, "sub_class");
                if (sub_class.equals("")) {
                    sub_class = node_id;
                    break;
                }
                direct_parent = getFieldCLASSYFIRE(parent_node_id, "direct_parent");
                if (direct_parent.equals("")) {
                    direct_parent = node_id;
                    break;
                }
                level_7 = getFieldCLASSYFIRE(parent_node_id, "level_7");
                if (level_7.equals("")) {
                    level_7 = node_id;
                    break;
                }
                level_8 = getFieldCLASSYFIRE(parent_node_id, "level_8");
                if (level_8.equals("")) {
                    level_8 = node_id;
                    break;
                }
                level_9 = getFieldCLASSYFIRE(parent_node_id, "level_9");
                if (level_9.equals("")) {
                    level_9 = node_id;
                    break;
                }
                level_10 = getFieldCLASSYFIRE(parent_node_id, "level_10");
                if (level_10.equals("")) {
                    level_10 = node_id;
                    break;
                }
                level_11 = getFieldCLASSYFIRE(parent_node_id, "level_11");
                if (level_11.equals("")) {
                    level_11 = node_id;
                    break;
                }
                break;
        }
        super_class = Utilities.escapeSQL(super_class);
        main_class = Utilities.escapeSQL(main_class);
        sub_class = Utilities.escapeSQL(sub_class);
        direct_parent = Utilities.escapeSQL(direct_parent);
        level_7 = Utilities.escapeSQL(level_7);
        level_8 = Utilities.escapeSQL(level_8);
        level_9 = Utilities.escapeSQL(level_9);
        level_10 = Utilities.escapeSQL(level_10);
        level_11 = Utilities.escapeSQL(level_11);
        insertion = "INSERT IGNORE INTO classyfire_classification(node_id, "
                + "kingdom,super_class,main_class, sub_class,direct_parent, "
                + "level_7, level_8, level_9, level_10, level_11) VALUES('"
                + node_id + "', '" + kingdom + "', '" + super_class + "', '" + main_class + "', '"
                + sub_class + "', '" + direct_parent + "', '"
                + level_7 + "', '" + level_8 + "', '" + level_9 + "', '" + level_10 + "', '" + level_11
                + "')";
        //System.out.println("INSERTING: " + insertion);
        this.executeNewIDU(insertion);
        //System.out.println(insertion);
    }

    /**
     * Returns the value of the field sqlField for the node node_id
     *
     * @param node_id
     * @param sqlField
     * @return
     */
    public String getFieldCLASSYFIRE(String node_id, String sqlField) {
        String sqlFieldResult = "";
        try {
            String query = "select " + sqlField + " from classyfire_classification where node_id = '" + node_id + "'";

            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                sqlFieldResult = prov.getString(1);
                if (prov.wasNull()) {
                    sqlFieldResult = "";

                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return sqlFieldResult;
    }

    /**
     * return true if the node exists. False otherwise
     *
     * @param node_id
     * @return
     */
    public boolean existsNodeIDClassyfire(String node_id) {
        String query = "select node_id from classyfire_classification where node_id = '" + node_id + "'";
        try {
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    /**
     * Insert the classification node_id into the compound compound_id
     *
     * @param compound_id
     * @param node_id
     */
    public void insertCompoundClassyfireClassification(int compound_id, String node_id) {

        String insertion = "INSERT IGNORE INTO compound_classyfire_classification(compound_id, "
                + "node_id) VALUES(" + compound_id + ",'" + node_id + "')";
        //System.out.println("INSERTING: " + insertion);
        this.executeNewIDU(insertion);
    }

    /**
     * return true if the compound is already classified in classyfire
     *
     * @param compoundId
     * @return
     */
    public boolean existsCompoundClassyfire(int compoundId) {
        String query = "select compound_id from compound_classyfire_classification where compound_id = " + compoundId;
        try {
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    /**
     *
     * @param compound_id
     * @param ancestorNodes
     * @return
     * @throws exceptions.NodeNotFoundException
     */
    public boolean insertFullCompoundClassyfireClassification(int compound_id, List<String> ancestorNodes) throws NodeNotFoundException {
        for (String nodeId : ancestorNodes) {
            boolean existsNodeId = this.existsNodeIDClassyfire(nodeId);
            if (!nodeId.equals("CHEMONTID:9999999")) {
                if (existsNodeId) {
                    this.insertCompoundClassyfireClassification(compound_id, nodeId);
                } else {
                    throw new NodeNotFoundException("Node: " + nodeId + " NOT FOUND IN THE DATABASE");
                }
            }
        }
        return true;
    }

    /**
     * return the classyfire dictonary id of the node nodeId
     *
     * @param nodeId
     * @return
     */
    public int getIDFromClassificationDict(String nodeId) {
        int classyfireDictId = 0;
        String query = "select classyfire_id from classyfire_classification_dictionary"
                + " where node_id = '" + nodeId + "'";
        try {
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {

                classyfireDictId = prov.getInt(1);

            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return classyfireDictId;
    }

    /**
     * Insert the Reactions in the db
     *
     * @param compound_id
     * @param reactions
     */
    public void insertReactions(int compound_id, List<String> reactions) {
        for (String reaction : reactions) {
            String insertion = "INSERT IGNORE INTO compounds_reactions_kegg(compound_id, reaction_id) VALUES("
                    + compound_id + ", '" + reaction + "')";
            this.executeIDU(insertion);
        }
    }

    /**
     * Get the ce_exp_prop_id to know the ID of the experimental properties from
     * the buffer the temperature, the ionization mode and the polarity
     *
     * @param buffer
     * @param temperature
     * @param ionization_mode
     * @param polarity
     * @return the ce_exp_prop_id, null otherwise
     */
    public Integer getCE_exp_prop_Id(Integer buffer, Integer temperature,
            Integer ionization_mode, Integer polarity) {
        Integer ce_exp_prop_id = null;
        String query = "SELECT ce_exp_prop_id FROM ce_experimental_properties "
                + " WHERE BUFFER = " + buffer + " AND TEMPERATURE = " + temperature
                + " AND ionization_mode = " + ionization_mode + " AND polarity = " + polarity;
        try {
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {

                ce_exp_prop_id = prov.getInt(1);

                if (prov.wasNull()) {
                    return null;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
            return ce_exp_prop_id;
        }
        return ce_exp_prop_id;
    }

    /**
     * Insert the ce_eff_mob of the CePrecursor ion and returns the id assigned
     *
     * @param cePrecursorIon
     * @return id assigned, 0 if the exp properties id do not exist
     */
    public Integer insertCECompoundEffMob(CEPrecursorIon cePrecursorIon) {
        Integer exp_prop_id = cePrecursorIon.getCe_exp_prop_type().getCode();
        if (exp_prop_id == null) {
            System.out.println("TRYING TO INSERT COMPOUND: " + cePrecursorIon
                    + " WITH NOT SUPPORTED EXPERIMENTAL CONDITIONS");
            return 0;
        }
        String insertion = "INSERT IGNORE INTO ce_eff_mob("
                + "ce_compound_id, ce_exp_prop_id, "
                + "cembio_id, eff_mobility) VALUES("
                + cePrecursorIon.getCompound_id() + ", " + cePrecursorIon.getCe_exp_prop_type().getCode() + ", "
                + cePrecursorIon.getCembio_id() + ", " + cePrecursorIon.getEff_mobility() + ")";
        //System.out.println("INSERTION OF EFF MOB: " + insertion);

        return this.executeNewInsertion(insertion);
    }

    /**
     * Insert the CE compound precursor ion
     *
     * @param cePrecursorIon
     * @return the id generated, null if exp_prop_id are wrong or 0 if the
     * insert did not insert any row
     */
    public Integer insertCEExperimentalPropertiesMetadata(CEPrecursorIon cePrecursorIon) {
        Integer exp_prop_id = cePrecursorIon.getCe_exp_prop_type().getCode();
        if (exp_prop_id == null) {
            System.out.println("TRYING TO INSERT COMPOUND: " + cePrecursorIon
                    + " WITH NOT SUPPORTED EXPERIMENTAL CONDITIONS");
            return null;
        }
        String insertion = "INSERT IGNORE INTO ce_experimental_properties_metadata("
                + "ce_eff_mob_id, experimental_mz, ce_identification_level, "
                + "ce_sample_type, capillary_voltage, capillary_length, "
                + "bge_compound_id, absolute_MT, relative_MT, commercial) VALUES("
                + cePrecursorIon.getCe_eff_mob_id() + ", "
                + cePrecursorIon.getExperimental_mz() + ", "
                + cePrecursorIon.getCe_identification_level() + ", " + cePrecursorIon.getCe_sample_type() + ", "
                + cePrecursorIon.getCapillary_voltage() + ", " + cePrecursorIon.getCapillary_length() + ", "
                + cePrecursorIon.getBge_compound_id() + ", "
                + cePrecursorIon.getAbsolute_MT() + ", " + cePrecursorIon.getRelative_MT() + ", '"
                + cePrecursorIon.getCommercial()
                + "')";
        int idGenerated = this.executeNewInsertion(insertion);
        return idGenerated;
    }

    /**
     * Get the Precursor Ion based on the ionization Mode and the CembioId
     *
     * @param CembioId
     * @param ionization_mode
     * @return the ce_exp_prop_id, null otherwise
     */
    public CEPrecursorIon getCEPrecursorIonFromCembioIdAndIonizationMode(
            Integer CembioId, Integer ionization_mode) {
        CEPrecursorIon cePrecursorIon = null;
        String query = "SELECT cembio_id, ce_compound_id, ce_eff_mob_id, polarity, buffer, temperature"
                + " FROM ce_eff_mob ce_eff_mob inner join "
                + " ce_experimental_properties cep on ce_eff_mob.ce_exp_prop_id=cep.ce_exp_prop_id"
                + " WHERE cembio_id = " + CembioId + " AND ionization_mode = " + ionization_mode;
        try {
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {

                Integer cembio_id = prov.getInt(1);
                Integer ce_compound_id = prov.getInt(2);
                Integer ce_eff_mob_id = prov.getInt(3);
                Integer polarity = prov.getInt(4);
                Integer buffer = prov.getInt(5);
                Integer temperature = prov.getInt(6);
                if (!prov.wasNull()) {
                    cePrecursorIon = new CEPrecursorIon(cembio_id, ce_compound_id,
                            ce_eff_mob_id, ionization_mode, polarity, buffer, temperature);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
            return cePrecursorIon;
        } catch (IncorrectTemperatureException | IncorrectBufferException | IncorrectIonizationModeException | IncorrectPolarityException ex) {
            Logger.getLogger(CMMDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cePrecursorIon;
    }

    /**
     * get the compound id from the inchi
     *
     * @param InChI
     * @return the compoundid
     */
    public int getCECompoundIdFromInChI(String InChI) {
        int compound_id = 0;
        String query = "select ceep.compound_id from ce_eff_mob ce_eff_mob "
                + "inner join compound_identifiers ci on ci.compound_id=ce_eff_mob.compound_id "
                + " where ci.inchi = '" + InChI + "'";
        try {
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                compound_id = prov.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return compound_id;

    }

    /**
     * Get the CE Product Ion ID o
     *
     * @param ceProductIon
     * @return the CE Product Ion ID if exists, 0 otherwise
     */
    public int getCEProductIonIdFromMZVoltAndCEFMob(CEProductIon ceProductIon) {
        int compound_id = 0;
        Double delta = 0.05d;
        Double mzLimitUp = ceProductIon.getCe_productIon_mz() + delta;
        Double mzLimitDown = ceProductIon.getCe_productIon_mz() - delta;
        String query = "select ce_product_ion_id  from compound_ce_product_ion "
                + " where ce_eff_mob_id = " + ceProductIon.getCe_eff_mob_id()
                + " and ion_source_voltage = " + ceProductIon.getIon_source_voltage()
                + " and ce_product_ion_mz < " + mzLimitUp + " and ce_product_ion_mz > " + mzLimitDown;
        try {
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                compound_id = prov.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return compound_id;

    }

    /**
     * Insert the CE compound precursor ion
     *
     * @param ceProductIon
     */
    public void insertCEProductIon(CEProductIon ceProductIon) {
        Integer compound_id_own = ceProductIon.getCompound_id_own();
        String insertion;
        String ceProductIon_name = Utilities.escapeSQL(ceProductIon.getCe_productIon_name());
        String ceProductIon_transformationType = Utilities.escapeSQL(ceProductIon.getCe_transformation_type());

        Integer ceProductIonId = this.getCEProductIonIdFromMZVoltAndCEFMob(ceProductIon);

        if (ceProductIonId == 0) {
            insertion = "INSERT IGNORE INTO compound_ce_product_ion("
                    + "ion_source_voltage, ce_product_ion_mz, ce_product_ion_intensity, "
                    + "ce_transformation_type, ce_product_ion_name, "
                    + "ce_eff_mob_id, compound_id_own) VALUES("
                    + ceProductIon.getIon_source_voltage() + ", "
                    + ceProductIon.getCe_productIon_mz() + ", "
                    + ceProductIon.getCe_productIon_intensity() + ", \"" + ceProductIon_transformationType + "\", \""
                    + ceProductIon_name + "\", " + ceProductIon.getCe_eff_mob_id()
                    + ", null)";
            // System.out.println("INSERTION of PRODUCT ION: " + insertion);
            ceProductIonId = this.executeNewInsertion(insertion);
        } else {
            insertion = "Update compound_ce_product_ion set ce_product_ion_intensity = " + ceProductIon.getCe_productIon_intensity()
                    + ", ce_transformation_type = \"" + ceProductIon_transformationType + "\", ce_product_ion_name = \"" + ceProductIon_name + "\""
                    + " where ce_product_ion_id = " + ceProductIonId;
            // System.out.println("UPDATING PRODUCT ION: " + insertion);
            this.executeIDU(insertion);
        }

        if (compound_id_own != null) {
            String updateCompoundIdOwn = "Update compound_ce_product_ion set compound_id_own = " + compound_id_own + " where ce_product_ion_id = " + ceProductIonId;
            this.executeIDU(updateCompoundIdOwn);
        }
    }

    /**
     * Update the logP of a compound
     *
     * @param compoundId
     * @param logP
     */
    public void updateLogP(int compoundId, Double logP) {
        String update;
        if (logP != null) {
            update = "update compounds set logP = " + logP
                    + " WHERE compound_id = " + compoundId;
            // System.out.println("UPDATING LOGP: " + update);
            this.executeIDU(update);
        }
    }

    /**
     * Update the RT predicted and transformed of a compound of a compound
     *
     * @param compoundId
     * @param RT_pred_transformed
     */
    public void updateRT_pred_transformed(int compoundId, Double RT_pred_transformed) {
        String update;
        if (RT_pred_transformed != null) {
            update = "update compounds set RT_pred = " + RT_pred_transformed
                    + " WHERE compound_id = " + compoundId;
            // System.out.println("UPDATING LOGP: " + update);
            this.executeIDU(update);
        }
    }

    /**
     * check the list of cas from the databases when they have more than one. If
     * the inchi_key is equal to the inchi_key from the original cas, then the
     * cas is returned. If not, the last cas from the database is returned
     *
     * @param cas
     * @param inChIKey
     * @return
     */
    public String checkCasChemId(List<String> cas, String inChIKey) {
        // If the casList is empty
        String verifiedCas = "";
        // System.out.println("\nINCHIKEY : " + inChIKey);
        for (String provCas : cas) {
            // System.out.println("\n provCas: " + provCas);
            String query = "select inchi_key from compounds_cas where cas_id= \"" + provCas + "\"";
            String inChIKeyFromChemId = this.getString(query);
            // System.out.println("\nINCHIKEY FROM DB: " + inChIKeyFromChemId);
            if (inChIKey.equals(inChIKeyFromChemId)) {
                //    System.out.println("\nCAS FOUND!: "+ provCas);
                return provCas;
            } else {
                // If there is no coincidence with any cas_id from uniChem then 
                // CasId is the last one in the List
                verifiedCas = provCas;
            }
        }
        return verifiedCas;
    }

    public int insertAspergillusCompound(String name, String MeSH_Nomenclature,
            String IUPAC_classification, String biological_activity, String formula, Double exact_mass,
            String inChI, String inChIKey, String smiles, Double logP, Integer pc_id,
            String aspergillus_link) {
        // Secondary_metabolite;MeSH_Nomenclature;IUPAC_Classification;
        // Formula;exact_mass;InChI;InChIKey;Smiles;LogP;PC_ID;Aspergillus_link

        int compound_id = this.getCompoundIdFromInchiKey(inChIKey);

        if (compound_id == 0) {
            int compound_status = 0;
            int compound_type = 0;
            String formulaType = PatternFinder.getTypeFromFormula(formula);
            int[] charges = PatternFinder.getChargeFromSmiles(smiles);
            int chargeType = charges[0];
            int numCharges = charges[1];
            compound_id = this.insertCompound(null, name, formula, exact_mass,
                    chargeType, numCharges, formulaType, compound_type, compound_status, logP);
            if (compound_id != 0) {
                this.insertIdentifiers(compound_id, inChI, inChIKey, smiles);
            } else {
                return 0;
            }
        }

        // Insert PC_ID 
        this.insertCompoundPC(compound_id, pc_id);
        // Insert aspergillus
        this.insertCompoundAspergillus(compound_id, biological_activity, aspergillus_link, MeSH_Nomenclature, IUPAC_classification);

        return compound_id;
        //System.out.println(insertion);
    }

    /**
     * Insert the relation between compound_id and knapsack_id
     *
     * @param compoundId
     * @param knapsackId
     */
    public void insertCompoundKnapsack(int compoundId, String knapsackId) {
        if (knapsackId != null) {
            String insertion = "INSERT IGNORE INTO "
                    + "compounds_knapsack(compound_id, knapsack_id) VALUES("
                    + compoundId + ", \"" + knapsackId + "\")";
            this.executeNewIDU(insertion);
            //System.out.println(insertion);
        }
    }

    /**
     * Insert the relation between compound_id and npatlas_id
     *
     * @param compoundId
     * @param npatlasId
     */
    public void insertCompoundNPAtlas(int compoundId, Integer npatlasId) {
        if (npatlasId != null) {
            String insertion = "INSERT IGNORE INTO "
                    + "compounds_npatlas(compound_id, npatlas_id) VALUES("
                    + compoundId + "," + npatlasId + ")";
            this.executeNewIDU(insertion);
            //System.out.println(insertion);
        }
    }

    /**
     * Get the compoundId from the knapsackId
     *
     * @param knapsackId
     */
    public Integer getCompoundIdFromKnapsack(String knapsackId) {
        int compoundId = 0;
        try {
            String query = "select compound_id from compounds_knapsack where knapsack_id = '" + knapsackId + "'";
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                compoundId = prov.getInt(1);
                compoundId = 0;
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return compoundId;
    }

    /**
     * Insert the reference
     *
     * @param reference
     * @return id assigned, 0 if the exp properties id do not exist
     */
    public Integer insertReference(Reference reference) {
        if (reference == null) {
            System.out.println("TRYING TO INSERT null reference: " + reference);
            return 0;
        }
        String referenceTextSQL = reference.getReferenceText() == null ? "NULL" : "\"" + reference.getReferenceText() + "\"";
        String referenceDOISQL = reference.getDoi() == null ? "NULL" : "\"" + reference.getDoi() + "\"";
        String referenceLinkSQL = reference.getLink() == null ? "NULL" : "\"" + reference.getLink() + "\"";
        Reference referenceFromDB;
        Integer referenceId = null;
        try {
            referenceFromDB = getReferenceFromDOI(reference.getDoi());
            referenceId = referenceFromDB.getId();
            reference.setId(referenceId);
        } catch (ReferenceException ex) {
            String insertion = "INSERT IGNORE INTO reference("
                    + "reference_text, "
                    + "doi, link) VALUES("
                    + referenceTextSQL + ", " + referenceDOISQL + ", "
                    + referenceLinkSQL + ")";
            //System.out.println("INSERTION OF EFF MOB: " + insertion);

            referenceId = this.executeNewInsertion(insertion);
            reference.setId(referenceId);
        }
        return referenceId;
    }

    /**
     * get the reference from id
     *
     * @param referenceId
     * @return the reference or throws a referenceNotFoundException
     * @throws exceptions.ReferenceException
     */
    public Reference getReferenceFromReferenceId(Integer referenceId) throws ReferenceException {
        Reference reference;
        String query = "select reference_id, reference_text, doi, link from reference "
                + " where reference_id = " + referenceId;
        try {
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {

                Integer reference_id = prov.getInt("reference_id");
                String reference_text = prov.getString("reference_text");
                String doi = prov.getString("doi");
                String link = prov.getString("link");
                reference = new Reference(reference_id, reference_text, doi, link);
                return reference;
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        throw new ReferenceException("Reference with ID: " + referenceId + " NOT FOUND", ErrorTypes.ERROR_TYPE.NOT_FOUND);
    }

    /**
     * get the reference id from reference text
     *
     * @param referenceText
     * @return the reference or throws a referenceNotFoundException
     * @throws exceptions.ReferenceException
     */
    public Integer getReferenceIDFromText(String referenceText) throws ReferenceException {
        Integer referenceId;
        String query = "select reference_id from reference "
                + " where reference_text = \"" + referenceText + "\"";
        try {
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                referenceId = prov.getInt("reference_id");
                if (!prov.wasNull()) {
                    return referenceId;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        throw new ReferenceException("Reference with DOI: " + referenceText + " NOT FOUND", ErrorTypes.ERROR_TYPE.NOT_FOUND);
    }

    /**
     * get the reference from DOI
     *
     * @param DOI
     * @return the reference or throws a referenceNotFoundException
     * @throws exceptions.ReferenceException
     */
    public Reference getReferenceFromDOI(String DOI) throws ReferenceException {
        Reference reference;
        String query = "select reference_id, reference_text, doi, link from reference "
                + " where doi like \"%" + DOI + "%\"";
        try {
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                Integer reference_id = prov.getInt("reference_id");

                if (!prov.wasNull()) {
                    String reference_text = prov.getString("reference_text");
                    String doi = prov.getString("doi");
                    String link = prov.getString("link");
                    reference = new Reference(reference_id, reference_text, doi, link);
                    return reference;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        throw new ReferenceException("Reference with DOI: " + DOI + " NOT FOUND", ErrorTypes.ERROR_TYPE.NOT_FOUND);
    }

    /**
     * get the reference from reference text
     *
     * @param referenceText
     * @return the reference or throws a referenceNotFoundException
     * @throws exceptions.ReferenceException
     */
    public Reference getReferenceFromText(String referenceText) throws ReferenceException {
        Reference reference;
        String query = "select reference_id, reference_text, doi, link from reference "
                + " where reference_text = \"" + referenceText + "\"";
        try {
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                Integer reference_id = prov.getInt("reference_id");

                if (!prov.wasNull()) {
                    String reference_text = prov.getString("reference_text");
                    String doi = prov.getString("doi");
                    String link = prov.getString("link");
                    reference = new Reference(reference_id, reference_text, doi, link);
                    return reference;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        throw new ReferenceException("Reference with text: " + referenceText + " NOT FOUND", ErrorTypes.ERROR_TYPE.NOT_FOUND);
    }

    /**
     * Insert the reference
     *
     * @param organismClassification
     * @return id assigned, 0 if the exp properties id do not exist
     */
    public Integer insertOrganism(OrganismClassification organismClassification) {
        if (organismClassification == null) {
            System.out.println("TRYING TO INSERT null organism classificaiton: " + organismClassification);
            return 0;
        }
        String kingdomName = organismClassification.getKingdom();
        String familyName = organismClassification.getFamily();
        //String genusName = organismClassification.getGenus();
        String specieName = organismClassification.getSpecie();
        String subSpecieName = organismClassification.getSubspecie();
        String nullString = "NULL";
        Integer kingdom_node_id;
        Integer family_node_id;
        Integer genus_node_id;
        Integer specie_node_id;
        Integer subspecie_node_id;

        // Start to insert from level 5 (subspecie); level 4 (specie); level 3 (genus); level 2(family); and level 1(kingdom)
        if (kingdomName == null) {
            System.out.println("TRYING TO INSERT null organism classification: " + organismClassification);
            return 0;
        } else {
            try {
                kingdom_node_id = getOrganismIdFromOrganismName(kingdomName);
            } catch (OrganismNotFoundException oe) {
                String insertion = "INSERT IGNORE INTO organism("
                        + "organism_name, "
                        + "organism_level, parent_id) VALUES(\""
                        + kingdomName + "\", 1, "
                        + nullString + ")";
                kingdom_node_id = this.executeNewInsertion(insertion);
            }
            if (familyName == null) {
                System.out.println("TRYING TO INSERT null organism classificaiton: " + organismClassification);
                return kingdom_node_id;
            } else {
                try {
                    family_node_id = getOrganismIdFromOrganismName(familyName);
                } catch (OrganismNotFoundException oe) {
                    String insertion = "INSERT IGNORE INTO organism("
                            + "organism_name, "
                            + "organism_level, parent_id) VALUES(\""
                            + familyName + "\", 2, "
                            + kingdom_node_id + ")";
                    family_node_id = this.executeNewInsertion(insertion);
                }

                if (specieName == null) {
                    return family_node_id;
                } else {
                    try {
                        specie_node_id = getOrganismIdFromOrganismName(specieName);
                    } catch (OrganismNotFoundException oe) {
                        String insertion = "INSERT IGNORE INTO organism("
                                + "organism_name, "
                                + "organism_level, parent_id) VALUES(\""
                                + specieName + "\", 3, "
                                + family_node_id + ")";
                        specie_node_id = this.executeNewInsertion(insertion);
                    }
                }
                if (subSpecieName == null) {
                    return specie_node_id;
                } else {
                    try {
                        subspecie_node_id = getOrganismIdFromOrganismName(subSpecieName);
                    } catch (OrganismNotFoundException oe) {
                        String insertion = "INSERT IGNORE INTO organism("
                                + "organism_name, "
                                + "organism_level, parent_id) VALUES(\""
                                + subSpecieName + "\", 4, "
                                + specie_node_id + ")";
                        subspecie_node_id = this.executeNewInsertion(insertion);
                    }
                    return subspecie_node_id;

                }
            }
        }
    }

    /**
     * get the reference from id
     *
     * @param organismName
     * @param organismLevel
     * @return the reference or throws a referenceNotFoundException
     * @throws exceptions.OrganismNotFoundException if the organism is not
     * present in the DB
     */
    public Integer getOrganismIdFromOrganismNameAndLevel(String organismName, Integer organismLevel) throws OrganismNotFoundException {
        Integer organism_id;
        String query = "select organism_id from organism "
                + " where organism_name = \"" + organismName + "\"" + " and organism_level = " + organismLevel;
        try {
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                organism_id = prov.getInt("organism_id");
                if (!prov.wasNull()) {
                    return organism_id;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        throw new OrganismNotFoundException("Organism with Name: " + organismName + " NOT FOUND", ErrorTypes.ERROR_TYPE.NOT_FOUND);
    }

    /**
     * get the reference from id
     *
     * @param organismName
     * @return the reference or throws a referenceNotFoundException
     * @throws exceptions.OrganismNotFoundException if the organism is not
     * present in the DB
     */
    public Integer getOrganismIdFromOrganismName(String organismName) throws OrganismNotFoundException {
        Integer organism_id;
        String query = "select organism_id from organism "
                + " where organism_name = \"" + organismName + "\"";
        try {
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                organism_id = prov.getInt("organism_id");
                if (!prov.wasNull()) {
                    return organism_id;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        throw new OrganismNotFoundException("Organism with Name: " + organismName + " NOT FOUND", ErrorTypes.ERROR_TYPE.NOT_FOUND);
    }

    /**
     * get the name from the organism id
     *
     * @param organismId
     * @return the reference or throws a referenceNotFoundException
     * @throws exceptions.OrganismNotFoundException if the organism is not
     * present in the DB
     */
    public String getOrganismNameFromOrganismId(Integer organismId) throws OrganismNotFoundException {
        String organismName;
        String query = "select organism_name from organism "
                + " where organism_id = " + organismId + "";
        try {
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                organismName = prov.getString("organism_name");
                if (!prov.wasNull()) {
                    return organismName;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        throw new OrganismNotFoundException("Organism with Id: " + organismId + " NOT FOUND", ErrorTypes.ERROR_TYPE.NOT_FOUND);
    }

    /**
     * get the parentId from the organism id
     *
     * @param organismId
     * @return the parent_id integer, null if it is a root node or throws a
     * referenceNotFoundException
     * @throws exceptions.OrganismNotFoundException if the organism is not
     * present in the DB
     */
    public Integer getParentIdFromOrganismId(Integer organismId) throws OrganismNotFoundException {
        Integer parentId;
        String query = "select parent_id from organism "
                + " where organism_id = " + organismId + "";
        try {
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                parentId = prov.getInt("parent_id");
                if (!prov.wasNull()) {
                    return parentId;
                } else {
                    return null;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        throw new OrganismNotFoundException("Organism with Id: " + organismId + " NOT FOUND", ErrorTypes.ERROR_TYPE.NOT_FOUND);
    }

    public OrganismClassification getOrganismFromOrganismName(String organismName) throws OrganismNotFoundException {
        Integer organismId = this.getOrganismIdFromOrganismName(organismName);
        return this.getOrganismFromOrganismId(organismId);
    }

    /**
     * get the organism from id
     *
     * @param organismId
     * @return the Organism from the Id. Null otherwise
     */
    public OrganismClassification getOrganismFromOrganismId(Integer organismId) {
        OrganismClassification organism = null;
        String query = "select organism_id, organism_name, organism_level, parent_id from organism o "
                + " where organism_id = " + organismId;
        String kingdom = null;
        String family = null;
        String genus = null;
        String specie = null;
        String subspecie = null;
        try {
            ResultSet prov = statement.executeQuery(query);
            if (prov.next()) {
                // To check if there are results
                organismId = prov.getInt("organism_id");

                if (!prov.wasNull()) {
                    Integer organism_level = prov.getInt("organism_level");
                    String organismName = prov.getString("organism_name");
                    Integer parentId = prov.getInt("parent_id");
                    for (Integer level = organism_level; level > 0; level--) {

                        switch (level) {
                            case 1:
                                kingdom = organismName;
                                break;
                            case 2:
                                family = organismName;
                                break;
                            case 3:
                                genus = organismName;
                                break;
                            case 4:
                                specie = organismName;
                                break;
                            case 5:
                                subspecie = organismName;
                                break;
                        }
                        if (level > 1) {
                            try {
                                organismName = this.getOrganismNameFromOrganismId(parentId);
                                parentId = this.getParentIdFromOrganismId(parentId);
                            } catch (OrganismNotFoundException ex) {
                                Logger.getLogger(CMMDatabase.class.getName()).log(Level.SEVERE, null, ex + " \n ERROR CREATING AND ORGANISM. PARENT NOT FOUND");
                            }
                        }
                    }
                    try {
                        organism = new OrganismClassification(organismId, kingdom, family, genus, specie, subspecie, null);
                    } catch (Exception ex) {
                        Logger.getLogger(CMMDatabase.class.getName()).log(Level.SEVERE, null, ex + "\n ERROR CREATING AN ORGANISM FROM ID: " + organismId);
                    }
                    return organism;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * get the names of organism by level
     *
     * @param organismLevel
     * @return the reference or throws a referenceNotFoundException
     * @throws exceptions.OrganismNotFoundException if the organism is not
     * present in the DB
     */
    public Set<String> getOrganismNamesByLevel(Integer organismLevel) throws OrganismNotFoundException {
        Set<String> organismNames = new TreeSet();
        String query = "select organism_name from organism "
                + " where organism_level = " + organismLevel + "";
        try {
            ResultSet prov = statement.executeQuery(query);
            while (prov.next()) {
                String organismName = prov.getString("organism_name");
                if (!prov.wasNull()) {
                    organismNames.add(organismName);
                }
            }
            return organismNames;
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        throw new OrganismNotFoundException("No organism in level " + organismLevel, ErrorTypes.ERROR_TYPE.NOT_FOUND);
    }

    /**
     * get the names of organism by level
     *
     * @return the reference or throws a referenceNotFoundException
     */
    public Set<String> getOrganismNamesForUpdatingOrganism() {
        Set<String> organismNames = new TreeSet();
        String query = "select organism_name from organism "
                + " where organism_level = 3 and organism_name like '% %'";
        try {
            ResultSet prov = statement.executeQuery(query);
            while (prov.next()) {
                String organismName = prov.getString("organism_name");
                if (!prov.wasNull()) {
                    organismNames.add(organismName);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CMMDatabase.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return organismNames;
    }

    /**
     * Insert the relation between the organism and the compound
     *
     * @param compound_id
     * @param organism_id
     */
    public void insertCompoundOrganism(int compound_id, int organism_id) {

        String insertion = "INSERT IGNORE INTO compound_organism(compound_id, "
                + "organism_id) VALUES(" + compound_id + "," + organism_id + ")";
        //System.out.println("INSERTING: " + insertion);
        this.executeNewIDU(insertion);
    }

    /**
     * Insert the relation between the organism and the compound
     *
     * @param compound_id
     * @param reference_id
     */
    public void insertCompoundReference(int compound_id, int reference_id) {

        String insertion = "INSERT IGNORE INTO compound_reference(compound_id, "
                + "reference_id) VALUES(" + compound_id + "," + reference_id + ")";
        //System.out.println("INSERTING: " + insertion);
        this.executeNewIDU(insertion);
    }

    /**
     * Insert the relation between the organism and the compound
     *
     * @param organism_id
     * @param reference_id
     */
    public void insertOrganismReference(int organism_id, int reference_id) {

        String insertion = "INSERT IGNORE INTO organism_reference(organism_id, "
                + "reference_id) VALUES(" + organism_id + "," + reference_id + ")";
        //System.out.println("INSERTING: " + insertion);
        this.executeNewIDU(insertion);
    }

    /**
     * Insert the adduct to be formed or returns the adduct_id if it is already
     * present in the DB
     *
     * @param adductName
     * @return the adduct Id or a not foundException
     * @throws exceptions.NotFoundException
     */
    public int getAdductId(String adductName) throws NotFoundException {
        int adduct_id = 0;
        String query = "select adduct_id from adduct where adduct_type = '" + adductName + "'";
        ResultSet prov;
        try {
            prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                adduct_id = prov.getInt(1);
            } else {
                throw new NotFoundException(adductName + " not present in the database");
            }
        } catch (SQLException ex) {
            throw new NotFoundException(adductName + " not present in the database");
        }

        return adduct_id;
    }

    /**
     * Insert the adduct to be formed or returns the adduct_id if it is already
     * present in the DB
     *
     * @param ion_mode 1 positive; 2 negative
     * @param adductName
     * @return the id of the adduct inserted or already present
     * @throws exceptions.IonModeAdductException
     */
    public int insertAdduct(int ion_mode, String adductName) throws IonModeAdductException {

        String insertion;

        int adduct_id;
        adductName = utilities.Utilities.getAdductNoBrackets(adductName);
        try {
            adduct_id = this.getAdductId(adductName);
            return adduct_id;
        } catch (NotFoundException nfe) {
            insertion = "INSERT IGNORE INTO adduct("
                    + "ionization_mode, adduct_type) VALUES(" + ion_mode + ", \""
                    + Utilities.escapeSQL(adductName) + "\")";
            // System.out.println("INSERTION of PRODUCT ION: " + insertion);
            adduct_id = this.executeNewInsertion(insertion);

        }
        return adduct_id;
    }

    /**
     * Insert the CCS value for a given compound, adduct and buffer gas
     *
     * @param compound_id
     * @param adduct_id
     * @param buffer_gas_id
     * @param CCS
     */
    public void insertCCSValue(int compound_id, int adduct_id, int buffer_gas_id, float CCS) {
        String insertion;
        insertion = "INSERT IGNORE INTO compound_ccs("
                + "compound_id, adduct_id, buffer_gas_id, ccs_value) VALUES(" + compound_id + ", "
                + adduct_id + ", " + buffer_gas_id + ", " + CCS + ")";
        this.executeNewInsertion(insertion);
    }

    /**
     * Insert the CCS value for a given compound, adduct and buffer gas
     *
     * @param compound_id
     * @param adduct_id
     * @param buffer_gas_id
     * @param CCS
     * @param adduct_intensity
     */
    public void insertCCSValue(int compound_id, int adduct_id, int buffer_gas_id, float CCS, int adduct_intensity) {
        String insertion;
        insertion = "INSERT IGNORE INTO compound_ccs("
                + "compound_id, adduct_id, buffer_gas_id, ccs_value, adduct_intensity) VALUES(" + compound_id + ", "
                + adduct_id + ", " + buffer_gas_id + ", " + CCS + ", " + adduct_intensity + ")";
        this.executeNewInsertion(insertion);
    }
    
    /**
     * 
     *
     * @param compound_id
     * @param adduct_id
     * @param buffer_gas_id
     * @return the CCS Value for the compound_id corresponding with the adduct_id and buffer_gas_id 
     * @throws exceptions.NotFoundException 
     */
    public float getCCSValue(int compound_id, int adduct_id, int buffer_gas_id) throws NotFoundException {
        String query;
        query = "select ccs_value from compound_ccs where "
                + "compound_id = " + compound_id + " and adduct_id = " + adduct_id + " and buffer_gas_id = " + buffer_gas_id;
        ResultSet prov;
        Float CCS_value = null;
        try {
            prov = statement.executeQuery(query);
            if (prov.next()) {
                // Build the String with the information of the compound
                CCS_value = prov.getFloat(1);
            } else {
                throw new NotFoundException("compound -> " + compound_id + "with adduct " + adduct_id 
                        + " and buffer gas id " + buffer_gas_id + "not present in the database");
            }
        } catch (SQLException ex) {
            throw new NotFoundException("Check the query and the DB Status");
        }

        return CCS_value;
    }
    
    /**
     * Returns if the compound_id for the corresponding adduct_id and buffer_gas_id contains 
     * already a CCS Value in the CMM DB
     *
     * @param compound_id
     * @param adduct_id
     * @param buffer_gas_id
     * @return 
     */
    public boolean hasCCSValue(int compound_id, int adduct_id, int buffer_gas_id) {
        try{
            getCCSValue(compound_id, adduct_id, buffer_gas_id);
            return true;
        } catch (NotFoundException ex) {
            return false;
        }
    }
    
    
    /**
     * Updates the CCS Value
     *
     * @param compound_id
     * @param adduct_id
     * @param buffer_gas_id
     * @param CCS_value
     */
    public void updateCCSValue(int compound_id, int adduct_id, int buffer_gas_id, float CCS_value) {
        String insertion;
        insertion = "Update compound_ccs set "
                + "ccs_value = " + CCS_value + " where "
                + "compound_id = " + compound_id + " and adduct_id = " + adduct_id + " and buffer_gas_id = " + buffer_gas_id;
        this.executeNewInsertion(insertion);
    }
    
    /**
     * Updates the CCS Value
     *
     * @param compound_id
     * @param adduct_id
     * @param buffer_gas_id
     * @param CCS_value
     * @param adduct_intensity
     */
    public void updateCCSValue(int compound_id, int adduct_id, int buffer_gas_id, float CCS_value, int adduct_intensity) {
        String insertion;
        insertion = "Update compound_ccs set "
                + "ccs_value = " + CCS_value + ", adduct_intensity = " + adduct_intensity + " where "
                + "compound_id = " + compound_id + " and adduct_id = " + adduct_id + " and buffer_gas_id = " + buffer_gas_id;
        this.executeNewInsertion(insertion);
    }

}
