/*
 * Database.java
 *
 * Created on 07-nov-2019, 20:53:39
 *
 * Copyright(c) 2018 Ceu Mass Mediator All Rights Reserved.
 * This software is the proprietary information of Alberto Gil de la Fuente.
 *
 */

package databases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * {Insert class description here}
 * 
 * @version $Revision: 1.1.1.1 $
 * @since Build {insert version here} 07-nov-2019
 * 
 * @author Alberto Gil de la Fuente
 */
public class Database {
    
    
    protected Statement statement;
    protected Connection connection;
    protected ResultSet rs;

    /**
     * Creates a new instance of Database
     */
    public Database ()
    {
        
    }
    
    public Statement connectToDB(String bd, String usuario, String clave) {
        statement = null;
        try {
            // MySQL driver registered
            //DriverManager.registerDriver(new org.gjt.mm.mysql.Driver());

            // get DatabaseConnection 
            connection = DriverManager.getConnection(bd, usuario, clave);

            // Statement created to perform queries
            statement = getConnection().createStatement();
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }

        return getStatement();
    }
    
    
    public void executeQuery(String query) {
        try {
            rs = statement.executeQuery(query);
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void executeIDU(String actualizacion) // insertar, borrar o actualizar
    {
        try {
            statement.executeUpdate(actualizacion);
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Executes the insert and return the First generated Key
     * @param actualizacion
     * @return the Id of the insertion, 0 otherwise
     */
    public int executeNewInsertion(String actualizacion) // insertar, borrar o actualizar
    {
        int id = 0;
        try {
            statement.executeUpdate(actualizacion, Statement.RETURN_GENERATED_KEYS);
            try (ResultSet provRS = statement.getGeneratedKeys()) {
                if (provRS.next()) {
                    id = provRS.getInt(1);
                }
                provRS.close();
            }

        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
//        System.out.println("\n GENERATED KEY of " + actualizacion + " : " + id);
        return id;
    }

    public int executeNewInsertionWithEx(String actualizacion) throws SQLException // insertar, borrar o actualizar
    {
        int id = 0;
        statement.executeUpdate(actualizacion, Statement.RETURN_GENERATED_KEYS);
        try (ResultSet provRS = statement.getGeneratedKeys()) {
            if (provRS.next()) {
                id = provRS.getInt(1);
            }
            provRS.close();
        }

//        System.out.println("\n GENERATED KEY of " + actualizacion + " : " + id);
        return id;
    }
    
    /**
     * @return the statement
     */
    public Statement getStatement() {
        return statement;
    }

    /**
     * @return the connection
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * @return the rs
     */
    public ResultSet getRs() {
        return rs;
    }
    
    public static void main(String[] args) {
        CMMDatabase db = new CMMDatabase();
        db.connectToDB("jdbc:mysql://localhost/compounds?useSSL=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", "alberto", "alberto");
        db.getInt("Select 1");
    }
}
