/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databases;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.hc.client5.http.fluent.Content;
import org.apache.hc.client5.http.fluent.Form;
import org.apache.hc.client5.http.fluent.Request;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import model.Compound;
import model.Identifier;
import org.apache.hc.client5.http.fluent.Response;
import static utilities.Constants.PUBCHEM_ENDPOINT_COMPOUND;
import static utilities.Constants.PUBCHEM_ENDPOINT_COMPOUND_NAME;

/**
 *
 * @author ceu
 */
public class PubchemRest {

    public static Identifier getIdentifiersFromInChIPC(String inchi, int retries, int sleep) throws IOException, InterruptedException {
        int rep = 0;
        while (rep <= retries) {
            try {
                return getIdentifiersFromInChIPC(inchi);
            } catch (IOException ioe) {
                if (rep >= retries) {
                    throw ioe;
                }
                Thread.sleep(sleep);
            }
            rep++;
        }
        // Statement never reached
        return null;
    }

    public static Identifier getIdentifiersFromInChIPC(String inchi) throws IOException {
        Content content = Request.post("https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/inchi/property/InChIKey,CanonicalSMILES/JSON").
                bodyForm(Form.form().add("inchi", inchi).build())
                .execute().returnContent();
        String jsonResponseString = content.asString();
        JsonObject jsonrepsonse = new JsonParser().parse(jsonResponseString).getAsJsonObject();

        JsonObject properties = jsonrepsonse.get(("PropertyTable")).getAsJsonObject().get("Properties").getAsJsonArray().get(0).getAsJsonObject();
        Integer cid = properties.get("CID").getAsInt();
        String canonicalSmiles = properties.get("CanonicalSMILES").getAsString();
        String inchi_key = properties.get("InChIKey").getAsString();
        Identifier identifier = new Identifier(inchi, inchi_key, canonicalSmiles);
        return identifier;
    }

    public static Identifier getIdentifiersFromINCHIKEYPC(String inchi_key) throws IOException {
        Content content = Request.post("https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/InChIKey/property/inchi,CanonicalSMILES/JSON").
                bodyForm(Form.form().add("inchikey", inchi_key).build())
                .execute().returnContent();
        String jsonResponseString = content.asString();
        JsonObject jsonrepsonse = new JsonParser().parse(jsonResponseString).getAsJsonObject();

        JsonObject properties = jsonrepsonse.get(("PropertyTable")).getAsJsonObject().get("Properties").getAsJsonArray().get(0).getAsJsonObject();
        Integer cid = properties.get("CID").getAsInt();
        String canonicalSmiles = properties.get("CanonicalSMILES").getAsString();
        String inchi = properties.get("InChI").getAsString();
        Identifier identifier = new Identifier(inchi, inchi_key, canonicalSmiles);
        return identifier;
    }

    public static Integer getPCIDFromInchiKey(String inchi_key) throws IOException {
        Content content = Request.post("https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/InChIKey/property/inchi,CanonicalSMILES/JSON").
                bodyForm(Form.form().add("inchikey", inchi_key).build())
                .execute().returnContent();
        String jsonResponseString = content.asString();
        JsonObject jsonrepsonse = new JsonParser().parse(jsonResponseString).getAsJsonObject();

        JsonObject properties = jsonrepsonse.get(("PropertyTable")).getAsJsonObject().get("Properties").getAsJsonArray().get(0).getAsJsonObject();
        Integer cid = properties.get("CID").getAsInt();
        return cid;
    }

    public static Identifier getIdentifiersFromSMILESPC(String smiles) throws IOException {
        Content content = Request.post("https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/smiles/property/InChI,inchikey/JSON").
                bodyForm(Form.form().add("smiles", smiles).build())
                .execute().returnContent();
        String jsonResponseString = content.asString();
        JsonObject jsonrepsonse = new JsonParser().parse(jsonResponseString).getAsJsonObject();

        JsonObject properties = jsonrepsonse.get(("PropertyTable")).getAsJsonObject().get("Properties").getAsJsonArray().get(0).getAsJsonObject();
        Integer cid = properties.get("CID").getAsInt();
        String inchi = properties.get("InChI").getAsString();
        String inchi_key = properties.get("InChIKey").getAsString();
        Identifier identifier = new Identifier(inchi, inchi_key, smiles);
        return identifier;
    }
    
    public static Compound getCompoundFromName(String name) throws IOException {
        name = name.replace(" ", "%20");
        name = name.replace(" ", "%20");
        String uriString = PUBCHEM_ENDPOINT_COMPOUND_NAME + name + "/property/IUPACName,MonoisotopicMass,inchi,InChIKey,CanonicalSMILES,MolecularFormula,XLogP/JSON";
        
        Request request = Request.get(uriString);
        request.addHeader("Connection", "keep-alive");

        Response response = request.execute();
        Content jsonResponse = response.returnContent();
        String jsonResponseString = jsonResponse.asString();

        JsonObject jsonrepsonse = new JsonParser().parse(jsonResponseString).getAsJsonObject();
        JsonObject properties = jsonrepsonse.get(("PropertyTable")).getAsJsonObject().get("Properties").getAsJsonArray().get(0).getAsJsonObject();
        Integer cid = properties.get("CID").getAsInt();
        String IUPACName = null;
        if (properties.has("IUPACName")) {
            IUPACName = properties.get("IUPACName").getAsString();
        }
        String molecularFormula = properties.get("MolecularFormula").getAsString();
        String inchi_key = properties.get("InChIKey").getAsString();
        String inchi = properties.get("InChI").getAsString();
        String smiles = properties.get("CanonicalSMILES").getAsString();
        Double logP = null;
        if (properties.has("XLogP")) {
            logP = properties.get("XLogP").getAsDouble();
        }
        Double mass = properties.get("MonoisotopicMass").getAsDouble();
        String casId = null;
        Integer compound_id = 0;
        Integer compound_status = 0;
        Integer compound_type = 0;

        Identifier identifiers = new Identifier(inchi, inchi_key, smiles);
        Compound compound = new Compound(compound_id, IUPACName, casId, molecularFormula, mass, compound_status, compound_type, logP, identifiers);

        return compound;
    }

    public static Compound getCompoundFromPCID(int pc_id) throws IOException {
        String uriString = PUBCHEM_ENDPOINT_COMPOUND + pc_id + "/property/IUPACName,MonoisotopicMass,inchi,InChIKey,CanonicalSMILES,MolecularFormula,XLogP/JSON";
        Request request = Request.get(uriString);
        request.addHeader("Connection", "keep-alive");

        Response response = request.execute();
        Content jsonResponse = response.returnContent();
        String jsonResponseString = jsonResponse.asString();

        JsonObject jsonrepsonse = new JsonParser().parse(jsonResponseString).getAsJsonObject();
        JsonObject properties = jsonrepsonse.get(("PropertyTable")).getAsJsonObject().get("Properties").getAsJsonArray().get(0).getAsJsonObject();
        Integer cid = properties.get("CID").getAsInt();
        String IUPACName = null;
        if (properties.has("IUPACName")) {
            IUPACName = properties.get("IUPACName").getAsString();
        }
        String molecularFormula = properties.get("MolecularFormula").getAsString();
        String inchi_key = properties.get("InChIKey").getAsString();
        String inchi = properties.get("InChI").getAsString();
        String smiles = properties.get("CanonicalSMILES").getAsString();
        Double logP = null;
        if (properties.has("XLogP")) {
            logP = properties.get("XLogP").getAsDouble();
        }
        Double mass = properties.get("MonoisotopicMass").getAsDouble();
        String casId = null;
        Integer compound_id = 0;
        Integer compound_status = 0;
        Integer compound_type = 0;

        Identifier identifiers = new Identifier(inchi, inchi_key, smiles);
        Compound compound = new Compound(compound_id, IUPACName, casId, molecularFormula, mass, compound_status, compound_type, logP, identifiers);

        return compound;
    }

    public static void main(String[] args) {
        try {
            String inchiInput = "InChI=1S/C46H78O4/c1-3-4-5-6-7-8-9-10-11-12-14-19-22-25-28-31-34-37-40-43-46(49)50-44(2)41-38-35-32-29-26-23-20-17-15-13-16-18-21-24-27-30-33-36-39-42-45(47)48/h4-5,7-8,10-11,14,19,25,28,34,37,44H,3,6,9,12-13,15-18,20-24,26-27,29-33,35-36,38-43H2,1-2H3,(H,47,48)/b5-4-,8-7-,11-10-,19-14-,28-25-,37-34-";
            String inchiKeyInput = "WSRNVQBHNOZMJH-YWCHUICKSA-N";
            String smilesInput = "CCC=CCC=CCC=CCC=CCC=CCC=CCCC(=O)OC(C)CCCCCCCCCCCCCCCCCCCCCC(=O)O";
            Identifier identifier = getIdentifiersFromInChIPC(inchiInput);
            Integer cid = getPCIDFromInchiKey(inchiKeyInput);
            String canonicalSmiles = identifier.getSmiles();
            String InchiKey = identifier.getInchi_key();
            System.out.println(cid);
            System.out.println(canonicalSmiles);
            System.out.println(InchiKey);

            if (cid == 134777005) {
                System.out.println("Test REST PUBCHEM PASSED");
            } else {
                System.out.println("FAILED TEST REST PUBCHEM. CHECK THE CONNECTION INTERNET AND THE METHOD");
            }

        } catch (IOException ex) {
            Logger.getLogger(PubchemRest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
