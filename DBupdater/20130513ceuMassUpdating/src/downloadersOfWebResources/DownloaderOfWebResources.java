package downloadersOfWebResources;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import exceptions.CompoundNotClassifiedException;
import exceptions.ErrorTypes.ERROR_TYPE;
import ioDevices.IoDevice;
import ioDevices.MyFile;
import ioDevices.UriDevice;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import mine.MineJSONProcessor;
import mine.MineSDFProcessor;
import org.apache.hc.client5.http.fluent.Content;
import org.apache.hc.client5.http.fluent.Request;
import org.apache.hc.client5.http.fluent.Response;
import org.apache.hc.core5.http.ContentType;
import patternFinders.PatternFinder;
import utilities.Constants;
import static utilities.Constants.CLASSYFIRE_ONLINE_NODES_PATH;
import static utilities.Constants.NUMBER_COMPOUNDS_KEGG;

/**
 *
 * @author USUARIO
 * @version: 4.0, 20/07/2016. Modified by Alberto Gil de la Fuente
 */
public class DownloaderOfWebResources {

    /**
     * Deprecated because now the application process the InChIs through
     * molfiles and do not download. / kr.downloadKeggUniChemWebResources();
     *
     * @Deprecated
     *
     */
    public static void downloadKeggUniChemWebResources() {
        int keggSrc = 6;
        // HMDB Not necessary because they supply the InChI

        for (int i = 1; i < NUMBER_COMPOUNDS_KEGG; i++) {
            //for (int i = 23000; i < 23002; i++) {
            // Why it is used another object instead of itself?
            String code = generateKeggCode(i);
            try {
                // Download the content of kegg compound and write in a file text
                //https://www.ebi.ac.uk/unichem/rest/structure/C19603/6
                String fileName = "./resources/unichem/kegg/" + code + ".txt";
                String content = UriDevice.readHTTPS("https://www.ebi.ac.uk/unichem/rest/structure/" + code + "/" + keggSrc);
                if (content.contains("error:") && content.contains("is not recognized as a src_compound_id from src_id:")) {
                    System.out.println("compound with code " + code + " not available in uniChem\n");
                } else {
                    File file = new File(fileName);
                    if (file.exists()) {
                        file.delete();
                    }
                    MyFile.write(content, fileName);
                    System.out.println(code);
                }
            } // This exception is not working? I tried to download new information and it seems that does not work
            catch (FileNotFoundException fnfe) {
                System.out.println("resource https://www.ebi.ac.uk/unichem/rest/structure/" + code + " not available ");
            } catch (UnknownHostException uhe) {
                MyFile.write(code, "LOG_UNREAD_KEGG_COMPOUNDS.txt");
            }
        }
    }

    public static void downloadKeggWebResources() {
        // DownloaderOfWebResources kd = new DownloaderOfWebResources();

        for (int i = 1; i < NUMBER_COMPOUNDS_KEGG; i++) {
            String code = generateKeggCode(i);
            try {
                // Download the content of kegg compound and write in a file text
                String content = UriDevice.readString("https://rest.kegg.jp/get/cpd:" + code);
                if (content.equals("")) {
                    System.out.println("compound with code " + code + " does not exist\n");
                } else {
                    MyFile.write(content, Constants.KEGG_RESOURCES_PATH + code + ".txt");
                    //System.out.println(code);
                }
            } // This exception is not working? I tried to download new information and it seems that does not work
            catch (FileNotFoundException fnfe) {
                System.out.println("El recurso https://rest.kegg.jp/get/cpd:" + code + " no se corresponde con ningún compuesto");
            } catch (UnknownHostException uhe) {
                MyFile.write(code, "LOG_UNREAD_KEGG_COMPOUNDS.txt");
            }
        }
    }

    public static String generateKeggCode(int number) {
        if (number < 10) {
            return "C0000" + number;
        } else if (number < 100) {
            return "C000" + number;
        } else if (number < 1000) {
            return "C00" + number;
        } else if (number < 10000) {
            return "C0" + number;
        } else {
            return "C" + number;
        }
    }

    public static void downloadMetlinWebPage(int metlinID) {
        StringBuilder content = IoDevice.read("http://metlin.scripps.edu/metabo_info.php?molid=" + metlinID);
        MyFile.write(content.toString(), "./resources/metlin/" + metlinID + ".html");
        //System.out.println(metlinID);

    }

    /**
     * Download XML File from the HMDB compound HMDBID
     *
     * @param HMDBID
     */
    public static void downloadHMDBXMLFile(String HMDBID) {
        String fileName = Constants.HMDB_RESOURCES_PATH + HMDBID + ".xml";
        String uriString = Constants.HMDB_ONLINE_RESOURCES_PATH + HMDBID + ".xml";
        File HMDBFile = new File(fileName);
        if (HMDBFile.exists()) {
            HMDBFile.delete();
            System.out.println("Deleting file: " + HMDBID);
        }
        try {

            String content = UriDevice.readPlainFile(uriString);
            MyFile.writeFirstLine(content, Constants.HMDB_RESOURCES_PATH + HMDBID + ".xml");
            //System.out.println("Writing content ONLINE of: " + HMDBID);
        } catch (FileNotFoundException ex) {
            System.out.println("Error reading: " + uriString + " . -> It does not exist");
        } catch (IOException ex) {
            Logger.getLogger(DownloaderOfWebResources.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Download the classification file for the inchi key inchikey from
     * Classyfire.
     *
     * @param inchiKey
     */
    public static void downloadCLASSYFIREJSONFile(String inchiKey) {
        File classyifire_file = new File(Constants.CLASSYFIRE_RESOURCES_PATH + inchiKey + ".json");
        String uriString = Constants.CLASSYFIRE_ONLINE_RESOURCES_PATH + inchiKey + ".json";
        if (classyifire_file.exists()) {
            //System.out.println("Already exists: " + inchiKey);
            //return;
            classyifire_file.delete();
            System.out.println("Deleting file: " + inchiKey);
        }
        try {

            String content = UriDevice.readPlainFile(uriString);
            if (!content.equals("")) {
                MyFile.writeFirstLine(content, Constants.CLASSYFIRE_RESOURCES_PATH + inchiKey + ".json");
            } else {
                System.out.println("CREATE QUERY AND DOWNLOAD FROM QUERY: " + inchiKey);
            }

        } catch (FileNotFoundException ex) {
            System.out.println("Error reading: " + uriString + " . -> It does not exist");
        } catch (IOException ex) {
            System.out.println("NO CLASSIFIED INCHI: " + inchiKey);
            Logger.getLogger(DownloaderOfWebResources.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void downloadLipidMapsJSONFile(String LM_ID) {
        File LMJSONFile = new File(Constants.LIPIDMAPS_RESOURCES_JSON_PATH + LM_ID + ".json");
        if (LMJSONFile.exists()) {
            return;
            //LMJSONFile.delete();
            //System.out.println("Deleting file: " + LM_ID);
        }
        try {
            String uriString = Constants.LIPIDMAPS_ONLINE_RESOURCES_PATH + LM_ID + Constants.LIPIDMAPS_ONLINE_RESOURCES_PATH_END;
            String content = UriDevice.readPlainFile(uriString);
            MyFile.writeFirstLine(content, Constants.LIPIDMAPS_RESOURCES_JSON_PATH + LM_ID + ".json");
            //System.out.println("Writing content ONLINE of: " + LM_ID);
        } catch (IOException ex) {
            System.out.println("LM_ID FAIL DOWNLOAD: " + LM_ID);
            Logger.getLogger(DownloaderOfWebResources.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void downloadNodesFromClassyfireJSONFile() {
        File LMJSONFile = new File(Constants.CLASSYFIRE_RESOURCES_PATH + Constants.CLASSYFIRE_FILE_NAME);
        if (LMJSONFile.exists()) {
            LMJSONFile.delete();
            //System.out.println("Deleting file: " + LM_ID);
        }
        try {
            String uriString = Constants.CLASSYFIRE_ONLINE_NODES_PATH + Constants.CLASSYFIRE_FILE_NAME;
            String content = UriDevice.readPlainFile(uriString);
            MyFile.writeFirstLine(content, Constants.CLASSYFIRE_RESOURCES_PATH + Constants.CLASSYFIRE_FILE_NAME);
            //System.out.println("Writing content ONLINE of: " + LM_ID);
        } catch (IOException ex) {
            System.out.println("NODES FROM CLASSYFIRE FAIL DOWNLOAD: ");
            Logger.getLogger(DownloaderOfWebResources.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void downloadMineSDFResources() {
        try {
            MineSDFProcessor.downloadMineSDFResources();
        } catch (IOException ex) {
            Logger.getLogger(DownloaderOfWebResources.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void downloadMineJSONResources() {
        try {
            MineJSONProcessor.downloadMineJSONResources();
        } catch (IOException ex) {
            Logger.getLogger(DownloaderOfWebResources.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Download the classification file for the inchi key Classyfire.
     *
     * @param inchiKey inchi to query in classyfire
     * @param inchiFileName inchi name to save it locally
     * @throws exceptions.CompoundNotClassifiedException
     */
    public static void downloadCLASSYFIREAncestorsFile(String inchiKey, String inchiFileName) throws CompoundNotClassifiedException {
        String classyfireFileName = Constants.CLASSYFIRE_RESOURCES_PATH + inchiFileName + ".ancestors";
        String classyifireInchiFileName = Constants.CLASSYFIRE_RESOURCES_PATH + inchiKey + ".ancestors";
        File classyifireFile = new File(classyfireFileName);
        File classyifireInchiFile = new File(classyifireInchiFileName);
        if (classyifireInchiFile.exists()) {
            String content = MyFile.obtainContentOfABigFile(classyifireInchiFileName).toString();
            MyFile.writeFirstLine(content, classyfireFileName);
        } else if (classyifireFile.exists()) {
            //System.out.println("Already exists: " + inchiKey);
            String content = MyFile.obtainContentOfABigFile(classyfireFileName).toString();
            MyFile.writeFirstLine(content, classyifireInchiFileName);

            //classyifire_file.delete();
            //System.out.println("Deleting file: " + inchiKey);
        } else {
            String uriString = Constants.CLASSYFIRE_ONLINE_RESOURCES_PATH + inchiKey + "/ancestors";
            try {
                String content = UriDevice.readPlainFile(uriString);
                Thread.sleep(8000);
                if (!content.equals("")) {
                    MyFile.writeFirstLine(content, classyifireInchiFileName);
                    MyFile.writeFirstLine(content, classyfireFileName);
                } else {
                    throw new CompoundNotClassifiedException(inchiKey + " NOT CLASSIFIED", ERROR_TYPE.NOT_FOUND);
                }

            } catch (FileNotFoundException ex) {
                throw new CompoundNotClassifiedException(inchiKey + " NOT CLASSIFIED", ERROR_TYPE.NOT_FOUND);
            } catch (IOException ex) {
                System.out.println("NO CLASSIFIED INCHI: " + inchiKey);
                Logger.getLogger(DownloaderOfWebResources.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                System.out.println("Problem sleeping");
            }
        }
    }

    /**
     *
     * @param inchi
     * @param inchiKey
     * @return the classyfire inchi key from the inchi introduced. If the Inchi
     * key is already classified it returns the inchi key
     * @throws exceptions.CompoundNotClassifiedException
     * @throws java.io.IOException
     */
    public static String getInChIKeyClassyFireFromInChI(String inchi, String inchiKey) throws CompoundNotClassifiedException, IOException {
        File classyifire_file = new File(Constants.CLASSYFIRE_RESOURCES_PATH + inchiKey + ".ancestors");
        if (classyifire_file.exists()) {
            //System.out.println("Already exists: " + inchiKey);
            return inchiKey;

            //classyifire_file.delete();
            //System.out.println("Deleting file: " + inchiKey);
        }
        String uriString = Constants.CLASSYFIRE_ONLINE_RESOURCES_PATH + inchiKey + "/ancestors";
        String content = "";
        try {
            content = UriDevice.readPlainFile(uriString);
            if (!content.equals("")) {
                MyFile.writeFirstLine(content, Constants.CLASSYFIRE_RESOURCES_PATH + inchiKey + ".ancestors");
                return inchiKey;
            }
        } catch (FileNotFoundException ex) {
        }
        String labelQuery = inchiKey + "_label";
        Request request = Request.post(CLASSYFIRE_ONLINE_NODES_PATH + "queries.json");
        request.addHeader("Accept", "application/json");
        request.addHeader("Content-Type", "application/json");
        request.addHeader("Connection", "keep-alive");
        // TODO JSON
        String json = "{\"label\": \"" + labelQuery + "\", \"query_type\":\"STRUCTURE\", \"query_input\":\"" + inchi + "\"}";
        Request request2 = request.bodyString(json, ContentType.APPLICATION_JSON);
        Response response = request2.execute();

        Content jsonResponse = response.returnContent();
        String jsonResponseString = jsonResponse.asString();
        JsonObject jsonrepsonse = new JsonParser().parse(jsonResponseString).getAsJsonObject();

        Integer query_id = jsonrepsonse.get("id").getAsInt();
        String inchiKeyFromQueryId = getInChIKeyFromQueryId(query_id);
        return inchiKeyFromQueryId;
    }

    /**
     * Get the InchiKey of the entity
     *
     * @param query_id
     * @return the InChIKey
     * @throws CompoundNotClassifiedException if the query did not contain any
     * valid inchi key
     */
    public static String getInChIKeyFromQueryId(Integer query_id) throws CompoundNotClassifiedException {
        String uriString = CLASSYFIRE_ONLINE_NODES_PATH + "queries/" + query_id + ".json";

        try {
            Request request = Request.get(uriString);
            request.addHeader("Accept", "application/json");
            request.addHeader("Content-Type", "application/json");
            Response response = request.execute();
            Content jsonResponse = response.returnContent();
            String jsonResponseString = jsonResponse.asString();
            JsonObject jsonrepsonse = new JsonParser().parse(jsonResponseString).getAsJsonObject();
            JsonArray entities_array = jsonrepsonse.get("entities").getAsJsonArray();
            JsonObject properties = entities_array.get(0).getAsJsonObject();
            String inchiKey = properties.get("inchikey").getAsString();
            inchiKey = inchiKey.replaceFirst("InChIKey=", "");
            return inchiKey;
        } catch (IOException | IndexOutOfBoundsException | NullPointerException npe) {
            throw new CompoundNotClassifiedException("query_id Does not contain any structure classified: ", ERROR_TYPE.NOT_FOUND);
        }
    }

    /**
     *
     * @param inchi
     * @param inchiKey
     * @return
     * @throws exceptions.CompoundNotClassifiedException
     */
    public static List<String> getAncestorNodesCompoundClassyfireClassification(String inchi, String inchiKey) throws CompoundNotClassifiedException {
        if (inchiKey.length() != 27) {
            throw new CompoundNotClassifiedException("Wrong inchi key length. Check the inchi key", ERROR_TYPE.NOT_FOUND);
        }
        try {
            List<String> ancestorNodes = PatternFinder.getNodesAncestorsCLASSYFIREFromInChIkey(inchiKey, inchiKey);
            return ancestorNodes;
        } catch (CompoundNotClassifiedException ex) {

            try {
                String classifiedInchi = DownloaderOfWebResources.getInChIKeyClassyFireFromInChI(inchi, inchiKey);
                List<String> ancestorNodes = PatternFinder.getNodesAncestorsCLASSYFIREFromInChIkey(classifiedInchi, inchiKey);
                return ancestorNodes;
            } catch (CompoundNotClassifiedException ex1) {
                String errorLogFileName = Constants.CLASSYFIRE_RESOURCES_PATH + "not_classified.log";
                File classyfireErrorfile = new File(errorLogFileName);
                if (!classyfireErrorfile.exists()) {
                    try {
                        classyfireErrorfile.createNewFile();
                    } catch (IOException ioe) {
                        Logger.getLogger(DownloaderOfWebResources.class.getName()).log(Level.SEVERE, null, ioe);
                    }
                }
                MyFile.writeWithEndJumpLine("InchiKEY: " + inchiKey + " ; inchi: " + inchi + " NOT ABLE TO BE CLASSIFIED", errorLogFileName);
                throw ex1;
            } catch (IOException ex1) {
                String errorLogFileName = Constants.CLASSYFIRE_RESOURCES_PATH + "not_classified.log";
                File classyfireErrorfile = new File(errorLogFileName);
                if (!classyfireErrorfile.exists()) {
                    try {
                        classyfireErrorfile.createNewFile();
                    } catch (IOException ioe) {
                        Logger.getLogger(DownloaderOfWebResources.class.getName()).log(Level.SEVERE, null, ioe);
                    }
                }
                MyFile.writeWithEndJumpLine("InchiKEY: " + inchiKey + " ; inchi: " + inchi + " IOEXCEPTION", errorLogFileName);
                throw new CompoundNotClassifiedException("IOEXCEPTION CLASSYIFING: InchiKEY: " + inchiKey + " ; inchi: " + inchi, ERROR_TYPE.IO_EXCEPTION);
            }
        }
    }
}
