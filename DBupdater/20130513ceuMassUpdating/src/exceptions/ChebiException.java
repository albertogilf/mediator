/*
 * ChebiException.java
 *
 * Created on 08-nov-2019, 0:37:03
 *
 * Copyright(c) 2018 Ceu Mass Mediator All Rights Reserved.
 * This software is the proprietary information of Alberto Gil de la Fuente.
 *
 */

package exceptions;

/**
 * Exception of chebiElements that does not contain information
 * 
 * @version $Revision: 1.1.1.1 $
 * @since Build 4.1.0.0 08-nov-2019
 * 
 * @author Alberto Gil de la Fuente
 */
public class ChebiException extends Exception {

    /**
     * Creates a new instance of ChebiException
     */
    public ChebiException (String message)
    {
        super(message);
    }
}
