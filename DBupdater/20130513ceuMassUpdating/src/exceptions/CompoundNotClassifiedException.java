/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

import exceptions.ErrorTypes.ERROR_TYPE;

/**
 *
 * @author ceu
 */
public class CompoundNotClassifiedException extends Exception {

    
    private final ERROR_TYPE error_type;

    public CompoundNotClassifiedException(String message, ERROR_TYPE error_type) {
        super(message);
        this.error_type = error_type;
    }

    public ERROR_TYPE getError_type() {
        return error_type;
    }

    @Override
    public String toString() {
        return this.error_type.toString();
    }
}
