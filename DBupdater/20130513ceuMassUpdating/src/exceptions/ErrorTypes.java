/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author alberto.gildelafuent
 */
public class ErrorTypes {
    public enum ERROR_TYPE {
        NOT_FOUND, BAD_STRUCTURE, IO_EXCEPTION
    }
}
