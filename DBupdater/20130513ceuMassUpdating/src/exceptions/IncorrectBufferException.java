/*
 * IncorrectBufferException.java
 *
 * Created on 18-dic-2019, 11:29:41
 *
 * Copyright(c) 2018 Ceu Mass Mediator All Rights Reserved.
 * This software is the proprietary information of Alberto Gil de la Fuente.
 *
 */

package exceptions;

/**
 * Exception if the buffer is not correct
 * 
 * @version $Revision: 1.1.1.1 $
 * @since Build 4.1.2 18-dic-2019
 * 
 * @author Alberto Gil de la Fuente
 */
public class IncorrectBufferException extends Exception {

    public IncorrectBufferException(String errorMessage) {
        super(errorMessage);
    }
}
