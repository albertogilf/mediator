/*
 * IncorrectPolarityException.java
 *
 * Created on 19-dic-2019, 19:07:19
 *
 * Copyright(c) 2018 Ceu Mass Mediator All Rights Reserved.
 * This software is the proprietary information of Alberto Gil de la Fuente.
 *
 */

package exceptions;

/**
 * {Insert class description here}
 * 
 * @version $Revision: 1.1.1.1 $
 * @since Build {insert version here} 19-dic-2019
 * 
 * @author Alberto Gil de la Fuente
 */
public class IncorrectPolarityException extends Exception {

    public IncorrectPolarityException(String errorMessage) {
        super(errorMessage);
    }
}
