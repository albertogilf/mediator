/*
 * IncorrectTemperatureException.java
 *
 * Created on 18-dic-2019, 11:28:15
 *
 * Copyright(c) 2018 Ceu Mass Mediator All Rights Reserved.
 * This software is the proprietary information of Alberto Gil de la Fuente.
 *
 */
package exceptions;

/**
 * Exception if the temperature in CE is incorrect
 *
 * @version $Revision: 1.1.1.1 $
 * @since Build 4.1.2 18-dic-2019
 *
 * @author Alberto Gil de la Fuente
 */
public class IncorrectTemperatureException extends Exception {

    public IncorrectTemperatureException(String errorMessage) {
        super(errorMessage);
    }
}
