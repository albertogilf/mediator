/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

import exceptions.ErrorTypes.ERROR_TYPE;

/**
 *
 * @author ceu
 */
public class NodeNotFoundException extends Exception {

    
    private final ERROR_TYPE error_type;

    public NodeNotFoundException(String message) {
        super(message);
        this.error_type = ERROR_TYPE.NOT_FOUND;
    }

    public ERROR_TYPE getError_type() {
        return error_type;
    }

    @Override
    public String toString() {
        return this.error_type.toString();
    }
}
