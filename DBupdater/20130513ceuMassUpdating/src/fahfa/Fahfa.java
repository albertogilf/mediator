/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fahfa;

import java.util.List;
import java.util.Objects;
import model.Identifier;
import testersMSMS.Msms;

/**
 *
 * @author ceu
 */
public class Fahfa {

    private final Integer compound_id;
    private final Integer fahfa_id;
    private final Integer fa_length;
    private final Integer fa_double_bonds;
    private final Integer hfa_length;
    private final Integer hfa_double_bonds;
    private final Integer total_length;
    private final Integer total_double_bonds;
    private final Integer oh_position;
    private final String formula;
    private final Double monoisotopic_mass;
    private final String compound_name;
    private final List<String> synonyms;
    private final Identifier identifier;
    private final Msms spectrum_40V;

    /**
     * @param compound_id
     * @param fahfa_id
     * @param fa_length
     * @param fa_double_bonds
     * @param hfa_length
     * @param hfa_double_bonds
     * @param oh_position
     * @param formula
     * @param monoisotopic_mass
     * @param compound_name
     * @param synonyms
     * @param identifier
     * @param spectrum_40V
     */
    public Fahfa(Integer compound_id, Integer fahfa_id, Integer fa_length, Integer fa_double_bonds, Integer hfa_length,
            Integer hfa_double_bonds, Integer oh_position, String formula, Double monoisotopic_mass,
            String compound_name, List<String> synonyms,
            Identifier identifier,
            Msms spectrum_40V) {
        this.compound_id = compound_id;
        this.fahfa_id = fahfa_id;
        this.fa_length = fa_length;
        this.fa_double_bonds = fa_double_bonds;
        this.hfa_length = hfa_length;
        this.hfa_double_bonds = hfa_double_bonds;
        this.oh_position = oh_position;
        this.formula = formula;
        this.monoisotopic_mass = monoisotopic_mass;
        this.compound_name = compound_name;
        this.synonyms = synonyms;
        this.identifier = identifier;
        this.spectrum_40V = spectrum_40V;
        this.total_length = this.fa_length + this.hfa_length;
        this.total_double_bonds = this.fa_double_bonds + this.hfa_double_bonds;
    }

    public Integer getCompound_id() {
        return compound_id;
    }

    public Integer getFahfa_id() {
        return fahfa_id;
    }

    public Integer getFa_length() {
        return fa_length;
    }

    public Integer getFa_double_bonds() {
        return fa_double_bonds;
    }

    public Integer getHfa_length() {
        return hfa_length;
    }

    public Integer getHfa_double_bonds() {
        return hfa_double_bonds;
    }

    public Integer getOh_position() {
        return oh_position;
    }

    public String getFormula() {
        return formula;
    }

    public Double getMonoisotopic_mass() {
        return monoisotopic_mass;
    }

    public String getCompound_name() {
        return compound_name;
    }

    public List<String> getSynonyms() {
        return synonyms;
    }

    public Integer getTotal_length() {
        return total_length;
    }

    public Integer getTotal_double_bonds() {
        return total_double_bonds;
    }

    public Identifier getIdentifier() {
        return identifier;
    }

    public Msms getSpectrum_40V() {
        return spectrum_40V;
    }

    @Override
    public String toString() {
        return "Fahfa{" + "compound_id=" + compound_id + ", fahfa_id=" + fahfa_id + ", fa_length=" + fa_length + ", fa_double_bonds=" + fa_double_bonds + ", hfa_length=" + hfa_length + ", hfa_double_bonds=" + hfa_double_bonds + ", total_length=" + total_length + ", total_double_bonds=" + total_double_bonds + ", oh_position=" + oh_position + ", formula=" + formula + ", monoisotopic_mass=" + monoisotopic_mass + ", compound_name=" + compound_name + ", synonyms=" + synonyms + ", identifier=" + identifier + ", spectrum_40V=" + spectrum_40V + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.fahfa_id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Fahfa other = (Fahfa) obj;
        if (!Objects.equals(this.fahfa_id, other.fahfa_id)) {
            return false;
        }
        return true;
    }

}
