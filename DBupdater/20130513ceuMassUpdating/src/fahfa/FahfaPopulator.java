/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fahfa;

import databases.CMMDatabase;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Identifier;
import static databases.PubchemRest.getIdentifiersFromInChIPC;
import static databases.PubchemRest.getPCIDFromInchiKey;
import static testersMSMS.InsertMsms.insertMsms;
import testersMSMS.Msms;
import testersMSMS.Peak;

/**
 *
 * @author ceu
 */
public class FahfaPopulator {

    /**
     * Executes the main method for the CE_library_reader
     *
     * @param args
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {

        CMMDatabase db = new CMMDatabase();
        db.connectToDB("jdbc:mysql://localhost/compounds?useSSL=false", "alberto", "alberto");

        readPrecursorIonsFromFAHFACSV(db);
    }

    public static void readPrecursorIonsFromFAHFACSV(CMMDatabase db) throws IOException {
        String FAHFAPath = "resources/fahfa/FAHFA_positional_compounds.csv";
        BufferedReader br = null;
        String line;
        String csvSplitBy = ";";

        try {
            br = new BufferedReader(new FileReader(FAHFAPath));
            // Skip the headers (4)
            br.readLine();
            br.readLine();
            br.readLine();
            br.readLine();
            int lineNumber = 5;
            int compounds_present_pc = 0;
            int compounds_not_present_pc = 0;
            int compounds_present_in_db = 0;
            int msms_id = 10000001;
            int peak_id = 1000000010;

            while ((line = br.readLine()) != null) {
                // Create the variables for the CE compound
                Integer fahfa_id;
                Integer fa_length;
                Integer fa_double_bonds;
                Integer hfa_length;
                Integer hfa_double_bonds;
                Integer oh_position;
                String formula;
                Double monoisotopic_mass;
                String compound_name_prefix = "FAHFA(";
                String compound_name_sufix = ")";
                String compound_name;
                List<String> synonyms = new LinkedList<String>();
                Integer num_chains = 2;
                Integer total_carbons;
                Integer total_double_bonds;
                String oxidation = "";
                String adduct;

                // Objects to insert msms from FAHFA
                Msms msms;
                String hmdb_id = "NULL";
                String voltage_level = "high";
                int voltage = 40;
                String instrument_type = "NULL";
                int ionization_mode = 2;
                int peak_count = 6;
                int predicted = 1;
                Double peak1_mz;
                Integer peak1_intensity;
                Double peak2_mz;
                Integer peak2_intensity;
                Double peak3_mz;
                Integer peak3_intensity;
                Double peak4_mz;
                Integer peak4_intensity;
                Double peak5_mz;
                Integer peak5_intensity;
                Double peak6_mz;
                Integer peak6_intensity;
                List<Peak> peaks = new LinkedList<Peak>();

                Identifier identifier;
                String formulaFromInChI;
                String inchi;

                String[] compound = line.split(csvSplitBy);
                try {
                    fahfa_id = Integer.parseInt(compound[0]);
                    fa_length = Integer.parseInt(compound[2]);
                    fa_double_bonds = Integer.parseInt(compound[3]);
                    hfa_length = Integer.parseInt(compound[4]);
                    hfa_double_bonds = Integer.parseInt(compound[5]);
                    oh_position = Integer.parseInt(compound[6]);
                    formula = compound[7];
                    monoisotopic_mass = Double.parseDouble(compound[8]);
                    compound_name = compound_name_prefix + compound[9] + compound_name_sufix;
                    total_carbons = fa_length + hfa_length;
                    total_double_bonds = fa_double_bonds + hfa_double_bonds;

                    adduct = compound[12];
                    if (adduct.contains("[M-H]-")) {
                        ionization_mode = 2;
                    } else {
                        throw new IOException("Error reading the adduct; column M, line " + lineNumber);
                    }

                    peak1_mz = Double.parseDouble(compound[14]);
                    peak1_intensity = Integer.parseInt(compound[15]);
                    peak2_mz = Double.parseDouble(compound[16]);
                    peak2_intensity = Integer.parseInt(compound[17]);
                    peak3_mz = Double.parseDouble(compound[18]);
                    peak3_intensity = Integer.parseInt(compound[19]);
                    peak4_mz = Double.parseDouble(compound[20]);
                    peak4_intensity = Integer.parseInt(compound[21]);
                    peak5_mz = Double.parseDouble(compound[22]);
                    peak5_intensity = Integer.parseInt(compound[23]);
                    peak6_mz = Double.parseDouble(compound[24]);
                    peak6_intensity = Integer.parseInt(compound[25]);
                    peaks.add(new Peak(peak_id, peak1_mz, peak1_intensity, msms_id));
                    peaks.add(new Peak(peak_id + 1, peak2_mz, peak2_intensity, msms_id));
                    peaks.add(new Peak(peak_id + 2, peak3_mz, peak3_intensity, msms_id));
                    peaks.add(new Peak(peak_id + 3, peak4_mz, peak4_intensity, msms_id));
                    peaks.add(new Peak(peak_id + 4, peak5_mz, peak5_intensity, msms_id));
                    peaks.add(new Peak(peak_id + 5, peak6_mz, peak6_intensity, msms_id));
                    synonyms.add(compound[31]);
                    compound_name = compound_name + "\n" + compound[31];
                    inchi = compound[33];
                    // Insert compound in the database
                    // Get compound_id from database

                    int retries = 5;
                    int sleep;

                    try {
                        sleep = ThreadLocalRandom.current().nextInt(0, 1001);
                        identifier = getIdentifiersFromInChIPC(inchi, retries, sleep);
                        Integer pc_id = getPCIDFromInchiKey(identifier.getInchi_key());
                        // Insert into compound_identifiers
                        int compound_id = db.getCompoundIdFromInchiKey(identifier.getInchi_key());
                        if (compound_id == 0) {
                            compound_id = db.insertCompound(null, compound_name, formula, monoisotopic_mass, 0, 0, "CHNOPS", 1, 0, null);
                            db.insertIdentifiers(compound_id, identifier.getInchi(), identifier.getInchi_key(), identifier.getSmiles());
                        }

                        // Insert into compounds_pc
                        db.insertCompoundPC(compound_id, pc_id);
                        // Create MSMS
                        msms = new Msms(msms_id, voltage_level, voltage, instrument_type, ionization_mode, peaks.size(), hmdb_id, compound_id, 0, predicted);
                        msms.setPeaks(peaks);
                        // Insert into compounds_fahfa
                        Fahfa fahfa_compound = new Fahfa(compound_id, fahfa_id, fa_length, fa_double_bonds, hfa_length, hfa_double_bonds, oh_position, formula, monoisotopic_mass, compound_name, synonyms, identifier, msms);
                        db.insertCompoundFahfa(fahfa_compound);

                        // Insert into compounds_lipids_classification
                        db.insertLipidClassification(compound_id, compound_name_prefix, num_chains, total_carbons, total_double_bonds);
                        // Check if there are already chains
                        int chains = db.areThereAlreadyChains(compound_id);
                        if (chains == 0) {
                            int fa_chain_id = db.getChainID(fa_length, fa_double_bonds, oxidation);
                            int hfa_chain_id = db.getChainID(hfa_length, hfa_double_bonds, oxidation);
                            int repetitions = db.getRepetitionsSameChain(compound_id, fa_chain_id);
                            if (repetitions == 0) {
                                db.insertCompoundChainRelation(compound_id, fa_chain_id, 1);
                            } else {
                                db.updateCompoundChainRepetitions(compound_id, fa_chain_id, (repetitions + 1));
                            }
                            repetitions = db.getRepetitionsSameChain(compound_id, hfa_chain_id);
                            if (repetitions == 0) {
                                db.insertCompoundChainRelation(compound_id, hfa_chain_id, 1);
                            } else {
                                db.updateCompoundChainRepetitions(compound_id, hfa_chain_id, (repetitions + 1));
                            }
                        }
                        // Insert into msms
                        insertMsms(msms, db);

                        msms_id++;
                        peak_id = peak_id + 10;
                        compounds_present_pc++;

                    } catch (IOException ioe) {
                        compounds_not_present_pc++;
                        System.out.println("COMPOUND of line: " + lineNumber + " NOT INSERTED BECAUSE IT WAS NOT PRESENT IN PC");
                        System.out.println(ioe);
                    } catch (InterruptedException ex) {
                        System.out.println("Problem with the thread library");
                        Logger.getLogger(FahfaPopulator.class.getName()).log(Level.SEVERE, null, ex);
                    }

                } catch (NumberFormatException e) {
                    // if there is no chebid
                    throw new IOException("Error reading the fahfa attributes; column A-G, line " + lineNumber);
                }
                System.out.println(lineNumber);
                lineNumber++;
            }
            System.out.println("compounds present in CMM DB: " + compounds_present_in_db);
            System.out.println("compounds present in pc: " + compounds_present_pc);
            System.out.println("compounds NOT present in pc: " + compounds_not_present_pc);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }

}
