/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ioDevices;

import ioDevices.inputStreams.MyFileInputStream;
import ioDevices.readers.ReaderUnescapeHTML;
import java.io.*;

/**
 *
 * @author USUARIO
 */
public abstract class IoDevice {

    public static InputStream obtenerInputStream(String path) throws IOException {
        return (new MyFileInputStream()).obtenerStreamEntrada(path);
    }

    public static String readLine(BufferedReader br) {
        ReaderUnescapeHTML ru = new ReaderUnescapeHTML();
        return ru.readline(br);
    }

    public static StringBuilder read(String ruta) {
        StringBuilder content = new StringBuilder();
        content.delete(0, content.length());
        try (BufferedReader br = new BufferedReader(new InputStreamReader(obtenerInputStream(ruta), "UTF-8"))) {
            String line;

            while ((line = readLine(br)) != null) {
                content.append(line);
            }
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        }

        return content;
    }

    public static void write(String content, String path) {
        try {
            File file = new File(path);
            if (!file.exists()) {
                //System.out.println("\nCreating file: " + path);
                file.createNewFile();
            }
            FileWriter fileWritter;
            fileWritter = new FileWriter(file, true);
            BufferedWriter bw;
            bw = new BufferedWriter(fileWritter);
            bw.write("\n");
            bw.write(content);
            bw.close();
        } catch (IOException ex) {
            System.out.println("exception occurred writing in file" + path + "\n" + ex);
        }
    }

    public static void writeFirstLine(String content, String path) {
        try {
            File file = new File(path);
            if (!file.exists()) {
                //System.out.println("\nCreating file: " + path);
                file.createNewFile();
            }
            FileWriter fileWritter;
            fileWritter = new FileWriter(file, true);
            BufferedWriter bw;
            bw = new BufferedWriter(fileWritter);
            bw.write(content);
            bw.close();

        } catch (IOException ex) {
            System.out.println("exception occurred writing in file" + path + "\n" + ex);
        }
    }

    public static void writeWithEndJumpLine(String content, String path) {
        try {
            File file = new File(path);
            if (!file.exists()) {
                //System.out.println("\nCreating file: " + path);
                file.createNewFile();
            }
            FileWriter fileWritter;
            fileWritter = new FileWriter(file, true);
            BufferedWriter bw;
            bw = new BufferedWriter(fileWritter);
            bw.write(content);
            bw.write("\n");
            bw.close();
        } catch (IOException ex) {
            System.out.println("exception occurred writing in file" + path + "\n" + ex);
        }
    }
}
