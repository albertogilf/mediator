/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ioDevices;

import com.google.gson.JsonObject;
import java.io.*;

/**
 *
 * @author USUARIO
 */
public abstract class MyFile extends IoDevice {


    // Obtain all the content of the file ruta
    // Change variable names?
    public static StringBuilder obtainContentOfABigFile(String fileName) {

        StringBuilder sb = new StringBuilder();
        try {
            File f = new File(fileName);
            if (f.exists()) {
                try (BufferedReader bsr = new BufferedReader(new InputStreamReader(new FileInputStream(f)))) {
                    sb = new StringBuilder((int) f.length());

                    String linea;
                    while ((linea = bsr.readLine()) != null) {
                        sb.append(linea).append("\n");
                    }
                }
            }
        } catch (IOException ex) {
            System.err.println("Problema al leer desde el fichero.");
            System.err.println("MENSAJE DE LA EXCEPCIÓN: " + ex.toString());
        }
        // TODO ALBERTO CHANGE CALLS TO DELETE THE LAST \n
        //sb.substring(0, sb.length()-1);
        return sb;
    }

    public static void splitContentOfBigXMLHMDBFileByNode(String path, String fileName, String nodeName) {
        String inputFile = path + fileName;
        String startNode = "<" + nodeName + ">";
        String endNode = "</" + nodeName + ">";
        String tagHMDBID = "accession";
        String startTagHMDBID = "<" + tagHMDBID + ">";
        String endTagHMDBID = "</" + tagHMDBID + ">";
        String HMDBID = "HMDBFAIL";
        boolean extractedHMDBID;
        try {
            File f = new File(inputFile);
            try (BufferedReader bsr = new BufferedReader(new InputStreamReader(new FileInputStream(f)))) {
                //sb = new StringBuilder((int)f.length());
                //System.out.println("LENGTH of FILE: " + f.length);
                System.out.println("start: " + startNode + " final: " + endNode);
                String linea;
                while ((linea = bsr.readLine()) != null) {
                    // start a metabolite
                    if (linea.equals(startNode)) {
                        StringBuilder sb = new StringBuilder();
                        extractedHMDBID = false;
                        sb.append(linea).append("\n");
                        while (!linea.equals(endNode)) {
                            linea = bsr.readLine();
                            if (!extractedHMDBID && linea.contains(startTagHMDBID)) {

                                HMDBID = linea.replaceAll(startTagHMDBID, "");
                                HMDBID = HMDBID.replaceAll(endTagHMDBID, "");
                                HMDBID = HMDBID.replaceAll("\\s+", "");
                                extractedHMDBID = true;

                                //System.out.println("HMDBID: " + HMDBID);
                            }
                            sb.append(linea).append("\n");

                            // Close the file and write it into the file
                            if (linea.equals(endNode)) {
                                String metaboliteInformation = sb.substring(0, sb.length() - 1);
                                createHMDBMetaboliteFile(path, HMDBID, metaboliteInformation);
                            }
                        }
                    }
                    //    sb.append(linea).append("\n");
                }
            }
        } catch (IOException ex) {
            System.err.println("Problema al leer desde el fichero.");
            System.err.println("MENSAJE DE LA EXCEPCIÓN: " + ex.toString());
        }
    }

    public static void createHMDBMetaboliteFile(String path, String HMDBId, String metaboliteInformation) {
        String fileName = path + HMDBId + ".xml";
        String xmlversion = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        File HMDBFile = new File(fileName);
        if (HMDBFile.exists()) {
            HMDBFile.delete();
        }
        MyFile.writeFirstLine(xmlversion, fileName);
        MyFile.write(metaboliteInformation, fileName);
    }

    /**
     *
     * @param json JSON OBJECT
     * @param attributeName ATRIBBUTE NAME OF JSON OBJECT
     * @return the value of the attribute atrributeName
     */
    public static String getStringFromJSON(JsonObject json, String attributeName) {
        String result = "";
        if (json.get(attributeName) != null) {
            result = json.get(attributeName).getAsString();
        }

        return result;
    }

}
