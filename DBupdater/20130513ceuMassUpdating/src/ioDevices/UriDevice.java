/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ioDevices;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;

/**
 *
 * @author USUARIO
 */
public abstract class UriDevice {

    /**
     * Read request from HTTP websites
     *
     * @param uri
     * @return a string with the content of the uri
     * @throws FileNotFoundException
     * @throws UnknownHostException
     */
    public static String readString(String uri) throws FileNotFoundException, UnknownHostException {
        try {
            URL url = new URL(uri);
            // take note that special formatting is required for "+" signs
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            try (OutputStream os = conn.getOutputStream()) {
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.close();
            } catch (FileNotFoundException ex) {
                System.out.println(ex + " Not possible to open a new outputStream   ");
                return "";
            }

            conn.connect();
            try (InputStream is = conn.getInputStream()) {
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader rd = new BufferedReader(isr);
                StringBuilder sb = new StringBuilder();
                String line;

                while ((line = rd.readLine()) != null) {
                    sb.append(line).append('\n');
                }
                return sb.toString();
            } catch (FileNotFoundException ex) {
                System.out.println(ex + " Not possible to open a new inputStream \n");
                return "";
            }

        } catch (IOException ex) {
            Logger.getLogger(UriDevice.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
    }

    /**
     * Read request from HTTPS websites
     *
     * @param uri
     * @return
     * @throws FileNotFoundException
     * @throws UnknownHostException
     */
    public static String readHTTPS(String uri) throws FileNotFoundException, UnknownHostException {
        URL url;
        try {
            url = new URL(uri);
            // take note that special formatting is required for "+" signs
            HttpsURLConnection conn;
            conn = (HttpsURLConnection) url.openConnection();

            try (InputStream is = conn.getInputStream()) {
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader rd = new BufferedReader(isr);
                StringBuilder sb = new StringBuilder();
                String line;

                while ((line = rd.readLine()) != null) {
                    sb.append(line).append('\n');
                    //  System.out.println(line);
                }
                //System.out.println("\n SB: " + sb.toString());
                return sb.toString();
            } catch (FileNotFoundException ex) {
                System.out.println(ex + " Not possible to open a new inputStream \n");
                return "";
            }

        } catch (IOException ex) {
            Logger.getLogger(UriDevice.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
    }

    /**
     * Deprecated Method to get the InChI from the soap service in ChemSpider
     * Actually it is now working.
     *
     * @param molString
     * @return
     * @throws FileNotFoundException
     * @throws UnknownHostException
     */
    public static String getInChIFromChemSpider(String molString) throws FileNotFoundException, UnknownHostException {
        URL url;
        try {
            String a = "http://www.chemspider.com/InChI.asmx/MolToInChIKey?mol=a";
            url = new URL(a);
            // take note that special formatting is required for "+" signs
            HttpURLConnection conn;
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);

            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            System.out.println(conn.getResponseMessage());
            System.out.println(conn.getURL());

            conn.connect();

            try (InputStream is = conn.getInputStream()) {
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader rd = new BufferedReader(isr);
                StringBuilder sb = new StringBuilder();
                String line;

                while ((line = rd.readLine()) != null) {
                    sb.append(line).append('\n');
                    //  System.out.println(line);
                }
                System.out.println("\n SB: " + sb.toString());
                return sb.toString();
            } catch (FileNotFoundException ex) {
                System.out.println(ex + " Not possible to open a new inputStream \n");
                return "";
            }

        } catch (IOException ex) {
            Logger.getLogger(UriDevice.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
    }

    private static void print_https_cert(HttpsURLConnection con) {

        if (con != null) {

            try {

                System.out.println("Response Code : " + con.getResponseCode());
                System.out.println("Cipher Suite : " + con.getCipherSuite());
                System.out.println("\n");

                java.security.cert.Certificate[] certs = con.getServerCertificates();
                for (java.security.cert.Certificate cert : certs) {
                    System.out.println("Cert Type : " + cert.getType());
                    System.out.println("Cert Hash Code : " + cert.hashCode());
                    System.out.println("Cert Public Key Algorithm : "
                            + cert.getPublicKey().getAlgorithm());
                    System.out.println("Cert Public Key Format : "
                            + cert.getPublicKey().getFormat());
                    System.out.println("\n");
                }

            } catch (SSLPeerUnverifiedException e) {
            } catch (IOException e) {
            }

        }
    }

    /**
     * Read request from HTTP websites
     *
     * @param uri
     * @return
     * @throws FileNotFoundException
     * @throws UnknownHostException
     * @throws IOException
     */
    public static String readPlainFile(String uri) throws FileNotFoundException, UnknownHostException, IOException {
        try {
            URL url = new URL(uri);
            // take note that special formatting is required for "+" signs
            URLConnection conn = (URLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setDoInput(true);
            conn.setDoOutput(true);

            //conn.connect();
            try (InputStream is = conn.getInputStream()) {
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader rd = new BufferedReader(isr);
                StringBuilder sb = new StringBuilder();
                String line;

                while ((line = rd.readLine()) != null) {
                    sb.append(line).append('\n');
                }
                return sb.toString();
            } catch (FileNotFoundException ex) {
                throw ex;
            }

        } catch (IOException ex) {
            throw ex;
        }
    }
}
