/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ioDevices.inputStreams;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author USUARIO
 */
public class MyFileInputStream implements MyInputStream{

    @Override
    public InputStream obtenerStreamEntrada(String path)  throws IOException {
        return new FileInputStream(path);
    }
    
    
}
