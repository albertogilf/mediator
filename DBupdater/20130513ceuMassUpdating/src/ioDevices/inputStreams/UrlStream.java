/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ioDevices.inputStreams;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 *
 * @author USUARIO
 */
public class UrlStream implements MyInputStream{

    @Override
    public InputStream obtenerStreamEntrada(String path) throws IOException {
      
      URL url = new URL(path);
      URLConnection con = url.openConnection();

      return con.getInputStream();

    }
    
}
