/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ioDevices.readers;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author USUARIO
 */
public class ReaderEscapeHTML implements Reader {

    @Override
    public String readline(BufferedReader br) {
        try {
            return br.readLine();
        } catch (IOException ex) {
            Logger.getLogger(ReaderEscapeHTML.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
}
