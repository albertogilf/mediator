/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ioDevices.readers;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringEscapeUtils;

/**
 *
 * @author USUARIO
 */
public class ReaderUnescapeHTML implements Reader {

    @Override
    public String readline(BufferedReader br) {
        try {
            String line = br.readLine();
            return StringEscapeUtils.unescapeHtml4(line);
        } catch (IOException ex) {
            Logger.getLogger(ReaderUnescapeHTML.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
}
