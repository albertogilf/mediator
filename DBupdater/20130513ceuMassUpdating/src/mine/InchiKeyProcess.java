package mine;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;

/**
 * Generate files with inchi and inchi key unsing INCHI-BIN executable.
 * 
 * @author aesteban
 */
public class InchiKeyProcess implements Runnable {

    private final File file;
    private final File outputPath;
    private final File logsPath;

    /**
     * Default constructor.
     * 
     * @param file
     * @param outputPath
     * @param logsPath 
     */
    public InchiKeyProcess(File file, File outputPath, File logsPath) {
        this.file = file;
        this.outputPath = outputPath;
        this.logsPath = logsPath;
    }

    /**
     * Run method.
     */
    @Override
    public void run() {
        try {

            String command;
            /*
            command = "\"" + MineConstants.INCHIKEY_EXEC + "\" \""
                    + file.getAbsoluteFile() + "\" \""
                    + outputPath.getAbsolutePath() + File.separator + file.getName() + "\" \""
                    + logsPath.getAbsolutePath() + File.separator + file.getName() + "\" " + MineConstants.INCHIKEY_OPTIONS;
            */
            // For UNIX 
            
                    command = MineConstants.INCHIKEY_EXEC + " "
                    + file.getAbsoluteFile() + " "
                    + outputPath.getAbsolutePath() + File.separator + file.getName() + " "
                    + logsPath.getAbsolutePath() + File.separator + file.getName() + " " + MineConstants.INCHIKEY_OPTIONS;
            
                        
            Process proccess = Runtime.getRuntime().exec(command);
            proccess.waitFor();

            // Delete PRB generated files.
            FileUtils.deleteQuietly(new File(file.getAbsoluteFile() + ".prb"));

            MineConstants.filesProc++;
            if (MineConstants.filesProc % 500 == 0) {
                System.out.println("##                  --> Files processed: " + MineConstants.filesProc + " in " + (System.currentTimeMillis() - MineConstants.initialTime) / 1000 + " seg.");
                MineConstants.initialTime = System.currentTimeMillis();
            }
            proccess.destroy();

        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(InchiKeyProcess.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
