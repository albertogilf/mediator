package mine;

/**
 * MINE Constants.
 * 
 * @author aesteban
 */
public class MineConstants {

    public static final boolean PROCCESS_KEGG = true;
    public static final boolean PROCCESS_YMDB = true;
    public static final boolean PROCCESS_ECOCYC = true;

    public static final String MINE_SDF_URL = "http://lincolnpark.chem-eng.northwestern.edu/release/";
    public static final String MINE_SDF_PATH = "./resources/mine/molfile/";

    public static final String MINE_URL = "http://modelseed.org/services/mine-database";
    public static final String MINE_JSON_PATH = "./resources/mine/jsondata/";
    
/*
    // Windows values for INCHI-BIN.
    public static final String INCHIKEY_EXEC = "./resources/INCHI-1-BIN/windows/64bit/inchi-1.exe";
    public static final String INCHIKEY_OPTIONS = " /key ";
*/
    // Unix values for INCHI-BIN.
    public static final String INCHIKEY_EXEC = "C:/Users/alberto.gildelafuent/Desktop/alberto/repo_cmm/mediator/inchi/INCHI-1-BIN/windows/64bit/inchi-1";
    public static final String INCHIKEY_OPTIONS = " -key ";

    public static final int INCHIKEY_PARALEL_PROCESS = 4;
    public static long filesProc = 0;
    public static long initialTime;
}
