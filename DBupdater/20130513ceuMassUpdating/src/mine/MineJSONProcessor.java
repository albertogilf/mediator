package mine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.google.gson.Gson;
import databases.CMMDatabase;
import java.sql.SQLException;

import mine.bean.MineJSONMoleculeBean;
import mine.bean.MineJSONRequestBean;
import mine.bean.MineJSONResponseBean;
import patternFinders.PatternFinder;
import utilities.Utilities;

/**
 * Processor for MINE JSON files.
 *
 * @author aesteban
 */
public class MineJSONProcessor {

    /**
     * Downloads JSON file from MineConstants.MINE_URL.
     *
     * @throws IOException
     */
    public static void downloadMineJSONResources() throws IOException {
        // Download JSON files. 
        // Generation = 1 for generated compounds. Generation = 0 for not generated compounds.
        System.out.println("## MineJSONProcessor --> Downloading files from " + MineConstants.MINE_URL + " ...");
        if (MineConstants.PROCCESS_KEGG) {
            getDatabaseQueryInFile("KEGGexp2", "Generation", 1, "KEGGexp2_generation_1.txt");
        }
        if (MineConstants.PROCCESS_KEGG) {
            getDatabaseQueryInFile("KEGGexp2", "Generation", 0, "KEGGexp2_generation_0.txt");
        }
        if (MineConstants.PROCCESS_YMDB) {
            getDatabaseQueryInFile("YMDBexp2", "Generation", 1, "YMDBexp2_generation_1.txt");
        }
        if (MineConstants.PROCCESS_YMDB) {
            getDatabaseQueryInFile("YMDBexp2", "Generation", 0, "YMDBexp2_generation_0.txt");
        }
        if (MineConstants.PROCCESS_ECOCYC) {
            getDatabaseQueryInFile("EcoCycexp2", "Generation", 1, "EcoCycexp2_generation_1.txt");
        }
        if (MineConstants.PROCCESS_ECOCYC) {
            getDatabaseQueryInFile("EcoCycexp2", "Generation", 0, "EcoCycexp2_generation_0.txt");
        }
    }

    /**
     * Checks if each molecule in downloaded JSON correspond with a mole file of
     * the SDF files.
     *
     * @throws IOException
     */
    public static void processJSONFileForFixingMolFile() throws IOException {
        System.out.println("## MineJSONProcessor --> Checking files with generated molecules with Mol Files... ");
        if (MineConstants.PROCCESS_KEGG) {
            MineJSONProcessor.processJSONFileForFixingMolFile("KEGGexp2_generation_1.txt", new String[]{"KEGGMINE1", "KEGGMINE2", "KEGGMINE3"});
        }
        if (MineConstants.PROCCESS_YMDB) {
            MineJSONProcessor.processJSONFileForFixingMolFile("YMDBexp2_generation_1.txt", new String[]{"YMDBMINE"});
        }
        if (MineConstants.PROCCESS_ECOCYC) {
            MineJSONProcessor.processJSONFileForFixingMolFile("EcoCycexp2_generation_1.txt", new String[]{"EcoCycMINE"});
        }
    }

    /**
     * Creates a CSV with the molecule information of each JSON files.
     *
     * @throws IOException
     */
    public static void processJSONFileIntoCSV() throws IOException {
        System.out.println("## MineJSONProcessor --> Processing files into CVS... ");
        if (MineConstants.PROCCESS_KEGG) {
            MineJSONProcessor.processJSONFileIntoCSV("KEGGexp2_generation_1.txt");
        }
        if (MineConstants.PROCCESS_KEGG) {
            MineJSONProcessor.processJSONFileIntoCSV("KEGGexp2_generation_0.txt");
        }
        if (MineConstants.PROCCESS_YMDB) {
            MineJSONProcessor.processJSONFileIntoCSV("YMDBexp2_generation_1.txt");
        }
        if (MineConstants.PROCCESS_YMDB) {
            MineJSONProcessor.processJSONFileIntoCSV("YMDBexp2_generation_0.txt");
        }
        if (MineConstants.PROCCESS_ECOCYC) {
            MineJSONProcessor.processJSONFileIntoCSV("EcoCycexp2_generation_1.txt");
        }
        if (MineConstants.PROCCESS_ECOCYC) {
            MineJSONProcessor.processJSONFileIntoCSV("EcoCycexp2_generation_0.txt");
        }
    }

    /**
     * Inserts in BBDD the generated molecules information of each JSON files.
     *
     * @param db
     * @throws IOException
     */
    public static void processGeneratedJSONFileIntoBBDD(CMMDatabase db) throws IOException {
        System.out.println("## MineJSONProcessor --> Processing files into BBDD... ");
        if (MineConstants.PROCCESS_KEGG) {
            MineJSONProcessor.processJSONFileIntoBBDD("KEGGexp2_generation_1.txt", db, new String[]{"KEGGMINE1", "KEGGMINE2", "KEGGMINE3"});
        }
        if (MineConstants.PROCCESS_YMDB) {
            MineJSONProcessor.processJSONFileIntoBBDD("YMDBexp2_generation_1.txt", db, new String[]{"YMDBMINE"});
        }
        if (MineConstants.PROCCESS_ECOCYC) {
            MineJSONProcessor.processJSONFileIntoBBDD("EcoCycexp2_generation_1.txt", db, new String[]{"EcoCycMINE"});
        }
    }

    /**
     * Checks the existence if BBDD of the non generated molecules.
     *
     * @throws IOException
     */
    public static void processJSONFileForCheckingExistence() throws IOException {
        System.out.println("## MineJSONProcessor --> Checking files with not generated molecules... ");
        if (MineConstants.PROCCESS_KEGG) {
            MineJSONProcessor.processJSONFileForCheckingExistence("KEGGexp2_generation_0.txt");
        }
        if (MineConstants.PROCCESS_YMDB) {
            MineJSONProcessor.processJSONFileForCheckingExistence("YMDBexp2_generation_0.txt");
        }
        if (MineConstants.PROCCESS_ECOCYC) {
            MineJSONProcessor.processJSONFileForCheckingExistence("EcoCycexp2_generation_0.txt");
        }
    }

    /**
     * Checks if each molecule in downloaded JSON correspond with a mole file of
     * the SDF files.
     *
     * @param jsonFile
     * @param folders
     * @throws IOException
     */
    public static void processJSONFileForFixingMolFile(String jsonFile, String[] folders) throws IOException {

        File outputErrorFile = new File(MineConstants.MINE_JSON_PATH + jsonFile + ".FIXING.ERROR.TXT");
        File outputSuccessFile = new File(MineConstants.MINE_JSON_PATH + jsonFile + ".FIXING.SUCCESS.TXT");
        if (outputErrorFile.exists()) {
            System.out.println("##                   --> ERROR: OUTPUT FILE ALREADY EXISTS. PLEASE REMOVE IT. " + outputErrorFile.getAbsolutePath());

        } else if (outputSuccessFile.exists()) {
            System.out.println("##                   --> ERROR: OUTPUT FILE ALREADY EXISTS. PLEASE REMOVE IT. " + outputSuccessFile.getAbsolutePath());

        } else {
            // Obtains molecules from JSON file.
            List<MineJSONMoleculeBean> molecules = getMolecules(MineConstants.MINE_JSON_PATH + jsonFile);
            File file = null;
            boolean found;
            for (MineJSONMoleculeBean mol : molecules) {
                found = false;
                for (String folder : folders) {
                    file = new File(MineConstants.MINE_SDF_PATH + folder + File.separator + mol.get_id() + ".txt");
                    if (file.exists()) {
                        FileUtils.writeStringToFile(outputSuccessFile, mol.get_id() + " : " + file.getAbsolutePath() + "\n", "UTF-8", true);
                        found = true;
                        break;
                    }
                }

                if (folders.length > 0 && !found) {
                    FileUtils.writeStringToFile(outputErrorFile, mol.get_id() + " : " + file.getAbsolutePath() + "\n", "UTF-8", true);
                }
            }
            System.out.println("##                   --> Parsed file in CHECKING SUCCESS: " + outputSuccessFile.getAbsolutePath());
            System.out.println("##                   --> Parsed file in CHECKING ERROR: " + outputErrorFile.getAbsolutePath());
        }
    }

    /**
     * Creates a CVS with the molecule information of each JSON files.
     *
     * @param jsonFile
     * @throws IOException
     */
    public static void processJSONFileIntoCSV(String jsonFile) throws IOException {

        // Checks the existence of the file.
        File outputFile = new File(MineConstants.MINE_JSON_PATH + jsonFile + ".CSV");
        if (outputFile.exists()) {
            System.out.println("##                   --> ERROR: CSV Output file already exists. PLEASE REMOVE IT. " + outputFile.getAbsolutePath());

        } else {
            // Obtains molecules from JSON file.
            List<MineJSONMoleculeBean> molecules = getMolecules(MineConstants.MINE_JSON_PATH + jsonFile);

            // Header.
            StringBuilder line = new StringBuilder();
            line.append("_id").append(";");
            line.append("MINE_id").append(";");
            line.append("Names").append(";");
            line.append("Formula").append(";");
            line.append("Inchikey").append(";");
            line.append("Mass").append(";");
            line.append("NP_likeness").append(";");
            line.append("Generation").append(";");
            line.append("SMILES").append("\n");
            FileUtils.writeStringToFile(outputFile, line.toString(), "UTF-8", true);

            // Molecules.
            for (MineJSONMoleculeBean mol : molecules) {
                line = new StringBuilder();
                line.append(mol.get_id() != null ? mol.get_id() : "").append(";");
                line.append(mol.getMINE_id()).append(";");
                line.append(mol.getNames() != null ? mol.getNames() : "").append(";");
                line.append(mol.getFormula() != null ? mol.getFormula() : "").append(";");
                line.append(mol.getInchikey() != null ? mol.getInchikey() : "").append(";");
                line.append(String.valueOf(mol.getMass()).replace(".", ",")).append(";");
                line.append(String.valueOf(mol.getNP_likeness()).replace(".", ",")).append(";");
                line.append(mol.getGeneration() != null ? mol.getGeneration() : "").append(";");
                line.append(mol.getSMILES() != null ? mol.getSMILES() : "").append("\n");
                FileUtils.writeStringToFile(outputFile, line.toString(), "UTF-8", true);
            }

            System.out.println("##                   --> CSV generated file: " + outputFile.getAbsolutePath());
        }
    }

    /**
     * Inserts in BBDD the generated molecules information of each JSON files.
     *
     * @param jsonFile
     * @param db
     * @param folders
     * @throws IOException
     */
    public static void processJSONFileIntoBBDD(String jsonFile, CMMDatabase db, String[] folders) throws IOException {

        File outputFile = new File(MineConstants.MINE_JSON_PATH + jsonFile + ".SQL.SUCCESS.txt");
        File outputErrorFile = new File(MineConstants.MINE_JSON_PATH + jsonFile + ".SQL.ERROR.txt");
        if (outputFile.exists() || outputErrorFile.exists()) {
            System.out.println("##                   --> ERROR: BBDD Output files already exist. PLEASE REMOVE THEM. "
                    + outputFile.getAbsolutePath() + " --- " + outputErrorFile.getAbsolutePath());

        } else {
            // Obtains molecules from JSON file.
            List<MineJSONMoleculeBean> molecules = getMolecules(MineConstants.MINE_JSON_PATH + jsonFile);

            int count = 0;

            // Header.
            //String queryINSERT = "INSERT INTO compounds_gen (file_id,MINE_id,compound_name,formula,mass,np_likeness) VALUES ";	
            String queryINSERT = "INSERT INTO compounds_gen (MINE_file_id,MINE_id,compound_name,formula,mass,"
                    + "charge_type, charge_number, np_likeness,formula_type, formula_type_int) VALUES ";
            String queryInchiINSERT = "INSERT INTO compound_gen_identifiers (compound_id,inchi,inchi_key, smiles) VALUES ";

            StringBuilder queryVALUE = new StringBuilder();
            StringBuilder queryInchiVALUE;
            String[] inchiVal;

            for (MineJSONMoleculeBean mol : molecules) {
                try {
                    queryVALUE = new StringBuilder();
                    queryVALUE.append("('");
                    queryVALUE.append(mol.get_id() != null ? mol.get_id() : "").append("',");
                    if (mol.getMINE_id() > 0) {
                        queryVALUE.append(mol.getMINE_id()).append(",'");
                    } else {
                        queryVALUE.append("null,'");
                    }
                    queryVALUE.append(mol.getNames() != null ? escapeSQL(mol.getNames().toString()) : "").append("','");
                    String formula = mol.getFormula();
                    queryVALUE.append(formula != null ? mol.getFormula() : "").append("',");
                    queryVALUE.append(mol.getMass()).append(",");

                    String SMILES = mol.getSMILES() != null ? mol.getSMILES() : "";
                    int[] charges = PatternFinder.getChargeFromSmiles(SMILES);
                    int chargeType = charges[0];
                    int chargeNumber = charges[1];
                    queryVALUE.append(Integer.toString(chargeType)).append(",");
                    queryVALUE.append(Integer.toString(chargeNumber)).append(",");
                    queryVALUE.append(mol.getNP_likeness()).append(",");
                    String formulaType = PatternFinder.getTypeFromFormula(mol.getFormula());
                    if (formulaType.equalsIgnoreCase("null") || formulaType.equals("")) {
                        formulaType = "ALL";
                    }
                    int formulaTypeInt = Utilities.getIntChemAlphabet(formulaType);
                    formulaType = "'" + formulaType + "'";
                    queryVALUE.append(formulaType).append(",");
                    queryVALUE.append(formulaTypeInt);

                    queryVALUE.append(");");

                    int massMediatorId = db.executeNewInsertionWithEx(queryINSERT + queryVALUE.toString());

                    // Gets Inchi and Inchikey.
                    if (massMediatorId > 0) {

                        inchiVal = getInchiValues(mol.get_id(), folders);
                        if (inchiVal[0] != null && inchiVal[1] != null) {
                            queryInchiVALUE = new StringBuilder();
                            queryInchiVALUE.append("(").append(massMediatorId).append(",'").append(inchiVal[0]).
                                    append("','").append(inchiVal[1]).append("','").append(SMILES).append("');");
                            db.executeNewIDUWithEx(queryInchiINSERT + queryInchiVALUE.toString());
                        }
                    } else {
                        FileUtils.writeStringToFile(outputFile, " ERROR --> (RETURN massMediatorId==0)" + queryINSERT + queryVALUE.toString() + "\n", "UTF-8", true);
                    }

                } catch (SQLException | IOException ex) {
                    FileUtils.writeStringToFile(outputFile, ex.toString() + " --> " + queryINSERT + queryVALUE.toString() + "\n", "UTF-8", true);
                }
            }

            System.out.println("##                   --> Parsed file in SQL: " + MineConstants.MINE_JSON_PATH + outputFile.getAbsolutePath());
        }
    }

    /**
     * Obtains inchi and inchikey from file, ir it exists.
     *
     * @param id
     * @param folders
     * @return
     * @throws IOException
     */
    private static String[] getInchiValues(String id, String[] folders) throws IOException {
        String[] inchiVal = new String[2];
        boolean found = false;
        File file = null;
        for (String folder : folders) {
            file = new File(MineConstants.MINE_SDF_PATH + folder + ".INCHI" + File.separator + id + ".txt");
            if (file.exists()) {
                found = true;
                break;
            }
        }

        if (found) {
            List<String> lines = FileUtils.readLines(file, "UTF-8");
            for (String line : lines) {
                if (line.startsWith("InChI=")) {
                    inchiVal[0] = line.substring(6).trim();
                } else if (line.startsWith("InChIKey=")) {
                    inchiVal[1] = line.substring(9).trim();
                }
            }
        }

        return inchiVal;
    }

    /**
     * Escape ' caracter.
     *
     * @param value
     * @return
     */
    private static String escapeSQL(String value) {
        return value.replace("'", "''");
    }

    /**
     * Checks if each molecule in downloaded JSON correspond with a mole file of
     * the SDF files.
     *
     * @param jsonFile
     * @throws IOException
     */
    public static void processJSONFileForCheckingExistence(String jsonFile) throws IOException {

        File outputFile = new File(MineConstants.MINE_JSON_PATH + jsonFile + ".TEST.SQL");

        // Obtains molecules from JSON file.
        List<MineJSONMoleculeBean> molecules = getMolecules(MineConstants.MINE_JSON_PATH + jsonFile);

        // Header.
        String queryINSERT = "INSERT INTO (_id,MINE_id,Names,Formula, formula_type, "
                + "formula_type_int, Inchikey,Mass,NP_likeness,Generation,SMILES) VALUES ";

        StringBuilder queryVALUE;
        for (MineJSONMoleculeBean mol : molecules) {
            queryVALUE = new StringBuilder();
            queryVALUE.append("('");
            queryVALUE.append(mol.get_id() != null ? mol.get_id() : "").append("','");
            queryVALUE.append(mol.getMINE_id()).append("','");
            queryVALUE.append(mol.getNames() != null ? mol.getNames() : "").append("','");
            queryVALUE.append(mol.getFormula() != null ? mol.getFormula() : "").append("',");
            String formulaType = PatternFinder.getTypeFromFormula(mol.getFormula());
            if (formulaType.equalsIgnoreCase("null") || formulaType.equals("")) {
                formulaType = "ALL";
            }
            int formulaTypeInt = Utilities.getIntChemAlphabet(formulaType);
            formulaType = "'" + formulaType + "'";
            queryVALUE.append(formulaType).append(",");
            queryVALUE.append(formulaTypeInt).append(",");
            queryVALUE.append(mol.getInchikey() != null ? mol.getInchikey() : "").append(",'");
            queryVALUE.append(mol.getMass()).append("','");
            queryVALUE.append(mol.getNP_likeness()).append("','");
            queryVALUE.append(mol.getGeneration() != null ? mol.getGeneration() : "").append("';'");
            queryVALUE.append(mol.getSMILES() != null ? mol.getSMILES() : "").append("');\n");

            FileUtils.writeStringToFile(outputFile, queryINSERT + queryVALUE.toString(), "UTF-8", true);
        }

        System.out.println("##                   --> Parsed file in CHECKING:: " + outputFile.getAbsolutePath());
    }

    /**
     * List molecules in JSON file.
     *
     * @param jsonFile
     * @return
     * @throws FileNotFoundException
     */
    private static List<MineJSONMoleculeBean> getMolecules(String jsonFile) throws FileNotFoundException {
        BufferedReader reader = new BufferedReader(new FileReader(jsonFile));
        Gson gson = new Gson();
        MineJSONResponseBean response = gson.fromJson(reader, MineJSONResponseBean.class);
        List<List<MineJSONMoleculeBean>> result = response.getResult();
        List<MineJSONMoleculeBean> molecules = result.get(0);
        return molecules;
    }

    /**
     * Make JSON query to server.
     *
     * @param database
     * @param key
     * @param value
     * @param outputFile
     * @throws IOException
     */
    private static void getDatabaseQueryInFile(String database, String key, String value, String outputFileName) throws IOException {
        List<Object> params = new ArrayList();
        params.add(database);
        params.add("{\"$and\":[{\"" + key + "\":\"" + value + "\"}]}");
        params.add(null);
        params.add(null);
        makeConnection("mineDatabaseServices.database_query", params, outputFileName);
    }

    /**
     * Make JSON query to server.
     *
     * @param database
     * @param key
     * @param value
     * @param outputFile
     * @throws IOException
     */
    private static void getDatabaseQueryInFile(String database, String key, long value, String outputFileName) throws IOException {
        List<Object> params = new ArrayList();
        params.add(database);
        params.add("{\"$and\":[{\"" + key + "\":" + value + "}]}");
        params.add(null);
        params.add(null);
        makeConnection("mineDatabaseServices.database_query", params, outputFileName);
    }

    /**
     * Make JSON query to server.
     *
     * @param database
     * @param key
     * @param value
     * @param outputFile
     * @throws IOException
     */
    private static void getDatabaseQueryInFile(String database, String key, double value, String outputFileName) throws IOException {
        List<Object> params = new ArrayList();
        params.add(database);
        params.add("{\"$and\":[{\"" + key + "\":" + value + "}]}");
        params.add(null);
        params.add(null);
        makeConnection("mineDatabaseServices.database_query", params, outputFileName);
    }

    /**
     * Make JSON connection to server.
     *
     * @param method
     * @param params
     * @param outputFile
     * @throws IOException
     */
    private static void makeConnection(String method, List<Object> params, String outputFileName) throws IOException {

        // Checks the existence of the file.
        File outputFile = new File(MineConstants.MINE_JSON_PATH + outputFileName);
        if (outputFile.exists()) {
            System.out.println("##                   --> ERROR: JSON output file already exists. PLEASE REMOVE IT. " + outputFile.getAbsolutePath());

        } else {
            // Creates the target folder.
            new File(MineConstants.MINE_JSON_PATH).mkdirs();

            // Request configuration
            MineJSONRequestBean requestBean = new MineJSONRequestBean();
            requestBean.setId(Long.toString((long) (Math.random() * 10000000000000L)));
            requestBean.setMethod(method);
            requestBean.setVersion("1.1");
            requestBean.setParams(params);

            Gson gson = new Gson();
            String json = gson.toJson(requestBean);

            // Request.
            URL mineURL = new URL(MineConstants.MINE_URL);
            HttpURLConnection connection = (HttpURLConnection) mineURL.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);

            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Authorization", "");

            OutputStream outputStream = connection.getOutputStream();
            OutputStreamWriter outWriter = new OutputStreamWriter(outputStream, "UTF-8");

            outWriter.write(json);
            outWriter.flush();
            outWriter.close();

            // Takes output and writes it in a file.
            InputStream inputStream = connection.getInputStream();
            FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
            int bytesRead;
            byte[] buffer = new byte[4096];
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                fileOutputStream.write(buffer, 0, bytesRead);
            }

            fileOutputStream.close();
            inputStream.close();

            System.out.println("##                   --> JSON downloaded file: " + outputFile.getAbsolutePath());
        }
    }
}
