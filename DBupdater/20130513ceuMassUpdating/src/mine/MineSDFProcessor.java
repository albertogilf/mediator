package mine;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

/**
 * Processor for MINE SDF files.
 *
 * @author aesteban
 */
public class MineSDFProcessor {

    /**
     * Download files from MineConstants.MINE_SDF_URL and decompress them.
     *
     * KEGGMINE1.sdf.zip KEGGMINE2.sdf.zip KEGGMINE3.sdf.zip YMDBMINE.sdf.zip
     * EcoCycMINE.sdf.zip
     *
     */
    public static void downloadMineSDFResources() throws IOException {
        // Downloads SDF files.
        System.out.println("## MineSDFProcessor --> Downloading SDF files from " + MineConstants.MINE_SDF_URL + " ...");
        if (MineConstants.PROCCESS_KEGG) {
            downloadSDFFile("KEGGMINE1.sdf.zip");
        }
        if (MineConstants.PROCCESS_KEGG) {
            downloadSDFFile("KEGGMINE2.sdf.zip");
        }
        if (MineConstants.PROCCESS_KEGG) {
            downloadSDFFile("KEGGMINE3.sdf.zip");
        }
        if (MineConstants.PROCCESS_YMDB) {
            downloadSDFFile("YMDBMINE.sdf.zip");
        }
        if (MineConstants.PROCCESS_ECOCYC) {
            downloadSDFFile("EcoCycMINE.sdf.zip");
        }
    }

    /**
     * Decompress downloaded files.
     *
     * @throws IOException
     */
    public static void decompressMineSDFFiles() throws IOException {
        // Decompress SDF files.
        System.out.println("## MineSDFProcessor --> Decompressing SDF files... ");
        if (MineConstants.PROCCESS_KEGG) {
            decompressSDFFile("KEGGMINE1.sdf.zip");
        }
        if (MineConstants.PROCCESS_KEGG) {
            decompressSDFFile("KEGGMINE2.sdf.zip");
        }
        if (MineConstants.PROCCESS_KEGG) {
            decompressSDFFile("KEGGMINE3.sdf.zip");
        }
        if (MineConstants.PROCCESS_YMDB) {
            decompressSDFFile("YMDBMINE.sdf.zip");
        }
        if (MineConstants.PROCCESS_ECOCYC) {
            decompressSDFFile("EcoCycMINE.sdf.zip");
        }
    }

    /**
     * Process SDF for generating the mol files for each component.
     *
     * @throws IOException
     */
    public static void processSDFFiles() throws IOException {
        // Process SDF files.
        System.out.println("## MineSDFProcessor --> Processing SDF files... ");
        if (MineConstants.PROCCESS_KEGG) {
            processSDFFile("KEGGMINE1.sdf");
        }
        if (MineConstants.PROCCESS_KEGG) {
            processSDFFile("KEGGMINE2.sdf");
        }
        if (MineConstants.PROCCESS_KEGG) {
            processSDFFile("KEGGMINE3.sdf");
        }
        if (MineConstants.PROCCESS_YMDB) {
            processSDFFile("YMDBMINE.sdf");
        }
        if (MineConstants.PROCCESS_ECOCYC) {
            processSDFFile("EcoCycMINE.sdf");
        }
    }

    /**
     * Generate inchi files from Mol files.
     *
     * @throws IOException
     * @throws InterruptedException
     */
    public static void generateInchiFiles() throws IOException, InterruptedException {
        System.out.println("## MineSDFProcessor --> Generating INCHIKEY for folder " + MineConstants.MINE_SDF_PATH + " ...");
        if (MineConstants.PROCCESS_KEGG) {
            generateInchiFiles("KEGGMINE1");
        }
        if (MineConstants.PROCCESS_KEGG) {
            generateInchiFiles("KEGGMINE2");
        }
        if (MineConstants.PROCCESS_KEGG) {
            generateInchiFiles("KEGGMINE3");
        }
        if (MineConstants.PROCCESS_YMDB) {
            generateInchiFiles("YMDBMINE");
        }
        if (MineConstants.PROCCESS_ECOCYC) {
            generateInchiFiles("EcoCycMINE");
        }
    }

    /**
     * Download SDF files.
     *
     * @param fileName
     * @throws IOException
     */
    private static void downloadSDFFile(String fileName) {

        File file = new File(MineConstants.MINE_SDF_PATH + fileName);
        if (file.exists()) {
            System.out.println("##                  --> ERROR: File for download already exists. PLEASE REMOVE IT. " + file.getAbsolutePath());

        } else {
            try {
                // Creates target folder.
                new File(MineConstants.MINE_SDF_PATH).mkdirs();
                
                // HTTP conection to server.
                URL mineURL = new URL(MineConstants.MINE_SDF_URL + fileName);
                HttpURLConnection connection = (HttpURLConnection) mineURL.openConnection();
                connection.setRequestMethod("POST");
                connection.setDoOutput(true);
                connection.setDoInput(true);
                connection.setUseCaches(false);
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "application/json");
                connection.setRequestProperty("Authorization", "");
                
                try {
                    // Gets output and writes in a file.
                    InputStream inputStream = connection.getInputStream();
                    try {
                        FileOutputStream fileOutputStream = new FileOutputStream(file);
                        int bytesRead;
                        byte[] buffer = new byte[4096];
                        while ((bytesRead = inputStream.read(buffer)) != -1) {
                            fileOutputStream.write(buffer, 0, bytesRead);
                        }
                    }
                    catch (IOException ioe) {
                        System.out.println("## Error opening fileoutputStream: " + fileName);
                    }
                } catch (IOException ioe) {
                    System.out.println("## Error opening fileInputStream");
                }
                
                System.out.println("##                  --> Downloaded file: " + file.getAbsolutePath());
            } catch (MalformedURLException ex) {
                Logger.getLogger(MineSDFProcessor.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(MineSDFProcessor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Decompress SDF files.
     *
     * @param zipFileName
     * @throws IOException
     */
    private static void decompressSDFFile(String SDFFileName) throws IOException {

        File file = new File(MineConstants.MINE_SDF_PATH + SDFFileName);
        if (!file.exists()) {
            System.out.println("##                  --> ERROR: File for decompressing does not exists. PLEASE VERIFY IT. " + file.getAbsolutePath());

        } else {
            int len;
            File newFile;
            FileOutputStream fos;
            byte[] buffer = new byte[1024];
            ZipInputStream zipFile = new ZipInputStream(new FileInputStream(file));
            ZipEntry zipEntry = zipFile.getNextEntry();

            while (zipEntry != null) {
                newFile = new File(MineConstants.MINE_SDF_PATH + zipEntry.getName());
                fos = new FileOutputStream(newFile);
                while ((len = zipFile.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();
                zipEntry = zipFile.getNextEntry();
            }

            zipFile.closeEntry();
            zipFile.close();

            System.out.println("##                  --> Decompressed file: " + file.getAbsolutePath());
        }
    }

    /**
     * Proccess SDF file.
     *
     * @param filePath
     * @throws IOException
     */
    private static void processSDFFile(String fileName) throws IOException {

        File file = new File(MineConstants.MINE_SDF_PATH + fileName);
        String outputPath = MineConstants.MINE_SDF_PATH + fileName.substring(0, fileName.indexOf(".")) + File.separator;
        File outputPathFile = new File(outputPath);
        if (outputPathFile.exists() && outputPathFile.isDirectory() && outputPathFile.list().length > 0) {
            System.out.println("##                  --> ERROR: Mol file Output path already exists. PLEASE REMOVE IT. " + outputPath);

        } else {
            // Creates output folder.
            outputPathFile.mkdir();

            ArrayList<String> lines = null;
            String outputFileName;
            String line;
            LineIterator it = null;
            int index = 0;
            long time = System.currentTimeMillis();

            try {
                boolean startReading = false;
                boolean saveFile = false;

                it = FileUtils.lineIterator(file, "UTF-8");
                while (it.hasNext()) {
                    line = it.nextLine();

                    if (line != null) {
                        if (line.indexOf("OpenBabel") > 0) {
                            startReading = true;
                            lines = new ArrayList();
                            // Add two lines for generating the mol file with three break lines. 
                            // Mandatory for generation of InChI through InChI Software.
                            lines.add("");
                            lines.add("");

                        } else if (line.indexOf("<_id>") > 0) {
                            startReading = false;
                            saveFile = true;

                        } else if (saveFile) {
                            saveFile = false;
                            outputFileName = line.trim();
                            FileUtils.writeLines(new File(outputPath + outputFileName + ".txt"), lines);
                            index++;

                        } else if (startReading) {
                            lines.add(line);
                        }
                    }
                }

            } finally {
                LineIterator.closeQuietly(it);
            }

            System.out.println("##                  --> Processed file: " + file.getAbsolutePath() + " ---- [" + index + "] mol files in " + (System.currentTimeMillis() - time) + " ms.");
        }
    }

    /**
     * Genetare inchi file.
     *
     * @param path
     * @throws IOException
     * @throws InterruptedException
     */
    private static void generateInchiFiles(String path) throws IOException, InterruptedException {

        File inputPath = new File(MineConstants.MINE_SDF_PATH + path);
        File outputPath = new File(MineConstants.MINE_SDF_PATH + path + ".INCHI");
        File logsPath = new File(MineConstants.MINE_SDF_PATH + path + ".INCHILOGS");
        if (!inputPath.exists()) {
            System.out.println("##                  --> ERROR: Input Mol File Path not exists. PLEASE CHECK IT. " + outputPath.getAbsolutePath());

        } else if (outputPath.exists()) {
            System.out.println("##                  --> ERROR: Output Mol File Path already exists. PLEASE REMOVE IT. " + outputPath.getAbsolutePath());

        } else {
            outputPath.mkdir();
            logsPath.mkdirs();
            Collection<File> files = FileUtils.listFiles(inputPath, new String[]{"txt"}, false);
            System.out.println("##                  --> Files to process in " + path + ": " + files.size());
            ExecutorService executor = Executors.newFixedThreadPool(MineConstants.INCHIKEY_PARALEL_PROCESS);
            MineConstants.initialTime = System.currentTimeMillis();
            for (File file : files) {
                InchiKeyProcess process = new InchiKeyProcess(file, outputPath, logsPath);
                executor.execute(process);
            }
            executor.shutdown();
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        }
    }

}
