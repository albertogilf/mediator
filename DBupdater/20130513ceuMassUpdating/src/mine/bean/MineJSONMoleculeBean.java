package mine.bean;

import java.util.List;

/**
 * JSON bean for molecules.
 * 
 * @author aesteban
 */
public class MineJSONMoleculeBean {

    private String SMILES;
    private double NP_likeness;
    private String Generation;
    private int MINE_id;
    private String Inchikey;
    private double Mass;
    private List<String> Names;
    private String Formula;
    private String _id;

    /**
     * @return the sMILES
     */
    public String getSMILES() {
            return SMILES;
    }

    /**
     * @return the nP_likeness
     */
    public double getNP_likeness() {
        return NP_likeness;
    }

    /**
     * @return the generation
     */
    public String getGeneration() {
            return Generation;
    }

    /**
     * @return the mINE_id
     */
    public int getMINE_id() {
            return MINE_id;
    }

    /**
     * @return the inchikey
     */
    public String getInchikey() {
            return Inchikey;
    }

    /**
     * @return the mass
     */
    public double getMass() {
        return Mass;
    }

    /**
     * @return the names
     */
    public List<String> getNames() {
            return Names;
    }

    /**
     * @return the formula
     */
    public String getFormula() {
            return Formula;
    }

    /**
     * @return the id
     */
    public String get_id() {
            return _id;
    }

    /**
     * @param sMILES the sMILES to set
     */
    public void setSMILES(String sMILES) {
            SMILES = sMILES;
    }

    /**
     * @param nP_likeness the nP_likeness to set
     */
    public void setNP_likeness(double nP_likeness) {
        NP_likeness = nP_likeness;
    }

    /**
     * @param generation the generation to set
     */
    public void setGeneration(String generation) {
            Generation = generation;
    }

    /**
     * @param mINE_id the mINE_id to set
     */
    public void setMINE_id(int mINE_id) {
            MINE_id = mINE_id;
    }

    /**
     * @param inchikey the inchikey to set
     */
    public void setInchikey(String inchikey) {
            Inchikey = inchikey;
    }

    /**
     * @param mass the mass to set
     */
    public void setMass(double mass) {
        Mass = mass;
    }

    /**
     * @param names the names to set
     */
    public void setNames(List<String> names) {
            Names = names;
    }

    /**
     * @param formula the formula to set
     */
    public void setFormula(String formula) {
            Formula = formula;
    }

    /**
     * @param _id the id to set
     */
    public void set_id(String _id) {
            this._id = _id;
    }
	
}
