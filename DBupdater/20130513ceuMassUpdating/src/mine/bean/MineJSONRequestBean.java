package mine.bean;

import java.util.List;

/**
 * JSON Bean for request.
 * 
 * @author aesteban
 */
public class MineJSONRequestBean {

	private String id = null;
	private String method = null;
	private String version = null;
	
	private List<Object> params = null;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the method
	 */
	public String getMethod() {
		return method;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @return the params
	 */
	public List<Object> getParams() {
		return params;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @param method the method to set
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * @param params the params to set
	 */
	public void setParams(List<Object> params) {
		this.params = params;
	}


	
}
