package mine.bean;

import java.util.List;

/**
 * JSON Bean for response.
 * 
 * @author aesteban
 */
public class MineJSONResponseBean {

	private String version;
	private String id;
	private List<List<MineJSONMoleculeBean>> result;
	
	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * @return the result
	 */
	public List<List<MineJSONMoleculeBean>> getResult() {
		return result;
	}
	
	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * @param result the result to set
	 */
	public void setResult(List<List<MineJSONMoleculeBean>> result) {
		this.result = result;
	}
	
	
	
}
