/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Set;

/**
 *
 * @author alberto.gildelafuent
 */
public class AspergillusCompound extends Compound {

    private final String knapSackId;
    private final Integer pubChemId;
    private final Integer chebId;
    private final Integer npatlasId;
    private final String biologicalActivity;

    public AspergillusCompound(String knapSackId, Integer npatlasId, Integer pubChemId, Integer chebId, String biologicalActivity, Integer compound_id, String name, String casId,
            String formula, Double mass, Integer compound_status, Integer compound_type, Double logP, Identifier identifiers) {
        super(compound_id, name, casId, formula, mass, compound_status, compound_type, logP, identifiers);
        this.knapSackId = knapSackId;
        this.npatlasId = npatlasId;
        this.pubChemId = pubChemId;
        this.chebId = chebId;
        this.biologicalActivity = biologicalActivity;
    }

    public AspergillusCompound(String knapSackId, Integer npatlasId, Integer pubChemId, Integer chebId, String biologicalActivity, Integer compound_id, String name, String casId,
            String formula, Double mass, Integer compound_status, Integer compound_type, Double logP, Identifier identifiers,
            ClassyfireClassification classyfireClassifcation, Set<Reference> references, Set<OrganismClassification> organisms) {
        super(compound_id, name, casId, formula, mass, compound_status, compound_type, logP, identifiers, classyfireClassifcation, references, organisms);
        this.knapSackId = knapSackId;
        this.npatlasId = npatlasId;
        this.pubChemId = pubChemId;
        this.chebId = chebId;
        this.biologicalActivity = biologicalActivity;
    }

    public String getKnapSackId() {
        return knapSackId;
    }

    public Integer getNpatlasId() {
        return npatlasId;
    }

    public Integer getPubChemId() {
        return pubChemId;
    }

    public Integer getChebId() {
        return chebId;
    }

    public String getBiologicalActivity() {
        return biologicalActivity;
    }

    @Override
    public String toString() {
        return "AspergillusCompound{" + "knapSackId=" + knapSackId + ", pubChemId=" + pubChemId + ", chebId=" + chebId + super.toString() + '}';
    }

}
