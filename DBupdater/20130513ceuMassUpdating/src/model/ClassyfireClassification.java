/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Objects;

/**
 *
 * @author alberto.gildelafuent
 */
public class ClassyfireClassification {

    private final Integer node_id;
    private final String kingdom;
    private final String super_class;
    private final String main_class;
    private final String sub_class;
    private final String direct_parent;
    private final String level_7;
    private final String level_8;
    private final String level_9;
    private final String level_10;
    private final String level_11;

    public ClassyfireClassification(Integer node_id, String kingdom, String super_class, String main_class, String sub_class, String direct_parent, String level_7, String level_8, String level_9, String level_10, String level_11) {
        this.node_id = node_id;
        this.kingdom = kingdom;
        this.super_class = super_class;
        this.main_class = main_class;
        this.sub_class = sub_class;
        this.direct_parent = direct_parent;
        this.level_7 = level_7;
        this.level_8 = level_8;
        this.level_9 = level_9;
        this.level_10 = level_10;
        this.level_11 = level_11;
    }

    public Integer getNode_id() {
        return node_id;
    }

    public String getKingdom() {
        return kingdom;
    }

    public String getSuper_class() {
        return super_class;
    }

    public String getMain_class() {
        return main_class;
    }

    public String getSub_class() {
        return sub_class;
    }

    public String getDirect_parent() {
        return direct_parent;
    }

    public String getLevel_7() {
        return level_7;
    }

    public String getLevel_8() {
        return level_8;
    }

    public String getLevel_9() {
        return level_9;
    }

    public String getLevel_10() {
        return level_10;
    }

    public String getLevel_11() {
        return level_11;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.node_id);
        hash = 59 * hash + Objects.hashCode(this.kingdom);
        hash = 59 * hash + Objects.hashCode(this.super_class);
        hash = 59 * hash + Objects.hashCode(this.main_class);
        hash = 59 * hash + Objects.hashCode(this.sub_class);
        hash = 59 * hash + Objects.hashCode(this.direct_parent);
        hash = 59 * hash + Objects.hashCode(this.level_7);
        hash = 59 * hash + Objects.hashCode(this.level_8);
        hash = 59 * hash + Objects.hashCode(this.level_9);
        hash = 59 * hash + Objects.hashCode(this.level_10);
        hash = 59 * hash + Objects.hashCode(this.level_11);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClassyfireClassification other = (ClassyfireClassification) obj;
        if (!Objects.equals(this.kingdom, other.kingdom)) {
            return false;
        }
        if (!Objects.equals(this.super_class, other.super_class)) {
            return false;
        }
        if (!Objects.equals(this.main_class, other.main_class)) {
            return false;
        }
        if (!Objects.equals(this.sub_class, other.sub_class)) {
            return false;
        }
        if (!Objects.equals(this.direct_parent, other.direct_parent)) {
            return false;
        }
        if (!Objects.equals(this.level_7, other.level_7)) {
            return false;
        }
        if (!Objects.equals(this.level_8, other.level_8)) {
            return false;
        }
        if (!Objects.equals(this.level_9, other.level_9)) {
            return false;
        }
        if (!Objects.equals(this.level_10, other.level_10)) {
            return false;
        }
        if (!Objects.equals(this.level_11, other.level_11)) {
            return false;
        }
        if (!Objects.equals(this.node_id, other.node_id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ClassyfireClassification{" + "node_id=" + node_id + ", kingdom=" + kingdom + ", super_class=" + super_class + ", main_class=" + main_class + ", sub_class=" + sub_class + ", direct_parent=" + direct_parent + ", level_7=" + level_7 + ", level_8=" + level_8 + ", level_9=" + level_9 + ", level_10=" + level_10 + ", level_11=" + level_11 + '}';
    }

}
