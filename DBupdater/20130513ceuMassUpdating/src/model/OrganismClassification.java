/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import exceptions.ErrorTypes;
import exceptions.OrganismNotFoundException;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author alberto.gildelafuent
 */
public class OrganismClassification implements Comparable<OrganismClassification> {

    private Integer organismId;
    private final Integer organismLevel;
    private final String kingdom;
    private final String family;
    //private final String genus;
    private final String specie;
    private final String subspecie;
    private final Set<Reference> references;

    /**
     *
     * @param node_id
     * @param kingdom
     * @param family
     * @param specie
     * @param references
     */
    public OrganismClassification(Integer node_id, String kingdom, String family, String specie, Set<Reference> references) throws Exception {
        this(node_id, kingdom, family, specie, null, references);
    }

    /**
     *
     * @param node_id
     * @param kingdom
     * @param family
     * @param specie
     * @param subspecie
     * @param references
     */
    public OrganismClassification(Integer node_id, String kingdom, String family, String specie, String subspecie, Set<Reference> references) throws Exception {

        this.organismId = node_id;
        this.kingdom = kingdom;
        this.family = family;

        this.specie = specie;
        this.subspecie = subspecie;
        this.references = references;
        this.organismLevel = OrganismClassification.processOrganismLevel(kingdom, family, specie, subspecie);
        //this.organismLevel = OrganismClassification.processOrganismLevel(kingdom, family, genus, specie, subspecie);
        

        /*
        
        String[] wordSpecies = genusAndSpecie.split("\\s");
        String genus = null;
        String specie = null;
        if (wordSpecies.length > 2) {
            throw new Exception("GenusAndSpecie cannot have more than 1 space");
        } else if (wordSpecies.length == 2) {
            genus = wordSpecies[0];
            specie = wordSpecies[1];
        } else {
            genus = wordSpecies[0];
        }
        if (kingdom == null) {
            throw new Exception("Organism should have a kingdom");
        } else if (subspecie != null && (specie == null || genus == null || family == null)) {
            throw new Exception("Subspecie should have a specie, a genus and a family");
        } else if (specie != null && (genus == null || family == null)) {
            throw new Exception("Specie should have a genus and a family");
        } else if (genus != null && family == null) {
            throw new Exception("genus should have a family");
        }
        this.genus = genus;
         */
    }

    /**
     *
     * @param node_id
     * @param kingdom
     * @param family
     * @param genus
     * @param specie
     * @param subspecie
     * @param references
     */
    public OrganismClassification(Integer node_id, String kingdom, String family, String genus, String specie, String subspecie, Set<Reference> references) throws Exception {
        if (kingdom == null) {
            throw new Exception("Organism should have a kingdom");
        } else if (subspecie != null && (specie == null || genus == null || family == null)) {
            throw new Exception("Subspecie should have a specie, a genus and a family");
        } else if (specie != null && (genus == null || family == null)) {
            throw new Exception("Specie should have a genus and a family");
        } else if (genus != null && family == null) {
            throw new Exception("genus should have a family");
        }
        this.organismId = node_id;
        this.kingdom = kingdom;
        this.family = family;
        //this.genus = genus;
        this.specie = specie;

        this.subspecie = subspecie;
        this.organismLevel = OrganismClassification.processOrganismLevel(kingdom, family, genus, specie, subspecie);
        this.references = references;
    }

    public Integer getOrganismId() {
        return organismId;
    }

    public Integer getOrganismLevel() {
        return organismLevel;
    }

    public void setOrganismId(Integer organismId) {
        this.organismId = organismId;
    }

    public String getKingdom() {
        return kingdom;
    }

    /*
    public String getGenus() {
        return genus;
    }
     */
    public String getFamily() {
        return family;
    }

    public String getSpecie() {
        return specie;
    }

    public String getSubspecie() {
        return this.subspecie;
    }

    public Set<Reference> getReferences() {
        return references;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.organismId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OrganismClassification other = (OrganismClassification) obj;
        if (!Objects.equals(this.kingdom, other.kingdom)) {
            return false;
        }
        if (!Objects.equals(this.family, other.family)) {
            return false;
        }
        /*
        if (!Objects.equals(this.genus, other.genus)) {
            return false;
        }
         */
        if (!Objects.equals(this.specie, other.specie)) {
            return false;
        }
        if (!Objects.equals(this.subspecie, other.subspecie)) {
            return false;
        }
        if (!Objects.equals(this.organismId, other.organismId)) {
            return false;
        }
        return true;
    }
    
    private static Integer processOrganismLevel(String kingdom, String family, String specie, String subspecie) throws Exception {
        if (subspecie != null) {
            return 4;
        } else if (subspecie == null && specie != null) {
            return 3;
        } else if (specie == null && family != null) {
            return 2;
        } else if (family == null && kingdom != null) {
            return 1;
        } else {
            throw new Exception("Organism should have a kingdom");
        }
    }

    
    private static Integer processOrganismLevel(String kingdom, String family, String genus, String specie, String subspecie) throws Exception {
        if (subspecie != null) {
            return 5;
        } else if (subspecie == null && specie != null) {
            return 4;
        } else if (specie == null && genus != null) {
            return 3;
        } else if (genus == null && family != null) {
            return 2;
        } else if (family == null && kingdom != null) {
            return 3;
        } else {

            throw new Exception("Organism should have a kingdom");
        }
    }
    
    public static String getKingdomFromNPAtlas(String kingdom) throws OrganismNotFoundException {
        kingdom = kingdom.trim().toLowerCase();
        if(kingdom.equals("bacterium"))
        {
            return "bacteria";
        }
        else if(kingdom.equals("fungus"))
        {
            return "fungi";
        }
        throw new OrganismNotFoundException(kingdom + "not found", ErrorTypes.ERROR_TYPE.NOT_FOUND);
    }
    

    @Override
    public String toString() {
        return "OrganismClassification{" + "node_id=" + organismId + ", kingdom=" + kingdom + ", family=" + family
                + ", specie=" + specie + ", subspecie=" + subspecie + ", references=" + references + '}';
        /*
        return "OrganismClassification{" + "node_id=" + organismId + ", kingdom=" + kingdom + ", family=" + family
                + ", genus=" + genus + ", specie=" + specie + ", subspecie=" + subspecie + ", references=" + references + '}';
         */
    }

    @Override
    public int compareTo(OrganismClassification organismClassification2) {
        int result = 0;
        if (this.kingdom == null && organismClassification2.kingdom == null) {
            return 0;
        } else if (this.kingdom == null && organismClassification2.kingdom != null) {
            return -1;
        } else if (this.kingdom != null && organismClassification2.kingdom == null) {
            return 1;
        } else {
            result = this.kingdom.compareToIgnoreCase(organismClassification2.kingdom);
            if (result == 0) {
                if (this.family == null && organismClassification2.family == null) {
                    return 0;
                } else if (this.family == null && organismClassification2.family != null) {
                    return -1;
                } else if (this.family != null && organismClassification2.family == null) {
                    return 1;
                } else {
                    result = this.family.compareToIgnoreCase(organismClassification2.family);
                    /*
                    if (result == 0) {  
                        if (this.genus == null && organismClassification2.genus == null) {
                            return 0;
                        } else if (this.genus == null && organismClassification2.genus != null) {
                            return -1;
                        } else if (this.genus != null && organismClassification2.genus == null) {
                            return 1;
                        } else {
                            result = this.genus.compareToIgnoreCase(organismClassification2.genus);
                     */
                    if (result == 0) {
                        if (this.specie == null && organismClassification2.specie == null) {
                            return 0;
                        } else if (this.specie == null && organismClassification2.specie != null) {
                            return -1;
                        } else if (this.specie != null && organismClassification2.specie == null) {
                            return 1;
                        } else {
                            result = this.specie.compareToIgnoreCase(organismClassification2.specie);
                            if (result == 0) {
                                if (this.subspecie == null && organismClassification2.subspecie == null) {
                                    return 0;
                                } else if (this.subspecie == null && organismClassification2.subspecie != null) {
                                    return -1;
                                } else if (this.subspecie != null && organismClassification2.subspecie == null) {
                                    return 1;
                                } else {
                                    return this.subspecie.compareToIgnoreCase(organismClassification2.subspecie);
                                }
                            }
                        }
                        //}
                        //}
                    }
                }
            }
        }
        return result;
    }

}
