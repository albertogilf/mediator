/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Objects;

/**
 *
 * @author alberto.gildelafuent
 */
public class Reference implements Comparable<Reference> {

    private Integer id; // id of compounds
    private final String referenceText;
    private String doi;
    private String link;

    public Reference(Integer id, String reference) {
        this(id, reference, null, null);
    }

    public Reference(Integer id, String reference, String doi, String link) {
        this.id = id;
        this.referenceText = reference;
        this.doi = doi;
        this.link = link;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getReferenceText() {
        return referenceText;
    }

    public String getDoi() {
        return doi;
    }

    public void setDoi(String doi) {
        this.doi = doi;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public String toString() {
        return "Reference{" + "id=" + id + ", reference=" + referenceText + ", doi=" + doi + ", link=" + link + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.id);
        hash = 37 * hash + Objects.hashCode(this.referenceText);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Reference other = (Reference) obj;
        if (!Objects.equals(this.referenceText, other.referenceText)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(Reference reference2) {
        if (this.referenceText == null && reference2.referenceText == null) {
            return 0;
        } else if (this.referenceText == null && reference2.referenceText != null) {
            return -1;
        } else if (this.referenceText != null && reference2.referenceText == null) {
            return 1;
        } else {
            return this.referenceText.compareToIgnoreCase(reference2.referenceText);
        }

    }

    /**
     * Returns the reference text in the format <Journal_name> <Year>, <volume>,
     * <issue>, <pages>
     *
     * @param journal_title
     * @param year
     * @param volume
     * @param issue
     * @param pages
     * @return
     */
    public static String referenceTextFromData(String journal_title, Integer year,
            String volume, String issue, String pages) {
        String reference_text = "";
        reference_text = reference_text + journal_title + " " + year;
        if (volume != null) {
            reference_text = reference_text + ", " + volume;
        }
        if (issue != null) {
            reference_text = reference_text + ", " + issue;
        }
        if (pages != null) {
            reference_text = reference_text + ", " + pages;
        }
        return reference_text;
    }

    /**
     * Returns the reference text in the format <Journal_name> <Year>, <volume>,
     * <issue>, <pages>
     *
     * @param journal_title
     * @param year
     * @param volume
     * @param issue
     * @param pages
     * @return
     */
    public static String referenceTextFromData(String journal_title, Integer year,
            Integer volume, Integer issue, String pages) {
        return Reference.referenceTextFromData(journal_title, year, pages, pages, pages);
    }

    /**
     * Returns the reference text in the format <Journal_name> <Year>, <volume>,
     * <issue>, <initial_page>-<end_page>
     *
     * @param journal_title
     * @param year
     * @param volume
     * @param issue
     * @param initial_page
     * @param end_page
     * @return
     */
    public static String referenceTextFromData(String journal_title, Integer year, Integer volume,
            Integer issue, Integer initial_page, Integer end_page) {
        String pages = "";
        if (initial_page != null) {
            pages = pages + initial_page;
        }
        if (end_page != null) {
            pages = pages + "-" + end_page;
        }
        return Reference.referenceTextFromData(journal_title, year, volume, issue, pages);
    }

}
