package newDatabasePopulators;

import databases.CMMDatabase;
import databasePopulators.MetlinDatabasePopulator;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang3.StringUtils;
import patternFinders.PatternFinder;
import utilities.Utilities;

/**
 * Agilent database populator. version 2.0
 *
 * Modified class for detecting cations from the source
 *
 * @author aesteban, Alberto Gil
 */
public class AgilentDatabasePopulator {

    public static final double ALLOWED_MASS_DIF = 0.01;

    public static final String AGILENT_FILE_PATH = "/home/alberto/alberto/repo/mediator/DBupdater/20130513ceuMassUpdating/resources/agilent/files/";
    public static final String AGILENT_FILE_PATH_CATIONS = "/home/alberto/alberto/repo/mediator/DBupdater/20130513ceuMassUpdating/resources/agilent/files/cations/";
    public static final String[] BAD_LINE_STARTS = new String[]{
        "Best Compound",
        "MassHunter PCDL Manager",
        "15/04/2016",
        "PCDL: D:\\METLIN PCDL",
        "Mass list file",
        "27/07/2016",
        "19/10/2017"};

    private Set casIDs = null;
    private Set hmdbIDs = null;
    private Set lmIDs = null;
    private Set keggIDs = null;

    private Set duplicatedCasIDs = null;
    private Set duplicatedHmdbIDs = null;
    private Set duplicatedLmIDs = null;
    private Set duplicatedKeggIDs = null;

    private File duplicatedFile = null;
    private File insertLinkFile = null;
    private File linkFile = null;
    private File linkUpdateFile = null;
    private File errorFile = null;

    private File LMNOTINCMMFILE = null;
    private File KEGGNOTINCMMFILE = null;
    private File HMDBNOTINCMMFILE = null;

    private File LMINCMMFILE = null;
    private File KEGGINCMMFILE = null;
    private File HMDBINCMMFILE = null;
    private File ONLYINMETLINFILE = null;
    private File updateCASFILE = null;
    private File updateInformationFromAgilentFILE = null;

    private File updatedCompoundsFile = null;

    /**
     * Default constructor.
     */
    public AgilentDatabasePopulator() {
    }

    /**
     * Populate DB with agilent files.
     *
     * @param db
     */
    public void populate(CMMDatabase db) {
        try {
            long time = System.currentTimeMillis();

            // Parse file into CSV output file.
            // Be careful! Order should be the next one. 1. Lipids, 2. metabolites 
            // 3. peptides and 4. rest of metabolites
            parseFileToCSV("lipids.txt", db, 36525, 1);
            parseFileAndCheckToBBDD("lipids.txt", db, 36525, 1);

            parseFileToCSV("metabolites.txt", db, 29442, 0);
            parseFileAndCheckToBBDD("metabolites.txt", db, 29442, 0);

            parseFileToCSV("AMRT_PCDL.txt", db, 77649, 0);
            parseFileAndCheckToBBDD("AMRT_PCDL.txt", db, 77649, 0);

            parseFileToCSV("peptidos1.txt", db, 0, 2);
            parseFileAndCheckToBBDD("peptidos1.txt", db, 0, 2);

            parseFileToCSV("peptidos2.txt", db, 0, 2);
            parseFileAndCheckToBBDD("peptidos2.txt", db, 0, 2);

            System.out.println("## TOTAL TIME --> " + (System.currentTimeMillis() - time) + " ms.");

        } catch (IOException | ParseException ex) {
            Logger.getLogger(AgilentDatabasePopulator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(AgilentDatabasePopulator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Populate DB with agilent files.
     *
     * @param db
     */
    public void updatePeptides(CMMDatabase db) {
        try {
            long time = System.currentTimeMillis();

            updatePeptidesBBDD("peptidos1.txt", db);
            updatePeptidesBBDD("peptidos2.txt", db);

            System.out.println("## TOTAL TIME --> " + (System.currentTimeMillis() - time) + " ms.");

        } catch (IOException | ParseException ex) {
            Logger.getLogger(AgilentDatabasePopulator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(AgilentDatabasePopulator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Get Information From Cations detected manually by Agilent.
     *
     * @param db
     */
    public void saveInfoFromCationsIntoCSV(CMMDatabase db) {
        try {
            long time = System.currentTimeMillis();
            /*
            parseCationsToCSV("lipids.txt", 36525, db);
            parseCationsToCSV("metabolites.txt", 29442, db);
            parseCationsToCSV("AMRT.txt", 77649, db);
             */
            parseCationsToCSV("lipids_cations.txt", db);
            parseCationsToCSV("metabolites_cations.txt", db);
            parseCationsToCSV("AMRT_cations.txt", db);
            System.out.println("## TOTAL TIME --> " + (System.currentTimeMillis() - time) + " ms.");

        } catch (IOException | ParseException ex) {
            Logger.getLogger(AgilentDatabasePopulator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(AgilentDatabasePopulator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Get information from cations and save it in a CVS output file.
     *
     * @param fileName
     * @throws IOException
     * @throws ParseException
     */
    private void parseCationsToCSV(String fileName, CMMDatabase db)
            throws IOException, ParseException, Exception {

        File inputFile = new File(AGILENT_FILE_PATH_CATIONS + fileName);

        if (!inputFile.exists()) {
            System.out.println("##                   --> ERROR: Intput file not exists. PLEASE CHECK IT. " + inputFile.getAbsolutePath());

        } else {

            File outputFile = new File(AGILENT_FILE_PATH_CATIONS + fileName + ".cations.csv");
            if (outputFile.exists()) {
                System.out.println("##                   --> ERROR: Output file already exists. PLEASE REMOVE IT. " + outputFile.getAbsolutePath());

            } else {

                File outputFileTXT = new File(AGILENT_FILE_PATH_CATIONS + fileName + ".cations.txt");
                if (outputFileTXT.exists()) {
                    System.out.println("##                   --> ERROR: Output file in txt already exists. PLEASE REMOVE IT. " + outputFile.getAbsolutePath());
                }
                casIDs = new HashSet();
                hmdbIDs = new HashSet();
                lmIDs = new HashSet();
                keggIDs = new HashSet();

                duplicatedCasIDs = new HashSet();
                duplicatedHmdbIDs = new HashSet();
                duplicatedLmIDs = new HashSet();
                duplicatedKeggIDs = new HashSet();

                Double massIntroduced = 500d;
                // Generates CSV.
                int countLine = 0;
                String line;
                StringBuilder outputLine;
                Object[] values;
                LineIterator it = FileUtils.lineIterator(inputFile, "ISO-8859-1");
                // Only look for cations if is there any of them
                while (it.hasNext()) {
                    line = it.nextLine();
                    if (isCompoundLine(line)) {
                        // Access to the real values accesing to different sources
                        // Order of the sources: 
                        // 1 -> LipidMaps, 2 -> HMDB, 3 -> KEGG, 4 -> Metlin
                        values = getValuesFromCationToCSV(line, db);
                        // If information was not obtained from other source

                        // Creates String for trace
                        String lineForTrace = "## CATION: ";
                        lineForTrace = lineForTrace + ("METLIN: " + (String) values[6] + " -> [CAS: ");
                        if (values[4] == null) {
                            lineForTrace = lineForTrace + "][HMDB: ";
                        } else {
                            lineForTrace = lineForTrace + (String) values[4] + " ][HMDB: ";
                        }
                        if (values[7] == null) {
                            lineForTrace = lineForTrace + "][LM: ";
                        } else {
                            lineForTrace = lineForTrace + (String) values[7] + " ][LM: ";
                        }
                        if (values[9] == null) {
                            lineForTrace = lineForTrace + "][KEGG: ";
                        } else {
                            lineForTrace = lineForTrace + (String) values[9] + " ][KEGG: ";
                        }
                        if (values[8] == null) {
                            lineForTrace = lineForTrace + "][MASS: ";
                        } else {
                            lineForTrace = lineForTrace + (String) values[8] + " ][MASS: ";
                        }
                        lineForTrace = lineForTrace + values[2].toString() + " ][MASS DETECTED: ";
                        Double massDetected = massIntroduced + ((Double) values[3] / 1000);
                        lineForTrace = lineForTrace + massDetected.toString() + " ][FORMULA: ";
                        lineForTrace = lineForTrace + (String) values[1] + " ][MASS OTHER SRC: ";
                        if (values[12] == null) {
                            lineForTrace = lineForTrace + "] [FORMULA OTHER SRC: ";
                        } else {
                            lineForTrace = lineForTrace + values[12].toString() + " ] [FORMULA OTHER SRC:";
                        }
                        if (values[13] == null) {
                            lineForTrace = lineForTrace + " -- ";
                        } else {
                            lineForTrace = lineForTrace + (String) values[13];
                        }
                        if (Math.abs(massDetected - (Double) values[12]) < 0.3d
                                && Math.abs(massDetected - (Double) values[12]) < -0.3d) {
                            lineForTrace = "DIFFERENCE MASS DETECTED BIGGER:" + lineForTrace;
                            System.out.println(lineForTrace);
                        }

                        int CMMID = (int) values[11];
                        if (CMMID >= 0) {
                            //FileUtils.writeStringToFile(outputFileTXT, lineForTrace + "\n", "ISO-8859-1", true);
                        } else {
                            System.out.println("ERROR: CMMID: " + CMMID + " STRANGE");
                        }
                        // Writes in CVS file.
                        outputLine = new StringBuilder();
                        for (Object value : values) {
                            outputLine.append(value != null ? value : "").append(";");
                        }
                        //FileUtils.writeStringToFile(outputFile, outputLine.toString() + "\n", "ISO-8859-1", true);

                        // Se almacenan los ids para encontrar repetidos en el siguiente paso.
                        // Metlin: 6, CAS: 4, HMDB: 7, LM:9, KEGG:8
                        checkIds(values[4], values[7], values[9], values[8]);
                    }
                    countLine++;
                }
            }
        }
    }

    /**
     * Parse file into CVS output file.
     *
     * @param fileName
     * @throws IOException
     * @throws ParseException
     */
    private void parseFileToCSV(String fileName, CMMDatabase db, int cationStart, int compoundType)
            throws IOException, ParseException, Exception {

        File inputFile = new File(AGILENT_FILE_PATH + fileName);

        if (!inputFile.exists()) {
            System.out.println("##                   --> ERROR: Intput file not exists. PLEASE CHECK IT. " + inputFile.getAbsolutePath());

        } else {

            File outputFile = new File(AGILENT_FILE_PATH + fileName + ".csv");
            if (outputFile.exists()) {
                System.out.println("##                   --> ERROR: Output file already exists. PLEASE REMOVE IT. " + outputFile.getAbsolutePath());

            } else {

                insertLinkFile = new File(AGILENT_FILE_PATH + fileName + ".agilent.INSERT_LINK.txt");
                if (insertLinkFile.exists()) {
                    insertLinkFile.delete();
                }

                linkFile = new File(AGILENT_FILE_PATH + fileName + ".agilent.LINK.txt");
                if (linkFile.exists()) {
                    linkFile.delete();
                }

                errorFile = new File(AGILENT_FILE_PATH + fileName + ".agilent.ERROR.txt");
                if (errorFile.exists()) {
                    errorFile.delete();
                }

                LMNOTINCMMFILE = new File(AGILENT_FILE_PATH + fileName + ".agilent.cation.LMNOTINCMM.txt");
                if (LMNOTINCMMFILE.exists()) {
                    LMNOTINCMMFILE.delete();
                }

                KEGGNOTINCMMFILE = new File(AGILENT_FILE_PATH + fileName + ".agilent.cation.KEGGNOTINCMM.txt");
                if (KEGGNOTINCMMFILE.exists()) {
                    KEGGNOTINCMMFILE.delete();
                }

                HMDBNOTINCMMFILE = new File(AGILENT_FILE_PATH + fileName + ".agilent.cation.HMDBNOTINCMM.txt");
                if (HMDBNOTINCMMFILE.exists()) {
                    HMDBNOTINCMMFILE.delete();
                }

                LMINCMMFILE = new File(AGILENT_FILE_PATH + fileName + ".agilent.cation.LMINCMM.txt");
                if (LMINCMMFILE.exists()) {
                    LMINCMMFILE.delete();
                }

                KEGGINCMMFILE = new File(AGILENT_FILE_PATH + fileName + ".agilent.cation.KEGGINCMM.txt");
                if (KEGGINCMMFILE.exists()) {
                    KEGGINCMMFILE.delete();
                }

                HMDBINCMMFILE = new File(AGILENT_FILE_PATH + fileName + ".agilent.cation.HMDBINCMM.txt");
                if (HMDBINCMMFILE.exists()) {
                    HMDBINCMMFILE.delete();
                }

                ONLYINMETLINFILE = new File(AGILENT_FILE_PATH + fileName + ".agilent.cation.ONLYMETLIN.txt");
                if (ONLYINMETLINFILE.exists()) {
                    ONLYINMETLINFILE.delete();
                }

                updateCASFILE = new File(AGILENT_FILE_PATH + fileName + ".agilent.cation.UPDATECASFROMAGILENT.txt");
                if (updateCASFILE.exists()) {
                    updateCASFILE.delete();
                }
                updateInformationFromAgilentFILE = new File(AGILENT_FILE_PATH + fileName + ".agilent.cation.UPDATEINFORMATIONFROMAGILENT.txt");
                if (updateInformationFromAgilentFILE.exists()) {
                    updateInformationFromAgilentFILE.delete();
                }
                updatedCompoundsFile = new File(AGILENT_FILE_PATH + fileName + ".agilent.UPDATE_COMPOUNDS.txt");
                if (updatedCompoundsFile.exists()) {
                    updatedCompoundsFile.delete();
                }

                casIDs = new HashSet();
                hmdbIDs = new HashSet();
                lmIDs = new HashSet();
                keggIDs = new HashSet();

                duplicatedCasIDs = new HashSet();
                duplicatedHmdbIDs = new HashSet();
                duplicatedLmIDs = new HashSet();
                duplicatedKeggIDs = new HashSet();

                // Generates CSV.
                int countLine = 0;
                String line;
                StringBuilder outputLine;
                Object[] values;
                LineIterator it = FileUtils.lineIterator(inputFile, "ISO-8859-1");
                // Only look for cations if is there any of them
                if (cationStart > 0) {
                    while (it.hasNext()) {
                        line = it.nextLine();
                        if (isCompoundLine(line)) {

                            // Values from Agilent correspond to neutral compound
                            if (countLine < cationStart) {
                                values = getValues(line);
                            } // Values from Agilent correspond to anion or cation
                            // It is necessary to access other source for obtain 
                            // neutral information of the compound
                            else {
                                // Access to the real values accesing to different sources
                                // Order of the sources: 
                                // 1 -> LipidMaps, 2 -> HMDB, 3 -> KEGG, 4 -> Metlin
                                values = getValuesFromCation(line, db);
                                // If the compound does not exist
                                if (!alreadyExistsInBBDD(db, values)) {
                                    // If information was not obtained from other source

                                    // Creates String for trace
                                    String lineForTrace = "## CATION  ";
                                    lineForTrace = lineForTrace + ("METLIN: " + (String) values[6] + " -> [CAS: ");
                                    if (values[4] == null) {
                                        lineForTrace = lineForTrace + "------ ][HMDB: ";
                                    } else {
                                        lineForTrace = lineForTrace + (String) values[4] + " ][HMDB: ";
                                    }
                                    if (values[7] == null) {
                                        lineForTrace = lineForTrace + "------ ][LM: ";
                                    } else {
                                        lineForTrace = lineForTrace + (String) values[7] + " ][LM: ";
                                    }
                                    if (values[9] == null) {
                                        lineForTrace = lineForTrace + "------ ][KEGG: ";
                                    } else {
                                        lineForTrace = lineForTrace + (String) values[9] + " ][KEGG: ";
                                    }
                                    if (values[8] == null) {
                                        lineForTrace = lineForTrace + "------ ] -----> ";
                                    } else {
                                        lineForTrace = lineForTrace + (String) values[8] + " ] MASS: ";
                                    }
                                    lineForTrace = lineForTrace + values[2].toString();

                                    int CMMID = (int) values[11];
                                    if (CMMID > 0) {
                                        createAgilentLink(db, values, CMMID, lineForTrace);
                                    } else if (CMMID == 0) {
                                        insertWithAgilentLink(db, values, lineForTrace, compoundType);
                                    } else {
                                        System.out.println("ERROR: CMMID: " + CMMID + " STRANGE");
                                    }
                                }
                            }

                            // Writes in CVS file.
                            outputLine = new StringBuilder();
                            for (Object value : values) {
                                outputLine.append(value != null ? value : "").append(";");
                            }
                            FileUtils.writeStringToFile(outputFile, outputLine.toString() + "\n", "ISO-8859-1", true);

                            // Se almacenan los ids para encontrar repetidos en el siguiente paso.
                            checkIds(values[4], values[7], values[9], values[8]);
                        }
                        countLine++;
                    }
                } else {
                    while (it.hasNext()) {
                        line = it.nextLine();
                        if (isCompoundLine(line)) {
                            values = getValues(line);

                            // Writes in CSV file.
                            outputLine = new StringBuilder();
                            for (Object value : values) {
                                outputLine.append(value != null ? value : "").append(";");
                            }
                            FileUtils.writeStringToFile(outputFile, outputLine.toString() + "\n", "ISO-8859-1", true);

                            // Se almacenan los ids para encontrar repetidos en el siguiente paso.
                            checkIds(values[4], values[7], values[9], values[8]);
                        }

                    }
                }
            }
        }
    }

    /**
     * Keeps track of duplicated IDs in CAS, HMDB, LM and KEGG.
     *
     * @param casID
     * @param hmdbID
     * @param lmID
     * @param keggID
     */
    private void checkIds(Object casID, Object hmdbID, Object lmID, Object keggID) {

        if (casID != null) {
            if (casIDs.contains(casID)) {
                duplicatedCasIDs.add(casID);
            } else {
                casIDs.add(casID);
            }
        }

        if (hmdbID != null) {
            if (hmdbIDs.contains(hmdbID)) {
                duplicatedHmdbIDs.add(hmdbID);
            } else {
                hmdbIDs.add(hmdbID);
            }
        }

        if (lmID != null) {
            if (lmIDs.contains(lmID)) {
                duplicatedLmIDs.add(lmID);
            } else {
                lmIDs.add(lmID);
            }
        }

        if (keggID != null) {
            if (keggIDs.contains(keggID)) {
                duplicatedKeggIDs.add(keggID);
            } else {
                keggIDs.add(keggID);
            }
        }
    }

    /**
     *
     * @param fileName
     * @param db database to check compound and insert if necessary
     * @param cationStart integer to indicates where the cations start
     * @param compoundsType type of the compounds in the file
     * @throws IOException
     * @throws ParseException
     */
    private void parseFileAndCheckToBBDD(String fileName, CMMDatabase db, int cationStart, int compoundsType)
            throws IOException, ParseException, Exception {

        File inputFile = new File(AGILENT_FILE_PATH + fileName);
        if (!inputFile.exists()) {
            System.out.println("##                   --> ERROR: Intput file not exists. PLEASE CHECK IT. " + inputFile.getAbsolutePath());

        } else {

            duplicatedFile = new File(AGILENT_FILE_PATH + fileName + ".agilent.DUPLICATED.txt");
            if (duplicatedFile.exists()) {
                duplicatedFile.delete();
            }

            linkUpdateFile = new File(AGILENT_FILE_PATH + fileName + ".agilent.LINK_UPDATE.txt");
            if (linkUpdateFile.exists()) {
                linkUpdateFile.delete();
            }

            int index = 0;
            String line;
            Object[] values;
            LineIterator it = FileUtils.lineIterator(inputFile, "ISO-8859-1");

            // Counter to know where the cations start
            int countLine = 0;
            if (cationStart > 0) {
                while (countLine < cationStart && it.hasNext()) {
                    line = it.nextLine();

                    if (isCompoundLine(line)) {

                        if (index++ % 500 == 0) {
                            System.out.println(" --> " + (index - 1));
                        }
                        values = getValues(line);
                        if (!alreadyExistsInBBDD(db, values)) {
                            checkCompoundFromBBDD(db, values, compoundsType);
                        } else if (compoundsType != 0) {
                            int compoundID = getCompoundIdFromBBDD(db, values);
                            if (compoundID != 0) {
                                updateCompoundType(db, compoundID, compoundsType);
                            }
                        }
                    }

                    //if (index++==100) break;
                    countLine++;
                }
            } else {
                while (it.hasNext()) {
                    
                    line = it.nextLine();

                    if (isCompoundLine(line)) {

                        if (index++ % 500 == 0) {
                            System.out.println(" --> " + (index - 1));
                        }
                        values = getValues(line);
                        if (!alreadyExistsInBBDD(db, values)) {
                            checkCompoundFromBBDD(db, values, compoundsType);
                        } else if (compoundsType != 0) {
                            int compoundID = getCompoundIdFromBBDD(db, values);
                            if (compoundID != 0) {
                                updateCompoundType(db, compoundID, compoundsType);
                            }
                        }

                    }

                    //if (index++==100) break;
                    countLine++;
                }
                 
            }
        }
    }

    /**
     *
     * @param fileName
     * @param db database to check compound and insert if necessary
     * @throws IOException
     * @throws ParseException
     */
    private void updatePeptidesBBDD(String fileName, CMMDatabase db)
            throws IOException, ParseException, Exception {

        File inputFile = new File(AGILENT_FILE_PATH + fileName);
        if (!inputFile.exists()) {
            System.out.println("##                   --> ERROR: Intput file not exists. PLEASE CHECK IT. " + inputFile.getAbsolutePath());

        } else {

            updatedCompoundsFile = new File(AGILENT_FILE_PATH + fileName + ".agilent.UPDATE_COMPOUNDS.txt");
            if (updatedCompoundsFile.exists()) {
                updatedCompoundsFile.delete();
            }

            int index = 0;
            String line;
            Object[] values;
            LineIterator it = FileUtils.lineIterator(inputFile, "ISO-8859-1");

            while (it.hasNext()) {
                line = it.nextLine();

                if (isCompoundLine(line)) {

                    if (index++ % 500 == 0) {
                        System.out.println(" --> " + (index - 1));
                    }
                    values = getValues(line);
                    int compoundID = getCompoundIdFromBBDD(db, values);
                    if (compoundID != 0) {
                        updateCompoundType(db, compoundID, 2);
                    }

                }

            }
        }
    }

    /**
     * 0. Compound Name -- OBL 1. Formula -- OBL 2. Mass -- OBL 3. Delta Mass --
     * OBL 4. CAS 5. ChemSpider 6. METLIN - AGILENT -- OBL 7. HMP 8. KEGG 9. LMP
     * -- OBL 10. Num Spectra - OBL If isCation, values retrieved from the file
     * does not correspond to neutral mass, so we have to access diferent
     * sources for obtaining data
     *
     * @param line
     * @param isCation
     * @return
     */
    private Object[] getValues(String line) {
        String[] words = line.split(" ");
        String word;
        Object[] values = new Object[11];
        boolean informationObtained = false;
        for (int i = words.length - 1; i >= 0; i--) {
            word = words[i];

            // 10. Num Spectra - OBL
            if (values[10] == null) {
                values[10] = word;

                // 6. METLIN -- OBL
            } else if (values[6] == null) {

                // 9. LMP -- OBL                 
                if (isLM(word)) {
                    values[9] = word;

                    // 8. KEGG 
                } else if (isKEGG(word)) {
                    values[8] = word;

                    // 7. HMDB
                } else if (isHMP(word)) {
                    values[7] = word.substring(0, 4) + "00" + word.substring(4);

                    // 6. METLIN                             
                } else {
                    values[6] = word;
                }

                // 3. Delta Mass -- OBL    
            } else if (values[3] == null) {

                // 5. ChemSpider
                if (values[5] == null) {
                    try {
                        values[5] = Integer.parseInt(word);
                    } catch (NumberFormatException e) {
                        if (values[4] == null && isCAS(word)) {
                            values[4] = word;
                            // 3. Delta Mass -- OBL
                        } else {
                            values[3] = Double.parseDouble(word.replace(",", ""));
                        }
                    }

                    // 4. CAS                            
                } else if (values[4] == null && isCAS(word)) {
                    values[4] = word;

                    // 3. Delta Mass -- OBL
                } else {
                    values[3] = Double.parseDouble(word.replace(",", ""));
                }

                // 2. Mass -- OBL                        
            } else if (values[2] == null) {
                values[2] = word;

                // 1. Formula -- OBL     
            } else if (values[1] == null) {
                if (Utilities.isDouble(word)) {
// There is a RT and the arguments are one column moved to the left since Delta Mass.
                    values[3] = values[2];
                    values[2] = word;
                } else {
                    if (!PatternFinder.checkFormulaElements(word)) {
                        //System.out.println("BAD FORMULA IN METLIN: " + values[6] + " FORMULA: " + word);
                    }
                    values[1] = word;
                }
                // 0. Compound Name -- OBL
            } else if (values[0] == null) {
                values[0] = word.replace(";", "");

            } else {
                values[0] = word.replace(";", "") + " " + values[0];
            }
        }
        return values;
    }

    /**
     * 0. Compound Name -- OBL 1. Formula -- OBL 2. Mass -- OBL 3. Delta Mass --
     * OBL 4. CAS 5. ChemSpider 6. METLIN - AGILENT -- OBL 7. HMP 8. KEGG 9. LMP
     * -- OBL 10. Num Spectra - OBL If isCation, values retrieved from the file
     * does not correspond to neutral mass, so we have to access diferent
     * sources for obtaining data It also insert the compound in the database
     *
     * @param line
     * @param isCation
     * @return
     */
    private Object[] getValuesFromCation(String line, CMMDatabase db) throws IOException {
        String[] words = line.split(" ");
        String word;
        // values [12] is a integer for the CMMID. If there is no CMMID, then is a 0
        Object[] values = new Object[13];
        Object[] auxiliarForMetlin;
        MetlinDatabasePopulator metlinp = new MetlinDatabasePopulator();
        int CMMid = 0;
        String cas = "";
        String name = "";
        String formula = "";
        double massDouble = 0.0d;
        String keggID = "";
        String HMDBID = "";
        int metlinID = 0;
        boolean isInformationObtained = false;
        boolean isMassNull = false;
        boolean isFormulaNull = false;
        for (int i = words.length - 1; i >= 0; i--) {
            word = words[i];

            // 10. Num Spectra - OBL
            if (values[10] == null) {
                values[10] = word;

                // 6. METLIN -- OBL
            } else if (values[6] == null) {

                // 9. LMP -- OBL                 
                if (isLM(word)) {
                    String lmId = word;
                    String lmpSELECT = "select c.compound_id, c.cas_id, c.compound_name, c.formula, c.mass, l.lm_id "
                            + " from compounds as c inner join compounds_lm l on c.compound_id=l.compound_id "
                            + "where l.lm_id=\"" + lmId + "\"";
                    db.executeQuery(lmpSELECT);
                    ArrayList<Object[]> lmpResult = db.queryResultInGrid(new int[]{1, 2, 3, 4, 5, 6});

                    if (lmpResult != null && lmpResult.size() == 1) {
                        CMMid = (Integer) lmpResult.get(0)[0];
                        cas = (String) lmpResult.get(0)[1];
                        name = (String) lmpResult.get(0)[2];
                        if (lmpResult.get(0)[3] == null) {
                            // If formula in CMM is null, update formula
                            isFormulaNull = true;
                        } else {
                            formula = (String) lmpResult.get(0)[3];
                            values[1] = formula;
                        }
                        if (lmpResult.get(0)[4] == null) {
                            // If mass in CMM is null, update mass
                            isMassNull = true;
                        } else {
                            massDouble = (double) lmpResult.get(0)[4];
                            values[2] = massDouble;
                        }
                        isInformationObtained = true;
                        FileUtils.writeStringToFile(LMINCMMFILE, lmId + "\n", "ISO-8859-1", true);

                        values[4] = cas;
                        values[0] = name;
                    } else {
                        FileUtils.writeStringToFile(LMNOTINCMMFILE, lmId + "\n", "ISO-8859-1", true);
                    }
                    values[9] = lmId;

                    // 8. KEGG 
                } else if (isKEGG(word)) {
                    if (isInformationObtained) {
                        String keggIdSelect = "select ck.kegg_id"
                                + " from compounds_kegg ck "
                                + "where ck.compound_id=" + CMMid;
                        keggID = db.getString(keggIdSelect);
                        if (keggID.equals("")) {
                            values[8] = word;
                        } else if (!word.equals(keggID)) {
                            System.out.println("INCONSISTENCY FROM PCDL KEGGID:  " + keggID + "  " + word + " CMMID " + CMMid);
                        }
                    } else // First, check into HMDB. Information from HMDB is more reliable than KEGG
                     if (isHMP(words[i - 1])) {
                            HMDBID = words[i - 1].substring(0, 4) + "00" + words[i - 1].substring(4);
                            String HMDBSelect = "select c.compound_id, c.cas_id, c.compound_name, c.formula, c.mass, ch.hmdb_id "
                                    + " from compounds as c inner join compounds_hmdb ch on c.compound_id=ch.compound_id "
                                    + "where ch.hmdb_id=\"" + HMDBID + "\"";
                            db.executeQuery(HMDBSelect);
                            ArrayList<Object[]> HMDBResult = db.queryResultInGrid(new int[]{1, 2, 3, 4, 5, 6});

                            if (HMDBResult != null && HMDBResult.size() == 1) {
                                CMMid = (Integer) HMDBResult.get(0)[0];
                                cas = (String) HMDBResult.get(0)[1];
                                name = (String) HMDBResult.get(0)[2];
                                if (HMDBResult.get(0)[3] == null) {
                                    // If formula in CMM is null, update formula
                                    isFormulaNull = true;
                                } else {
                                    formula = (String) HMDBResult.get(0)[3];
                                    values[1] = formula;
                                }
                                if (HMDBResult.get(0)[4] == null) {
                                    // If mass in CMM is null, update mass
                                    isMassNull = true;
                                } else {
                                    massDouble = (double) HMDBResult.get(0)[4];
                                    values[2] = massDouble;
                                }
                                isInformationObtained = true;

                                values[4] = cas;
                                values[0] = name;
                                FileUtils.writeStringToFile(HMDBINCMMFILE, HMDBID + "\n", "ISO-8859-1", true);
                            } else {
                                FileUtils.writeStringToFile(HMDBNOTINCMMFILE, HMDBID + "\n", "ISO-8859-1", true);
                            }
                            values[7] = HMDBID;
                        } else {
                            keggID = word;
                            String keggSELECT = "select c.compound_id, c.cas_id, c.compound_name, c.formula, c.mass, ck.kegg_id "
                                    + " from compounds as c inner join compounds_kegg ck on c.compound_id=ck.compound_id "
                                    + "where ck.kegg_id=\"" + keggID + "\"";
                            db.executeQuery(keggSELECT);
                            ArrayList<Object[]> keggResult = db.queryResultInGrid(new int[]{1, 2, 3, 4, 5, 6});

                            if (keggResult != null && keggResult.size() == 1) {
                                CMMid = (Integer) keggResult.get(0)[0];
                                cas = (String) keggResult.get(0)[1];
                                name = (String) keggResult.get(0)[2];
                                if (keggResult.get(0)[3] == null) {
                                    // If mass in CMM is null, update mass
                                    isFormulaNull = true;
                                } else {
                                    formula = (String) keggResult.get(0)[3];
                                    values[1] = formula;
                                }
                                if (keggResult.get(0)[4] == null) {
                                    // If mass in CMM is null, update mass
                                    isMassNull = true;
                                } else {
                                    massDouble = (double) keggResult.get(0)[4];
                                    values[2] = massDouble;
                                }
                                isInformationObtained = true;

                                values[4] = cas;
                                values[0] = name;
                                FileUtils.writeStringToFile(KEGGINCMMFILE, keggID + "\n", "ISO-8859-1", true);
                            } else {
                                FileUtils.writeStringToFile(KEGGNOTINCMMFILE, keggID + "\n", "ISO-8859-1", true);
                            }
                            values[8] = keggID;
                        }

                    // 7. HMDB
                } else if (isHMP(word)) {
                    word = word.substring(0, 4) + "00" + word.substring(4);
                    if (isInformationObtained) {
                        String HMDBSelect = "select ch.hmdb_id"
                                + " from compounds_hmdb ch "
                                + "where ch.compound_id=" + CMMid;
                        HMDBID = db.getString(HMDBSelect);
                        if (HMDBID.equals("")) {
                            values[7] = word;
                        } else if (!word.equals(HMDBID)) {
                            System.out.println("INCONSISTENCY HMDBID: " + HMDBID + "  " + word + " CMMID " + CMMid);
                        }
                    } else {
                        HMDBID = word;
                        String HMDBSelect = "select c.compound_id, c.cas_id, c.compound_name, c.formula, c.mass, ch.hmdb_id "
                                + " from compounds as c inner join compounds_hmdb ch on c.compound_id=ch.compound_id "
                                + "where ch.hmdb_id=\"" + HMDBID + "\"";
                        db.executeQuery(HMDBSelect);
                        ArrayList<Object[]> HMDBResult = db.queryResultInGrid(new int[]{1, 2, 3, 4, 5, 6});

                        if (HMDBResult != null && HMDBResult.size() == 1) {
                            CMMid = (Integer) HMDBResult.get(0)[0];
                            cas = (String) HMDBResult.get(0)[1];
                            name = (String) HMDBResult.get(0)[2];
                            if (HMDBResult.get(0)[3] == null) {
                                // If mass in CMM is null, update mass
                                isFormulaNull = true;
                            } else {
                                formula = (String) HMDBResult.get(0)[3];
                                values[1] = formula;
                            }
                            if (HMDBResult.get(0)[4] == null) {
                                // If mass in CMM is null, update mass
                                isMassNull = true;
                            } else {
                                massDouble = (double) HMDBResult.get(0)[4];
                                values[2] = massDouble;
                            }
                            isInformationObtained = true;

                            values[4] = cas;
                            values[0] = name;
                            FileUtils.writeStringToFile(HMDBINCMMFILE, HMDBID + "\n", "ISO-8859-1", true);
                        } else {
                            FileUtils.writeStringToFile(HMDBNOTINCMMFILE, HMDBID + "\n", "ISO-8859-1", true);
                        }
                        values[7] = HMDBID;
                    }

                    // 6. METLIN                             
                } else {
                    metlinID = Integer.parseInt(word);
                    if (isInformationObtained) {
                        if (isMassNull) {
                            // 0: mass, 1:formula, 2:name, 3:cas
                            auxiliarForMetlin = metlinp.getValuesFromMetlin(metlinID);
                            String massFromMetlin = (String) auxiliarForMetlin[0];
                            if (massFromMetlin.equals("")) {
                                // There is no mass either in CMM or Metlin
                            } else {
                                String updateMass = "UPDATE compounds SET mass = " + massFromMetlin
                                        + " WHERE compound_id = " + CMMid;
                                try {
                                    db.executeNewIDUWithEx(updateMass);
                                    FileUtils.writeStringToFile(updateInformationFromAgilentFILE, "UPDATING MASS of "
                                            + CMMid + " METLIN ID" + metlinID + " " + "\n", "ISO-8859-1", true);
                                } catch (SQLException ex) {
                                    FileUtils.writeStringToFile(updateInformationFromAgilentFILE, "ERROR UPDATING MASS of "
                                            + CMMid + " METLIN ID" + metlinID + " " + updateMass + "\n", "ISO-8859-1", true);
                                }
                            }
                        }

                        if (isFormulaNull) {
                            auxiliarForMetlin = metlinp.getValuesFromMetlin(metlinID);
                            String formulaFromMetlin = (String) auxiliarForMetlin[1];
                            if (formulaFromMetlin.equals("")) {
                                // There is no mass either in CMM or Metlin
                            } else {
                                String formulaType = PatternFinder.getTypeFromFormula(formula);
                                if (formulaType.equalsIgnoreCase("null") || formulaType.equals("")) {
                                    formulaType = "ALL";
                                }
                                int formulaTypeInt = Utilities.getIntChemAlphabet(formulaType);
                                formulaType = "'" + formulaType + "'";
                                if (formulaType.equals("")) {
                                    formulaType = "ALL";
                                } else {
                                    formulaType = "'" + formulaType + "'";
                                }
                                String updateFormula = "UPDATE compounds SET formula = '" + formulaFromMetlin
                                        + "', formula_type = " + formulaType
                                        + "', formula_type_int = " + formulaTypeInt + " WHERE compound_id = " + CMMid;
                                try {
                                    db.executeNewIDUWithEx(updateFormula);
                                    FileUtils.writeStringToFile(updateInformationFromAgilentFILE, "UPDATING FORMULA of "
                                            + CMMid + " METLIN ID" + metlinID + " " + "\n", "ISO-8859-1", true);
                                } catch (SQLException ex) {
                                    FileUtils.writeStringToFile(updateInformationFromAgilentFILE, "ERROR UPDATING FORMULA of "
                                            + CMMid + " METLIN ID" + metlinID + " " + updateFormula + "\n", "ISO-8859-1", true);
                                }
                            }
                        }
                    } else {
                        auxiliarForMetlin = metlinp.getValuesFromMetlin(metlinID);

                        FileUtils.writeStringToFile(ONLYINMETLINFILE, word + " " + auxiliarForMetlin[0]
                                + " " + auxiliarForMetlin[1] + " " + auxiliarForMetlin[2]
                                + " " + auxiliarForMetlin[3] + "\n", "ISO-8859-1", true);
                        cas = (String) auxiliarForMetlin[3];
                        String massFromMetlin = (String) auxiliarForMetlin[0];
                        formula = (String) auxiliarForMetlin[1];
                        name = (String) auxiliarForMetlin[2];
                        values[4] = cas;
                        values[2] = massFromMetlin;
                        values[1] = formula;
                        values[0] = name;
                    }
                    values[6] = word;

                }

                // 3. Delta Mass -- OBL    
            } else if (values[3] == null) {

                // 5. ChemSpider
                if (values[5] == null) {
                    try {
                        values[5] = Integer.parseInt(word);
                    } catch (NumberFormatException e) {
                        if (isCAS(word)) {
                            if (cas == null || cas.equals("")) {
                                // CAS WHICH APPEARS IN AGILENT AND NOT IN CMM
                                //System.out.println("CAS ONLY IN AGILENT BUT NOT IN OTHER SOURCES: " + word);
                                // UPDATE CAS FROM AGILENT FILE
                                try {
                                    // TODO Update CAS always from METLIN?
                                    String updateCas = "UPDATE compounds SET cas_id = '" + word
                                            + "' WHERE compound_id = " + CMMid;
                                    db.executeNewIDUWithEx(updateCas);

                                    FileUtils.writeStringToFile(updateCASFILE, updateCas + "\n", "ISO-8859-1", true);

                                } catch (SQLException ex) {
                                    String errorUpdatingCAS = "ERROR UPDATING CAS IN CMMID: " + CMMid + " CAS FROM AGILENT: " + word;
                                    FileUtils.writeStringToFile(errorFile, errorUpdatingCAS + " --> " + ex.toString() + "\n", "ISO-8859-1", true);
                                }
                            } else if (!cas.equals(word)) {
                                System.out.println("INCONSISTENT CAS: " + cas + "  PCDL: " + word + " in CMM: " + CMMid);
                            }
                            // 3. Delta Mass -- OBL
                        } else {
                            values[3] = Double.parseDouble(word.replace(",", ""));
                        }
                    }

                    // 4. CAS                            
                } else if (isCAS(word)) {
                    if (cas == null || cas.equals("")) {
                        // CAS WHICH APPEARS IN AGILENT AND NOT IN CMM
                        //System.out.println("CAS ONLY IN AGILENT BUT NOT IN OTHER SOURCES: " + word);
                        // UPDATE CAS FROM AGILENT FILE
                        try {
                            // TODO Update CAS always from METLIN?
                            String updateCas = "UPDATE compounds SET cas_id = '" + word
                                    + "' WHERE compound_id = " + CMMid;
                            db.executeNewIDUWithEx(updateCas);

                            FileUtils.writeStringToFile(updateCASFILE, updateCas + "\n", "ISO-8859-1", true);

                        } catch (SQLException ex) {
                            String errorUpdatingCAS = "ERROR UPDATING CAS IN CMMID: " + CMMid + " CAS FROM AGILENT: " + word;
                            FileUtils.writeStringToFile(errorFile, errorUpdatingCAS + " --> " + ex.toString() + "\n", "ISO-8859-1", true);
                        }

                    } else if (!cas.equals(word)) {
                        System.out.println("INCONSISTENT CAS: " + cas + "  PCDL: " + word + " in CMM: " + CMMid);
                    }
                } else {
                    values[3] = Double.parseDouble(word.replace(",", ""));
                }
            }
        }
        values[11] = CMMid;
        return values;
    }

    /**
     * 0. Compound Name -- OBL 1. Formula -- OBL 2. Mass -- OBL 3. Delta Mass --
     * OBL 4. CAS 5. ChemSpider 6. METLIN - AGILENT -- OBL 7. HMP 8. KEGG 9. LMP
     * -- OBL 10. Num Spectra - OBL If isCation, values retrieved from the file
     * does not correspond to neutral mass, so we have to access diferent
     * sources for obtaining data It also insert the compound in the database
     *
     * @param line
     * @param isCation
     * @return
     */
    private Object[] getValuesFromCationToCSV(String line, CMMDatabase db) throws IOException {
        String[] words = line.split(" ");
        String word;
        // values [12] is a integer for the CMMID. If there is no CMMID, then is a 0
        Object[] values = new Object[14];
        Object[] auxiliarForMetlin;
        MetlinDatabasePopulator metlinp = new MetlinDatabasePopulator();
        int CMMid = 0;
        String cas = "";
        String name = "";
        String formula = "";
        double massDouble = 0.0d;
        String keggID = "";
        String HMDBID = "";
        int metlinID = 0;
        boolean isInformationObtained = false;
        boolean isMassNull = false;
        boolean isFormulaNull = false;
        for (int i = words.length - 1; i >= 0; i--) {
            word = words[i];

            // 10. Num Spectra - OBL
            if (values[10] == null) {
                values[10] = word;

                // 6. METLIN -- OBL
            } else if (values[6] == null) {

                // 9. LMP -- OBL                 
                if (isLM(word)) {
                    String lmId = word;
                    String lmpSELECT = "select c.compound_id, c.cas_id, c.compound_name, c.formula, c.mass, l.lm_id "
                            + " from compounds as c inner join compounds_lm l on c.compound_id=l.compound_id "
                            + "where l.lm_id=\"" + lmId + "\"";
                    db.executeQuery(lmpSELECT);
                    ArrayList<Object[]> lmpResult = db.queryResultInGrid(new int[]{1, 2, 3, 4, 5, 6});

                    if (lmpResult != null && lmpResult.size() == 1) {
                        CMMid = (Integer) lmpResult.get(0)[0];
                        cas = (String) lmpResult.get(0)[1];
                        name = (String) lmpResult.get(0)[2];
                        if (lmpResult.get(0)[3] == null) {
                            // If formula in CMM is null, update formula
                            isFormulaNull = true;
                        } else {
                            formula = (String) lmpResult.get(0)[3];
                            values[13] = formula;
                        }
                        if (lmpResult.get(0)[4] == null) {
                            // If mass in CMM is null, update mass
                            isMassNull = true;
                        } else {
                            massDouble = (double) lmpResult.get(0)[4];
                            values[12] = massDouble;
                        }
                        isInformationObtained = true;

                        //values[4] = cas;
                        //values[0] = name;
                    }
                    values[9] = lmId;

                    // 8. KEGG 
                } else if (isKEGG(word)) {
                    if (isInformationObtained) {
                        String keggIdSelect = "select ck.kegg_id"
                                + " from compounds_kegg ck "
                                + "where ck.compound_id=" + CMMid;
                        keggID = db.getString(keggIdSelect);
                        if (keggID.equals("")) {
                            values[8] = word;
                        } else if (!word.equals(keggID)) {
                            System.out.println("INCONSISTENCY FROM PCDL KEGGID:  " + keggID + "  " + word + " CMMID " + CMMid);
                        }
                    } else // First, check into HMDB. Information from HMDB is more reliable than KEGG
                     if (isHMP(words[i - 1])) {
                            HMDBID = words[i - 1].substring(0, 4) + "00" + words[i - 1].substring(4);
                            String HMDBSelect = "select c.compound_id, c.cas_id, c.compound_name, c.formula, c.mass, ch.hmdb_id "
                                    + " from compounds as c inner join compounds_hmdb ch on c.compound_id=ch.compound_id "
                                    + "where ch.hmdb_id=\"" + HMDBID + "\"";
                            db.executeQuery(HMDBSelect);
                            ArrayList<Object[]> HMDBResult = db.queryResultInGrid(new int[]{1, 2, 3, 4, 5, 6});

                            if (HMDBResult != null && HMDBResult.size() == 1) {
                                CMMid = (Integer) HMDBResult.get(0)[0];
                                cas = (String) HMDBResult.get(0)[1];
                                name = (String) HMDBResult.get(0)[2];
                                if (HMDBResult.get(0)[3] == null) {
                                    // If formula in CMM is null, update formula
                                    isFormulaNull = true;
                                } else {
                                    formula = (String) HMDBResult.get(0)[3];
                                    values[13] = formula;
                                }
                                if (HMDBResult.get(0)[4] == null) {
                                    // If mass in CMM is null, update mass
                                    isMassNull = true;
                                } else {
                                    massDouble = (double) HMDBResult.get(0)[4];
                                    values[12] = massDouble;
                                }
                                isInformationObtained = true;

                                //values[4] = cas;
                                //values[0] = name;
                            }
                            values[7] = HMDBID;
                        } else {
                            keggID = word;
                            String keggSELECT = "select c.compound_id, c.cas_id, c.compound_name, c.formula, c.mass, ck.kegg_id "
                                    + " from compounds as c inner join compounds_kegg ck on c.compound_id=ck.compound_id "
                                    + "where ck.kegg_id=\"" + keggID + "\"";
                            db.executeQuery(keggSELECT);
                            ArrayList<Object[]> keggResult = db.queryResultInGrid(new int[]{1, 2, 3, 4, 5, 6});

                            if (keggResult != null && keggResult.size() == 1) {
                                CMMid = (Integer) keggResult.get(0)[0];
                                cas = (String) keggResult.get(0)[1];
                                name = (String) keggResult.get(0)[2];
                                if (keggResult.get(0)[3] == null) {
                                    // If mass in CMM is null, update mass
                                    isFormulaNull = true;
                                } else {
                                    formula = (String) keggResult.get(0)[3];
                                    values[13] = formula;
                                }
                                if (keggResult.get(0)[4] == null) {
                                    // If mass in CMM is null, update mass
                                    isMassNull = true;
                                } else {
                                    massDouble = (double) keggResult.get(0)[4];
                                    values[12] = massDouble;
                                }
                                isInformationObtained = true;

                                //values[4] = cas;
                                //values[0] = name;
                            }
                            values[8] = keggID;
                        }

                    // 7. HMDB
                } else if (isHMP(word)) {
                    word = word.substring(0, 4) + "00" + word.substring(4);
                    if (isInformationObtained) {
                        String HMDBSelect = "select ch.hmdb_id"
                                + " from compounds_hmdb ch "
                                + "where ch.compound_id=" + CMMid;
                        HMDBID = db.getString(HMDBSelect);
                        if (HMDBID.equals("")) {
                            values[7] = word;
                        } else if (!word.equals(HMDBID)) {
                            System.out.println("INCONSISTENCY HMDBID: " + HMDBID + "  " + word + " CMMID " + CMMid);
                        }
                    } else {
                        HMDBID = word;
                        String HMDBSelect = "select c.compound_id, c.cas_id, c.compound_name, c.formula, c.mass, ch.hmdb_id "
                                + " from compounds as c inner join compounds_hmdb ch on c.compound_id=ch.compound_id "
                                + "where ch.hmdb_id=\"" + HMDBID + "\"";
                        db.executeQuery(HMDBSelect);
                        ArrayList<Object[]> HMDBResult = db.queryResultInGrid(new int[]{1, 2, 3, 4, 5, 6});

                        if (HMDBResult != null && HMDBResult.size() == 1) {
                            CMMid = (Integer) HMDBResult.get(0)[0];
                            cas = (String) HMDBResult.get(0)[1];
                            name = (String) HMDBResult.get(0)[2];
                            if (HMDBResult.get(0)[3] == null) {
                                // If mass in CMM is null, update mass
                                isFormulaNull = true;
                            } else {
                                formula = (String) HMDBResult.get(0)[3];
                                values[13] = formula;
                            }
                            if (HMDBResult.get(0)[4] == null) {
                                // If mass in CMM is null, update mass
                                isMassNull = true;
                            } else {
                                massDouble = (double) HMDBResult.get(0)[4];
                                values[12] = massDouble;
                            }
                            isInformationObtained = true;

                            //values[4] = cas;
                            //values[0] = name;
                        }
                        values[7] = HMDBID;
                    }

                    // 6. METLIN                             
                } else {
                    metlinID = Integer.parseInt(word);
                    if (isInformationObtained) {
                        if (isMassNull) {
                            // 0: mass, 1:formula, 2:name, 3:cas
                            auxiliarForMetlin = metlinp.getValuesFromMetlin(metlinID);
                            String massFromMetlin = (String) auxiliarForMetlin[0];
                            if (massFromMetlin.equals("")) {
                                // There is no mass either in CMM or Metlin
                            } else {
                                values[12] = Double.parseDouble(massFromMetlin);
                            }
                        }

                        if (isFormulaNull) {
                            auxiliarForMetlin = metlinp.getValuesFromMetlin(metlinID);
                            String formulaFromMetlin = (String) auxiliarForMetlin[1];
                            if (formulaFromMetlin.equals("")) {
                                // There is no mass either in CMM or Metlin
                            } else {
                                values[13] = formulaFromMetlin + " FROM METLIN(UPDATE)";
                            }
                        }
                    } else {
                        auxiliarForMetlin = metlinp.getValuesFromMetlin(metlinID);
                        cas = (String) auxiliarForMetlin[3];
                        String massFromMetlin = (String) auxiliarForMetlin[0];
                        formula = (String) auxiliarForMetlin[1];
                        name = (String) auxiliarForMetlin[2];
                        //values[4] = cas;
                        values[12] = Double.parseDouble(massFromMetlin);
                        values[13] = formula + " FROM METLIN";
                        //values[0] = name;
                    }
                    values[6] = word;

                }

                // 3. Delta Mass -- OBL    
            } else if (values[3] == null) {

                // 5. ChemSpider
                if (values[5] == null) {
                    try {
                        values[5] = Integer.parseInt(word);
                    } catch (NumberFormatException e) {
                        if (isCAS(word)) {
                            //values[4] = word;
                            // 3. Delta Mass -- OBL
                        } else {
                            values[3] = Double.parseDouble(word.replace(",", ""));
                        }
                    }

                    // 4. CAS                            
                } else if (values[4] == null && isCAS(word)) {
                    values[4] = word;

                    // 3. Delta Mass -- OBL
                } else {
                    values[3] = Double.parseDouble(word.replace(",", ""));
                }

                // 2. Mass -- OBL                        
            } else if (values[2] == null) {
                values[2] = Double.parseDouble(word.replace(",", ""));
                // 1. Formula -- OBL     
            } else if (values[1] == null) {
                values[1] = word;

                // 0. Compound Name -- OBL
            } else if (values[0] == null) {
                values[0] = word.replace(";", "");

            } else {
                values[0] = word.replace(";", "") + " " + values[0];
            }
        }
        values[11] = CMMid;
        return values;
    }

    private boolean alreadyExistsInBBDD(CMMDatabase db, Object[] inputValues) {
        boolean result;

        String agilentID = (String) inputValues[6];
        String agilentSELECT = "select compound_id, agilent_id from compounds_agilent where agilent_id=\"" + agilentID + "\"";
        db.executeQuery(agilentSELECT);
        ArrayList<Object[]> casResult = db.queryResultInGrid(new int[]{1, 2});
        result = (casResult != null && casResult.size() > 0);

        return result;
    }

    private int getCompoundIdFromBBDD(CMMDatabase db, Object[] inputValues) {
        int result = 0;

        String agilentID = (String) inputValues[6];
        result = db.getCompoundIdFromAgilentId(agilentID);

        return result;
    }

    private void updateCompoundType(CMMDatabase db, int compoundID, int compoundType) throws IOException {
        try {
            String update = "UPDATE compounds SET compound_type = " + compoundType + " WHERE compound_id = " + compoundID;
            db.executeNewIDUWithEx(update);

            FileUtils.writeStringToFile(updatedCompoundsFile, update + "\n", "ISO-8859-1", true);

        } catch (SQLException ex) {
            FileUtils.writeStringToFile(errorFile, "error updating petide "
                    + " --> " + ex.toString() + "\n", "ISO-8859-1", true);
        }

    }

    /**
     *
     * @param db
     * @param values
     * @param compoundType
     */
    private void checkCompoundFromBBDD(CMMDatabase db, Object[] inputValues, int compoundType)
            throws IOException, Exception {

        String agilentId = (String) inputValues[6]; // METLIN
        String agilentFormula = (String) inputValues[1]; // METLIN
        Double agilentMass = Double.parseDouble(((String) inputValues[2]).replace(",", "")); // METLIN

        String casId = (String) inputValues[4];
        String hmdbId = (String) inputValues[7];
        String lmId = (String) inputValues[9];
        String keggId = (String) inputValues[8];

        Integer id1 = null;
        Integer id2 = null;
        Integer id3 = null;
        Integer id4 = null;

        String formula1 = null;
        String formula2 = null;
        String formula3 = null;
        String formula4 = null;

        Double mass1 = null;
        Double mass2 = null;
        Double mass3 = null;
        Double mass4 = null;

        // This Set is used for keeping track of the corresponding IDS found in DDBB with no duplicates.
        HashSet<Integer> compareIds = new HashSet();

        StringBuilder line = new StringBuilder();
        String duplicated = "";
        line.append("## METLIN: ").append(String.format("%1$6s", agilentId)).append("## -> ");

        // CAS
        if (casId == null) {
            line.append("[CAS: ----------- ]");

        } else {
            line.append("[CAS: ").append(String.format("%1$11s", casId)).append(" ]");

            if (duplicatedCasIDs.contains(casId)) {
                duplicated += "[CAS]";

            } else {
                // Query for getting id, formula and mass from DB.
                String casSELECT = "select c.compound_id, c.cas_id, c.compound_name, c.formula, c.mass "
                        + "from compounds as c where c.cas_id=\"" + casId + "\"";
                db.executeQuery(casSELECT);
                ArrayList<Object[]> casResult = db.queryResultInGrid(new int[]{1, 2, 3, 4, 5});
                if (casResult != null && casResult.size() == 1) {
                    id1 = (Integer) casResult.get(0)[0];
                    formula1 = (String) casResult.get(0)[3];
                    mass1 = (Double) casResult.get(0)[4];
                    compareIds.add(id1);
                }
            }
        }

        // HMDB
        if (hmdbId == null) {
            line.append("[HMDB: --------- ]");

        } else {
            line.append("[HMDB: ").append(hmdbId).append(" ]");

            if (duplicatedHmdbIDs.contains(hmdbId)) {
                duplicated += "[HMDB]";

            } else {
                // Query for getting id, formula and mass from DB.
                String hmdbSELECT = "select c.compound_id, c.cas_id, c.compound_name, c.formula, c.mass, h.hmdb_id"
                        + " from compounds as c inner join compounds_hmdb h on c.compound_id=h.compound_id "
                        + "where h.hmdb_id=\"" + hmdbId + "\"";
                db.executeQuery(hmdbSELECT);
                ArrayList<Object[]> hmdbResult = db.queryResultInGrid(new int[]{1, 2, 3, 4, 5, 6});
                if (hmdbResult != null && hmdbResult.size() == 1) {
                    id2 = (Integer) hmdbResult.get(0)[0];
                    formula2 = (String) hmdbResult.get(0)[3];
                    mass2 = (Double) hmdbResult.get(0)[4];
                    compareIds.add(id2);
                }
            }
        }

        // LM
        if (lmId == null) {
            line.append("[LM: -------------- ]");

        } else {
            line.append("[LM: ").append(String.format("%1$14s", lmId)).append(" ]");

            if (duplicatedLmIDs.contains(lmId)) {
                duplicated += "[LM]";

            } else {
                // Query for getting id, formula and mass from DB.
                String lmpSELECT = "select c.compound_id, c.cas_id, c.compound_name, c.formula, c.mass, l.lm_id "
                        + " from compounds as c inner join compounds_lm l on c.compound_id=l.compound_id "
                        + "where l.lm_id=\"" + lmId + "\"";
                db.executeQuery(lmpSELECT);
                ArrayList<Object[]> lmpResult = db.queryResultInGrid(new int[]{1, 2, 3, 4, 5, 6});
                if (lmpResult != null && lmpResult.size() == 1) {
                    id3 = (Integer) lmpResult.get(0)[0];
                    formula3 = (String) lmpResult.get(0)[3];
                    mass3 = (Double) lmpResult.get(0)[4];
                    compareIds.add(id3);
                }
            }
        }

        // KEGG
        if (keggId == null) {
            line.append("[KEGG: ------ ]");

        } else {
            line.append("[KEGG: ").append(keggId).append(" ]");

            if (duplicatedKeggIDs.contains(keggId)) {
                duplicated += "[KEGG]";

            } else {
                // Query for getting id, formula and mass from DB.
                String keggSELECT = "select c.compound_id, c.cas_id, c.compound_name, c.formula, c.mass, k.kegg_id "
                        + " from compounds as c inner join compounds_kegg as k on c.compound_id = k.compound_id "
                        + " where k.kegg_id=\"" + keggId + "\"";
                db.executeQuery(keggSELECT);
                ArrayList<Object[]> keggResult = db.queryResultInGrid(new int[]{1, 2, 3, 4, 5, 6});
                if (keggResult != null && keggResult.size() == 1) {
                    id4 = (Integer) keggResult.get(0)[0];
                    formula4 = (String) keggResult.get(0)[3];
                    mass4 = (Double) keggResult.get(0)[4];
                    compareIds.add(id4);
                }
            }
        }

        // -----------------------------------------
        // Writes information.
        // -----------------------------------------
        // Keeps a track of cases with duplicated IDs.
        if (!duplicated.equals("")) {
            FileUtils.writeStringToFile(duplicatedFile, line.toString() + " ---> Duplicated ID in  " + duplicated + "\n", "ISO-8859-1", true);
        }

// CASE 1. The compound is not in MASS MEDIATOR. ----> INSERT + LINK TO AGILENT.
        if (compareIds.isEmpty()) {

            if (duplicated.equals("")) {
                line.append(" ---> CASE 1 ");
                insertWithAgilentLink(db, inputValues, line.toString(), compoundType);

            } else {
                line.append(" ---> CASE 1 ***DUP*** ");
                insertWithAgilentLink(db, inputValues, line.toString(), compoundType);
            }

// CASE 2. The compound is in MASS MEDIATOR with only one reference. 
        } else if (compareIds.size() == 1) {

            Integer id = compareIds.iterator().next();
            Double mass = null;
            String formula = null;
            String BD = "found in ";

            if (id1 != null) {
                formula = formula1;
                mass = mass1;
                BD += " CAS";
            }
            if (id2 != null) {
                formula = formula2;
                mass = mass2;
                BD += " HMDB";
            }
            if (id3 != null) {
                formula = formula3;
                mass = mass3;
                BD += " LM";
            }
            if (id4 != null) {
                formula = formula4;
                mass = mass4;
                BD += " KEGG";
            }

// CASE 2.A. Different formula. ----> INSERT + LINK TO AGILENT.
            if (!agilentFormula.equals(formula)) {
                line.append(" ---> CASE 2.A. FORMULA [").append(agilentFormula).append(" <> ").append(formula).append("] ").append(BD);
                insertWithAgilentLink(db, inputValues, line.toString(), compoundType);

// CASE 2.B. Same formula.
            } else // CASE 2.B.1. Mass in MASS MEDIATOR is NULL. ----> INSERT + LINK TO AGILENT.
             if (mass == null) {
                    line.append(" -----> CASE 2.B.1. MASS [").append(agilentMass).append(" <> ").append(mass).append("] ");
                    insertWithAgilentLink(db, inputValues, line.toString(), compoundType);

// CASE 2.B.2. Same mass. LINK TO AGILENT.  
                } else if (agilentMass - mass == 0) {
                    line.append(" -----> CASE 2.B.2. MASS [").append(agilentMass).append(" <> ").append(mass).append("] ");
                    createAgilentLink(db, inputValues, id, line.toString());

// CASE 2.B.3. Similar mass.
                } else if (Math.abs(agilentMass - mass) <= ALLOWED_MASS_DIF) {

// CASE 2.B.3.1. Similar mass with LESS precision in MASS MEDIATOR. ----> LINK TO AGILENT + UPDATE MASS.
                    if (getPrecission(agilentMass) > getPrecission(mass)) {
                        line.append(" -----> CASE 2.B.3.1. MASS [").append(agilentMass).append(" >>> ").append(mass).append("] ");
                        createAgilentLinkWithMassUpdate(db, inputValues, id, line.toString());

// CASE 2.B.3.2. Similar mass with MORE precision in MASS MEDIATOR. ----> LINK TO AGILENT.
                    } else {
                        line.append(" -----> CASE 2.B.3.2. MASS [").append(agilentMass).append(" <<< ").append(mass).append("] ");
                        createAgilentLink(db, inputValues, id, line.toString());
                    }

// CASE 2.B.4. Truncated mass in MASS MEDIATOR. ----> LINK TO AGILENT + UPDATE MASS. 
                } else if (agilentMass.toString().startsWith(mass.toString())) {
                    line.append(" -----> CASE 2.B.4. MASS [").append(agilentMass).append(" >>> ").append(mass).append("] ");
                    createAgilentLinkWithMassUpdate(db, inputValues, id, line.toString());

// CASE 2.B.5. Truncated mass in AGILENT. ----> LINK TO AGILENT.
                } else if (mass.toString().startsWith(agilentMass.toString())) {
                    line.append(" -----> CASE 2.B.5. MASS [").append(agilentMass).append(" <<< ").append(mass).append("] ");
                    createAgilentLink(db, inputValues, id, line.toString());

// CASE 2.B.6. Different mass. ----> INSERT + LINK TO AGILENT.
                } else {
                    line.append(" -----> CASE 2.B.6. MASS [").append(agilentMass).append(" <> ").append(mass).append("] ");
                    insertWithAgilentLink(db, inputValues, line.toString(), compoundType);
                }

// CASE 3. The compound is in MASS MEDIATOR with more than one reference. 
        } else {

            int count = 0;
            Integer id = null;
            Double mass = null;

            if (id1 != null && agilentFormula.equals(formula1)) {
                count++;
                id = id1;
                mass = mass1;
            }
            if (id2 != null && !id2.equals(id) && agilentFormula.equals(formula2)) {
                count++;
                id = id2;
                mass = mass2;
            }
            if (id3 != null && !id3.equals(id) && agilentFormula.equals(formula3)) {
                count++;
                id = id3;
                mass = mass3;
            }
            if (id4 != null && !id4.equals(id) && agilentFormula.equals(formula4)) {
                count++;
                id = id4;
                mass = mass4;
            }

            StringBuilder extraLine = new StringBuilder();
            // Writes information.
            extraLine.append(" #### COUNT [").append(count).append("] ");
            extraLine.append(" [AGILENT: ").append(agilentId).append(" - ").append(agilentFormula).append(" | ").append(agilentMass).append("] >>>> ");

            if (id1 != null) {
                extraLine.append(" [CAS: ").append(id1).append(" - ").append(formula1).append(" - ").append(mass1).append(" ] ");
            }
            if (id2 != null) {
                extraLine.append(" [HMDB: ").append(id2).append(" - ").append(formula2).append(" - ").append(mass2).append(" ] ");
            }
            if (id3 != null) {
                extraLine.append(" [LM: ").append(id3).append(" - ").append(formula3).append(" - ").append(mass3).append(" ] ");
            }
            if (id4 != null) {
                extraLine.append(" [KEGG: ").append(id4).append(" - ").append(formula4).append(" - ").append(mass4).append(" ] ");
            }

// CASE 3.A. Only one has the same formula.
            if (count == 1) {

// CASE 3.A.1. Mass in MASS MEDIATOR is NULL. ----> INSERT + LINK TO AGILENT.
                if (mass == null) {
                    line.append(" -----> CASE 3.A.1. MASS [").append(agilentMass).append(" <> ").append(mass).append("] ");
                    insertWithAgilentLink(db, inputValues, line.toString() + extraLine.toString(), compoundType);

// CASE 3.A.2. Same mass. ----> LINK TO AGILENT.  
                } else if (agilentMass - mass == 0) {
                    line.append(" -----> CASE 3.A.2. MASS [").append(agilentMass).append(" == ").append(mass).append("] ");
                    createAgilentLink(db, inputValues, id, line.toString() + extraLine.toString());

// CASE 3.A.3. Similar mass.
                } else if (Math.abs(agilentMass - mass) <= ALLOWED_MASS_DIF) {

// CASE 3.A.3.1. Similar mass with LESS precision in MASS MEDIATOR. ----> LINK TO AGILENT + UPDATE MASS.
                    if (getPrecission(agilentMass) > getPrecission(mass)) {
                        line.append(" -----> CASE 3.A.3.1. MASS [").append(agilentMass).append(" >>> ").append(mass).append("] ");
                        createAgilentLinkWithMassUpdate(db, inputValues, id, line.toString() + extraLine.toString());

// CASE 3.A.3.2. Similar mass with MORE precision in MASS MEDIATOR. ----> LINK TO AGILENT.
                    } else {
                        line.append(" -----> CASE 3.A.3.2. MASS [").append(agilentMass).append(" <<< ").append(mass).append("] ");
                        createAgilentLink(db, inputValues, id, line.toString() + extraLine.toString());
                    }

// CASE 3.A.4. Truncated mass in MASS MEDIATOR. ----> LINK TO AGILENT + UPDATE MASS. 
                } else if (agilentMass.toString().startsWith(mass.toString())) {
                    line.append(" -----> CASE 3.A.4. MASS [").append(agilentMass).append(" >>> ").append(mass).append("] ");
                    createAgilentLinkWithMassUpdate(db, inputValues, id, line.toString() + extraLine.toString());

// CASE 3.A.5. Truncated mass in AGILENT. ----> LINK TO AGILENT.
                } else if (mass.toString().startsWith(agilentMass.toString())) {
                    line.append(" -----> CASE 3.A.5. MASS [").append(agilentMass).append(" <<< ").append(mass).append("] ");
                    createAgilentLink(db, inputValues, id, line.toString() + extraLine.toString());

// CASE 3.A.5. Different mass. ----> INSERT + LINK TO AGILENT.
                } else {
                    line.append(" -----> CASE 3.A.6. MASS [").append(agilentMass).append(" <> ").append(mass).append("] ");
                    insertWithAgilentLink(db, inputValues, line.toString() + extraLine.toString(), compoundType);
                }

// CASE 3.B. More than one have the same formula. ----> INSERT + LINK TO AGILENT.
            } else {
                line.append(" -----> CASE 3.A.6. ");
                insertWithAgilentLink(db, inputValues, line.toString() + extraLine.toString(), compoundType);
            }
        }
    }

    /**
     * Creates new compound in DB and links it to AGILENT.
     *
     * @param db
     * @param inputValues
     */
    private void insertWithAgilentLink(CMMDatabase db, Object[] inputValues, String trace,
            int compoundType)
            throws IOException, Exception {
        String insertion;
        int massMediatorId = 0;
        // Detected
        int compoundStatus = 1;
        // Default for charge is 0 
        int chargeType = 0;
        int chargeNumbers = 0;
        String compound_name = (String) inputValues[0];
        String formula = (String) inputValues[1];
        String formulaType = PatternFinder.getTypeFromFormula(formula);
        if (formulaType.equalsIgnoreCase("null") || formulaType.equals("")) {
            formulaType = "ALL";
        }
        Double mass = Double.parseDouble(((String) inputValues[2]).replace(",", ""));;
        String casId = (String) inputValues[4];
        String agilentId = (String) inputValues[6]; // METLIN
        int compoundIdFromCAS = 0;
        if (casId != null) {
            compoundIdFromCAS = db.getCompounIdFromCAS(casId);
        } else {
            casId = "null";
        }
        Double logP = null;
        if (compoundIdFromCAS == 0) {
            massMediatorId = db.insertCompound(casId, escapeSQL(compound_name), formula, mass,
                    chargeType, chargeNumbers, formulaType, compoundType, compoundStatus, logP);
        } else {
            massMediatorId = compoundIdFromCAS;
        }
        if (massMediatorId == 0) {
            throw new Exception("FATAL ERROR > massMediatorId == 0 in insertWithAgilentLink!!!");
        }
        db.insertCompoundAgilent(massMediatorId, agilentId);

        FileUtils.writeStringToFile(insertLinkFile, trace + "\n", "ISO-8859-1", true);
    }

    /**
     * Creates LINK to AGILENT
     *
     * @param db
     * @param inputValues
     * @param massMediatorId
     */
    private void createAgilentLink(CMMDatabase db, Object[] inputValues, Integer massMediatorId, String trace) throws IOException {

        String agilentId = (String) inputValues[6]; // METLIN

        db.insertCompoundAgilent(massMediatorId, agilentId);
        FileUtils.writeStringToFile(linkFile, trace + "\n", "ISO-8859-1", true);
    }

    /**
     * Creates link to AGILENT and updates mass.
     *
     * @param db
     * @param inputValues
     * @param massMediatorId
     */
    private void createAgilentLinkWithMassUpdate(CMMDatabase db, Object[] inputValues, Integer massMediatorId, String trace) throws IOException {

        String mass = (String) inputValues[2];
        String agilentId = (String) inputValues[6]; // METLIN

        db.insertCompoundAgilent(massMediatorId, agilentId);

        db.updateCompoundMass(massMediatorId, escapeThousandSeparatorSQL(mass));

        FileUtils.writeStringToFile(linkUpdateFile, trace + "\n", "ISO-8859-1", true);

    }

    /**
     *
     * @param mass
     * @return
     */
    public static int getPrecission(Double mass) {
        return mass.toString().split("\\.")[1].length();
    }

    /**
     *
     * @param line
     * @return
     */
    private boolean isCompoundLine(String line) {
        boolean isCompound = true;
        for (String bad : BAD_LINE_STARTS) {
            isCompound = isCompound && !line.startsWith(bad);
        }
        return isCompound;
    }

    /**
     *
     * @param word
     * @return
     */
    private boolean isCAS(String word) {
        return StringUtils.countMatches(word, "-") == 2;
    }

    /**
     *
     * @param word
     * @return
     */
    private boolean isLM(String word) {
        return word.startsWith("LM");
    }

    /**
     *
     * @param word
     * @return
     */
    private boolean isKEGG(String word) {
        return (word.startsWith("C") || word.startsWith("D") || word.startsWith("G"));
    }

    /**
     *
     * @param word
     * @return
     */
    private boolean isHMP(String word) {
        return word.startsWith("HMDB");
    }

    /**
     * Escape ' caracter.
     *
     * @param value
     * @return
     */
    private static String escapeSQL(String value) {
        value = value.replace("'", "\\'");
        value = value.replace("\"", "\\\"");
        return value;
    }

    /**
     * Escape ' caracter.
     *
     * @param value
     * @return
     */
    private static String escapeThousandSeparatorSQL(String value) {
        return value.replace(",", "");
    }
}
