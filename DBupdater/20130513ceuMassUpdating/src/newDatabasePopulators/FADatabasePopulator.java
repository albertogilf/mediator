/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newDatabasePopulators;

import databases.CMMDatabase;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import patternFinders.PatternFinder;

/**
 * JPA definition for compounds extracted in CEMBIO
 *
 * @author: San Pablo-CEU
 * @version: 4.0, 28/09/2017
 */
public class FADatabasePopulator {

    private PatternFinder pf;

    public FADatabasePopulator() {
        this.pf = new PatternFinder();
    }

    /**
     *
     * @param fileName
     * @param db
     */
    public void populate(String fileName, CMMDatabase db) {

        String name;
        String mass;
        String formula;
        int inHouseID = 1;
        int compoundId;
        int chainId;
        String formulaType = "CHNOPS";
        int compoundType = 1; // For Lipids
        int compoundStatus = 3; // Predicted
        String codeCategory = "FA";
        String codeMainClass = "FA01";
        String codeSubClass;
        String codeLevel4Class = "";
        String lipidType = "FA(";
        int numChains = 1;
        int numCarbons;
        int numDoubleBonds;
        String FA;
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            try {
                while ((FA = br.readLine()) != null) {
                    if (FA.startsWith("C")) {
                        String[] attributes = FA.split(",");
                        name = attributes[0];
                        name = name.replaceAll("'", "\\'");
                        name = name.replaceAll("\"", "\\'");
                        numCarbons = pf.getNumberOfCarbons(name);
                        numDoubleBonds = pf.getNumberOfDoubleBonds(name);
                        // Updated weight to calculated
                        mass = attributes[6];
                        formula = attributes[2];
                        String casId = "NULL";
                        int chargeType = 0;
                        int numCharges = 0;
                        String source = "canonical";
                        String logP = null;
                        String description = "FA with " + numChains + " chains and "
                                + numDoubleBonds + " double bonds";
                        compoundId = db.insertCompound(casId, name, formula, mass,
                                chargeType, numCharges, formulaType, compoundType, compoundStatus, logP);

                        db.insertCompoundInHouse(compoundId, inHouseID, source, description);
                        if (numDoubleBonds == 0) {
                            // SATURATED FA
                            codeSubClass = "FA010S";
                        } else if (numDoubleBonds > 0 && numDoubleBonds < 7) {
                            // UNSATURATED FA
                            codeSubClass = "FA010U";
                        } else {
                            codeSubClass = "FA010U";
                            System.out.println("FA WITH " + numDoubleBonds + " double bonds in the file.. ERROR");
                        }

                        db.insertLMClassification(compoundId, codeCategory, codeMainClass, codeSubClass, codeLevel4Class);

                        db.insertLipidClassification(compoundId, lipidType, numChains, numCarbons, numDoubleBonds);
                        chainId = db.insertChain(numCarbons, numDoubleBonds, "", mass, formula);
                        db.insertCompoundChainRelation(compoundId, chainId, 1);
                        Double massOxidation = Double.parseDouble(mass) + 13.9793d;
                        db.insertChain(numCarbons, numDoubleBonds, "O", massOxidation, formula);
                        massOxidation = Double.parseDouble(mass) + 15.9949d;
                        db.insertChain(numCarbons, numDoubleBonds, "OH", massOxidation, formula);
                        massOxidation = Double.parseDouble(mass) + 31.9898d;
                        db.insertChain(numCarbons, numDoubleBonds, "OH-OH", massOxidation, formula);
                        massOxidation = Double.parseDouble(mass) + 31.9898d;
                        db.insertChain(numCarbons, numDoubleBonds, "OOH", massOxidation, formula);
                        massOxidation = Double.parseDouble(mass) + 27.995d;
                        db.insertChain(numCarbons, numDoubleBonds, "COH", massOxidation, formula);
                        massOxidation = Double.parseDouble(mass) + 43.9899d;
                        db.insertChain(numCarbons, numDoubleBonds, "COOH", massOxidation, formula);

                        inHouseID++;
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(FADatabasePopulator.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(FADatabasePopulator.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
