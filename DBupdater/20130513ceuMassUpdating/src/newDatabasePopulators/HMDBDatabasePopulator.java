package newDatabasePopulators;

import checkers.cas.CheckerCas;
import databases.CMMDatabase;
import downloadersOfWebResources.DownloaderOfWebResources;
import exceptions.CompoundNotClassifiedException;
import exceptions.NodeNotFoundException;
import ioDevices.MyFile;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import patternFinders.PatternFinder;
import utilities.Checker;
import utilities.Constants;
import static utilities.Constants.HMDB_FILENAME;
import static utilities.Constants.HMDB_METABOLITE_NODE_NAME;
import static utilities.Constants.HMDB_RESOURCES_PATH;
import static utilities.Constants.PRECISSION_TO_BE_SAME_COMPOUND_MASS_UNIFICATION;
import utilities.ReaderXML;
import utilities.Utilities;
import static utilities.Constants.CLASSYFIRE_LIPID_NODEID;

/**
 * @author: San Pablo-CEU, Alberto Gil de la Fuente
 * @version: 3.1, 17/02/2016
 */
public class HMDBDatabasePopulator {
// TODO. CHECK ANOTHER WAY TO OBTAIN NUMBER OF CARBONS AND DOUBLE BONDS FROM LIPIDMAPS AND/HMDB

    ReaderXML readerXML = new ReaderXML();

    public HMDBDatabasePopulator() {
    }

    private void insertPathways(int compound_id, Element doc, CMMDatabase db) {

        // TODO INSERT PATHWAYS FROM HMDB NOT ONLY FROM KEGG
        // Check database model
        NodeList pathwaysList = doc.getElementsByTagName("pathway");
        String pathwayName = "";
        String pathway_map = "";
        if (pathwaysList != null && pathwaysList.getLength() > 0) {
            for (int i = 0; i < pathwaysList.getLength(); i++) {
                NodeList test = pathwaysList.item(i).getChildNodes();
                if (test.item(1).getNodeName().equals("name")) {
                    pathwayName = test.item(1).getTextContent();
                }
                if (test.item(5).getNodeName().equals("kegg_map_id")) {
                    pathway_map = test.item(5).getTextContent();
                }
                if (!pathway_map.equals("")) {
                    // System.out.println("\n pathway Name : " + pathwayName);
                    // System.out.println("\n pathway Code : " + pathway_map);

                    int pathwayId = db.getPathwayId(pathway_map);
                    //if (pathway_id == 0 && !pathwayName.equals("")) {
                    // If the pathway does not exist in the database, insert it
                    if (pathwayId == 0) {
                        pathwayId = db.insertPathway(pathway_map, pathwayName);
                    }

                    int IsThereRelation = db.getCompoundIDFromCompoundsPathways(compound_id, pathwayId);
                    if (IsThereRelation == 0) {
                        db.insertCompoundPathway(compound_id, pathwayId);
                    }

                }
            }
        }
    }

    /**
     * Populate the database db with compound fileName from HMDB
     *
     * @param fileName
     * @param db
     */
    public void populateFromADownloadedFile(String fileName, CMMDatabase db) {
        String hmdbId = fileName.replaceAll(".xml", "");

        int compoundId = db.getCompoundIdFromHMDBID(hmdbId);
        // System.out.println("\n"+ query + "  compoundId: "+compoundId);
        if (compoundId == 0) {
            insertFromADownloadedFile(hmdbId, fileName, db);
        } else {
            updateFromADownloadedFile(compoundId, hmdbId, fileName, db);
        }
    }

    public void insertFromADownloadedFile(String hmdbId, String fileName, CMMDatabase db) {
        File inputFile = new File(HMDB_RESOURCES_PATH + fileName);
        if (!inputFile.exists()) {
            DownloaderOfWebResources.downloadHMDBXMLFile(hmdbId);
        }
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document dom = dBuilder.parse(inputFile);
            // doc.getDocumentElement().normalize();
            Element doc = dom.getDocumentElement();

            String formula = readerXML.getTextValue(doc, "chemical_formula");
            String mass = readerXML.getTextValue(doc, "monisotopic_molecular_weight");
            String inChI = readerXML.getTextValue(doc, "inchi");

            if (formula.equalsIgnoreCase("null") || mass.equalsIgnoreCase("null")
                    || inChI.equalsIgnoreCase("null")) {
                DownloaderOfWebResources.downloadHMDBXMLFile(hmdbId);
                inputFile = new File(HMDB_RESOURCES_PATH + fileName);
                dbFactory = DocumentBuilderFactory.newInstance();
                dBuilder = dbFactory.newDocumentBuilder();
                dom = dBuilder.parse(inputFile);
                doc = dom.getDocumentElement();
            }

            // Only take the usual name
            /*
            // System.out.println("\n NAME " + name + "\n");
            // String iupac_name = readerXML.getTextValue(doc, "iupac_name");
            // System.out.println("\n iupac_name " + iupac_name + "\n");
            //name = name + "; " + iupac_name;
            // NodeList synonymsList = doc.getElementsByTagName("synonyms").item(0).getChildNodes();
            NodeList synonymsList = doc.getElementsByTagName("synonym");
            String synonyms = "";
            if (synonymsList != null && synonymsList.getLength() > 0) {
                for (int i = 0; i < synonymsList.getLength(); i++) {
                    synonyms = synonyms + "; " + synonymsList.item(i).getTextContent();
                    // System.out.println("\n synonym : " + synonym);
                }
                name = "\"" + name + synonyms + "\"";
            } else {
                name = "\"" + name + "\"";
            }
             */
            int compoundStatus = getStatus(doc, hmdbId);

            String compoundName = readerXML.getTextValue(doc, "name");
            if (compoundName.equalsIgnoreCase("null")) {
                compoundName = "NULL";
            } else {
                compoundName = compoundName.replaceAll("'", "\\'");
                compoundName = compoundName.replaceAll("\"", "\\'");
            }

            formula = readerXML.getTextValue(doc, "chemical_formula");

            String formulaType = PatternFinder.getTypeFromFormula(formula);
            boolean correctFormula;
            correctFormula = !formulaType.equals("");
            // boolean correctFormula = Checker.hasFormulaNoElements(formulaRetrieved);

            inChI = "";
            String inChIKey = "";
            String formulaFromInChI = "";
            String SMILES = "";
            if (correctFormula) {
                // First of all, check if the structure is in the database
                inChI = getInChI(doc);
                if (inChI.equals("NULL")) {
                    inChI = "";
                }
                inChIKey = getInChIKey(doc, hmdbId);
                if (inChIKey.equals("NULL")) {
                    inChIKey = "";
                }
                SMILES = getSmiles(doc);
                if (inChIKey.equals("NULL")) {
                    inChIKey = "";
                }
                formulaFromInChI = Checker.getFormulaFromInChI(inChI);
            } else {
                String logFileFormulaBadElements = "./log/hmdb_formula_bad_elements.txt";
                String duplicateInformation = "\nCompound " + hmdbId + " has a formula "
                        + formula + " with no chemical elements";
                MyFile.write(duplicateInformation, logFileFormulaBadElements);
            }
            // System.out.println("\n FORMULA " + formula + "\n");
            mass = readerXML.getTextValue(doc, "monisotopic_molecular_weight");
            double massFromHMDB;
            if (mass.equals("null")) {
                mass = "NULL";
                massFromHMDB = 0.0d;
            } else {
                massFromHMDB = Double.parseDouble(mass);
            }
            String mol_weight = readerXML.getTextValue(doc, "average_molecular_weight");
            String logP = null;
            // System.out.println("\n MASS " + mass + "\n");
            String casId = readerXML.getTextValue(doc, "cas_registry_number");

            int[] charges = PatternFinder.getChargeFromSmiles(SMILES);
            int chargeType = charges[0];
            int numCharges = charges[1];
            // Check if the cas has coherence with cas compounds from the checker
            // Insert the compound in compound_cas
            // insertCasId. If there is no compound, try to download. If there is one, leave it as before
            // It is passed casId and not casId because it is handled without \" \" at the beginning and the end
            CheckerCas.insertCasId(db, casId);

            /*
            String metlin_id = readerXML.getTextValue(doc, "metlin_id");
            if (metlin_id.equals("null")) {
                metlin_id = "NULL";
            }
            // System.out.println("\n metlin_id " + metlin_id + "\n");
            String kegg_id = readerXML.getTextValue(doc, "kegg_id");
            if (kegg_id.equals("null")) {
                kegg_id = "NULL";
            } else {
                kegg_id = "\"" + kegg_id + "\"";
            }
            // System.out.println("\n kegg_id " + kegg_id + "\n");

            String pc_id = readerXML.getTextValue(doc, "pubchem_compound_id");
            if (pc_id.equals("null")) {
                pc_id = "NULL";
            }
             */
            String query;
            String insertion;
            int compound_id;
            compound_id = 0;
            boolean foundCas = false;
            // Compounds which have InChI
            //if (correctFormula && !inChIKey.equals("")) {
            if (!inChIKey.equals("")) {
                // Check if there is any compound with the same structure
                compound_id = db.getCompoundIdFromInchiKey(inChIKey);
                // Case 1 There is a compound with same structure
                if (compound_id != 0) {
// Case 1.0 // Case 1.0 the compound has another cas previously
                    // Check Cas ID
                    if (!casId.equals("null")) {
                        String casFromDB = db.getCasFromCompound(compound_id);
                        if (!casFromDB.equals("")) {
                            if (!casId.equals(casFromDB)) {
                                String logFileDuplicates = "./log/hmdb_structure_duplicate_different_cas.txt";
                                String duplicateInformation = "\nCompound " + hmdbId + " has other compound "
                                        + compound_id + " with the same structure but different CAS";
                                MyFile.write(duplicateInformation, logFileDuplicates);
                                // Check information from ChemIdPlus to check what cas is right
                                String inchiFromChemId = CheckerCas.getInChIKeyFromDB(casId, db);
                                if (inchiFromChemId.equals(inChIKey)) {
                                    // Cas Id information is from this compound. 
                                    // Update CAS Identifier
                                    db.updateCompoundCAS(compound_id, casId);
                                }
                            }
                        }
                    }

// Case 1.1 The compound has previously different formula and/or mass
                    String formulaFromCompound = db.getformulaFromCompound(compound_id);
                    double massFromCompound = db.getMassFromCompound(compound_id);

                    if (formula.equals(formulaFromCompound) && (massFromCompound - massFromHMDB) < PRECISSION_TO_BE_SAME_COMPOUND_MASS_UNIFICATION) {
                        // The compound is the same, mass is closer between existent compound and HMDB compound 
                        // and formula is the same
                        // Update information from HMDB if they have more precission that previous information
                        int precissionFromDB = Utilities.getPrecission(massFromCompound);
                        int precissionFromHMDB = Utilities.getPrecission(massFromHMDB);
                        if (precissionFromDB < precissionFromHMDB) {
                            db.updateCompoundMass(compound_id, mass);
                        }
                    } else if (formula.equals(formulaFromCompound) && (massFromCompound - massFromHMDB) > PRECISSION_TO_BE_SAME_COMPOUND_MASS_UNIFICATION) {
                        // Case 1.2 There is a compound with different mass

                        String logFileDuplicates = "./log/HMDB_structure_duplicate_bad_mass.txt";
                        String duplicateInformation = "\nCompound " + hmdbId + " has other compound "
                                + compound_id + " with same structure and different mass";
                        MyFile.write(duplicateInformation, logFileDuplicates);
                    } else if (!formula.equals(formulaFromCompound) && (massFromCompound - massFromHMDB) < PRECISSION_TO_BE_SAME_COMPOUND_MASS_UNIFICATION) {
                        // Case 1.3 There is a compound with different formula

                        int precissionFromDB = Utilities.getPrecission(massFromCompound);
                        int precissionFromHMDB = Utilities.getPrecission(massFromHMDB);
                        if (precissionFromDB < precissionFromHMDB) {
                            db.updateCompoundMass(compound_id, mass);
                        }
                        String logFileDuplicates = "./log/HMDB_structure_duplicate_bad_formula.txt";
                        String duplicateInformation = "\nCompound " + hmdbId + " has other compound "
                                + compound_id + " with same structure and different formula";
                        MyFile.write(duplicateInformation, logFileDuplicates);
                    } else {
                        String logFileDuplicates = "./log/HMDB_structure_duplicate.txt";
                        String duplicateInformation = "\nCompound " + hmdbId + " has other compound "
                                + compound_id + " with the same structure, different formula and different mass";
                        MyFile.write(duplicateInformation, logFileDuplicates);
                    }

                    // Compound is the same if they have the same structure
                    db.insertCompoundHMDB(compound_id, hmdbId);

                    // If the compound is already in the database, just do the link
                    // to the HMDB, link the pathways compound and exit the method
                    insertPathways(compound_id, doc, db);
                    return;
                } else // Check Cas ID
                if (!casId.equals("")) {
                    // Compound has InChI and Cas Identifier
                    // Check information from ChemIdPlus to check if cas is right
                    String inchiFromChemId = CheckerCas.getInChIKeyFromDB(casId, db);
                    if (!inchiFromChemId.equals(inChIKey)) {
                        // If the first layer of the InChI is the same, then we keep the cas Id.
                        // If not, we update the casId to null
                        String firstLayerInChIFromHMDB = inChIKey.split("-")[0];
                        String firstLayerInChIFromChemId = inchiFromChemId.split("-")[0];
                        //System.out.println("First Layer HMDB: " + firstLayerInChIFromHMDB + " From ChemID: " + firstLayerInChIFromChemId);
                        if (!firstLayerInChIFromHMDB.equals(firstLayerInChIFromChemId)) // If the cas identifier from ChemId corresponds to another compound (InChIKey),
                        // then update casId to null
                        {
                            casId = "null";
                        }
                    }
                }
            }

            // Check if the compound CAS exists before
            if (!casId.equalsIgnoreCase("NULL")) {
                compound_id = db.getCompounIdFromCAS(casId);

                // Writing compounds of HMDB with bad information
                String formulaCas = CheckerCas.getFormulaFromDB(casId, db);
                double weightCas = CheckerCas.getMassFromDB(casId, db);

                if (correctFormula && !formula.equals("NULL")) {
                    if (!formulaCas.equals("")) {
                        if (!formulaCas.equals(formula)) {
                            //System.out.println("\n HMDB COMPOUND INCOHERENT FORMULA WITH CHEMID.. "
                            //        + " CHEMID: " + formulaCas + "HMDB: " + formulaRetrieved);
                            String logFileDuplicates = "./log/hmdb_chemid_formula_incoherence.txt";
                            String duplicateInformation = "Inserting compound " + hmdbId
                                    + "\tcas: " + casId
                                    + "\tformula: " + formula + "\tmass: " + mass
                                    + "\n COMPOUND FROM CHEMID PLUS: \t FORMULA: " + formulaCas
                                    + "\t MASS: " + weightCas
                                    + "\n";
                            MyFile.write(duplicateInformation, logFileDuplicates);
                        }
                    }
                }
                if (!mass.equals("NULL")) {
                    int isWeightCas = Double.compare(0.0d, weightCas);
                    if (isWeightCas != 0 && Double.parseDouble(mass) - weightCas > 2) {
                        String logFileDuplicates = "./log/hmdb_mass_incoherence.txt";
                        String duplicateInformation = "Inserting compound " + hmdbId
                                + "\tcas: " + casId
                                + "\tformula: " + formula + "\t mol weight: " + mass
                                + "\tmass: " + mass
                                + "\n COMPOUND FROM CHEMID PLUS: \t FORMULA" + formulaCas
                                + "\n MASS: " + weightCas
                                + "\n";
                        MyFile.write(duplicateInformation, logFileDuplicates);
                    }
                }
                if (compound_id != 0) {
                    query = "SELECT * FROM compounds where cas_id='" + casId + "'";
                    String compoundDB = db.getInformationCompound(query);
                    String logFileDuplicates = "./log/hmdb_cas_duplicates.txt";
                    String duplicateInformation = "Inserting compound " + hmdbId
                            + "\ncas: " + casId + "\nname: " + compoundName
                            + "\nformula: " + formula + "\nmass: " + mass
                            + "\n CAS Compound already in the database: \n"
                            + compoundDB + "\n";
                    MyFile.write(duplicateInformation, logFileDuplicates);
                    foundCas = true;
                }
            }

// Check what compounds has different Formula that InChI
            if (correctFormula && !formula.equals("NULL")) {

                if (!formulaFromInChI.equals("")) {
                    if (!formulaFromInChI.equals(formula)) {
                        String logFileDuplicates = "./log/hmdb_inchi_formula_incoherence.txt";
                        String duplicateInformation = "Inserting compound " + hmdbId
                                + "\tcas: " + casId + "\tformula: " + formula
                                + "\n FORMULA FROM INCHI: \t FORMULA: " + formulaFromInChI
                                + "\n";
                        MyFile.write(duplicateInformation, logFileDuplicates);
                        //Then, the real formula is the formula from InChI identifier
                        //formula = "\"" + formulaFromInChI + "\"";
                    }
                }
            }

            String lipidType;
            int numChains;
            int numCarbons;
            int numDoubleBonds;
            List<String> chains;
            lipidType = PatternFinder.getLipidTypeFromName(compoundName);
            if (lipidType.equalsIgnoreCase("")) {
                chains = new LinkedList<>();
                numChains = 0;
                numDoubleBonds = 0;
                numCarbons = 0;
            } else {
                numCarbons = PatternFinder.getNumberOfCarbonsFromName(compoundName);
                numDoubleBonds = PatternFinder.getNumberOfDoubleBondsFromName(compoundName);
                chains = PatternFinder.getListOfChains(compoundName);
                numChains = chains.size();
            }

            // System.out.println("COMPOUND: " + hmdbId + " MAIN: " + compound_main_class 
            //        + " CLASS: " + compound_class + " SUBCLASS: " + compound_sub_class);
            String compoundOrigin = ReaderXML.getStringAllNodes(doc, "origin");

            // KNOWLEDGE ABOUT IONIZATION TECHNIQUES!
            int compoundType = 0;
            List<String> ancestorNodes = new LinkedList();
            try {
                ancestorNodes = downloadersOfWebResources.DownloaderOfWebResources.getAncestorNodesCompoundClassyfireClassification(inChI, inChIKey);
                if (ancestorNodes.contains(CLASSYFIRE_LIPID_NODEID)) {
                    compoundType = 1;
                }
            } catch (CompoundNotClassifiedException ex) {
            }

            compound_id = db.insertCompound(casId, compoundName, formula, mass,
                    chargeType, numCharges, formulaType, compoundType, compoundStatus, logP);
            // System.out.println("\nID retrieved: " + compound_id);
            if (correctFormula && !inChIKey.equals("")) {
                db.insertIdentifiers(compound_id, inChI, inChIKey, SMILES);
            }
            db.insertCompoundHMDB(compound_id, hmdbId);

            // Insert the classification from HMDB
            try {
                db.insertFullCompoundClassyfireClassification(compound_id, ancestorNodes);
            } catch (NodeNotFoundException ex) {
                Logger.getLogger(HMDBDatabasePopulator.class.getName()).log(Level.SEVERE, null, ex);
            }

            // Insert classification from lipids
            db.insertLipidClassification(compound_id, lipidType, numChains, numCarbons, numDoubleBonds);
            int existChainsForCompound = db.areThereAlreadyChains(compound_id);
            if (existChainsForCompound == 0) {
                for (String chain : chains) {
                    int carbonsOfChain = PatternFinder.getNumberOfCarbons(chain);
                    int doubleBondsOfChain = PatternFinder.getNumberOfDoubleBonds(chain);
                    String oxidationOfChain = PatternFinder.getOxidationFromAbbrev(chain);
                    int chain_id = db.getChainID(carbonsOfChain, doubleBondsOfChain, oxidationOfChain);
                    if (chain_id == 0) {
                        double massOfChain = Utilities.calculateMassChain(carbonsOfChain,
                                doubleBondsOfChain, oxidationOfChain);
                        String formulaOfChain = Utilities.calculateFormulaChain(carbonsOfChain,
                                doubleBondsOfChain, oxidationOfChain);
                        db.insertChain(carbonsOfChain, doubleBondsOfChain,
                                oxidationOfChain, massOfChain, formulaOfChain);
                    }
                    int repetitions = db.getRepetitionsSameChain(compound_id, chain_id);
                    if (repetitions == 0) {
                        db.insertCompoundChainRelation(compound_id, chain_id, 1);
                    } else {
                        db.updateCompoundChainRepetitions(compound_id, chain_id, (repetitions + 1));
                    }
                }
            }

            // Insert the information about pathways
            insertPathways(compound_id, doc, db);

        } catch (ParserConfigurationException | SAXException pce) {
            System.out.println(pce.getMessage());
        } catch (IOException ioe) {
            System.err.println(ioe.getMessage());
        }

    }

    public void updateFromADownloadedFile(int compound_id, String hmdbId, String fileName, CMMDatabase db) {
// Only update Cas and Mass if the precission is greater than before
        File inputFile = new File(HMDB_RESOURCES_PATH + fileName);
        if (!inputFile.exists()) {
            DownloaderOfWebResources.downloadHMDBXMLFile(hmdbId);
        }
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document dom = dBuilder.parse(inputFile);
            // doc.getDocumentElement().normalize();

            Element doc = dom.getDocumentElement();

            String formula = readerXML.getTextValue(doc, "chemical_formula");
            String mass = readerXML.getTextValue(doc, "monisotopic_molecular_weight");
            String inChI = readerXML.getTextValue(doc, "inchi");
            // Check if the compound is updated to read it or not
            String dateUpdateString = readerXML.getTextValue(doc, "update_date");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date dateUpdate;
            try {
                dateUpdate = formatter.parse(dateUpdateString);
                if (dateUpdate.before(Constants.LAST_UPDATE_CMM)) {
                    //    return;
                }
            } catch (ParseException ex) {
                Logger.getLogger(HMDBDatabasePopulator.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("UPDATE DATE of: " + hmdbId + "not recognizible");
            }

            if (formula.equalsIgnoreCase("null") || mass.equalsIgnoreCase("null")
                    || inChI.equalsIgnoreCase("null")) {
                DownloaderOfWebResources.downloadHMDBXMLFile(hmdbId);
                inputFile = new File(HMDB_RESOURCES_PATH + fileName);
                dbFactory = DocumentBuilderFactory.newInstance();
                dBuilder = dbFactory.newDocumentBuilder();
                dom = dBuilder.parse(inputFile);
                doc = dom.getDocumentElement();
            }

            int compoundStatus = getStatus(doc, hmdbId);
            if (compoundStatus != 16) {
                db.updateCompoundStatus(compound_id, compoundStatus);
            }

            String compoundName = readerXML.getTextValue(doc, "name");
            if (compoundName.equalsIgnoreCase("null")) {
                compoundName = "NULL";
            } else {
                compoundName = compoundName.replaceAll("'", "\\'");
                compoundName = compoundName.replaceAll("\"", "\\'");
            }
            formula = readerXML.getTextValue(doc, "chemical_formula");
            String formulaType = PatternFinder.getTypeFromFormula(formula);
            boolean correctFormula;
            correctFormula = !formulaType.equals("");
            // boolean correctFormula = Checker.hasFormulaNoElements(formulaRetrieved);

            inChI = "";
            String inChIKey = "";
            String formulaFromInChI = "";
            String SMILES = "";
            if (correctFormula) {
                // First of all, check if the structure is in the database
                inChI = getInChI(doc);
                if (inChI.equals("NULL")) {
                    inChI = "";
                }
                inChIKey = getInChIKey(doc, hmdbId);
                if (inChIKey.equals("NULL")) {
                    inChIKey = "";
                }
                SMILES = getSmiles(doc);
                db.updateSMILESIdentifier(compound_id, SMILES);
                if (SMILES.equals("NULL")) {
                    SMILES = "";
                }

                String inchiKeyFromDb = db.getInChIKeyFromCompound(compound_id);
                if (!inChIKey.equals(inchiKeyFromDb)) {
                    System.out.println("BAD INCHI KEY IN BBDD: ");
                    int new_compound_id = db.getCompoundIdFromInchiKey(inChIKey);
                    if (new_compound_id > 0) {
                        db.deleteCompoundHMDB(compound_id, hmdbId);
                        db.insertCompoundHMDB(new_compound_id, hmdbId);
                    } else if (new_compound_id == 0) {
                        if (inchiKeyFromDb.length() == 27) {
                            db.updateIdentifiers(compound_id, inChI, inChIKey, SMILES);
                        } else {
                            db.insertIdentifiers(compound_id, inChI, inChIKey, SMILES);
                        }
                    } else {
                        System.out.println("ERROR OBTAINING ID FROM INCHI");
                    }
                }

                formulaFromInChI = Checker.getFormulaFromInChI(inChI);
            } else {
                String logFileFormulaBadElements = "./log/hmdb_formula_bad_elements.txt";
                String duplicateInformation = "\nCompound " + hmdbId + " has a formula "
                        + formula + " with no chemical elements";
                MyFile.write(duplicateInformation, logFileFormulaBadElements);
            }
            // System.out.println("\n FORMULA " + formula + "\n");
            mass = readerXML.getTextValue(doc, "monisotopic_molecular_weight");
            double massFromHMDB;
            if (mass.equals("null")) {
                mass = "NULL";
                massFromHMDB = 0.0d;
            } else {
                massFromHMDB = Double.parseDouble(mass);
            }
            String mol_weight = readerXML.getTextValue(doc, "average_molecular_weight");
            // System.out.println("\n MASS " + mass + "\n");
            String casId = readerXML.getTextValue(doc, "cas_registry_number");

            int[] charges = PatternFinder.getChargeFromSmiles(SMILES);
            int chargeType = charges[0];
            int numCharges = charges[1];
            // Check if the cas has coherence with cas compounds from the checker
            // Insert the compound in compound_cas
            // insertCasId. If there is no compound, try to download. If there is one, leave it as before
            // It is passed casId and not cas_id because it is handled without \" \" at the beginning and the end
            CheckerCas.insertCasId(db, casId);

// Case 0. Update Mass
            double massFromCompound = db.getMassFromCompound(compound_id);
            if (Math.abs(massFromCompound - massFromHMDB) < PRECISSION_TO_BE_SAME_COMPOUND_MASS_UNIFICATION) {
                // The compound is the same, mass is closer between existent compound and kegg compound 
                // and formula is the same
                // Update information from Kegg if they have more precission that previous information
                if (massFromHMDB > PRECISSION_TO_BE_SAME_COMPOUND_MASS_UNIFICATION) {
                    int precissionFromDB = Utilities.getPrecission(massFromCompound);
                    int precissionFromKegg = Utilities.getPrecission(massFromHMDB);
                    if (precissionFromDB < precissionFromKegg) {
                        db.updateCompoundMass(compound_id, mass);
                    }
                }
            }

// Case 0.1. Update Mass cause is null or lower than PRECISSION_TO_BE_SAME_COMPOUND_MASS_UNIFICATION in CMM
            if (massFromCompound < PRECISSION_TO_BE_SAME_COMPOUND_MASS_UNIFICATION
                    && massFromHMDB > PRECISSION_TO_BE_SAME_COMPOUND_MASS_UNIFICATION) {
                db.updateCompoundMass(compound_id, mass);
            }

// Case 1. Update Cas
            if (!casId.equals("")) {
                String casFromCompound = db.getCasFromCompound(compound_id);
                if (!casFromCompound.equals("")) {
                    if (!casId.equals(casFromCompound)) {
                        // Check information from ChemIdPlus to check what cas is right
                        String inchiFromChemId = CheckerCas.getInChIKeyFromDB(casId, db);
                        if (inchiFromChemId.equals(inChIKey)) {
                            // Cas Id information is from this compound. 
                            // Update CAS Identifier
                            db.updateCompoundCAS(compound_id, casId);

                        }
                    }
                } else {
                    String inchiFromChemId = CheckerCas.getInChIKeyFromDB(casId, db);
                    if (inchiFromChemId.equals(inChIKey)) {
                        db.updateCompoundCAS(compound_id, casId);
                    }
                }
            }

// Case 2. Update Formula cause is null in CMM
            String formulaFromCompound = db.getformulaFromCompound(compound_id);
            if (formulaFromCompound.equalsIgnoreCase("null")) {
                db.updateCompoundFormula(compound_id, formula);
            }

// Case 3. Update InChI cause it is not included in CMM
            String InChIKeyFromCompound = db.getInChIKeyFromCompound(compound_id);
            if (InChIKeyFromCompound.equals("") || InChIKeyFromCompound.equals("null")) {
                // InChI and InChIKEY are not in the database

                db.insertIdentifiers(compound_id, inChI, inChIKey, SMILES);
            }

            // Start with knowledge
            int compoundType = 0;
            List<String> ancestorNodes = new LinkedList();
            try {
                ancestorNodes = downloadersOfWebResources.DownloaderOfWebResources.getAncestorNodesCompoundClassyfireClassification(inChI, inChIKey);
                if (ancestorNodes.contains(CLASSYFIRE_LIPID_NODEID)) {
                    compoundType = 1;
                    db.updateCompoundType(compound_id, compoundType);
                }
            } catch (CompoundNotClassifiedException ex) {
            }
            try {
                db.insertFullCompoundClassyfireClassification(compound_id, ancestorNodes);
            } catch (NodeNotFoundException ex) {
                Logger.getLogger(HMDBDatabasePopulator.class.getName()).log(Level.SEVERE, null, ex);
            }

            String lipidType;
            int numChains;
            int numCarbons;
            int numDoubleBonds;
            List<String> chains;
            lipidType = PatternFinder.getLipidTypeFromName(compoundName);
            if (lipidType.equalsIgnoreCase("")) {
                chains = new LinkedList<>();
                numChains = 0;
                numDoubleBonds = 0;
                numCarbons = 0;
            } else {
                numCarbons = PatternFinder.getNumberOfCarbonsFromName(compoundName);
                numDoubleBonds = PatternFinder.getNumberOfDoubleBondsFromName(compoundName);
                chains = PatternFinder.getListOfChains(compoundName);
                numChains = chains.size();
            }

            // System.out.println("COMPOUND: " + hmdbId + " MAIN: " + compound_main_class 
            //        + " CLASS: " + compound_class + " SUBCLASS: " + compound_sub_class);
            String compoundOrigin = ReaderXML.getStringAllNodes(doc, "origin");
            String compound_origin;
            if (compoundOrigin.equals("")) {
                compound_origin = "NULL";
            } else {
                compound_origin = "\"" + compoundOrigin + "\"";
            }
//            System.out.println("\nORIGIN: " + compound_origin);

            int existChainsForCompound = db.areThereAlreadyChains(compound_id);
            if (existChainsForCompound == 0) {
                for (String chain : chains) {
                    int carbonsOfChain = PatternFinder.getNumberOfCarbons(chain);
                    int doubleBondsOfChain = PatternFinder.getNumberOfDoubleBonds(chain);
                    String oxidationOfChain = PatternFinder.getOxidationFromAbbrev(chain);
                    int chain_id = db.getChainID(carbonsOfChain, doubleBondsOfChain, oxidationOfChain);
                    if (chain_id == 0) {
                        double massOfChain = Utilities.calculateMassChain(carbonsOfChain,
                                doubleBondsOfChain, oxidationOfChain);
                        String formulaOfChain = Utilities.calculateFormulaChain(carbonsOfChain,
                                doubleBondsOfChain, oxidationOfChain);
                        db.insertChain(carbonsOfChain, doubleBondsOfChain,
                                oxidationOfChain, massOfChain, formulaOfChain);
                    }
                    int repetitions = db.getRepetitionsSameChain(compound_id, chain_id);
                    if (repetitions == 0) {
                        db.insertCompoundChainRelation(compound_id, chain_id, 1);
                    } else {
                        db.updateCompoundChainRepetitions(compound_id, chain_id, (repetitions + 1));
                    }
                }
            }

            // Insert the information about pathways
            insertPathways(compound_id, doc, db);
            // KNOWLEDGE ABOUT IONIZATION TECHNIQUES!
        } catch (ParserConfigurationException | SAXException pce) {
            System.out.println(pce.getMessage());
        } catch (IOException ioe) {
            System.err.println(ioe.getMessage());
        }
    }

    public void updateStatusFromADownloadedFile(int compound_id, String hmdbId, String fileName, CMMDatabase db) {
// Only update Cas and Mass if the precission is greater than before
        File inputFile = new File(HMDB_RESOURCES_PATH + fileName);
        if (!inputFile.exists()) {
            DownloaderOfWebResources.downloadHMDBXMLFile(hmdbId);
        }
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document dom = dBuilder.parse(inputFile);
            // doc.getDocumentElement().normalize();

            Element doc = dom.getDocumentElement();

            String formulaRetrieved = readerXML.getTextValue(doc, "chemical_formula");
            String mass = readerXML.getTextValue(doc, "monisotopic_molecular_weight");
            String inChI = readerXML.getTextValue(doc, "inchi");
            // Check if the compound is updated to read it or not
            String dateUpdateString = readerXML.getTextValue(doc, "update_date");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date dateUpdate;
            try {
                dateUpdate = formatter.parse(dateUpdateString);
                if (dateUpdate.before(Constants.LAST_UPDATE_CMM)) {
                    return;
                }
            } catch (ParseException ex) {
                Logger.getLogger(HMDBDatabasePopulator.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("UPDATE DATE of: " + hmdbId + "not recognizible");
            }

            if (formulaRetrieved.equalsIgnoreCase("null") || mass.equalsIgnoreCase("null")
                    || inChI.equalsIgnoreCase("null")) {
                DownloaderOfWebResources.downloadHMDBXMLFile(hmdbId);
                inputFile = new File(HMDB_RESOURCES_PATH + fileName);
                dbFactory = DocumentBuilderFactory.newInstance();
                dBuilder = dbFactory.newDocumentBuilder();
                dom = dBuilder.parse(inputFile);
                doc = dom.getDocumentElement();
            }

            int compoundStatus = getStatus(doc, hmdbId);
            if (compoundStatus != 16) {
                db.updateCompoundStatus(compound_id, compoundStatus);
            } else {
                System.out.println("NO STATUS! HMDB ID: " + hmdbId + " STATUS " + compoundStatus + " compound_id" + compound_id);
            }

        } catch (ParserConfigurationException | SAXException pce) {
            System.out.println(pce.getMessage());
        } catch (IOException ioe) {
            System.err.println(ioe.getMessage());
        }
    }

    /**
     * Classify compound fileName from HMDB. TODO. NOT DEVELOPED YET, COMPOUNDS
     * ARE CLASSIFIED WHEN THEY ARE INSERTED OR UPDATED.
     *
     * @param fileName
     * @param db
     */
    public void updateStatusFromJSON(String fileName, CMMDatabase db) {
        String hmdbId = fileName.replaceAll(".xml", "");

        int compoundId = db.getCompoundIdFromHMDBID(hmdbId);

        if (compoundId == 0) {
            System.out.println("HMDB ID: " + hmdbId + " not inserted.");
            insertFromADownloadedFile(hmdbId, fileName, db);
        } else {
            updateStatusFromADownloadedFile(compoundId, hmdbId, fileName, db);
        }
    }

    public void updateStatusFromHMDB(CMMDatabase db) {

        File dir = new File(HMDB_RESOURCES_PATH);
        String[] files = dir.list();

        for (String file : files) {
//            System.out.println(fichero);
            if (file.endsWith(".xml")) {
                if (!file.equals(HMDB_FILENAME)) {
                    updateStatusFromJSON(file, db);
                }
            }
        }

    }

    /**
     * Populate database from HMDB
     *
     * @param db
     */
    public void populateFromHMDB(CMMDatabase db) {
        /*
        insertFromADownloadedFile("HMDB00001.xml", db);
        insertFromADownloadedFile("HMDB00002.xml", db);
        insertFromADownloadedFile("HMDB00003.xml", db);
        insertFromADownloadedFile("HMDB00004.xml", db);
        insertFromADownloadedFile("HMDB00005.xml", db);
        insertFromADownloadedFile("HMDB00006.xml", db);
        insertFromADownloadedFile("HMDB00007.xml", db);
        insertFromADownloadedFile("HMDB00008.xml", db);
        insertFromADownloadedFile("HMDB00009.xml", db);
        insertFromADownloadedFile("HMDB00010.xml", db);
         */

        File logDuplicates = new File("./log/hmdb_formula_bad_elements.txt");
        if (logDuplicates.exists()) {
            logDuplicates.delete();
        }

        logDuplicates = new File("./log/hmdb_cas_duplicates.txt");
        if (logDuplicates.exists()) {
            logDuplicates.delete();
        }
        logDuplicates = new File("./log/hmdb_structure_duplicate.txt");
        if (logDuplicates.exists()) {
            logDuplicates.delete();
        }
        logDuplicates = new File("./log/hmdb_structure_formula_duplicate");
        if (logDuplicates.exists()) {
            logDuplicates.delete();
        }
        File logFormulaHMDBInChIIncoherence = new File("./log/hmdb_inchi_formula_incoherence.txt");
        if (logFormulaHMDBInChIIncoherence.exists()) {
            logFormulaHMDBInChIIncoherence.delete();
        }
        File logFormulaHMDBChemIdIncoherences = new File("./log/hmdb_chemid_formula_incoherence.txt");
        if (logFormulaHMDBChemIdIncoherences.exists()) {
            logFormulaHMDBChemIdIncoherences.delete();
        }
        File logMassIncoherences = new File("./log/hmdb_mass_incoherence.txt");
        if (logMassIncoherences.exists()) {
            logMassIncoherences.delete();
        }

        File dir = new File(HMDB_RESOURCES_PATH);
        String[] files = dir.list();

        for (String file : files) {
//            System.out.println(fichero);
            if (file.endsWith(".xml")) {
                if (!file.equals(HMDB_FILENAME)) {
                    populateFromADownloadedFile(file, db);
                }
            }
        }
    }

    public void printOntologies(CMMDatabase db) {

        // TODO EVERYTHING RELATED WITH ONTOLOGIES (TAKEN DIRECTLY FROM HMDB DB)
        //File dir = new File(HMDB_RESOURCES_PATH);
        File dir = new File("C:\\Users\\ceu\\Desktop\\alberto\\CMM\\DBUpdater\\resources\\HMDB\\");
        String[] files = dir.list();
        getOntology("test_ontology.xml", db);
        /*
        for (String file : files) {
//            System.out.println(fichero);
            if (file.endsWith(".xml")) {
                if (!file.equals(HMDB_FILENAME)) {
                    getOntology(file, db);
                }
            }
        }
         */
    }

    private void getOntology(String fileName, CMMDatabase db) {
        String hmdbId = fileName.replaceAll(".xml", "");
        //File inputFile = new File(HMDB_RESOURCES_PATH + fileName);
        File inputFile = new File("C:\\Users\\ceu\\Desktop\\alberto\\CMM\\DBUpdater\\resources\\HMDB\\" + fileName);
        if (!inputFile.exists()) {
            DownloaderOfWebResources.downloadHMDBXMLFile(hmdbId);
        }
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document dom = dBuilder.parse(inputFile);
            // doc.getDocumentElement().normalize();
            Element doc = dom.getDocumentElement();
            Node ontologyNode = getOntology(doc);
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(HMDBDatabasePopulator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Check if the compound compoundId is coherent with the HMDB compound
     * database
     *
     * @param compoundId
     * @param db
     * @param hmdbId
     * @return 0 if compounds have the same InChIKey. 4 If the compound had not
     * InChI (In this case the InChI and InChIKey are added). 8 if the compound
     * has different inchiKey. 12 if the HMDB compound cannot be opened
     */
    public int checkFromHmdb(int compoundId, CMMDatabase db, String hmdbId) {
        int result = 12;
        String fileName = HMDB_RESOURCES_PATH + hmdbId + ".xml";
        File inputFile = new File(fileName);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document dom = dBuilder.parse(inputFile);
            // doc.getDocumentElement().normalize();
            Element doc = dom.getDocumentElement();
            String inChIKeyCompound = db.getInChIKeyFromCompound(compoundId);
            String inChIKeyHMDB = getInChIKey(doc, hmdbId);
            String SMILES = getSmiles(doc);
            if (!inChIKeyCompound.equals("")) {

                if (!inChIKeyCompound.equals(inChIKeyHMDB)) {
                    return 8;
                }
            } else {
                String inChIHMDB = getInChI(doc);
                db.insertIdentifiers(compoundId, inChIHMDB, inChIKeyHMDB, SMILES);
                return 4;
            }

        } catch (ParserConfigurationException | SAXException pce) {
            System.out.println(pce.getMessage());
        } catch (IOException ioe) {
            System.err.println(ioe.getMessage());
        }
        return result;
    }

    /* 
    * Get the status of the compound
    * @param doc
    * @param hmdbId
    * @return int
     */
    private int getStatus(Element doc, String hmdbId) {
        String status;
        int statusCode;
        status = readerXML.getTextValue(doc, "status");
        switch (status) {
            case "expected":
                statusCode = 0;
                break;
            case "detected":
                statusCode = 1;
                break;
            case "quantified":
                statusCode = 2;
                break;
            case "predicted":
                statusCode = 3;
                break;
            default:
                statusCode = 16;
                break;
        }
        return statusCode;
    }

    private String getInChI(Element doc) {
        String inChI;
        inChI = readerXML.getTextValue(doc, "inchi");
        if (inChI.equalsIgnoreCase("null")) {
            inChI = "NULL";
        }
        return inChI;
    }

    private String getSmiles(Element doc) {
        String SMILES;
        SMILES = readerXML.getTextValue(doc, "smiles");
        if (SMILES.equalsIgnoreCase("null")) {
            SMILES = "NULL";
        }
        return SMILES;
    }

    private Node getOntology(Element doc) {
        Node rootOntologyNode;
        rootOntologyNode = doc.getElementsByTagName("ontology").item(0);
        String rootName = rootOntologyNode.getNodeName();
        //System.out.println("ONTOLOGY: " + rootName);
        //System.out.println("ROOT: " + rootName);
        getDescendants(rootOntologyNode, null);
        /*
        NodeList ontologyNodesList = rootOntologyNode.getChildNodes();
        for (int i = 0; i < ontologyNodesList.getLength(); i++) {
            Node ontologyNode = ontologyNodesList.item(i);
            String nodeName = ontologyNode.getNodeName();
            System.out.println("NODE NAME: " + nodeName);
            if (ontologyNode instanceof Element) {
                Element ontologyElement = (Element) ontologyNode;
                Node rootDescendantNode = ontologyElement.getElementsByTagName("root").item(0);
                System.out.println("CURRENT NODE: " + rootDescendantNode);
                //calls this method for all the children which is Element

                
            }
            System.out.println("I: " + i);
        }
         */
        return rootOntologyNode;
    }

    private static void getDescendants(Node node, Node parent) {
        if (node != null) {
            if (node.hasChildNodes()) {
                System.out.println(node.getNodeName());
                NodeList childrens = node.getChildNodes();
                for (int i = 0; i < childrens.getLength(); i++) {
                    getDescendants(childrens.item(i), node);
                }//for
            }//fi:root_childrens
            else {
                String nodeValue = node.getTextContent().trim();
                if (nodeValue.length() > 0) {
                    if (nodeValue.equals("term")) {
                        System.out.println(parent.getNodeName() + "::" + nodeValue);
                    }
                }
            }
        }
    }

    private String getInChIKey(Element doc, String hmdbId) {
        String inChIKey;
        inChIKey = readerXML.getTextValue(doc, "inchikey");
        if (inChIKey.equalsIgnoreCase("null")) {
            inChIKey = "NULL";
        } else {
            String inChIKeyStarting = "InChIKey=";
            if (inChIKey.equals("")) {
                System.out.println("\nInChIKey of " + hmdbId + " not available because it is not possible yet");
            } else if (inChIKey.startsWith(inChIKeyStarting)) {
                inChIKey = PatternFinder.searchWithReplacement(inChIKey, inChIKeyStarting + "(.*?)-(.*?)-[A-Z]",
                        inChIKeyStarting + "|\n");
                inChIKey = inChIKey.substring(0, inChIKey.length() - 1);
            }

        }
        return inChIKey;
    }

    public void insertRelationsFromUniChem(CMMDatabase db) {
        // Specify the name of the files

        // File Relations HMDB PubChemical
        String fileNameHMDBPC = "./resources/unichem/src18src22.txt";
        relationHMDBPC(db, fileNameHMDBPC);
    }

    // TODO CHECK THE NEW FILE FROM HMDB
    private void relationHMDBPC(CMMDatabase db, String fileName) {
        // Open the file
        FileInputStream fstream = null;
        try {
            fstream = new FileInputStream(fileName);

        } catch (FileNotFoundException ex) {
            Logger.getLogger(HMDBDatabasePopulator.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

        String strLine;

        try {
            //Read File Line By Line
            // Skip the first Line
            br.readLine();
            while ((strLine = br.readLine()) != null) {
                String hmdbId = PatternFinder.searchFirstOcurrence(strLine, "(HMDB)[0-9]+");
                String PCId = PatternFinder.searchSecondOcurrence(strLine, "[0-9]+");

                // System.out.println( PCId);
                hmdbId = generateHMDBIDCorrectLength(hmdbId);
                int compoundId = db.getCompoundIdFromHMDBID(hmdbId);
                // insert the compound from pubChem
                db.insertCompoundPC(compoundId, Integer.parseInt(PCId));

            }
        } catch (IOException ex) {
            Logger.getLogger(HMDBDatabasePopulator.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        try {
            //Close the input stream
            br.close();

        } catch (IOException ex) {
            Logger.getLogger(HMDBDatabasePopulator.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String generateHMDBIDCorrectLength(String hmdbId) {
        String newHMDBID = PatternFinder.searchFirstOcurrence(hmdbId, "(HMDB)[0-9]+");
        switch (newHMDBID.length()) {
            case 8:
                newHMDBID = newHMDBID.substring(0, 4) + "000" + hmdbId.substring(4);
                return newHMDBID;
            case 9:
                newHMDBID = newHMDBID.substring(0, 4) + "00" + hmdbId.substring(4);
                return newHMDBID;
            case 10:
                newHMDBID = newHMDBID.substring(0, 4) + "0" + hmdbId.substring(4);
                return newHMDBID;
            default:
                return newHMDBID;
        }
    }

    public void splitHMDBFileintometabolites() {
        //File dir = new File(HMDB_RESOURCES_PATH + HMDBFileName);
        MyFile.splitContentOfBigXMLHMDBFileByNode(HMDB_RESOURCES_PATH, HMDB_FILENAME, HMDB_METABOLITE_NODE_NAME);
        //Delete the source file
        File SourceToDelete = new File(HMDB_RESOURCES_PATH + HMDB_FILENAME);
        if (SourceToDelete.exists()) {
            //SourceToDelete.delete();
        }
    }

}
