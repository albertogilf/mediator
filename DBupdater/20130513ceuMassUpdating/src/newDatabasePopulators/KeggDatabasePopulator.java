package newDatabasePopulators;

import databases.CMMDatabase;
import ioDevices.MyFile;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import patternFinders.PatternFinder;
import checkers.cas.CheckerCas;
import downloadersOfWebResources.DownloaderOfWebResources;
import exceptions.CompoundNotClassifiedException;
import exceptions.NodeNotFoundException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utilities.Checker;
import utilities.Constants;
import static utilities.Constants.CLASSYFIRE_LIPID_NODEID;
import static utilities.Constants.KEGG_MOLFILES_PATH;
import static utilities.Constants.KEGG_RESOURCES_PATH;
import static utilities.Constants.UNICHEM_RESOURCES_PATH;
import utilities.Utilities;

/**
 *
 * @author: San Pablo-CEU, Alberto Gil de la Fuente
 * @version: 3.1, 17/02/2016
 */
public class KeggDatabasePopulator {

    PatternFinder pf;

    /**
     * Constructor
     */
    public KeggDatabasePopulator() {
        this.pf = new PatternFinder();
    }

    /**
     * Insert the pathways in the db
     *
     */
    void insertPathways(int compound_id, String content, CMMDatabase db) {

        // Se obtiene la porción de fichero que contiene los pathways
        // Check database model
        // Look one string called in one of the next words between |
        // MODULE added by Alberto
        String pathwayEnding = "FORMULA|EXACT|EXACT|MOL|REMARK|REACTION|SEQUENCE|BRITE|"
                + "REFERENCE|ENZYME|DBLINKS|ATOM|BOND|COMMENT|MODULE|///";
        // Look the patter Pathway until one of the previous words appear.
        // Delete spaces (trim) of pathways
        String pathwaysString = PatternFinder.searchWithReplacement(content, "PATHWAY(.*?)(" + pathwayEnding + ")",
                "PATHWAY|" + pathwayEnding).trim();

        // Si no hay pathways, la ejecución del método termina aquí
        if (pathwaysString.compareToIgnoreCase("") == 0) {
            return;
        }

        // Se lee cada pareja código-nombre
        try {
            BufferedReader bsr = new BufferedReader(new StringReader(pathwaysString));

            String linea;
            while ((linea = bsr.readLine()) != null) {
                String pathway_map = linea.trim().split(" ")[0].trim();
                String pathway_name = linea.trim().substring(pathway_map.length()).trim();
                int pathway_id = db.getPathwayId(pathway_map);

                // If the pathway is not in the database, insert it
                if (pathway_id == 0) {
                    db.insertPathway(pathway_map, pathway_name);
                }
                // Insert the relation with the compound
                int IsThereRelation = db.getCompoundIDFromCompoundsPathways(compound_id, pathway_id);
                if (IsThereRelation == 0) {
                    db.insertCompoundPathway(compound_id, pathway_id);
                }
            }
            bsr.close();
        } catch (IOException ex) {
            System.err.println("Problema al leer desde la cadena.");
            System.err.println("MENSAJE DE LA EXCEPCIÓN: " + ex.toString());
        }

    }

    /**
     * Populate the database db with compound fileName from Kegg
     *
     * @param fileName
     * @param db
     */
    public void populateFromADownloadedFile(String fileName, CMMDatabase db) {
        String keggId = fileName.replaceAll(".txt", "");

        int compoundId = db.getCompoundIdFromKeggId(keggId);
        if (compoundId == 0) {
            insertFromADownloadedFile(keggId, fileName, db);
        } else {
            updateFromADownloadedFile(compoundId, keggId, fileName, db);
        }
    }

    /**
     * Populate the database db with compound fileName from Kegg
     *
     * @param keggId
     * @param fileName
     * @param db
     */
    public void insertFromADownloadedFile(String keggId, String fileName, CMMDatabase db) {
        // First of all, check if the compound structure is already in the database:

        //System.out.println("\n kegg: " + keggId + "\ninCHI: " + inChI 
        //        + "\n inchiKey: " + inChIKey + "\n formula: " + formulaFromInChI);
        // POPULATING DATABASE
        // System.out.println("\n Working on new file: " + keggId);
        String content = MyFile.obtainContentOfABigFile(KEGG_RESOURCES_PATH + fileName).toString();

        int compoundType = 0; // metabolites
        int compoundStatus = 0; // EXpected

        String compound_name = getName(content);

        String formula = getFormula(content);
        // Prepare formula for the database
        String formulaType = PatternFinder.getTypeFromFormula(formula);
        boolean correctFormula;
        correctFormula = !formulaType.equals("");

        String inChI = "";
        String inChIKey = "";
        String SMILES = "";
        String formulaFromInChI = "";
        if (correctFormula) {
            inChI = getInChIdentifier(keggId);
            inChIKey = getInChIKeydentifier(keggId);
            SMILES = getSMILESIdentifier(keggId);
            formulaFromInChI = Checker.getFormulaFromInChI(inChI);
        } else {
            String logFileFormulaBadElements = "./log/kegg_formula_bad_elements.txt";
            String duplicateInformation = "\nCompound " + keggId + " has a formula "
                    + formula + " with no chemical elements";
            MyFile.write(duplicateInformation, logFileFormulaBadElements);
        }

        // Check if the cas has coherence with cas compounds from the checker
        // Insert the compound in compound_cas
        // insertCasId. If there is no compound, try to download. If there is one, leave it as before
        // It is passed casId and not cas_id because it is handled without \" \" at the beginning and the end
        String casId = "";
        if (correctFormula) {
            casId = getCas(content, inChIKey, db);
        }

        String mass = getMass(content);
        double massFromKegg = 0.0d;
        if (!mass.equals("NULL")) {
            massFromKegg = Double.parseDouble(mass);
        }
        String logP = null;
        String mol_weight = getMolWeight(content);

        int[] charges = PatternFinder.getChargeFromSmiles(SMILES);
        int chargeType = charges[0];
        int numCharges = charges[1];
        //LM_ID is not necessary nowadays. The unification of compounds is done by InChIKey
/*
        String lm_id = PatternFinder.searchWithReplacement(content, "LIPIDMAPS: [0-9]+-[0-9]+-[0-9]+",
                "LIPIDMAPS: |\n");
        if (lm_id.equalsIgnoreCase("")) {
            lm_id = "NULL";
        } else {
            lm_id = "\"" + lm_id.substring(0, lm_id.length() - 1) + "\"";
        }
         */
        String query;
        String insertion;
        int compound_id;
        boolean foundCas;
        foundCas = false;
        // Compounds which have InChI
        //if (correctFormula && !inChIKey.equals("")) {
        if (!inChIKey.equals("")) {
            // Check if there is any compound with the same structure
            compound_id = db.getCompoundIdFromInchiKey(inChIKey);
            // Case 1 There is a compound with same structure
            if (compound_id != 0) {
// Case 1.0 the compound has another cas previously
                if (!casId.equals("")) {
                    String casFromDB = db.getCasFromCompound(compound_id);
                    if (!casFromDB.equals("")) {
                        if (!casId.equals(casFromDB)) {
                            String logFileDuplicates = "./log/kegg_structure_duplicate_different_cas.txt";
                            String duplicateInformation = "\nCompound " + keggId + " has other compound "
                                    + compound_id + " with the same structure but different CAS";
                            MyFile.write(duplicateInformation, logFileDuplicates);
                            // Check information from ChemIdPlus to check what cas is right
                            String inchiFromChemId = CheckerCas.getInChIKeyFromDB(casId, db);
                            if (inchiFromChemId.equals(inChIKey)) {
                                // Cas Id information is from this compound. 
                                // Update CAS Identifier
                                db.updateCompoundCAS(compound_id, casId);
                            }
                        }
                    }
                }

// Case 1.1 The compound has previously different formula and/or mass
                String formulaFromCompound = db.getformulaFromCompound(compound_id);
                double massFromCompound = db.getMassFromCompound(compound_id);

                if (formula.equals(formulaFromCompound) && (massFromCompound - massFromKegg) < 2) {
                    // The compound is the same, mass is closer between existent compound and kegg compound 
                    // and formula is the same
                    // Update information from Kegg if they have more precission that previous information
                    int precissionFromDB = Utilities.getPrecission(massFromCompound);
                    int precissionFromKegg = Utilities.getPrecission(massFromKegg);
                    if (precissionFromDB < precissionFromKegg) {
                        // Commented. KEGG is less trustable than HMDB OR LipidMaps
                        //query = "update compounds set mass = " + mass + " WHERE compound_id = " + compound_id;
                        //db.executeNewIDU(query);
                    }
                } else if (formula.equals(formulaFromCompound) && (massFromCompound - massFromKegg) > 2) {
                    // Case 1.2 There is a compound with different mass

                    String logFileDuplicates = "./log/kegg_structure_duplicate_bad_mass.txt";
                    String duplicateInformation = "\nCompound " + keggId + " has other compound "
                            + compound_id + " with same structure and different mass";
                    MyFile.write(duplicateInformation, logFileDuplicates);
                } else if (!formula.equals(formulaFromCompound) && (massFromCompound - massFromKegg) < 2) {
                    // Case 1.3 There is a compound with different formula

                    int precissionFromDB = Utilities.getPrecission(massFromCompound);
                    int precissionFromKegg = Utilities.getPrecission(massFromKegg);
                    if (precissionFromDB < precissionFromKegg) {
                        // Commented. KEGG is less trustable than HMDB OR LipidMaps
                        //query = "update compounds set mass = " + mass + " WHERE compound_id = " + compound_id;
                        //db.executeNewIDU(query);
                    }
                    String logFileDuplicates = "./log/kegg_structure_duplicate_bad_formula.txt";
                    String duplicateInformation = "\nCompound " + keggId + " has other compound "
                            + compound_id + " with same structure and different formula";
                    MyFile.write(duplicateInformation, logFileDuplicates);
                } else {
                    String logFileDuplicates = "./log/kegg_structure_duplicate.txt";
                    String duplicateInformation = "\nCompound " + keggId + " has other compound "
                            + compound_id + " with the same structure, different formula and different mass";
                    MyFile.write(duplicateInformation, logFileDuplicates);
                }

                // Compound is the same if they have the same structure
                db.insertCompoundKegg(compound_id, keggId);
                // If the compound is already in the database, just do the link
                // to the kegg, link the pathways compound and exit the method
                insertPathways(compound_id, content, db);
                List<String> reactions = getListReactions(content);
                db.insertReactions(compound_id, reactions);

                // Check if Cas Id is already in the database
                return;
            } else if (!casId.equals("")) {
                // Check Cas ID
                // Compound has InChI and Cas Identifier
                // Check information from ChemIdPlus to check if cas is right
                String inchiFromChemId = CheckerCas.getInChIKeyFromDB(casId, db);
                if (!inchiFromChemId.equals(inChIKey)) {
                    // If the first layer of the InChI is the same, then we keep the cas Id.
                    // If not, we update the cas_id to null
                    String firstLayerInChIFromKegg = inChIKey.split("-")[0];
                    String firstLayerInChIFromChemId = inchiFromChemId.split("-")[0];
                    //System.out.println("First Layer Kegg: " + firstLayerInChIFromKegg + " From ChemID: " + firstLayerInChIFromChemId);
                    if (!firstLayerInChIFromKegg.equals(firstLayerInChIFromChemId)) // If the cas identifier from ChemId corresponds to another compound (InChIKey),
                    // then update cas_id to null
                    {
                        casId = "NULL";
                    }
                }
            }
        }

        // Check if the compound CAS exists before
        if (!casId.equalsIgnoreCase("NULL") && !casId.equals("")) {
            // Check if there is a compound with the same cas identifier
            compound_id = db.getCompounIdFromCAS(casId);

            // Writing compounds of Kegg with bad information
            String formulaCas = CheckerCas.getFormulaFromDB(casId, db);
            double weightCas = CheckerCas.getMassFromDB(casId, db);

            if (correctFormula && !formula.equalsIgnoreCase("NULL")) {
                if (!formulaCas.equals("")) {
                    if (!formulaCas.equals(formula)) {
                        String logFileDuplicates = "./log/kegg_chemid_formula_incoherence.txt";
                        String duplicateInformation = "Inserting compound " + keggId
                                + "\tcas: " + casId + "\tname: " + compound_name
                                + "\tformula: " + formula + "\tmass: " + mass
                                + "\n COMPOUND FROM CHEMID PLUS: \t FORMULA: " + formulaCas
                                + "\t MASS: " + weightCas
                                + "\n";
                        MyFile.write(duplicateInformation, logFileDuplicates);
                        // We assume the real formula from the compound
                        // formula = "\"" + formulaCas + "\"";
                    }
                }
            }
            if (!mass.equals("NULL")) {
                int isWeightCas = Double.compare(0.0d, weightCas);
                if (isWeightCas != 0 && massFromKegg - weightCas > 2) {
                    String logFileDuplicates = "./log/kegg_mass_incoherence.txt";
                    String duplicateInformation = "Inserting compound " + keggId
                            + "\tcas: " + casId + "\tname: " + compound_name
                            + "\tformula: " + formula + "\tmol weight: " + mass
                            + "\tmass: " + mass
                            + "\n COMPOUND FROM CHEMID PLUS: \t FORMULA" + formulaCas
                            + "\n MASS: " + weightCas
                            + "\n";
                    MyFile.write(duplicateInformation, logFileDuplicates);
                }
            }
            if (compound_id != 0) {
                query = "SELECT * FROM compounds where cas_id=" + casId;
                String compoundDB = db.getInformationCompound(query);
                String logFileDuplicates = "./log/kegg_cas_duplicates.txt";
                String duplicateInformation = "Inserting compound " + keggId
                        + "\ncas: " + casId + "\nname: " + compound_name
                        + "\nformula: " + formula + "\nmass: " + mass
                        + "\n Compound already in the database: \n"
                        + compoundDB + "\n";
                MyFile.write(duplicateInformation, logFileDuplicates);
                foundCas = true;
            }
        }

        // Check what compounds has different Formula that InChI
        if (correctFormula && !formula.equals("NULL")) {
            if (!formulaFromInChI.equals("")) {
                if (!formulaFromInChI.equals(formula)) {
                    //System.out.println("\n KEGG COMPOUND INCOHERENT FORMULA WITH INCHI..  "
                    //        + " INCHI: " + formulaFromInChI + "KEGG: " + formulaRetrieved);
                    String logFileDuplicates = "./log/kegg_inchi_formula_incoherence.txt";
                    String duplicateInformation = "Inserting compound " + keggId
                            + "\tcas: " + casId + "\tformula: " + formula
                            + "\n FORMULA FROM INCHI: \t FORMULA: " + formulaFromInChI
                            + "\n";
                    MyFile.write(duplicateInformation, logFileDuplicates);
                    //Then, the real formula is the formula from InChI identifier
                    //formula = "\"" + formulaFromInChI + "\"";
                }
            }
        }

        if (foundCas) {
            casId = "NULL";
        }
        compound_id = db.insertCompound(casId, compound_name, formula, mass,
                chargeType, numCharges, formulaType, compoundType, compoundStatus, logP);
        // System.out.println("\nID retrieved: " + compound_id);
        if (correctFormula && !inChIKey.equals("")) {
            // TODO OBTAIN SMILES GENERATED BY PYTHON
            db.insertIdentifiers(compound_id, inChI, inChIKey, SMILES);
        }
        db.insertCompoundKegg(compound_id, keggId);

        if (compound_id > 0 && !inChIKey.equals("")) {
            // Insert classyfire class
            List<String> ancestorNodes = new LinkedList();
            try {
                ancestorNodes = downloadersOfWebResources.DownloaderOfWebResources.getAncestorNodesCompoundClassyfireClassification(inChI, inChIKey);
                if (ancestorNodes.contains(CLASSYFIRE_LIPID_NODEID)) {
                    compoundType = 1;
                    db.updateCompoundType(compound_id, compoundType);
                }
            } catch (CompoundNotClassifiedException ex) {
            }
            try {
                db.insertFullCompoundClassyfireClassification(compound_id, ancestorNodes);
            } catch (NodeNotFoundException ex) {
                Logger.getLogger(HMDBDatabasePopulator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        /*
        String pubChem = PatternFinder.searchWithReplacement(content, "PubChem: [0-9]+",
                "PubChem: ");
        if (pubChem.equalsIgnoreCase("")) {
        } else {
            // Insert the link to pubChemical
            insertion = "INSERT IGNORE INTO compounds_pc(compound_id, pc_id) VALUES("
                    + compound_id + "," + pubChem + ")";
//            System.out.println("\ninserting PubChemical COMPOUND: " + insertion);
            db.executeIDU(insertion);
        }
         */
        insertPathways(compound_id, content, db);
        List<String> reactions = getListReactions(content);

        db.insertReactions(compound_id, reactions);
        // Insert InChI and InChIKey from uniChem
        //insertInChIdentifiersFromKegg(compound_id, keggId, db);
    }

    /**
     * update the compound compoundId with the ifnormation from fileName in
     * database db
     *
     * @param compoundId
     * @param keggId
     * @param fileName
     * @param db
     */
    public void updateFromADownloadedFile(int compoundId, String keggId, String fileName, CMMDatabase db) {
// Only update Cas and Mass if the precission is greater than before

        String content = MyFile.obtainContentOfABigFile(KEGG_RESOURCES_PATH + fileName).toString();

        String formula = getFormula(content);
        // Prepare formula for the database
        
        String formulaType = PatternFinder.getTypeFromFormula(formula);
        boolean correctFormula;
        correctFormula = !formulaType.equals("");

        String inChI = "";
        String inChIKey = "";
        String SMILES = "";
        String formulaFromInChI = "";
        if (correctFormula) {
// Only update information if the compound is not a skeleton.
            inChI = getInChIdentifier(keggId);
            inChIKey = getInChIKeydentifier(keggId);
            SMILES = getSMILESIdentifier(keggId);
            formulaFromInChI = Checker.getFormulaFromInChI(inChI);

            String casId = getCas(content, inChIKey, db);
            if (casId.equalsIgnoreCase("")) {
                casId = "NULL";
            }
            //System.out.println("\nCAS ID: " + cas_id);

            String mass = getMass(content);
            double massFromKegg = 0.0d;
            if (!mass.equals("NULL")) {
                try{
                massFromKegg = Double.parseDouble(mass);
                }
                catch(NumberFormatException nfe)
                {
                    System.out.println("CHECK the file: " + keggId);
                }
            }

            String InChIKeyFromDatabase = db.getInChIKeyFromCompound(compoundId);
            if (InChIKeyFromDatabase.equals("") || InChIKeyFromDatabase.equals("null")) {
                db.insertIdentifiers(compoundId, inChI, inChIKey, SMILES);
            }

// Do not insert Cas Id because it could have some. Done previously in getCas method
// Case 0. Update Mass
            double massFromCompound = db.getMassFromCompound(compoundId);
            if ((massFromCompound - massFromKegg) < 1) {
                // The compound is the same, mass is closer between existent compound and kegg compound 
                // and formula is the same
                // Update information from Kegg if they have more precission that previous information
                if (massFromKegg != 0.0d) {
                    int precissionFromDB = Utilities.getPrecission(massFromCompound);
                    int precissionFromKegg = Utilities.getPrecission(massFromKegg);
                    if (precissionFromDB < precissionFromKegg) {
                        // Commented. KEGG is less trustable than HMDB OR LipidMaps
                        //insertion = "update compounds set mass = " + mass + " WHERE compound_id = " + compoundId;
                        //db.executeNewIDU(insertion);
                    }
                }
            }
// Case 1. Update Cas
            if (!casId.equals("")) {
                String casFromDB = db.getCasFromCompound(compoundId);
                if (!casFromDB.equals("")) {
                    if (!casId.equals(casFromDB)) {
                        // Check information from ChemIdPlus to check what cas is right
                        String inchiFromChemId = CheckerCas.getInChIKeyFromDB(casId, db);
                        if (inchiFromChemId.equals(inChIKey)) {
                            // Cas Id information is from this compound. 
                            // Update CAS Identifier
                            db.updateCompoundCAS(compoundId, casId);
                        }
                    }
                }
            }
            
            // Update classyfire classification
            List<String> ancestorNodes = new LinkedList();
            try {
                ancestorNodes = downloadersOfWebResources.DownloaderOfWebResources.getAncestorNodesCompoundClassyfireClassification(inChI, inChIKey);
                if (ancestorNodes.contains(CLASSYFIRE_LIPID_NODEID)) {
                    int compoundType = 1;
                    db.updateCompoundStatus(compoundId, compoundType);
                }
            } catch (CompoundNotClassifiedException ex) {
            }
            try {
                db.insertFullCompoundClassyfireClassification(compoundId, ancestorNodes);
            } catch (NodeNotFoundException ex) {
                Logger.getLogger(HMDBDatabasePopulator.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        insertPathways(compoundId, content, db);
        List<String> reactions = getListReactions(content);
        db.insertReactions(compoundId, reactions);
    }

    /**
     * Populate all the kegg CMMDatabase
     *
     * @param db
     */
    public void populateFromKegg(CMMDatabase db) {

        // Testing the new population of database
        /*
        populateFromADownloadedFile("C00157.txt", db);
        populateFromADownloadedFile("C00109.txt", db);
        populateFromADownloadedFile("C00002.txt", db);
        populateFromADownloadedFile("C00003.txt", db);
        populateFromADownloadedFile("C00004.txt", db);
        populateFromADownloadedFile("C00005.txt", db);
        populateFromADownloadedFile("C00006.txt", db);
        populateFromADownloadedFile("C00007.txt", db);
        populateFromADownloadedFile("C00008.txt", db);
        populateFromADownloadedFile("C00009.txt", db);
        populateFromADownloadedFile("C00010.txt", db);
         */
        // Changed to linux distribution
        //File dir = new File(".\\resources\\kegg");
        // Delete the files of duplicate
        File logDuplicates = new File("./log/kegg_formula_bad_elements.txt");
        if (logDuplicates.exists()) {
            logDuplicates.delete();
        }
        logDuplicates = new File("./log/kegg_cas_duplicates.txt");
        if (logDuplicates.exists()) {
            logDuplicates.delete();
        }
        logDuplicates = new File("./log/kegg_structure_duplicate_bad_formula.txt");
        if (logDuplicates.exists()) {
            logDuplicates.delete();
        }
        logDuplicates = new File("./log/kegg_structure_duplicate_bad_mass.txt");
        if (logDuplicates.exists()) {
            logDuplicates.delete();
        }
        logDuplicates = new File("./log/kegg_structure_duplicate");
        if (logDuplicates.exists()) {
            logDuplicates.delete();
        }
        logDuplicates = new File("./log/kegg_structure_duplicate_different_cas");
        if (logDuplicates.exists()) {
            logDuplicates.delete();
        }

        File logFormulaKeggInChIIncoherence = new File("./log/kegg_inchi_formula_incoherence.txt");
        if (logFormulaKeggInChIIncoherence.exists()) {
            logFormulaKeggInChIIncoherence.delete();
        }
        File logFormulaKeggChemIdIncoherences = new File("./log/kegg_chemid_formula_incoherence.txt");
        if (logFormulaKeggChemIdIncoherences.exists()) {
            logFormulaKeggChemIdIncoherences.delete();
        }
        File logMassIncoherences = new File("./log/kegg_mass_incoherence.txt");
        if (logMassIncoherences.exists()) {
            logMassIncoherences.delete();
        }
        File dir = new File(KEGG_RESOURCES_PATH);
        String[] ficheros = dir.list();
        // iterate all files in RESOURCES_PATH + /resources/kegg
        for (String fichero : ficheros) {
            if (!fichero.equals("molfiles")) {
                populateFromADownloadedFile(fichero, db);
            }
        }
        //To be able to delete with no restriction
        //db.executeIDU("set SQL_SAFE_UPDATES=0;");
        // db.executeIDU("delete from compounds where formula ='' and mass = 0;");
        // insertFromUniChem(db);
        // insertRelationsFromUniChem(db);

    }

    private String getName(String content) {
        // POSSIBLE ATRIBUTES OF KEGG COMPOUNDS
        String nameEnding = "FORMULA|EXACT|MOL|REMARK|REACTION|PATHWAY|SEQUENCE|"
                + "REFERENCE|BRITE|ENZYME|DBLINKS|ATOM|BOND|COMMENT|MODULE|///";
        String namesString = PatternFinder.searchWithReplacement(content, "NAME(.*?)(" + nameEnding + ")",
                "NAME|" + nameEnding + "|\n");
        //System.out.println("\n NAME STRING: " + namesString);
        // Replace in order to insert in database
        //namesString = namesString.replaceAll("\"", "''");
        String compound_name = "";
        if (namesString.equalsIgnoreCase("")) {
            // If the compound has no name, exit the method.
            //return;
            compound_name = "NULL";
        } else {
            // put all names in a list
            List<String> names = PatternFinder.searchListWithReplacement(namesString, "[^;]+", "\n");
            // write the names with \n1
            for (int i = 0; i < names.size(); i++) {
                // In order to separate Names with a ;
                //compound_name = compound_name + names.get(i).trim() + "\n";
                compound_name = compound_name + names.get(i).trim() + ";\n";
            }
            // Replace in order to insert in database
            compound_name = compound_name.replaceAll("\"", "\\'");
            compound_name = compound_name.replaceAll("'", "\\'");
            // quit the last \n and put \" at the beginning and the end
            //compound_name = "\"" + compound_name.substring(0, compound_name.length() - 1) + "\"";
            // The last one does not need the ;
            compound_name = "" + compound_name.substring(0, compound_name.length() - 2) + "";
        }
        return compound_name;
    }

    private List<String> getListReactions(String content) {
        String reactionEnding = "FORMULA|EXACT|MOL|REMARK|NAME|PATHWAY|SEQUENCE|"
                + "REFERENCE|BRITE|ENZYME|DBLINKS|ATOM|BOND|COMMENT|MODULE|///";
        String reactionString = PatternFinder.searchWithReplacement(content, "REACTION(.*?)(" + reactionEnding + ")",
                "REACTION|" + reactionEnding + "|\n");
        // Replace in order to insert in database
        //namesString = namesString.replaceAll("\"", "''");
        List<String> reactions;
        if (reactionString.equalsIgnoreCase("")) {
            // If the compound has no name, exit the method.
            //return;
            reactions = new ArrayList<>();
        } else {
            // put all names in a list
            reactionString = reactionString.trim();
            reactions = PatternFinder.searchListWithReplacement(reactionString, "[^ ]+", "\n");
            // System.out.println("\n ReactionsString: " +reactionString  + "Reactions: " + reactions);
        }
        return reactions;
    }

    private String getFormula(String content) {
        // Search for a string started by FORMULA.* and after that one of the next words
        // When one word is found, this is the end of the field
        // word MODULE added by ALBERTO
        String formulaEnding = "EXACT|MOL|REMARK|REACTION|PATHWAY|SEQUENCE|"
                + "REFERENCE|BRITE|ENZYME|DBLINKS|ATOM|BOND|COMMENT|MODULE|///";
        String formulaRetrieved = PatternFinder.searchWithReplacement(content, "FORMULA(.*?)(" + formulaEnding + ")",
                "FORMULA|" + formulaEnding + "|\n");
        if (formulaRetrieved.equalsIgnoreCase("")) {
        } else {
            formulaRetrieved = formulaRetrieved.trim();
            formulaRetrieved = formulaRetrieved.replaceAll("\\s+", "");
        }
        return formulaRetrieved;
    }

    public String testCas(String content, CMMDatabase db) {
        String inChIKey = getInChIKeydentifier("C02284");
        String cas = getCas(content, inChIKey, db);
        return cas;
    }

    private String getCas(String content, String inChIKey, CMMDatabase db) {
        // Look for this pattern CAS: 7732-18-5
        // in order to extract the number of the CAS
        // Take Care about number of spaces between DBLINKS and CAS
        //String cas_id = PatternFinder.searchWithReplacement(content, "DBLINKS     CAS: [0-9]+-[0-9]+-[0-9]+",
        //        "DBLINKS     CAS: |\n");
        // If there is one Cas -> 
        String casId;
        // System.out.println("\n CONTENT: " + content);
        String casStart = "DBLINKS(.*)CAS: ";
        String casEnd = "\n";
        String casPattern = "[0-9]+-[0-9]+-[0-9]+";
        String someCas = PatternFinder.searchWithReplacement(content, casStart + "(.*?)" + casEnd, casStart + "|" + casEnd);
        //System.out.println("\n cas: " + someCas);
        if (someCas.equals("")) {
            // System.out.println("\n There is no cas in the compound");
            casId = "";
        } else {
            List<String> cas = PatternFinder.searchListWithoutReplacement(someCas, casPattern);
            // If there is only one CAS
            if (cas.size() == 1) {
                casId = cas.get(0);
                CheckerCas.insertCasId(db, casId);
                // System.out.println("\n There is one cas in the compound " + casId);
            } else if (inChIKey.equals("")) {
                // If there is no information from the InChI identifier
                casId = cas.get(cas.size() - 1);
                CheckerCas.insertCasId(db, casId);
                //System.out.println("\n There is some cas but the compound has"
                //        + " no InChI to check " + casId);
            } else {
                // If there is an InChIKey, look what Cas fits with the InChIKey
                //System.out.println("\n MULTIPLE CAS:");
                for (String provCas : cas) {
                    CheckerCas.insertCasId(db, provCas);
                }
                casId = db.checkCasChemId(cas, inChIKey);
                // System.out.println("\n FINAL CAS: " + casId);
            }
        }

        return casId;
    }

    private String getMass(String content) {
        String mass = PatternFinder.searchWithReplacement(content, "EXACT_MASS  [0-9]+\\.[0-9]+",
                "EXACT_MASS  ");
        if (mass.equalsIgnoreCase("")) {
            mass = "NULL";
        } else {
            mass = mass.substring(0, mass.length() - 1);
        }
        return mass;
    }

    private String getMolWeight(String content) {
        String mol_weight = PatternFinder.searchWithReplacement(content, "MOL_WEIGHT  [0-9]+\\.[0-9]+",
                "MOL_WEIGHT  ");
        if (mol_weight.equalsIgnoreCase("")) {
            mol_weight = "NULL";
        } else {
            mol_weight = mol_weight.substring(0, mol_weight.length() - 1);
        }
        return mol_weight;
    }

    private String getInChIdentifier(String keggId) {
        String inChI = "";
        String molFileName = KEGG_MOLFILES_PATH + keggId + ".mol.txt";
        File molFile = new File(molFileName);

        if (molFile.exists()) {
            String content = MyFile.obtainContentOfABigFile(molFileName).toString();

            String inChIStarting = "InChI=";
            String inChIEnding = "\n";
            inChI = PatternFinder.searchWithReplacement(content, inChIStarting + "(.*?)(" + inChIEnding + ")",
                    inChIEnding);
            if (inChI.equals("")) {
                System.out.println("\nInChI of " + keggId + " not available because it is not possible yet");
            } else {
                inChI = inChI.substring(0, inChI.length() - 1);
            }
        } else {
            System.out.println("\nCompound " + keggId + " has no InChI available because it is not possible yet");
        }
        return inChI;
    }

    private String getInChIKeydentifier(String keggId) {
        String inChIKey = "";
        String molFileName = KEGG_MOLFILES_PATH + keggId + ".mol.txt";
        File molFile = new File(molFileName);

        if (molFile.exists()) {
            String content = MyFile.obtainContentOfABigFile(molFileName).toString();
            String inChIKeyStarting = "InChIKey=";
            inChIKey = PatternFinder.searchWithReplacement(content, inChIKeyStarting + "(.*?)-(.*?)-[A-Z]",
                    inChIKeyStarting);
            if (inChIKey.equals("")) {
                System.out.println("\nInChIKey of " + keggId + " not available because it is not possible yet");
            } else {
                inChIKey = inChIKey.substring(0, inChIKey.length() - 1);
            }
        } else {
            System.out.println("\nCompound " + keggId + " has no InChIKey available because it is not possible yet");
        }
        return inChIKey;
    }

    private String getSMILESIdentifier(String keggId) {
        String SMILES = "";
        String smilesFileName = KEGG_MOLFILES_PATH + keggId + ".smiles";
        File smilesFile = new File(smilesFileName);

        if (smilesFile.exists()) {
            SMILES = MyFile.obtainContentOfABigFile(smilesFileName).toString();

            if (SMILES.equals("")) {
                System.out.println("Smiles of " + keggId + " not available because it is not possible yet");
            } else {
                SMILES = SMILES.substring(0, SMILES.length() - 1);
            }
        } else {
            System.out.println("\nCompound " + keggId + " has no SMILES available because it is not possible yet");
        }
        return SMILES;
    }

    private void insertInChIdentifiersFromKegg(int compoundId, String keggId, CMMDatabase db) {
        String molFileName = KEGG_MOLFILES_PATH + keggId + ".mol.txt";
        File molFile = new File(molFileName);

        if (molFile.exists()) {
            String content = MyFile.obtainContentOfABigFile(molFileName).toString();

            String inChIStarting = "InChI=";
            String inChIKeyStarting = "InChIKey=";
            String inChIEnding = "\n";
            String inChI;
            inChI = PatternFinder.searchWithReplacement(content, inChIStarting + "(.*?)(" + inChIEnding + ")",
                    inChIEnding);
            String inChIKey = PatternFinder.searchWithReplacement(content, inChIKeyStarting + "(.*?)-(.*?)-[A-Z]",
                    inChIKeyStarting);
            String SMILES = getSMILESIdentifier(keggId);
            // System.out.println("\n Inchi: " + inChI + "\nInchiKey: " + inChIKey);
            db.insertIdentifiers(compoundId, inChI, inChIKey, SMILES);

            //System.out.println(insertion);
        } else {
            System.out.println("\nCompound KeggId not available because it is not possible yet");
        }
    }

    public void insertRelationsFromUniChem(CMMDatabase db) {
        // Specify the name of the files
        // File Relations Kegg HMDB
        String fileNameKeggHMDB = UNICHEM_RESOURCES_PATH + "src6src18.txt";
        // Not used because it has errors.
        // It is based in the inchiKey only, and the inchi Key does not distinguish
        // between compounds with molecules added to the compound
        //relationKeggHMDB(db, fileNameKeggHMDB);

        // File Relations Kegg PubChemical
        String fileNameKeggPC = UNICHEM_RESOURCES_PATH + "src6src22.txt";
        relationKeggPC(db, fileNameKeggPC);

    }

    private void relationKeggHMDB(CMMDatabase db, String fileName) {
        // Open the file
        FileInputStream fstream = null;
        try {
            fstream = new FileInputStream(fileName);

        } catch (FileNotFoundException ex) {
            Logger.getLogger(KeggDatabasePopulator.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

        String strLine;

        try {
            //Read File Line By Line
            // Skip the first Line
            br.readLine();
            while ((strLine = br.readLine()) != null) {
                String keggId = PatternFinder.searchFirstOcurrence(strLine, "[C][0-9]+");
                String hmdbId = PatternFinder.searchFirstOcurrence(strLine, "(HMDB)[0-9]+");
                // For pubChemical compounds
                // String PCId = PatternFinder.searchWithoutReplacement(strLine, "[0-9]+");
                String hmdb_id = "\"" + hmdbId + "\"";

                String query = "SELECT compound_id FROM compounds_kegg where kegg_id=\""
                        + keggId + "\"";
                int compoundId = db.getInt(query);
                System.out.println("\ncompound " + compoundId + "KEGG " + keggId + " TO HMDB: " + hmdbId);
                // insert the compound from hmdb
                // No check because it was checked by the tool that they have 
                // the same inChI
                String insertion = "INSERT IGNORE INTO compounds_hmdb(compound_id, hmdb_id) VALUES("
                        + compoundId + ", " + hmdb_id + ")";
                db.executeIDU(insertion);

            }
        } catch (IOException ex) {
            Logger.getLogger(KeggDatabasePopulator.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        try {
            //Close the input stream
            br.close();

        } catch (IOException ex) {
            Logger.getLogger(KeggDatabasePopulator.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void relationKeggPC(CMMDatabase db, String fileName) {
        // Open the file
        FileInputStream fstream = null;
        try {
            fstream = new FileInputStream(fileName);

        } catch (FileNotFoundException ex) {
            Logger.getLogger(KeggDatabasePopulator.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

        String strLine;

        try {
            //Read File Line By Line
            // Skip the first Line
            br.readLine();
            while ((strLine = br.readLine()) != null) {
                String keggId = PatternFinder.searchFirstOcurrence(strLine, "[C][0-9]+");
                String PCId = PatternFinder.searchSecondOcurrence(strLine, "[0-9]+");
                //pcId = PCId.
                //System.out.println("\n" + PCId);
                String query = "SELECT compound_id FROM compounds_kegg where kegg_id= \""
                        + keggId + "\"";
                int compoundId = db.getInt(query);
                // insert the compound from pubChem
                String insertion = "INSERT IGNORE INTO compounds_pc(compound_id, pc_id) VALUES("
                        + compoundId + ", " + PCId + ")";
                db.executeNewIDU(insertion);

            }
        } catch (IOException ex) {
            Logger.getLogger(KeggDatabasePopulator.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        try {
            //Close the input stream
            br.close();

        } catch (IOException ex) {
            Logger.getLogger(KeggDatabasePopulator.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

}
