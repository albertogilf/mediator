package newDatabasePopulators;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import databases.CMMDatabase;
import downloadersOfWebResources.DownloaderOfWebResources;
import exceptions.CompoundNotClassifiedException;
import exceptions.NodeNotFoundException;
import ioDevices.MyFile;
import ioDevices.UriDevice;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import patternFinders.PatternFinder;
import utilities.Checker;
import utilities.Constants;
import static utilities.Constants.CLASSYFIRE_LIPID_NODEID;
import static utilities.Constants.LIPIDMAPS_RESOURCES_INCHIS_PATH;
import static utilities.Constants.LIPIDMAPS_RESOURCES_PATH;
import static utilities.Constants.PRECISSION_TO_BE_SAME_COMPOUND_MASS_UNIFICATION;
import utilities.Utilities;

/**
 *
 * @author: San Pablo-CEU, Alberto Gil de la Fuente
 * @version: 4.1, 21/03/2018
 */
public class LMDatabasePopulator {

    PatternFinder pf;

    public LMDatabasePopulator() {
        this.pf = new PatternFinder();
    }

    /**
     *
     * @param fileName
     * @param db
     */
    public void populateFromADownloadedFile(String fileName, CMMDatabase db) {
        StringBuilder content;
        content = MyFile.obtainContentOfABigFile(LIPIDMAPS_RESOURCES_PATH + fileName);
        StringBuilder result = new StringBuilder();
        PatternFinder compoundFinder = new PatternFinder();
        while (compoundFinder.searchInDifferentCalls(content, "<LM_ID>(.*?)LM(.*?)\\n",
                "<LM_ID>|\\n", result)) {
            String lm_id = result.toString();
            //System.out.println("LMID: " + lm_id);
            populateFromAJSONFile(lm_id, db);
        }
    }

    /**
     *
     * @param lm_id
     * @param db
     */
    public void populateFromAJSONFile(String lm_id, CMMDatabase db) {;
        int compoundId = db.getCompoundIdFromLMId(lm_id);
        if (compoundId == 0) {
            // System.out.println("compound from LM: " + lm_id + "not inserted");
            insertFromAJSONFile(lm_id, db);
        } else {
            /*
            query = "SELECT compound_id FROM compounds_classification_hmdb where compound_id=" + compoundId;
            int compoundId2 = db.getInt(query);
            if (compoundId2 == 0) {
                updateFromAJSONFile(compoundId, lm_id, db);
            }
             */
            updateFromAJSONFile(compoundId, lm_id, db);
        }
    }

    private void insertFromAJSONFile(String lm_id, CMMDatabase db) {

        DownloaderOfWebResources.downloadLipidMapsJSONFile(lm_id);
        try {
            FileReader fileJSON = new FileReader(Constants.LIPIDMAPS_RESOURCES_JSON_PATH + lm_id + ".json");
            JsonReader reader = new JsonReader(fileJSON);
            JsonParser parser = new JsonParser();
            try {
                JsonObject lmCompoundJSON = parser.parse(reader).getAsJsonObject();
                //System.out.println("RESPONSE FULL: " + lmCompoundJSON.toString());

                // compoundType 1 for lipids
                int compoundType = 1;
                // 0 for expected
                int compoundStatus = 0;

                String compound_name;
                compound_name = getNameFromJSON(lmCompoundJSON);

                String common_name;
                common_name = getCommonNameFromJSON(lmCompoundJSON);

                String formula;
                formula = getFormulaFromJSON(lmCompoundJSON);

                String formulaType;
                try {
                    formulaType = PatternFinder.getTypeFromFormula(formula);
                } catch (IllegalArgumentException iae) {
                    System.out.println("LM: " + lm_id + " WITH SKELETON FORMULA");
                    return;
                }
                boolean correctFormula;
                correctFormula = !formulaType.equals("");
                String casID = "NULL";

                String inChI = "";
                String inChIKey = "";
                String SMILES = "";
                String formulaFromInChI = "";
                if (correctFormula) {
                    inChI = getInChIFromJSON(lmCompoundJSON);
                    inChIKey = getInChIKeyFromJSON(lmCompoundJSON);
                    SMILES = getSMILESFromJSON(lmCompoundJSON);
                    formulaFromInChI = Checker.getFormulaFromInChI(inChI);
                } else {
                    String logFileFormulaBadElements = "./log/lm_formula_bad_elements.txt";
                    String duplicateInformation = "\nCompound " + lm_id + " has a formula "
                            + formula + " with no chemical elements";
                    MyFile.write(duplicateInformation, logFileFormulaBadElements);
                }
                String mass;
                double massFromLM;
                mass = getMassFromJSON(lmCompoundJSON);
                if (mass.equalsIgnoreCase("") || Double.parseDouble(mass) < 0.01d) {
                    mass = "NULL";
                    massFromLM = 0.0d;
                } else {
                    massFromLM = Double.parseDouble(mass);
                }
                String logP = null;
                int[] charges = PatternFinder.getChargeFromSmiles(SMILES);
                int chargeType = charges[0];
                int numCharges = charges[1];

                String abbrev;
                abbrev = getABBREVFromJSON(lmCompoundJSON);
                String lipidType;
                String abbrev_chains;
                int numChains = 0;
                List<String> chains;
                int numCarbons;
                int numDoubleBonds;
                // If both abbrev and abbrev_chains are not present
                // Take information from the name!
                if (!abbrev.equals("")) {

                    lipidType = getLipidTypeFromAbbrev(abbrev);
                    //System.out.println("ABBREV: " + abbrev);
                    abbrev_chains = getAbbrevChainsFromJSON(lmCompoundJSON);
                    numCarbons = getNumberOfCarbonsFromAbbrev(abbrev);
                    numDoubleBonds = getNumberOfDoubleBondsFromAbbrev(abbrev);
                    //System.out.println("CARBONS: " + numCarbons + "  Double Bonds: " + numDoubleBonds);
                    if (!abbrev_chains.equals("")) {
// Get number of carbons and double bonds from abbrev chains. ABBREV CHAINS SHOULD NOT BE EMPTY

                        chains = PatternFinder.getListOfChains(abbrev_chains);
                        numChains = chains.size();
                        //System.out.println("chains: " + numChains + " \t" + chains);
                    } else {
                        // Try to get chains from NAME, the abbrev usually 
                        // put them together. 
                        chains = PatternFinder.getListOfChains(compound_name);
                        numChains = chains.size();
                        if (numChains == 0) {
                            chains = PatternFinder.getListOfChains(abbrev);
                            numChains = chains.size();
                            if (numChains > 1) {
                                System.out.println(" CHAINS > 1 in LM: " + lm_id + " but not abbrev_chains!");
                            }
                        }
                    }
                } else {
                    abbrev_chains = getAbbrevChainsFromJSON(lmCompoundJSON);
                    if (!abbrev_chains.equals("")) {
                        System.out.println("ABBREV CHAINS WITHOUT ABBREV IN; " + lm_id);
                    }
                    // Obtain information from NAME, sometimes the attributes from lipidmaps are wrong
                    lipidType = PatternFinder.getLipidTypeFromName(compound_name);
                    numCarbons = PatternFinder.getNumberOfCarbonsFromName(compound_name);
                    numDoubleBonds = PatternFinder.getNumberOfDoubleBondsFromName(compound_name);
                    chains = PatternFinder.getListOfChains(compound_name);
                    numChains = chains.size();
                }

                String codeCategory;
                codeCategory = getCodeCategory(lm_id);

                String codeMainClass;
                codeMainClass = getCodeMainClass(lm_id);

                String codeSubClass;
                codeSubClass = getCodeSubClass(lm_id);

                String codeLevel4Class;
                codeLevel4Class = getCodeLevel4Class(lm_id);

                //System.out.println("CATEGORY: " + codeCategory + " MAIN: " + codeMainClass + " SubClass: " + codeSubClass);
                String query;
                String insertion;
                int compound_id;

                // Compounds which have InChI
                //if (correctFormula && !inChIKey.equals("")) {
                if (!inChIKey.equals("")) {
                    // Check if there is any compound with the same structure
                    compound_id = db.getCompoundIdFromInchiKey(inChIKey);
                    if (compound_id != 0) {

// Case 1.0 there is a compound with same Cas Identifier
// This case does not apply in LipidMaps cause lipidsMaps does not supply CAS
                        String formulaFromCompound = db.getformulaFromCompound(compound_id);
                        double massFromCompound = db.getMassFromCompound(compound_id);
                        // Update formula if the formula is null
// Case  Update formula if it is null in database
                        if (!formula.equals("NULL")) {
                            if (formulaFromCompound.equals("null")) {
                                db.updateCompoundFormula(compound_id, formula);
                            }
                        } else if (formula.equals(formulaFromCompound) && (massFromCompound - massFromLM) < PRECISSION_TO_BE_SAME_COMPOUND_MASS_UNIFICATION) {
// Case 1.1 There is a compound with same mass and formula
                            // The compound is the same, mass is closer between existent compound and LM compound 
                            // and formula is the same
                            // Update information from LM if they have more precission that previous information
                            if (massFromLM != 0.0d) {
                                int precissionFromDB = Utilities.getPrecission(massFromCompound);
                                int precissionFromLM = Utilities.getPrecission(massFromLM);
                                if (precissionFromDB < precissionFromLM) {
                                    db.updateCompoundMass(compound_id, mass);
                                }
                            }
                        } else if (formula.equals(formulaFromCompound) && (massFromCompound - massFromLM) > PRECISSION_TO_BE_SAME_COMPOUND_MASS_UNIFICATION) {
// Case 1.2 There is a compound with different mass
                            String logFileDuplicates = "./log/lm_structure_duplicate_bad_mass.txt";
                            String duplicateInformation = "\nCompound " + lm_id + " has other compound "
                                    + compound_id + " with same structure and different mass";
                            MyFile.write(duplicateInformation, logFileDuplicates);
                        } else if (!formula.equals(formulaFromCompound) && (massFromCompound - massFromLM) < PRECISSION_TO_BE_SAME_COMPOUND_MASS_UNIFICATION) {
// Case 1.3 There is a compound with different formula
                            if (massFromLM != 0.0d) {
                                int precissionFromDB = Utilities.getPrecission(massFromCompound);
                                int precissionFromLM = Utilities.getPrecission(massFromLM);
                                if (precissionFromDB < precissionFromLM) {
                                    db.updateCompoundMass(compound_id, mass);
                                }
                            }
                            String logFileDuplicates = "./log/lm_structure_duplicate_bad_formula.txt";
                            String duplicateInformation = "\nCompound " + lm_id + " has other compound "
                                    + compound_id + " with same structure and different formula";
                            MyFile.write(duplicateInformation, logFileDuplicates);
                        } else {
                            String logFileDuplicates = "./log/lm_structure_duplicate.txt";
                            String duplicateInformation = "\nCompound " + lm_id + " has other compound "
                                    + compound_id + " with the same structure, different formula and different mass";
                            MyFile.write(duplicateInformation, logFileDuplicates);
                        }

                        // Compound is the same if they have the same structure
                        db.insertCompoundLM(compound_id, lm_id);
                        // INSERT THE KNWOLEDGE FROM LIPIDMAPS
                        db.insertLMClassification(compound_id,
                                codeCategory, codeMainClass, codeSubClass, codeLevel4Class);
                        db.insertLipidClassification(compound_id, lipidType, numChains, numCarbons, numDoubleBonds);

                        int existChainsForCompound = db.areThereAlreadyChains(compound_id);
                        if (existChainsForCompound == 0) {
                            for (String chain : chains) {
                                int carbonsOfChain = PatternFinder.getNumberOfCarbons(chain);
                                int doubleBondsOfChain = PatternFinder.getNumberOfDoubleBonds(chain);
                                String oxidationOfChain = PatternFinder.getOxidationFromAbbrev(chain);
                                int chain_id = db.getChainID(carbonsOfChain, doubleBondsOfChain, oxidationOfChain);
                                if (chain_id == 0) {
                                    double massOfChain = Utilities.calculateMassChain(carbonsOfChain,
                                            doubleBondsOfChain, oxidationOfChain);
                                    String formulaOfChain = Utilities.calculateFormulaChain(carbonsOfChain,
                                            doubleBondsOfChain, oxidationOfChain);
                                    chain_id = db.insertChain(carbonsOfChain, doubleBondsOfChain,
                                            oxidationOfChain, massOfChain, formulaOfChain);
                                }
                                int repetitions = db.getRepetitionsSameChain(compound_id, chain_id);
                                if (repetitions == 0) {
                                    db.insertCompoundChainRelation(compound_id, chain_id, 1);
                                } else {
                                    db.updateCompoundChainRepetitions(compound_id, chain_id, (repetitions + 1));
                                }
                            }
                        }
                        return;
                    }
// Does not check CAS ID because it is not supplied
                }

// Check what compounds has different Formula that InChI
                if (correctFormula && !formula.equals("NULL")) {

                    if (!formulaFromInChI.equals("")) {
                        if (!formulaFromInChI.equals(formula)) {
                            String logFileDuplicates = "./log/lm_inchi_formula_incoherence.txt";
                            String duplicateInformation = "Inserting compound " + lm_id
                                    + "\tformula: " + formula
                                    + "\n FORMULA FROM INCHI: \t FORMULA: " + formulaFromInChI
                                    + "\n";
                            MyFile.write(duplicateInformation, logFileDuplicates);
                            //Then, the real formula is the formula from InChI identifier
                            //formula = "\"" + formulaFromInChI + "\"";
                        }
                    }
                }

                compound_id = db.insertCompound(casID, compound_name, formula, mass,
                        chargeType, numCharges, formulaType, compoundType, compoundStatus, logP);
                // System.out.println("\nID retrieved: " + compound_id);
                if (correctFormula && !inChIKey.equals("")) {
                    db.insertIdentifiers(compound_id, inChI, inChIKey, SMILES);
                }
                db.insertCompoundLM(compound_id, lm_id);

                db.insertLMClassification(compound_id, codeCategory, codeMainClass, codeSubClass, codeLevel4Class);
                db.insertLipidClassification(compound_id, lipidType, numChains, numCarbons, numDoubleBonds);

                for (String chain : chains) {
                    int carbonsOfChain = PatternFinder.getNumberOfCarbons(chain);
                    int doubleBondsOfChain = PatternFinder.getNumberOfDoubleBonds(chain);
                    String oxidationOfChain = PatternFinder.getOxidationFromAbbrev(chain);
                    int chain_id = db.getChainID(carbonsOfChain, doubleBondsOfChain, oxidationOfChain);
                    if (chain_id == 0) {
                        double massOfChain = Utilities.calculateMassChain(carbonsOfChain,
                                doubleBondsOfChain, oxidationOfChain);
                        String formulaOfChain = Utilities.calculateFormulaChain(carbonsOfChain,
                                doubleBondsOfChain, oxidationOfChain);
                        chain_id = db.insertChain(carbonsOfChain, doubleBondsOfChain,
                                oxidationOfChain, massOfChain, formulaOfChain);
                    }
                    int repetitions = db.getRepetitionsSameChain(compound_id, chain_id);
                    if (repetitions == 0) {
                        db.insertCompoundChainRelation(compound_id, chain_id, 1);
                    } else {
                        db.updateCompoundChainRepetitions(compound_id, chain_id, (repetitions + 1));
                    }
                }

                // Start classification from Classyfire
                // Start with knowledge
                List<String> ancestorNodes = new LinkedList();
                try {
                    ancestorNodes = downloadersOfWebResources.DownloaderOfWebResources.getAncestorNodesCompoundClassyfireClassification(inChI, inChIKey);
                    db.insertFullCompoundClassyfireClassification(compound_id, ancestorNodes);
                } catch (CompoundNotClassifiedException ex) {
                } catch (NodeNotFoundException ex) {
                    Logger.getLogger(HMDBDatabasePopulator.class.getName()).log(Level.SEVERE, null, ex);
                }

            } catch (IllegalStateException ex) {
                System.out.println("LM: " + lm_id + "DOES NOT EXIST");
                //String query = "delete from compounds_lm where lm_id = " + lm_id;
                //db.executeNewIDU(query);
                return;
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(LMDatabasePopulator.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
    }

    private void updateFromAJSONFile(int compound_id, String lmId, CMMDatabase db) {
        String lm_id = "\"" + lmId + "\"";
// ONLY IF YOU DONT WANT TO UPDATE
/*
        File LMJSONFile = new File(Constants.LIPIDMAPS_RESOURCES_JSON_PATH + lmId + ".json");
        if (LMJSONFile.exists()) {
            return;
        }
         */

        DownloaderOfWebResources.downloadLipidMapsJSONFile(lmId);
        try {
            FileReader fileJSON = new FileReader(Constants.LIPIDMAPS_RESOURCES_JSON_PATH + lmId + ".json");
            JsonReader reader = new JsonReader(fileJSON);
            JsonParser parser = new JsonParser();
            try {
                JsonObject lmCompoundJSON = parser.parse(reader).getAsJsonObject();
                //System.out.println("RESPONSE FULL: " + lmCompoundJSON.toString());
                String compound_name;
                compound_name = getNameFromJSON(lmCompoundJSON);

                String common_name;
                common_name = getCommonNameFromJSON(lmCompoundJSON);

                String formula;
                formula = getFormulaFromJSON(lmCompoundJSON);

                String formulaType;
                try {
                    formulaType = PatternFinder.getTypeFromFormula(formula);
                } catch (IllegalArgumentException iae) {
                    System.out.println("LM: " + lm_id + " WITH SKELETON FORMULA");
                    return;
                }
                boolean correctFormula;
                correctFormula = !formulaType.equals("");
                String inChI = "";
                String inChIKey = "";
                String inchiKeyFromDb = "-";
                String SMILES = "";
                String formulaFromInChI = "";
                if (correctFormula) {
                    inChI = getInChIFromJSON(lmCompoundJSON);
                    inChIKey = getInChIKeyFromJSON(lmCompoundJSON);
                    SMILES = getSMILESFromJSON(lmCompoundJSON);
                    inchiKeyFromDb = db.getInChIKeyFromCompound(compound_id);
                    if (!inChIKey.equals(inchiKeyFromDb)) {
                        System.out.println("BAD INCHI KEY IN BBDD: ");
                        int new_compound_id = db.getCompoundIdFromInchiKey(inChIKey);
                        if (new_compound_id > 0) {
                            db.deleteCompoundLM(compound_id, lmId);
                            db.insertCompoundLM(new_compound_id, lmId);
                        } else if (new_compound_id == 0) {
                            if (inchiKeyFromDb.length() == 27) {
                                db.updateIdentifiers(compound_id, inChI, inChIKey, SMILES);
                            } else {
                                db.insertIdentifiers(compound_id, inChI, inChIKey, SMILES);
                            }
                        } else {
                            System.out.println("ERROR OBTAINING ID FROM INCHI");
                        }
                    }
                    formulaFromInChI = Checker.getFormulaFromInChI(inChI);
                } else {
                    String logFileFormulaBadElements = "./log/lm_formula_bad_elements.txt";
                    String duplicateInformation = "\nCompound " + lmId + " has a formula "
                            + formula + " with no chemical elements";
                    MyFile.write(duplicateInformation, logFileFormulaBadElements);
                }
                String mass;
                double massFromLM;
                mass = getMassFromJSON(lmCompoundJSON);
                if (mass.equalsIgnoreCase("") || Double.parseDouble(mass) < 0.01d) {
                    mass = "NULL";
                    massFromLM = 0.0d;
                } else {
                    massFromLM = Double.parseDouble(mass);
                }
                String abbrev;
                abbrev = getABBREVFromJSON(lmCompoundJSON);
                String lipidType;
                String abbrev_chains;
                int numChains = 0;
                List<String> chains;
                int numCarbons;
                int numDoubleBonds;
                if (!abbrev.equals("")) {

                    lipidType = getLipidTypeFromAbbrev(abbrev);
                    //System.out.println("ABBREV: " + abbrev);
                    abbrev_chains = getAbbrevChainsFromJSON(lmCompoundJSON);
                    numCarbons = getNumberOfCarbonsFromAbbrev(abbrev);
                    numDoubleBonds = getNumberOfDoubleBondsFromAbbrev(abbrev);
                    //System.out.println("CARBONS: " + numCarbons + "  Double Bonds: " + numDoubleBonds);
                    if (!abbrev_chains.equals("")) {
// Get number of carbons and double bonds from abbrev chains. ABBREV CHAINS SHOULD NOT BE EMPTY

                        chains = PatternFinder.getListOfChains(abbrev_chains);
                        numChains = chains.size();
                        //System.out.println("chains: " + numChains + " \t" + chains);
                    } else {
                        // Try to get chains from NAME, the abbrev usually 
                        // put them together. 
                        chains = PatternFinder.getListOfChains(compound_name);
                        numChains = chains.size();
                        if (numChains == 0) {
                            chains = PatternFinder.getListOfChains(abbrev);
                            numChains = chains.size();
                            if (numChains > 1) {
                                System.out.println(" CHAINS > 1 in LM: " + lmId + " but not abbrev_chains!");
                            }
                        } // TODO ALBERTO QUIT WHEN IT UPDATES!
                        else {
                            // TAKING INFO FROM COMPOUND_NAME
                            //System.out.println("NAME: " + compound_name + " ABBREV: " + abbrev
                            //+ " ABBREV CHAINS: " + abbrev_chains +" LM_ID: " + lm_id );
                            String lipidTypeFromName = PatternFinder.getLipidTypeFromName(compound_name);
                            int numCarbonsFromName = PatternFinder.getNumberOfCarbonsFromName(compound_name);
                            int numDoubleBondsFromName = PatternFinder.getNumberOfDoubleBondsFromName(compound_name);
                            if (!lipidTypeFromName.equals(lipidType) || numCarbons != numCarbonsFromName
                                    || numDoubleBonds != numDoubleBondsFromName) {
                                System.out.println("BAD RELATION IN CHAINS:");
                                System.out.println("LIPID TYPE ABBREV: " + lipidType + " NAME: " + lipidTypeFromName);
                                System.out.print(" NUM CARBONS ABBREV: " + numCarbons + " NAME: " + numCarbonsFromName);
                                System.out.print(" DOUBLE BONDS ABBREV: " + numDoubleBonds + " NAME: " + numDoubleBondsFromName);
                            }
                            db.deleteCompoundChainRelationCompoundId(compound_id);
                            db.updateLipidClassification(compound_id, lipidType, numChains, numCarbons, numDoubleBonds);
                        }
                    }
                } else {
                    abbrev_chains = getAbbrevChainsFromJSON(lmCompoundJSON);
                    if (!abbrev_chains.equals("")) {
                        System.out.println("ABBREV CHAINS WITHOUT ABBREV IN; " + lmId);
                    }
                    // Obtain information from NAME, sometimes the attributes from lipidmaps are wrong
                    lipidType = PatternFinder.getLipidTypeFromName(compound_name);
                    if (!lipidType.equals("")) {
                        System.out.println("NAME: " + compound_name + " NO ABBREV. LM_ID: " + lm_id);
                    }
                    numCarbons = PatternFinder.getNumberOfCarbonsFromName(compound_name);
                    numDoubleBonds = PatternFinder.getNumberOfDoubleBondsFromName(compound_name);
                    chains = PatternFinder.getListOfChains(compound_name);
                    numChains = chains.size();
                }

                String codeCategory;
                codeCategory = getCodeCategory(lmId);

                String codeMainClass;
                codeMainClass = getCodeMainClass(lmId);

                String codeSubClass;
                codeSubClass = getCodeSubClass(lmId);

                String codeLevel4Class;
                codeLevel4Class = getCodeLevel4Class(lmId);

                // compoundType 1 for lipids
                int compoundType = 1;

                String query;
                String insertion;

// Case 1.0 there is a compound with same Cas Identifier
// This case does not apply in LipidMaps cause lipidsMaps does not supply CAS
                String formulaFromCompound = db.getformulaFromCompound(compound_id);
                double massFromCompound = db.getMassFromCompound(compound_id);
                // Update formula if the formula is null
// Case  Update formula if it is null in database
                if (!formula.equalsIgnoreCase("NULL")) {
                    if (formulaFromCompound.equals("null")) {
                        db.updateCompoundFormula(compound_id, formula);
                    }
                } else if (formula.equals(formulaFromCompound) && (massFromCompound - massFromLM) < PRECISSION_TO_BE_SAME_COMPOUND_MASS_UNIFICATION) {
// Case 1.1 There is a compound with same mass and formula
                    // The compound is the same, mass is closer between existent compound and LM compound 
                    // and formula is the same
                    // Update information from LM if they have more precission that previous information
                    if (massFromLM != 0.0d) {
                        int precissionFromDB = Utilities.getPrecission(massFromCompound);
                        int precissionFromLM = Utilities.getPrecission(massFromLM);
                        if (precissionFromDB < precissionFromLM) {
                            db.updateCompoundMass(compound_id, mass);
                        }
                    }
                } else if (formula.equals(formulaFromCompound) && (massFromCompound - massFromLM) > PRECISSION_TO_BE_SAME_COMPOUND_MASS_UNIFICATION) {
// Case 1.2 There is a compound with different mass
                    String logFileDuplicates = "./log/lm_structure_duplicate_bad_mass.txt";
                    String duplicateInformation = "\nCompound " + lmId + " has other compound "
                            + compound_id + " with same structure and different mass";
                    MyFile.write(duplicateInformation, logFileDuplicates);
                } else if (!formula.equals(formulaFromCompound) && (massFromCompound - massFromLM) < PRECISSION_TO_BE_SAME_COMPOUND_MASS_UNIFICATION) {
// Case 1.3 There is a compound with different formula
                    if (massFromLM != 0.0d) {
                        int precissionFromDB = Utilities.getPrecission(massFromCompound);
                        int precissionFromLM = Utilities.getPrecission(massFromLM);
                        if (precissionFromDB < precissionFromLM) {
                            db.updateCompoundMass(compound_id, mass);
                        }
                    }
                    String logFileDuplicates = "./log/lm_structure_duplicate_bad_formula.txt";
                    String duplicateInformation = "\nCompound " + lmId + " has other compound "
                            + compound_id + " with same structure and different formula";
                    MyFile.write(duplicateInformation, logFileDuplicates);
                } else {
                    String logFileDuplicates = "./log/lm_structure_duplicate.txt";
                    String duplicateInformation = "\nCompound " + lmId + " has other compound "
                            + compound_id + " with the same structure, different formula and different mass";
                    MyFile.write(duplicateInformation, logFileDuplicates);
                }

                // INSERT THE KNWOLEDGE FROM LIPIDMAPS
                db.insertLMClassification(compound_id, codeCategory, codeMainClass, codeSubClass, codeLevel4Class);
                db.insertLipidClassification(compound_id, lipidType, numChains, numCarbons, numDoubleBonds);

                int existChainsForCompound = db.areThereAlreadyChains(compound_id);
                // If new information was present in LM, there are no chains since 
                // the old ones were deleted in line 480
                if (existChainsForCompound == 0) {
                    for (String chain : chains) {
                        int carbonsOfChain = PatternFinder.getNumberOfCarbons(chain);
                        int doubleBondsOfChain = PatternFinder.getNumberOfDoubleBonds(chain);
                        String oxidationOfChain = PatternFinder.getOxidationFromAbbrev(chain);
                        int chain_id = db.getChainID(carbonsOfChain, doubleBondsOfChain, oxidationOfChain);
                        if (chain_id == 0) {
                            double massOfChain = Utilities.calculateMassChain(carbonsOfChain,
                                    doubleBondsOfChain, oxidationOfChain);
                            String formulaOfChain = Utilities.calculateFormulaChain(carbonsOfChain,
                                    doubleBondsOfChain, oxidationOfChain);
                            chain_id = db.insertChain(carbonsOfChain, doubleBondsOfChain, oxidationOfChain,
                                    massOfChain, formulaOfChain);
                        }
                        int repetitions = db.getRepetitionsSameChain(compound_id, chain_id);
                        if (repetitions == 0) {
                            db.insertCompoundChainRelation(compound_id, chain_id, 1);
                        } else {
                            db.updateCompoundChainRepetitions(compound_id, chain_id, (repetitions + 1));
                        }
                    }
                }

// Check what compounds has different Formula that InChI
                if (correctFormula && !formula.equalsIgnoreCase("NULL")) {

                    if (!formulaFromInChI.equals("")) {
                        if (!formulaFromInChI.equals(formula)) {
                            String logFileDuplicates = "./log/lm_inchi_formula_incoherence.txt";
                            String duplicateInformation = "Inserting compound " + lmId
                                    + "\tformula: " + formula
                                    + "\n FORMULA FROM INCHI: \t FORMULA: " + formulaFromInChI
                                    + "\n";
                            MyFile.write(duplicateInformation, logFileDuplicates);
                            //Then, the real formula is the formula from InChI identifier
                            //formula = "\"" + formulaFromInChI + "\"";
                        }
                    }
                }
                // System.out.println("\nID retrieved: " + compound_id);
                if (correctFormula && !inChIKey.equals("")) {
                    //db.updateIdentifiers(compound_id, inChI, inChIKey, SMILES);
                }

                //Insert identifiers if they are not in the database
                int compoundIdFromInchi = db.getCompoundIdFromInchiKey(inChIKey);
                if (compoundIdFromInchi > 0 && compoundIdFromInchi != compound_id) {
                    db.insertIdentifiers(compound_id, inChI, inChIKey, SMILES);
                }

                // Start with knowledge from CLASSIFYFIRE
                List<String> ancestorNodes = new LinkedList();
                try {
                    ancestorNodes = downloadersOfWebResources.DownloaderOfWebResources.getAncestorNodesCompoundClassyfireClassification(inChI, inChIKey);
                    db.insertFullCompoundClassyfireClassification(compound_id, ancestorNodes);
                } catch (CompoundNotClassifiedException ex) {
                } catch (NodeNotFoundException ex) {
                    Logger.getLogger(HMDBDatabasePopulator.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (IllegalStateException ex) {
                System.out.println("LM" + lm_id + "DOES NOT EXIST");
                //String query = "delete from compounds_lm where lm_id = " + lm_id;
                //db.executeNewIDU(query);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(LMDatabasePopulator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void populateFromLM(CMMDatabase db) {

        File logFileDuplicates = new File("./log/lm_formula_bad_elements.txt");
        if (logFileDuplicates.exists()) {
            //    logFileDuplicates.delete();
        }
        logFileDuplicates = new File("./log/lm_structure_formula_duplicate.txt");
        if (logFileDuplicates.exists()) {
            //    logFileDuplicates.delete();
        }
        logFileDuplicates = new File("./log/lm_structure_duplicate.txt");
        if (logFileDuplicates.exists()) {
            //    logFileDuplicates.delete();
        }
        logFileDuplicates = new File("./log/lm_inchi_formula_incoherence.txt");
        if (logFileDuplicates.exists()) {
            //    logFileDuplicates.delete();
        }

        File dir = new File(LIPIDMAPS_RESOURCES_PATH);
        //System.out.println(" ---> " + dir.getAbsolutePath());
        String[] files = dir.list();
//        populateFromADownloadedCompleteFile("LMSDFDownload28Jun15FinalPR.sdf", db);
        for (String file : files) {
            // I GO FOR GP
            if (file.endsWith(".sdf")) {
                // Deprecated. Used when we did not access the REST API
                //populateFromADownloadedCompleteFile(file, db);
                populateFromADownloadedFile(file, db);
                // Deprecated. Used when we did not access the REST API
                // downloadInChIs(file);
            }
        }
    }

    private String getLMID(String compound) {
        //System.out.println("COMPOUND: " + compound);
        String lm_id = PatternFinder.searchWithReplacement(compound, "<LM_ID>(.*?)>\\s", "<LM_ID>|>\\s");
        if (!lm_id.equals("")) {
            lm_id = lm_id.substring(0, lm_id.length() - 1);
        }
        return lm_id;
    }

    private String getName(String compound) {
        String compound_name = "";
        String systematic_name = PatternFinder.searchWithReplacement(compound, "<SYSTEMATIC_NAME>(.*?)>\\s", "<SYSTEMATIC_NAME>|>\\s");
        String common_name = PatternFinder.searchWithReplacement(compound, "<COMMON_NAME>(.*?)>\\s", "<COMMON_NAME>|>\\s");
        if (common_name.equalsIgnoreCase("")) {
            if (systematic_name.equalsIgnoreCase("")) {

            } else {
                compound_name = systematic_name.replaceAll("\"", "\\'");
                compound_name = compound_name.replaceAll("'", "\\'");
                compound_name = compound_name.substring(0, compound_name.length() - 1);
            }
        } else {
            compound_name = common_name.replaceAll("\"", "\\'");
            compound_name = compound_name.replaceAll("'", "\\'");
            compound_name = compound_name.substring(0, compound_name.length() - 1);
            //if (!systematic_name.equalsIgnoreCase("")) {
            //    compound_name = compound_name + ";\n" + systematic_name + ";";
            //    compound_name = compound_name.substring(0, compound_name.length() - 2);
            //}
        }
        return compound_name;
    }

    private String getNameFromJSON(JsonObject lmCompoundJSON) {
        String compound_name = "";
        String systematic_name;
        String common_name;
        if (lmCompoundJSON.get(Constants.LIPIDMAPS_JSON_COMMON_NAME) != null) {
            common_name = lmCompoundJSON.get(Constants.LIPIDMAPS_JSON_COMMON_NAME).getAsString();
            compound_name = common_name.replaceAll("\"", "\\'");
            compound_name = compound_name.replaceAll("'", "\\'");
        } else if (lmCompoundJSON.get(Constants.LIPIDMAPS_JSON_SYSTEMATIC_NAME) != null) {
            systematic_name = lmCompoundJSON.get(Constants.LIPIDMAPS_JSON_SYSTEMATIC_NAME).getAsString();
            compound_name = systematic_name.replaceAll("\"", "\\'");
            compound_name = compound_name.replaceAll("'", "\\'");
        }
        return compound_name;
    }

    private String getCommonName(String compound) {
        String compound_name = "";
        String common_name = PatternFinder.searchWithReplacement(compound, "<COMMON_NAME>(.*?)>\\s", "<COMMON_NAME>|>\\s");
        if (common_name.equalsIgnoreCase("")) {
        } else {
            compound_name = common_name.replaceAll("\"", "\\'");
            compound_name = compound_name.replaceAll("'", "\\'");
            compound_name = compound_name.substring(0, compound_name.length() - 1);
        }
        return compound_name;
    }

    private String getCommonNameFromJSON(JsonObject lmCompoundJSON) {
        String compound_name = "";
        String common_name;
        if (lmCompoundJSON.get(Constants.LIPIDMAPS_JSON_COMMON_NAME) != null) {
            common_name = lmCompoundJSON.get(Constants.LIPIDMAPS_JSON_COMMON_NAME).getAsString();
            compound_name = common_name.replaceAll("\"", "\\'");
            compound_name = compound_name.replaceAll("'", "\\'");
        }
        return compound_name;
    }

    private String getFormula(String compound) {
        String formula = PatternFinder.searchWithReplacement(compound, "<FORMULA>(.*?)>\\s", "<FORMULA>|>\\s");
        if (!formula.equalsIgnoreCase("")) {
            formula = formula.substring(0, formula.length() - 1);
        } else {
            formula = "NULL";
        }
        return formula;
    }

    private String getFormulaFromJSON(JsonObject lmCompoundJSON) {
        String formula = "";
        if (lmCompoundJSON.get(Constants.LIPIDMAPS_JSON_FORMULA) != null) {
            formula = lmCompoundJSON.get(Constants.LIPIDMAPS_JSON_FORMULA).getAsString();
        } else {
            formula = "NULL";
        }
        return formula;
    }

    private String getMass(String compound) {
        String mass = PatternFinder.searchWithReplacement(compound, "<EXACT_MASS>(.*?)>\\s", "<EXACT_MASS>|>\\s");
        if (!mass.equalsIgnoreCase("")) {
            mass = mass.substring(0, mass.length() - 1);
        }
        return mass;
    }

    private String getMassFromJSON(JsonObject lmCompoundJSON) {
        String mass = "";
        if (lmCompoundJSON.get(Constants.LIPIDMAPS_JSON_MASS) != null) {
            mass = lmCompoundJSON.get(Constants.LIPIDMAPS_JSON_MASS).getAsString();
        }
        return mass;
    }

    private String getAbbrevChainsFromJSON(JsonObject lmCompoundJSON) {
        String chains = "";
        if (lmCompoundJSON.get(Constants.LIPIDMAPS_JSON_ABBREV_CHAINS) != null) {
            chains = lmCompoundJSON.get(Constants.LIPIDMAPS_JSON_ABBREV_CHAINS).getAsString();
        }
        return chains;
    }

    private int getNumberOfChains(String chains) {
        int numChains;
        numChains = chains.split("_").length;
        return numChains;
    }

    private String getABBREVFromJSON(JsonObject lmCompoundJSON) {
        String abbrev = "";
        if (lmCompoundJSON.get(Constants.LIPIDMAPS_JSON_ABBREV) != null) {
            abbrev = lmCompoundJSON.get(Constants.LIPIDMAPS_JSON_ABBREV).getAsString();
        }
        return abbrev;
    }

    private String getLipidTypeFromAbbrev(String abbrev) {
        String lipidType = "";
        if (!abbrev.equalsIgnoreCase("")) {
            lipidType = PatternFinder.getLipidType(abbrev);
        }
        return lipidType;
    }

    private int getNumberOfCarbonsFromAbbrev(String abbrev) {
        int numCarbons = 0;
        numCarbons = PatternFinder.getNumberOfCarbons(abbrev);
        return numCarbons;
    }

    private int getNumberOfDoubleBondsFromAbbrev(String abbrev) {
        int numDoubleBonds = 0;
        numDoubleBonds = PatternFinder.getNumberOfDoubleBonds(abbrev);
        return numDoubleBonds;
    }

    private String getKeggIdFromLM(String compound) {
        String kegg_id = PatternFinder.searchWithReplacement(compound, "<KEGG_ID>(.*?)>\\s", "<KEGG_ID>|>\\s");
        if (!kegg_id.equalsIgnoreCase("")) {
            kegg_id = kegg_id.substring(0, kegg_id.length() - 1);
        }
        return kegg_id;
    }

    private String getHMDBIdFromLM(String compound) {
        String hmdb_id = PatternFinder.searchWithReplacement(compound, "<HMDBID>(.*?)>\\s", "<HMDBID>|>\\s");
        if (!hmdb_id.equalsIgnoreCase("")) {
            hmdb_id = hmdb_id.substring(0, hmdb_id.length() - 1);
        }
        return hmdb_id;
    }

    private String getPCIdFromLM(String compound) {
        String pc_id = PatternFinder.searchWithReplacement(compound, "<PUBCHEM_CID>(.*?)>\\s", "<PUBCHEM_CID>|>\\s");
        if (!pc_id.equalsIgnoreCase("")) {
            pc_id = pc_id.substring(0, pc_id.length() - 1);
        }
        return pc_id;
    }

    private String getInChI(String lm_id) {
        // NECESSARY TO ACCESS API REST :
        String inchiFileName = LIPIDMAPS_RESOURCES_INCHIS_PATH + lm_id + ".txt";
        File inchiFile = new File(inchiFileName);
        if (!inchiFile.exists()) {
            downloadInChILM(lm_id);
            // The update of the inchis may be done separately with the method downloadInChIs(String fileName)
        }
        if (!inchiFile.exists()) {
            return "";
        } else {
            String content = MyFile.obtainContentOfABigFile(inchiFileName).toString();
            String inchiStart = "inchi\t";
            String inchiEnd = "\n";
            String inchi = PatternFinder.searchWithReplacement(content, inchiStart + "(.*?)" + inchiEnd, inchiStart + "|" + inchiEnd);
            inchi = "InChI=" + inchi;
            inchi = inchi.substring(0, inchi.length() - 1);
            //System.out.println("Inchi From LIPIDMAPS: " + inchi);
            return inchi;
        }
    }

    private String getInChIFromJSON(JsonObject lmCompoundJSON) {
        String inchi = "";
        if (lmCompoundJSON.get(Constants.LIPIDMAPS_JSON_INCHI) != null) {
            inchi = lmCompoundJSON.get(Constants.LIPIDMAPS_JSON_INCHI).getAsString();
        }
        return inchi;
    }

    private String getInChIKey(String compound) {
        String inchi_key = PatternFinder.searchWithReplacement(compound, "<INCHI_KEY>(.*?)>\\s", "<INCHI_KEY>|>\\s");
        if (!inchi_key.equalsIgnoreCase("")) {
            inchi_key = inchi_key.substring(0, inchi_key.length() - 1);
        }
        return inchi_key;
    }

    private String getInChIKeyFromJSON(JsonObject lmCompoundJSON) {
        String inchi_key = "";
        if (lmCompoundJSON.get(Constants.LIPIDMAPS_JSON_INCHI_KEY) != null) {
            inchi_key = lmCompoundJSON.get(Constants.LIPIDMAPS_JSON_INCHI_KEY).getAsString();
        }
        return inchi_key;
    }

    private String getSMILESFromJSON(JsonObject lmCompoundJSON) {
        String SMILES = "";
        if (lmCompoundJSON.get(Constants.LIPIDMAPS_JSON_SMILES) != null) {
            SMILES = lmCompoundJSON.get(Constants.LIPIDMAPS_JSON_SMILES).getAsString();
        }
        return SMILES;
    }

    public void downloadInchi(String compound) {
        String lm_id = getLMID(compound);
        String inchiFileName = LIPIDMAPS_RESOURCES_INCHIS_PATH + lm_id + ".txt";
        File inchiFile = new File(inchiFileName);
        if (!inchiFile.exists()) {
            downloadInChILM(lm_id);
        }

    }

    /**
     * Download all the inchi codes for the compounds listed in the resources
     * folder
     *
     * @param fileName
     */
    public void downloadInChIs(String fileName) {

        StringBuilder content;
        content = MyFile.obtainContentOfABigFile(LIPIDMAPS_RESOURCES_PATH + fileName);
        StringBuilder result = new StringBuilder();
        PatternFinder compoundFinder = new PatternFinder();

        while (compoundFinder.searchInDifferentCalls(content, "<LIPID_MAPS_CMPD_URL>(.*?)<STATUS>",
                "<LIPID_MAPS_CMPD_URL>|<STATUS>", result)) {
            String compound = result.toString();
            downloadInchi(compound);
        }
    }

    private void downloadInChILM(String lm_id) {
        // http://www.lipidmaps.org/rest/compound/lm_id/LM_ID/inchi/txt
        String LMInChIFileName = "http://www.lipidmaps.org/rest/compound/lm_id/" + lm_id + "/inchi/txt";
        try {
            // Download the content of the lipidmapscompound compound and write it in a file text
            String content = UriDevice.readString(LMInChIFileName);
            if (content.equals("")) {
                System.out.println("compound with code " + lm_id + " does not exist\n");
            } else {
                MyFile.write(content, LIPIDMAPS_RESOURCES_INCHIS_PATH + lm_id + ".txt");
                System.out.println("downloading inchi file of: " + lm_id);
                try {
                    Random randomGenerator = new Random();
                    int randomInt;
                    randomInt = randomGenerator.nextInt((10000 - 1000) + 1) + 1000;
                    Thread.sleep(randomInt);                 //1000 milliseconds is one second.
                } catch (InterruptedException ex) {
                    System.out.println("Thread interrupted");
                    Thread.currentThread().interrupt();
                }
            }
        } catch (FileNotFoundException fnfe) {
            System.out.println("Resource " + LMInChIFileName + " is not available");
        } catch (UnknownHostException uhe) {
            MyFile.write(lm_id, "LOG_UNREAD_LM_INCHIS.txt");
        }
    }

    /*
    private String getCodeCategory(String compound) {
        String compound_category = PatternFinder.searchWithReplacement(compound, "<CATEGORY>(.*?)>\\s", "<CATEGORY>|>\\s");
        String codeCategory = PatternFinder.searchWithReplacement(compound_category, "\\[(.*?)\\]", "\\[|\\]");
        return codeCategory;
    }

    private String getCodeMainClass(String compound) {
        String compound_main_class = PatternFinder.searchWithReplacement(compound, "<MAIN_CLASS>(.*?)>\\s", "<MAIN_CLASS>|>\\s");
        String codeMainClass = PatternFinder.searchWithReplacement(compound_main_class, "\\[(.*?)\\]", "\\[|\\]");
        return codeMainClass;
    }

    private String getCodeSubClass(String compound) {
        String compound_sub_class = PatternFinder.searchWithReplacement(compound, "<SUB_CLASS>(.*?)>\\s", "<SUB_CLASS>|>\\s");
        String codeSubClass = PatternFinder.searchWithReplacement(compound_sub_class, "\\[(.*?)\\]", "\\[|\\]");
        return codeSubClass;
    }

    private String getCodeLevel4Class(String compound) {
        String compound_class_level4 = PatternFinder.searchWithReplacement(compound, "<CLASS_LEVEL4>(.*?)>\\s", "<CLASS_LEVEL4>|>\\s");
        String codeClassLevel4 = PatternFinder.searchWithReplacement(compound_class_level4, "\\[(.*?)\\]", "\\[|\\]");
        return codeClassLevel4;
    }
     */
    private String getCodeCategory(String LMId) {
        String codeCategory = LMId.substring(2, 4);
        return codeCategory;
    }

    private String getCategoryFromJSON(JsonObject lmCompoundJSON) {
        String category = "";
        String codeCategory = "";
        if (lmCompoundJSON.get(Constants.LIPIDMAPS_JSON_CATEGORY) != null) {
            category = lmCompoundJSON.get(Constants.LIPIDMAPS_JSON_CATEGORY).getAsString();
            codeCategory = PatternFinder.getCodeLMJSON(category);
        }
        return codeCategory;
    }

    private String getCodeMainClass(String LMId) {
        String codeMainClass = LMId.substring(2, 6);
        return codeMainClass;
    }

    private String getMainClassFromJSON(JsonObject lmCompoundJSON) {
        String mainClass = "";
        String codeMainClass = "";
        if (lmCompoundJSON.get(Constants.LIPIDMAPS_JSON_MAIN_CLASS) != null) {
            mainClass = lmCompoundJSON.get(Constants.LIPIDMAPS_JSON_MAIN_CLASS).getAsString();
            codeMainClass = PatternFinder.getCodeLMJSON(mainClass);
        }
        return codeMainClass;
    }

    private String getCodeSubClass(String LMId) {
        String hasSubClass = LMId.substring(6, 9);
        if (hasSubClass.equals("00")) {
            return "";
        } else {
            String codeSubClass = LMId.substring(2, 8);
            return codeSubClass;
        }
    }

    private String getSubClassFromJSON(JsonObject lmCompoundJSON) {
        String subClass = "";
        String codeSubClass = "";
        if (lmCompoundJSON.get(Constants.LIPIDMAPS_JSON_SUB_CLASS) != null) {
            subClass = lmCompoundJSON.get(Constants.LIPIDMAPS_JSON_SUB_CLASS).getAsString();
            codeSubClass = PatternFinder.getCodeLMJSON(subClass);
        }
        return codeSubClass;
    }

    private String getCodeLevel4Class(String LMId) {
        if (LMId.length() != 14) {
            return "";
        } else {
            String codeSubClass = LMId.substring(2, 10);
            return codeSubClass;
        }
    }

    private String getLevel4ClassFromJSON(JsonObject lmCompoundJSON) {
        String level4Class = "";
        String codeLevel4Class = "";
        if (lmCompoundJSON.get(Constants.LIPIDMAPS_JSON_CLASS_LEVEL_4) != null) {
            level4Class = lmCompoundJSON.get(Constants.LIPIDMAPS_JSON_CLASS_LEVEL_4).getAsString();
            codeLevel4Class = PatternFinder.getCodeLMJSON(level4Class);
        }
        return level4Class;
    }

}
