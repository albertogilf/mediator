package newDatabasePopulators;

import databases.CMMDatabase;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import mine.MineJSONProcessor;
import mine.MineSDFProcessor;

/**
 *
 * @author aesteban
 */
public class MineDatabasePopulator {

    public MineDatabasePopulator() {
    }

    /**
     * Populate wirh MINE databases. Those are KEGG, YMDB, EcoCyG.
     *
     * @param db
     */
    public void populate(CMMDatabase db) {
        try {
            long inittime = System.currentTimeMillis();
            long time = System.currentTimeMillis();

            // For enabling databases look ar MineConstants: PROCCESS_KEGG, PROCCESS_YMDB PROCCESS_ECOCYC
            // -----------------------------------------
            // Processing SDF files.
            // -----------------------------------------
            // 1. Files are downloaded from server.
            // MineSDFProcessor.downloadMineSDFResources();        
            System.out.println("## MineSDFProcessor.downloadMineSDFResources --> ELAPSED TIME: " + (System.currentTimeMillis() - time) + " ms.");
            time = System.currentTimeMillis();

            // 2. Files are decompressed.
            // MineSDFProcessor.decompressMineSDFFiles();        
            System.out.println("## MineSDFProcessor.decompressMineSDFFiles --> ELAPSED TIME: " + (System.currentTimeMillis() - time) + " ms.");
            time = System.currentTimeMillis();

            // 3. Files are divided in mol files.
            // MineSDFProcessor.processSDFFiles();
            System.out.println("## MineSDFProcessor.processSDFFiles --> ELAPSED TIME: " + (System.currentTimeMillis() - time) + " ms.");
            time = System.currentTimeMillis();

            // 4. Generates inchi and inchi-key files.
            // MineSDFProcessor.generateInchiFiles();
            System.out.println("## MineSDFProcessor.generateInchiFiles --> ELAPSED TIME: " + (System.currentTimeMillis() - time) + " ms.");
            time = System.currentTimeMillis();

            // -----------------------------------------
            // Processing JSON files.
            // -----------------------------------------
            // 1. Downloading JSON files.
            //MineJSONProcessor.downloadMineJSONResources();
            System.out.println("## MineJSONProcessor.downloadMineJSONResources --> ELAPSED TIME: " + (System.currentTimeMillis() - time) + " ms.");
            time = System.currentTimeMillis();

            // 2. Verify compounds with mol files.
            //MineJSONProcessor.processJSONFileForFixingMolFile();
            System.out.println("## MineJSONProcessor.processJSONFileForFixingMolFile --> ELAPSED TIME: " + (System.currentTimeMillis() - time) + " ms.");
            time = System.currentTimeMillis();

            // 3. Creates CSV with molecules.
            //MineJSONProcessor.processJSONFileIntoCSV();
            System.out.println("## MineJSONProcessor.processJSONFileIntoCSV --> ELAPSED TIME: " + (System.currentTimeMillis() - time) + " ms.");
            time = System.currentTimeMillis();

            // 4. Insert into BBDD generated compounds. TABLE compounds_gen y TABLE compounds_gen_identifiers.
            MineJSONProcessor.processGeneratedJSONFileIntoBBDD(db);
            System.out.println("## MineJSONProcessor.processGeneratedJSONFileIntoBBDD --> ELAPSED TIME: " + (System.currentTimeMillis() - time) + " ms.");
            time = System.currentTimeMillis();

            // 5. Check not generated compounds.
            MineJSONProcessor.processJSONFileForCheckingExistence();
            System.out.println("## MineJSONProcessor.processJSONFileForChecking --> ELAPSED TIME: " + (System.currentTimeMillis() - time) + " ms.");
            time = System.currentTimeMillis();

            System.out.println("## MineDatabasePopulator --> TOTAL ELAPSED TIME: " + (System.currentTimeMillis() - inittime) + " ms.");

//        } catch (IOException | InterruptedException ex) {
        } catch (IOException ex) {
            Logger.getLogger(MineDatabasePopulator.class.getName()).log(Level.SEVERE, null, ex);
        }
        /*catch (InterruptedException ex) {
            Logger.getLogger(MineDatabasePopulator.class.getName()).log(Level.SEVERE, null, ex);
        }*/
    }
}
