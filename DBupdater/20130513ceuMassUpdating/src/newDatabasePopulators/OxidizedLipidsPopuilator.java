/*
 * OxidizedLipidsPopuilator.java
 *
 * Created on 24-mar-2018, 16:04:17
 *
 * Copyright(c) 2018 Ceu Mass Mediator All Rights Reserved.
 * This software is the proprietary information of Alberto Gil de la Fuente.
 *
 */
package newDatabasePopulators;

import databases.CMMDatabase;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import patternFinders.PatternFinder;
import utilities.Utilities;

/**
 * {Insert class description here}
 *
 * @version $Revision: 1.1.1.1 $
 * @since Build {insert version here} 24-mar-2018
 *
 * @author Alberto Gil de la Fuente
 */
public class OxidizedLipidsPopuilator {

    private PatternFinder pf;

    /**
     * Creates a new instance of OxidizedLipidsPopuilator
     */
    public OxidizedLipidsPopuilator() {
        this.pf = new PatternFinder();
    }

    /**
     *
     * @param fileName
     * @param db
     */
    public void populate(String fileName, CMMDatabase db) {

        String name;
        String mass;
        String formula;
        String sourceData;
        String description ="";
        String oxidationType;
        // TODO IT STARTS IN THIS NUMBER 229 or read from COLUMN IN EXCEL
        // Check wher it starts 

        int inHouseID;
        int compound_id;
        String formulaType = "CHNOPS";
        int compoundType = 1; // For Lipids
        // Expected
        int compoundStatus = 0;
        String codeCategory = "GP";
        String codeMainClass = "GP20";
        String codeSubClass = "GP2001";
        String codeLevel4Class = "";
        String lipidType = "PC(";
        List<String> chains;
        int numChains;
        int numCarbons;
        int numDoubleBonds;
        String oxPC;
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            try {
                while ((oxPC = br.readLine()) != null) {
                    if (oxPC.startsWith("PC")) {
                        String[] attributes = oxPC.split(";");
                        String abbrev = attributes[0];
                        name = attributes[0] + "\n" + attributes[1] + "\n" + attributes[2];
                        name = name.replaceAll("'", "\\'");
                        name = name.replaceAll("\"", "\\'");
                        numCarbons = pf.getNumberOfCarbons(name);
                        numDoubleBonds = pf.getNumberOfDoubleBonds(name);
                        // Updated weight to calculated
                        mass = attributes[3].replace(",",".");
                        String logP = null;
                        formula = attributes[4];
                        sourceData = attributes[5];
                        //description = attributes[6];
                        inHouseID = db.getNextInHouseID();
                        oxidationType = pf.getOxidationFromAbbrev(abbrev);
                        chains = pf.getListOfChains(abbrev);
                        numChains = chains.size();
                        String casID = "NULL";
                        int chargeType = 0;
                        int numCharges = 0;
                        compound_id = db.insertCompound(casID, name, formula, mass, chargeType, numCharges,
                                formulaType, compoundType, compoundStatus, logP);

                        db.insertCompoundInHouse(compound_id, inHouseID, sourceData, description);
                        db.insertLMClassification(compound_id, codeCategory, codeMainClass, codeSubClass, codeLevel4Class);

                        db.insertLipidClassification(compound_id, lipidType, numChains, numCarbons, numDoubleBonds);

                        for (String chain : chains) {
                            int carbonsOfChain = pf.getNumberOfCarbons(chain);
                            int doubleBondsOfChain = pf.getNumberOfDoubleBonds(chain);
                            String oxidationOfChain = pf.getOxidationFromAbbrev(chain);
                            int chain_id = db.getChainID(carbonsOfChain, doubleBondsOfChain, oxidationOfChain);
                            if (chain_id == 0) {
                                double massOfChain = Utilities.calculateMassChain(carbonsOfChain,
                                        doubleBondsOfChain, oxidationOfChain);
                                String formulaOfChain = Utilities.calculateFormulaChain(carbonsOfChain,
                                        doubleBondsOfChain, oxidationOfChain);
                                chain_id = db.insertChain(carbonsOfChain, doubleBondsOfChain, oxidationOfChain,
                                        Double.toString(massOfChain), formulaOfChain);
                            }
                            int repetitions = db.getRepetitionsSameChain(compound_id, chain_id);
                            if (repetitions == 0) {
                                db.insertCompoundChainRelation(compound_id, chain_id, 1);
                            } else {
                                db.updateCompoundChainRepetitions(compound_id, chain_id, (repetitions + 1));
                            }
                        }

                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(FADatabasePopulator.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(FADatabasePopulator.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
