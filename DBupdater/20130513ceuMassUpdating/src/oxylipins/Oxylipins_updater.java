/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oxylipins;

import databases.CMMDatabase;
import databases.ChemSpiderRest;
import exceptions.IonModeAdductException;
import exceptions.WrongRequestException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import static utilities.Utilities.readHeaders;

/**
 *
 * @author alberto.gildelafuent
 */
public class Oxylipins_updater {

    /**
     * Executes the main method for the oxylipins
     *
     * @param args
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {

        CMMDatabase db = new CMMDatabase();
        db.connectToDB("jdbc:mysql://localhost/compounds?useSSL=false&serverTimezone=UTC", "alberto", "alberto");
        String CCSFilePath = "resources/oxylipins/CCS_Oxylipins_Negative.xlsx";

        readCCScompounds(db, CCSFilePath, false);

        CCSFilePath = "resources/oxylipins/CCS_Oxylipins_Positive.xlsx";

        readCCScompounds(db, CCSFilePath, true);

    }

    /**
     *
     * @param db
     * @param CCSFilePath
     * @param isAbundance if the column for abundance is present
     * @throws IOException
     */
    public static void readCCScompounds(CMMDatabase db, String CCSFilePath, boolean isAbundance) throws IOException {

        File excelFile = new File(CCSFilePath);
        // we create an XSSF Workbook object for our XLSX Excel File
        FileInputStream fis = new FileInputStream(excelFile);
        try (// we create an XSSF Workbook object for our XLSX Excel File
                XSSFWorkbook workbook = new XSSFWorkbook(fis)) {

            // we get first sheet
            XSSFSheet sheet = workbook.getSheetAt(0);
            int totalRows = sheet.getPhysicalNumberOfRows();
            System.out.println("READING " + (totalRows - 1) + " CCS compounds");

            Map<String, Integer> mapHeaders = readHeaders(CCSFilePath);

            int ion_mode = 2; // negative ionization mode
            int buffer_gas_id = 1; // N2
            int adduct_id;
            int compound_id;
            // continue from here
            // Create the variables for the CE compound
            Integer cembio_id_code; // ccs_id o the structure
            String compound_name;
            String formula;
            String smiles;
            Double RT;
            String adduct;
            int abundance = 0;
            Double monoisopotic_mass;
            float experimental_CCS;

            // we iterate on rows
            Iterator<Row> rowIt = sheet.iterator();
            // skip the header
            rowIt.next();
            int product_ion_id = 0;
            int lineNumber = 2;
            while (rowIt.hasNext()) {
                //System.out.println("line: " + lineNumber);

                Row row = rowIt.next();
                // iterate on cells for the current row
                Iterator<Cell> cellIterator = row.cellIterator();

                cembio_id_code = (int) cellIterator.next().getNumericCellValue();
                if (cembio_id_code > 0) {
                    compound_name = cellIterator.next().getStringCellValue();
                    formula = cellIterator.next().getStringCellValue();
                    smiles = cellIterator.next().getStringCellValue();
                    RT = (double) cellIterator.next().getNumericCellValue(); // Skip RT
                    adduct = cellIterator.next().getStringCellValue();
                    if (isAbundance) {
                        abundance = (int) cellIterator.next().getNumericCellValue();
                    }
                    monoisopotic_mass = (double) cellIterator.next().getNumericCellValue();
                    cellIterator.next(); // Skip Theoretical m/z
                    cellIterator.next(); // Skip experimental m/z
                    experimental_CCS = (float) cellIterator.next().getNumericCellValue();
                    cellIterator.next(); // Skip STD DEV
                    cellIterator.next(); // Skip RSD
                    try {
                        adduct_id = db.insertAdduct(ion_mode, adduct);

                        // get identifiers INCHI and INCHI KEY
                        String inchi = ChemSpiderRest.getINCHIFromSMILES(smiles);
                        String inchi_key = ChemSpiderRest.getINCHIKeyFromInchi(inchi);
                        compound_id = db.getCompoundIdFromInchi(inchi.trim());
                        if (compound_id == 0) {
                            // add compound, but first check with Maria
                            System.out.println(lineNumber + "\t" + smiles + "\t" + inchi_key + "\t" + inchi);
                            // add compounds that are not present in the database

                        } else {
                            boolean hasCCSValue = db.hasCCSValue(compound_id, adduct_id, buffer_gas_id);
                            if (isAbundance && !hasCCSValue) {
                                db.insertCCSValue(compound_id, adduct_id, buffer_gas_id, experimental_CCS, abundance);
                            } else if (!isAbundance && !hasCCSValue) {
                                db.insertCCSValue(compound_id, adduct_id, buffer_gas_id, experimental_CCS);
                            } else if (!isAbundance && hasCCSValue) {
                                db.updateCCSValue(compound_id, adduct_id, buffer_gas_id, experimental_CCS);
                            } else {
                                db.updateCCSValue(compound_id, adduct_id, buffer_gas_id, experimental_CCS, abundance);
                            }
                        }
                    } catch (IonModeAdductException ex) {
                        Logger.getLogger(Oxylipins_updater.class.getName()).log(Level.SEVERE, null, ex);
                        System.out.println("Line " + lineNumber + " has a mistake in the adduct");
                    } catch (WrongRequestException ex) {
                        System.out.println("Line " + lineNumber + " has a mistake in the structure");
                        Logger.getLogger(Oxylipins_updater.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                lineNumber++;
            }
        }
    }

}
