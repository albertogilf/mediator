package pkgceumassupdating;

//import comprobadoresResultados.LogGenerator;
import CE.MS.CE_library_reader;
import checkers.cas.CheckerCas;
import databases.CMMDatabase;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import newDatabasePopulators.AgilentDatabasePopulator;
import newDatabasePopulators.FADatabasePopulator;
import newDatabasePopulators.HMDBDatabasePopulator;
import newDatabasePopulators.KeggDatabasePopulator;
import newDatabasePopulators.LMDatabasePopulator;
import newDatabasePopulators.MineDatabasePopulator;
import newDatabasePopulators.OxidizedLipidsPopuilator;

/**
 *
 * @author Alberto gil de la Fuente
 * @version: 4.0, 16/01/2019.
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        KeggDatabasePopulator keggDBP = new KeggDatabasePopulator();
        LMDatabasePopulator lmDBP = new LMDatabasePopulator();
        HMDBDatabasePopulator hmdDBP = new HMDBDatabasePopulator();
        FADatabasePopulator faDBP = new FADatabasePopulator();
        OxidizedLipidsPopuilator oxPCs = new OxidizedLipidsPopuilator();
        AgilentDatabasePopulator agilentPopulator = new AgilentDatabasePopulator();
        MineDatabasePopulator minePopulator = new MineDatabasePopulator();

        CMMDatabase db = new CMMDatabase();

        db.connectToDB("jdbc:mysql://localhost/compounds?useSSL=false&serverTimezone=UTC", "alberto", "alberto");
        //db.connectToDB("jdbc:mysql://localhost/compounds", "alberto", "alberto");
        //downloadersOfWebResources.DownloaderOfWebResources.downloadKeggWebResources();
        //hmdDBP.splitHMDBFileintometabolites();
        //classyfire.ClassifyFromClassyFire.insertNodesFromClassyfire(db);
        // Right now the ontologies are taken directly from the HMDB database, with export/import
        //hmdDBP.printOntologies(db);
        //lmDBP.populateFromLM(db);
        //hmdDBP.populateFromHMDB(db);
        //hmdDBP.insertRelationsFromUniChem(db);
        keggDBP.populateFromKegg(db);
        //keggDBP.insertRelationsFromUniChem(db);
        //hmdDBP.updateStatusFromHMDB(db);
        // Insert from agilent
        // Before generate agilent data, delete IUPAC NAME FROM RESULTS!
        // agilentPopulator.populate(db);
        //agilentPopulator.updatePeptides(db);
        // insert oxidized compounds. Should be commented
        // Insert FA
        //faDBP.populate(IN_HOUSE_FA_NAME, db);
        //oxPCs.populate(Constants.OXPC_FILENAME, db);
//        CheckerCas.downloadAllCasIds(db);
/*
        // DEPRECATED: IT DOES NOT UNIFY COMPOUNDS SINCE PREVIOUS STEP IS RELATED!!!
        AgilentUpdateCationsAnions agilentUpdaterCations = new AgilentUpdateCationsAnions();
        agilentUpdaterCations.populate(db);
         */
        //minePopulator.populate(db);
        /*
        try {
            CE_library_reader.readPrecursorIons(db);
            CE_library_reader.readProductIons(db);
            CE_library_reader.readPrecursorIonsFromMSMLS(db);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
*/
        /*
        System.out.println();
        System.out.println("OBTAINING LOGS TO TEST RESULTS");

        // for what it is used the log?

        LogGenerator lg = new LogGenerator();
        lg.obtainLogsToTestResults(db);
        db.closeConnection();
         */
    }
}
