/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package predRT;

import static CE.MS.CE_library_reader.readProductIons;
import databases.CMMDatabase;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * Class to load the data from the SMRT database
 *
 * @author alberto Gil de la Fuente. San Pablo-CEU
 * @version: 4.0 21/08/2020
 */
public class SMRTLoader {

    public static void main(String[] args) {
        CMMDatabase db = new CMMDatabase();
        db.connectToDB("jdbc:mysql://localhost/compounds?useSSL=false&serverTimezone=UTC", "alberto", "alberto");

        try {
            //readPrecursorIons(db);
            insertRT_pred_transformed(db);
        } catch (IOException ex) {
            Logger.getLogger(SMRTLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void insertRT_pred_transformed(CMMDatabase db) throws IOException {
        String RT_pred_path = "resources/RT_pred/CMM_predictions_transformed.csv";
        BufferedReader br = null;
        String line;
        String csvSplitBy = ",";

        br = new BufferedReader(new FileReader(RT_pred_path));
        int lineNumber = 0;
        // skip header
        br.readLine();
        while ((line = br.readLine()) != null) {
            // Create the variables for the CE compound
            String pc_id;
            Integer compound_id;
            Double RT_pred;
            Double RT_pred_transformed;

            if (line.endsWith(csvSplitBy)) {
                line = line + " ;";
            }
            String[] compound = line.split(csvSplitBy);
            pc_id = compound[0];
            try {
                compound_id = Integer.parseInt(compound[1]);
                try {
                    RT_pred = Double.parseDouble(compound[2]);
                    RT_pred_transformed = Double.parseDouble(compound[3]);
                    db.updateRT_pred_transformed(compound_id, RT_pred_transformed);
                } catch (NumberFormatException e) {
                    System.out.println("What the fuck!! ERror in line: " + lineNumber + " RT NOT PREDICTED");
                }
            } catch (NumberFormatException e) {
                // if there is no chebid
                compound_id = null;
                System.out.println("What the fuck!! ERror in line: " + lineNumber + " COMPOUND_ID NOT FOUND");
            }

        }

    }
}
