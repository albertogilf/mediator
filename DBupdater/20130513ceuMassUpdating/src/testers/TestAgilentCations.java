/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testers;

import databases.CMMDatabase;
import newDatabasePopulators.AgilentDatabasePopulator;

/**
 *
 * @author alberto
 */
public class TestAgilentCations {
    
    public static void main(String[] args) {
        
        AgilentDatabasePopulator agilentPopulator = new AgilentDatabasePopulator();
        CMMDatabase db = new CMMDatabase();
        // db.connectToDB("jdbc:mysql://localhost/BKcompounds_new", "alberto", "alberto");
        db.connectToDB("jdbc:mysql://localhost/compounds_new?useSSL=false", "alberto", "alberto");
        //dwr.downloadHMDBXMLFile("HMDB00305");
        agilentPopulator.saveInfoFromCationsIntoCSV(db);
        
        
        //db.getInChIKeyFromCompound(81641);
    }
}
