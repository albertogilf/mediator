/*
 * TestClassyfire.java
 *
 * Created on 30-abr-2018, 22:35:10
 *
 * Copyright(c) 2018 Ceu Mass Mediator All Rights Reserved.
 * This software is the proprietary information of Alberto Gil de la Fuente.
 *
 */

package testers;

import databases.CMMDatabase;

/**
 * To insert the nodes from classyfire
 * 
 * @version $Revision: 1.1.1.1 $
 * @since Build {insert version here} 30-abr-2018
 * 
 * @author Alberto Gil de la Fuente
 */
public class TestClassyfire {

    public static void main(String[] args) {
        CMMDatabase db = new CMMDatabase();
        db.connectToDB("jdbc:mysql://localhost/compounds?useSSL=false&serverTimezone=UTC", "alberto", "alberto");
        classyfire.ClassifyFromClassyFire.insertNodesFromClassyfire(db);
    }
}
