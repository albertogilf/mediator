/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testers;

import databases.CMMDatabase;
import newDatabasePopulators.HMDBDatabasePopulator;

/**
 *
 * @author alberto
 */
public class TestHMDBParser {

    public static void main(String[] args) {
        HMDBDatabasePopulator hmdbdp = new HMDBDatabasePopulator();
        CMMDatabase db = new CMMDatabase();
        // db.connectToDB("jdbc:mysql://localhost/BKcompounds_new", "alberto", "alberto");
        db.connectToDB("jdbc:mysql://localhost/compounds?useSSL=false&serverTimezone=UTC", "alberto", "alberto");
        //hmdbdp.insertRelationsFromUniChem(db);
        downloadersOfWebResources.DownloaderOfWebResources.downloadCLASSYFIREJSONFile("aaa");
        downloadersOfWebResources.DownloaderOfWebResources.downloadCLASSYFIREJSONFile("BTIJJDXEELBZFS-MVOJXMGJSA-K");
        hmdbdp.populateFromADownloadedFile("HMDB0038846.xml",db);
        //db.getInChIKeyFromCompound(81641);
    }
}
