/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testers;

import checkers.cas.CheckerCas;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import databases.CMMDatabase;
import downloadersOfWebResources.DownloaderOfWebResources;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import newDatabasePopulators.HMDBDatabasePopulator;
import newDatabasePopulators.KeggDatabasePopulator;
import newDatabasePopulators.LMDatabasePopulator;
import utilities.Constants;

/**
 *
 * @author alberto
 */
public class TestLipidMaps {

    public static void main(String[] args) {

        LMDatabasePopulator lmdp = new LMDatabasePopulator();
        HMDBDatabasePopulator hmdbp = new HMDBDatabasePopulator();
        KeggDatabasePopulator kdbp = new KeggDatabasePopulator();
        CMMDatabase db = new CMMDatabase();
        db.connectToDB("jdbc:mysql://localhost/compounds?useSSL=false&serverTimezone=UTC", "alberto", "alberto");
        //HMDBDatabasePopulator hmdDBP = new HMDBDatabasePopulator();
        //hmdDBP.populateFromHMDB(db);
        hmdbp.populateFromADownloadedFile("HMDB0062451.xml",db);
        /*
        List<String> WrongSMILES = listWrongSMILES();
        for (String kegg_id : WrongSMILES) {
            kdbp.populateFromADownloadedFile(kegg_id + ".txt" , db);
        }
        /*

        List<String> wrongClassified = listWrongClassified();
        for (String hmdb_id : wrongClassified) {
            hmdbp.populateFromADownloadedFile(hmdb_id + ".xml", db);
        }

        //List<String> wrongDownloaded = listWrongClassified();
        
        List<String> wrongDownloaded = listWrongDownloaded();
        for (String lm_id : wrongDownloaded) {
            lmdp.populateFromAJSONFile(lm_id, db);
        }
         */
 /*
        lmdp.populateFromAJSONFile("LMST02020028", db);
        lmdp.populateFromAJSONFile("LMGP02010000", db);
        lmdp.populateFromAJSONFile("LMGP01030016", db);
        lmdp.populateFromAJSONFile("LMGP01030017", db);
        lmdp.populateFromAJSONFile("LMFA01010024", db);
        lmdp.populateFromAJSONFile("LMGP01070008", db);
        lmdp.populateFromAJSONFile("LMGP01020269", db);
        lmdp.populateFromAJSONFile("LMGP01010003", db);

         */
    }

    public static List<String> listWrongSMILES() {
        List<String> wrongdownloaded = new LinkedList<String>();
        wrongdownloaded.add("C21831");

        return wrongdownloaded;
    }

    public static List<String> listWrongDownloaded() {
        List<String> wrongdownloaded = new LinkedList<String>();
        wrongdownloaded.add("LMSP0601FV04");
        wrongdownloaded.add("LMSP0601FV05");
        wrongdownloaded.add("LMSP0601GH04");
        wrongdownloaded.add("LMSP0601GH05");
        wrongdownloaded.add("LMSP0601GH06");
        wrongdownloaded.add("LMSP0601GH07");
        wrongdownloaded.add("LMSP0601GH08");
        wrongdownloaded.add("LMSP0601GI00");
        wrongdownloaded.add("LMSP0601GI01");
        wrongdownloaded.add("LMSP0601GI02");
        wrongdownloaded.add("LMSP0601GI03");
        wrongdownloaded.add("LMSP0601GI04");
        wrongdownloaded.add("LMSP0601GR01");
        wrongdownloaded.add("LMST03020637");
        wrongdownloaded.add("LMST05010036");

        return wrongdownloaded;
    }

    public static List<String> listWrongClassified() {
        List<String> wrongClassified = new LinkedList<String>();

        wrongClassified.add("HMDB0036080");
        wrongClassified.add("HMDB0036087");
        wrongClassified.add("HMDB0036088");
        wrongClassified.add("HMDB0031617");
        wrongClassified.add("HMDB0141081");
        wrongClassified.add("HMDB0005084");
        wrongClassified.add("HMDB0000201");
        wrongClassified.add("HMDB0002363");
        wrongClassified.add("HMDB0036575");
        wrongClassified.add("HMDB0140629");
        wrongClassified.add("HMDB0030071");
        wrongClassified.add("HMDB0000624");
        wrongClassified.add("HMDB0000746");
        wrongClassified.add("HMDB0034972");
        wrongClassified.add("HMDB0041628");
        wrongClassified.add("HMDB0035764");
        wrongClassified.add("HMDB0034928");
        wrongClassified.add("HMDB0036076");
        wrongClassified.add("HMDB0036113");
        wrongClassified.add("HMDB0036114");
        wrongClassified.add("HMDB0012534");
        wrongClassified.add("HMDB0062296");
        wrongClassified.add("HMDB0062293");
        wrongClassified.add("HMDB0032797");
        wrongClassified.add("HMDB0035140");
        wrongClassified.add("HMDB0004321");
        wrongClassified.add("HMDB0003375");
        wrongClassified.add("HMDB0036792");
        wrongClassified.add("HMDB0061779");
        wrongClassified.add("HMDB0003415");
        wrongClassified.add("HMDB0034697");
        wrongClassified.add("HMDB0035087");
        wrongClassified.add("HMDB0062514");
        wrongClassified.add("HMDB0032301");
        wrongClassified.add("HMDB0036188");
        wrongClassified.add("HMDB0037729");
        wrongClassified.add("HMDB0032268");
        wrongClassified.add("HMDB0005079");
        wrongClassified.add("HMDB0011134");
        wrongClassified.add("HMDB0004699");
        wrongClassified.add("HMDB0004679");
        wrongClassified.add("HMDB0004670");
        wrongClassified.add("HMDB0004243");
        wrongClassified.add("HMDB0004692");
        wrongClassified.add("HMDB0032413");
        wrongClassified.add("HMDB0112099");
        wrongClassified.add("HMDB0031127");
        wrongClassified.add("HMDB0032396");
        wrongClassified.add("HMDB0000222");
        wrongClassified.add("HMDB0010737");
        wrongClassified.add("HMDB0031057");
        wrongClassified.add("HMDB0010734");
        wrongClassified.add("HMDB0010731");
        wrongClassified.add("HMDB0010728");
        wrongClassified.add("HMDB0059633");
        wrongClassified.add("HMDB0010725");
        wrongClassified.add("HMDB0003876");
        wrongClassified.add("HMDB0062294");
        wrongClassified.add("HMDB0061708");
        wrongClassified.add("HMDB0004667");
        wrongClassified.add("HMDB0002752");
        wrongClassified.add("HMDB0005844");
        wrongClassified.add("HMDB0005083");
        wrongClassified.add("HMDB0001220");
        wrongClassified.add("HMDB0002710");
        wrongClassified.add("HMDB0001403");
        wrongClassified.add("HMDB0010722");
        wrongClassified.add("HMDB0002283");
        wrongClassified.add("HMDB0013716");
        wrongClassified.add("HMDB0010718");
        wrongClassified.add("HMDB0000452");
        wrongClassified.add("HMDB0000650");
        wrongClassified.add("HMDB0062222");
        wrongClassified.add("HMDB0143635");
        wrongClassified.add("HMDB0061051");
        wrongClassified.add("HMDB0131137");
        wrongClassified.add("HMDB0000955");
        wrongClassified.add("HMDB0136156");
        wrongClassified.add("HMDB0040801");
        wrongClassified.add("HMDB0032655");
        wrongClassified.add("HMDB0135786");
        wrongClassified.add("HMDB0125096");
        wrongClassified.add("HMDB0000954");
        wrongClassified.add("HMDB0138526");
        wrongClassified.add("HMDB0034315");
        wrongClassified.add("HMDB0131141");
        wrongClassified.add("HMDB0041748");
        wrongClassified.add("HMDB0127955");
        wrongClassified.add("HMDB0029200");
        wrongClassified.add("HMDB0033096");
        wrongClassified.add("HMDB0137293");
        wrongClassified.add("HMDB0136158");
        wrongClassified.add("HMDB0037440");
        wrongClassified.add("HMDB0127490");
        wrongClassified.add("HMDB0035484");
        wrongClassified.add("HMDB0032616");
        wrongClassified.add("HMDB0127488");
        wrongClassified.add("HMDB0124869");
        wrongClassified.add("HMDB0002511");
        wrongClassified.add("HMDB0130855");
        wrongClassified.add("HMDB0030439");
        wrongClassified.add("HMDB0132949");
        wrongClassified.add("HMDB0030904");
        wrongClassified.add("HMDB0002040");
        wrongClassified.add("HMDB0135651");
        wrongClassified.add("HMDB0135643");
        wrongClassified.add("HMDB0032611");
        wrongClassified.add("HMDB0135738");
        wrongClassified.add("HMDB0032586");
        wrongClassified.add("HMDB0134088");
        wrongClassified.add("HMDB0037644");
        wrongClassified.add("HMDB0030394");
        wrongClassified.add("HMDB0125513");
        wrongClassified.add("HMDB0000317");
        wrongClassified.add("HMDB0035842");
        wrongClassified.add("HMDB0035820");
        wrongClassified.add("HMDB0035093");
        wrongClassified.add("HMDB0035094");
        wrongClassified.add("HMDB0002299");
        wrongClassified.add("HMDB0002166");
        wrongClassified.add("HMDB0000023");
        wrongClassified.add("HMDB0000336");
        wrongClassified.add("HMDB0062596");
        wrongClassified.add("HMDB0000410");
        wrongClassified.add("HMDB0000351");
        wrongClassified.add("HMDB0000642");
        wrongClassified.add("HMDB0013130");
        wrongClassified.add("HMDB0135598");
        wrongClassified.add("HMDB0003066");
        wrongClassified.add("HMDB0060459");
        wrongClassified.add("HMDB0060518");
        wrongClassified.add("HMDB0003501");
        wrongClassified.add("HMDB0125102");
        wrongClassified.add("HMDB0041706");
        wrongClassified.add("HMDB0127962");
        wrongClassified.add("HMDB0127985");
        wrongClassified.add("HMDB0124975");
        wrongClassified.add("HMDB0002035");
        wrongClassified.add("HMDB0128076");
        wrongClassified.add("HMDB0128078");
        wrongClassified.add("HMDB0000930");
        wrongClassified.add("HMDB0002641");
        wrongClassified.add("HMDB0134028");
        wrongClassified.add("HMDB0030997");
        wrongClassified.add("HMDB0039612");
        wrongClassified.add("HMDB0135588");
        wrongClassified.add("HMDB0040986");
        wrongClassified.add("HMDB0135648");
        wrongClassified.add("HMDB0134617");
        wrongClassified.add("HMDB0033295");
        wrongClassified.add("HMDB0134039");
        wrongClassified.add("HMDB0031725");
        wrongClassified.add("HMDB0037944");
        wrongClassified.add("HMDB0029255");
        wrongClassified.add("HMDB0135644");
        wrongClassified.add("HMDB0003654");
        wrongClassified.add("HMDB0029697");
        wrongClassified.add("HMDB0029698");
        wrongClassified.add("HMDB0030822");
        wrongClassified.add("HMDB0133035");
        wrongClassified.add("HMDB0133036");
        wrongClassified.add("HMDB0129241");
        wrongClassified.add("HMDB0129242");
        wrongClassified.add("HMDB0133051");
        wrongClassified.add("HMDB0133053");
        wrongClassified.add("HMDB0129260");
        wrongClassified.add("HMDB0129258");
        wrongClassified.add("HMDB0126252");
        wrongClassified.add("HMDB0126253");
        wrongClassified.add("HMDB0126306");
        wrongClassified.add("HMDB0126299");
        wrongClassified.add("HMDB0126300");
        wrongClassified.add("HMDB0126293");
        wrongClassified.add("HMDB0126303");
        wrongClassified.add("HMDB0126296");
        wrongClassified.add("HMDB0126304");
        wrongClassified.add("HMDB0126297");
        wrongClassified.add("HMDB0126294");
        wrongClassified.add("HMDB0126301");
        wrongClassified.add("HMDB0126295");
        wrongClassified.add("HMDB0126302");
        wrongClassified.add("HMDB0126305");
        wrongClassified.add("HMDB0126298");
        wrongClassified.add("HMDB0133098");
        wrongClassified.add("HMDB0133100");

        return wrongClassified;
    }
}
