/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testers;

import checkers.cas.CheckerCas;
import databases.CMMDatabase;
import java.util.LinkedList;
import java.util.List;
import newDatabasePopulators.HMDBDatabasePopulator;
import newDatabasePopulators.KeggDatabasePopulator;
import newDatabasePopulators.LMDatabasePopulator;
import patternFinders.PatternFinder;

/**
 *
 * @author alberto
 */
public class TestUpdate {

    private static void testKeggInsert(CMMDatabase db) {
        KeggDatabasePopulator keggDBP;
        keggDBP = new KeggDatabasePopulator();
        keggDBP.populateFromADownloadedFile("C03799.txt", db);
        keggDBP.populateFromADownloadedFile("C01501.txt", db);
        keggDBP.populateFromADownloadedFile("C01502.txt", db);
    }
    
    private static void testLMInsert(CMMDatabase db) {
        LMDatabasePopulator lmdp = new LMDatabasePopulator();
        lmdp.populateFromADownloadedFile("test.test",db);
    }
    
    private static void testHMDBInsert(CMMDatabase db) {
        
        HMDBDatabasePopulator hmdbdp = new HMDBDatabasePopulator();
        hmdbdp.populateFromADownloadedFile("HMDB00115.xml", db);
        hmdbdp.populateFromADownloadedFile("HMDB11627.xml", db);
    }
    
    private static void testCasInsert(CMMDatabase db)
    {
        List<String> casIdTest = new LinkedList();
        casIdTest.add("25090-73-7");
        casIdTest.add("9009-58-9");
        casIdTest.add("41295-10-2");
        casIdTest.add("69239-55-0");
        casIdTest.add("3416-24-8");
        casIdTest.add("58-27-5");
        casIdTest.add("18542-37-5");
        casIdTest.add("157078-48-3");
        casIdTest.add("57-13-6");
        casIdTest.add("288-32-4");
        casIdTest.add("5147-00-2");
        casIdTest.add("17090-79-8");

        for (String casId : casIdTest) {
            CheckerCas.updateCasId(db, casId);
            //checker.getFormulaCasId(casId);
            //checker.getWeightCasId(casId);
            //checker.getInChICasId(casId);
            //checker.getInChIKeyCasId(casId);
            // Still to do the method to retrieve Smiles of the compound
            //checker.getSmilesCasId(casId);
            //checker.insertCasId(db, casId);
        }
    }
    
    private static void testFeaturesLM()
    {
        PatternFinder pf = new PatternFinder();
        //String content = "PT(18:0/18:1(9Z))";
        //String content = "MIPC(t18:0/16:0)";
        //String content = "PC(P-08:0/18:2(9Z,12Z))";
        //String content = "PI-Cer(t20:0/26:0)";
        //String content = "(+)-cis-(S)-allethrin";
        //String content = "LysoPC(20:4(8Z,11Z,14Z,17Z))";
        String content = " PC(P-18:0/19:0)";
        //String content = "";
        pf.getLipidType(content);
        pf.getNumberOfCarbons(content);
        pf.getNumberOfDoubleBonds(content);
         
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        CMMDatabase db = new CMMDatabase();
        db.connectToDB("jdbc:mysql://localhost/compounds?useSSL=false&serverTimezone=UTC", "alberto", "alberto");

        // Check Download of CasIds
        testCasInsert(db);

        //Testing keggDB
        testKeggInsert(db);
        
        //Testing HMDB
        testHMDBInsert(db);

        //Testing LM
        testLMInsert(db);
    }

}
