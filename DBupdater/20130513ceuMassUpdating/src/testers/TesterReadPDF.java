/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testers;

import java.io.*;
import org.apache.pdfbox.pdmodel.*;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.apache.pdfbox.util.*;

/**
 *
 * @author alberto
 */
public class TesterReadPDF {

    public static void main(String[] args) throws IOException {

        try {
            PDDocument document = null;
            document = PDDocument.load(new File("/home/alberto/alberto/repo/mediator/joanna_tables/Metabolites.pdf"));
            document.getClass();
            if (!document.isEncrypted()) {
                PDFTextStripperByArea stripper = new PDFTextStripperByArea();
                stripper.setSortByPosition(true);
                PDFTextStripper Tstripper = new PDFTextStripper();
                String st = Tstripper.getText(document);
                System.out.println("Text:" + st);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
