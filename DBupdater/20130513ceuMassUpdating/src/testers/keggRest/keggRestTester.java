/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testers.keggRest;

/**
 *
 * @author USUARIO
 */
import ioDevices.UriDevice;
import java.io.FileNotFoundException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import newDatabasePopulators.LMDatabasePopulator;
import utilities.Checker;

public class keggRestTester {

    /*
    // See http://www.kegg.jp/kegg/docs/keggapi.html
    
    public static void main(String[] args)
    {
        try {
            UriDevice ud = new UriDevice();
          
            //System.out.println(ud.read("http://rest.kegg.jp/find/compound/300-310/mol_weight"));
            System.out.println(ud.read("http://rest.kegg.jp/get/cpd:C00001"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(keggRestTester.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnknownHostException ex) {
            Logger.getLogger(keggRestTester.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
     */
    public static void main(String[] args) {
        //LMDatabasePopulator lmdp = new LMDatabasePopulator();
        //lmdp.getInChI("LMPR0102010010");
        Checker.debugFormula("C19H15BN.Na");
        /*
        Checker.hasFormulaNoElements("C19H15NaB(Na)");
        Checker.hasFormulaNoElements("(C12H16O15S2R2)n");
        Checker.hasFormulaNoElements("C23H16O6.(C11H14ClN5)2");
        Checker.hasFormulaNoElements("C9H8O4.C2H4NO2.CO3.Al.Mg.2OH");
        Checker.hasFormulaNoElements("C28H46N5O29P5R2(R1)(C5H8O6PR)n");
        Checker.hasFormulaNoElements("C11H16N5O8P(C5H8O6PR)n(C5H8O6PR)n");
         */
    }
}
