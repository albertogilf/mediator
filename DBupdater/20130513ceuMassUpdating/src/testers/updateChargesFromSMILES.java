/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testers;

import checkers.cas.CheckerCas;
import databases.CMMDatabase;
import newDatabasePopulators.FADatabasePopulator;

/**
 *
 * @author alberto
 */
public class updateChargesFromSMILES {

    public static void main(String[] args) {

        CMMDatabase db = new CMMDatabase();

        db.connectToDB("jdbc:mysql://localhost/compounds?useSSL=false&serverTimezone=UTC", "alberto", "alberto");
        for (int i = 0; i < 180836; i++) {
            String SMILES = db.getSMILESFromCompound(i);
            if (!"".equals(SMILES) && !"null".equals(SMILES)) {
                int[] charges = patternFinders.PatternFinder.getChargeFromSmiles(SMILES);
                int chargeType = charges[0];
                int chargeNumber = charges[1];
                //System.out.println(SMILES + " TYPE: " + chargeType + " NUMBER: " + chargeNumber);
                db.updateCompoundCharge(i, chargeType, chargeNumber);
            }
        }

    }
}
