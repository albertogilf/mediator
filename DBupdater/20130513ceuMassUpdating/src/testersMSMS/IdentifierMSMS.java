/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testersMSMS;

import databases.CMMDatabase;
import static java.lang.Math.abs;
import static java.lang.Math.sqrt;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import static utilities.Constants.PROTON_WEIGTH;

/**
 *
 * @author María 606888798
 */
public class IdentifierMSMS {

    private static double MZEXP = 3d;//3
    private static double INTEXP = 0.6d;//0.6

    /**
     * 
     * @param m
     * @param parentIonMass
     * @return parentIonMass modified acording to the ionization mode
     */
    public static double generateParentIonMass(Msms m, double parentIonMass) {
        //if adduct==M+H
        if (m.getIonization_mode()==1) {
            return parentIonMass + PROTON_WEIGTH;
        } else if (m.getIonization_mode()==0) {
            return parentIonMass - PROTON_WEIGTH;
        }
        //in the future we will do: if adduct==M+Na, if adduct==M+K... ect
        return parentIonMass;
    }

    

    /**
     *
     * @param inputMsms
     * @param db
     * @param parentIonTolerance
     * @return a candidates list of msms, depending on the parent ion mass: The parent ion mass will depend on the precursor mass and the torerance
     */
    public static List<Msms> getMatchedMsmsByParentIon(Msms inputMsms, CMMDatabase db, double parentIonTolerance) {
        List<Msms> libraryMsmsList = new LinkedList<>();
        try {
            String sql, sql1;
            int msms_id;
            inputMsms.setPrecursorMass();
            //System.out.println(inputMsms.getPrecursor());
            //Peak libraryPeak= new Peak();
            //todo: seleccionar los parametros que vayamos a usar. Creo que todos los datos cualitativos del msms pueden servir y en un futuro alomejor la variación de la masa del parent ion
           if(inputMsms.getPredicted()==2)
            {
                sql = "SELECT msms.msms_id,msms.hmdb_id, msms.voltage, msms.voltage_level, msms.ionization_mode,msms.compound_id, msms.predicted, "
                    + "compounds.compound_name, compounds.formula, compounds.mass FROM msms "
                    + "JOIN compounds WHERE msms.compound_id= compounds.compound_id AND compounds.mass "
                    + "BETWEEN "+(inputMsms.getPrecursor() - parentIonTolerance)+" and "+(inputMsms.getPrecursor() + parentIonTolerance)
                    +"and msms.voltage='"+inputMsms.getVoltage_level().toLowerCase()+"' and msms.ionization_mode="
                    +inputMsms.getIonization_mode();
            }
            else{
             sql = "SELECT msms.msms_id,msms.hmdb_id, msms.voltage, msms.voltage_level, msms.ionization_mode,msms.compound_id, msms.predicted, "
                    + "compounds.compound_name, compounds.formula, compounds.mass FROM msms "
                    + "JOIN compounds WHERE msms.compound_id= compounds.compound_id AND compounds.mass "
                    + "BETWEEN "+(inputMsms.getPrecursor() - parentIonTolerance)+" and "+(inputMsms.getPrecursor() + parentIonTolerance)
                    +" and msms.predicted="+inputMsms.getPredicted();
            }
            ResultSet rs;
            PreparedStatement st;
            st = db.getConnection().prepareStatement(sql);
            
            rs = st.executeQuery(sql);

            while (rs.next()) {
                Msms libraryMsms = new Msms();//fresh instance
                msms_id = rs.getInt("msms_id");
                libraryMsms.setId(msms_id);
                libraryMsms.setHmdb_id(rs.getString("hmdb_id"));
                libraryMsms.setVoltage(rs.getInt("voltage"));
                libraryMsms.setVoltage_level(rs.getString("voltage_level"));
                //libraryMsms.setInstrument_type(rs.getString("instrument_type"));
                libraryMsms.setIonizationMode(rs.getInt("ionization_mode"));
                //libraryMsms.setPeak_count(rs.getInt("peak_count"));
                libraryMsms.setCompound_id(rs.getInt("compound_id"));
                libraryMsms.setPredicted(rs.getInt("predicted"));
                libraryMsms.setCompound_name(rs.getString("compound_name"));
                libraryMsms.setCompound_formula(rs.getString("formula"));
                libraryMsms.setCompound_mass(rs.getDouble("mass"));
                //know for each msms i will add the peaks
                sql1 = "SELECT msms_peaks.* FROM peaks JOIN msms WHERE msms.msms_id=msms_peaks.msms_id AND peaks.msms_id= "+msms_id;
                //st1=db.getConnection().createStatement();
                //rs1=st1.executeQuery(sql1);
                libraryMsms.addPeaks(getPeakFromDataBase(sql1, db));
                /*    sql1="SELECT peaks.* FROM peaks JOIN msms WHERE msms.msms_id=peaks.msms_id AND peaks.msms_id="+msms_id;
                    rs1=db.getResultSet(sql1);
                    while(rs1.next())
                    {
                        libraryPeak.setId(rs1.getInt("peak_id"));
                        libraryPeak.setIntensity((float) rs1.getDouble("intensity"));
                        libraryPeak.setMz((float) rs1.getDouble("mz"));
                        libraryPeak.setMsms_id(msms_id);
                        libraryMsms.addPeak(libraryPeak);
                    }*/
                //System.out.println(libraryMsms.toString());
                libraryMsmsList.add(libraryMsms);
            }

        } catch (SQLException ex) {
            Logger.getLogger(IdentifierMSMS.class.getName()).log(Level.SEVERE, null, ex);
        }
        //scoreMatchedPeaks (inputMsms, libraryMsmsList ,mzTolerance, intTolerance);
        return libraryMsmsList;

    }

    private static List<Peak> getPeakFromDataBase(String sql1, CMMDatabase db) {
        List<Peak> libraryPeakList = new LinkedList<>();

        try {
            PreparedStatement prep = db.getConnection().prepareStatement(sql1);
            
            ResultSet rs1 = prep.executeQuery(sql1);

            while (rs1.next()) {
                Peak libraryPeak = new Peak();//fresh instance
                libraryPeak.setId(rs1.getInt("peak_id"));
                libraryPeak.setIntensity((float) rs1.getDouble("intensity"));
                libraryPeak.setMz((float) rs1.getDouble("mz"));
                libraryPeak.setMsms_id(rs1.getInt("msms_id"));
                //System.out.println(libraryPeak.toString());
                libraryPeakList.add(libraryPeak);
            }
        } catch (SQLException ex) {
            Logger.getLogger(IdentifierMSMS.class.getName()).log(Level.SEVERE, null, ex);
        }
        return libraryPeakList;
    }

    //todo: añadir un atributo score a los Msms??
    //scoring as in myCompoundID
    /**
     *
     * @param inputMsms the original MSMS introduced by the user
     * @param candidatesList The cadidates obtained by getMatchedMsmsByParentIon
     * @param mzTolerance the tolerance for the peaks (mz)
     */
    public static List <Msms> scoreMatchedPeaks(Msms inputMsms, List<Msms> candidatesList, double mzTolerance) {
        Iterator candidates_iterator = candidatesList.iterator();
        inputMsms.normalizeIntensities();
        List <Msms> scored= new LinkedList<>();
        List<Peak> matchedpeaks = new LinkedList<>();
        while (candidates_iterator.hasNext()) {
            Msms msmsToScore = (Msms) candidates_iterator.next();
            //to ensure that the intensities are relative
            msmsToScore.normalizeIntensities();
            //Peak basePeak= msmsToScore.getBasePeak();
            List<Peak> candidatePeaks = msmsToScore.getPeaks();
            Collections.sort(candidatePeaks);//sorted by mz
            Iterator candidatePeaks_iterator = candidatePeaks.iterator();//iterator of the peaks of the candidates
            //I will compare each input Peak with the library peaks 
            List<Peak> inputPeaks = inputMsms.getPeaks();
            Collections.sort(inputPeaks);
            ListIterator inputPeaks_iterator = inputPeaks.listIterator();
            matchedpeaks = new LinkedList<>();
            while (inputPeaks_iterator.hasNext()) {
                Peak inputPeak = (Peak) inputPeaks_iterator.next();
                while (candidatePeaks_iterator.hasNext()) {

                    Peak match = (Peak) candidatePeaks_iterator.next();
                    
                    if (inputPeak.peakMatch(match, mzTolerance)) {
                        matchedpeaks.add(match);
                        break;
                    }
                    
                }
            }
            if (!matchedpeaks.isEmpty()) {
                
                msmsToScore.setScore(scoreMsmsAsMyCompound(matchedpeaks, inputMsms));

                scored.add(msmsToScore);
            }
        }
        return scored;

    }

    private static double scoreMsmsAsMyCompound(List<Peak> matchedpeaks, Msms inputMsms) {
        //at my compound an initial weight is obtained from the dot product between the matched peaks (intensities and mz)
        double score = dotProduct(matchedpeaks, 1, 1);
        double fitScore = score / dotProduct(inputMsms.getPeaks(), 1, 1);
        return fitScore;
    }

    private static double dotProduct(List<Peak> matchedpeaks, double mzExp, double intExp) {
        Iterator it = matchedpeaks.iterator();
        double score = 0;
        while (it.hasNext()) {
            Peak p = (Peak) it.next();
            score += Math.pow(p.getMz(), mzExp) * Math.pow(p.getIntensity(), intExp);
        }
        return score;
    }

    public static List<Msms> getTopNMatches(List<Msms> MsmsList, int N)//N is the rank of best matches
    {

        Collections.sort(MsmsList, new ScoreComparator());
        Iterator it = MsmsList.iterator();
        List<Msms> topN = new LinkedList<>();
        List<Integer> topNCompound_id = new LinkedList<>();
        while (it.hasNext()) {
            Msms msms = (Msms) it.next();
            if (N > 0 && msms.getScore() > 0) {

                int n = msms.getCompound_id();
                if (!topNCompound_id.contains(n)) {
                    topNCompound_id.add(n);
                    topN.add(msms);
                    N--;
                }

            }
        }
        return topN;
    }

    private static double scoreMsmsAsMetFrag(List<Peak> matchedpeaks, Msms inputMsms) {
        //at metfrag an initial weight is obtained from the weighted dot product between the matched peaks (intensities and mz)
        double score = dotProduct(matchedpeaks, MZEXP, INTEXP);
        double fitScore = score / dotProduct(inputMsms.getPeaks(), MZEXP, INTEXP);
        
        return fitScore;
    }

    public static double scoreMsmsByEuclideanDistance(List<Peak> matchedpeaks, Msms inputMsms, double mzTolerance) {
        double score;
        //the score is preformed as the euclidean distance between the peaks.
        double sumTosqrt = 0d;//euclidean distance berfore squared root
        Collections.sort(matchedpeaks);
        
           int n=matchedpeaks.size();
        Iterator it = matchedpeaks.iterator();
        List<Peak> inputPeaks = inputMsms.getPeaks();
        Collections.sort(inputPeaks);//sorted by mz
        if (matchedpeaks.size() == inputPeaks.size()) {
            for (Peak input : inputPeaks) {
                while (it.hasNext()) {
                    Peak candidate = (Peak) it.next();
                    if (candidate.peakMatch(input, mzTolerance)) {
                        
                        double mz = Math.pow(candidate.getMz() - input.getMz(), 2);
                        double inte = Math.pow((candidate.getIntensity() - input.getIntensity()), 2);
                        sumTosqrt += mz + inte;
                        break;
                    }
                }
            }
        } else if (matchedpeaks.size() < inputPeaks.size()) {
            List<Peak> matchedInput = getMatchFromInput(matchedpeaks, inputPeaks);
            Collections.sort(matchedInput);
            Collections.sort(matchedpeaks);
            Iterator input = matchedInput.iterator();
            Iterator lib = matchedpeaks.iterator();

            while (input.hasNext()) {
                Peak ip = (Peak) input.next();
                while (lib.hasNext()) {

                    Peak lp = (Peak) lib.next();
                    if (lp.peakMatch(ip, mzTolerance)) {
                        //System.out.println("MZ: "+lp.getMz()+ "-"+ ip.getMz()+"="+(lp.getMz() - ip.getMz()));
                        //System.out.println("INTENSITY: "+lp.getIntensity() +"-"+ ip.getIntensity()+"="+(lp.getIntensity() - ip.getIntensity()));
                        double mz = Math.pow(lp.getMz() - ip.getMz(), 2);
                        double inte = Math.pow((lp.getIntensity() - ip.getIntensity()), 2);
                        sumTosqrt += mz + inte;
                        break;
                    }
                    //d = Math.sqrt(mz + inte);
                    //score += d;
                }
            }
        }
        score = Math.sqrt(sumTosqrt);

        
        return 1/score;
    }

    private static List<Peak> getMatchFromInput(List<Peak> matchedpeaks, List<Peak> inputPeaks) {
        List<Peak> inputMatch = new LinkedList<>();
        Peak inMatchP = new Peak();
        Iterator itLib = matchedpeaks.iterator();
        Iterator itIn = inputPeaks.iterator();
        Double min = 10d;//por ejemplo
        while (itLib.hasNext()) {
            Peak pLib = (Peak) itLib.next();
            while (itIn.hasNext()) {
                Double diff;
                Peak pIn = (Peak) itIn.next();
                diff = abs(pLib.getMz() - pIn.getMz());
                if (diff < min) {
                    min = diff;
                    inMatchP = pIn;
                }
            }
            inputMatch.add(inMatchP);
        }
        return inputMatch;
    }



  

}
