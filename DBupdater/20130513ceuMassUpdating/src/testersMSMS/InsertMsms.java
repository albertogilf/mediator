/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testersMSMS;

import databases.CMMDatabase;
import downloadersOfWebResources.DownloaderOfWebResources;
import ioDevices.MyFile;
import ioDevices.UriDevice;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import patternFinders.PatternFinder;
import utilities.Constants;
import utilities.Utilities;

//connection, statetment and rs are the variables in class database
/**
 *
 * @author María 606888798
 */
public class InsertMsms {

    public static void insertMsmsList(CMMDatabase db, List<Msms> msms) {

        Msms m, msyn;
        Iterator it = msms.iterator();

        while (it.hasNext()) {
            m = (Msms) it.next();
            //case 1: compound id is different to zero, since the hmdb_id corresponding to the msms is in the ddbb
            if (m.getCompound_id() != 0) {
                if (ableToInsert(m, db)) {//check if the msms is already in the ddbb, if it is not in the ddbb we insert it, if it is, we 
                    //don't do anything
                    insertMsms(m, db);
                    System.out.println("Case 1 msms inserted: id:" + m.getId() + " hmdb_id:" + m.getHmdb_id() + "compound_id:" + m.getCompound_id());

                }
            } else //case 2: The hmdb_id corresponding to that msms is not in the db
            {
                if (ableToInsert(m, db)) {//first, we check that a valid synonim of thet msms wasnt already inserted
                    msyn = getSynonim(m, msms);//if its not inserted yet we look for a synonim,because since the hmdb_id is not in the ddbb
                    //the compound id is set to zero and we aret interested in that
                    if (msyn != null && msyn.getCompound_id() != 0)//the msyn will be set to null if we dont find any synonim. If this happens, 
                    // a synonim log will be written, and that msms wont be inserted in the ddbb
                    {
                        System.out.println("Case 2 msms: id:" + m.getId() + " hmdb_id:" + m.getHmdb_id() + "compound_id:" + m.getCompound_id());
                        System.out.println("Case 2 synonim inserted: id:" + msyn.getId() + " hmdb_id:" + msyn.getHmdb_id() + "compound_id:" + msyn.getCompound_id());
                        insertMsms(msyn, db);

                    }
                }
            }
            //System.out.println("msms");
        }
        //to add the compounds_id field to the whole table
        //addCompoundID(db);
    }

    public static void insertMsmsListTrial(CMMDatabase db, List<Msms> msms, Msms m) {

        Msms temp, msyn;
        Iterator it = msms.iterator();

        while (it.hasNext()) {
            temp = (Msms) it.next();
            //case 1: compound id is different to zero, since the hmdb_id corresponding to the msms is in the ddbb
            if (temp.equals(m)) {
                if (m.getCompound_id() != 0) {
                    if (ableToInsert(m, db)) {//check if the msms is already in the ddbb, if it is not in the ddbb we insert it, if it is, we 
                        //don't do anything
                        insertMsms(m, db);
                        System.out.println("Case 1 msms inserted: id:" + m.getId() + " hmdb_id:" + m.getHmdb_id() + "compound_id:" + m.getCompound_id());

                    }
                } else //case 2: The hmdb_id corresponding to that msms is not in the ddbb
                {
                    if (ableToInsert(m, db)) {//first, we check that a valid synonim of thet msms wasnt already inserted
                        msyn = getSynonim(m, msms);//if its not inserted yet we look for a synonim,because since the hmdb_id is not in the ddbb
                        //the compound id is set to zero and we aret interested in that
                        if (msyn != null && msyn.getCompound_id() != 0)//the msyn will be set to null if we dont find any synonim. If this happens, 
                        // a synonim log will be written, and that msms wont be inserted in the ddbb
                        {
                            System.out.println("Case 2 msms: id:" + m.getId() + " hmdb_id:" + m.getHmdb_id() + "compound_id:" + m.getCompound_id());
                            System.out.println("Case 2 synonim inserted: id:" + msyn.getId() + " hmdb_id:" + msyn.getHmdb_id() + "compound_id:" + msyn.getCompound_id());
                            insertMsms(msyn, db);

                        }
                    }
                }
                //System.out.println("msms");
            }
        }
        //to add the compounds_id field to the whole table
        //addCompoundID(db);
    }

    public static void insertMsms(Msms m, CMMDatabase db) {
        
        String hmdb_id = m.getHmdb_id();
        String instrument_type = m.getInstrument_type();
        if(hmdb_id == null || hmdb_id.equalsIgnoreCase("null") || hmdb_id.equals(""))
        {
            hmdb_id = "NULL";
        }
        else{
            hmdb_id = "'" + hmdb_id + "'";
        }
        
        if(instrument_type == null || instrument_type.equalsIgnoreCase("null") || instrument_type.equals(""))
        {
            instrument_type = "NULL";
        }
        else{
            instrument_type = "'" + instrument_type + "'";
        }
        
                
        String sql = "INSERT INTO msms (msms_id, hmdb_id, voltage,voltage_level, instrument_type, ionization_mode, peak_count, compound_id, predicted) "
                + "VALUES (" + m.getId() + "," + hmdb_id + "," + m.getVoltage() + ",'" + m.getVoltage_level() + "'," + instrument_type + ","
                + m.getIonization_mode() + "," + m.getPeak_count() + "," + m.getCompound_id() + "," + m.getPredicted() + ")";
        //System.out.println("SQL: " + sql);
        db.executeIDU(sql);

        insertPeaks(m, db);
        insertReferences(m, db);
    }

    private static void insertPeaks(Msms m, CMMDatabase db) {
        Iterator it = m.getPeaks().iterator();
        Peak p;
        String sql;
        while (it.hasNext()) {
            p = (Peak) it.next();
            sql = "INSERT INTO msms_peaks (peak_id, intensity,mz,msms_id) VALUES (" + p.getId() + "," + p.getIntensity() + "," + p.getMz() + "," + p.getMsms_id() + ")";
            db.executeIDU(sql);
            //System.out.println("peak\n");

        }
    }

    private static void insertReferences(Msms m, CMMDatabase db) {
        Iterator it = m.getReferences().iterator();
        Reference r;
        String sql2;
        while (it.hasNext()) {
            r = (Reference) it.next();
            sql2 = "INSERT INTO msms_reference (reference_id, pubmed_id, ref_text, msms_id) VALUES (" + r.getId() + ",'" + r.getPubmed_id() + "','" + r.getRef_text() + "', "
                    + r.getMsms_id() + ")";

            db.executeIDU(sql2);
            //System.out.println("reference\n");
        }

    }

    private static File downloadHMDBXMLFile(String HMDBID) {
        //where you are going to save the downloaded files
        String path = Constants.RESOURCES_PATH + "HMDB" + File.separator;
        String content = "";
        File HMDBFile = new File(path + HMDBID + ".xml");

        if (HMDBFile.exists()) {
            return HMDBFile;
        } else {
            try {
                String uriString = Constants.HMDB_ONLINE_RESOURCES_PATH + HMDBID + ".xml";
                //System.out.println(uriString);
                content = UriDevice.readPlainFile(uriString);

                if (content.isEmpty()) {
                    //writeSynonimLog(HMDBID);
                    return null;
                }
                // First we download the primary xml file
                String primaryHMDBID = PatternFinder.getPrimaryHMDBIDFromHTML(content);
                uriString = Constants.HMDB_ONLINE_RESOURCES_PATH + primaryHMDBID + ".xml";
                //System.out.println("NEW URI STRING" + uriString);
                content = UriDevice.readPlainFile(uriString);
                MyFile.writeFirstLine(content, path + HMDBID + ".xml");

                System.out.println("Writing content ONLINE of: " + HMDBID);
            } catch (IOException ex) {
                Logger.getLogger(DownloaderOfWebResources.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return HMDBFile;
    }

    private static void writeSynonymLog(Msms m) throws IOException {
        File file = new File(Constants.RESOURCES_PATH + "MSMS/synonymsLog.txt");
        FileWriter fw = new FileWriter(file, true);
        PrintWriter pw = new PrintWriter(fw, true);
        pw.println(m.toString());
        pw.close();
        fw.close();
    }

    private static void duplicateLog(String s) throws IOException {
        File file = new File(Constants.RESOURCES_PATH + "MSMS/DuplicatesLog.txt");
        FileWriter fw = new FileWriter(file, true);
        PrintWriter pw = new PrintWriter(fw, true);
        pw.println(s);
        pw.close();
        fw.close();
    }

    private static Msms getSynonim(Msms m, List<Msms> msmslist) {

        File file;
        try {

            file = downloadHMDBXMLFile(m.getHmdb_id());

            if (file != null) {

                Msms msmsSyn;
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder b = dbf.newDocumentBuilder();
                Document dom;
                try {
                    dom = b.parse(file);
                    org.w3c.dom.Element doc = dom.getDocumentElement();
                    NodeList syn_list = doc.getElementsByTagName("accession");
                    System.out.println(syn_list.getLength());
                    for (int i = 0; i < syn_list.getLength(); i++) {
                        String syn;
                        syn = (syn_list.item(i)).getTextContent();
                        System.out.println(syn);

                        if ((msmsSyn = isSynonimAbleToInsert(syn, msmslist, m)) != null) {
                            System.out.println("The synonim with hmdb_id: " + syn + " is able to be inserted.");
                            return msmsSyn;
                        }

                    }
                } catch (IOException ex) {
                    Logger.getLogger(InsertMsms.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SAXException ex) {
                    Logger.getLogger(InsertMsms.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                writeSynonymLog(m);
            }

        } catch (ParserConfigurationException ex) {
            Logger.getLogger(InsertMsms.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(InsertMsms.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private static Msms isSynonimAbleToInsert(String syn, List<Msms> msmslist, Msms m) {
        Iterator it = msmslist.iterator();

        Msms temp;
        while (it.hasNext()) {
            temp = (Msms) it.next();
            //To be a valid synonim it must have a synonim hmdb if and the same msms id
            if (temp.getHmdb_id().equals(syn) && temp.getId() == m.getId()) {
                if (temp.getCompound_id() != 0)//if we find at the list a synonim with compound id !=0 we will insert it
                {
                    return temp;
                }
            }

        }
        return null;
    }

    //to search the duplicated msms
    public static void searchDuplicates(List<Msms> msmslist) throws IOException {
        Collections.sort(msmslist);//msms are sorted according to their msms_id by a compareTo method in class Msms
        Iterator it = msmslist.iterator();
        LinkedList<Msms> duplicates = new LinkedList<>();

        LinkedList<Integer> idlist = new LinkedList<>();
        while (it.hasNext()) {
            Msms t = (Msms) it.next();
            int id = t.getId();
            idlist.add(id);
        }

        Set<Integer> quipu = new HashSet<>(idlist);
        Msms temp;
        for (int key : quipu) {

            int freq = Collections.frequency(idlist, key);

            if (freq >= 2)//in order to get each msms id with its corresponding number of duplicates
            {
                //duplicates.addAll(getDuplicates(key, freq, msmslist));
                //checkSynonim (key, freq, msmslist);

                //to keep the freq value
                it = msmslist.iterator();
                while (it.hasNext()) {
                    temp = (Msms) it.next();
                    if (temp.getId() == key) {
                        duplicates.add(temp);
                        System.out.println(key + ": " + freq);
                        String log = "Msms_id: " + temp.getId() + " Freq: " + freq + " HMDB_id: " + temp.getHmdb_id();
                        duplicateLog(log);
                    }

                }
            }

        }
        Collections.sort(duplicates);
        //return duplicates;
        /*it= duplicates.iterator();
        while(it.hasNext())
        {
            System.out.println(((Msms) it.next()).toString());
        }*/
    }
//seems that some of the duplicates hmdb_id are in the ddbb too (therefore, the compound_id in those duplicates isn't set to zero)
    //so, only compound_id= 0 cannot be used as constraint to not insert that msms in ddbb.

    private static boolean ableToInsert(Msms m, CMMDatabase db) { //and msms will be able to be inserted if the compound id is different to zero
        //that would imply that the hmdb corresponding info is in the database
        String sql = "SELECT COUNT(*)FROM msms WHERE msms_id=" + m.getId();
        int check = db.getInt(sql);
        return check == 0; //to know the count of that msms in the ddbb
    }

    public static void insertMsmsFromFolder(String source, CMMDatabase db) {
        //to create the folders with null data content. Source and destination folders must exist.
        //when executing it doesnt overwritte de content. Therefore, if you execute several times, the .txt content will be duplicated.
        //it has been executed so we comment it.
        //Utilities.readMsmsFromFileForLog(source, destination);
        //To read all the msms:
        List<Msms> msmslist = new LinkedList();
        Msms temp;
        File sourceFile = new File(source);//source is the path where we have all the xml with the msms data
        String[] msmsfiles = sourceFile.list();
        //Database db = new CMMDatabase();
        //db.connectToDB("jdbc:mysql://localhost/compounds", "alberto", "alberto");

        for (String msmsfile : msmsfiles) {
            if (msmsfile.contains("HMDB")) {

                if (msmsfile.contains("_ms_ms_spectrum_")) {
                    //System.out.println("READING");

                    File file = new File(source + msmsfile);
                    temp = Utilities.readMsmsFromFile(file);
                    //get the compound id from the db
                    temp.setCompoundIDFromDatabase(db);
                    msmslist.add(temp);
                }
                // System.out.println(temp.toString());
                //System.out.println("\n\nMSMS");
            }
        }
        //at this moment we have the msms list with all the compound_id, but some compound_id are zero because the hmdb_id are synonims
        //and they do not appear in the ddbb with that id. In order to check that everything is correct we will check that the duplicated
        //msms have a hmdb_id that is synonim of one already contained in the ddbb
        //1st   sort the list of msms by msms id (that way, the duplicates will be near each other, more effiecient

//--uncomment fot generating a file with the duplicates information
        //InsertMsms.searchDuplicates(msmslist);
        System.out.println(msmslist.size() + " msms sucessfully loaded\n");
        System.out.println("Proceed to the insertion, good luck!");
        InsertMsms.insertMsmsList(db, msmslist);
    }

    public static void insertWrongMsmsFromFolder(String source, CMMDatabase db) {
        //to create the folders with null data content. Source and destination folders must exist.
        //when executing it doesnt overwritte de content. Therefore, if you execute several times, the .txt content will be duplicated.
        //it has been executed so we comment it.
        //Utilities.readMsmsFromFileForLog(source, destination);
        //To read all the msms:
        List<Msms> msmslist = new LinkedList();
        Msms temp;
        File sourceFile = new File(source);//source is the path where we have all the xml with the msms data
        String[] msmsfiles = sourceFile.list();
        //Database db = new CMMDatabase();
        //db.connectToDB("jdbc:mysql://localhost/compounds", "alberto", "alberto");
        List<String> listWrongInserted = listWrongInserted();
        for (String msmsfile : msmsfiles) {
            if (msmsfile.contains("HMDB")) {
                String HMDBID = PatternFinder.getPrimaryHMDBIDFromHTML(msmsfile);
                if (msmsfile.contains("_ms_ms_spectrum_") && listWrongInserted.contains(HMDBID)) {
                    //System.out.println("READING");

                    File file = new File(source + msmsfile);
                    temp = Utilities.readMsmsFromFile(file);
                    //get the compound id from the db
                    temp.setCompoundIDFromDatabase(db);
                    msmslist.add(temp);
                }
                // System.out.println(temp.toString());
                //System.out.println("\n\nMSMS");
            }
        }
        //at this moment we have the msms list with all the compound_id, but some compound_id are zero because the hmdb_id are synonims
        //and they do not appear in the ddbb with that id. In order to check that everything is correct we will check that the duplicated
        //msms have a hmdb_id that is synonim of one already contained in the ddbb
        //1st   sort the list of msms by msms id (that way, the duplicates will be near each other, more effiecient

//--uncomment fot generating a file with the duplicates information
        //InsertMsms.searchDuplicates(msmslist);
        System.out.println(msmslist.size() + " msms sucessfully loaded\n");
        System.out.println("Proceed to the insertion, good luck!");
        InsertMsms.insertMsmsList(db, msmslist);
    }

    public static List<String> listWrongInserted() {
        List<String> wrongInserted = new LinkedList<String>();
        wrongInserted.add("HMDB0031075");
        

        return wrongInserted;
    }

}
