/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testersMSMS;

import databases.CMMDatabase;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;
import utilities.ReaderXML;

/**
 *
 * @author María 606888798
 */
public class Msms implements Comparable<Msms> {

    //generated getters and setters; full, nonid and empty constructors.
    private int id;
    private String voltage_level;
    private int voltage;
    private String instrument_type;
    private int ionization_mode;//2 negative 1 positive -1 null
    private int peak_count;
    private String hmdb_id;
    private List<Peak> peaks;
    private List<Reference> references;
    private int compound_id;
    private double score;
    private double precursor;
    private String precursor_type;
    private String compound_name;
    private String collisionEnergy;
    private int predicted;//0 experimental, 1 predicted, -1 null
    private String compound_formula;
    private double compound_mass;

    public Msms(String voltage_level, int ionization_mode, double precursor, List <Peak> peaks, int predicted) {
        this.references = new LinkedList();
        this.peaks = new LinkedList();
        this.voltage_level = voltage_level;
        this.ionization_mode = ionization_mode;
        this.precursor = precursor;
        this.peaks=peaks;
        this.predicted=predicted;
    }

    public int getPredicted() {
        return predicted;
    }

    public void setPredicted(String predicted) {
        if (predicted.contains("true")) {
            this.predicted = 1;
        } else if (predicted.contains("false")) {
            this.predicted = 0;

        } else {

            this.predicted = -1;
        }
    }

    public void setPredicted(int p)
    {
        this.predicted=p;
    }

    public String getCollisionEnergy() {
        return collisionEnergy;
    }

    public void setCollisionEnergy(String collisionEnergy) {
        this.collisionEnergy = collisionEnergy;
    }

    public String getCompound_name() {
        return compound_name;
    }

    public void setCompound_name(String compound_name) {
        this.compound_name = compound_name;
    }

    public String getPrecursor_type() {
        return precursor_type;
    }

    public void setPrecursor_type(String precursor_type) {
        this.precursor_type = precursor_type;
    }

    public double getPrecursor() {
        return precursor;
    }

    public void setPrecursor(double precursor) {
        this.precursor = precursor;
    }

    public Msms(String instrument_type, int ionization_mode, double precursor, String precursor_type, int peak_count, int predicted) {
        this.references = new LinkedList();
        this.peaks = new LinkedList();
        this.precursor = precursor;
        this.precursor_type = precursor_type;
        this.instrument_type = instrument_type;
        this.ionization_mode = ionization_mode;
        this.peak_count = peak_count;
        this.predicted = predicted;
    }

    public Msms(String voltage_level, int voltage, String instrument_type, int ionization_mode, int peak_count, int predicted) {
        this.references = new LinkedList();
        this.peaks = new LinkedList();
        this.voltage_level = voltage_level;
        this.voltage = voltage;
        this.instrument_type = instrument_type;
        this.ionization_mode = ionization_mode;
        this.peak_count = peak_count;
        this.predicted = predicted;
    }

    public Msms(int compound_id, double score) {
        this.references = new LinkedList();
        this.peaks = new LinkedList();
        this.compound_id = compound_id;
        this.score = score;
    }

    public Msms(int id, String voltage_level, int voltage, String instrument_type, int ionization_mode, int peak_count, String hmdb_id, int compound_id, double score, int predicted) {
        this.references = new LinkedList();
        this.peaks = new LinkedList();
        this.id = id;
        this.voltage_level = voltage_level;
        this.voltage = voltage;
        this.instrument_type = instrument_type;
        this.ionization_mode = ionization_mode;
        this.peak_count = peak_count;
        this.hmdb_id = hmdb_id;
        this.compound_id = compound_id;
        this.score = score;
        this.predicted = predicted;
    }

    public void setScore(double score) {
        //System.out.println("This msms_id: "+id);
        this.score = score;
    }

    public double getScore() {
        return score;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setVoltage_level(String voltage_level) {
        if (voltage_level.equalsIgnoreCase("null")) {
            this.voltage_level = "";
        } else if (voltage_level.equalsIgnoreCase("high") || voltage_level.equalsIgnoreCase("low") || voltage_level.equalsIgnoreCase("medium") || voltage_level.equalsIgnoreCase("med")) {
            this.voltage_level = voltage_level;
        } else {
            this.voltage_level = "";
        }
    }

    public Msms(int id, String voltage_level, int voltage, String instrument_type, int ionization_mode, int peak_count, String hmdb_id, int compound_id) {
        this.references = new LinkedList();
        this.peaks = new LinkedList();
        this.id = id;
        this.voltage_level = voltage_level;
        this.voltage = voltage;
        this.instrument_type = instrument_type;
        this.ionization_mode = ionization_mode;
        this.peak_count = peak_count;
        this.hmdb_id = hmdb_id;
        this.compound_id = compound_id;
    }

    public int getCompound_id() {
        return compound_id;
    }

    public Peak getBasePeak() {
        if (this.getMaxIntensity() != 100d) {
            this.normalizeIntensities();
        }

        Iterator it = this.getPeaks().iterator();
        while (it.hasNext()) {
            Peak p = (Peak) it.next();
            if (p.getIntensity() == 100) {
                return p;
            }
        }

        return null;//this will never happen since we always normalize the peaks
    }

    public void setCompound_id(int compound_id) {
        this.compound_id = compound_id;
    }

    public void setVoltage(int voltage) {
        this.voltage = voltage;
    }

    public void setVoltage(String voltage) {
        if (voltage.equalsIgnoreCase("null")) {
            this.voltage = 0;
        } else {
            this.voltage = Integer.parseInt(voltage);
        }
    }

    public void setInstrument_type(String instrument_type) {
        if (instrument_type.equalsIgnoreCase("null")) {
            this.instrument_type = "";
        } else {
            this.instrument_type = instrument_type;
        }
    }

    public void setIonization_mode(String ionization_mode) {
        if (ionization_mode.equals("null")) {
            this.ionization_mode = -1;
        } else if (ionization_mode.equalsIgnoreCase("positive")) {
            this.ionization_mode = 1;
        } else if (ionization_mode.equalsIgnoreCase("negative")) {
            this.ionization_mode = 2;
        }
    }
        public void setIonizationMode(int ionizationMode) {
        this.ionization_mode = ionizationMode;
    }

    public void setPeak_count(String peak_count) {
        if (peak_count.equalsIgnoreCase("null")) {
            this.peak_count = 0;
        } else {
            this.peak_count = Integer.parseInt(peak_count);
        }
    }

    public void setPeak_count(int peak_count) {
        this.peak_count = peak_count;
    }

    public String getVoltage_level() {
        return voltage_level;
    }

    public void setHmdb_id(String hmdb_id) {
        if (hmdb_id.equals("null")) {
            this.hmdb_id = "";
        } else {
            /*if(hmdb_id.length()==9)//hmdb00000 must be hmdb0000000
            {
                this.hmdb_id=hmdb_id.substring(0, 3)+"00"+hmdb_id.substring(4,8);
            }*/ //must do this when the db is updated
            this.hmdb_id = hmdb_id;
        }
    }

    public void setPeaks(List<Peak> peaks) {
        this.peaks = peaks;
    }

    public void setReferences(List<Reference> references) {
        this.references = references;
    }

    public int getVoltage() {
        return voltage;
    }

    public String getInstrument_type() {
        return instrument_type;
    }

    public int getIonization_mode() {
        return ionization_mode;
    }

    public int getPeak_count() {
        return peak_count;
    }

    public String getHmdb_id() {
        return hmdb_id;
    }

    public List<Peak> getPeaks() {
        return peaks;
    }

    public List<Reference> getReferences() {
        return references;
    }

    public Msms() {
        this.references = new LinkedList();
        this.peaks = new LinkedList();
    }

    public Msms(int id, String level, int voltage, String instrument, int ionization_mode, int peak_count, String hmdb_id, List<Peak> peaks, List<Reference> references) {
        this.references = new LinkedList();
        this.peaks = new LinkedList();
        this.id = id;
        this.voltage_level = level;
        this.voltage = voltage;
        this.instrument_type = instrument;
        this.ionization_mode = ionization_mode;
        this.peak_count = peak_count;
        this.hmdb_id = hmdb_id;
        this.peaks = peaks;
        this.references = references;
    }

    public Msms(String level, int voltage, String instrument, int ionization_mode, int peak_count, String hmdb_id, List<Peak> peaks, List<Reference> references) {
        this.references = new LinkedList();
        this.peaks = new LinkedList();
        this.voltage_level = level;
        this.voltage = voltage;
        this.instrument_type = instrument;
        this.ionization_mode = ionization_mode;
        this.peak_count = peak_count;
        this.hmdb_id = hmdb_id;
        this.peaks = peaks;
        this.references = references;
    }

    @Override
    public String toString() {
        if (this.score != 0) {
            return "\nMSMS ID: " + this.id + "\nHMBD_ID: " + this.hmdb_id + "\nVOLTAGE: " + this.voltage + "\nVOLTAGE LEVEL: " + this.voltage_level
                    + "\nINSTRUMENT TYPE: " + this.instrument_type + "\nIONIZATION MODE: " + this.ionization_mode + "\nPEAK COUNT: " + this.peak_count + "\nCOMPOUND_ID: " + this.compound_id
                    + "\n---------SCORE: " + this.score;
        }
        return "\nMSMS ID: " + this.id + "\nHMBD_ID: " + this.hmdb_id + "\nVOLTAGE: " + this.voltage + "\nVOLTAGE LEVEL: " + this.voltage_level
                + "\nINSTRUMENT TYPE: " + this.instrument_type + "\nIONIZATION MODE: " + this.ionization_mode + "\nPEAK COUNT: " + this.peak_count + "\nCOMPOUND_ID: " + this.compound_id;
    }

    public Msms readMsmsFromFile(File file) {
        ReaderXML readerXML = new ReaderXML();
        Msms msms = new Msms();
        List<Reference> references = new ArrayList();
        List<Peak> peaks = new ArrayList();

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder b = dbf.newDocumentBuilder();
            Document dom = b.parse(file);
            Element doc = dom.getDocumentElement();
            //here we read the msms
            msms.setVoltage_level(readerXML.getTextValue(doc, "collision-energy-level"));
            msms.setVoltage(Integer.parseInt(readerXML.getTextValue(doc, "collision-energy-voltage")));
            msms.setHmdb_id(readerXML.getTextValue(doc, "database-id"));
            msms.setId(Integer.parseInt(readerXML.getTextValue(doc, "id")));
            msms.setInstrument_type(readerXML.getTextValue(doc, "instrument-type"));
            msms.setIonization_mode(readerXML.getTextValue(doc, "ionization-mode"));
            msms.setPeak_count(Integer.parseInt(readerXML.getTextValue(doc, "peak-counter")));

            //now read references
            List<Element> ref_list = readerXML.findAllElementsByTagName(doc, "reference");
            Reference r = new Reference();
            for (int i = 0; i < ref_list.size(); i++) {
                r.setId(Integer.parseInt(readerXML.getTextValue(ref_list.get(i), "id")));
                r.setPubmed_id(readerXML.getTextValue(ref_list.get(i), "pubmed-id"));
                r.setRef_text(readerXML.getTextValue(ref_list.get(i), "ref-text"));
                //System.out.println(r.toString());
                references.add(r);
            }

            msms.setReferences(references);

            List<Element> peak_list = ReaderXML.findAllElementsByTagName(doc, "ms-ms-peak");
            Peak p = new Peak();
            for (int j = 0; j < peak_list.size(); j++) {
                p.setId(Integer.parseInt(readerXML.getTextValue(peak_list.get(j), "id")));
                p.setIntensity(Double.parseDouble(readerXML.getTextValue(peak_list.get(j), "intensity")));
                p.setMz(Double.parseDouble(readerXML.getTextValue(peak_list.get(j), "mass-charge")));
                //System.out.println(p.toString());
                peaks.add(p);
            }

            msms.setPeaks(peaks);

            //System.out.println(msms.toString());
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(TFG.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(TFG.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TFG.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msms;
    }

    public void setCompoundIDFromDatabase(CMMDatabase db) {
        //with this method i set all the compound_id of the msms that has an HMDB_id from the database
        String sql = "SELECT compound_id FROM compounds_hmdb WHERE hmdb_id='" + this.hmdb_id + "'";

        //now in rs we have the compound id
        // TODO MANAGE EXCEPTIONS! WRITE IN A LOG and NO INSERTION IN DB 
        int compound_id = db.getInt(sql);
        if (compound_id > 0) {
            this.compound_id = compound_id;
        }
        else{
            this.compound_id = compound_id;
            //System.out.println("hmdb_id: " + this.hmdb_id  + " has no compound_id");
        }

        //System.out.println(compound_id);
        //id the corresponding hmdbd_id of that msms is not in the database (probably because it is a synonim) the compound_id is set to zero.
    }

    public void addPeak(Peak p) {
        this.peaks.add(p);
    }

    public void addPeaks(List<Peak> peaks) {
        this.peaks.addAll(peaks);
    }

    public void addReference(Reference r) {
        this.references.add(r);
    }

    public void removePeak(Peak p) {
        this.peaks.remove(p);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Msms other = (Msms) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.voltage != other.voltage) {
            return false;
        }
        if (this.peak_count != other.peak_count) {
            return false;
        }
        if (!Objects.equals(this.voltage_level, other.voltage_level)) {
            return false;
        }
        if (!Objects.equals(this.instrument_type, other.instrument_type)) {
            return false;
        }
        if (!Objects.equals(this.ionization_mode, other.ionization_mode)) {
            return false;
        }
        if (!Objects.equals(this.hmdb_id, other.hmdb_id)) {
            return false;
        }
        if (!Objects.equals(this.peaks, other.peaks)) {
            return false;
        }
        if (!Objects.equals(this.references, other.references)) {
            return false;
        }
        return true;
    }

    //to order the elements of the msms list by the msms_id
    @Override
    public int compareTo(Msms msms) {
        int result = 0;
        if (this.id < msms.getId()) {
            result = -1;
        } else if (this.id > msms.getId()) {
            result = 1;
        } else {
            result = 0;
        }
        return result;
    }

    public double getMaxIntensity() {
        Iterator it = this.peaks.iterator();
        Double maxIntensity = 0d;
        while (it.hasNext()) {
            Peak p = (Peak) it.next();
            maxIntensity = Math.max(maxIntensity, p.getIntensity());
        }
        return maxIntensity;
    }

    public void normalizeIntensities() {

        Double maxIntensity = this.getMaxIntensity();
        if (maxIntensity == 1) {
            Iterator it = this.peaks.iterator();
            while (it.hasNext()) {
                Peak p = (Peak) it.next();
                double intensity = p.getIntensity();
                p.setIntensity(intensity * 100);
            }
        } else if (maxIntensity != 100) {
            Iterator it = this.peaks.iterator();
            while (it.hasNext()) {
                Peak p = (Peak) it.next();
                Double normalizedIntensity = (p.getIntensity() * 100) / maxIntensity;
                p.setIntensity(normalizedIntensity);
            }
        }
    }

    public void checkPrecursorType() {
        if (this.precursor_type.contains("[M+H]+")) {
            this.precursor = this.precursor - 1.0079;//during ionization, an hydrogen with that mass was added to the molecule
            //we must remove it to go back to the original mass
        } else if (this.precursor_type.contains("[M-H]-")) {
            this.precursor = this.precursor + 1.0079;
        }
    }



    void setCompound_formula(String cf) {
       this.compound_formula=cf;
    }

    void setCompound_mass(double aDouble) {
        this.compound_mass=aDouble;
    }
     
public String toString2()

{
    return "Compound id: "+this.compound_id+" Compound name: "+this.compound_name+" Compound mass: "+this.compound_mass;
}


    void setPrecursorMass() {
       if(this.ionization_mode==2) //means negative ionization
        {
            this.precursor = this.precursor + 1.0079;
        }
        if (this.ionization_mode==1)
        {
            this.precursor = this.precursor - 1.0079;
        }
    }
}
