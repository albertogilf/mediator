/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testersMSMS;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author María 606888798
 */
public class MsmsReader {

    private static String filePath = "C:\\Users\\María 606888798\\Desktop\\CMM\\ToTestWithResidues.txt";
    private static List<Msms> myMsmsList = new LinkedList<>();

    public static List <Msms> loadMsmsToTest() {
        try {
            FileReader fr = new FileReader(filePath);
            BufferedReader bf = new BufferedReader(fr);
            String line;
            Msms msms = new Msms();
            while ((line = bf.readLine()) != null) {
                //System.out.println(line);CH$NAME:
                if (line.contains("CH$NAME")) {
                    int start = 0;
                    if (line.contains(": ")) {
                        start = line.indexOf(": ") + 2;//position plus the space plus the initial position of the info
                    } else if (line.contains(":")) {
                        start = line.indexOf(":") + 1;
                    }
                    msms.setCompound_name(line.substring(start));
                }
                if (line.contains("AC$INSTRUMENT_TYPE")) {
                    int start = 0;
                    if (line.contains(": ")) {
                        start = line.indexOf(": ") + 2;//position plus the space plus the initial position of the info
                    } else if (line.contains(":")) {
                        start = line.indexOf(":") + 1;
                    }
                    msms.setInstrument_type(line.substring(start));
                }
                if (line.contains("AC$MASS_SPECTROMETRY: ION_MODE")) {
                    int start = 0;
                    if (line.contains("MODE ")) {
                        start = line.indexOf("MODE ") + 5;//position plus the space plus the initial position of the info
                    }
                    msms.setIonization_mode(line.substring(start));
                }
                if (line.contains("AC$MASS_SPECTROMETRY: COLLISION_ENERGY")) {
                    int start = 0;
                    if (line.contains("ENERGY ")) {
                        start = line.indexOf("ENERGY ") + 7;//position plus the space plus the initial position of the infoAC$MASS_SPECTROMETRY: COLLISION_ENERGY
                    }
                    msms.setCollisionEnergy(line.substring(start));
                }
                if (line.contains("PRECURSOR_M/Z")) {
                    int start = 0;
                    if (line.contains("Z ")) {
                        start = line.indexOf("Z ") + 2;//position plus the space plus the initial position of the info
                    } else if (line.contains("Z")) {
                        start = line.indexOf("Z") + 1;
                    }
                    msms.setPrecursor(Double.parseDouble(line.substring(start)));
                    //System.out.println(msms.getPrecursor());
                }
                if (line.contains("PRECURSOR_TYPE")) {
                    int start = 0;
                    if (line.contains("[")) {
                        start = line.indexOf("[") ;
                    }
                    msms.setPrecursor_type(line.substring(start));
                    //System.out.println(line.substring(start));
                }
                if (line.contains("PK$NUM_PEAK")) {
                    int start = 0;
                    if (line.contains(": ")) {
                        start = line.indexOf(": ") + 2;//position plus the space plus the initial position of the info
                    } else if (line.contains(":")) {
                        start = line.indexOf(":") + 1;
                    }
                    msms.setPeak_count(Integer.parseInt(line.substring(start)));
                }
                if (line.contains("PK$PEAK: m/z int. rel.int.")) {
                    int peak_count = msms.getPeak_count();
                    for (int i = 0; i < peak_count; i++) {
                        if ((line = bf.readLine()) != null) {
                            if (line.contains("----")){break;}
                            String[] lineValues = line.split(" ");
                            //for (String lineValue : lineValues) {
                            //   System.out.println("<"+lineValue+">");
                            //}
                            //positions 2 and 3 since there are many spaces when starting
                            //first we have mz
                            //then we have intensity
                            double mz=0;
                            double intensity=0;
                            for(int j=0; j<lineValues.length-1;j++){
                            if(!lineValues[j].isEmpty()){
                            mz = Double.parseDouble(lineValues[j]);
                            //System.out.println(mz);
                            j++;
                            intensity = Double.parseDouble(lineValues[j]);
                            //System.out.println(intensity);
                                
                            j++;}}
                            msms.addPeak(new Peak(mz, intensity));
                        }
                    }

                    
                }
                if (line.contains("----"))
                {
                    //System.out.println("Precursor type:"+msms.getPrecursor_type());
                    myMsmsList.add(msms);
                    //refresh object
                    msms = new Msms();
                }
                
            }
            return myMsmsList;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MsmsReader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MsmsReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        
    }

}
