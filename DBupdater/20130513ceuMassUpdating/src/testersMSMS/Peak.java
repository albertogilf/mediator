/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testersMSMS;

import java.util.Objects;

/**
 *
 * @author María 606888798
 */
public class Peak implements Comparable<Peak> {

    private int id;
    private double intensity;
    private double mz;
    private Integer msms_id;

    public Peak(int id, double intensity, double mz, Integer msms_id) {
        this.id = id;
        this.intensity = intensity;
        this.mz = mz;
        this.msms_id = msms_id;
    }

    public Peak(double mz, double intensity) {
        this.intensity = intensity;
        this.mz = mz;
    }

    public Peak() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMsms_id() {
        return msms_id;
    }

    public void setMsms_id(int msms_id) {
        this.msms_id = msms_id;
    }

    public void setIntensity(double intensity) {
        this.intensity = intensity;
    }

    public void setIntensity(String intensity) {
        if (intensity.equals("null")) {
            this.intensity = 0;
        } else {
            this.intensity = Integer.parseInt(intensity);
        }
    }

    public void setMz(double mz) {
        this.mz = mz;
    }

    public void setMz(String mz) {
        if (mz.equals("null")) {
            this.mz = 0;
        } else {
            this.mz = Float.parseFloat(mz);
        }
    }

    

    public int getId() {
        return id;
    }

    public double getIntensity() {
        return intensity;
    }

    public double getMz() {
        return mz;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.id;
        hash = 97 * hash + (int) (Double.doubleToLongBits(this.intensity) ^ (Double.doubleToLongBits(this.intensity) >>> 32));
        hash = 97 * hash + (int) (Double.doubleToLongBits(this.mz) ^ (Double.doubleToLongBits(this.mz) >>> 32));
        hash = 97 * hash + this.msms_id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Peak other = (Peak) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.intensity, other.intensity)) {
            return false;
        }
        if (!Objects.equals(this.mz, other.mz)) {
            return false;
        }
        if (this.msms_id != other.msms_id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {

        return "\nPEAK ID: " + this.id + "\nINTENSITY: " + this.intensity + "\n MASS CHARGE: " + this.mz + "\n MSMS ID: " + this.msms_id;
    }

    public boolean equalspeak(Peak p) {
        return (this.id == p.getId() && this.intensity == p.getIntensity() && this.mz == p.mz && this.msms_id == p.msms_id);
    }

    @Override
    public int compareTo(Peak peak) {
        int result;
        if (this.mz < peak.getMz()) {
            result = -1;
        } else if (this.mz > peak.getMz()) {
            result = 1;
        } else {
            if (this.intensity < peak.getIntensity()) {
                result = -1;
            } else if (this.intensity > peak.getIntensity()) {
                result = 1;
            } else {
                return 0;
            }
        }
        return result;
    }

    public boolean peakMatch(Peak libraryPeak, double mzTolerance) {
        return this.mz <= (libraryPeak.getMz() + mzTolerance) && this.mz >= (libraryPeak.getMz() - mzTolerance);
    }

}
