/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testersMSMS;

/**
 *
 * @author María 606888798
 */
public class Reference {

    private int id;
    //private String database;
    private String pubmed_id;
    private String ref_text;
    private int msms_id;

    public Reference(int id, String pubmed_id, String text, int msms_id) {
        this.id = id;
        this.pubmed_id = pubmed_id;
        this.ref_text = text;
        this.msms_id=msms_id;
    }

    public Reference(String pubmed_id, String text) {
        this.pubmed_id = pubmed_id;
        this.ref_text = text;
    }

    public int getMsms_id() {
        return msms_id;
    }

    public void setMsms_id(int msms_id) {
        this.msms_id = msms_id;
    }

    public Reference() {
    }

    public int getId() {
        return id;
    }

    public String getPubmed_id() {
        return pubmed_id;
    }

    public String getRef_text() {
        return ref_text;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPubmed_id(String pubmed_id) {
        if (pubmed_id.equals("null")) {
            this.pubmed_id = "";
        } else {
            this.pubmed_id = pubmed_id;
        }
    }

    public void setRef_text(String ref_text) {
        if (ref_text.equals("null")) {
            this.ref_text = "";
        } else {
            this.ref_text = ref_text;
        }
    }

    @Override
    public String toString() {

        return "\nREFERENCE ID: " + this.id + "\nPUBMED ID: " + this.pubmed_id + "\nTEXT: " + this.ref_text+ "\nMSMS ID: " + this.msms_id;
    }

}
