/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testersMSMS;

import databases.CMMDatabase;
import utilities.Constants;

/**
 *
 * @author María 606888798
 */
public class TFG {

    public static void main(String[] args) {

        double scoreTop1 = 0;
        double scoreTop5 = 0;

        //this is the path where the xml with the data from the different msms is located (change if necessary)
        String source = (Constants.RESOURCES_PATH + "hmdb_spectra/");
        //this is the path where some log info is saved. Those msms with null values at some fields are saved in this folder 
        //(change if necessary)
        String destination = (Constants.RESOURCES_PATH + "hmdb_spectra_log/");
//-- Uncomment to generate log txt files
        // Utilities.readMsmsFromFileForLog(source, destination);

        //To be able to add to each msms the corresponding compound id from the database
        CMMDatabase db = new CMMDatabase();
        //db.connectToDB("jdbc:mysql://localhost/compounds_new", "alberto", "alberto");
        db.connectToDB("jdbc:mysql://localhost/compounds?useSSL=false&serverTimezone=UTC", "alberto", "alberto");
//--Uncomment to insert in the ddbb 
        InsertMsms.insertMsmsFromFolder(source, db);
        InsertMsms.insertWrongMsmsFromFolder(source, db);

//to load a .txt file created for testing
/*
        String pks = "41.03912516 0.09539862\n"
                + "43.01838972 0.0055383057\n"
                + "43.05477522 0.000583306\n"
                + "45.03403978 0.0004223234\n"
                + "51.0234751 0.000568683\n"
                + "63.04460446 0.0019679615\n"
                + "67.01838972 0.0884683321\n"
                + "68.99765427 0.0088594822\n"
                + "69.03403978 0.0298719862\n"
                + "71.01330434 0.0015593881\n"
                + "87.00821896 0.0127656709\n"
                + "89.02386902 0.0009830922\n"
                + "92.99765427 0.0348968241\n"
                + "95.01330434 0.1980337182\n"
                + "111.0446045 3.075703465\n"
                + "113.023869 0.0660805011\n"
                + "115.0031336 0.038181344\n"
                + "129.0187836 0.0289051747\n"
                + "137.023869 45.12447135\n"
                + "139.0031336 0.1563630399\n"
                + "155.0344337 51.03037743";

        List<Peak> peaks = new LinkedList<>();
        String[] p = pks.split("\n");

        for (int i = 0; i < p.length; i++) {

            String[] mzi = p[i].split(" ");
            Peak peak = new Peak();
            //System.out.println(mzi[0]);
            //System.out.println(mzi[1]);
            peak.setMz(Double.parseDouble(mzi[0]));
            peak.setIntensity(Double.parseDouble(mzi[1]));
            peaks.add(peak);
        }
         */
 /* 
        Msms inputMsms= new Msms("low",1, 423, peaks,1);
    /*
95.04968984 0.3250990717
123.08099 0.6759205436
125.09664 0.9282494477
147.08099 1.077208482
151.1122901 1.215757339
227.1435902 1.052916668
253.1592403 12.043192
271.169805 66.95610462
         */
 /*
        List<Msms> candidates = IdentifierMSMS.getMatchedMsmsByParentIon(inputMsms, db, 1);
        System.out.println("number of candidates: " + candidates.size());
        List<Msms> candidatesScored = IdentifierMSMS.scoreMatchedPeaks(inputMsms, candidates, 1);
        List<Msms> top5 = IdentifierMSMS.getTopNMatches(candidatesScored, candidatesScored.size());
        Iterator it = top5.iterator();
        while (it.hasNext()) {
            Msms m = (Msms) it.next();
            System.out.println(m.toString2());
        //InsertMsms.insertMsmsFromFolder(source, db);
        //InsertMsms.insertWrongMsmsFromFolder(source, db);

        double parentiontolerance = 0.1;
        double mztolerance = 0.5;
        List<Msms> totest = loadMsmsToTest();
        
        for (Msms m : totest) {
            List<Msms> candidates = getMatchedMsmsByParentIon(m, db, parentiontolerance);
            System.out.println("Candidates for " + m.getCompound_name() + " with id " + m.getCompound_id() + " is " + candidates.size());

            //List<Msms> candidatesScored = scoreMatchedPeaks(m, candidates,mztolerance, "Da");
            //List<Msms> top5 = getTopNMatches(totest, 0);
        }
         */
    }

}
