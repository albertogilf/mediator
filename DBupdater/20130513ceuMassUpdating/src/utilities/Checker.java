/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import databases.CMMDatabase;
import java.util.List;
import patternFinders.PatternFinder;

/**
 * @author: San Pablo-CEU, Alberto Gil de la Fuente
 * @version: 3.1, 23/05/2016
 */
public class Checker {

    static PatternFinder pf = new PatternFinder();

    /**
     *
     * @param formula1
     * @param formula2
     * @return
     */
    public static boolean checkFormula(String formula1, String formula2) {
        //TODO
        // Check all components of both strings and compare them
        

        return formula1.equals(formula2);
    }
    
        /**
     *
     * @param formula1
     * @return
     */
    public static boolean debugFormula(String formula1) {
        if( formula1.equalsIgnoreCase("") || formula1.equalsIgnoreCase("NULL"))
        {
            return true;
        }
        return pf.debugFormula(formula1);
    }
    

    /**
     *
     * @param formula1
     * @return
     */
    public static boolean hasFormulaNoElements(String formula1) {
        if( formula1.equalsIgnoreCase("") || formula1.equalsIgnoreCase("NULL"))
        {
            return true;
        }
        return pf.checkFormulaElements(formula1);
    }

    /**
     * Checks if the formula of the compound is coherent with the formula from
     * the structure (InChI); It is done checking the number of hydrogens; If
     * the formula from the database only differs from the structure in the
     * number of hydrogens or or does not differ, then the compound formula is
     * not an error; If the formula from InChI has different compounds than
     * formula from database, the formula returned is the formula from the inChI
     *
     * @param formula
     * @param formulaFromInChI
     * @return the formula of the compound.
     */
    /*
    public static String getRealFormula(String formula, String formulaFromInChI) {
        String finalFormula = formula;
        return finalFormula;
    }
     */
    /**
     * return the formula generated from InChI structure
     * 
     * 
     * @param inChI
     * @return the formula contained in the InChI
     */
    public static String getFormulaFromInChI(String inChI) {
        // If the inChI is empty
        if (inChI.equals("") || inChI.equals("NULL") || inChI.equals("null")) {
            return "";
        }
        String formula;
        String formulaStart = "[/]";
        String formulaEnd = "[/]";
        formula = pf.searchFirstOcurrence(inChI, formulaStart + "(.*?)" + formulaEnd);
        // Quit the first / and the last /
        if (!formula.equals("")) {
            formula = formula.substring(1, formula.length() - 1);
        }

        return formula;
    }

    

}
