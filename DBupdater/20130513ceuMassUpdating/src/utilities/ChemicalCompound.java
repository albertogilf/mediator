/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import exceptions.InvalidFormulaException;
import utilities.PeriodicTable;

/**
 *
 * @author alberto.gildelafuent
 */
public class ChemicalCompound {

    private final Formula formula;
    private Double mass;

    public ChemicalCompound(String formulaString) throws InvalidFormulaException {
        this.formula = new Formula(formulaString);
        this.mass = 0d;
        for (Element e : formula.getElements()) {
            for (Element e_table : PeriodicTable.MAPELEMENTS.keySet()) {
                if (e.equals(e_table)) {
                    this.mass = this.mass + formula.getElementQuantity(e) * PeriodicTable.MAPELEMENTS.get(e);
                }

            }
        }
    }

    public double getMass() {
        return this.getMass();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((formula == null) ? 0 : formula.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ChemicalCompound other = (ChemicalCompound) obj;
        if (formula == null) {
            if (other.formula != null) {
                return false;
            }
        } else if (!formula.equals(other.formula)) {
            return false;
        }

        return true;
    }

}
