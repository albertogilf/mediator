/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author: San Pablo-CEU, Alberto Gil de la Fuente
 * @version: 3.1, 14/11/2016
 */
public class Constants {

    public static final String RESOURCES_PATH = "/home/alberto/alberto/repo/mediator/DBupdater/20130513ceuMassUpdating/resources/";
    //public static final String RESOURCES_PATH = "C:" + File.separator + "Users" + File.separator + "ceu" + File.separator + "Desktop" + File.separator + "alberto" + File.separator + "repo_CMM" + File.separator + "mediator" + File.separator + "DBUpdater" + File.separator + "20130513ceuMassUpdating" + File.separator + "resources" + File.separator;
    //public static final String RESOURCES_PATH = "C:" + File.separator + "Users" + File.separator + "alberto.gildelafuent" + File.separator + "Desktop" + File.separator + "alberto" + File.separator + "repo_cmm" + File.separator + "mediator" + File.separator + "DBUpdater" + File.separator + "20130513ceuMassUpdating" + File.separator + "resources" + File.separator;

    public static final int NUMBER_COMPOUNDS_KEGG = 22510;

    public static final String IN_HOUSE_RESOURCES_PATH = RESOURCES_PATH + "in-house" + File.separator;
    public static final String IN_HOUSE_FA_NAME = IN_HOUSE_RESOURCES_PATH + "FA_values.csv";
    public static final String OXPC_FILENAME = IN_HOUSE_RESOURCES_PATH + "oxPCs.csv";
    public static final String UNICHEM_RESOURCES_PATH = RESOURCES_PATH + "unichem" + File.separator;
    public static final String CAS_RESOURCES_PATH = RESOURCES_PATH + "cas" + File.separator;
    public static final String KNAPSACK_RESOURCES_PATH = RESOURCES_PATH + "knapsack" + File.separator;
    public static final String CAS_ONLINE_PATH = "https://chem.sis.nlm.nih.gov/chemidplus/rn/";
    public static final String KEGG_RESOURCES_PATH = RESOURCES_PATH + "kegg" + File.separator;
    public static final String KEGG_MOLFILES_PATH = KEGG_RESOURCES_PATH + "molfiles" + File.separator;
    public static final String HMDB_RESOURCES_PATH = RESOURCES_PATH + "HMDB" + File.separator;
    public static final String CLASSYFIRE_RESOURCES_PATH = RESOURCES_PATH + "CLASSYFIRE" + File.separator;
    public static final String CLASSYFIRE_ONLINE_NODES_PATH = "http://classyfire.wishartlab.com/";
    public static final String CLASSYFIRE_FILE_NAME = "tax_nodes.json";
    public static final String HMDB_ONLINE_RESOURCES_PATH = "http://www.hmdb.ca/metabolites/";
    public static final String HMDB_FILENAME = "hmdb_metabolites.xml";
    public static final String HMDB_METABOLITE_NODE_NAME = "metabolite";
    public static final String CLASSYFIRE_ONLINE_RESOURCES_PATH = CLASSYFIRE_ONLINE_NODES_PATH + "entities/";
    public static final String LIPIDMAPS_RESOURCES_PATH = RESOURCES_PATH + "lipidMaps" + File.separator;
    public static final String LIPIDMAPS_RESOURCES_INCHIS_PATH = LIPIDMAPS_RESOURCES_PATH + "inchis" + File.separator;
    public static final String LIPIDMAPS_RESOURCES_JSON_PATH = LIPIDMAPS_RESOURCES_PATH + "json" + File.separator;
    public static final String LIPIDMAPS_ONLINE_RESOURCES_PATH = "http://www.lipidmaps.org/rest/compound/lm_id/";
    public static final String LIPIDMAPS_ONLINE_RESOURCES_PATH_END = "/all";
    public static final String LIPIDMAPS_JSON_SYSTEMATIC_NAME = "sys_name";
    public static final String LIPIDMAPS_JSON_COMMON_NAME = "name";
    public static final String LIPIDMAPS_JSON_ABBREV = "abbrev";
    public static final String LIPIDMAPS_JSON_ABBREV_CHAINS = "abbrev_chains";
    public static final String LIPIDMAPS_JSON_CATEGORY = "core";
    public static final String LIPIDMAPS_JSON_MAIN_CLASS = "main_class";
    public static final String LIPIDMAPS_JSON_SUB_CLASS = "sub_class";
    public static final String LIPIDMAPS_JSON_CLASS_LEVEL_4 = "class_level4";
    public static final String LIPIDMAPS_JSON_FORMULA = "formula";
    public static final String LIPIDMAPS_JSON_MASS = "exactmass";
    public static final String LIPIDMAPS_JSON_INCHI = "inchi";
    public static final String LIPIDMAPS_JSON_INCHI_KEY = "inchi_key";
    public static final String LIPIDMAPS_JSON_SMILES = "smiles";
    public static final String HMDB_JSON_KINGDOM = "kingdom";
    public static final String HMDB_JSON_SUPER_CLASS = "superclass";
    public static final String HMDB_JSON_CLASS = "class";
    public static final String HMDB_JSON_SUB_CLASS = "subclass";
    public static final String HMDB_JSON_DIRECT_PARENT = "direct_parent";
    public static final String HMDB_ELEMENTS_NAME = "name";
    public static final String KNAPSACK_ASPERGILLUS_PATH = "http://www.knapsackfamily.com/knapsack_core/result.php?sname=organism&word=aspergillus";
    public static final String KNAPSACK_ELEMENT_PATH = "http://www.knapsackfamily.com/knapsack_core/information.php?word=";
    public static final String KNAPSACK_ELEMENT_REFERENCE_PATH = "http://www.knapsackfamily.com/knapsack_core/information.php?mode=r&word=";
    
    public static final String DOI_WEBPAGE = "https://doi.org/";

    public static final String PUBCHEM_ENDPOINT_COMPOUND = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/";
    public static final String PUBCHEM_ENDPOINT_COMPOUND_NAME = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/";
    public static final String CHEMSPIDER_END_POINT = "https://www.chemspider.com/InChI.asmx/";
    public static final String CHEMSPIDER_SERVICE_INCHI_TO_INCHIKEY = CHEMSPIDER_END_POINT + "InChIToInChIKey";
    public static final String CHEMSPIDER_SERVICE_INCHI_TO_MOL = CHEMSPIDER_END_POINT + "InChIToMol";
    public static final String CHEMSPIDER_SERVICE_INCHI_TO_SMILES = CHEMSPIDER_END_POINT + "InChIToSMILES";
    public static final String CHEMSPIDER_SERVICE_INCHIKEY_TO_INCHI = CHEMSPIDER_END_POINT + "InChIKeyToInChI";
    public static final String CHEMSPIDER_SERVICE_INCHIKEY_TO_MOL = CHEMSPIDER_END_POINT + "InChIKeyToMol";
    public static final String CHEMSPIDER_SERVICE_SMILES_TO_INCHI = CHEMSPIDER_END_POINT + "SMILESToInChI";

    public static final double PRECISSION_TO_BE_SAME_COMPOUND_MASS_UNIFICATION = 0.5d;

    public static final String CLASSYFIRE_NODE_NAME = "name";
    public static final String CLASSYFIRE_NODE_ID = "chemont_id";
    public static final String CLASSYFIRE_NODE_PARENT_ID = "parent_chemont_id";
    public static final String CLASSYFIRE_LIPID_NODE_NAME = "Lipids and lipid-like molecules";
    public static final String CLASSYFIRE_LIPID_NODEID = "CHEMONTID:0000012";

    public static Date LAST_UPDATE_CMM;

    static {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            LAST_UPDATE_CMM = formatter.parse("2018-03-25");
        } catch (ParseException ex) {
            Logger.getLogger(Constants.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static final double PROTON_WEIGTH = 1.0073d;

    static final Map<String, Integer> MAP_SAMPLE_TYPES;

    static {
        Map<String, Integer> temp_map_sample_types = new HashMap<>();
        temp_map_sample_types.put("standard", 1);
        temp_map_sample_types.put("plasma", 2);
        MAP_SAMPLE_TYPES = Collections.unmodifiableMap(temp_map_sample_types);
    }
}
