/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import exceptions.InvalidFormulaException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import testersMSMS.Msms;
import testersMSMS.Peak;
import testersMSMS.Reference;
import testersMSMS.TFG;

/**
 *
 * @author alberto
 */
public final class Utilities {

    private Utilities() {

    }

    public static final Map<String, Double> MAPOXIDATIONS;
    public static final Double WEIGHT_C = 12d;
    public static final Double WEIGHT_H = 1.007825032d;
    public static final Double WEIGHT_O = 15.99491462d;

    static {
        Map<String, Double> mapOXIDATIONSTMP = new LinkedHashMap<String, Double>();
        mapOXIDATIONSTMP.put("O", 13.9793d);
        mapOXIDATIONSTMP.put("OH", 15.9949d);
        mapOXIDATIONSTMP.put("OH-OH", 31.9898d);
        mapOXIDATIONSTMP.put("OOH", 31.9898d);
        mapOXIDATIONSTMP.put("COH", 13.9793d);
        mapOXIDATIONSTMP.put("COOH", 29.9742d);
        MAPOXIDATIONS = Collections.unmodifiableMap(mapOXIDATIONSTMP);
    }

    /**
     *
     * @param numCarbons
     * @param oxidation
     * @param numDoubleBonds
     * @return the mass of the Fatty Acid
     */
    public static Double calculateMassChain(int numCarbons, int numDoubleBonds, String oxidation) {
        double mass;

        double massOxidation = MAPOXIDATIONS.getOrDefault(oxidation, 0d);
        mass = numCarbons * WEIGHT_C
                + (2 * numCarbons - numDoubleBonds * 2) * WEIGHT_H
                + 2 * WEIGHT_O
                + massOxidation;
        return mass;
    }

    /**
     *
     * @param numCarbons
     * @param oxidation
     * @param numDoubleBonds
     * @return the mass of the Fatty Acid
     */
    public static String calculateFormulaChain(int numCarbons, int numDoubleBonds, String oxidation) {
        String formula;

        // TODO CHECK OXIDATIONS WITH JOANNA!
        switch (oxidation) {
            case "O":
                formula = "C" + numCarbons + "H" + (2 * numCarbons - numDoubleBonds * 2 - 2) + "O3";
                break;
            case "OH":
                formula = "C" + numCarbons + "H" + (2 * numCarbons - numDoubleBonds * 2) + "O3";
                break;
            case "OH-OH":
                formula = "C" + numCarbons + "H" + (2 * numCarbons - numDoubleBonds * 2) + "O4";
                break;
            case "OOH":
                formula = "C" + numCarbons + "H" + (2 * numCarbons - numDoubleBonds * 2) + "O4";
                break;
            case "COH":
                formula = "C" + (numCarbons + 1) + "H" + (2 * numCarbons - numDoubleBonds * 2) + "O3";
                break;
            case "COOH":
                formula = "C" + (numCarbons + 1) + "H" + (2 * numCarbons - numDoubleBonds * 2) + "O4";
                break;
            default:
                formula = "C" + numCarbons + "H" + (2 * numCarbons - numDoubleBonds * 2) + "O2";
                break;
        }

        return formula;
    }

    /**
     *
     * @param mass
     * @return
     */
    public static int getPrecission(Double mass) throws NullPointerException {
        if (mass == null) {
            NullPointerException npe = new NullPointerException("nullException");
            throw npe;
        } else {
            return mass.toString().split("\\.")[1].length();
        }
    }

    /**
     *
     * @param mass
     * @return
     */
    public static int getPrecission(String mass) {
        if (mass == null) {
            NullPointerException npe = new NullPointerException("nullException");
            throw npe;
        } else {
            Double.parseDouble(mass);
        }
        return mass.toString().split("\\.")[1].length();
    }

    public static Msms readMsmsFromFile(File file) {

        ReaderXML readerXML = new ReaderXML();
        Msms msms = new Msms();
        List<Reference> references = new LinkedList();
        List<Peak> peaks = new LinkedList();
        String line;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder b = dbf.newDocumentBuilder();
            Document dom = b.parse(file);
            org.w3c.dom.Element doc = dom.getDocumentElement();
            //here we read the msms
            line = readerXML.getTextValue(doc, "collision-energy-level");
            msms.setVoltage_level(line);

            line = readerXML.getTextValue(doc, "collision-energy-voltage");
            msms.setVoltage(line);

            line = readerXML.getTextValue(doc, "database-id");

            String substring1 = "HMDB";

            String substring2 = line.substring(4);

            while (line.length() != 11) {

                String zero = "0";
                substring2 = zero.concat(substring2);
                line = substring1.concat(substring2);

            }
            //System.out.println(line);
            msms.setHmdb_id(line);

            line = readerXML.getTextValue(doc, "id");
            msms.setId(Integer.parseInt(line));

            line = readerXML.getTextValue(doc, "predicted");
            msms.setPredicted(line);

            line = readerXML.getTextValue(doc, "instrument-type");
            msms.setInstrument_type(line);

            line = readerXML.getTextValue(doc, "ionization-mode");
            msms.setIonization_mode(line);

            line = readerXML.getTextValue(doc, "peak-counter");
            msms.setPeak_count(Integer.parseInt(line));

            //now read references
            List<org.w3c.dom.Element> ref_list = readerXML.findAllElementsByTagName(doc, "reference");

            for (int i = 0; i < ref_list.size(); i++) {
                Reference r = new Reference();
                line = readerXML.getTextValue(ref_list.get(i), "id");
                r.setId(Integer.parseInt(line));

                line = readerXML.getTextValue(ref_list.get(i), "pubmed-id");
                r.setPubmed_id(line);

                line = readerXML.getTextValue(ref_list.get(i), "ref-text");
                r.setRef_text(line);

                line = readerXML.getTextValue(ref_list.get(i), "spectra-id");
                r.setMsms_id(Integer.parseInt(line));
                //System.out.println(r.toString());
                references.add(r);
            }

            msms.setReferences(references);

            List<org.w3c.dom.Element> peak_list = ReaderXML.findAllElementsByTagName(doc, "ms-ms-peak");

            //System.out.println(peak_list.size());
            for (int j = 0; j < peak_list.size(); j++) {

                Peak p = new Peak();
                line = readerXML.getTextValue(peak_list.get(j), "id");
                //System.out.println(line);
                p.setId(Integer.parseInt(line));

                line = readerXML.getTextValue(peak_list.get(j), "intensity");
                //System.out.println(line);
                p.setIntensity(Double.parseDouble(line));

                line = readerXML.getTextValue(peak_list.get(j), "mass-charge");
                //System.out.println(line);
                p.setMz(Double.parseDouble(line));

                line = readerXML.getTextValue(peak_list.get(j), "ms-ms-id");
                p.setMsms_id(Integer.parseInt(line));
                //System.out.println(line);
                //System.out.println(p.toString());//aquí funciona

                peaks.add(p);
                //System.out.println("Dentro del bucle: "+peaks);
            }
            //System.out.println("fuera del bucle: "+peaks);
            msms.setPeaks(peaks);

            //System.out.println(msms.toString());
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(TFG.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(TFG.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TFG.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msms;
    }

    public static void readMsmsFromFileForLog(String source, String destination) {
        ReaderXML readerXML = new ReaderXML();

        // LOOP READING ALL FILES
        //String directory = ("C:/Users/María 606888798/Desktop/CMM/resources/MSMS/hmdb_spectra_xml/");
        File dir_read = new File(source);
        String[] file_names = dir_read.list();

        //DIRECTORY where the error files will be
        File dir_write = new File(destination);
        //creating all the files where the errors will be located
        File error_msmsid = new File(destination + "MSMSID.txt");
        File error_hmdbid = new File(destination + "HMDBID.txt");
        File error_predicted = new File(destination + "PREDICTED.txt");
        File error_voltage = new File(destination + "VOLTAGE.txt");
        File error_level = new File(destination + "LEVEL.txt");
        File error_instrument = new File(destination + "INSTRUMENT.txt");
        File error_ionization = new File(destination + "IONIZATION.txt");
        File error_peakcount = new File(destination + "PEAKCOUNT.txt");
        File error_refid = new File(destination + "REFID.txt");
        File error_reftext = new File(destination + "REFTEXT.txt");
        File error_pubmed = new File(destination + "PUBMED.txt");
        File error_peakid = new File(destination + "PEAKID.txt");
        File error_intensity = new File(destination + "INTENSITY.txt");
        File error_mz = new File(destination + "MZ.txt");
        ///////////////////////////////////////////////////////////////////////

        //Now we check one by one the elements of the msms.xmls in order to create text files containing the errors.
        for (String file_name : file_names) {
            //only take the files containing msms information
            if (file_name.contains("HMDB")) {
                //System.out.println(directory + file);
                if (file_name.contains("_ms_ms_")) {
                    File file = new File(source + file_name);
                    System.out.println("READING");
                    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                    try {
                        DocumentBuilder b = dbf.newDocumentBuilder();
                        Document dom = b.parse(file);
                        org.w3c.dom.Element doc = dom.getDocumentElement();
                        String test;

                        //here we read the msms
                        //check level
                        test = readerXML.getTextValue(doc, "predicted");
                        ifnull(test, file, error_predicted);

                        test = readerXML.getTextValue(doc, "collision-energy-level");
                        ifnull(test, file, error_level);

                        //check collision 
                        test = readerXML.getTextValue(doc, "collision-energy-voltage");
                        ifnull(test, file, error_voltage);

                        //check hmdbid 
                        test = readerXML.getTextValue(doc, "database-id");
                        ifnull(test, file, error_hmdbid);

                        //check msms id
                        test = readerXML.getTextValue(doc, "id");
                        ifnull(test, file, error_msmsid);

                        //check instrument
                        test = readerXML.getTextValue(doc, "instrument-type");
                        ifnull(test, file, error_instrument);

                        //check ionization
                        test = readerXML.getTextValue(doc, "ionization-mode");
                        ifnull(test, file, error_ionization);

                        //check peakcount
                        test = readerXML.getTextValue(doc, "peak-counter");
                        ifnull(test, file, error_peakcount);

                        //now references
                        List<org.w3c.dom.Element> ref_list = readerXML.findAllElementsByTagName(doc, "reference");

                        for (int i = 0; i < ref_list.size(); i++) {

                            test = readerXML.getTextValue(ref_list.get(i), "id");
                            ifnull(test, file, error_refid);
                            test = readerXML.getTextValue(ref_list.get(i), "pubmed-id");
                            ifnull(test, file, error_pubmed);
                            test = readerXML.getTextValue(ref_list.get(i), "ref-text");
                            ifnull(test, file, error_reftext);

                        }

                        //now peaks 
                        List<org.w3c.dom.Element> peak_list = readerXML.findAllElementsByTagName(doc, "ms-ms-peak");

                        for (int j = 0; j < peak_list.size(); j++) {
                            test = readerXML.getTextValue(peak_list.get(j), "id");
                            ifnull(test, file, error_peakid);
                            test = readerXML.getTextValue(peak_list.get(j), "intensity");
                            ifnull(test, file, error_intensity);
                            test = readerXML.getTextValue(peak_list.get(j), "mass-charge");
                            ifnull(test, file, error_mz);

                        }

                        //System.out.println(msms.toString());
                    } catch (ParserConfigurationException ex) {
                        Logger.getLogger(TFG.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (SAXException ex) {
                        Logger.getLogger(TFG.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(TFG.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }

        }
    }

    public static void ifnull(String test, File source, File error) throws IOException {
        if (test.equals("null")) {
            FileWriter f = new FileWriter(error, true);
            PrintWriter pw = new PrintWriter(f, true);
            pw.println(source.getName());
            pw.close();
            f.close();
        }
    }

    public static boolean isDouble(String word) {
        try {
            Double.parseDouble(word);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    public static String escapeSQL(String value) {
        value = value.replace("\\", "\\\\");
        value = value.replaceAll("\"", "\\'");
        value = value.replaceAll("'", "\\'");
        value = value.replace("\'", "\\'");

        return value;
    }
    
    public static String getAdductNoBrackets(String adductName) {
        adductName = adductName.replace("[", "");
        adductName = adductName.replaceAll("][0-9]*[+|-]", "");

        return adductName;
    }

    public static final Map<String, Integer> MAPCHEMALPHABET;

    static {
        Map<String, Integer> mapChemAlphabetTMP = new LinkedHashMap<>();
        mapChemAlphabetTMP.put("CHNOPS", 0);
        mapChemAlphabetTMP.put("CHNOPSD", 1);
        mapChemAlphabetTMP.put("CHNOPSCL", 2);
        mapChemAlphabetTMP.put("CHNOPSCLD", 3);
        mapChemAlphabetTMP.put("ALL", 4);
        mapChemAlphabetTMP.put("ALLD", 5);
        MAPCHEMALPHABET = Collections.unmodifiableMap(mapChemAlphabetTMP);
    }

    /**
     *
     * @param inputChemAlphabet
     * @return the formula type as int according to the chemAlphabet or 4 (ALL) by default (No deuterium)
     */
    public static int getIntChemAlphabet(String inputChemAlphabet) {
        int intChemAlphabet = MAPCHEMALPHABET.getOrDefault(inputChemAlphabet, 4);
        return intChemAlphabet;
    }

    public static List<Integer> getSampleTypes(String sampleType) {
        if (sampleType == null) {
            return new LinkedList();
        } else {
            List<Integer> sampleTypesInt = new LinkedList();
            String[] sampleTypesString = sampleType.split("\n");
            for (int i = 0; i < sampleTypesString.length; i++) {
                sampleTypesInt.add(Constants.MAP_SAMPLE_TYPES.get(sampleTypesString[i].trim()));
            }
            return sampleTypesInt;
        }
    }

    public static Double getMassFromFormula(String formulaString) throws InvalidFormulaException {

        Double mass = 0d;

        Formula formula = new Formula(formulaString);
        for (Element e : formula.getElements()) {
            for (Element e_table : PeriodicTable.MAPELEMENTS.keySet()) {
                if (e.equals(e_table)) {
                    mass = mass + formula.getElementQuantity(e) * PeriodicTable.MAPELEMENTS.get(e);
                }

            }
        }
        return mass;
    }
    
    public static Map<String, Integer> readHeaders(String path) throws IOException {
        File excelFile = new File(path);
        Map<String, Integer> mapHeaders;
        // we create an XSSF Workbook object for our XLSX Excel File
        try (FileInputStream fis = new FileInputStream(excelFile); // we create an XSSF Workbook object for our XLSX Excel File
                XSSFWorkbook workbook = new XSSFWorkbook(fis)) {
            // we get first sheet
            XSSFSheet sheet = workbook.getSheetAt(0);
            mapHeaders = new HashMap<>(); //Create map
            XSSFRow firstRow = sheet.getRow(0); //Get first row
            //following is boilerplate from the java doc
            short minColIx = firstRow.getFirstCellNum(); //get the first column index for a row
            short maxColIx = firstRow.getLastCellNum(); //get the last column index for a row
            for (short colIx = minColIx; colIx < maxColIx; colIx++) { //loop from first to last index
                XSSFCell cell = firstRow.getCell(colIx); //get the cell
                mapHeaders.put(cell.getStringCellValue(), cell.getColumnIndex()); //add the cell contents (name of column) and cell index to the map
            }
            for (String key : mapHeaders.keySet()) {
                //System.out.println("KEY: " + key + " IDX: " + mapNames.get(key));
                //System.out.print("\"" + key + "\", ");
                //System.out.print("CE_FILE_HEADERS." + key + ", ");
            }
        }
        return mapHeaders;
    }

}

// CREATE ONE DIFFERENT FILE FOR SAVING ATRIBBUTES TO CHECK

