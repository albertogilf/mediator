/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CE.MS;

import CE.MS.CE_Exp_properties.CE_BUFFER_ENUM;
import CE.MS.CE_Exp_properties.CE_EXP_PROP_ENUM;
import exceptions.IncorrectBufferException;
import exceptions.IncorrectIonizationModeException;
import exceptions.IncorrectPolarityException;
import exceptions.IncorrectTemperatureException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ceu
 */
public class CE_Exp_propertiesTest {

    public CE_Exp_propertiesTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void whenConvertedIntoCE_BufferEnum_thenGetsConvertedCorrectly() {

        String buffer_enum_value = "ACETIC";

        CE_BUFFER_ENUM ce_buffer_enum
                = CE_BUFFER_ENUM.valueOf(buffer_enum_value);
        assertTrue(ce_buffer_enum == CE_BUFFER_ENUM.ACETIC);
    }

    @Test
    public void fromBufferCodeCorrect() {

        try {
            Integer buffer_enum_code = 1;

            CE_BUFFER_ENUM ce_buffer_enum
                    = CE_BUFFER_ENUM.fromBufferCode(buffer_enum_code);
            assertTrue(ce_buffer_enum == CE_BUFFER_ENUM.FORMIC);
        } catch (IncorrectBufferException ex) {
            assertFalse(true);
        }
    }

    @Test
    public void fromBufferCodeInCorrect() {

        try {
            Integer buffer_enum_code = 3;

            CE_BUFFER_ENUM ce_buffer_enum
                    = CE_BUFFER_ENUM.fromBufferCode(buffer_enum_code);
            assertTrue(false);
        } catch (IncorrectBufferException ex) {
            assertTrue(true);
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenConvertedIntoCE_BufferEnum_thenGetsTrhowsException() {

        String buffer_enum_value = "acetic";

        CE_BUFFER_ENUM ce_buffer_enum
                = CE_BUFFER_ENUM.valueOf(buffer_enum_value);
        assertTrue(ce_buffer_enum == CE_BUFFER_ENUM.ACETIC);
    }

    @Test
    public void fromBufferTemperatureAndPolarityTestCorrect() {
        try {
            CE_EXP_PROP_ENUM ce_exp_prop_enum = CE_EXP_PROP_ENUM.fromBufferTemperatureIonModeAndPolarity(CE_BUFFER_ENUM.FORMIC, 20,1,1);
            assertTrue(ce_exp_prop_enum == CE_EXP_PROP_ENUM.FORMIC_20_POS_DIR);
        } catch (IncorrectTemperatureException | IncorrectBufferException 
                | IncorrectIonizationModeException | IncorrectPolarityException ex) {
            assertTrue((false));
                }
    }

    @Test
    public void fromBufferTemperatureAndPolarityTestIncorrectTemperature() {
        try {
            CE_EXP_PROP_ENUM ce_exp_prop_enum = CE_EXP_PROP_ENUM.fromBufferTemperatureIonModeAndPolarity(CE_BUFFER_ENUM.FORMIC, 21,1, 1);
        } catch (IncorrectTemperatureException ex) {
            assertTrue(true);
        } catch (IncorrectBufferException | IncorrectIonizationModeException | IncorrectPolarityException ex) {
            assertTrue((false));
        }
    }

}
