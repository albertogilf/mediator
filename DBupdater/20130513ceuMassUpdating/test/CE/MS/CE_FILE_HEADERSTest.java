/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CE.MS;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ceu
 */
public class CE_FILE_HEADERSTest {

    public CE_FILE_HEADERSTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of values method, of class CE_FILE_HEADERS.
     */
    @Test
    public void testValues() {
        System.out.println("values of headers test");
        CE_FILE_HEADERS[] expResult = {CE_FILE_HEADERS.CMM_ID, CE_FILE_HEADERS.MZ_THEORETICAL, 
            CE_FILE_HEADERS.METLIN_LINK, CE_FILE_HEADERS.LM_ID, CE_FILE_HEADERS.CLASS_II, 
            CE_FILE_HEADERS.METLIN_ID, CE_FILE_HEADERS.CLASS_I, CE_FILE_HEADERS.HMDB_ID, 
            CE_FILE_HEADERS.INCHI, CE_FILE_HEADERS.MONOISOTOPIC_MASS, CE_FILE_HEADERS.ID_CODE, 
            CE_FILE_HEADERS.FRAGMENTS_200V_AND_ADDUCTS_100V, CE_FILE_HEADERS.MZ_EXPERIMENTAL, 
            CE_FILE_HEADERS.MT_PLASMA, CE_FILE_HEADERS.LM_LINK, CE_FILE_HEADERS.MT, 
            CE_FILE_HEADERS.HMDB_LINK, CE_FILE_HEADERS.MOLECULAR_FORMULA, 
            CE_FILE_HEADERS.CAS_ID, CE_FILE_HEADERS.LEVEL, CE_FILE_HEADERS.NAME, 
            CE_FILE_HEADERS.SAMPLE, CE_FILE_HEADERS.RMT_PLASMA, CE_FILE_HEADERS.RMT, 
            CE_FILE_HEADERS.COMMERCIAL};
        CE_FILE_HEADERS[] result = CE_FILE_HEADERS.values();
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of valueOf method, of class CE_FILE_HEADERS.
     */
    @Test
    public void testValueOf() {
        System.out.println("value of CMM header test");
        String name = "CMM_ID";
        CE_FILE_HEADERS expResult = CE_FILE_HEADERS.CMM_ID;
        CE_FILE_HEADERS result = CE_FILE_HEADERS.valueOf(name);
        assertEquals(expResult, result);
    }

}
