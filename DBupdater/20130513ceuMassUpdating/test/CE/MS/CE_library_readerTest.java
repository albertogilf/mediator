/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CE.MS;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static utilities.Utilities.readHeaders;

/**
 *
 * @author Alberto Gil de la Fuente
 */
public class CE_library_readerTest {

    public CE_library_readerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of main method, of class CE_library_reader.
     */
    /*
    @Test
    public void testMain() throws Exception {
        System.out.println("main test");
        String[] args = null;
        CE_library_reader.main(args);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
     */
    @Test
    public void testHEADERS() throws Exception {
        System.out.println("file headers test");
        String path = "resources/CE/CE_MS_precursor_ions.xlsx";
        CE_FILE_HEADERS[] expResult = {CE_FILE_HEADERS.CMM_ID, CE_FILE_HEADERS.MZ_THEORETICAL,
            CE_FILE_HEADERS.METLIN_LINK, CE_FILE_HEADERS.LM_ID, CE_FILE_HEADERS.CLASS_II,
            CE_FILE_HEADERS.METLIN_ID, CE_FILE_HEADERS.CLASS_I, CE_FILE_HEADERS.HMDB_ID,
            CE_FILE_HEADERS.INCHI, CE_FILE_HEADERS.MONOISOTOPIC_MASS, CE_FILE_HEADERS.ID_CODE,
            CE_FILE_HEADERS.FRAGMENTS_200V_AND_ADDUCTS_100V, CE_FILE_HEADERS.MZ_EXPERIMENTAL,
            CE_FILE_HEADERS.MT_PLASMA, CE_FILE_HEADERS.LM_LINK, CE_FILE_HEADERS.MT,
            CE_FILE_HEADERS.HMDB_LINK, CE_FILE_HEADERS.MOLECULAR_FORMULA,
            CE_FILE_HEADERS.CAS_ID, CE_FILE_HEADERS.LEVEL, CE_FILE_HEADERS.NAME,
            CE_FILE_HEADERS.SAMPLE, CE_FILE_HEADERS.RMT_PLASMA, CE_FILE_HEADERS.RMT,
            CE_FILE_HEADERS.COMMERCIAL};

        CE_FILE_HEADERS[] result = new CE_FILE_HEADERS[expResult.length];
        Map<String, Integer> headersMap = readHeaders(path);
        List<String> headersString = new ArrayList<String>(headersMap.keySet());
        for (int i = 0; i < headersString.size(); i++) {
            result[i] = CE_FILE_HEADERS.valueOf(headersString.get(i));
        }
        assertArrayEquals(expResult, result);
    }

}
