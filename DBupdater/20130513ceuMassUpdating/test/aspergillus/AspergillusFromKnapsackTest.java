/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aspergillus;

import ioDevices.MyFile;
import java.util.Set;
import java.util.TreeSet;
import model.OrganismClassification;
import model.Reference;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static utilities.Constants.KNAPSACK_RESOURCES_PATH;

/**
 *
 * @author alberto.gildelafuent
 */
public class AspergillusFromKnapsackTest {

    String knapSackFileName;
    String knapSackHTML;

    public AspergillusFromKnapsackTest() {
        knapSackFileName = KNAPSACK_RESOURCES_PATH + "test" + ".html";
        knapSackHTML = MyFile.obtainContentOfABigFile(knapSackFileName).toString();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getNameFromKnapSackFile method, of class AspergillusFromKnapsack.
     */
    @Test
    public void testGetNameFromKnapSackFile() throws Exception {
        System.out.println("getNameFromKnapSackFile");
        String expResult = "Cholesterol\n"
                + "Cholesterin\n"
                + "Cholest-5-en-3beta-ol";
        String result = AspergillusFromKnapsack.getNameFromKnapSackFile(knapSackHTML);
        assertEquals(expResult, result);
    }

    /**
     * Test of getFormulaFromKnapSackFile method, of class
     * AspergillusFromKnapsack.
     */
    @Test
    public void testGetFormulaFromKnapSackFile() throws Exception {
        System.out.println("getFormulaFromKnapSackFile");
        String expResult = "C27H46O";
        String result = AspergillusFromKnapsack.getFormulaFromKnapSackFile(knapSackHTML);
        assertEquals(expResult, result);
    }

    /**
     * Test of getMassFromKnapSackFile method, of class AspergillusFromKnapsack.
     */
    @Test
    public void testGetMassFromKnapSackFile() throws Exception {
        System.out.println("getMassFromKnapSackFile");
        Double expResult = 386.35486609;
        Double result = AspergillusFromKnapsack.getMassFromKnapSackFile(knapSackHTML);
        assertEquals(expResult, result, 0.0005d);
    }

    /**
     * Test of getCASFromKnapSackFile method, of class AspergillusFromKnapsack.
     */
    @Test
    public void testGetCASFromKnapSackFile() throws Exception {
        System.out.println("getCASFromKnapSackFile");
        String expResult = "57-88-5";
        String result = AspergillusFromKnapsack.getCASFromKnapSackFile(knapSackHTML);
        assertEquals(expResult, result);
    }

    /**
     * Test of getInChIKeyFromKnapsackFile method, of class
     * AspergillusFromKnapsack.
     */
    @Test
    public void testGetInChIKeyFromKnapsackFile() throws Exception {
        System.out.println("getInChIKeyFromKnapsackFile");
        String expResult = "HVYWMOMLDIMFJA-ZMWIUTBPNA-N";
        String result = AspergillusFromKnapsack.getInChIKeyFromKnapsackFile(knapSackHTML);
        assertEquals(expResult, result);
    }

    /**
     * Test of getInChIFromKnapsackFile method, of class
     * AspergillusFromKnapsack.
     */
    @Test
    public void testGetInChIFromKnapsackFile() throws Exception {
        System.out.println("getInChIFromKnapsackFile");
        String expResult = "InChI=1S/C27H46O/c1-18(2)7-6-8-19(3)23-11-12-24-22-10-9-20-17-21(28)13-15-26(20,4)25(22)14-16-27(23,24)5/h9,18-19,21-25,28H,6-8,10-17H2,1-5H3/t19-,21+,22+,23-,24+,25+,26+,27-/m1/s1";
        String result = AspergillusFromKnapsack.getInChIFromKnapsackFile(knapSackHTML);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSMILESFromKnapsackFile method, of class
     * AspergillusFromKnapsack.
     */
    @Test
    public void testGetSMILESFromKnapsackFile() throws Exception {
        System.out.println("getSMILESFromKnapsackFile");
        String expResult = "C1[C@@H](CC2=CC[C@@H]3[C@@H]([C@]2(C1)C)CC[C@]1([C@H]3CC[C@@H]1[C@@H](CCCC(C)C)C)C)O";
        String result = AspergillusFromKnapsack.getSMILESFromKnapsackFile(knapSackHTML);
        assertEquals(expResult, result);
    }

    /**
     * Test of getOrganismFromHTML method, of class AspergillusFromKnapsack.
     */
    @Test
    public void testGetOrganismFromHTML() throws Exception {
        System.out.println("getOrganismFromHTML");
        String knapSackId = "C00003648";
        String organismHTML = "<td class=org>Fungi</td><td class=org>Acaulosporaceae</td><td class=org2>Acaulospora laevis</td><td class=org><a href=information.php?mode=r&word=C00003648&key=4 title=Acaulospora laevis > Ref.</a></td>";
        Set<Reference> references = new TreeSet<Reference>();
        references.add(new Reference(1, "Nes,Analysis of Sterols and Other Biologically Significant Steroids,Academic Press Inc.,New York,Ny,341 pp.(1989)"));
        references.add(new Reference(2, "Turner,Fungal Metabolites II,Academic Press,New York,NY,631 pp.(1983)"));
        String kingdom = "Fungi";
        String family = "Acaulosporaceae";
        String species = "Acaulospora laevis";
        OrganismClassification expResult = new OrganismClassification(1, kingdom, family, species, references);
        OrganismClassification result = AspergillusFromKnapsack.getOrganismFromHTML(knapSackId, organismHTML);
        assertEquals(expResult, result);
    }

    /**
     * Test of getReferencesFromKnapsackIdAndRefNumber method, of class
     * AspergillusFromKnapsack.
     */
    @Test
    public void testGetReferenceTextFromKnapsackIdAndRefNumber() throws Exception {
        System.out.println("getReferenceTextFromKnapsackIdAndRefNumber");
        String knapSackId = "C00003648";
        Integer numReference = 4;
        Set<Reference> expResult = new TreeSet<Reference>();
        expResult.add(new Reference(1, "Nes,Analysis of Sterols and Other Biologically Significant Steroids,Academic Press Inc.,New York,Ny,341 pp.(1989)"));
        expResult.add(new Reference(2, "Turner,Fungal Metabolites II,Academic Press,New York,NY,631 pp.(1983)"));
        Set<Reference> result = AspergillusFromKnapsack.getReferencesFromKnapsackIdAndRefNumber(knapSackId, numReference);
        assertEquals(expResult, result);
    }

    /**
     * Test of getOrganisms method, of class AspergillusFromKnapsack.
     */
    @Test
    public void testGetOrganisms() throws Exception {
        System.out.println("getOrganisms: C00000119");
        String knapSackId = "C00000119";
        String specificFileName = KNAPSACK_RESOURCES_PATH + knapSackId + ".html";
        String contentKnapSackFile = MyFile.obtainContentOfABigFile(specificFileName).toString();
        String kingdom = "Fungi";
        String family = "Trichocomaceae";
        String species = "Aspergillus ochraceus";
        String cells = "Wilhelm";
        Set<Reference> references = new TreeSet<Reference>();
        references.add(new Reference(1, "Moore,Appl.Microbiol.,23,(1972),1067"));
        OrganismClassification organism1 = new OrganismClassification(1, kingdom, family, species, cells, references);
        Set<OrganismClassification> expResult = new TreeSet();
        expResult.add(organism1);
        Set<OrganismClassification> result = AspergillusFromKnapsack.getOrganisms(knapSackId, contentKnapSackFile);
        assertEquals(expResult, result);
        
        // Here it starts the second one
    }
    
    @Test
    public void testGetOrganisms2() throws Exception {
        System.out.println("getOrganisms: C00000486");
        String knapSackId = "C00000486";
        String specificFileName = KNAPSACK_RESOURCES_PATH + knapSackId + ".html";
        String contentKnapSackFile = MyFile.obtainContentOfABigFile(specificFileName).toString();
        String kingdom = "Bacteria";
        String family = "Streptomycetaceae";
        String species = "Streptomyces coelicolor";
        String cells = null;
        Set<Reference> references = new TreeSet<Reference>();
        references.add(new Reference(1, "Bedford,J.Bacteriol,177,(1995),4544"));
        OrganismClassification organism1 = new OrganismClassification(1, kingdom, family, species, cells, references);
        
        kingdom = "Bacteria";
        family = "Streptomycetaceae";
        species = "Streptomyces viridochromogenes";
        cells = "Tu57";
        Set<Reference> references2 = new TreeSet<Reference>();
        references2.add(new Reference(1, "Bedford,J.Bacteriol,177,(1995),4544"));
        OrganismClassification organism2 = new OrganismClassification(2, kingdom, family, species, cells, references2);
        
        kingdom = "Fungi";
        family = "Botryosphaeriaceae";
        species = "Phyllosticta spp.";
        cells = null;
        Set<Reference> references3 = new TreeSet<Reference>();
        references3.add(new Reference(1, "Harborne,Phytochemical Dictionary Second Edition,Taylor and Francis,(1999),Chapter40"));
        OrganismClassification organism3 = new OrganismClassification(3, kingdom, family, species, cells, references3);
        
        kingdom = "Fungi";
        family = "Trichocomaceae";
        species = "Aspergillus terreus";
        cells = "IMI16043";
        Set<Reference> references4 = new TreeSet<Reference>();
        references4.add(new Reference(1, "Fujii,Biosci.Biotech.Biochem,59,(1995),1869"));
        OrganismClassification organism4 = new OrganismClassification(4, kingdom, family, species, cells, references4);
        
        kingdom = "Fungi";
        family = "Trichocomaceae";
        species = "Penicillium spp.";
        cells = null;
        Set<Reference> references5 = new TreeSet<Reference>();
        references5.add(new Reference(1, "Harborne,Phytochemical Dictionary Second Edition,Taylor and Francis,(1999),Chapter40"));
        OrganismClassification organism5 = new OrganismClassification(5, kingdom, family, species, cells, references5);

        kingdom = "Plantae";
        family = "Hydrophyllaceae";
        species = "Eriodictyon angustifolium";
        cells = null;
        Set<Reference> references6 = new TreeSet<Reference>();
        references6.add(new Reference(1, "Harborne,Phytochemical Dictionary Second Edition,Taylor and Francis,(1999),Chapter40"));
        OrganismClassification organism6 = new OrganismClassification(6, kingdom, family, species, cells, references6);        
        
        Set<OrganismClassification> expResult = new TreeSet();
        expResult.add(organism1);
        expResult.add(organism2);
        expResult.add(organism3);
        expResult.add(organism4);
        expResult.add(organism5);
        expResult.add(organism6);
        Set<OrganismClassification> result = AspergillusFromKnapsack.getOrganisms(knapSackId, contentKnapSackFile);
        assertEquals(expResult, result);
        
        // Here it starts the second one
    }
    
    
    

}
