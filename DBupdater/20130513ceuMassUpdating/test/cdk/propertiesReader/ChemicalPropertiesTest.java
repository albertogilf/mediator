/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cdk.propertiesReader;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.qsar.DescriptorValue;
import org.openscience.cdk.qsar.descriptors.molecular.HBondAcceptorCountDescriptor;
import org.openscience.cdk.qsar.descriptors.molecular.HBondDonorCountDescriptor;

/**
 *
 * @author ceu
 */
public class ChemicalPropertiesTest {
    
    public ChemicalPropertiesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of Read method, of class ChemicalProperties.
     * @throws java.lang.Exception
     */
    @Test
    public void testRead() throws Exception {
        // TODO FROM MOL FILES IN LAB COMPUTER!
        System.out.println("Read");
        String file = "";
        IAtomContainer expResult = null;
        IAtomContainer result = ChemicalProperties.Read(file);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of ReadFromString method, of class ChemicalProperties.
     * @throws java.lang.Exception
     */
    @Test
    public void testReadFromString() throws Exception {
        System.out.println("ReadFromString");
        String molString = "\n  Mrv1652303211804362D          \n" +
"\n" +
" 12 12  0  0  0  0            999 V2000\n" +
"10000.709810001.1466    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
"10001.424610000.7345    0.0000 C   0  0  1  0  0  0  0  0  0  0  0  0\n" +
"10002.137310001.1466    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
"10002.852210000.7345    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
"10002.137310001.9705    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n" +
"10001.4246 9999.9105    0.0000 N   0  0  0  0  0  0  0  0  0  0  0  0\n" +
" 9998.4154 9999.2970    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
" 9999.326310001.2290    0.0000 N   0  0  0  0  0  0  0  0  0  0  0  0\n" +
" 9998.658910000.7439    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
" 9998.9138 9999.9592    0.0000 N   0  0  0  0  0  0  0  0  0  0  0  0\n" +
" 9999.7389 9999.9592    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
" 9999.993810000.7439    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n" +
"  1  2  1  0  0  0  0\n" +
"  2  3  1  0  0  0  0\n" +
"  2  6  1  1  0  0  0\n" +
"  3  4  1  0  0  0  0\n" +
"  3  5  2  0  0  0  0\n" +
" 11 12  2  0  0  0  0\n" +
"  8  9  2  0  0  0  0\n" +
"  8 12  1  0  0  0  0\n" +
"  9 10  1  0  0  0  0\n" +
" 10 11  1  0  0  0  0\n" +
" 10  7  1  0  0  0  0\n" +
"  1 12  1  0  0  0  0\n" +
"M  END";
        int expResult = 2;
        IAtomContainer result = ChemicalProperties.ReadFromString(molString);
        HBondDonorCountDescriptor HBondDonor = new HBondDonorCountDescriptor();
        DescriptorValue descriptor = HBondDonor.calculate(result);
        int numHBondDonor = Integer.parseInt(descriptor.getValue().toString());
        assertEquals(expResult, numHBondDonor);
        expResult = 4;
        HBondAcceptorCountDescriptor HBondAcceptor = new HBondAcceptorCountDescriptor();
        descriptor = HBondAcceptor.calculate(result);
        int numHBondAceptor = Integer.parseInt(descriptor.getValue().toString());
        assertEquals(expResult, numHBondAceptor);
    }
    
}
