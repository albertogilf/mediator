/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databases;

import exceptions.ChebiException;
import model.Identifier;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author alberto.gildelafuent
 */
public class ChebiDatabaseTest {
    
    public ChebiDatabaseTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getInChIFromChebID method, of class ChebiDatabase.
     */
    @Test
    public void testGetInChIFromChebID() throws Exception {
        System.out.println("getInChIFromChebID");
        int chebId = 42223;
        String expResult = "InChI=1S/C15H10O5/c1-6-2-8-12(10(17)3-6)15(20)13-9(14(8)19)4-7(16)5-11(13)18/h2-5,16-18H,1H3";
        String result = ChebiDatabase.getInChIFromChebID(chebId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getInChIKeyFromChebID method, of class ChebiDatabase.
     */
    @Test
    public void testGetInChIKeyFromChebID() throws Exception {
        System.out.println("getInChIKeyFromChebID");
        int chebId = 42223;
        String expResult = "RHMXXJGYXNZAPX-UHFFFAOYSA-N";
        String result = ChebiDatabase.getInChIKeyFromChebID(chebId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getFormulaFromChebID method, of class ChebiDatabase.
     */
    @Test
    public void testGetFormulaFromChebID() throws Exception {
        System.out.println("getFormulaFromChebID");
        int chebId = 42223;
        String expResult = "C15H10O5";
        String result = ChebiDatabase.getFormulaFromChebID(chebId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getMonoIsotopicMassFromChebID method, of class ChebiDatabase.
     */
    @Test
    public void testGetMonoIsotopicMassFromChebID() throws Exception {
        System.out.println("getMonoIsotopicMassFromChebID");
        int chebId = 42223;
        Double expResult = 270.05282;
        Double result = ChebiDatabase.getMonoIsotopicMassFromChebID(chebId);
        assertEquals(expResult, result, 0.001D);
    }

    /**
     * Test of getHMDBLink method, of class ChebiDatabase.
     */
    @Test
    public void testGetHMDBLink() {
        System.out.println("getHMDBLink");
        int chebId = 166764;
        String expResult = "HMDB0011195";
        String result = ChebiDatabase.getHMDBLink(chebId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getChebiFromIdentifiers method, of class ChebiDatabase.
     */
    @Test
    public void testGetChebiFromIdentifiers() throws ChebiException {
        System.out.println("getChebiFromIdentifiers");
        Integer compound_id = 17544;
        String inchi = "InChI=1S/C15H10O5/c1-6-2-8-12(10(17)3-6)15(20)13-9(14(8)19)4-7(16)5-11(13)18/h2-5,16-18H,1H3";
        String inchi_key = "RHMXXJGYXNZAPX-UHFFFAOYSA-N";
        String smiles = "CC1=CC2=C(C(O)=C1)C(=O)C1=C(C=C(O)C=C1O)C2=O";
        
        Identifier identifiers = new Identifier(inchi, inchi_key, smiles);
        Integer expResult = 42223;
        Integer result = ChebiDatabase.getChebiFromIdentifiers(identifiers);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testGetChebiNumber() {
        System.out.println("getChebiNumber");
        String chebId = "CHEBI:42223";
        
        Integer expResult = 42223;
        Integer result = ChebiDatabase.getChebiNumber(chebId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAsciiName method, of class ChebiDatabase.
     */
    @Test
    public void testGetFullEntity() throws Exception {
        System.out.println("getFullEntity");
        int chebId = 10352;
        String expResult = "beta-amyrin";
        String result = ChebiDatabase.getAsciiName(chebId);
        assertEquals(expResult,result);
    }
    
}
