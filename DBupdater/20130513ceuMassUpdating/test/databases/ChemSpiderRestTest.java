/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databases;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author alberto.gildelafuent
 */
public class ChemSpiderRestTest {

    private static String inchi_input = "InChI=1S/C24H27N3O2/c1-7-24(5,6)21-18(13-20-23(29)25-15(4)22(28)27-20)17-11-10-16(9-8-14(2)3)12-19(17)26-21/h7-8,10-13,26H,1,4,9H2,2-3,5-6H3,(H,25,29)(H,27,28)/b20-13+";
    private static String inchi_key_input = "DTTXMEFLUMXFTB-NASQKTNHSA-N";
    private static String smiles_input = "C(CC/C=C\\C/C=C\\CC1OC1C/C=C\\C/C=C\\C/C=C\\CC)(=O)O";

    public ChemSpiderRestTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getINCHIKeyFromInchi method, of class ChemSpiderRest.
     */
    @Test
    public void testGetINCHIKeyFromInchi() throws Exception {
        System.out.println("getINCHIKeyFromInchi");
        String inchi = "";
        String expResult = "WXWNIBJUIDHOOC-DEDYPNTBSA-N";
        String result = ChemSpiderRest.getINCHIKeyFromInchi(inchi_input);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSMILESFromInchi method, of class ChemSpiderRest.
     */
    @Test
    public void testGetSMILESFromInchi() throws Exception {
        System.out.println("getSMILESFromInchi");
        String expResult = "C=CC(C)(C)c1c(/C=c/2\\c(nc(=C)c(n2)O)O)c2ccc(CC=C(C)C)cc2[nH]1";
        String result = ChemSpiderRest.getSMILESFromInchi(inchi_input);
        assertEquals(expResult, result);
    }

    /**
     * Test of getMolFromInchi method, of class ChemSpiderRest.
     */
    @Test
    public void testGetMolFromInchi() throws Exception {
        System.out.println("getMolFromInchi");
        String inchi = "";
        String expResult = "";
        String result = ChemSpiderRest.getMolFromInchi(inchi_input);
        assertTrue(true);
    }

    /**
     * Test of getINCHIFromInchiKey method, of class ChemSpiderRest.
     */
    @Test
    public void testGetINCHIFromInchiKey() throws Exception {
        System.out.println("getINCHIFromInchiKey");
        String expResult = "InChI=1S/C9H13ClO4/c1-4(11)7-3-6(9(13)14-7)8(10)5(2)12/h3-5,7-8,11-12H,1-2H3/t4-,5?,7-,8?/m0/s1";
        String result = ChemSpiderRest.getINCHIFromInchiKey(inchi_key_input);
        assertEquals(expResult, result);
    }

    /**
     * Test of getMolFromInchiKey method, of class ChemSpiderRest.
     */
    @Test
    public void testGetMolFromInchiKey() throws Exception {
        System.out.println("getMolFromInchiKey");
        String expResult = "";
        String result = ChemSpiderRest.getMolFromInchiKey(inchi_key_input);
        assertTrue(true);
    }

    /**
     * Test of getInChI from SMILES method, of class ChemSpiderRest.
     */
    @Test
    public void testGetInchiFromSMILES() throws Exception {
        System.out.println("testGetInchiFromSMILES");
        String expResult = "InChI=1S/C22H32O3/c1-2-3-4-5-6-7-8-11-14-17-20-21(25-20)18-15-12-9-10-13-16-19-22(23)24/h3-4,6-7,10-15,20-21H,2,5,8-9,16-19H2,1H3,(H,23,24)/b4-3-,7-6-,13-10-,14-11-,15-12-";
        String result = ChemSpiderRest.getINCHIFromSMILES(smiles_input);
        assertEquals(expResult, result);
    }

}
