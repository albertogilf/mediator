/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databases;

import model.Compound;
import model.Identifier;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author alberto.gildelafuent
 */
public class PubchemRestTest {

    private Compound expResultCompound;
    private Integer pc_id;

    public PubchemRestTest() {
        this.pc_id = 101007633;
        String IUPACName = "(1S,7S,9S,11S)-1',10,10,13-tetramethylspiro[3,13-diazatetracyclo[5.5.2.01,9.03,7]tetradecane-11,3'-pyrrolidine]-2',5',14-trione";
        String molecularFormula = "C19H27N3O3";
        String inchi_key = "OIZZXOCTAZZJQV-OZOSWLFCSA-N";
        String inchi = "InChI=1S/C19H27N3O3/c1-16(2)12-8-18-6-5-7-22(18)11-19(12,21(4)15(18)25)10-17(16)9-13(23)20(3)14(17)24/h12H,5-11H2,1-4H3/t12-,17-,18-,19+/m0/s1";
        String smiles = "CC1(C2CC34CCCN3CC2(CC15CC(=O)N(C5=O)C)N(C4=O)C)C";
        Double logP = 0.3d;
        Double mass = 345.205242d;
        String casId = null;
        Integer compound_id = 0;
        Integer compound_status = 0;
        Integer compound_type = 0;

        Identifier identifiers = new Identifier(inchi, inchi_key, smiles);
        this.expResultCompound = new Compound(compound_id, IUPACName, casId, molecularFormula, mass, compound_status, compound_type, logP, identifiers);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getIdentifiersFromInChIPC method, of class PubchemRest.
     */
    @Test
    public void testGetIdentifiersFromInChIPC_3args() throws Exception {
        System.out.println("getIdentifiersFromInChIPC");

        String inchiKey = "OIZZXOCTAZZJQV-OZOSWLFCSA-N";
        String inchi = "InChI=1S/C19H27N3O3/c1-16(2)12-8-18-6-5-7-22(18)11-19(12,21(4)15(18)25)10-17(16)9-13(23)20(3)14(17)24/h12H,5-11H2,1-4H3/t12-,17-,18-,19+/m0/s1";
        String smiles = "CC1(C2CC34CCCN3CC2(CC15CC(=O)N(C5=O)C)N(C4=O)C)C";
        int retries = 1;
        int sleep = 0;
        Identifier expResult = new Identifier(inchi, inchiKey, smiles);
        Identifier result = PubchemRest.getIdentifiersFromInChIPC(inchi, retries, sleep);
        assertEquals(expResult, result);
    }

    /**
     * Test of getIdentifiersFromInChIPC method, of class PubchemRest.
     */
    @Test
    public void testGetIdentifiersFromInChIPC_String() throws Exception {
        System.out.println("getIdentifiersFromInChIPC");
        
        String inchiKey = "OIZZXOCTAZZJQV-OZOSWLFCSA-N";
        String inchi = "InChI=1S/C19H27N3O3/c1-16(2)12-8-18-6-5-7-22(18)11-19(12,21(4)15(18)25)10-17(16)9-13(23)20(3)14(17)24/h12H,5-11H2,1-4H3/t12-,17-,18-,19+/m0/s1";
        String smiles = "CC1(C2CC34CCCN3CC2(CC15CC(=O)N(C5=O)C)N(C4=O)C)C";
        Identifier expResult = new Identifier(inchi, inchiKey, smiles);
        Identifier result = PubchemRest.getIdentifiersFromInChIPC(inchi);
        assertEquals(expResult, result);
    }

    /**
     * Test of getIdentifiersFromINCHIKEYPC method, of class PubchemRest.
     */
    @Test
    public void testGetIdentifiersFromINCHIKEYPC() throws Exception {
        System.out.println("getIdentifiersFromINCHIKEYPC");
        String inchiKey = "OIZZXOCTAZZJQV-OZOSWLFCSA-N";
        String inchi = "InChI=1S/C19H27N3O3/c1-16(2)12-8-18-6-5-7-22(18)11-19(12,21(4)15(18)25)10-17(16)9-13(23)20(3)14(17)24/h12H,5-11H2,1-4H3/t12-,17-,18-,19+/m0/s1";
        String smiles = "CC1(C2CC34CCCN3CC2(CC15CC(=O)N(C5=O)C)N(C4=O)C)C";
        Identifier expResult = new Identifier(inchi, inchiKey, smiles);
        Identifier result = PubchemRest.getIdentifiersFromINCHIKEYPC(inchiKey);
        assertEquals(expResult, result);
    }

    /**
     * Test of getPCIDFromInchiKey method, of class PubchemRest.
     */
    @Test
    public void testGetPCIDFromInchiKey() throws Exception {
        System.out.println("getPCIDFromInchiKey");
        
        String inchiKey = "OIZZXOCTAZZJQV-OZOSWLFCSA-N";
        Integer expResult = this.pc_id;
        Integer result = PubchemRest.getPCIDFromInchiKey(inchiKey);
        assertEquals(expResult, result);
    }

    /**
     * Test of getIdentifiersFromSMILESPC method, of class PubchemRest.
     */
    @Test
    public void testGetIdentifiersFromSMILESPC() throws Exception {
        System.out.println("getIdentifiersFromSMILESPC");
        String inchiKey = "OIZZXOCTAZZJQV-OZOSWLFCSA-N";
        String inchi = "InChI=1S/C19H27N3O3/c1-16(2)12-8-18-6-5-7-22(18)11-19(12,21(4)15(18)25)10-17(16)9-13(23)20(3)14(17)24/h12H,5-11H2,1-4H3/t12-,17-,18-,19+/m0/s1";
        String smiles = "CC1(C2CC34CCCN3CC2(CC15CC(=O)N(C5=O)C)N(C4=O)C)C";
        Identifier expResult = new Identifier(inchi, inchiKey, smiles);
        Identifier result = PubchemRest.getIdentifiersFromSMILESPC(smiles);
        assertEquals(expResult, result);
    }

    /**
     * Test of getCompoundFromPCID method, of class PubchemRest.
     */
    @Test
    public void testGetCompoundFromPCID() throws Exception {
        System.out.println("getCompoundFromPCID");
        Compound result = PubchemRest.getCompoundFromPCID(pc_id);
        assertEquals(this.expResultCompound, result);
    }

    /**
     * Test of main method, of class PubchemRest.
     */
    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        //PubchemRest.main(args);
    }

    /**
     * Test of getCompoundFromName method, of class PubchemRest.
     */
    @Test
    public void testGetCompoundFromName() throws Exception {
        System.out.println("getCompoundFromName");
        String name = "Asperparaline C";
        Compound result = PubchemRest.getCompoundFromName(name);
        assertEquals(this.expResultCompound, result);
    }

}
