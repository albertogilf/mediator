/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package downloadersOfWebResources;

import exceptions.CompoundNotClassifiedException;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import utilities.Constants;

/**
 *
 * @author alberto.gildelafuent
 */
public class DownloaderOfWebResourcesTest {

    public DownloaderOfWebResourcesTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of generateKeggCode method, of class DownloaderOfWebResources.
     */
    @Test
    public void testGenerateKeggCode() {
        System.out.println("generateKeggCode");
        int number = 190;
        String expResult = "C00190";
        String result = DownloaderOfWebResources.generateKeggCode(number);
        assertEquals(expResult, result);
    }

    /**
     * Test of getInChIKeyClassyFireFromInChI method, of class
     * DownloaderOfWebResources.
     */
    @Test
    public void testDownloadCLASSYFIREAncestorsFileFromInChI() {
        System.out.println("downloadCLASSYFIREAncestorsFileFromInChI");
        String inchi = "InChI=1S/C15H20N2O4S2/c1-16-12(20)14(22-2)7-9-5-4-6-10(19)11(9)17(14)13(21)15(16,8-18)23-3/h4-6,10-11,18-19H,7-8H2,1-3H3/t10-,11-,14+,15+/m0/s1";
        String inchiKey = "OVBAGMZLGLXSBN-DEZDBRPKNA-N";
        String expResult = "OVBAGMZLGLXSBN-UOVKNHIHSA-N";
        try {
            String inchiKeyToClassify = DownloaderOfWebResources.getInChIKeyClassyFireFromInChI(inchi, inchiKey);
            assertEquals(expResult, inchiKeyToClassify);
            File classyifire_file = new File(Constants.CLASSYFIRE_RESOURCES_PATH + inchiKeyToClassify + ".ancestors");
            System.out.println(classyifire_file.length());
            if (classyifire_file.exists()) {
                assertTrue(true);
            } else {
                fail("File was not downloaded");
            }
        } catch (CompoundNotClassifiedException ex) {
            Logger.getLogger(DownloaderOfWebResourcesTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DownloaderOfWebResourcesTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Test of getInChIKeyClassyFireFromInChI method, of class
     * DownloaderOfWebResources.
     */
    @Test
    public void testDownloadCLASSYFIREAncestorsFileFromInChIWrongInchi() {
        System.out.println("downloadCLASSYFIREAncestorsFileFromInChIWRONG");
        String inchi = "InCh/c1-16-12(20)14(22-2)7-9-5-4-6-10(19)11(9)17(14)13(21)15(16,8-18)23-3/h4-6,10-11,18-19H,7-8H2,1-3H3/t10-,11-,14+,15+/m0/s1";
        String inchiKey = "OVBAGMZLGLXSBN-DEZDBRPKNA-A";
        try {
            String inchiKeyToClassify = DownloaderOfWebResources.getInChIKeyClassyFireFromInChI(inchi, inchiKey);
        } catch (CompoundNotClassifiedException ex) {
            assertTrue(true);
        } catch (IOException ex) {
            Logger.getLogger(DownloaderOfWebResourcesTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test of getInChIKeyClassyFireFromInChI method, of class
     * DownloaderOfWebResources.
     */
    @Test
    public void testGetInChIKeyFromQueryId() {
        System.out.println("testGetInChIKeyFromQueryId");
        Integer queryId = 5336307;
        String result = null;
        String expResult = "KYDNOVLBVBYOSW-UHFFFAOYSA-N";
        try {
            result = DownloaderOfWebResources.getInChIKeyFromQueryId(queryId);
        } catch (CompoundNotClassifiedException ex) {
            Logger.getLogger(DownloaderOfWebResourcesTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertEquals(expResult, result);
    }

    /**
     * Test of getInChIKeyClassyFireFromInChI method, of class
     * DownloaderOfWebResources.
     */
    @Test
    public void testGetInChIKeyFromQueryIdNoEntities() {
        System.out.println("downloadCLASSYFIREAncestorsFileFromInChINoEntities");
        Integer queryId = 5341458;
        String result = null;
        try {
            result = DownloaderOfWebResources.getInChIKeyFromQueryId(queryId);
        } catch (CompoundNotClassifiedException ex) {
            assertTrue(true);
        }
    }

}
