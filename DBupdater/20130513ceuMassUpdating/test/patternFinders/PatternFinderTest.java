/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patternFinders;

import java.util.List;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author alberto.gildelafuent
 */
public class PatternFinderTest {
    
    public PatternFinderTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of searchWithReplacement method, of class PatternFinder.
     */
    @Test
    public void testSearchWithReplacement() {
        System.out.println("searchWithReplacement");
        String content = "";
        String pattern = "";
        String stringToRemove = "";
        String expResult = "";
        String result = PatternFinder.searchWithReplacement(content, pattern, stringToRemove);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of searchWithoutReplacement method, of class PatternFinder.
     */
    @Test
    public void testSearchWithoutReplacement() {
        System.out.println("searchWithoutReplacement");
        String content = "";
        String pattern = "";
        String expResult = "";
        String result = PatternFinder.searchWithoutReplacement(content, pattern);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of searchFirstOcurrence method, of class PatternFinder.
     */
    @Test
    public void testSearchFirstOcurrence() {
        System.out.println("searchFirstOcurrence");
        String content = "";
        String pattern = "";
        String expResult = "";
        String result = PatternFinder.searchFirstOcurrence(content, pattern);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of searchSecondOcurrence method, of class PatternFinder.
     */
    @Test
    public void testSearchSecondOcurrence() {
        System.out.println("searchSecondOcurrence");
        String content = "";
        String pattern = "";
        String expResult = "";
        String result = PatternFinder.searchSecondOcurrence(content, pattern);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of searchListWithReplacement method, of class PatternFinder.
     */
    @Test
    public void testSearchListWithReplacement() {
        System.out.println("searchListWithReplacement");
        String content = "";
        String pattern = "";
        String stringToRemove = "";
        List<String> expResult = null;
        List<String> result = PatternFinder.searchListWithReplacement(content, pattern, stringToRemove);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of searchListWithoutReplacement method, of class PatternFinder.
     */
    @Test
    public void testSearchListWithoutReplacement() {
        System.out.println("searchListWithoutReplacement");
        String content = "";
        String pattern = "";
        List<String> expResult = null;
        List<String> result = PatternFinder.searchListWithoutReplacement(content, pattern);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of searchSetWithoutReplacement method, of class PatternFinder.
     */
    @Test
    public void testSearchSetWithoutReplacement() {
        System.out.println("searchSetWithoutReplacement");
        String content = "";
        String pattern = "";
        Set<String> expResult = null;
        Set<String> result = PatternFinder.searchSetWithoutReplacement(content, pattern);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of searchInDifferentCalls method, of class PatternFinder.
     */
    @Test
    public void testSearchInDifferentCalls() {
        System.out.println("searchInDifferentCalls");
        StringBuilder content = null;
        String pattern = "";
        String stringToRemove = "";
        StringBuilder result_2 = null;
        PatternFinder instance = new PatternFinder();
        boolean expResult = false;
        boolean result = instance.searchInDifferentCalls(content, pattern, stringToRemove, result_2);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of searchDouble method, of class PatternFinder.
     */
    @Test
    public void testSearchDouble() {
        System.out.println("searchDouble");
        String content = "";
        double expResult = 0.0;
        double result = PatternFinder.searchDouble(content);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of searchPCID method, of class PatternFinder.
     */
    @Test
    public void testSearchPCID() {
        System.out.println("searchPCID");
        String pubchemLink = "http://pubchem.ncbi.nlm.nih.gov/compound/123";
        Integer expResult = 123;
        Integer result = PatternFinder.searchPCID(pubchemLink);
        assertEquals(expResult, result);
        pubchemLink = "https://pubchem.ncbi.nlm.nih.gov/compound/123";
        result = PatternFinder.searchPCID(pubchemLink);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getLipidTypeFromName method, of class PatternFinder.
     */
    @Test
    public void testGetLipidTypeFromName() {
        System.out.println("getLipidTypeFromName");
        String common_name = "";
        String expResult = "";
        String result = PatternFinder.getLipidTypeFromName(common_name);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNumberOfCarbonsFromName method, of class PatternFinder.
     */
    @Test
    public void testGetNumberOfCarbonsFromName() {
        System.out.println("getNumberOfCarbonsFromName");
        String common_name = "";
        int expResult = 0;
        int result = PatternFinder.getNumberOfCarbonsFromName(common_name);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNumberOfDoubleBondsFromName method, of class PatternFinder.
     */
    @Test
    public void testGetNumberOfDoubleBondsFromName() {
        System.out.println("getNumberOfDoubleBondsFromName");
        String common_name = "";
        int expResult = 0;
        int result = PatternFinder.getNumberOfDoubleBondsFromName(common_name);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getPrimaryHMDBIDFromHTML method, of class PatternFinder.
     */
    @Test
    public void testGetPrimaryHMDBIDFromHTML() {
        System.out.println("getPrimaryHMDBIDFromHTML");
        String content = "";
        String expResult = "";
        String result = PatternFinder.getPrimaryHMDBIDFromHTML(content);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getLipidType method, of class PatternFinder.
     */
    @Test
    public void testGetLipidType() {
        System.out.println("getLipidType");
        String content = "";
        String expResult = "";
        String result = PatternFinder.getLipidType(content);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNumberOfCarbons method, of class PatternFinder.
     */
    @Test
    public void testGetNumberOfCarbons() {
        System.out.println("getNumberOfCarbons");
        String content = "";
        int expResult = 0;
        int result = PatternFinder.getNumberOfCarbons(content);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNumberOfDoubleBonds method, of class PatternFinder.
     */
    @Test
    public void testGetNumberOfDoubleBonds() {
        System.out.println("getNumberOfDoubleBonds");
        String content = "";
        int expResult = 0;
        int result = PatternFinder.getNumberOfDoubleBonds(content);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getOxidationFromAbbrev method, of class PatternFinder.
     */
    @Test
    public void testGetOxidationFromAbbrev() {
        System.out.println("getOxidationFromAbbrev");
        String content = "";
        String expResult = "";
        String result = PatternFinder.getOxidationFromAbbrev(content);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getListOfChains method, of class PatternFinder.
     */
    @Test
    public void testGetListOfChains() {
        System.out.println("getListOfChains");
        String content = "";
        List<String> expResult = null;
        List<String> result = PatternFinder.getListOfChains(content);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getCodeLMJSON method, of class PatternFinder.
     */
    @Test
    public void testGetCodeLMJSON() {
        System.out.println("getCodeLMJSON");
        String sentence = "";
        String expResult = "";
        String result = PatternFinder.getCodeLMJSON(sentence);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of checkFormulaElements method, of class PatternFinder.
     */
    @Test
    public void testCheckFormulaElements() {
        System.out.println("checkFormulaElements");
        String formula = "";
        boolean expResult = false;
        boolean result = PatternFinder.checkFormulaElements(formula);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getTypeFromFormula method, of class PatternFinder.
     */
    @Test
    public void testGetTypeFromFormula() {
        System.out.println("getTypeFromFormula");
        String formula = "";
        String expResult = "";
        String result = PatternFinder.getTypeFromFormula(formula);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of debugFormula method, of class PatternFinder.
     */
    @Test
    public void testDebugFormula() {
        System.out.println("debugFormula");
        String content = "";
        boolean expResult = false;
        boolean result = PatternFinder.debugFormula(content);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNodesAncestorsCLASSYFIREFromInChIkey method, of class PatternFinder.
     */
    @Test
    public void testGetNodesAncestorsCLASSYFIREFromInChIkey() throws Exception {
        System.out.println("getNodesAncestorsCLASSYFIREFromInChIkey");
        String inchiKey = "";
        String inchiFileName = "";
        List<String> expResult = null;
        List<String> result = PatternFinder.getNodesAncestorsCLASSYFIREFromInChIkey(inchiKey, inchiFileName);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNodesAncestorsCLASSYFIRE method, of class PatternFinder.
     */
    @Test
    public void testGetNodesAncestorsCLASSYFIRE() {
        System.out.println("getNodesAncestorsCLASSYFIRE");
        String htmlString = "";
        List<String> expResult = null;
        List<String> result = PatternFinder.getNodesAncestorsCLASSYFIRE(htmlString);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getChargeFromSmiles method, of class PatternFinder.
     */
    @Test
    public void testGetChargeFromSmiles() {
        System.out.println("getChargeFromSmiles");
        String Smiles = "";
        int[] expResult = null;
        int[] result = PatternFinder.getChargeFromSmiles(Smiles);
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getGenusSpecieFromGenusSpecieAndSubspecies method, of class PatternFinder.
     */
    @Test
    public void testGetSpeciesFromSpeciesAndCells() throws Exception {
        System.out.println("getSpeciesFromSpeciesAndCells");
        String species = "Aspergillus ochraceus";
        String expResult = "Aspergillus ochraceus";
        String result = PatternFinder.getGenusSpecieFromGenusSpecieAndSubspecies(species);
        assertEquals(expResult, result);
        species = "Aspergillus terreus NRRL 11156";
        expResult = "Aspergillus terreus";
        result = PatternFinder.getGenusSpecieFromGenusSpecieAndSubspecies(species);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSubspecieFromGenusSpecieAndSubspecie method, of class PatternFinder.
     */
    @Test
    public void testGetCellsFromSpeciesAndCells() throws Exception {
        System.out.println("getCellsFromSpeciesAndCells");
        String species = "Aspergillus ochraceus";
        String expResult = null;
        String result = PatternFinder.getSubspecieFromGenusSpecieAndSubspecie(species);
        assertEquals(expResult, result);
        species = "Aspergillus terreus NRRL 11156";
        expResult = "NRRL 11156";
        result = PatternFinder.getSubspecieFromGenusSpecieAndSubspecie(species);
        assertEquals(expResult, result);
    }
    
}
