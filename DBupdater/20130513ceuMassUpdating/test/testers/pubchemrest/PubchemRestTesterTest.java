/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testers.pubchemrest;

import databases.PubchemRest;
import static databases.PubchemRest.getPCIDFromInchiKey;
import model.Identifier;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ceu
 */
public class PubchemRestTesterTest {
    
    public PubchemRestTesterTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    /**
     * Test of getIdentifiersFromInChIPC method, of class PubchemRest.
     */
    @Test
    public void testGetIdentifiersFromInChIPC() throws Exception {
        System.out.println("getIdentifiersFromInChIPC");
        String inchi = "InChI=1S/C46H78O4/c1-3-4-5-6-7-8-9-10-11-12-14-19-22-25-28-31-34-37-40-43-46(49)50-44(2)41-38-35-32-29-26-23-20-17-15-13-16-18-21-24-27-30-33-36-39-42-45(47)48/h4-5,7-8,10-11,14,19,25,28,34,37,44H,3,6,9,12-13,15-18,20-24,26-27,29-33,35-36,38-43H2,1-2H3,(H,47,48)/b5-4-,8-7-,11-10-,19-14-,28-25-,37-34-";
        Integer expResult = 134777005;
        Identifier identifier = PubchemRest.getIdentifiersFromInChIPC(inchi);
        Integer pc_id = PubchemRest.getPCIDFromInchiKey(identifier.getInchi_key());
        Integer result = pc_id;
        assertEquals(expResult, result);
    }

    /**
     * Test of getIdentifiersFromSmilesPC method, of class PubchemRest.
     */
    @Test
    public void getIdentifiersFromINCHIKEYPC() throws Exception {
        System.out.println("getIdentifiersFromINCHIKEYPC");
        String inchi_key = "WSRNVQBHNOZMJH-YWCHUICKSA-N";
        Integer expResult = 134777005;
        Identifier identifier = PubchemRest.getIdentifiersFromINCHIKEYPC(inchi_key);
        Integer pc_id = PubchemRest.getPCIDFromInchiKey(inchi_key);
        Integer result = pc_id;
        assertEquals(expResult, result);
    }

    /**
     * Test of getIdentifiersFromSMILESPC method, of class PubchemRest.
     */
    @Test
    public void testGetIdentifiersFromSMILESPC() throws Exception {
        System.out.println("getIdentifiersFromSMILESPC");
        String smiles = "CCC=CCC=CCC=CCC=CCC=CCC=CCCC(=O)OC(C)CCCCCCCCCCCCCCCCCCCCCC(=O)O";
        Integer expResult = 133051587;
        Identifier identifier = PubchemRest.getIdentifiersFromSMILESPC(smiles);
        Integer pc_id = PubchemRest.getPCIDFromInchiKey(identifier.getInchi_key());
        Integer result = pc_id;
        assertEquals(expResult, result);
    }

    
}
