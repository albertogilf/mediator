drop table ce_eff_mob_with_formic_pos;
drop table ce_eff_mob_with_formic_neg;
drop table ce_eff_mob_with_formic01_neg;
drop table ce_eff_mob_with_acetic_pos;
drop table ce_eff_mob_with_acetic_neg;
-- CREATE THE VIEWS TO LOOK INTO DIFFERENT IONIZATION MODES AND POLARITIES
CREATE TABLE ce_eff_mob_with_formic_pos (
  ce_eff_mob_id INT(11) NOT NULL PRIMARY KEY,
  ce_compound_id INT NOT NULL,
  eff_mob_exp_prop_id int(11) NOT NULL,
  cembio_id INT NULL, -- INTERNAL ID FROM CEMBIO TO RELATE WITH THE PRODUCT IONS
  eff_mobility double default null, -- eff mobility By default null
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  CONSTRAINT ce_compound_id_ce_eff_mob_formic_pos_constraint FOREIGN KEY (ce_compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  CONSTRAINT eff_mob_exp_prop_id_ce_eff_mob_formic_pos_constraint FOREIGN KEY (eff_mob_exp_prop_id) REFERENCES eff_mob_experimental_properties(eff_mob_exp_prop_id) on DELETE CASCADE,
  UNIQUE KEY `eff_mob_key` (`ce_compound_id`,`eff_mob_exp_prop_id`), 
  INDEX eff_mob_formic_pos_index (eff_mobility),
  INDEX eff_mob_formic_pos_eff_mob_exp_prop_id_index (eff_mob_exp_prop_id),
  INDEX eff_mob_formic_pos_eff_mob_exp_prop_id_eff_mob_index (eff_mob_exp_prop_id, eff_mobility),
  INDEX eff_mob_formic_pos_compound_id_index (ce_compound_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE ce_eff_mob_with_formic_neg (
  ce_eff_mob_id INT(11) NOT NULL PRIMARY KEY,
  ce_compound_id INT NOT NULL,
  eff_mob_exp_prop_id int(11) NOT NULL,
  cembio_id INT NULL, -- INTERNAL ID FROM CEMBIO TO RELATE WITH THE PRODUCT IONS
  eff_mobility double default null, -- eff mobility By default null
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  CONSTRAINT ce_compound_id_ce_eff_mob_formic_neg_constraint FOREIGN KEY (ce_compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  CONSTRAINT eff_mob_exp_prop_id_ce_eff_mob_formic_neg_constraint FOREIGN KEY (eff_mob_exp_prop_id) REFERENCES eff_mob_experimental_properties(eff_mob_exp_prop_id) on DELETE CASCADE,
  UNIQUE KEY `eff_mob_key` (`ce_compound_id`,`eff_mob_exp_prop_id`), 
  INDEX eff_mob_formic_neg_index (eff_mobility),
  INDEX eff_mob_formic_neg_eff_mob_exp_prop_id_index (eff_mob_exp_prop_id),
  INDEX eff_mob_formic_neg_eff_mob_exp_prop_id_eff_mob_index (eff_mob_exp_prop_id, eff_mobility),
  INDEX eff_mob_formic_neg_compound_id_index (ce_compound_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE ce_eff_mob_with_formic01_neg (
  ce_eff_mob_id INT(11) NOT NULL PRIMARY KEY,
  ce_compound_id INT NOT NULL,
  eff_mob_exp_prop_id int(11) NOT NULL,
  cembio_id INT NULL, -- INTERNAL ID FROM CEMBIO TO RELATE WITH THE PRODUCT IONS
  eff_mobility double default null, -- eff mobility By default null
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  CONSTRAINT ce_compound_id_ce_eff_mob_formic01_neg_constraint FOREIGN KEY (ce_compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  CONSTRAINT eff_mob_exp_prop_id_ce_eff_mob_formic01_neg_constraint FOREIGN KEY (eff_mob_exp_prop_id) REFERENCES eff_mob_experimental_properties(eff_mob_exp_prop_id) on DELETE CASCADE,
  UNIQUE KEY `eff_mob_key` (`ce_compound_id`,`eff_mob_exp_prop_id`), 
  INDEX eff_mob_formic01_neg_index (eff_mobility),
  INDEX eff_mob_formic01_neg_eff_mob_exp_prop_id_index (eff_mob_exp_prop_id),
  INDEX eff_mob_formic01_neg_eff_mob_exp_prop_id_eff_mob_index (eff_mob_exp_prop_id, eff_mobility),
  INDEX eff_mob_formic01_neg_compound_id_index (ce_compound_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE ce_eff_mob_with_acetic_pos (
  ce_eff_mob_id INT(11) NOT NULL PRIMARY KEY,
  ce_compound_id INT NOT NULL,
  eff_mob_exp_prop_id int(11) NOT NULL,
  cembio_id INT NULL, -- INTERNAL ID FROM CEMBIO TO RELATE WITH THE PRODUCT IONS
  eff_mobility double default null, -- eff mobility By default null
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  CONSTRAINT ce_compound_id_ce_eff_mob_acetic_pos_constraint FOREIGN KEY (ce_compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  CONSTRAINT eff_mob_exp_prop_id_ce_eff_mob_acetic_pos_constraint FOREIGN KEY (eff_mob_exp_prop_id) REFERENCES eff_mob_experimental_properties(eff_mob_exp_prop_id) on DELETE CASCADE,
  UNIQUE KEY `eff_mob_key` (`ce_compound_id`,`eff_mob_exp_prop_id`), 
  INDEX eff_mob_acetic_pos_index (eff_mobility),
  INDEX eff_mob_acetic_pos_eff_mob_exp_prop_id_index (eff_mob_exp_prop_id),
  INDEX eff_mob_acetic_pos_eff_mob_exp_prop_id_eff_mob_index (eff_mob_exp_prop_id, eff_mobility),
  INDEX eff_mob_acetic_pos_compound_id_index (ce_compound_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE ce_eff_mob_with_acetic_neg (
  ce_eff_mob_id INT(11) NOT NULL PRIMARY KEY,
  ce_compound_id INT NOT NULL,
  eff_mob_exp_prop_id int(11) NOT NULL,
  cembio_id INT NULL, -- INTERNAL ID FROM CEMBIO TO RELATE WITH THE PRODUCT IONS
  eff_mobility double default null, -- eff mobility By default null
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  CONSTRAINT ce_compound_id_ce_eff_mob_acetic_neg_constraint FOREIGN KEY (ce_compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  CONSTRAINT eff_mob_exp_prop_id_ce_eff_mob_acetic_neg_constraint FOREIGN KEY (eff_mob_exp_prop_id) REFERENCES eff_mob_experimental_properties(eff_mob_exp_prop_id) on DELETE CASCADE,
  UNIQUE KEY `eff_mob_key` (`ce_compound_id`,`eff_mob_exp_prop_id`), 
  INDEX eff_mob_acetic_neg_index (eff_mobility),
  INDEX eff_mob_acetic_neg_eff_mob_exp_prop_id_index (eff_mob_exp_prop_id),
  INDEX eff_mob_acetic_neg_eff_mob_exp_prop_id_eff_mob_index (eff_mob_exp_prop_id, eff_mobility),
  INDEX eff_mob_acetic_neg_compound_id_index (ce_compound_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into ce_eff_mob_with_formic_pos select ce_eff_mob_id, ce_compound_id, eff_mob_exp_prop_id, cembio_id, eff_mobility, 
CURRENT_TIMESTAMP, CURRENT_TIMESTAMP FROM
  (select ce_eff_mob.ce_eff_mob_id, ce_eff_mob.ce_compound_id, 
  ce_eff_mob.eff_mob_exp_prop_id, ce_eff_mob.cembio_id, ce_eff_mob.eff_mobility
from ce_eff_mob ce_eff_mob where ce_eff_mob.eff_mobility is not null and ce_eff_mob.eff_mob_exp_prop_id = 1) as pos ;

insert into ce_eff_mob_with_formic_neg select ce_eff_mob_id, ce_compound_id, eff_mob_exp_prop_id, cembio_id, eff_mobility, 
CURRENT_TIMESTAMP, CURRENT_TIMESTAMP FROM
  (select ce_eff_mob.ce_eff_mob_id, ce_eff_mob.ce_compound_id, 
  ce_eff_mob.eff_mob_exp_prop_id, ce_eff_mob.cembio_id, ce_eff_mob.eff_mobility
from ce_eff_mob ce_eff_mob where ce_eff_mob.eff_mobility is not null and ce_eff_mob.eff_mob_exp_prop_id = 4) as neg ;


insert into ce_eff_mob_with_formic01_neg select ce_eff_mob_id, ce_compound_id, eff_mob_exp_prop_id, cembio_id, eff_mobility, 
CURRENT_TIMESTAMP, CURRENT_TIMESTAMP FROM
  (select ce_eff_mob.ce_eff_mob_id, ce_eff_mob.ce_compound_id, 
  ce_eff_mob.eff_mob_exp_prop_id, ce_eff_mob.cembio_id, ce_eff_mob.eff_mobility
from ce_eff_mob ce_eff_mob where ce_eff_mob.eff_mobility is not null and ce_eff_mob.eff_mob_exp_prop_id = 17) as neg;

insert into ce_eff_mob_with_acetic_pos select ce_eff_mob_id, ce_compound_id, eff_mob_exp_prop_id, cembio_id, eff_mobility, 
CURRENT_TIMESTAMP, CURRENT_TIMESTAMP FROM
  (select ce_eff_mob.ce_eff_mob_id, ce_eff_mob.ce_compound_id, 
  ce_eff_mob.eff_mob_exp_prop_id, ce_eff_mob.cembio_id, ce_eff_mob.eff_mobility
from ce_eff_mob ce_eff_mob where ce_eff_mob.eff_mobility is not null and ce_eff_mob.eff_mob_exp_prop_id = 13) as pos  ;

insert into ce_eff_mob_with_acetic_neg select ce_eff_mob_id, ce_compound_id, eff_mob_exp_prop_id, cembio_id, eff_mobility, 
CURRENT_TIMESTAMP, CURRENT_TIMESTAMP FROM
  (select ce_eff_mob.ce_eff_mob_id, ce_eff_mob.ce_compound_id, 
  ce_eff_mob.eff_mob_exp_prop_id, ce_eff_mob.cembio_id, ce_eff_mob.eff_mobility
from ce_eff_mob ce_eff_mob where ce_eff_mob.eff_mobility is not null and ce_eff_mob.eff_mob_exp_prop_id = 16) as neg ;





/*

insert into ce_eff_mob_with_formic_pos select ce_eff_mob_id, ce_compound_id, eff_mob_exp_prop_id, cembio_id, eff_mobility, 
CURRENT_TIMESTAMP, CURRENT_TIMESTAMP FROM
  ((select ce_eff_mob.ce_eff_mob_id, ce_eff_mob.ce_compound_id, 
  ce_eff_mob.eff_mob_exp_prop_id, ce_eff_mob.cembio_id, ce_eff_mob.eff_mobility
from ce_eff_mob ce_eff_mob where ce_eff_mob.eff_mobility is not null and ce_eff_mob.eff_mob_exp_prop_id = 1) 
UNION 
  (select ce_eff_mob.ce_eff_mob_id, ce_eff_mob.ce_compound_id, 
  ce_eff_mob.eff_mob_exp_prop_id, ce_eff_mob.cembio_id, ce_eff_mob.eff_mobility
from ce_eff_mob ce_eff_mob where ce_eff_mob.eff_mobility is not null and ce_eff_mob.eff_mob_exp_prop_id in (2,3,4) 
and ce_eff_mob.ce_compound_id not in 
(SELECT ce_compound_id FROM ce_eff_mob ce_eff_mob 
  WHERE eff_mob_exp_prop_id = 1 and ce_eff_mob.eff_mobility is not null))) as pos ;

insert into ce_eff_mob_with_formic_neg select ce_eff_mob_id, ce_compound_id, eff_mob_exp_prop_id, cembio_id, eff_mobility, 
CURRENT_TIMESTAMP, CURRENT_TIMESTAMP FROM
  ((select ce_eff_mob.ce_eff_mob_id, ce_eff_mob.ce_compound_id, 
  ce_eff_mob.eff_mob_exp_prop_id, ce_eff_mob.cembio_id, ce_eff_mob.eff_mobility
from ce_eff_mob ce_eff_mob where ce_eff_mob.eff_mobility is not null and ce_eff_mob.eff_mob_exp_prop_id = 2) 
UNION 
  (select ce_eff_mob.ce_eff_mob_id, ce_eff_mob.ce_compound_id, 
  ce_eff_mob.eff_mob_exp_prop_id, ce_eff_mob.cembio_id, ce_eff_mob.eff_mobility
from ce_eff_mob ce_eff_mob where ce_eff_mob.eff_mobility is not null and ce_eff_mob.eff_mob_exp_prop_id in (1,3,4) 
and ce_eff_mob.ce_compound_id not in 
(SELECT ce_compound_id FROM ce_eff_mob ce_eff_mob 
  WHERE eff_mob_exp_prop_id = 2 and ce_eff_mob.eff_mobility is not null))) as pos ;


insert into ce_eff_mob_with_formic01_neg select ce_eff_mob_id, ce_compound_id, eff_mob_exp_prop_id, cembio_id, eff_mobility, 
CURRENT_TIMESTAMP, CURRENT_TIMESTAMP FROM
  ((select ce_eff_mob.ce_eff_mob_id, ce_eff_mob.ce_compound_id, 
  ce_eff_mob.eff_mob_exp_prop_id, ce_eff_mob.cembio_id, ce_eff_mob.eff_mobility
from ce_eff_mob ce_eff_mob where ce_eff_mob.eff_mobility is not null and ce_eff_mob.eff_mob_exp_prop_id = 17);

insert into ce_eff_mob_with_acetic_pos select ce_eff_mob_id, ce_compound_id, eff_mob_exp_prop_id, cembio_id, eff_mobility, 
CURRENT_TIMESTAMP, CURRENT_TIMESTAMP FROM
  ((select ce_eff_mob.ce_eff_mob_id, ce_eff_mob.ce_compound_id, 
  ce_eff_mob.eff_mob_exp_prop_id, ce_eff_mob.cembio_id, ce_eff_mob.eff_mobility
from ce_eff_mob ce_eff_mob where ce_eff_mob.eff_mobility is not null and ce_eff_mob.eff_mob_exp_prop_id = 13) 
UNION 
  (select ce_eff_mob.ce_eff_mob_id, ce_eff_mob.ce_compound_id, 
  ce_eff_mob.eff_mob_exp_prop_id, ce_eff_mob.cembio_id, ce_eff_mob.eff_mobility
from ce_eff_mob ce_eff_mob where ce_eff_mob.eff_mobility is not null and ce_eff_mob.eff_mob_exp_prop_id in (14,15,16) 
and ce_eff_mob.ce_compound_id not in 
(SELECT ce_compound_id FROM ce_eff_mob ce_eff_mob 
  WHERE eff_mob_exp_prop_id = 13 and ce_eff_mob.eff_mobility is not null))) as pos ;

insert into ce_eff_mob_with_acetic_neg select ce_eff_mob_id, ce_compound_id, eff_mob_exp_prop_id, cembio_id, eff_mobility, 
CURRENT_TIMESTAMP, CURRENT_TIMESTAMP FROM
  ((select ce_eff_mob.ce_eff_mob_id, ce_eff_mob.ce_compound_id, 
  ce_eff_mob.eff_mob_exp_prop_id, ce_eff_mob.cembio_id, ce_eff_mob.eff_mobility
from ce_eff_mob ce_eff_mob where ce_eff_mob.eff_mobility is not null and ce_eff_mob.eff_mob_exp_prop_id = 16) 
UNION 
  (select ce_eff_mob.ce_eff_mob_id, ce_eff_mob.ce_compound_id, 
  ce_eff_mob.eff_mob_exp_prop_id, ce_eff_mob.cembio_id, ce_eff_mob.eff_mobility
from ce_eff_mob ce_eff_mob where ce_eff_mob.eff_mobility is not null and ce_eff_mob.eff_mob_exp_prop_id in (13,14,15) 
and ce_eff_mob.ce_compound_id not in 
(SELECT ce_compound_id FROM ce_eff_mob ce_eff_mob 
  WHERE eff_mob_exp_prop_id = 16 and ce_eff_mob.eff_mobility is not null))) as pos ;
*/