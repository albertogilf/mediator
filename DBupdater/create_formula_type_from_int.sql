
use compounds_new;

-- 0 CHNOPS, 1 CHNOPSD, 2 CHNOPSCL, 3 CHNOPSCLD, 4 ALL, 5 ALLD
alter table compounds add column formula_type varchar(20) DEFAULT NULL;
update compounds set formula_type='CHNOPS' where formula_type_int =0 ;
update compounds set formula_type='CHNOPSD' where formula_type_int =1 ;
update compounds set formula_type='CHNOPSCL' where formula_type_int =2 ;
update compounds set formula_type='CHNOPSCLD' where formula_type_int =3 ;
update compounds set formula_type='ALL' where formula_type_int =4 ;
update compounds set formula_type='ALLD' where formula_type_int =5 ;

alter table compounds add index compounds_formula_type_index (formula_type);
