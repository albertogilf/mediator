-- create table for the compound search. 

drop table compounds_view;

CREATE TABLE compounds_view (
  compound_id INT NOT NULL PRIMARY KEY,
  cas_id varchar(100) UNIQUE DEFAULT NULL,
  compound_name text not null,
  formula varchar(100) DEFAULT '',
  mass double DEFAULT 0,
  -- charge 0 for neutral, 1 for positive 2 for negative
  charge_type int default 0,
  -- number of charges (negative or positive)
  charge_number int default 0,
  -- CHNOPS, CHNOPSD, CHNOPSCL, CHNOPSCLD, ALLD or ALL
  -- CHANGE FOR INT? -- 0 CHNOPS, 1 CHNOPSD, 2 CHNOPSCL, 3 CHNOPSCLD, 4 ALL, 5 ALLD
  formula_type_int int default 4,
  -- type of compound: 0 for metabolite, 1 for lipids, 2 for peptide
  compound_type int default 0,
  -- status of compound: 0 expected, 1 detected, 2 quantified, 3 predicted (HMDB)
  compound_status int default 0,
  logP double default null, -- LogP of the compound. By default 0
  RT_pred double default null, -- LogP of the compound. By default null
  kegg_id varchar(100) default NULL,
  lm_id varchar(100) default NULL,
  hmdb_id varchar(100) default NULL,
  agilent_id varchar(100) default NULL,
  pc_id INT default NULL,
  chebi_id INT default NULL,
  in_house_id varchar(100) default NULL, 
  aspergillus_id INT default NULL, 
  knapsack_id varchar(100)  default NULL, 
  npatlas_id INT default NULL, 
  fahfa_id INT default NULL, 
  oh_position INT default NULL,
  biological_activity text default NULL,
  mesh_nomenclature varchar(100) default NULL,
  iupac_classification varchar(100) default NULL,
  aspergillus_web_name varchar(100) default NULL,
  inchi varchar(1800) default NULL,
  inchi_key varchar(50) default NULL,
  smiles varchar(1200) default NULL,
  lipid_type varchar(150) default NULL, 
  num_chains int default NULL,
  number_carbons int default NULL,
  double_bonds int default NULL,
  category varchar(10) DEFAULT NULL,
  main_class varchar(10) DEFAULT NULL,
  sub_class varchar(10) DEFAULT NULL,
  class_level4 varchar(10) DEFAULT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  INDEX compounds_view_formula_index (formula),
  INDEX compounds_view_mass_index (mass),
  INDEX compounds_view_charge_type_index (charge_type),
  INDEX compounds_view_gen_formula_type_int_index (formula_type_int),
  INDEX compounds_view_compound_type_index (compound_type),
  INDEX compounds_view_compound_status_index (compound_status),
  INDEX compounds_RT_pred_index (RT_pred), 
  INDEX compounds_view_kegg_id_index (kegg_id), 
  INDEX compounds_view_lm_id_index (lm_id),
  INDEX compounds_view_hmdb_id_index (hmdb_id), 
  INDEX compounds_view_agilent_id_index (agilent_id),
  INDEX compounds_view_pc_id_index (pc_id), 
  INDEX compounds_view_in_house_id_index (in_house_id),
  INDEX compounds_view_npatlas_id_index (npatlas_id),
  INDEX compounds_view_aspergillus_id_index (aspergillus_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- create table for the compound gen search. 

drop table compounds_gen_view;

CREATE TABLE compounds_gen_view (
  compound_id INT NOT NULL PRIMARY KEY,
  compound_name text not null,
  formula varchar(100) DEFAULT '',
  mass double DEFAULT 0,
  -- charge 0 for neutral, 1 for positive 2 for negative
  charge_type int default 0,
  -- number of charges (negative or positive)
  charge_number int default 0,
  -- CHNOPS, CHNOPSD, CHNOPSCL, CHNOPSCLD, ALLD or ALL
  -- CHANGE FOR INT? -- 0 CHNOPS, 1 CHNOPSD, 2 CHNOPSCL, 3 CHNOPSCLD, 4 ALL, 5 ALLD
  formula_type_int int default 4,
  MINE_id INT UNIQUE DEFAULT NULL,
  np_likeness double DEFAULT NULL,
  logP double default null, -- LogP of the compound. By default 0
  inchi varchar(1800) default NULL,
  inchi_key varchar(50) default NULL,
  smiles varchar(1200) default NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  INDEX compounds_view_formula_index (formula),
  INDEX compounds_view_mass_index (mass),
  INDEX compounds_view_charge_type_index (charge_type),
  INDEX compounds_view_gen_formula_type_int_index (formula_type_int)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

