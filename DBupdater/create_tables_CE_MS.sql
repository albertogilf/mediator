################################################ NEW DATA MODEL ######################################################


########################################### TABLES FOR COMPOUNDS #####################################################


drop table compound_ce_product_ion;
drop table eff_mob;
drop table ce_experimental_properties_metadata;
drop table ce_experimental_properties;
drop table eff_mob_experimental_properties;


create table eff_mob_experimental_properties(
  eff_mob_exp_prop_id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  buffer int not null, -- 1: formico 1 molar (1M) al 10% de methanol, 2: acetic acid 10% -- 3 Formico 0.1M al 10% de methanol -- 4 ammonium acetate 50mM
  temperature int not null , -- temperature in celsius degrees
  polarity int not null, -- 1: direct, 2: inverse
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  UNIQUE KEY `experimental_properties` (`buffer`,`temperature`,`polarity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 1: FORMIC 1M 
-- 2: acetic acid 10% 
-- 3: Formic 1M methanol 10%
-- 4: ammonium acetate 50mM
-- 5: Formic Acid 0.26M
-- 6: Ammonium Bicarbonate 20mM
-- 7: Formic 0.5% Methanol 5%
-- 8: Ammonium Bicarbonate 50mM
-- 9: Ammonium acetate 50mM Methanol 5%
-- 10: Formic acid 1 M 15% ACN
-- 11: 35 mM ammonium acetate in 70 vol % acetonitrile, 15 vol % methanol, 10 vol % H2O
-- 12: Formic 0.1M methanol 10%
-- 13: Formic 0.8M methanol 10%

-- Formic acid 1 M 15% ACN 20 °C Positive  Positive
-- 35 mM ammonium acetate in 70 vol % acetonitrile, 15 vol % methanol, 10 vol % H2O, and 5 vol % 2-propanol  20 °C Negative  Positive

insert into eff_mob_experimental_properties(buffer,temperature, polarity) VALUES (1,20,1);
insert into eff_mob_experimental_properties(buffer,temperature, polarity) VALUES (1,20,2);
insert into eff_mob_experimental_properties(buffer,temperature, polarity) VALUES (1,25,1);
insert into eff_mob_experimental_properties(buffer,temperature, polarity) VALUES (1,25,2);
insert into eff_mob_experimental_properties(buffer,temperature, polarity) VALUES (2,20,1);
insert into eff_mob_experimental_properties(buffer,temperature, polarity) VALUES (2,20,2);
insert into eff_mob_experimental_properties(buffer,temperature, polarity) VALUES (2,25,1);
insert into eff_mob_experimental_properties(buffer,temperature, polarity) VALUES (2,25,2);
insert into eff_mob_experimental_properties(buffer,temperature, polarity) VALUES (3,20,2);
insert into eff_mob_experimental_properties(buffer,temperature, polarity) VALUES (4,20,1);
insert into eff_mob_experimental_properties(buffer,temperature, polarity) VALUES (5,20,1);
insert into eff_mob_experimental_properties(buffer,temperature, polarity) VALUES (6,20,1);
insert into eff_mob_experimental_properties(buffer,temperature, polarity) VALUES (7,20,1);
insert into eff_mob_experimental_properties(buffer,temperature, polarity) VALUES (8,20,1);
insert into eff_mob_experimental_properties(buffer,temperature, polarity) VALUES (9,20,2);
insert into eff_mob_experimental_properties(buffer,temperature, polarity) VALUES (10,20,1);
insert into eff_mob_experimental_properties(buffer,temperature, polarity) VALUES (11,20,1);
insert into eff_mob_experimental_properties(buffer,temperature, polarity) VALUES (12,20,2);
insert into eff_mob_experimental_properties(buffer,temperature, polarity) VALUES (13,20,2);
insert into eff_mob_experimental_properties(buffer,temperature, polarity) VALUES (8,20,2);


create table ce_experimental_properties(
  ce_exp_prop_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  eff_mob_exp_prop_id int NOT NULL, 
  ce_sample_type int NOT NULL default 1, -- 1: standard, 2: plasma, 3: urine, 4: feces, 5: serum, 
  capillary_length int null default 1000, -- capillary length in mm (1000)
  capillary_voltage int not null default 30, -- capillary voltage in kV 
  ionization_mode int not null, -- 1 for positive; 2 for negative,
  CONSTRAINT eff_mob_exp_prop_id_ce_exp_prop_constraint FOREIGN KEY (eff_mob_exp_prop_id) REFERENCES eff_mob_experimental_properties(eff_mob_exp_prop_id) on DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


create table ce_experimental_properties_metadata(
  ce_exp_prop_metadata_id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  ce_exp_prop_id INT NOT NULL, 
  compound_id INT NOT NULL,
  experimental_mz double default null, -- experimental mz obtained in the analysis
  ce_identification_level int not null default 1, -- 1: level 1 (standard), 2: L2 and 3: L3 (not used here). 4 would be unknown
  rmt_ref_compound_id int default null, -- ce_exp_prop_metadata_id of the 180838: methionine sulfone, 2: 73414 PARACETAMOL OR EOF in the same experiment id (ce_exp_prop_id)
  absolute_MT double default null, 
  relative_MT double default null, -- regarding reference compound (methionine sulfone or paracetamol)
  exp_eff_mob double, 
  commercial varchar(20) default null, 
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  CONSTRAINT ce_exp_prop_id_ce_experimental_properties_metadata_constraint FOREIGN KEY (ce_exp_prop_id) REFERENCES ce_experimental_properties(ce_exp_prop_id) on DELETE CASCADE,
  CONSTRAINT compound_id_ce_eff_mob_constraint FOREIGN KEY (compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  CONSTRAINT rmt_ref_compound_id_constraint FOREIGN KEY (rmt_ref_compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  UNIQUE KEY `metadata_exp_com_key` (`ce_exp_prop_id`,`compound_id`, `rmt_ref_compound_id`), 
  INDEX ce_experimental_mz_index (experimental_mz), 
  INDEX ce_identification_level_index (ce_identification_level), 
  INDEX ce_rmt_ref_exp_prop_id_index (rmt_ref_exp_prop_id), 
  INDEX ce_absolute_MT_index (absolute_MT), 
  INDEX ce_relative_MT_index (relative_MT), 
  INDEX ce_exp_eff_mob_index (exp_eff_mob)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


create table eff_mob(
  eff_mob_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  compound_id INT NOT NULL,
  eff_mob_exp_prop_id int NOT NULL,
  ce_exp_prop_id int NOT NULL, 
  cembio_id INT NULL, -- INTERNAL ID FROM CEMBIO TO RELATE WITH THE PRODUCT IONS
  eff_mobility double default null, -- eff mobility By default null
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  CONSTRAINT compound_id_eff_mob_constraint FOREIGN KEY (compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  CONSTRAINT eff_mob_exp_prop_id_eff_mob_constraint FOREIGN KEY (eff_mob_exp_prop_id) REFERENCES eff_mob_experimental_properties(eff_mob_exp_prop_id) on DELETE CASCADE,
  CONSTRAINT ce_exp_prop_id_eff_mob_constraint FOREIGN KEY (ce_exp_prop_id) REFERENCES ce_experimental_properties(ce_exp_prop_id) on DELETE CASCADE,
  UNIQUE KEY `eff_mob_key` (`compound_id`,`eff_mob_exp_prop_id`), 
  INDEX eff_mob_index (eff_mobility),
  INDEX eff_mob_ce_exp_prop_id_index (ce_exp_prop_id),
  INDEX eff_mob_compound_id_index (compound_id), 
  INDEX eff_mob_cembio_id_index (cembio_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Check values of Compound name between brackets 
create table compound_ce_product_ion (
  ce_product_ion_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
  ion_source_voltage INT default NULL, -- ion_source_voltage in the ionization source
  ce_product_ion_mz double not null default 0, 
  ce_product_ion_intensity double default null, 
  ce_transformation_type varchar(20) not null, -- Adduct, fragment, etc.
  ce_product_ion_name varchar(100), 
  eff_mob_id int NOT NULL,
  compound_id_own INT, -- Identification of the fragment
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  CONSTRAINT compound_ce_product_ion_compound_id_constraint FOREIGN KEY (eff_mob_id) REFERENCES eff_mob(eff_mob_id) on DELETE CASCADE,
  CONSTRAINT compound_id_own_constraint FOREIGN KEY (compound_id_own) REFERENCES compounds(compound_id) on delete set null, 
  UNIQUE KEY `compound_ce_product_ion_key` (`ion_source_voltage`,`ce_product_ion_mz`,`eff_mob_id`), 
  INDEX cepi_mz_index (ce_product_ion_mz), 
  INDEX cepi_peak_intensity_index (ce_product_ion_intensity),
  INDEX cepi_ce_transformation_type_index (ce_transformation_type),
  INDEX cepi_eff_mob_id_id_index (eff_mob_id),
  INDEX cepi_ion_source_voltage_index (ion_source_voltage)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- After inserting the values of compound_ce_product_ion, then we need to unify the transformations.
-- Check the distinct types, and according to that:
select distinct ce_transformation_type from compound_ce_product_ion;
update compound_ce_product_ion set ce_transformation_type="[M+K]+" where ce_transformation_type="[M+K]";
update compound_ce_product_ion set ce_transformation_type="[M+Na]+" where ce_transformation_type="[M+Na]";
update compound_ce_product_ion set ce_transformation_type="[M+H]+" where ce_transformation_type="[M+H]";
update compound_ce_product_ion set ce_transformation_type="[M+H+Na]2+" where ce_transformation_type="[M+H+Na]";
update compound_ce_product_ion set ce_transformation_type="[M+2Na]++" where ce_transformation_type="[M+2Na]";
update compound_ce_product_ion set ce_transformation_type="[M+2Na-H]+" where ce_transformation_type="[M+2Na-H]";
update compound_ce_product_ion set ce_transformation_type="[M+2K+H]+++" where ce_transformation_type="[M+2K+H]+";
update compound_ce_product_ion set ce_transformation_type="[M+H+Na]2+" where ce_transformation_type="[M+H+Na]" or ce_transformation_type="[M+H+Na]+";
update compound_ce_product_ion set ce_transformation_type="[M+H+K]2+" where ce_transformation_type="[M+H+K]" or ce_transformation_type="[M+H+K]+";
update compound_ce_product_ion set ce_transformation_type="[M+2H]++" where ce_transformation_type="[M+2H]+2" or ce_transformation_type="[M+2H]+" or ce_transformation_type="[M+2H]";
update compound_ce_product_ion set ce_transformation_type="[M+2Na-H]+" where ce_transformation_type like "%Fragmnet%";
update compound_ce_product_ion set ce_transformation_type="[M+2K+H]3+" where ce_transformation_type like "[M+2K+H]+++";
update compound_ce_product_ion set ce_transformation_type="[M+3H]3+" where ce_transformation_type like "[M+3H]+++";
update compound_ce_product_ion set ce_transformation_type="[M+2H]2+" where ce_transformation_type like "[M+2H]++";
update compound_ce_product_ion set ce_transformation_type="[M+2Na]2+" where ce_transformation_type like "[M+2Na]++";
select distinct ce_transformation_type from compound_ce_product_ion;
