################################################ TABLES FOR LOGIN ######################################################
drop table users;

CREATE TABLE users( 
user_id int(20) NOT NULL AUTO_INCREMENT, 
username VARCHAR(45) UNIQUE NOT NULL,
first_name VARCHAR(45) NOT NULL,
last_name VARCHAR(45) NOT NULL,
email varchar(45) UNIQUE NOT NULL,
password CHAR(128) NOT NULL,
reg_date date NOT NULL,
is_admin BOOLEAN NOT NULL default 0,
PRIMARY KEY(user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

################################################ INSERTING A VALUE ######################################################
INSERT INTO users (username, first_name, last_name, email, password, reg_date, is_admin) VALUES('alberto','alberto','alberto', 'albgil01@ucm.es',ENCRYPT('alberto', 'alberto'), current_date(),true);
