 ############################# MSMS TABLE ######################################

drop table msms_peaks;
drop table msms_reference;
drop table msms;

CREATE TABLE `msms` (
  `msms_id` int(11) NOT NULL,
  `hmdb_id` varchar(20) DEFAULT NULL,
  `voltage` int(11) DEFAULT NULL,
  `voltage_level` varchar(10) DEFAULT NULL, -- low < 15V; medium >=15V and <30V; high >=30V
  `instrument_type` varchar(100) DEFAULT NULL,
  `ionization_mode` int(11) DEFAULT NULL, -- 1 positive; 2 negative
  `peak_count` int(11) DEFAULT NULL,
  `compound_id` int(11) DEFAULT NULL,
  `predicted` int(11) DEFAULT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  PRIMARY KEY (`msms_id`),
  UNIQUE KEY `msms_id_UNIQUE` (`msms_id`),
  CONSTRAINT `compound_id` FOREIGN KEY (`compound_id`) REFERENCES `compounds` (`compound_id`) ON DELETE CASCADE,
  CONSTRAINT `hmdb_id` FOREIGN KEY (`hmdb_id`) REFERENCES `compounds_hmdb` (`hmdb_id`) ON DELETE CASCADE ON UPDATE CASCADE, 
  INDEX msms_predicted_index (predicted),
  INDEX msms_ionization_mode_index (ionization_mode),
  INDEX msms_voltage_level_index (voltage_level)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

############################# PEAKS TABLE ######################################

CREATE TABLE `msms_peaks` (
  `peak_id` int(11) NOT NULL,
  `intensity` double DEFAULT NULL,
  `mz` double DEFAULT NULL,
  `msms_id` int(11) DEFAULT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  FOREIGN KEY (msms_id) REFERENCES msms(msms_id) on DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (`peak_id`),
  INDEX peaks_msms_id_index (msms_id), 
  INDEX peaks_msms_intensity_index (intensity), 
  INDEX peaks_msms_mz_index (mz)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

############################# REFERENCES TABLE ######################################

CREATE TABLE `msms_reference` (
  `reference_id` int(11) NOT NULL,
  `pubmed_id` varchar(20) DEFAULT NULL,
  `ref_text` varchar(700) DEFAULT NULL,
  `msms_id` int(11) DEFAULT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  FOREIGN KEY (msms_id) REFERENCES msms(msms_id) on DELETE CASCADE,
  PRIMARY KEY (`reference_id`),
  INDEX peaks_msms_id_index (msms_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
