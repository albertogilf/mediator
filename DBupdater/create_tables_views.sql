################################################ NEW DATA MODEL ######################################################

################################################ TABLES FOR LOGIN ######################################################

drop table users;

CREATE TABLE users( 
user_id int(20) NOT NULL AUTO_INCREMENT, 
username VARCHAR(45) UNIQUE NOT NULL,
first_name VARCHAR(45) NOT NULL,
last_name VARCHAR(45) NOT NULL,
email varchar(45) UNIQUE NOT NULL,
password CHAR(128) NOT NULL,
reg_date date NOT NULL,
is_admin BOOLEAN NOT NULL default 0,
PRIMARY KEY(user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


########################################### TABLES FOR COMPOUNDS #####################################################


drop table compounds_chebi;
drop table compounds_aspergillus;
drop table compounds_fahfa;
drop table compounds_kegg;
drop table compounds_agilent;
drop table compounds_lm;
drop table compounds_hmdb;
drop table compounds_pc;
drop trigger compounds_in_house_insert;
drop table compounds_in_house;
drop table compounds_in_house_seq;
drop table compound_identifiers;
drop table compounds_lipids_classification;
drop table compounds_lm_classification;
drop table compound_classyfire_classification;
drop table classyfire_classification;
drop table classyfire_classification_dictionary;
drop table compounds_pathways;
drop table compounds_reactions_kegg;
drop table pathways;
drop table compound_chain;
drop table chains;
drop table compounds;
drop table compounds_cas;


-- COMPOUND TYPE: 0 for metabolites, 1 for lipids, 2 for peptides, 3 for oxidized compounds
CREATE TABLE compounds (
  compound_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  cas_id varchar(100) UNIQUE DEFAULT NULL,
  compound_name text not null,
  formula varchar(100) DEFAULT '',
  mass double DEFAULT 0, 
  charge_type int default 0, -- charge 0 for neutral, 1 for positive 2 for negative
  charge_number int default 0, -- number of charges (negative or positive)
  formula_type varchar(20) DEFAULT NULL, -- CHNOPS, CHNOPSD, CHNOPSCL, CHNOPSCLD, ALLD or ALL
  formula_type_int int, -- 0 CHNOPS, 1 CHNOPSD, 2 CHNOPSCL, 3 CHNOPSCLD, 4 ALL, 5 ALLD
  compound_type int default 0, -- type of compound: 0 for metabolite, 1 for lipids, 2 for peptide
  compound_status int default 0, -- status of compound: 0 expected, 1 detected, 2 quantified, 3 predicted (HMDB)
  logP double default null, -- LogP of the compound. By default null
  RT_pred double default null, -- LogP of the compound. By default null
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  INDEX compounds_cas_id_index (cas_id),
  INDEX compounds_formula_index (formula),
  INDEX compounds_mass_index (mass),
  INDEX compounds_charge_type_index (charge_type),
  INDEX compounds_formula_type_index (formula_type),
  INDEX compounds_formula_type_int_index (formula_type_int),
  INDEX compounds_compound_type_index (compound_type),
  INDEX compounds_compound_status_index (compound_status),
  INDEX compounds_logP_index (logP), 
  INDEX compounds_RT_pred_index (RT_pred)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE pathways (
  pathway_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  pathway_map varchar(100) UNIQUE NOT NULL,
  pathway_name varchar(100) NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  INDEX pathway_map_index (pathway_map),
  INDEX pathway_name_index (pathway_name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE compounds_pathways (
  compound_id INT,
  pathway_id INT,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  FOREIGN KEY (pathway_id) REFERENCES pathways(pathway_id) on DELETE CASCADE,
  CONSTRAINT pk_pathways_kegg PRIMARY KEY (compound_id,pathway_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE compounds_kegg (
  compound_id INT NOT NULL,
  kegg_id varchar(100) NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  CONSTRAINT pk_compounds_kegg PRIMARY KEY (compound_id,kegg_id),
  INDEX kegg_id_index (kegg_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE compounds_agilent (
  compound_id INT NOT NULL,
  agilent_id varchar(100) NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  CONSTRAINT pk_compounds_agilent PRIMARY KEY (compound_id,agilent_id),
  INDEX agilent_id_index (agilent_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE compounds_lm (
  compound_id INT NOT NULL,
  lm_id varchar(100) NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  CONSTRAINT pk_compounds_lm PRIMARY KEY (compound_id,lm_id),
  INDEX lm_id_index (lm_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE compounds_hmdb (
  compound_id INT NOT NULL,
  hmdb_id varchar(100) NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  CONSTRAINT pk_compounds_hmdb PRIMARY KEY (compound_id,hmdb_id),
  INDEX hmdb_id_index (hmdb_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE compounds_pc (
  compound_id INT NOT NULL,
  pc_id INT NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  CONSTRAINT pk_compounds_pc PRIMARY KEY (compound_id,pc_id),
  INDEX pc_id_index (pc_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE compounds_chebi (
  compound_id INT NOT NULL,
  chebi_id INT NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  CONSTRAINT pk_compounds_chebi PRIMARY KEY (compound_id,chebi_id),
  INDEX chebi_id_index (chebi_id)
) ENGINE=InnoDB DEFAULT CHARSET=ut


-- TO DO COMPOUNDS ASPERGILLUS
CREATE TABLE compounds_aspergillus (
  compound_id INT NOT NULL PRIMARY KEY,
  aspergillus_id INT UNIQUE NOT NULL AUTO_INCREMENT,
  biological_activity text default NULL,
  mesh_nomenclature varchar(100) default NULL,
  iupac_classification varchar(100) default NULL,
  aspergillus_web_name varchar(100),
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE, 
  INDEX aspergillus_id_index (aspergillus_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE compounds_knapsack (
  compound_id INT NOT NULL,
  knapsack_id varchar(100) NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  CONSTRAINT pk_compounds_knapsack PRIMARY KEY (compound_id,knapsack_id),
  INDEX knapsack_id_index (knapsack_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE compounds_npatlas (
  compound_id INT NOT NULL,
  npatlas_id INT NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  CONSTRAINT pk_compounds_npatlas PRIMARY KEY (compound_id,npatlas_id),
  INDEX npatlas_id_index (npatlas_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE compounds_fahfa (
  compound_id INT NOT NULL,
  fahfa_id INT NOT NULL,
  oh_position INT NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  CONSTRAINT pk_compounds_fahfa PRIMARY KEY (compound_id,fahfa_id),
  INDEX fahfa_id_index (fahfa_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE compounds_in_house (
  compound_id INT NOT NULL,
  in_house_id varchar(100) NOT NULL,
  -- CANONICAL OR NEW
  source_data varchar(20) NOT NULL,
  description varchar(300) NOT NULL default '',
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  CONSTRAINT pk_compounds_in_house_id PRIMARY KEY (compound_id,in_house_id),
  INDEX hmdb_id_index (in_house_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- CREATE AUX TABLE AND TRIGGER FOR GENERATING IN_HOUSE_ID

CREATE TABLE compounds_in_house_seq
(
  compounds_in_house_seq_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY
);

DELIMITER $$
CREATE TRIGGER compounds_in_house_insert
BEFORE INSERT ON compounds_in_house
FOR EACH ROW
BEGIN
  INSERT INTO compounds_in_house_seq VALUES (NULL);
  SET NEW.in_house_id = CONCAT(LPAD(LAST_INSERT_ID(), 6, '0'));
END$$
DELIMITER ;


CREATE TABLE compounds_cas (
  cas_id varchar(100) PRIMARY KEY,
  formula varchar(100) DEFAULT NULL,
  mass double DEFAULT 0,
  inchi varchar(1800) DEFAULT NULL,
  inchi_key varchar(50) DEFAULT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  INDEX cas_inchi_key_index (inchi_key)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE compound_identifiers (
  compound_id INT NOT NULL,
  inchi varchar(1800) DEFAULT '',
  inchi_key varchar(50) DEFAULT '' UNIQUE NOT NULL,
  smiles varchar(1200) DEFAULT '',
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  CONSTRAINT pk_inchi PRIMARY KEY (compound_id),
  INDEX inchi_key_index (inchi_key)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE compounds_reactions_kegg (
  compound_id int NOT NULL,
  reaction_id varchar(100) NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  FOREIGN KEY (compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  CONSTRAINT pk_reaction_kegg PRIMARY KEY (compound_id,reaction_id),
  INDEX reaction_id_index (reaction_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE compounds_lipids_classification (
  compound_id int NOT NULL,
  lipid_type varchar(150) DEFAULT '',
  num_chains int default 0,
  number_carbons int,
  double_bonds int,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  FOREIGN KEY (compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  CONSTRAINT pk_lipids_classifcation PRIMARY KEY (compound_id),
  INDEX lipids_classification_lipid_type_index (lipid_type),
  INDEX lipids_classification_num_chains_index (num_chains),
  INDEX lipids_classification_number_carbons_index (number_carbons),
  INDEX lipids_classification_double_bonds_index (double_bonds)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE compounds_lm_classification (
  compound_id int NOT NULL,
  category varchar(10) DEFAULT '',
  main_class varchar(10) DEFAULT '',
  sub_class varchar(10) DEFAULT '',
  class_level4 varchar(10) DEFAULT '',
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  FOREIGN KEY (compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  CONSTRAINT pk_lm_classification PRIMARY KEY (compound_id),
  INDEX lm_classification_category_index (category),
  INDEX lm_classification_main_class_index (main_class),
  INDEX lm_classification_sub_class_index (sub_class),
  INDEX lm_classification_level_4_index (class_level4)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE classyfire_classification_dictionary (
  classyfire_id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  node_id varchar(20) NOT NULL UNIQUE,
  node_name varchar(100) DEFAULT '',
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  INDEX classyfire_classification_dictionary_node_id_index (node_id),
  INDEX classyfire_classification_dictionary_node_name_index (node_name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE classyfire_classification (
  node_id varchar(20) NOT NULL UNIQUE,
  kingdom varchar(20) DEFAULT '',
  super_class varchar(20) DEFAULT '',
  main_class varchar(20) DEFAULT '',
  sub_class varchar(20) DEFAULT '',
  level_5 varchar(20) DEFAULT '',
  level_6 varchar(20) DEFAULT '',
  level_7 varchar(20) DEFAULT '',
  level_8 varchar(20) DEFAULT '',
  level_9 varchar(20) DEFAULT '',
  level_10 varchar(20) DEFAULT '',
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  FOREIGN KEY (node_id) REFERENCES classyfire_classification_dictionary(node_id) on DELETE CASCADE,
  CONSTRAINT pk_compound_classyfire PRIMARY KEY (node_id),
  INDEX classyfire_classification_node_id_index (node_id),
  INDEX classyfire_classification_category_index (kingdom),
  INDEX classyfire_classification_super_class_index (super_class),
  INDEX classyfire_classification_class_index (main_class),
  INDEX classyfire_classification_sub_class_index (sub_class),
  INDEX classyfire_classification_level_5_index (level_5),
  INDEX classyfire_classification_level_6_index (level_6),
  INDEX classyfire_classification_level_7_index (level_7),
  INDEX classyfire_classification_level_8_index (level_8),
  INDEX classyfire_classification_level_9_index (level_9),
  INDEX classyfire_classification_level_10_index (level_10)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- TEMPORARY ANNOTATIONS UNTIL IT IS CONSISTENT

ALTER TABLE classyfire_classification RENAME COLUMN direct_parent TO level_5;
ALTER TABLE classyfire_classification RENAME COLUMN level_7 TO level_6;
ALTER TABLE classyfire_classification RENAME COLUMN level_8 TO level_7;
ALTER TABLE classyfire_classification RENAME COLUMN level_9 TO level_8;
ALTER TABLE classyfire_classification RENAME COLUMN level_10 TO level_9;
ALTER TABLE classyfire_classification RENAME COLUMN level_11 TO level_10;

drop index classyfire_classification_direct_parent_index on classyfire_classification;
drop index classyfire_classification_level_7_index on classyfire_classification;
drop index classyfire_classification_level_8_index on classyfire_classification;
drop index classyfire_classification_level_9_index on classyfire_classification;
drop index classyfire_classification_level_10_index on classyfire_classification;
drop index classyfire_classification_level_11_index on classyfire_classification;

alter table classyfire_classification add INDEX classyfire_classification_level_5_index (level_5);
alter table classyfire_classification add INDEX classyfire_classification_level_6_index (level_6);
alter table classyfire_classification add INDEX classyfire_classification_level_7_index (level_7);
alter table classyfire_classification add INDEX classyfire_classification_level_8_index (level_8);
alter table classyfire_classification add INDEX classyfire_classification_level_9_index (level_9);
alter table classyfire_classification add INDEX classyfire_classification_level_10_index (level_10);

CREATE TABLE compound_classyfire_classification (
  compound_id int NOT NULL,
  node_id varchar(20) NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  FOREIGN KEY (compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  FOREIGN KEY (node_id) REFERENCES classyfire_classification(node_id) on DELETE CASCADE,
  CONSTRAINT pk_compound_classyfire_classification PRIMARY KEY (compound_id, node_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE chains( 
  chain_id int NOT NULL AUTO_INCREMENT PRIMARY KEY, 
  num_carbons int NOT NULL,
  double_bonds int NOT NULL,
  oxidation VARCHAR(10),
  mass double, 
  formula VARCHAR(100),
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  CONSTRAINT UC_chains_carbons_bonds_oxid UNIQUE (num_carbons,double_bonds,oxidation),
  INDEX chains_num_carbons_index (num_carbons),
  INDEX chains_double_bonds_index (double_bonds),
  INDEX chains_oxidation_index (oxidation)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE compound_chain (
  compound_id INT NOT NULL,
  chain_id int NOT NULL, 
  number_chains int NOT NULL default 1, 
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  FOREIGN KEY (chain_id) REFERENCES chains(chain_id) on DELETE CASCADE,
  CONSTRAINT pk_compound_chain PRIMARY KEY (compound_id, chain_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE ontology_terms (
  ontology_term_id int NOT NULL PRIMARY KEY, 
  term varchar(255) DEFAULT NULL,
  definition text,
  external_id varchar(255) DEFAULT NULL,
  external_source varchar(255) DEFAULT NULL,
  ontology_comment varchar(255) DEFAULT NULL,
  curator varchar(255) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  ontology_level int(11) DEFAULT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  KEY fk_ontology_term_parent_id (parent_id),
  CONSTRAINT fk_ontology_term_parent_id FOREIGN KEY (parent_id) REFERENCES ontology_terms (ontology_term_id),
  INDEX ontology_terms_term_index (term)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE compound_ontology_terms (
  compound_id int(11) NOT NULL,
  ontology_term_id int(11) NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  FOREIGN KEY (compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  FOREIGN KEY (ontology_term_id) REFERENCES ontology_terms(ontology_term_id) on DELETE CASCADE,
  CONSTRAINT pk_compound_ontology_terms PRIMARY KEY (compound_id, ontology_term_id)
) ENGINE=InnoDB AUTO_INCREMENT=5693416 DEFAULT CHARSET=utf8;

CREATE TABLE ontology_synonyms (
  ontology_term_id int(11) NOT NULL,
  ontology_synonym varchar(255) NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  FOREIGN KEY  (ontology_term_id) REFERENCES ontology_terms (ontology_term_id) on DELETE CASCADE,
  CONSTRAINT pk_ontology_synonyms PRIMARY KEY (ontology_term_id, ontology_synonym)
) ENGINE=InnoDB AUTO_INCREMENT=5026 DEFAULT CHARSET=utf8;


######################         TABLES    FOR    ORGANISM CLASSIFICATION       #########################################


/*
drop table organism_reference;
drop table compound_reference;
drop table compound_organism;
*/
drop table compound_organism_reference;
drop table reference;
drop table organism;


CREATE TABLE organism (
  organism_id int NOT NULL AUTO_INCREMENT PRIMARY KEY, 
  organism_name varchar(255) DEFAULT NULL,
  organism_level int not NULL, -- Value from level 4 (cells); level 3 (specie); level 2(family); and level 1(kingdom)
  parent_id int DEFAULT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  KEY fk_organism_parent_id (parent_id),
  CONSTRAINT fk_organism_parent_id FOREIGN KEY (parent_id) REFERENCES organism (organism_id),
  INDEX organism_name_index (organism_name),
  INDEX organism_level_index (organism_level)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE reference (
  reference_id int NOT NULL AUTO_INCREMENT PRIMARY KEY, 
  reference_text varchar(255) UNIQUE NOT NULL,
  doi varchar(255) UNIQUE DEFAULT NULL,
  link varchar(255) UNIQUE DEFAULT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  INDEX reference_reference_text_index (reference_text),
  INDEX reference_doi_index (doi),
  INDEX reference_link_index (link)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE compound_organism (
  compound_id int NOT NULL,
  organism_id int NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  FOREIGN KEY (compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  FOREIGN KEY (organism_id) REFERENCES organism(organism_id) on DELETE CASCADE,
  CONSTRAINT pk_compound_organism PRIMARY KEY (compound_id, organism_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE compound_reference (
  compound_id int NOT NULL,
  reference_id int NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  FOREIGN KEY (compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  FOREIGN KEY (reference_id) REFERENCES reference(reference_id) on DELETE CASCADE,
  CONSTRAINT pk_compound_reference PRIMARY KEY (compound_id, reference_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE organism_reference (
  organism_id int NOT NULL,
  reference_id int NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  FOREIGN KEY (organism_id) REFERENCES organism(organism_id) on DELETE CASCADE,
  FOREIGN KEY (reference_id) REFERENCES reference(reference_id) on DELETE CASCADE,
  CONSTRAINT pk_organism_reference PRIMARY KEY (organism_id, reference_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

######################         TABLES    FOR    CCS       #########################################

drop TABLE compound_ccs;
drop table adduct;
drop table buffer_gas;

CREATE TABLE adduct (
  adduct_id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  ionization_mode INT NOT NULL default 1,  -- 1: positive 2: negative
  adduct_type varchar(255) UNIQUE NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  INDEX adduct_ionization_mode (ionization_mode), 
  INDEX adduct_type_index (adduct_type)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO ADDUCT (ionization_mode, adduct_type) values (1,"M+H");
INSERT INTO ADDUCT (ionization_mode, adduct_type) values (2,"M-H");
INSERT INTO ADDUCT (ionization_mode, adduct_type) values (1,"M+Na");

CREATE TABLE buffer_gas (
  buffer_gas_id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  buffer_gas_name varchar(255) UNIQUE NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO BUFFER_GAS (buffer_gas_name) values ("N2");

CREATE TABLE compound_ccs (
  compound_id int NOT NULL,
  adduct_id int NOT NULL,
  buffer_gas_id int NOT NULL, 
  ccs_value float NOT NULL, 
  adduct_intensity int default NULL, 
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  FOREIGN KEY (compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  FOREIGN KEY (adduct_id) REFERENCES adduct(adduct_id) on DELETE CASCADE,
  FOREIGN KEY (buffer_gas_id) REFERENCES buffer_gas(buffer_gas_id) on DELETE CASCADE,
  CONSTRAINT pk_compound_reference PRIMARY KEY (compound_id, adduct_id, buffer_gas_id),
  INDEX compound_ccs_value_index (ccs_value)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



########################################### TABLES FOR GENERATED COMPOUNDS #####################################################

drop table compound_gen_identifiers;
drop table compounds_gen;

CREATE TABLE compounds_gen (
  compound_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  MINE_id INT UNIQUE DEFAULT '',
  MINE_file_id varchar(100) UNIQUE DEFAULT '',
  compound_name text not null,
  formula varchar(100) DEFAULT '',
  mass double DEFAULT 0,
  -- charge 0 for neutral, 1 for positive 2 for negative
  charge_type int default 0,
  -- number of charges (negative or positive)
  charge_number int default 0,
  np_likeness double DEFAULT 0,
  -- CHNOPS (0), CHNOPSD, CHNOPSCL, CHNOPSCLD, ALLD or ALL
  formula_type varchar(20) DEFAULT '',
  -- CHNOPS (0), CHNOPSD (1), CHNOPSCL (2), CHNOPSCLD (3), ALL (4) or ALLD (5)
  formula_type_int int,
  logP double default null, -- LogP of the compound. By default 0
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  INDEX compounds_gen_cas_id_index_gen (MINE_id),
  INDEX compounds_gen_mass_index_gen (mass),
  INDEX compounds_gen_formula_type_index (formula_type),
  INDEX compounds_gen_formula_type_int_index (formula_type_int)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE compound_gen_identifiers (
  compound_id INT NOT NULL,
  inchi varchar(1800) DEFAULT '',
  inchi_key varchar(50) DEFAULT '' UNIQUE NOT NULL,
  smiles varchar(1200) DEFAULT '',
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (compound_id) REFERENCES compounds_gen(compound_id) on DELETE CASCADE,
  CONSTRAINT pk_inchi_gen PRIMARY KEY (compound_id),
  INDEX generated_inchi_key_index (inchi_key)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


########################################### TABLES FOR GENERATED COMPOUNDS #####################################################

