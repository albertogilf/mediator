update compounds_agilent set compound_id=32675  where agilent_id=961;

SELECT ca.compound_id, agilent_id
FROM compounds_agilent ca INNER JOIN
     ( SELECT ca2.compound_id, COUNT(compound_id) AS NumOccurrences
        FROM compounds_agilent ca2
        GROUP BY compound_id
        HAVING ( COUNT(compound_id) > 1 ) ) AS duplicates
ON ca.compound_id = duplicates.compound_id order by ca.compound_id;


SELECT ca.compound_id, hmdb_id
FROM compounds_hmdb ca INNER JOIN
     ( SELECT ca2.compound_id, COUNT(compound_id) AS NumOccurrences
        FROM compounds_hmdb ca2
        GROUP BY compound_id
        HAVING ( COUNT(compound_id) > 1 ) ) AS duplicates
ON ca.compound_id = duplicates.compound_id order by ca.compound_id;

SELECT ca.compound_id, kegg_id
FROM compounds_kegg ca INNER JOIN
     ( SELECT ca2.compound_id, COUNT(compound_id) AS NumOccurrences
        FROM compounds_kegg ca2
        GROUP BY compound_id
        HAVING ( COUNT(compound_id) > 1 ) ) AS duplicates
ON ca.compound_id = duplicates.compound_id order by ca.compound_id;

SELECT ca.compound_id, lm_id
FROM compounds_lm ca INNER JOIN
     ( SELECT ca2.compound_id, COUNT(compound_id) AS NumOccurrences
        FROM compounds_lm ca2
        GROUP BY compound_id
        HAVING ( COUNT(compound_id) > 1 ) ) AS duplicates
ON ca.compound_id = duplicates.compound_id order by ca.compound_id;

SELECT ca.compound_id, pc_id
FROM compounds_pc ca INNER JOIN
     ( SELECT ca2.compound_id, COUNT(compound_id) AS NumOccurrences
        FROM compounds_pc ca2
        GROUP BY compound_id
        HAVING ( COUNT(compound_id) > 1 ) ) AS duplicates
ON ca.compound_id = duplicates.compound_id order by ca.compound_id;

SELECT ca.compound_id
FROM compounds ca INNER JOIN
     ( SELECT ca2.compound_id, COUNT(compound_id) AS NumOccurrences
        FROM compounds ca2
        GROUP BY cas_id
        HAVING ( COUNT(cas_id) > 1 ) ) AS duplicates
ON ca.compound_id = duplicates.compound_id order by ca.compound_id;

-- delete duplicates FROM COMPOUNDS_PC
delete from compounds_pc cpc where 
compound_id in 
(SELECT compound_id
   FROM compounds_pc 
   GROUP BY compound_id
   HAVING ( COUNT(compound_id) > 1 ) 
   )
and pc_id not in 
   (SELECT MIN(pc_id) as pc_id
   FROM compounds_pc 
   GROUP BY compound_id
   HAVING ( COUNT(compound_id) > 1 ) 
   );
   
select * from compounds_pc cpc
INNER JOIN (
   SELECT compound_id, MIN(pc_id) as pc_id
   FROM compounds_pc 
   GROUP BY compound_id
   HAVING ( COUNT(compound_id) > 1 ) 
) as KeepRows ON
   cpc.compound_id = KeepRows.compound_id
WHERE
   KeepRows.compound_id = cpc.compound_id 
   and cpc.pc_id != KeepRows.pc_id;
