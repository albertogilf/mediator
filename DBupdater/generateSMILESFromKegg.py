# -*- coding: utf-8 -*-
"""
Created on Tue May  1 10:45:56 2018

@author: Alberto Gil de la Fuente
"""
import rdkit
from rdkit import Chem
from rdkit.Chem import AllChem
from rdkit.Chem import rdmolops
import csv
import os
import time

def generateSmilesFromMol(mol):
    return (Chem.MolToSmiles(mol,True), True)

def main():
    #resources_dir = "C:/Users/ceu/Desktop/alberto/CMM/DBUpdater/resources/kegg/molfiles"
    resources_dir = "20130513ceuMassUpdating/resources/kegg/molfiles"
    os.chdir(resources_dir)
    print(os.getcwd())
    #molfile='C00001.mol'
    for molfile in os.listdir(os.getcwd()):
        if(molfile.endswith(".mol")):
            #print(molfile)
            m = Chem.MolFromMolFile(molfile)
            #print(m)
            try:
              mp = AllChem.MMFFGetMoleculeProperties(m)
              #print(mp)
              if m:
                smiles = generateSmilesFromMol(m)
                #q = GetFormalCharge(m)
                #print(molfile)
                #print(q)
                if(smiles[1]==True):
                  #Save Smiles 
                  smiles = smiles[0]
                  #print('smiles: ', smiles)
                  keggId = ('.').join(molfile.split('.')[:-1])
                  smiles_file_name=keggId + ".smiles"
                  #print(smiles_file_name)
                  smiles_file = open(smiles_file_name, "w")
                  smiles_file.write(smiles)
                  smiles_file.close()
                else:
                   print('error')
            except:
              print(molfile + 'is a multimer')
        

if __name__ == "__main__":
    # execute only if run as a script
    main()

