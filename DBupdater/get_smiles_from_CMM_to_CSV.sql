select MIN(pc_id) as "pid", ci.compound_id as "CMM_ID", SMILES from compound_identifiers ci 
left join compounds_pc cpc on ci.compound_id = cpc.compound_id group by ci.compound_id
order by ci.compound_id asc
INTO OUTFILE 'C:\\Users\\alberto.gildelafuent\\Desktop\\alberto\\uploads_mysql\\CMM_ID_SMILES.csv' 
FIELDS ENCLOSED BY '' 
TERMINATED BY ','
LINES TERMINATED BY '\r\n';

select c.compound_id as "compound_id", compound_name, ci.inchi as "INCHI", SMILES 
from compounds c
inner join compound_identifiers ci on c.compound_id=ci.compound_id
where RT_pred is not null
order by ci.compound_id asc
INTO OUTFILE 'C:\\Users\\alberto.gildelafuent\\Desktop\\alberto\\uploads_mysql\\CMM_list_compounds.csv' 
FIELDS ENCLOSED BY '\"' 
TERMINATED BY '|'
LINES TERMINATED BY '\r\n';


select ci.compound_id, INCHI from compound_identifiers ci 
where SMILES is NULL
order by ci.compound_id asc
INTO OUTFILE 'C:\\Users\\alberto.gildelafuent\\Desktop\\alberto\\uploads_mysql\\INCHI_NO_SMILES.csv' 
FIELDS ENCLOSED BY '' 
TERMINATED BY '\t'
LINES TERMINATED BY '\r\n';


update compound_identifiers set smiles='F[Si-2](F)(F)(F)(F)F.[Na+].[Na+]', inchi_key='TWGUZEUZLCYTCG-UHFFFAOYSA-N' where compound_id=151628;
update compound_identifiers set smiles='', inchi_key='' where compound_id=38207;
update compound_identifiers set smiles='', inchi_key='' where compound_id=38208;
update compound_identifiers set smiles='', inchi_key='' where compound_id=38209;
update compound_identifiers set smiles='', inchi_key='' where compound_id=38210;


