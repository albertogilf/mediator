insert into compounds 
(compound_name, formula, mass, charge_type, charge_number, formula_type, compound_type, compound_status, 
formula_type_int)
VALUES ("","",,,,"",,,);

-- GET COMPOUND ID and set it up here
insert into compound_identifiers (compound_id, inchi, inchi_key, smiles)
VALUES (,"","","");

insert into compounds_in_house (compound_id, in_house_id, source_data,description)
VALUES (,"","","");

-- GET VALUES FROM THE PREVIOUS QUERIES 
-- GET IN HOUSE ID and set it up here

insert into compounds_view 
(compound_id, compound_name, formula, mass, charge_type, charge_number, compound_type, compound_status, 
formula_type_int, in_house_id, inchi, inchi_key, smiles);
VALUES();

-- EXAMPLES

insert into compounds 
(compound_name, formula, mass, charge_type, charge_number, formula_type, compound_type, compound_status, 
formula_type_int)
VALUES ("Cyclic argininosuccinic acid derivative 1","C10H16N4O5",272.112,0,0,"CHNOPS",0,0,0);
insert into compounds 
(compound_name, formula, mass, charge_type, charge_number, formula_type, compound_type, compound_status, 
formula_type_int)
VALUES ("Cyclic argininosuccinic acid derivative 2","C10H16N4O5",272.112,0,0,"CHNOPS",0,0,0);

-- GET COMPOUND ID and set it up here
insert into compound_identifiers (compound_id, inchi, inchi_key, smiles)
VALUES (183082,"InChI=1S/C10H16N4O5/c11-5(9(18)19)2-1-3-14-8(17)6(4-7(15)16)13-10(14)12/h5,17H,1-4,11H2,(H2,12,13)(H,15,16)(H,18,19)/t5-/m0/s1","TZUYPEWROFQNLU-YFKPBYRVSA-N","[H][C@](N)(CCCN1C(=N)NC(CC(O)=O)=C1O)C(O)=O");
insert into compound_identifiers (compound_id, inchi, inchi_key, smiles)
VALUES (183083,"InChI=1S/C10H16N4O5/c11-5(9(18)19)2-1-3-12-10-13-6(4-7(15)16)8(17)14-10/h5-6H,1-4,11H2,(H,15,16)(H,18,19)(H2,12,13,14,17)/t5-,6?/m0/s1","VBXDAAZXSNXFFX-ZBHICJROSA-N","[H][C@](N)(CCCN=C1NC([H])(CC(O)=O)C(O)=N1)C(O)=O");

insert into compounds_in_house (compound_id, in_house_id, source_data,description)
VALUES (183082,"000253","new","");
insert into compounds_in_house (compound_id, in_house_id, source_data,description)
VALUES (183083,"000254","new","");


-- GET VALUES FROM THE PREVIOUS QUERIES 
-- GET IN HOUSE ID and set it up here

insert into compounds_view 
(compound_id, compound_name, formula, mass, charge_type, charge_number, compound_type, compound_status, formula_type_int, in_house_id, inchi, inchi_key, smiles)
VALUES(183082,"Cyclic argininosuccinic acid derivative 1","C10H16N4O5",272.112,0,0,0,0,0, "000253", 
	"InChI=1S/C10H16N4O5/c11-5(9(18)19)2-1-3-14-8(17)6(4-7(15)16)13-10(14)12/h5,17H,1-4,11H2,(H2,12,13)(H,15,16)(H,18,19)/t5-/m0/s1","TZUYPEWROFQNLU-YFKPBYRVSA-N","[H][C@](N)(CCCN1C(=N)NC(CC(O)=O)=C1O)C(O)=O");

insert into compounds_view 
(compound_id, compound_name, formula, mass, charge_type, charge_number, compound_type, compound_status, 
formula_type_int, in_house_id, inchi, inchi_key, smiles)
VALUES(183083,"Cyclic argininosuccinic acid derivative 2","C10H16N4O5",272.112,0,0,0,0,0, "000254", 
"InChI=1S/C10H16N4O5/c11-5(9(18)19)2-1-3-12-10-13-6(4-7(15)16)8(17)14-10/h5-6H,1-4,11H2,(H,15,16)(H,18,19)(H2,12,13,14,17)/t5-,6?/m0/s1","VBXDAAZXSNXFFX-ZBHICJROSA-N","[H][C@](N)(CCCN=C1NC([H])(CC(O)=O)C(O)=N1)C(O)=O");

SELECT TABLE_NAME,`AUTO_INCREMENT`
FROM  INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = 'compounds'
AND   TABLE_NAME   = 'TableName';