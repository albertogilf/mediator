-- INSERT INTO COMPOUNDS_VIEW

insert into compounds_view select c.compound_id, c.cas_id, c.compound_name, c.formula, c.mass, 
c.charge_type, c.charge_number, c.formula_type_int, c.compound_type, 
c.compound_status, c.logP, c.RT_pred, 
MAX(ck.kegg_id), 
MAX(clm.lm_id), 
MAX(ch.hmdb_id), 
MAX(ca.agilent_id), 
MAX(cpc.pc_id), 
MAX(chebi.chebi_id), 
MAX(cih.in_house_id), 
MAX(c_asp.aspergillus_id), 
MAX(c_knapsack.knapsack_id),
MAX(c_npatlas.npatlas_id),
MAX(c_fahfa.fahfa_id), 
c_fahfa.oh_position,
c_asp.biological_activity, c_asp.mesh_nomenclature, c_asp.iupac_classification, c_asp.aspergillus_web_name, 
ci.inchi, ci.inchi_key, ci.smiles, 
clc.lipid_type, clc.num_chains, clc.number_carbons, clc.double_bonds, 
clmc.category, clmc.main_class, clmc.sub_class, clmc.class_level4, 
CURRENT_TIMESTAMP, CURRENT_TIMESTAMP 
from compounds c 
left join compounds_kegg ck on c.compound_id=ck.compound_id 
left join compounds_lm clm on c.compound_id=clm.compound_id 
left join compounds_hmdb ch on c.compound_id=ch.compound_id 
left join compounds_agilent ca on c.compound_id=ca.compound_id 
left join compounds_pc cpc on c.compound_id=cpc.compound_id 
left join compounds_chebi chebi on c.compound_id=chebi.compound_id 
left join compounds_in_house cih on c.compound_id=cih.compound_id 
left join compounds_aspergillus c_asp on c.compound_id=c_asp.compound_id 
left join compounds_knapsack c_knapsack on c.compound_id=c_knapsack.compound_id 
left join compounds_npatlas c_npatlas on c.compound_id=c_npatlas.compound_id 
left join compounds_fahfa c_fahfa on c.compound_id=c_fahfa.compound_id 
left join compound_identifiers ci on c.compound_id=ci.compound_id 
left join compounds_lipids_classification clc on c.compound_id=clc.compound_id 
left join compounds_lm_classification clmc on c.compound_id=clmc.compound_id 
group by compound_id;


-- INSERT INTO COMPOUNDS_GEN_VIEW
insert into compounds_gen_view select c.compound_id, c.compound_name, c.formula, c.mass, 
c.charge_type, c.charge_number, c.formula_type_int, c.MINE_id, c.np_likeness, c.logP, 
ci.inchi, ci.inchi_key, ci.smiles, 
CURRENT_TIMESTAMP, CURRENT_TIMESTAMP
from compounds_gen c 
left join compound_gen_identifiers ci on c.compound_id=ci.compound_id
group by compound_id;