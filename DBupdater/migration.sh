# FIRST -> CREATE TABLE VIEWS.sh

dateMIGRATION=$(date +"%Y%m%d")
NAMEDBBKMIGRATION=compounds.$dateMIGRATION.sql
pathDBBKMIGRATION=/home/alberto/PHD/mediator/DBupdater/migration/$NAMEDBBKMIGRATION
echo 'Trying to backup on: ' $pathDBBKMIGRATION
mysqldump --user=root -p compounds > $pathDBBKMIGRATION

scp $pathDBBKMIGRATION biolab@10.210.228.3:/home/biolab/alberto/migrations/$NAMEDBBKMIGRATION

NAMEAPPBKMIGRATION=mediator.$dateMIGRATION.war
pathAPPBKMIGRATION=/home/alberto/PHD/mediator/DBupdater/migration/$NAMEAPPBKMIGRATION

cp /home/alberto/PHD/mediator/frontEnd/ceu-mediator-v2-2014-7-9/dist/ceuMass.war $pathAPPBKMIGRATION

scp $pathAPPBKMIGRATION biolab@10.210.228.3:/home/biolab/alberto/migrations/$NAMEAPPBKMIGRATION






########################## DUMP FOR ONLY SPECIFIC TABLE
mysqldump --user=root -p compounds compounds > $pathDBBKMIGRATION

###################################################### OLD MIGRATION #################################################
dateMIGRATION=$(date +"%Y%m%d")
NAMEDBBKMIGRATION=compounds.$dateMIGRATION.sql
pathDBBKMIGRATION=/home/alberto/PHD/mediator/DBupdater/migration/$NAMEDBBKMIGRATION
echo 'Trying to backup on: ' $pathDBBKMIGRATION
mysqldump --user=root -p compounds > $pathDBBKMIGRATION

scp $pathDBBKMIGRATION mariano@ramonycajal.eps.uspceu.es:/home/mariano/alberto/migrations/$NAMEDBBKMIGRATION

NAMEAPPBKMIGRATION=mediator.$dateMIGRATION.war
pathAPPBKMIGRATION=/home/alberto/PHD/mediator/DBupdater/migration/$NAMEAPPBKMIGRATION

cp /home/alberto/PHD/mediator/frontEnd/ceu-mediator-v2-2014-7-9/dist/ceuMass.war $pathAPPBKMIGRATION

scp $pathAPPBKMIGRATION mariano@ramonycajal.eps.uspceu.es:/home/mariano/alberto/migrations/$NAMEAPPBKMIGRATION



## OLD
mysqldump --user=root -p BKcompounds_new --ignore-table=BKcompounds_new.BKcompounds_HMDB --ignore-table=BKcompounds_new.BKcompounds_LM --ignore-table=BKcompounds_new.BKcompounds_hmdb_HMDB --ignore-table=BKcompounds_new.BKcompounds_hmdb_LM --ignore-table=BKcompounds_new.BKcompounds_kegg_HMDB --ignore-table=BKcompounds_new.BKcompounds_kegg_LM --ignore-table=BKcompounds_new.BKcompounds_metlin_HMDB --ignore-table=BKcompounds_new.BKcompounds_pathways_HMDB --ignore-table=BKcompounds_new.BKcompounds_pc_HMDB --ignore-table=BKcompounds_new.BKcompounds_pc_LM --ignore-table=BKcompounds_new.BKpathways_HMDB > $pathBKMIGRATION


