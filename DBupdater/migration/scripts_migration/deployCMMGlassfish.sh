dateDeploy=$(date +"%y%m%d")
nameOldDeploy=mediator.$dateDeploy.war
oldDeploy=/home/mariano/Documentos/trabajo/investigacion/grupo-ceu/proyectos/coral-grupo/desplegado/mediator.war
BKDeploy=/home/mariano/Documentos/trabajo/investigacion/grupo-ceu/proyectos/coral-grupo/desplegado/$nameOldDeploy
newDeploy=/home/mariano/alberto/migrations/ceuMass.war
#undeploy application
/home/mariano/glassfish-3.1.2/bin/asadmin undeploy mediator
# stop app server
sudo /home/mariano/glassfish-3.1.2/bin/asadmin stop-domain domain1
#backup for the previous deployed war
mv $oldDeploy $BKDeploy 
#mv the new war to the path
mv $newDeploy $oldDeploy
#start app server
sudo /home/mariano/glassfish-3.1.2/bin/asadmin start-domain domain1
#deploy new war
/home/mariano/glassfish-3.1.2/bin/asadmin deploy $oldDeploy
#list applications deployed
/home/mariano/glassfish-3.1.2/bin/asadmin list-applications
