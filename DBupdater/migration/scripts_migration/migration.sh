dateMIGRATION=$(date +"%y%m%d")
NAMEDBBKMIGRATION=compounds.$dateMIGRATION.sql
pathDBBKMIGRATION=/home/alberto/PHD/mediator/DBupdater/migration/$NAMEDBBKMIGRATION
echo 'Trying to backup on: ' $pathDBBKMIGRATION
mysqldump --user=root -p compounds > $pathDBBKMIGRATION

scp $pathDBBKMIGRATION biolab@10.210.228.3:/home/biolab/alberto/migrations/$NAMEDBBKMIGRATION

NAMEAPPBKMIGRATION=mediator.$dateMIGRATION.war
pathAPPBKMIGRATION=/home/alberto/PHD/mediator/DBupdater/migration/$NAMEAPPBKMIGRATION

cp /home/alberto/PHD/mediator/frontEnd/ceu-mediator-v2-2014-7-9/dist/ceuMass.war $pathAPPBKMIGRATION

scp $pathAPPBKMIGRATION biolab@10.210.228.3:/home/biolab/alberto/migrations/$NAMEAPPBKMIGRATION


