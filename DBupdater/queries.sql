-- To Check compounds from kegg with same structure
select compound_id,kegg_id from compounds_kegg where compound_id in (select compound_id from compounds_kegg group by compound_id having count(*) >1) order by compound_id;


select ci.compound_id,ci.inchi_key,ck.kegg_id from compounds_identifiers ci, compounds_kegg ck where ck.compound_id in (select compound_id from compounds_kegg group by compound_id having count(*) >1) and ci.compound_id=ck.compound_id into outfile '/home/alberto/PHD/mediator/DBupdater/20130513ceuMassUpdating/log/duplicatesKEGG.out.txt';

-- Check compounds unified from Kegg to HMDB
select count(*) from compounds_kegg where compound_id in (select compound_id from compounds_hmdb);
-- Check compounds unified from Kegg to LM
select count(*) from compounds_kegg where compound_id in (select compound_id from compounds_lm);
-- Check compounds unified from Kegg to LM and HMDB
select count(*) from compounds_kegg where compound_id in (select compound_id from compounds_hmdb) and compound_id in (select compound_id from compounds_lm);

-- Check compounds from Kegg associated with two compounds of our database
select count(*) from compounds_kegg where compound_id in (select compound_id from compounds_kegg group by kegg_id having count(*) >1);

-- Check compounds which appears more than one time from HMDB
select k.kegg_id,c.compound_id,h.hmdb_id from compounds_kegg k, compounds_hmdb h, compounds c where k.kegg_id in (select kegg_id from compounds_kegg group by kegg_id having count(*) >1) and c.compound_id=k.compound_id and h.compound_id=c.compound_id order by kegg_id;

-- Check compounds with no mass
select distinct kegg_id from compounds_kegg where compound_id in(select compound_id from compounds where mass is null);
-- Check compounds with no mass and WITH IDENTIFIER
select count(*) from compounds_kegg where compound_id in(select compound_id from compounds where mass is null) and compound_id in (select compound_id from compounds_identifiers);
-- Check compounds with no cas, no formula and no mass
select kegg_id from compounds_kegg where compound_id in(select compound_id from compounds where cas_id is null and formula is null and mass is null);



-- To Check compounds from HMDB with same structure
select compound_id,hmdb_id from compounds_hmdb where compound_id in (select compound_id from compounds_hmdb group by compound_id having count(*) >1) order by compound_id;

select ci.compound_id,ci.inchi_key,ch.hmdb_id from compounds_identifiers ci, compounds_hmdb ch where ch.compound_id in (select compound_id from compounds_hmdb group by compound_id having count(*) >1) and ci.compound_id=ch.compound_id into outfile '/home/alberto/PHD/mediator/DBupdater/20130513ceuMassUpdating/log/duplicatesHMDB.out.txt';

-- Check compounds unified from HMDB to KEGG
select count(*) from compounds_hmdb where compound_id in (select compound_id from compounds_kegg);
-- Check compounds unified from HMDB to LM
select count(*) from compounds_hmdb where compound_id in (select compound_id from compounds_lm);
-- Check compounds unified from HMDB to LM and KEGG
select count(*) from compounds_hmdb where compound_id in (select compound_id from compounds_kegg) and compound_id in (select compound_id from compounds_lm);

-- Check compounds from HMDB associated with two compounds of our database
select count(*) from compounds_hmdb where compound_id in (select compound_id from compounds_hmdb group by hmdb_id having count(*) >1);

-- Check compounds which appears more than one time from HMDB
select h.hmdb_id,c.compound_id,k.kegg_id from compounds_kegg k, compounds_hmdb h, compounds c where h.hmdb_id in (select hmdb_id from compounds_hmdb group by hmdb_id having count(*) >1) and c.compound_id=h.compound_id and k.compound_id=c.compound_id order by hmdb_id;

-- Check compounds with no mass or formula
select c.compound_id, cl.hmdb_id, c.mass, c.formula,c.compound_name, c.formula_type, c.compound_type from compounds c left join compounds_hmdb cl on cl.compound_id=c.compound_id where (c.mass is null or c.formula is null) and cl.hmdb_id is not null;
-- Check compounds with no mass and WITH IDENTIFIER
select count(*) from compounds_hmdb where compound_id in(select compound_id from compounds where mass is null) and compound_id in (select compound_id from compounds_identifiers);
-- Check compounds with no cas, no formula and no mass
select hmdb_id from compounds_hmdb where compound_id in(select compound_id from compounds where cas_id is null and formula is null and mass is null);

--Check wich compounds from LipidMaps has wrong formula (WE DONT INTRODUCE INCHIS OF COMPOUNDS WITH WRONG FORMULA.
select c.compound_id,c.formula from compounds c left join compounds_identifiers ci on c.compound_id=ci.compound_id where ci.compound_id is null and c.formula is not null;
select c.compound_id,c.formula from compounds c where c.formula like '%R%' or c.formula like '%)n%';

-- Check if some compound with a not valid formula has inchi
select c.compound_id,ci.inchi from compounds c left join compounds_identifiers ci on c.compound_id=ci.compound_id where c.formula_type='' and ci.compound_id is not null;




-- To Check compounds from LM with same structure than other compound from LM
select compound_id,lm_id from compounds_lm where compound_id in (select compound_id from compounds_lm group by compound_id having count(*) >1) order by compound_id;
select count(*) from compounds_lm where compound_id in (select compound_id from compounds_lm group by lm_id having count(*) >1);

select ci.compound_id,ci.inchi_key,clm.lm_id from compounds_identifiers ci, compounds_lm clm where clm.compound_id in (select compound_id from compounds_lm group by compound_id having count(*) >1) and ci.compound_id=clm.compound_id into outfile '/home/alberto/PHD/mediator/DBupdater/20130513ceuMassUpdating/log/duplicatesLM.out.txt';

-- Check compounds unified from LM to HMDB
select count(*) from compounds_lm where compound_id in (select compound_id from compounds_hmdb);
-- Check compounds unified from LM to Kegg
select count(*) from compounds_lm where compound_id in (select compound_id from compounds_kegg);
-- Check compounds unified from LM to HMDB and KEGG
select count(*) from compounds_lm where compound_id in (select compound_id from compounds_kegg) and compound_id in (select compound_id from compounds_hmdb);


select clm.lm_id,c.compound_id,h.hmdb_id from compounds_lm clm, compounds_hmdb h, compounds c where clm.lm_id in (select lm_id from compounds_lm group by lm_id having count(*) >1) and c.compound_id=clm.compound_id and h.compound_id=c.compound_id order by lm_id;

-- Check compounds with no mass or formula
select c.compound_id, cl.lm_id, c.mass, c.formula,c.compound_name, c.formula_type, c.compound_type from compounds c left join compounds_lm cl on cl.compound_id=c.compound_id where (c.mass is null or c.formula is null) and cl.lm_id is not null;
-- Check compounds with no mass and WITH IDENTIFIER
select count(*) from compounds_lm where compound_id in(select compound_id from compounds where mass is null) and compound_id in (select compound_id from compounds_identifiers);
-- Check compounds with no cas, no formula and no mass
select lm_id from compounds_lm where compound_id in(select compound_id from compounds where cas_id is null and formula is null and mass is null);

--Check wich compounds from LipidMaps has wrong formula (WE DONT INTRODUCE INCHIS OF COMPOUNDS WITH WRONG FORMULA.
select c.compound_id,c.formula,clm.lm_id from compounds c left join compounds_identifiers ci on c.compound_id=ci.compound_id inner join compounds_lm clm on c.compound_id=clm.compound_id where ci.compound_id is null and c.formula is not null;
select c.compound_id,c.formula from compounds c where c.formula like '%D%';

-- Check if some compound with a not valid formula has inchi (IT SHOULD NOT)
select c.compound_id,ci.inchi from compounds c left join compounds_identifiers ci on c.compound_id=ci.compound_id where c.formula_type='' and ci.compound_id is not null;

-- Check compounds with 
select c.compound_name,clc.*,clm.lm_id from compounds c inner join compounds_lipids_classification clc on c.compound_id=clc.compound_id inner join compounds_lm clm on clm.compound_id=c.compound_id where lipid_type != '' and num_carbons=0;

-- Check  compounds without classification from lipids
select main_class,count(*) from compounds_lipids_classification where lipid_type ='' group by main_class order by 2;



-- To Check compounds from AGILENT with same structure than other compound from AGILENT
select compound_id,agilent_id from compounds_agilent where compound_id in (select compound_id from compounds_agilent group by compound_id having count(*) >1) order by compound_id;
select count(*) from compounds_agilent where compound_id in (select compound_id from compounds_agilent group by agilent_id having count(*) >1);

select ci.compound_id,ci.inchi_key,cagilent.agilent_id from compounds_identifiers ci, compounds_agilent cagilent where cagilent.compound_id in (select compound_id from compounds_agilent group by compound_id having count(*) >1) and ci.compound_id=cagilent.compound_id into outfile '/home/alberto/PHD/mediator/DBupdater/20130513ceuMassUpdating/log/duplicatesAGILENT.out.txt';

-- Check compounds unified from agilent to HMDB
select count(*) from compounds_agilent where compound_id in (select compound_id from compounds_hmdb);
-- Check compounds unified from agilent to Kegg
select count(*) from compounds_agilent where compound_id in (select compound_id from compounds_kegg);
-- Check compounds unified from agilent to HMDB and KEGG
select count(*) from compounds_agilent where compound_id in (select compound_id from compounds_kegg) and compound_id in (select compound_id from compounds_hmdb);


select cagilent.agilent_id,c.compound_id,h.hmdb_id from compounds_agilent cagilent, compounds_hmdb h, compounds c where cagilent.agilent_id in (select agilent_id from compounds_agilent group by agilent_id having count(*) >1) and c.compound_id=cagilent.compound_id and h.compound_id=c.compound_id order by agilent_id;

-- Check compounds with no mass or formula
select c.compound_id, cl.agilent_id, c.mass, c.formula,c.compound_name, c.formula_type, c.compound_type from compounds c left join compounds_agilent cl on cl.compound_id=c.compound_id where (c.mass is null or c.formula is null) and cl.agilent_id is not null;
-- Check compounds with no mass and WITH IDENTIFIER
select count(*) from compounds_agilent where compound_id in(select compound_id from compounds where mass is null) and compound_id in (select compound_id from compounds_identifiers);
-- Check compounds with no cas, no formula and no mass
select agilent_id from compounds_agilent where compound_id in(select compound_id from compounds where cas_id is null and formula is null and mass is null);

--Check wich compounds from LipidMaps has wrong formula (WE DONT INTRODUCE INCHIS OF COMPOUNDS WITH WRONG FORMULA.
select c.compound_id,c.formula from compounds c left join compounds_identifiers ci on c.compound_id=ci.compound_id where ci.compound_id is null and c.formula is not null;
select c.compound_id,c.formula from compounds c where c.formula like '%D%';

-- Check if some compound with a not valid formula has inchi (IT SHOULD NOT)
select c.compound_id,ci.inchi from compounds c left join compounds_identifiers ci on c.compound_id=ci.compound_id where c.formula_type='' and ci.compound_id is not null;




-- To refine Metlin compounds (Compounds with duplicate cas-id)

select * from fast_metlin_compounds where id in ( select id from fast_metlin_compounds group by cas_identifier having count(*) > 1) order by cas_identifier;

select cas_identifier from fast_metlin_compounds where id in ( select id from fast_metlin_compounds group by cas_identifier having count(*) > 1) order by cas_identifier;

-- To see compounds with no mass and no formula

select * from compounds where mass is null and formula is null;
select compound_id,compound_name from compounds where mass is null and formula is null;

select c.compound_id,hmdb_id from compounds c, compounds_hmdb h where mass is null and formula is null and c.compound_id=h.compound_id;

select c.compound_id,kegg_id from compounds c, compounds_kegg k where mass is null and formula is null and c.compound_id=k.compound_id;

select c.compound_id,lm_id from compounds c, compounds_lm l where mass is null and formula is null and c.compound_id=l.compound_id;

select c.compound_id,pc_id from compounds c, compounds_pc p where mass is null and formula is null and c.compound_id=p.compound_id;

--look for compounds which are not present in any databases

select count(*) from compounds c where compound_id not in (select compound_id from compounds_kegg);
select count(*) from compounds c where compound_id not in (select compound_id from compounds_kegg) 
            and compound_id not in (select compound_id from compounds_hmdb);

select c.compound_id from compounds c where compound_id not in (select compound_id from compounds_kegg) 
            and compound_id not in (select compound_id from compounds_hmdb) 
            and compound_id not in (select compound_id from compounds_lm);


select count(*) from compounds c where compound_id not in (select compound_id from compounds_kegg) 
            and compound_id not in (select compound_id from compounds_hmdb) 
            and compound_id not in (select compound_id from compounds_lm) 
            and compound_id not in (select compound_id from compounds_pc);

select count(*) from compounds c where compound_id not in (select compound_id from compounds_kegg) 
            and compound_id not in (select compound_id from compounds_hmdb) 
            and compound_id not in (select compound_id from compounds_lm)
	    and compound_id not in (select compound_id from compounds_agilent);

delete from compounds c where compound_id not in (select compound_id from compounds_kegg) 
            and compound_id not in (select compound_id from compounds_hmdb) 
            and compound_id not in (select compound_id from compounds_lm)
	    and compound_id not in (select compound_id from compounds_agilent);



--Delete compounds with no mass 

select count(*) from compounds where mass is null;
delete from compounds where mass is null;

-- To see duplicate references From HMDB To KEGG
select hmdb_id, kegg_id from compounds_hmdb h, compounds_kegg k where kegg_id in (select kegg_id from compounds_kegg group by kegg_id having count(*) > 1) and h.compound_id=k.compound_id order by kegg_id into outfile '/home/alberto/PHD/mediator/DBupdater/20130513ceuMassUpdating/log/multipleReferencesFromHMDBToKegg.out';

-- To see duplicate references From HMDB To Metlin
select hmdb_id, metlin_id from compounds_hmdb h, compounds_metlin m where metlin_id in (select metlin_id from compounds_metlin group by metlin_id having count(*) > 1) and h.compound_id=m.compound_id order by metlin_id into outfile '/home/alberto/PHD/mediator/DBupdater/20130513ceuMassUpdating/log/multipleReferencesFromHMDBToMetlin.out';

-- To see duplicate references From HMDB To Pub Chemical
select hmdb_id, pc_id from compounds_hmdb h, compounds_pc p where pc_id in (select pc_id from compounds_pc group by pc_id having count(*) > 1) and h.compound_id=p.compound_id order by pc_id into outfile '/home/alberto/PHD/mediator/DBupdater/20130513ceuMassUpdating/log/multipleReferencesFromHMDBToPubC.out';


--To know the number of duplicate references:
select compound_id,hmdb_id,count(*) from compounds_hmdb group by hmdb_id having count(*) > 1;

select compound_id,kegg_id,count(*) from compounds_kegg group by kegg_id having count(*) > 1;

select compound_id,pc_id,count(*) from compounds_pc group by pc_id having count(*) > 1;

select compound_id,metlin_id,count(*) from compounds_metlin group by metlin_id having count(*) > 1;

--To save the registers of HMDB Compounds
CREATE TABLE BKcompounds_HMDB LIKE compounds; 
INSERT BKcompounds_HMDB SELECT * FROM compounds;

CREATE TABLE BKcompounds_hmdb_HMDB LIKE compounds_hmdb; 
INSERT BKcompounds_hmdb_HMDB SELECT * FROM compounds_hmdb;

CREATE TABLE BKcompounds_kegg_HMDB LIKE compounds_kegg; 
INSERT BKcompounds_kegg_HMDB SELECT * FROM compounds_kegg;

CREATE TABLE BKcompounds_pc_HMDB LIKE compounds_pc;
INSERT BKcompounds_pc_HMDB SELECT * FROM compounds_pc;

CREATE TABLE BKpathways_HMDB LIKE pathways;
INSERT BKpathways_HMDB SELECT * FROM pathways;

CREATE TABLE BKcompounds_pathways_HMDB LIKE compounds_pathways;
INSERT BKcompounds_pathways_HMDB SELECT * FROM compounds_pathways;






-- To see duplicate references From LM To KEGG
select lm_id, kegg_id from compounds_lm l, compounds_kegg k where kegg_id in (select kegg_id from compounds_kegg group by kegg_id having count(*) > 1) and l.compound_id=k.compound_id order by kegg_id into outfile '/home/alberto/PHD/mediator/DBupdater/20130513ceuMassUpdating/log/multipleReferencesFromLMToKegg.out';

-- To see duplicate references From LM To HMDB
select lm_id, hmdb_id from compounds_lm l, compounds_hmdb h where hmdb_id in (select hmdb_id from compounds_hmdb group by hmdb_id having count(*) > 1) and l.compound_id=h.compound_id order by hmdb_id into outfile '/home/alberto/PHD/mediator/DBupdater/20130513ceuMassUpdating/log/multipleReferencesFromLMToHMDB.out';

-- To see duplicate references From LM To Pub Chemical
select lm_id, pc_id from compounds_lm l, compounds_pc p where pc_id in (select pc_id from compounds_pc group by pc_id having count(*) > 1) and l.compound_id=p.compound_id order by pc_id into outfile '/home/alberto/PHD/mediator/DBupdater/20130513ceuMassUpdating/log/multipleReferencesFromLMToPubC.out';


--To know what compounds are not in specific table
select compounds.compound_id from compounds left join compounds_agilent on compounds.compound_id=compounds_agilent.compound_id where compounds_agilent.compound_id is null order by compounds.compound_id desc;

--To know what compounds of Table A are in table B
select compounds.compound_id from compounds inner join compounds_agilent on compounds.compound_id=compounds_agilent.compound_id  order by compounds.compound_id desc;

--To know what compounds of Table B are not in table A
select compounds_agilent.compound_id from compounds_kegg right join compounds_agilent on compounds_kegg.compound_id=compounds_agilent.compound_id where compounds_kegg.compound_id is null order by compounds_agilent.compound_id desc;

--To know what compounds of Table B are in table A
select compounds_kegg.compound_id from compounds_kegg inner join compounds_agilent on compounds_kegg.compound_id=compounds_agilent.compound_id  order by compounds_kegg.compound_id desc;


--To save the registers of LipidMaps Compounds
CREATE TABLE BKcompounds_LM LIKE compounds; 
INSERT BKcompounds_LM SELECT * FROM compounds;

CREATE TABLE BKcompounds_hmdb_LM LIKE compounds_hmdb; 
INSERT BKcompounds_hmdb_LM SELECT * FROM compounds_hmdb;

CREATE TABLE BKcompounds_kegg_LM LIKE compounds_kegg; 
INSERT BKcompounds_kegg_LM SELECT * FROM compounds_kegg;

CREATE TABLE BKcompounds_pc_LM LIKE compounds_pc;
INSERT BKcompounds_pc_LM SELECT * FROM compounds_pc;


--To see what compounds have InChI Identifier from each source.

select count(*) from compounds_kegg where compound_id in (select compound_id from compounds_identifiers);

--To see what compounds are repeated in each source


--To see what compounds are in Metlin and Agilent

select count(*) from compounds where metlin_id in (select agilent_id from compounds_agilent);

SELECT count(*)
FROM compounds_metlin
INNER JOIN compounds_agilent
ON compounds_metlin.metlin_id=compounds_agilent.agilent_id;


--To check what compounds from lipid_maps are NOT classified. (CATEGORY = '')
select * from compounds_lipids_classification where category='';


--Histogram from the lipids_type for retention time rules (RT rules)
select lipid_type,count(*) as number_compounds from compounds_lipids_classification where lipid_type!='' group by lipid_type order by number_compounds desc;
select clc.lipid_type,count(*) as number_compounds from compounds_lipids_classification clc left join compounds c on clc.compound_id=c.compound_id where lipid_type!='' group by clc.lipid_type order by 2 desc;


--Histogram from the compounds_pathways for pathway analyzer
select compound_id,count(*) as number_pathways from compounds_pathways group by compound_id order by 2 desc;
select pathway_id,count(*) as number_compounds from compounds_pathways group by pathway_id order by 2 desc;

select cp.compound_id,count(*), c.mass, c.formula as number_pathways from compounds_pathways cp LEFT JOIN compounds c on cp.compound_id=c.compound_id where c.mass is not null group by cp.compound_id order by 2 desc;

--For calculating quartils of frequencies.
select sum(number_compounds) from (select pathway_id,count(*) as number_compounds from compounds_pathways group by pathway_id order by 2 desc limit 197) b;
--WHERE 197 is the percentage of the quartil manually calculated.


-- Calculating number of compounds from HMDB, KEGG and LipidMaps.
select count(*) from compounds where compound_id in (select compound_id from compounds_hmdb) or compound_id in (select compound_id from compounds_kegg) or compound_id in (select compound_id from compounds_lm);


-- Look into compounds naturally charged

mysql -u alberto -p compounds -e "select ci.compound_id, ci.inchi,concat('http://www.hmdb.ca/metabolites/',ch.hmdb_id) , concat('http://www.genome.jp/dbget-bin/www_bget?cpd%3A',ck.kegg_id) , concat('http://www.lipidmaps.org/data/LMSDRecord.php?LMID=',clm.lm_id) , concat('https://metlin.scripps.edu/metabo_info.php?molid=',ca.agilent_id) from compounds_identifiers ci left join compounds_hmdb ch on ci.compound_id=ch.compound_id left join compounds_kegg ck on ci.compound_id=ck.compound_id left join compounds_lm clm on ci.compound_id=clm.compound_id left join compounds_agilent ca on ci.compound_id=ca.compound_id where ci.inchi like '%p+%' or ci.inchi like '%q+%'" -B > /home/alberto/Desktop/charged_compounds/positive_charge.csv


mysql -u alberto -p compounds -e "select ci.compound_id, ci.inchi,concat('http://www.hmdb.ca/metabolites/',ch.hmdb_id) , concat('http://www.genome.jp/dbget-bin/www_bget?cpd%3A',ck.kegg_id) , concat('http://www.lipidmaps.org/data/LMSDRecord.php?LMID=',clm.lm_id) , concat('https://metlin.scripps.edu/metabo_info.php?molid=',ca.agilent_id) from compounds_identifiers ci left join compounds_hmdb ch on ci.compound_id=ch.compound_id left join compounds_kegg ck on ci.compound_id=ck.compound_id left join compounds_lm clm on ci.compound_id=clm.compound_id left join compounds_agilent ca on ci.compound_id=ca.compound_id where ci.inchi like '%p-%' or ci.inchi like '%q-%'" -B > /home/alberto/Desktop/charged_compounds/negative_charge.csv


-- TO SEE AUTO INCREMENT

SELECT `AUTO_INCREMENT`
FROM  INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = 'compounds_new'
AND   TABLE_NAME   = 'compounds_in_house';

-- TO CHANGE AUTO INCREMENT
ALTER TABLE `compounds` AUTO_INCREMENT = 1;
