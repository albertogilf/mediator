-- get compounds from formic, 20 ºC, positive, direct polarity. Expected 226
select c.compound_id, ce_eff_mob_id, ce_exp_prop_id, cembio_id
from compounds c 
inner join ce_eff_mob ce_eff_mob
on c.compound_id = ce_eff_mob.ce_compound_id where ce_exp_prop_id = 1
order by 2;

-- get compounds from acetic, 25 ºC, positive, direct polarity. Expected 371
select c.compound_id, ce_exp_prop_id
from compounds c 
inner join ce_eff_mob ce_eff_mob
on c.compound_id = ce_eff_mob.ce_compound_id where ce_exp_prop_id = 13;
select c.compound_id, ce_exp_prop_id
from compounds c 
inner join ce_eff_mob ce_eff_mob
on c.compound_id = ce_eff_mob.ce_compound_id
inner join ce_experimental_properties_metadata ce_exp_prop
on ce_eff_mob.ce_eff_mob_id = ce_exp_prop.ce_eff_mob_id
where ce_exp_prop_id = 13;

-- get compounds from acetic, 25 ºC, negative, reverse polarity. Expected 278
select c.compound_id, ce_exp_prop_id
from compounds c 
inner join ce_eff_mob ce_eff_mob
on c.compound_id = ce_eff_mob.ce_compound_id where ce_exp_prop_id = 16;
select c.compound_id, ce_exp_prop_id
from compounds c 
inner join ce_eff_mob ce_eff_mob
on c.compound_id = ce_eff_mob.ce_compound_id
inner join ce_experimental_properties_metadata ce_exp_prop
on ce_eff_mob.ce_eff_mob_id = ce_exp_prop.ce_eff_mob_id
where ce_exp_prop_id = 16;


select ce_eff_mob.*
from compounds c 
inner join ce_eff_mob ce_eff_mob
on c.compound_id = ce_eff_mob.ce_compound_id
inner join compound_identifiers ci
on c.compound_id = ci.compound_id
where ci.inchi='InChI=1S/C31H40O2/c1-22(2)12-9-13-23(3)14-10-15-24(4)16-11-17-25(5)20-21-27-26(6)30(32)28-18-7-8-19-29(28)31(27)33/h7-8,12,14,16,18-20H,9-11,13,15,17,21H2,1-6H3/b23-14+,24-16+,25-20+';

# Insert manually a compound for CE:
insert into compounds_chebi(compound_id, chebi_id) VALUES(124925,20680);
INSERT IGNORE INTO ce_eff_mob(ce_compound_id, ce_exp_prop_id, cembio_id, eff_mobility) VALUES(124925, 13, 100468, 685.339);
INSERT IGNORE INTO ce_eff_mob(ce_compound_id, ce_exp_prop_id, cembio_id, eff_mobility) VALUES(124925, 16, 100468, -13.0005);
INSERT IGNORE INTO ce_experimental_properties_metadata(ce_eff_mob_id, experimental_mz, ce_identification_level, ce_sample_type, capillary_voltage, capillary_length, bge_compound_id, absolute_MT, relative_MT, commercial) 
					VALUES(886, NULL, 1, 1, 1000, 30, NULL, NULL, NULL, "SIGMA-ALDRICH");
INSERT IGNORE INTO ce_experimental_properties_metadata(ce_eff_mob_id, experimental_mz, ce_identification_level, ce_sample_type, capillary_voltage, capillary_length, bge_compound_id, absolute_MT, relative_MT, commercial) 
					VALUES(884, NULL, 1, 1, 1000, 30, NULL, NULL, NULL, "SIGMA-ALDRICH");

insert into compounds(cas_id, compound_name, formula, mass, charge_type, charge_number, formula_type, compound_type, compound_status, formula_type_int, logP) 
	VALUES();
insert into compound_identifiers(compound_id,inchi,inchi_key,smiles) VALUES(,'InChI=1S/C34H36N4O4/c1-7-21-17(3)25-13-26-19(5)23(9-11-33(39)40)31(37-26)16-32-24(10-12-34(41)42)20(6)28(38-32)15-30-22(8-2)18(4)27(36-30)14-29(21)35-25/h7-8,13-16,35-38H,1-2,9-12H2,3-6H3,(H,39,40)(H,41,42)','YLWVQFBYKLFJMB-UHFFFAOYSA-N','C=Cc1c(C)c2C=c3c(C)c(CCC(=O)O)c(=Cc4c(CCC(=O)O)c(C)c(C=c5c(C=C)c(C)c(=Cc1[nH]2)[nH]5)[nH]4)[nH]3')

# To change the auto incremental key
SELECT AUTO_INCREMENT
FROM  INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = 'compounds'
AND   TABLE_NAME   = 'ce_eff_mob';
(163321)
delete from ce_eff_mob where ce_eff_mob_id > 226;
alter table ce_eff_mob AUTO_INCREMENT = 226;

SELECT AUTO_INCREMENT
FROM  INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = 'compounds'
AND   TABLE_NAME   = 'ce_experimental_properties_metadata';
(163321)
delete from ce_experimental_properties_metadata where ce_eff_mob_id > 226;
alter table ce_eff_mob AUTO_INCREMENT = 395;