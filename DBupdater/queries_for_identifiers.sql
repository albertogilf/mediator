-- get compounds without identifiers
select distinct c.compound_id,c.mass,c.formula, hmdb_id, lm_id
from compounds c left join compound_classyfire_classification ccc 
on c.compound_id=ccc.compound_id 
left join compounds_hmdb ch 
on c.compound_id=ch.compound_id
left join compounds_lm clm
on c.compound_id=clm.compound_id
inner join compound_identifiers ci 
on c.compound_id = ci.compound_id
where ccc.compound_id is null order by hmdb_id; 

-- get compounds without identifiers and with formula
select distinct c.compound_id,c.mass,c.formula, hmdb_id, lm_id, kegg_id
from compounds c left join compound_identifiers ci
on c.compound_id = ci.compound_id
left join compounds_hmdb ch 
on c.compound_id=ch.compound_id
left join compounds_lm clm
on c.compound_id=clm.compound_id
left join compounds_kegg ck
on c.compound_id=ck.compound_id
where ci.compound_id is null 
and formula is not null order by hmdb_id;

-- get compounds with mass but no formula 
select distinct c.compound_id,c.mass,c.formula, hmdb_id, lm_id, kegg_id
from compounds c
left join compounds_hmdb ch 
on c.compound_id=ch.compound_id
left join compounds_lm clm
on c.compound_id=clm.compound_id
left join compounds_kegg ck
on c.compound_id=ck.compound_id
where (formula is null or formula = '' )
and (mass is not null or mass > 0.1)
order by hmdb_id;


--Find inchi duplicates
SELECT dup.inchi
FROM compound_identifiers
   INNER JOIN (SELECT inchi
               FROM   compound_identifiers
               GROUP  BY inchi
               HAVING COUNT(inchi) > 1) dup
           ON compound_identifiers.inchi = dup.inchi;
		   
--Find smiles duplicates
SELECT c.compound_id, dup.smiles, hmdb_id,lm_id,kegg_id
FROM compound_identifiers c
   INNER JOIN (SELECT smiles
               FROM   compound_identifiers
			   where smiles !=''
               GROUP  BY smiles
               HAVING COUNT(smiles) > 1) dup
           ON c.smiles = dup.smiles 
left join compounds_hmdb ch 
on c.compound_id=ch.compound_id
left join compounds_lm clm
on c.compound_id=clm.compound_id
left join compounds_kegg ck
on c.compound_id=ck.compound_id order by dup.smiles;


-- Get SMILES DUPLICATED FROM LIPIDMAPS
SELECT distinct lm_id, c.smiles
FROM compound_identifiers c
   INNER JOIN (SELECT BINARY smiles as smiles2
               FROM   compound_identifiers
			   where smiles !=''
               GROUP  BY BINARY smiles
               HAVING COUNT(BINARY smiles) > 1) dup
           ON c.smiles = dup.smiles2 
left join compounds_hmdb ch 
on c.compound_id=ch.compound_id
left join compounds_lm clm
on c.compound_id=clm.compound_id
left join compounds_kegg ck
on c.compound_id=ck.compound_id 
where lm_id is not null 
order by c.smiles 
INTO OUTFILE '/tmp/smiles_duplicated.csv'
  FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
  LINES TERMINATED BY '\n';

 -- Get SMILES from KEGG ended in \n
select smiles, kegg_id
from compound_identifiers ci
inner join compounds_kegg ck
on ci.compound_id=ck.compound_id
where smiles is not null and smiles !='' and smiles like "%\n";

-- Get compounds with deuterium
select formula, hmdb_id,kegg_id,lm_id
from compounds c left join compounds_hmdb ch 
on c.compound_id=ch.compound_id
left join compounds_lm clm
on c.compound_id=clm.compound_id
left join compounds_kegg ck
on c.compound_id=ck.compound_id 
where formula like '%D%' ;
  
-- get compounds with no classification but identifiers
select distinct smiles
from compounds c 
inner join compound_identifiers ci
on c.compound_id=ci.compound_id
left join compound_classyfire_classification ccc 
on ci.compound_id=ccc.compound_id
where  smiles is not null and smiles !='' and formula like '%D'
INTO OUTFILE '/tmp/inchis_not_classified.csv'
  FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
  LINES TERMINATED BY '\n';

-- get which compounds are not assigned to any database
select c.compound_id, c.compound_name, c.mass, ci.inchi
from compounds c 
left join compounds_hmdb ch 
on c.compound_id=ch.compound_id
left join compounds_lm clm
on c.compound_id=clm.compound_id
left join compounds_kegg ck
on c.compound_id=ck.compound_id
left join compounds_agilent ca
on c.compound_id=ca.compound_id
left join compounds_in_house cih
on c.compound_id=cih.compound_id
left join compounds_chebi chebi
on c.compound_id=chebi.compound_id
left join compounds_pc pc
on c.compound_id=pc.compound_id
left join compound_identifiers ci
on c.compound_id=ci.compound_id
where ch.compound_id is null
and ck.compound_id is null 
and clm.compound_id is null
and ca.compound_id is null
and cih.compound_id is null
and chebi.compound_id is null;

-- To change auto_increment 
SELECT AUTO_INCREMENT
FROM  INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = 'compounds'
AND   TABLE_NAME   = 'compounds';
(163321)
alter table compounds AUTO_INCREMENT = 163320;