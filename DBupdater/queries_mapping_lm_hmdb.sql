use compounds_new;

desc compounds_classification_hmdb;
desc compounds_lipids_classification;

-- number of compounds classified in lipidmaps and hmdb
select count(*)
from compounds_classification_hmdb cch 
inner join compounds_lipids_classification clc on cch.compound_id=clc.compound_id;

-- number of compounds present in lipidmaps and hmdb
select count(*)
from compounds_hmdb cch 
inner join compounds_lm clc on cch.compound_id=clc.compound_id;

select distinct kingdom,super_class, category, main_class, count(*)
from compounds_classification_hmdb cch 
inner join compounds_lipids_classification clc 
on cch.compound_id=clc.compound_id
where kingdom != 'null' 
group by super_class,category,main_class 
order by super_class, category, main_class;


select lm_id, category, main_class, clc.sub_class, kingdom, super_class, inchi_key
from compounds_classification_hmdb cch
inner join compounds_lipids_classification clc
on clc.compound_id=cch.compound_id
inner join compounds_lm clm
on clm.compound_id=cch.compound_id
inner join compounds_identifiers ci
on ci.compound_id=cch.compound_id
where main_class='FA01'
and super_class !='Lipids and lipid-like molecules' 
order by super_class;



select compound_id, 
from compounds_classification_hmdb cch
where kingdom = 'null';
