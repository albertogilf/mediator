-- CHECK DUPLICATE COMPOUNDS BY NAME
select compound_name from compounds c inner join compounds_aspergillus ca on c.compound_id = ca.compound_id
group by compound_name having count(compound_name) > 1 order by compound_name desc;

select * from compounds where compound_name in( select compound_name from compounds c inner join compounds_aspergillus ca on c.compound_id = ca.compound_id
group by compound_name having count(compound_name) > 1 order by compound_name desc);

-- SELECT ONE COMPOUND
select c.compound_id,c.compound_name, c.formula, c.mass, ck.kegg_id,ca.agilent_id, clm.lm_id, 
ch.hmdb_id, cpc.pc_id, ci.inchi_key, cks.knapsack_id
from compounds c 
left join compound_identifiers ci on c.compound_id=ci.compound_id 
left join compounds_kegg ck on c.compound_id=ck.compound_id 
left join compounds_agilent ca on c.compound_id=ca.compound_id 
left join compounds_lm clm on c.compound_id=clm.compound_id 
left join compounds_hmdb ch on c.compound_id=ch.compound_id 
left join compounds_pc cpc on c.compound_id=cpc.compound_id 
left join compounds_knapsack cks on c.compound_id=cks.compound_id 
where c.compound_name like "%neoxaline%" order by compound_id;

select c.compound_id,c.compound_name, c.formula, c.mass, ck.kegg_id,ca.agilent_id, clm.lm_id, 
ch.hmdb_id, cpc.pc_id, ci.inchi_key, cks.knapsack_id
from compounds c 
left join compound_identifiers ci on c.compound_id=ci.compound_id 
left join compounds_kegg ck on c.compound_id=ck.compound_id 
left join compounds_agilent ca on c.compound_id=ca.compound_id 
left join compounds_lm clm on c.compound_id=clm.compound_id 
left join compounds_hmdb ch on c.compound_id=ch.compound_id 
left join compounds_pc cpc on c.compound_id=cpc.compound_id 
left join compounds_knapsack cks on c.compound_id=cks.compound_id 
where c.compound_name in(select c.compound_name from compounds c inner join compounds_knapsack ck on c.compound_id = ck.compound_id
group by compound_name having count(compound_name) > 1 order by compound_name desc) order by compound_name;


-- UNIFY COMPOUNDS

select * from compounds where compound_id in(@old_compound_id,@new_compound_id);


SET @old_compound_id1 = 164530     ;
--SET @old_compound_id2 = 117450;
--SET @old_compound_id3 = 164585;
SET @new_compound_id = 41189         ;

SET @new_cas_id = (select cas_id from compounds where compound_id = @old_compound_id1);

-- compound_id in (@old_compound_id1,@old_compound_id2,@old_compound_id3);
update compounds set cas_id = null where compound_id = @old_compound_id1;
update compounds set cas_id = @new_cas_id where compound_id=@new_compound_id and cas_id is null;
update compounds_pathways set compound_id=@new_compound_id where compound_id =@old_compound_id1;
update compounds_kegg set compound_id=@new_compound_id where compound_id =@old_compound_id1;
update compounds_agilent set compound_id=@new_compound_id where compound_id =@old_compound_id1;
update compounds_lm set compound_id=@new_compound_id where compound_id =@old_compound_id1;
update compounds_hmdb set compound_id=@new_compound_id where compound_id =@old_compound_id1;
update compounds_pc set compound_id=@new_compound_id where compound_id =@old_compound_id1;
update compounds_chebi set compound_id=@new_compound_id where compound_id =@old_compound_id1;
update compounds_aspergillus set compound_id=@new_compound_id where compound_id =@old_compound_id1;
update compounds_knapsack set compound_id=@new_compound_id where compound_id =@old_compound_id1;
update compounds_fahfa set compound_id=@new_compound_id where compound_id =@old_compound_id1;
update compounds_in_house set compound_id=@new_compound_id where compound_id =@old_compound_id1;
update compounds_reactions_kegg set compound_id=@new_compound_id where compound_id =@old_compound_id1;
update compounds_lipids_classification set compound_id=@new_compound_id where compound_id =@old_compound_id1;
update compounds_lm_classification set compound_id=@new_compound_id where compound_id =@old_compound_id1;
update compound_classyfire_classification set compound_id=@new_compound_id where compound_id =@old_compound_id1;
update compound_chain set compound_id=@new_compound_id where compound_id =@old_compound_id1;
update compound_ontology_terms set compound_id=@new_compound_id where compound_id =@old_compound_id1;
update compound_organism set compound_id=@new_compound_id where compound_id =@old_compound_id1;
update compound_reference set compound_id=@new_compound_id where compound_id =@old_compound_id1;
update msms set compound_id=@new_compound_id where compound_id =@old_compound_id1;
update compound_identifiers set compound_id=@new_compound_id where compound_id =@old_compound_id1;
update ce_eff_mob set ce_compound_id=@new_compound_id where ce_compound_id =@old_compound_id1;
update compound_ce_product_ion set compound_id_own=@new_compound_id where compound_id_own =@old_compound_id1;

select * from compound_identifiers where compound_id in(@new_compound_id,@old_compound_id);

delete from compound_identifiers where compound_id =@old_compound_id1;

delete from compounds where compound_id =@old_compound_id1;






select * from compounds c 
inner join compound_identifiers ci on c.compound_id = ci.compound_id where compound_name like '%benzeno%';

select * from compounds c 
inner join compounds_aspergillus ca on c.compound_id = ca.compound_id where compound_name like '%methylsterigmatocystin%';


insert into compounds (cas_id, compound_name, formula, mass, charge_type, charge_number, formula_type, formula_type_int, compound_type, compound_status, logP) 
VALUES(null, "", "", , 0, 0, "CHNOPS", 0, 0, 0, null),
(null, "", "", , 0, 0, "CHNOPS", 0, 0, 0, null);

insert into compound_identifiers (compound_id, inchi, inchi_key, smiles) 
values( , "","",""),
( , "","","");


insert into compounds_aspergillus(compound_id, biological_activity,mesh_nomenclature,iupac_classification,aspergillus_web_name) 
values(, null, null, null, null),
(, null, null, null, null);	

insert into reference(reference_text, doi, link) 
values ("","","");
values ("","","");


insert into compound_reference (compound_id, reference_id) values(188061, 409),
(188062, 409),
(188061, 1257),
(188062, 1257);

insert into organism(organism_name, organism_level, parent_id) values("",,);

insert into compound_organism (compound_id,organism_id) values(188061,152),
	(188062, 152),
(, ),
(, );



insert into organism_reference (organism_id, reference_id) values(,);
insert into organism_reference (organism_id, reference_id) values(,);

insert into compounds_pc(compound_id,pc_id) values(,);



select knapsack_id, c.compound_id, compound_name, formula, mass, 
ca.biological_activity, mesh_nomenclature, iupac_classification, aspergillus_web_name, 
c.created 
from compounds c 
inner join compounds_aspergillus ca 
on c.compound_id=ca.compound_id
left join compound_identifiers ci 
on c.compound_id = ci.compound_id 
left join compounds_knapsack ck
on c.compound_id=ck.compound_id
where ci.compound_id is null order by c.created desc;

select c.compound_id, c.compound_name, c.formula, c.mass, ci.* from compounds c 
inner join compound_identifiers ci 
on c.compound_id = ci.compound_id
where inchi_key="QKCJGDIUSFKBPI-UHFFFAOYSA-N";




insert into compound_identifiers (compound_id, inchi, inchi_key, smiles) 
values(187852, "InChI=1S/C18H16O7/c1-21-10-6-11-15(8-5-12(22-2)24-18(8)23-11)16-14(10)7-3-4-9(19)13(7)17(20)25-16/h6,8,12,18H,3-5H2,1-2H3","QKCJGDIUSFKBPI-UHFFFAOYSA-N","COC1CC2C(O1)OC3=CC(=C4C5=C(C(=O)CC5)C(=O)OC4=C23)OC"),
( , "","","");



select * from organism o group by organism_name having count(organism_name) > 1 order by organism_name;

select * from organism o 
where organism_id 
in (select organism_id from organism o group by organism_name having count(organism_name) > 1 order by organism_name desc)
group by organism_name, parent_id having count(organism_name) > 1  order by organism_name;

-- GET PARENTS MISSING
select * from organism o inner join organism parent 
on o.parent_id=parent.organism_id 
where o.organism_level = 2 and parent.organism_level != o.organism_level - 1;
-- where o.organism_level = 1 and parent.organism_level is not null


-- UNIFY ORGANISMS:

SET @old_organism_id = 4029       ;

SET @new_organism_id = 153      ;

-- SET @new_parent_id = (select parent_id from organism where organism_id = @new_organism_id);
-- update organism set parent_id = @new_parent_id where organism_id=@old_organism_id;

select * from organism where organism_name in(@old_organism_id,@new_organism_id);


select * from compound_organism where organism_id in(@old_organism_id,@new_organism_id) order by organism_id, compound_id;
select * from organism_reference where organism_id in(@old_organism_id,@new_organism_id) order by organism_id, reference_id;
select * from compound_organism where organism_id in(@old_organism_id,@new_organism_id) order by compound_id, organism_id;

update compound_organism set organism_id = @new_organism_id where organism_id = @old_organism_id 
and compound_id not in 
(select compound_id from (select compound_id from compound_organism where organism_id=@new_organism_id ) as t);
update organism_reference set organism_id = @new_organism_id where organism_id = @old_organism_id
and reference_id not in 
(select reference_id from (select reference_id from organism_reference where organism_id=@new_organism_id ) as t);

update organism set parent_id=@new_organism_id where parent_id=@old_organism_id;

-- IF EVERYTHING GOES FINE -> 
delete from organism where organism_id = @old_organism_id;



-- UNIFY REFERENCES:

SET @old_reference_id = 3960;

SET @new_reference_id = 2353 ;


select * from reference where reference_id in(@old_reference_id,@new_reference_id);

select * from compound_reference where reference_id in(@old_reference_id,@new_reference_id) order by compound_id;
select * from organism_reference where reference_id in(@old_reference_id,@new_reference_id) order by organism_id;

update compound_reference set reference_id = @new_reference_id where reference_id = @old_reference_id
and compound_id not in 
(select compound_id from (select compound_id from compound_reference where reference_id=@new_reference_id ) as t) ;

update organism_reference set reference_id = @new_reference_id where reference_id = @old_reference_id 
and organism_id not in 
(select organism_id from (select organism_id from organism_reference where reference_id=@new_reference_id ) as t) ;

-- IF EVERYTHING GOES FINE -> 
delete from reference where reference_id = @old_reference_id;



insert into organism (organism_name, organism_level, parent_id) 
values ("Ustilaginaceae",2,1),
("ZLN-62",4,134),
("ZLN-63",4,134);

insert into compound_organism (compound_id, organism_id)
values (187994,2366);

insert into organism_reference (organism_id, reference_id)
values (1918,1432), 
(1732,1432), 
(1775,1432), 
(,1432), 
(,1432), 
(,1432), 
(,1432), 
(,1432), 
(,1432), 
;


-- INSERT THE REFERENCES FOR LEVEL 4 TO THEIR PARENTS TOO
set 

update organism set organism_name

UPDATE organism SET  organism_name=replace(organism_name, 'NRRL', 'NRRL ')
where organism_name like'%NRRL%' and organism_name not like'%NRRL %';

select organism_id, organism_name, organism_level, parent_id, replace(organism_name, 'NRRL', 'NRRL ') 
from organism where organism_name like'%NRRL%' and organism_name not like'%NRRL %';


select o.organism_id, organism_name, organism_level, parent_id, ck.knapsack_id
from organism o 
left join compound_organism co
on o.organism_id = co.organism_id
left join compounds c
on c.compound_id = co.compound_id
left join compounds_knapsack ck
on c.compound_id = ck.compound_id
where o.organism_id in (53,54, 499, 1489, 1561, 1569) or o.parent_id in (53,54, 499, 1489, 1561, 1569);

-- Get references linking to aspergillus
select count(distinct reference_id) from organism_reference or1 
inner join organism o
on o.organism_id = or1.organism_id 
where organism_name like '%aspergillus%' 
or parent_id in( select organism_id from organism where organism_name like '%aspergillus%');


select co.*, c.cas_id,c.compound_name,o.organism_name, o.organism_level from compounds c 
inner join compound_organism co
on c.compound_id = co.compound_id
inner join organism o
on co.organism_id = o.organism_id
where compound_name like '%asperparaline a%' order by co.last_updated desc limit 5;

-- GET KEGG
select count(distinct c.compound_id) from compounds c 
inner join compounds_aspergillus c_asp on c.compound_id=c_asp.compound_id
inner join compounds_kegg ck on c.compound_id=ck.compound_id;

-- GET LM
select count(distinct c.compound_id) from compounds c 
inner join compounds_aspergillus c_asp on c.compound_id=c_asp.compound_id
inner join compounds_lm clm on c.compound_id=clm.compound_id ;

-- GET HMDB
select count(distinct c.compound_id) from compounds c 
inner join compounds_aspergillus c_asp on c.compound_id=c_asp.compound_id
inner join compounds_hmdb ch on c.compound_id=ch.compound_id  ;

-- GET METLIN
select count(distinct c.compound_id) from compounds c 
inner join compounds_aspergillus c_asp on c.compound_id=c_asp.compound_id
inner join compounds_agilent ca on c.compound_id=ca.compound_id ;

-- Get KNAPSACK
select count(distinct c.compound_id) from compounds c 
inner join compounds_aspergillus c_asp on c.compound_id=c_asp.compound_id
inner join compounds_knapsack ca on c.compound_id=ca.compound_id ;


-- GET THREE RELATIONSHIP
select count(distinct c.compound_id) from compounds c 
inner join compounds_aspergillus c_asp on c.compound_id=c_asp.compound_id
inner join compounds_hmdb clm on c.compound_id=clm.compound_id
inner join compounds_agilent ck on c.compound_id=ck.compound_id;

select count(distinct c.compound_id) from compounds c 
inner join compounds_aspergillus c_asp on c.compound_id=c_asp.compound_id
inner join compounds_lm clm on c.compound_id=clm.compound_id 
inner join compounds_hmdb ca on c.compound_id=ca.compound_id
inner join compounds_agilent ch on c.compound_id=ch.compound_id
inner join compounds_kegg ck on c.compound_id=ck.compound_id;


select count(distinct c.compound_id) from compounds c 
left join compounds_kegg ck on c.compound_id=ck.compound_id 
left join compounds_lm clm on c.compound_id=clm.compound_id 
left join compounds_hmdb ch on c.compound_id=ch.compound_id 
left join compounds_agilent ca on c.compound_id=ca.compound_id 
inner join compounds_aspergillus c_asp on c.compound_id=c_asp.compound_id
where ck.compound_id is null and clm.compound_id is null 
and ch.compound_id is null and ca.compound_id is null;






ASPERGILLUS = 1165
KEGG = 18698
LIPIDMAPS = 43734
HMDB = 114034
METLIN = 79345
ASPERGILLUS, KEGG = 112
ASPERGILLUS, LIPIDMAPS = 30
ASPERGILLUS, HMDB = 107
ASPERGILLUS, METLIN = 139

978 NO ESTAN EN BBDD Generales -> 187 sí están

https://venndiagram.imageonline.co/es/ 


update compounds set cas_id=null, formula = "C5H9NO5", mass=163.048072403, charge_type = 0, charge_number = 0 
	where compound_id=107277;

update compound_identifiers set SMILES = "NC(CC(O)C(O)=O)C(O)=O", inchi_key = "HBDWQSHEVMSFGY-UHFFFAOYSA-N", inchi="InChI=1S/C5H9NO5/c6-2(4(8)9)1-3(7)5(10)11/h2-3,7H,1,6H2,(H,8,9)(H,10,11)"
	where compound_id=107277;


	