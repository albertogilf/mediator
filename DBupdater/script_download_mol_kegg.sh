#!/bin/sh

generateKeggCode () {
variable=10
if [ $1 -lt $variable ]; then
    eval $2=C0000$1
else
    variable=100
    if [ $1 -lt $variable ]; then
        eval $2=C000$1
    else
        variable=1000
        if [ $1 -lt $variable ]; then
            eval $2=C00$1
        else
            variable=10000
            if [ $1 -lt $variable ]; then
                eval $2=C0$1
            else
                eval $2=C$1
            fi
        fi
    fi
fi
}

downloadMolFiles () {
for i in `seq 1 $1`
do
    fileName=''
    generateKeggCode $i fileName
#    echo $fileName
    wget --tries=50 http://www.genome.jp/dbget-bin/www_bget?-f+m+compound+$fileName -P ./20130513ceuMassUpdating/resources/kegg/molfiles
    mv ./20130513ceuMassUpdating/resources/kegg/molfiles/www_bget?-f+m+compound+$fileName ./20130513ceuMassUpdating/resources/kegg/molfiles/$fileName.mol
done
}

generateInChIFromMol()
{
/home/alberto/PHD/mediator/inchi/INCHI-1-BIN/linux/64bit/inchi-1 $1 $1.txt -key
}

generateKeggInChIs()
{
for file in ./20130513ceuMassUpdating/resources/kegg/molfiles/*mol
do
    generateInChIFromMol $file
#    echo $file
done
}

numberCompoundsKegg=21200
#downloadMolFiles $numberCompoundsKegg
generateKeggInChIs
