

INSERT INTO compounds (compound_name, formula, mass) select compound_name,formula,mass from fast_metlin_compounds order by metlin_id;

# It is necessary to create two temp tables with autoincremental key in order be able to join metlin_id with compound_id
CREATE TEMPORARY TABLE temp_compounds_metlin
(
    tmpid INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY
)
SELECT compound_id
FROM   compounds
where compound_id>204222
order by compound_id;

CREATE TEMPORARY TABLE temp_metlin 
(
    tmpid INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY
)
SELECT metlin_id
FROM   fast_metlin_compounds
order by metlin_id;

INSERT INTO compounds_metlin (compound_id,metlin_id) select tcm.compound_id,tm.metlin_id from temp_compounds_metlin tcm, temp_metlin tm where tm.tmpid=tcm.tmpid;

# Now, delete the temporary table
drop temporary table temp_compounds_metlin;
drop temporary table temp_metlin;
