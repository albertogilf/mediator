#!/bin/bash
set -x
#download files from LipidMaps and HMDB and extract the files which the project is working with.
# WARNING: LOOK IF THE FILE ON THE WEB PAGES IS THE LAST AVAILABLE
# AND CHANGE THE DATE OF THE FILE ON LIPIDMAPS IN THE FIRST VARIABLE

#Function to Retrieve the mol Files from Kegg
# Function to generate KeggCode
generateKeggCode () {
variable=10
if [ $1 -lt $variable ]; then
    eval $2=C0000$1
else
    variable=100
    if [ $1 -lt $variable ]; then
        eval $2=C000$1
    else
        variable=1000
        if [ $1 -lt $variable ]; then
            eval $2=C00$1
        else
            variable=10000
            if [ $1 -lt $variable ]; then
                eval $2=C0$1
            else
                eval $2=C$1
            fi
        fi
    fi
fi
}
#Function do DownloadMolFiles
downloadMolFiles () {
resourcesDirectory=/home/alberto/alberto/repo/mediator/DBupdater/20130513ceuMassUpdating/resources
#resourcesDirectory=C:/Users/ceu/Desktop/alberto/CMM/DBUpdater/resources
for i in `seq $1 $2`
do
    fileName=''
    generateKeggCode $i fileName
#    echo $fileName
    wget --tries=50 http://www.genome.jp/dbget-bin/www_bget?-f+m+compound+$fileName -P $resourcesDirectory/kegg/molfiles
    mv $resourcesDirectory/kegg/molfiles/www_bget?-f+m+compound+$fileName $resourcesDirectory/kegg/molfiles/$fileName.mol
done
}

generateInChIFromMol()
{
PATHINCHISOFTWARE=/home/alberto/alberto/inchi_software
#PATHINCHISOFTWARE=C:/Users/ceu/Desktop/alberto/repo_CMM/mediator/inchi
$PATHINCHISOFTWARE/INCHI-1-BIN/linux/64bit/inchi-1 $1 $1.txt -key
}

generateKeggInChIs()
{
resourcesDirectory=/home/alberto/alberto/repo/mediator/DBupdater/20130513ceuMassUpdating/resources
#resourcesDirectory=C:/Users/ceu/Desktop/alberto/CMM/DBUpdater/resources

for file in $resourcesDirectory/kegg/molfiles/*mol
do
    generateInChIFromMol $file
done
}

generateKeggInChIsByNumber()
{
resourcesDirectory=/home/alberto/alberto/repo/mediator/DBupdater/20130513ceuMassUpdating/
#resourcesDirectory=C:/Users/ceu/Desktop/alberto/CMM/DBUpdater/resources
for i in `seq $1 $2`
do
    fileName=''
    generateKeggCode $i fileName
    generateInChIFromMol $resourcesDirectory/kegg/molfiles/$fileName.mol
done
}

generateKeggSMILES()
{
python3 generateSMILESFromKegg.py
}



#Variables
dateExecution=$(date +"%y%m%d")
repoLocation=/home/alberto/alberto/repo/
#repoLocation=C:/Users/ceu/Desktop/alberto/repo_CMM
pathProjectDBUpdater=$repoLocation/mediator/DBupdater/20130513ceuMassUpdating/
resourcesDirectory=$repoLocation/mediator/DBupdater/20130513ceuMassUpdating/resources
#resourcesDirectory=C:/Users/ceu/Desktop/alberto/CMM/DBUpdater/resources
backupCodeDirectory=/home/alberto/backups/backups_code/
#backupCodeDirectory=C:/Users/ceu/Desktop/alberto/CMM/DBUpdater/backups_code
#ACCESS LIPIDMAPS PAGE TO CHANGE DATA http://www.lipidmaps.org/resources/downloads/index.html
dateLMFile=LMSD_2022-06-01
numberDBKegg=6
numberDBHMDB=18
numberDBPubChem=22
sourceSeparation=src
#CHECK HOW MANY COMPOUNDS ARE IN KEGG IN KEGG PAGE http://www.genome.jp/dbget-bin/www_bget?cpd%3AC21510
numberCompoundsKegg=22510
nameFileKeggHMDB=$sourceSeparation$numberDBKegg$sourceSeparation$numberDBHMDB.txt
nameFileKeggPubChem=$sourceSeparation$numberDBKegg$sourceSeparation$numberDBPubChem.txt
nameFileHMDBPubChem=$sourceSeparation$numberDBHMDB$sourceSeparation$numberDBPubChem.txt
#To test the script it is created a directory test which is necessary to change for the final version
#test=test
#Commands
#Backup the previous project
cp -R $pathProjectDBUpdater $backupCodeDirectory/20130513ceuMassUpdating$dateExecution
#Delete compounds from Kegg
for i in ls $resourcesDirectory/kegg/C*; do rm -v $i -f; done
#Download Files from Kegg
#Nowadays is done manually launching Java Project
#Delete mofiles and inchis from Kegg
for i in ls $resourcesDirectory/kegg/molfiles/*; do rm -v $i -f; done
#download molFiles from Kegg
downloadMolFiles 0 $numberCompoundsKegg
find $resourcesDirectory/kegg/molfiles -size  0 -print0 |xargs -0 rm --
generateKeggInChIs
#TODO ADD THE CALL TO CREATE SMILES STRUCTURE FROM MOLFILES!

generateKeggSMILES

#download files from LipidMaps
https://www.lipidmaps.org/files/?file=LMSD&ext=sdf.zip

rm -Rf $resourcesDirectory/$test/lipidMaps/LM*
wget --tries=50 https://www.lipidmaps.org/files/?file=LMSD&ext=sdf.zip -P $resourcesDirectory/$test/lipidMaps
mv 'LMSD.sdf.zip' $dateLMFile.sdf.zip
gunzip $resourcesDirectory/$test/lipidMaps/$dateLMFile.sdf.zip


#download files from HMDB
for i in ls $resourcesDirectory/$test/HMDB/*; do rm -v $i -f; done
wget --tries=50 https://hmdb.ca/system/downloads/current/hmdb_metabolites.zip -P $resourcesDirectory/$test/HMDB
unzip $resourcesDirectory/$test/HMDB/hmdb_metabolites.zip -d $resourcesDirectory/$test/HMDB/
#Download classyification nodes
#wget --tries=50 http://classyfire.wishartlab.com/tax_nodes.json -P $resourcesDirectory/$test/CLASSYFIRE/tax_nodes.json
#download files from UniChem
rm $resourcesDirectory/$test/unichem/$nameFileKeggHMDB

file_name_unichem=src$numberDBKegg
file_name_unichem+=src$numberDBHMDB

wget --tries=50 https://ftp.ebi.ac.uk/pub/databases/chembl/UniChem/data/wholeSourceMapping/src_id$numberDBKegg/$file_name_unichem.txt.gz -P $resourcesDirectory/$test/unichem
gunzip $resourcesDirectory/$test/unichem/$nameFileKeggHMDB
rm $resourcesDirectory/$test/unichem/$nameFileKeggPubChem
file_name_unichem=src$numberDBKegg
file_name_unichem+=src$numberDBHMDB

wget --tries=50 https://ftp.ebi.ac.uk/pub/databases/chembl/UniChem/data/wholeSourceMapping/src_id$numberDBKegg/$numberDBPubChem.gz -P $resourcesDirectory/$test/unichem
gunzip $resourcesDirectory/$test/unichem/$nameFileKeggPubChem
rm $resourcesDirectory/$test/unichem/$nameFileHMDBPubChem
wget --tries=50 https://ftp.ebi.ac.uk/pub/databases/chembl/UniChem/data/wholeSourceMapping/src_id$numberDBHMDB/$numberDBPubChem.gz -P $resourcesDirectory/$test/unichem
gunzip $resourcesDirectory/$test/unichem/$nameFileHMDBPubChem

#remove files from ChemIdPlus. But do not drop information from cas. This information is usually stable
#for i in ls $resourcesDirectory/$test/cas/*; do rm -v $i -f; done
#The download of the files is done in the java program

#clear the log (backup of the log is done previosly in the first line of the script)
for i in ls $pathProjectDBUpdater/log/*; do rm -v $i -f; done
#/dev/null > $pathProjectDBUpdater/log/*
#Creation of tables of the database
#create_tables_views.sql
#create_tables_msms.sql
#execute the java project to download kegg compounds and populate databases
#java -jar "$pathProjectDBUpdater/dist/20130513ceuMassUpdating.jar"

#Update Inchis Bad Starting
#update_inchis_bad_starting.sql

#Creation of tables (views) for optimizing searches and insertion of ontologies
#create_table_ms_search.sql
#insert_into_compounds_view.sql
#update_formula_type_to_int.sql
