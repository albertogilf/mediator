# -*- coding: utf-8 -*-
"""
Created on Tue May  1 10:45:56 2018

@author: Alberto Gil de la Fuente
"""

from rdkit import Chem
from rdkit.Chem import AllChem
import csv
import os
import re
import urllib.request,urllib.error, json 
import time

def generateSmilesFromMol(mol):
    return (Chem.MolToSmiles(mol,True), True)

def main():
    resources_dir = "C:/Users/ceu/Desktop/alberto/CMM/DBUpdater/resources/kegg/molfiles"
    os.chdir(resources_dir)
    for molfile in os.listdir(resources_dir):
        if(molfile.endswith(".mol")):
            stringWithMolData=open(molfile,'r').read()
            m = Chem.MolFromMolBlock(stringWithMolData)
            smiles = generateSmilesFromMol(m)
            if(smiles[1]==True):
                #Save Smiles 
                smiles = smiles[0]
                keggId = ('.').join(molfile.split('.')[:-1])
                smiles_file_name=keggId + ".smiles"
                print(smiles_file_name)
                smiles_file = open(smiles_file_name, "w")
                smiles_file.write(smiles)
                smiles_file.close()
            else:
                print(error)
        

if __name__ == "__main__":
    # execute only if run as a script
    main()
