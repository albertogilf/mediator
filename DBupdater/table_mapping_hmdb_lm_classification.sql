use compounds;

drop table compounds_classification_hmdb;

CREATE TABLE compounds_classification_hmdb (
  compound_id int NOT NULL,
  kingdom varchar(100) DEFAULT '',
  super_class varchar(100) DEFAULT '',
  class varchar(100) DEFAULT '',
  sub_class varchar(100) DEFAULT '',
  direct_parent varchar(100) DEFAULT '',
  lipid_type varchar(20) DEFAULT '',
  num_chains int default 0,
  number_carbons int,
  double_bonds int,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  FOREIGN KEY (compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  CONSTRAINT pk_hmdb_classifcation PRIMARY KEY (compound_id),
  INDEX hmdb_classification_category_index (kingdom),
  INDEX hmdb_classification_super_class_index (super_class),
  INDEX hmdb_classification_class_index (class),
  INDEX hmdb_classification_sub_class_index (sub_class),
  INDEX hmdb_classification_direct_parent_index (direct_parent),
  INDEX hmdb_classification_lipid_type_index (lipid_type),
  INDEX hmdb_classification_num_chains_index (num_chains),
  INDEX hmdb_classification_number_carbons_index (number_carbons),
  INDEX hmdb_classification_double_bonds_index (double_bonds)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;