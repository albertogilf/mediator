drop table compound_chain;
drop table chains;

CREATE TABLE chains( 
  chain_id int NOT NULL AUTO_INCREMENT PRIMARY KEY, 
  num_carbons int NOT NULL,
  double_bonds int NOT NULL,
  oxidation VARCHAR(10),
  mass double, 
  formula VARCHAR(100),
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  CONSTRAINT UC_chains_carbons_bonds_oxid UNIQUE (num_carbons,double_bonds,oxidation),
  INDEX chains_num_carbons_index (num_carbons),
  INDEX chains_double_bonds_index (double_bonds),
  INDEX chains_oxidation_index (oxidation)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table compound_chain (
  compound_id INT NOT NULL,
  chain_id int NOT NULL, 
  number int NOT NULL, 
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (compound_id) REFERENCES compounds(compound_id) on DELETE CASCADE,
  FOREIGN KEY (chain_id) REFERENCES chains(chain_id) on DELETE CASCADE,
  CONSTRAINT pk_compound_chain  PRIMARY KEY (compound_id, chain_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- INSERT DATA FROM PREVIOUS TABLE FOR THE NEW ONE
insert into chains (num_carbons, double_bonds, oxidation, mass,formula) 
select clc.number_carbons, clc.double_bonds, '', c.mass, c.formula 
from compounds c 
inner join compounds_lipids_classification clc 
on c.compound_id=clc.compound_id 
inner join compounds_in_house cih 
on c.compound_id=cih.compound_id;
insert into chains (num_carbons, double_bonds, oxidation, mass,formula) 
select clc.number_carbons, clc.double_bonds, 'O', c.mass + 13.9793, c.formula 
from compounds c 
inner join compounds_lipids_classification clc 
on c.compound_id=clc.compound_id 
inner join compounds_in_house cih 
on c.compound_id=cih.compound_id;
insert into chains (num_carbons, double_bonds, oxidation, mass,formula) 
select clc.number_carbons, clc.double_bonds, 'OH', c.mass + 15.9949, c.formula 
from compounds c 
inner join compounds_lipids_classification clc 
on c.compound_id=clc.compound_id 
inner join compounds_in_house cih 
on c.compound_id=cih.compound_id;
insert into chains (num_carbons, double_bonds, oxidation, mass,formula) 
select clc.number_carbons, clc.double_bonds, 'OH-OH', c.mass + 31.9898, c.formula 
from compounds c 
inner join compounds_lipids_classification clc 
on c.compound_id=clc.compound_id 
inner join compounds_in_house cih 
on c.compound_id=cih.compound_id;
insert into chains (num_carbons, double_bonds, oxidation, mass,formula) 
select clc.number_carbons, clc.double_bonds, 'OOH', c.mass + 31.9898, c.formula 
from compounds c 
inner join compounds_lipids_classification clc 
on c.compound_id=clc.compound_id 
inner join compounds_in_house cih 
on c.compound_id=cih.compound_id;
insert into chains (num_carbons, double_bonds, oxidation, mass,formula) 
select clc.number_carbons, clc.double_bonds, 'COH', c.mass + 27.995, c.formula 
from compounds c 
inner join compounds_lipids_classification clc 
on c.compound_id=clc.compound_id 
inner join compounds_in_house cih 
on c.compound_id=cih.compound_id;
insert into chains (num_carbons, double_bonds, oxidation, mass,formula) 
select clc.number_carbons, clc.double_bonds, 'COOH', c.mass + 43.9899, c.formula 
from compounds c 
inner join compounds_lipids_classification clc 
on c.compound_id=clc.compound_id 
inner join compounds_in_house cih 
on c.compound_id=cih.compound_id;

-- INSERT RELATIONS BETWEEN LIPIDS WITH FATTY ACIDS AND FATTY ACIDS THEMSELVES. DONE IN JAVA
update chains set oxidation='CHO' where oxidation='COH';