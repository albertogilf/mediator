UPDATE `table` SET `col_name` = TRIM(`col_name`)

update organism set organism_name = TRIM(organism_name);

update compounds set compound_name = TRIM(compound_name);

update reference set link = TRIM(link);

select * from organism where organism_name like '% ';

update reference set link = replace(link,'?redirectedFrom=fulltext','') 
where link like '%?redirectedFrom=fulltext%';

select replace(link,'?redirectedFrom=fulltext','') 
from reference
where link like '%?redirectedFrom=fulltext%';

update compounds set compound_name = REGEXP_REPLACE(compound_name,'\n(.|\n)*','') 
where compound_name like '%\n%';

update reference set reference_text = REGEXP_REPLACE(reference_text,'[0-9]+\?[0-9]+','[0-9]+-[0-9]+') 
where compound_name like '%\n%';

select reference_text from reference
where reference_text REGEXP '[0-9]+\?[0-9]+';

update reference set reference_text = replace(reference_text,'  ',' ');

Update organism 
Set organism_name = convert(binary convert(organism_name using latin1) using utf8) where organism_id=2347;