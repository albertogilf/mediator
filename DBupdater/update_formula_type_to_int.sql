
use compounds_new;

update compounds set formula_Type='ALL' where formula_type is null and mass is not null;

-- 0 CHNOPS, 1 CHNOPSD, 2 CHNOPSCL, 3 CHNOPSCLD, 4 ALL, 5 ALLD
alter table compounds add column formula_type_int int;
update compounds set formula_type_int =0 where formula_type='CHNOPS';
update compounds set formula_type_int =1 where formula_type='CHNOPSD';
update compounds set formula_type_int =2 where formula_type='CHNOPSCL';
update compounds set formula_type_int =3 where formula_type='CHNOPSCLD';
update compounds set formula_type_int =4 where formula_type='ALL';
update compounds set formula_type_int =4 where formula_type='';
update compounds set formula_type_int =4 where formula_type is null;
update compounds set formula_type_int =5 where formula_type='ALLD';

update compounds_gen set formula_Type='ALL' where formula_type is null and mass is not null;

-- 0 CHNOPS, 1 CHNOPSD, 2 CHNOPSCL, 3 CHNOPSCLD, 4 ALL, 5 ALLD
alter table compounds_gen add column formula_type_int int;
update compounds_gen set formula_type_int =0 where formula_type='CHNOPS';
update compounds_gen set formula_type_int =1 where formula_type='CHNOPSD';
update compounds_gen set formula_type_int =2 where formula_type='CHNOPSCL';
update compounds_gen set formula_type_int =3 where formula_type='CHNOPSCLD';
update compounds_gen set formula_type_int =4 where formula_type='ALL';
update compounds_gen set formula_type_int =4 where formula_type='';
update compounds_gen set formula_type_int =4 where formula_type is null;
update compounds_gen set formula_type_int =5 where formula_type='ALLD';

-- alter table compounds_gen drop column formula_type;
