select ci.inchi from 
compound_identifiers ci 
inner join compounds_lm cl 
on ci.compound_id=cl.compound_id 
where inchi not like 'InChI=%';

select ci.inchi from 
compound_identifiers ci 
inner join compounds_lm cl 
on ci.compound_id=cl.compound_id 
where inchi not like 'InChI=%' 
and inchi like '1S%';


update compound_identifiers 
set inchi = REPLACE(inchi, '1S/', 'InChI=1S/') 
where inchi not like 'InChI=%' 
and inchi like '1S%';

-- update SMILES BAD ENDING
update compound_identifiers 
set smiles = REPLACE(smiles, '\n', '') 
where smiles like '%\n' ;