ALTER TABLE compound_ce_product_ion
ADD CONSTRAINT compound_ce_product_ion_key
UNIQUE (`ion_source_voltage`,`ce_product_ion_mz`,`ce_eff_mob_id`) ;

select * from compound_ce_product_ion ccepi
inner join ce_eff_mob ceeff
on ccepi.ce_eff_mob_id=ceeff.ce_eff_mob_id
where ce_compound_id = 128249;

select ce_product_ion_id, ce_eff_mob_id, ion_source_voltage, ce_product_ion_mz, ce_product_ion_intensity, ce_transformation_type, 
ce_product_ion_name, cembio_id, ce_compound_id
from compound_ce_product_ion ccepi
inner join ce_eff_mob ceeff
on ccepi.ce_eff_mob_id=ceeff.ce_eff_mob_id
where cembio_id = 68;


SELECT ce_product_ion_mz, ion_source_voltage, ce_transformation_type, ce_eff_mob_id, count(*)
FROM compound_ce_product_ion 
GROUP BY ce_product_ion_mz, ion_source_voltage, ce_eff_mob_id
HAVING COUNT(*)>1

select ccepi.*, ce_compound_id from compound_ce_product_ion ccepi
inner join ce_eff_mob ceeff
on ccepi.ce_eff_mob_id=ceeff.ce_eff_mob_id
where cembio_id = 242;


select ccepi.*, cembio_id from compound_ce_product_ion ccepi
inner join ce_eff_mob ceeff
on ccepi.ce_eff_mob_id=ceeff.ce_eff_mob_id where ccepi.ce_eff_mob_id = 52;

select * from ce_eff_mob where cembio_id = 38;

update compound_ce_product_ion set ion_source_voltage = 200 where ce_product_ion_id in(1798,1799,1800,1801,1802);
update compound_ce_product_ion set ion_source_voltage = 100 where ce_product_ion_id in(1803,1804,1805);
update compound_ce_product_ion set ce_product_ion_name = "Fragment of L-beta-Imidazolelactic acid" 
	where ce_product_ion_id >2217 and ce_product_ion_id < 2220;

update compound_ce_product_ion set ce_product_ion_mz = 119.0346, 
ion_source_voltage = 100, ce_product_ion_intensity = 0.15 
where ce_product_ion_id = 449;

update compound_ce_product_ion set ce_product_ion_id = 2216
where ce_product_ion_id = 2230;


alter table compound_ce_product_ion auto_increment = 2222;