select c.*,ca.agilent_id,ch.hmdb_id,clm.lm_id, ck.kegg_id, ci.inchi, ci.smiles, ci.inchi_key from compounds c left join compounds_agilent ca 
on c.compound_id=ca.compound_id
left join compounds_hmdb ch 
on c.compound_id=ch.compound_id
left join compounds_lm clm
on c.compound_id=clm.compound_id
left join compounds_kegg ck 
on c.compound_id=ck.compound_id
left join compound_identifiers ci
on c.compound_id=ci.compound_id 
where c.compound_id=165871 or c.compound_id=78438 or ca.agilent_id=390948 c.cas_id="55528-53-5" or hmdb_id="HMDB0000097" c.compound_id=78438;


select c.*,ca.agilent_id,ch.hmdb_id,clm.lm_id,ci.inchi, ci.smiles from compounds c left join compounds_agilent ca 
on c.compound_id=ca.compound_id
left join compounds_hmdb ch 
on c.compound_id=ch.compound_id
left join compounds_lm clm
on c.compound_id=clm.compound_id
left join compound_identifiers ci
on c.compound_id=ci.compound_id 
where inchi="InChI=1/C5H13NO/c1-4(2)5(6)3-7/h4-5,7H,3,6H2,1-2H3/t5-/s2" or inchi="InChI=1S/C5H13NO/c1-4(2)5(6)3-7/h4-5,7H,3,6H2,1-2H3" 
or ca.agilent_id=582741 or ca.agilent_id=44703; 


insert into compound_identifiers(compound_id, inchi, inchi_key,smiles) values(165070, "InChI=1S/C7H13NO2/c8-7(6(9)10)4-2-1-3-5-7/h1-5,8H2,(H,9,10)","WOXWUZCRWJWTRT-UHFFFAOYSA-N","C1CCC(CC1)(C(=O)O)N");
insert into compounds_agilent(compound_id,agilent_id) values(112671, 24101);
insert into compounds_hmdb(compound_id,hmdb_id) values(41189, "HMDB0000036");
delete from compounds where compound_id=106301;
update compounds set cas_id='81-24-3' where compound_id=41189;

update compounds set compound_name = 'L-Isoleucine' where compound_id=27412;

update compounds set compound_name = 'Histamine' where compound_id=76386;

INCONSISTENT CAS 7622-29-9 IN DB: 165097	 IN EXCEL FILE 78438

TODO BUSCAR p+ en la lista de maricruz


------------------ UPDATE ALL TABLES FROM A COMPOUND FOR SAVING INFORMATION -------------------

select * from msms where compound_id=41189 or compound_id=106301;
select * from compounds_pathways where compound_id=41189 or compound_id=106301;
select * from compounds_pc where compound_id=41189 or compound_id=106301;
select * from compounds_reactions_kegg where compound_id=41189 or compound_id=106301;
select * from compounds_lipids_classification where compound_id=41189 or compound_id=106301;
select * from compounds_lm_classification where compound_id=41189 or compound_id=106301;
select * from compound_classyfire_classification where compound_id=41189 or compound_id=106301;
select * from compound_chain where compound_id=41189 or compound_id=106301;
select * from compound_ontology_terms where compound_id=41189 or compound_id=106301;

update msms set compound_id=41189 where compound_id=106301;
update compounds_pathways set compound_id=41189 where compound_id=106301;
update compounds_pc set compound_id=41189 where compound_id=106301;
update compounds_reactions_kegg set compound_id=41189 where compound_id=106301;
update compounds_lipids_classification set compound_id=41189 where compound_id=106301;
update compounds_lm_classification set compound_id=41189 where compound_id=106301;
update compound_classyfire_classification set compound_id=41189 where compound_id=106301;
update compound_chain set compound_id=41189 where compound_id=106301;
update compound_ontology_terms set compound_id=41189 where compound_id=106301;


alter table compound_ce_experimental_properties add polarity int default 1 not null;

select ceep.compound_id, ceep.ce_ms_id, cepi.ce_product_ion_mz,cepi.ce_transformation_type from compound_ce_experimental_properties ceep 
inner join compound_ce_product_ion cepi 
on ceep.ce_ms_id=cepi.ce_ms_id
where ceep.compound_id=48056;


select ceep.compound_id, ceep.ce_ms_id, cepi.ce_product_ion_mz,cepi.ce_transformation_type from compound_ce_experimental_properties ceep 
inner join compound_ce_product_ion cepi 
on ceep.ce_ms_id=cepi.ce_ms_id
where cepi.ce_product_ion_mz > 73.05 and cepi.ce_product_ion_mz < 73.06;

select c.formula, c.mass, ceep.compound_id, ceep.relative_MT, ceep.ce_ms_id, cepi.ce_product_ion_mz, cepi.ce_transformation_type 
from compound_ce_experimental_properties ceep 
inner join compound_ce_product_ion cepi on ceep.ce_ms_id=cepi.ce_ms_id
inner join compounds c on ceep.compound_id=c.compound_id
where ceep.ce_ms_id=61;


select * from compounds c inner join compound_ce_experimental_properties ccep 
on c.compound_id=ccep.compound_id
where ccep.ce_compound_id=193;

alter table compounds add column eff_mobility double default null;
alter table compounds add column logP double default null;

alter table compounds add INDEX compounds_eff_mobility_index (eff_mobility);
alter table compounds add INDEX compounds_logP_index (logP);

COMPOUNDS_CE = 394

PRODUCT IONS = 2149

update compounds set eff_mobility = null where compound_id = 1;

insert into compounds(cas_id,compound_name,formula,mass,charge_type,charge_number,formula_type,compound_type, compound_status, formula_type_int, eff_mobility,logP)
	VALUES ("2026-48-4","L-Valinol","C5H13NO",103.099714036,0,0,"CHNOPS",0,0,0,1563.98931387377,null);
insert into compound_identifiers(compound_id, inchi, inchi_key, smiles) values(180840 ,"InChI=1S/C5H13NO/c1-4(2)5(6)3-7/h4-5,7H,3,6H2,1-2H3/t5-/m1/s1","NWYYWIJOWOLJNR-RXMQYKEDSA-N","CC(C)[C@@H](CO)N");

