Creacion claves ssh para conectar con bitbucket

git keyfinger:
The key fingerprint is:
SHA256:8BacB+2DPIAn7y6TLmJQvf31NmGZNGF0Q56XtBNhrwE alberto@PC-ALBERTO

GIT for dummies:

https://try.github.io/levels/1/challenges/25

# for initialize a new repository
git init 

# to know the status of our branch
git status 

# to change the branch
git checkout <BRANCHNAME>

# to delete the branch
git branch -d 

# to delete the branch discarding the changes on it
git branch -D 

# to add files (It is possible to use patterns '*txt*'
git add AAA.txt 

# to delete files (It is possible to use patterns '*txt*'
git rm AAA.txt 

# to commit the changes
git commit -m "COMMENTS ON THE COMMIT" 

# to view the log of GIT
git log

# to push the commands on the origin repo (no branchs)
git push -u origin master

# to download the local code from repository
git pull

# to see the changes on the files
git diff

# to unstage files
git reset <FILENAME>

# to checkout one file to the last commit
git checkout <FILENAME>

