/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DBManager;

import facades.MSFacade;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.tomcat.jdbc.pool.DataSource;

/**
 *
 * @author: San Pablo-CEU, Maria Postigo Fliquete
 * @version: 4.0, 31/12/2016
 */
public class DBManager {

    private Context ctx;
    private Connection conn;

    public DBManager() {
        try {
            ctx = new InitialContext();
            //ds = (DataSource) ctx.lookup(dsName);
        } catch (NamingException ex) {
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Connection connect() {
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            //conn = DriverManager.getConnection("jdbc:mysql://localhost/DATABASE_NAME", "password", "password");

            DataSource ds;
            ds = (DataSource) ctx.lookup("java:comp/env/jdbc/linkToNewDataModel");
            this.conn = ds.getConnection();

        } catch (NamingException | SQLException ex) {
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return this.conn;
    }
 
    
    public static void forName(String className)throws ClassNotFoundException{};
    
    public Connection testConnect() {
        
        try {
        List<String> myData = getDataForConnection();
        // bd, user, clave
        
        String dbUrl = "jdbc:mysql://localhost/";
        
        String db = myData.get(0);
        String user = myData.get(1);
        String password = myData.get(2);
        
        String finaldbUrl = dbUrl + db+"?user="+user+"&password="+password+"&useSSL=false";
        
        this.conn = DriverManager.getConnection(finaldbUrl);
        
        } catch (SQLException ex){//| ClassNotFoundException ex) {
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return this.conn;
    }
    
    private List<String> getDataForConnection() {
        List<String> dataToConnect = new LinkedList<>();
        try {

            File file = new File("/home/alberto/Desktop/test_connection.txt");

            FileReader fr;
            BufferedReader bf;
            dataToConnect = new LinkedList<>();
            fr = new FileReader(file);
            bf = new BufferedReader(fr);
            String line;
            while ((line = bf.readLine()) != null) {
                dataToConnect.add(line);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MSFacade.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MSFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dataToConnect;
    }
        
    
    public void disconnect() {
        try {
            this.conn.close();
        } catch (SQLException e) {
            System.out.println("exception: " + e);
        }   
    }

            
}
