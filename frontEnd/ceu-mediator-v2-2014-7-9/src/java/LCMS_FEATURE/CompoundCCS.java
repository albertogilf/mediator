/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LCMS_FEATURE;

import pathway.Pathway;
import compound.Classyfire_Classification;
import compound.LM_Classification;
import compound.Lipids_Classification;
import compound.Structure;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import static utilities.Constants.MIN_RETENTION_TIME_SCORE;
import utilities.RulesProcessing;
import static utilities.Utilities.calculatePercentageError;

/**
 * CompoundLCMS. Represents a chemical compound. This class is the child of
 * CMMCompound. Therefore, it contains the compound's data from the database and
 * the user's input data about the experiment (ex. rt)
 *
 * @author Maria Postigo. San Pablo-CEU
 * @version: 4.0, 24/04/2018
 */
public class CompoundCCS extends CompoundLCMS {

    private final Double expCCS;
    private final Double CCS;
    // Key of EM_CCS for scores
    private final Integer errorCCS;


    /**
     *
     * @param EM
     * @param RT
     * @param CS
     * @param expCCS CCS experimentally measured 
     * @param CCS
     * @param ionizationMode 0 neutral, 1 positive, 2 negative
     * @param adduct
     * @param compound_id
     * @param compound_mass
     * @param formula
     * @param compound_name
     * @param cas_id
     * @param formula_type
     * @param compound_type
     * @param compound_status
     * @param charge_type
     * @param charge_number
     * @param lm_id
     * @param kegg_id
     * @param hmdb_id
     * @param pc_id
     * @param metlin_id
     * @param in_house_id
     * @param chebi_id
     * @param MINE_id
     * @param knapsack_id
     * @param npatlas_id
     * @param aspergillus_id
     * @param mesh_nomenclature
     * @param iupac_classification
     * @param fahfa_id
     * @param oh_position
     * @param structure
     * @param aspergillus_web_name
     * @param lm_classification
     * @param classyfire_classification
     * @param lipids_classification
     * @param pathways
     */
    public CompoundCCS(Double EM, Double RT, Map<Double, Double> CS, Double expCCS, String adduct,
            int ionizationMode, int compound_id, double compound_mass,
            String formula, String compound_name, String cas_id, int formula_type, int compound_type, int compound_status,
            int charge_type, int charge_number, double CCS, String lm_id, 
            String kegg_id, String hmdb_id, String metlin_id, String in_house_id, Integer pc_id, Integer chebi_id, Integer MINE_id,
            String knapsack_id, Integer npatlas_id,
            String aspergillus_id, String mesh_nomenclature, String iupac_classification, String aspergillus_web_name,
            Integer fahfa_id, Integer oh_position,
            Structure structure,
            LM_Classification lm_classification, List<Classyfire_Classification> classyfire_classification,
            Lipids_Classification lipids_classification, List<Pathway> pathways) {
        super(EM, RT, CS, adduct, 1, ionizationMode, true, compound_id, compound_mass, formula, compound_name, cas_id, formula_type, compound_type, compound_status,
                charge_type, charge_number,
                lm_id, kegg_id, hmdb_id, metlin_id, in_house_id, pc_id, chebi_id, MINE_id, knapsack_id, npatlas_id,
                aspergillus_id, mesh_nomenclature, iupac_classification, aspergillus_web_name,
                fahfa_id, oh_position,
                structure, lm_classification, classyfire_classification, lipids_classification, pathways);
             
        this.expCCS = expCCS; // experimental
        this.CCS = CCS; // BBDD
        
        this.myKey = super.myKey + "_" + this.expCCS.toString();
        
        this.errorCCS = calculatePercentageError(expCCS, CCS);
    }
    
    public CompoundCCS(Double expCCS, double CCS, CompoundLCMS compound) {
        
        super (compound.getEM(), compound.getRT(), compound.getCS(), compound.getAdduct(), compound.getEMMode(), compound.getIonization_mode(),
                compound.getIsSignificative(), compound.getCompound_id(), compound.getMass(), compound.getFormula(),
                compound.getCompound_name(), compound.getCas_id(), compound.getFormula_type(), compound.getCompound_type(), compound.getCompound_status(),
                compound.getCharge_type(), compound.getCharge_number(),
                compound.getLm_id(), compound.getKegg_id(), 
                compound.getHmdb_id(), compound.getMetlin_id(), 
                compound.getIn_house_id(), 
                Integer.parseInt(compound.getPc_id()),
                Integer.parseInt(compound.getChebi_id()), 
                Integer.parseInt(compound.getMINE_id()), 
                compound.getKnapsack_id(), 
                Integer.parseInt(compound.getNpatlas_id()),
                compound.getAspergillus_id(), compound.getMesh_nomenclature(), compound.getIupac_classification(), compound.getAspergillus_web_name(),
                compound.getFahfa_id(), compound.getOh_position(),
                compound.getStructure(), compound.getLm_classification(), compound.getClasssyfire_classification(), compound.getLipids_classification(), 
                compound.getPathways());
        
        this.expCCS = expCCS; // experimental
        this.CCS = CCS; // BBDD
        
        this.myKey = super.myKey + "_" + this.expCCS.toString();
        
        this.errorCCS = calculatePercentageError(expCCS, CCS);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.getEM());
        hash = 59 * hash + Objects.hashCode(this.CCS);
        hash = 59 * hash + Objects.hashCode(super.getAdduct());
        hash = 59 * hash + Objects.hashCode(this.expCCS);
        hash = 59 * hash + Objects.hashCode(this.getCompound_id());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!Objects.equals(getClass(), obj.getClass())) {
            return false;
        }
        final CompoundCCS other = (CompoundCCS) obj;
        if (!Objects.equals(super.getAdduct(), other.getAdduct())) {
            return false;
        }
        if (!Objects.equals(this.getEM(), other.getEM())) {
            return false;
        }
        if (!Objects.equals(this.CCS, other.CCS)) {
            return false;
        }
        if (!Objects.equals(this.expCCS, other.expCCS)) {
            return false;
        }
        if (!Objects.equals(super.getCompound_id(), other.getCompound_id())) {
            return false;
        }
        return true;
    }

    public Double getCCS() {
        return CCS;
    }

    public Double getExpCCS() {
        return expCCS;
    }

    public Integer getErrorCCS() {
        return errorCCS;
    }
    
    
   
    @Override
    public void calculateFinalScore(int maxNumberOfRTRulesApplied) {
        super.calculateFinalScore(maxNumberOfRTRulesApplied);
    }
    
    
       
    @Override
    public Float getRTscore() {
        return super.RTscore * 2;
    }

    /*
    @Override
    public void setFinalScore(Float finalScore) {
        super.setFinalScore(finalScore);
    }*/
    
    @Override
    public Float getFinalScore() {
        return super.finalScore * 2;
    }

   
    
    @Override
        public String getColorFinalScore() {
        return super.colorFinalScore;
    }

    
    @Override
    public String getColorRTscore() {
        return super.colorRTscore;
    }
    
    
    
    @Override
    public String toString() {
        return "            Name: " + super.getCompound_name() + " CCS: " + this.CCS + " Mass: " + super.getMass();
    }

}
