/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LCMS_FEATURE;


import static CEMS.CEMSFragment.roundToFourDecimals;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * FeatureCCS. A feature is a tuple of mz, retention time, CCS value and 
 * composite spectra. A FeatureCCS can represent several compounds.
 * 
 * @author alberto
 */
public class FeatureCCS extends Feature {
    
    private final Double expCCS;//Esperimental CCS.
    //private final Collection<CompoundCCS> compoundsCCS;
    
    //CONSTRUCTORS:
    
    // For IM-MS: when attibutes in parent class are not being used:
    /**
     *
     * @param EM
     * @param expCCS
     * @param ionizationMode 0 neutral, 1 positive, 2 negative
     */
    public FeatureCCS(Double EM, Double expCCS, int ionizationMode) {
        
        //Attributes used by the parent class, for LC-MS:
        super(EM, 1, ionizationMode);
        //EXPERIMENTAL MASS MODE (EMMode) IS ALWAYS 1 --> m/z. 
        
        //Attributes used by IM-MS:
        this.expCCS = expCCS;
        
    }
    
    /**
     *
     * @param EM
     * @param isSignificativeFeature
     * @param expCCS
     * @param ionizationMode 0 neutral, 1 positive, 2 negative
     */
    public FeatureCCS(Double EM, boolean isSignificativeFeature, Double expCCS, int ionizationMode) {
        
        //Attributes used by the parent class, for LC-MS:
        super(EM, isSignificativeFeature, 1, ionizationMode);
        
        //Attributes used by IM-MS:
        this.expCCS = expCCS;
        
    }
   
    
        
    // For LC-IM-MS: when attRibutes in parent class (Rt, CS, isAdductAutoDEtected, ...)
    // are being used:
    /**
     *
     * @param EM
     * @param expCCS
     * @param ionizationMode 0 neutral, 1 positive, 2 negative
     * @param isSignificativeFeature
     */
     public FeatureCCS(Double EM, boolean isSignificativeFeature,
             int ionizationMode, Double expCCS) {
            
        //Attributes for LC-MS:
        super(EM, isSignificativeFeature, 1, ionizationMode); 
                
        //Attributes for IMS:
        this.expCCS = expCCS;
    }
    
     /**
     *
     * @param EM
     * @param RT
     * @param expCCS
     * @param ionizationMode 0 neutral, 1 positive, 2 negative
     * @param isSignificativeFeature
     */
    public FeatureCCS(Double EM, Double RT, boolean isSignificativeFeature,
            int ionizationMode, Double expCCS) {
            
        //Attributes for LC-MS:
        super(EM, RT, isSignificativeFeature, 1, ionizationMode); 
                
        //Attributes for IMS:
        this.expCCS = expCCS;
    }
    
     /**
     *
     * @param EM
     * @param CS
     * @param expCCS
     * @param adductAutoDetected
     * @param isAdductAutoDetected
     * @param ionizationMode 0 neutral, 1 positive, 2 negative
     * @param isSignificativeFeature
     */
    public FeatureCCS(Double EM, Map<Double, Double> CS, boolean isAdductAutoDetected,
            String adductAutoDetected, boolean isSignificativeFeature, int ionizationMode,
            Double expCCS) {
            
        //Attributes for LC-MS:
        super(EM, CS, isAdductAutoDetected, adductAutoDetected, isSignificativeFeature,
             1, ionizationMode);
        //Attributes for IMS:
        
        this.expCCS = expCCS;
    }

 /**
     *
     * @param EM
     * @param RT
     * @param CS
     * @param expCCS
     * @param adductAutoDetected
     * @param isAdductAutoDetected
     * @param ionizationMode 0 neutral, 1 positive, 2 negative
     * @param isSignificativeFeature
     */
    public FeatureCCS(Double EM, Double RT, Map<Double, Double> CS, boolean isAdductAutoDetected,
            String adductAutoDetected, boolean isSignificativeFeature, int ionizationMode,
            Double expCCS) {
            
        //Attributes for LC-MS:
        super(EM, RT, CS, isAdductAutoDetected, adductAutoDetected, isSignificativeFeature,
              1, ionizationMode);
        
        //Attributes for IMS:
        this.expCCS = expCCS;
    }     
    
    
    /**
     *
     * @param EM
     * @param RT
     * @param CS
     * @param expCCS
     * @param ionizationMode 0 neutral, 1 positive, 2 negative
     * @param adduct
     * @param adductAutoDetected
     * @param isSignificativeFeature
     * @param isAdductAutoDetected
     * @param annotations
     */
    public FeatureCCS(Double EM, Double RT, Map<Double, Double> CS, 
            boolean isAdductAutoDetected, String adductAutoDetected,
            boolean isSignificativeFeature, List<CompoundsLCMSGroupByAdduct> annotations,
            int ionizationMode, Double expCCS, String adduct) {
        
        super(EM, RT, CS, isAdductAutoDetected, adductAutoDetected, isSignificativeFeature,
            annotations, 1, ionizationMode);
                

        this.expCCS = expCCS;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.getEM());
        hash = 59 * hash + Objects.hashCode(this.getRT()); 
        hash = 59 * hash + Objects.hashCode(this.expCCS);       
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!Objects.equals(getClass(), obj.getClass())) {
            return false;
        }
        final FeatureCCS other = (FeatureCCS) obj;
        if (!Objects.equals(this.getEM(), other.getEM())) {
            return false;
        }
        if (!Objects.equals(this.getRT(), other.getRT())) {
            return false;
        }
        if (!Objects.equals(this.expCCS, other.expCCS)) {
            return false;
        }
        return true;
    }

    public Double getExpCCS() {
        return expCCS;
    }
    
    
    @Override
    public String toString() {
        return " INPUT MASS: " + this.getEM() + " RT"+ this.getRT() +" CCS: " + this.expCCS;
    }

    
    /**
     * Add the annotation annotation corresponding to the feature
     *
     * @param annotation
     */
    /**
    public void addAnnotation(CompoundCCS annotation) {
        this.compoundsCCS.add(annotation);
    }
    **/

    /**
     * Get the empty title when there are no compounds for a group of
     * annotations
     *
     * @return
     */
    /**
    public String getEmptyAnnotationMessage() {

        String titleMessage = "No compounds found for m/z: " + massIntroduced;
        
        if (super.RT != null && super.RT > 0d) {
            titleMessage = titleMessage + ", RT: " + super.RT;
        }
        
        if (this.expCCS != null && this.expCCS > 0d) {
            titleMessage = titleMessage + " and CCS: " + this.expCCS;
        }
        return titleMessage;
    }
    **/

    
    /**
     * Method used in .xhtml to generate the title of the output annotations.
     * @return
    **/
    @Override
    public String getTitleMessage() {
        String titleMessage;
        titleMessage = "Compounds found for m/z: " + roundToFourDecimals(this.EM) + "";
        
        if (super.RT != null && super.RT > 0d) {
            titleMessage = titleMessage + ", RT: " + roundToFourDecimals(super.RT);
        }
        
        if (this.expCCS != null && this.expCCS > 0d) {
            titleMessage = titleMessage + " and CCS: " + roundToFourDecimals(this.expCCS);
        }

        return titleMessage;
    }
    
    
    @Override
    public Integer getNumAnnotationsGroupedByAdduct(){
       return super.annotationsGroupedByAdduct == null ? 0 : annotationsGroupedByAdduct.size();
    }
    
}

