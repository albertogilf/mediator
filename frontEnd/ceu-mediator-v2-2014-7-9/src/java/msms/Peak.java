/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msms;

import java.util.Objects;

/**
 * Peak Object. 
 *
 * @author Maria Postigo. Alberto Gil de la Fuente. San Pablo-CEU
 * @version: 4.0, 15/04/2018
 */
public class Peak implements Comparable<Peak> {

    // mz and intensity were refactored to facilitate the communication with rickshaw, 
    // the js library to paint the spectra
    private final Double x; // mz 
    private final Double y; // intensity 
    private transient final Integer msms_id;

    public Peak(Double mz, Double intensity) {
        this.y = intensity;
        this.x = mz;
        this.msms_id = 0;
    }

    public Peak(Double mz, Double intensity, Integer msmsId) {
        this.y = intensity;
        this.x = mz;
        this.msms_id = msmsId;
    }

    public Integer getMsms_id() {
        return msms_id;
    }



    public Double getY() {
        return y;
    }

    public Double getX() {
        return x;
    }

    @Override
    public String toString() {

        return " INTENSITY: " + this.y + " MASS CHARGE: " + this.x;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + (int) (Double.doubleToLongBits(this.y) ^ (Double.doubleToLongBits(this.y) >>> 32));
        hash = 59 * hash + (int) (Double.doubleToLongBits(this.x) ^ (Double.doubleToLongBits(this.x) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Peak other = (Peak) obj;
        if (!Objects.equals(this.y, other.y)) {
            return false;
        }
        if (!Objects.equals(this.x, other.x)) {
            return false;
        }
        return true;
    }

    //Rewrite compareTo in order to compare the peaks acording to their x
    //in ascending order to pair the peaks properly
    @Override
    public int compareTo(Peak peak) {
        int result;
        if (this.x < peak.getX()) {
            result = -1;
        } else if (this.x > peak.getX()) {
            result = 1;
        } else {
            if (this.y < peak.getY()) {
                result = -1;
            } else if (this.y > peak.getY()) {
                result = 1;
            } else {
                return 0;
            }
        }
        return result;
    }

    /**
     * This method checks if the peaks are similar within the given tolerance in Da
     * 
     * @param libraryPeak is the peak from the candidate obtained through the database
     * @param mzTolerance is the tolerance provide by the user
     * @return true or false according to if the peak is withing the window tolerance
     */
    public boolean peakMatch(Peak libraryPeak, double mzTolerance) {
        
        //return  this.x<= (libraryPeak.getMz() + mzTolerance) &&  this.x>= (libraryPeak.getMz() - mzTolerance);
        return  libraryPeak.getX()<= (this.x + mzTolerance) &&  libraryPeak.getX()>= (this.x - mzTolerance);
    }

    public void setId(int aInt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
