package presentation;

import LCMS_FEATURE.CompoundCCS;
import LCMS_FEATURE.CompoundsLCMSGroupByAdduct;
import LCMS_FEATURE.FeatureCCS;
import controllers.InterfaceValidators;
import exporters.CompoundExcelExporter;
import facades.MSFacade;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import ruleengine.ConfigFilter;
import ruleengine.RuleProcessor;
import utilities.Cadena;
import utilities.AdductsLists;
import static utilities.Constants.*;
import utilities.DataFromInterfacesUtilities;
import static utilities.DataFromInterfacesUtilities.MAPDATABASES;
import utilities.Constants;
import utilities.RulesProcessing;

/**
 * Controller (Bean) of the application
 *
 * @author alberto Gil de la Fuente. San Pablo-CEU
 * @version: 3.1, 17/02/2016
 */
@ManagedBean(name = "theoreticalCompoundsCCSController")
@SessionScoped
public class TheoreticalCompoundsCCSController implements Serializable, Controller {

    private static final long serialVersionUID = 1L;

    private String queryInputMasses;
    private String queryInputCCS;
    //Added to support RT data from FrontEnd:
    private String queryInputRetentionTimes;

    private String inputTolerance;
    private String inputModeTolerance;
    private String inputCCSTolerance;
    private String chemAlphabet;
    private boolean includeDeuterium;
    private String modifier;
    private List<SelectItem> modifierCandidates;
    private List<String> databases;
    private final List<SelectItem> DBcandidates;
    private String metabolitesType;
    private final List<SelectItem> metabolitesTypecandidates;

    private String massesMode;
    private int ionMode;
    private List<SelectItem> ionizationModeCandidates;
    private List<String> adducts;
    private List<SelectItem> adductsCandidates;
    // to Improve efficiency. They are assigned to adductsCandidates
    private final List<SelectItem> positiveCandidates;
    private final List<SelectItem> negativeCandidates;

    // Declared as a variable because JSF needs that even Framework marks as not used.
    private int numAdducts;

    private List<Double> queryMasses;
    private List<Double> queryCCSs;
    //Added to support RT data from FrontEnd:
    private List<Double> queryRetentionTimes;

    private List<FeatureCCS> items;

    
    private facades.MSFacade msFacade;

    public TheoreticalCompoundsCCSController() {

        this.msFacade = new MSFacade();
        this.inputTolerance = TOLERANCE_DEFAULT_VALUE;
        this.inputModeTolerance = TOLERANCE_MODE_DEFAULT_VALUE;
        this.inputCCSTolerance = TOLERANCE_CCS_SEARCH_DEFAULT_VALUE;
        //String version = FacesContext.class.getPackage().getImplementationVersion();
        //System.out.println("\n\n  VERSION DE JSF: " + version + "\n\n");
        this.items = null;
        this.DBcandidates = new LinkedList<>();
        this.DBcandidates.add(new SelectItem("AllWM", "All"));
        MAPDATABASES.entrySet().forEach((e) -> {
            this.DBcandidates.add(new SelectItem(e.getKey(), (String) e.getKey()));
        });
        this.DBcandidates.remove(this.DBcandidates.size() - 1);
        this.databases = new LinkedList<>();
        this.databases.add("AllWM");

        // MODIFIERS
        this.modifierCandidates = new LinkedList<>();
        DataFromInterfacesUtilities.MODIFIERS.entrySet().forEach((e) -> {
            this.modifierCandidates.add(new SelectItem(e.getKey(), (String) e.getKey()));
        });
        this.modifier = "None";

        this.metabolitesTypecandidates = new LinkedList<>();
        DataFromInterfacesUtilities.METABOLITESTYPES.entrySet().forEach((e) -> {
            this.metabolitesTypecandidates.add(new SelectItem(e.getKey(), (String) e.getKey()));
        });
        this.metabolitesType = "All except peptides";

        this.positiveCandidates = new LinkedList<>();
        this.positiveCandidates.add(new SelectItem("M+H", "M+H"));

        this.negativeCandidates = new LinkedList<>();
        this.negativeCandidates.add(new SelectItem("M-H", "M-H"));


        this.massesMode = "mz";
        this.ionizationModeCandidates = AdductsLists.LISTIONIZEDMODES;
        this.ionMode = 1;
        this.adductsCandidates = positiveCandidates;
        this.adducts = new LinkedList<>();
        this.adducts.add("M+H");

        this.queryInputMasses = "";
        this.queryInputCCS = "";
        this.chemAlphabet = "CHNOPS";
        this.includeDeuterium = false;
        //Added to support RT data from FrontEnd:
        this.queryInputRetentionTimes = "";
        
        
    }
    
    
    /**
     * Calculate the scores of the annotations based on the rules developed on
     * the compoundLCMS
     */
    public void calculateScoresAnnotations() {
        
        ConfigFilter configFilter = new ConfigFilter();
        configFilter.setModifier(this.modifier);
        configFilter.setIonMode(this.ionMode);
        configFilter.setAllCompounds(false);
        
        RuleProcessor.processRTRulesFeatures(this.items,configFilter);
        int max_numberOfRTScoresApplied;
        max_numberOfRTScoresApplied = RulesProcessing.calculateMaxNumberOfRTScoresFeatures(this.items);
        
        
        for (FeatureCCS feature : this.items) {
            for (CompoundsLCMSGroupByAdduct compoundsLCMSGroupByAdduct : feature.getAnnotationsGroupedByAdduct()) {
                for (CompoundCCS compoundCCS : (List<CompoundCCS>) compoundsLCMSGroupByAdduct.getCompounds()) {
                    compoundCCS.calculateFinalScore(max_numberOfRTScoresApplied);
                    // Create tags for colours
                    compoundCCS.calculateRTScore();
                    //System.out.println(compoundCCS.getFinalScore());
                    compoundCCS.createColorIonizationScore();
                    //System.out.println(compoundCCS.getColorIonizationScore());
                    compoundCCS.createColorAdductRelationScore();
                    //
                    compoundCCS.createColorRetentionTimeScore();
                    compoundCCS.createColorFinalScore();
                    
                }
            }
        }
    }

    /**
     * This method is used to load a list of LC-IM-MS data declared in the class
     * Constants
     */
    public void setDemoLCIMMSdata() {
        this.setQueryInputMasses(DEMOMZSCCSs);
        this.setQueryInputCCS(DEMOCCSs);
        //Added to support RT data from FrontEnd:
        this.setQueryInputRetentionTimes(NEWDEMORETENTIONTIME_LCIMMS);
        
        this.chemAlphabet = "CHNOPS";
        this.includeDeuterium = false;
        this.setMassesMode("mz");
    }
    
    /**
     * This method is used to load a list of IM-MS data declared in the class
     * Constants
     */
    public void setDemoIMMSdata() {
        this.setQueryInputMasses(DEMOMZSCCSs);
        this.setQueryInputCCS(DEMOCCSs);        
        this.chemAlphabet = "CHNOPS";
        this.includeDeuterium = false;
        this.setMassesMode("mz");
    }

    /**
     * Method to clean the input formulary where the queryMasses are written
     */
    public void clearLCIMMSForm() {

        this.queryInputMasses = "";
        this.queryInputCCS = "";
        //Added to support RT data from FrontEnd:
        this.queryInputRetentionTimes = "";
        
        this.inputTolerance = TOLERANCE_DEFAULT_VALUE;
        this.inputModeTolerance = TOLERANCE_MODE_DEFAULT_VALUE;
        this.inputCCSTolerance = TOLERANCE_CCS_SEARCH_DEFAULT_VALUE;

        this.chemAlphabet = "CHNOPS";
        this.includeDeuterium = false;

        this.databases.clear();
        this.databases.add("AllWM");
        resetItems();
    }
    
    /**
     * Method to clean the input formulary where the queryMasses are written
     */
    public void clearIMMSForm() {

        this.queryInputMasses = "";
        this.queryInputCCS = "";
        
        this.inputTolerance = TOLERANCE_DEFAULT_VALUE;
        this.inputModeTolerance = TOLERANCE_MODE_DEFAULT_VALUE;
        this.inputCCSTolerance = TOLERANCE_CCS_SEARCH_DEFAULT_VALUE;

        this.chemAlphabet = "CHNOPS";
        this.includeDeuterium = false;

        this.databases.clear();
        this.databases.add("AllWM");
        resetItems();
    }

    private void resetItems() {
        this.items = null;
    }

    /**
     * Method that permits to create a excel from the current results. Flag
     * indicates if the excel generated contains RT field or not. 1 yes 0 no
     *
     * @param flag
     */
    public void exportToExcel(int flag) {
        // Only export to Excel no significative compounds
        if (this.items != null && !this.items.isEmpty()) {

            CompoundExcelExporter compoundExcelExporter = new CompoundExcelExporter(flag);
            compoundExcelExporter.generateWholeExcelCompound(this.items, flag);
        }
    }

    /**
     * Submit compounds in advanced mode for LC-IM-MS.
     */
    public void submitCompoundsLCIMMSCCSPred() {
        this.items = null;
        List<Double> massesAux; // auxiliar List for input Masses
        int numInputMasses;
        
        //Method returns an ArrayList because it is acceded by index
        this.queryMasses = Cadena.extractDoubles(this.queryInputMasses);
        numInputMasses = this.queryMasses.size();
        this.queryCCSs = Cadena.getListOfDoubles(this.queryInputCCS, numInputMasses);
        this.queryRetentionTimes = Cadena.getListOfDoubles(this.queryInputRetentionTimes, numInputMasses);

        // System.out.println("INPUT: " + queryInputMasses + " \n ARRAY: " + massesAux);
        //System.out.println("INPUT RETENTION TIME: " + this.queryInputRetentionTimes);
        //System.out.println("INPUT RETENTION TIME after submit: " + this.queryRetentionTimes);
        // System.out.println("INPUT COMPOSITE SPECTRUM: " + this.queryInputCompositeSpectra);
        //System.out.println("SIMPLE SEARCH");
        //System.out.println("Sign Compounds: "+ isSignifativeCompoundAux.toString() + " size: " + isSignifativeCompoundAux.size());
        // Convert RTs to seconds
        // First it is needed to transform the RT into the model unit
        int chemAlphabetInt = DataFromInterfacesUtilities.getChemAlphabetAsInt(this.getChemAlphabet());
        Integer mztoleranceModeAsInt = DataFromInterfacesUtilities.toleranceTypeToInteger(getInputModeTolerance());

        if (this.items == null) {
            
            this.items = this.msFacade.findCompoundsCCSPred( //This method returns list of FeatureCCS
                    this.queryMasses,
                    this.queryCCSs,
                    this.queryRetentionTimes,
                    mztoleranceModeAsInt,
                    Double.parseDouble(this.inputTolerance),
                    Integer.parseInt(this.inputCCSTolerance),
                    this.adducts,
                    chemAlphabetInt,
                    this.ionMode,
                    this.databases,
                    this.metabolitesType
            );
            calculateScoresAnnotations();
        }
        
    }
        
        
    /**
     * Submit compounds in advanced mode for IM-MS.
     */
    public void submitCompoundsIMMSCCSPred() {
        this.items = null;
        List<Double> massesAux; // auxiliar List for input Masses
        int numInputMasses;
        
        //Method returns an ArrayList because it is acceded by index
        this.queryMasses = Cadena.extractDoubles(this.queryInputMasses);
        numInputMasses = this.queryMasses.size();
        this.queryCCSs = Cadena.getListOfDoubles(this.queryInputCCS, numInputMasses);
        
        /*FIRST ATTEMPT:
        // As there is no Retention Time in IM-MS, the queryRetentionTimes will
        // be filled with a list the size of queryMasses.
        List<Double> emptyRetentionTimes;
        this.queryRetentionTimes = new ArrayList<>(Collections.nCopies(numInputMasses, 0d));
        setQueryRetentionTimes(emptyRetentionTimes);
        */
        
        //SECOND ATTEMPT:
        //this.queryRetentionTimes = null;
        this.queryRetentionTimes = new ArrayList<>(Collections.nCopies(numInputMasses, null));
        
        // System.out.println("INPUT: " + queryInputMasses + " \n ARRAY: " + massesAux);
        //System.out.println("INPUT RETENTION TIME: " + this.queryInputRetentionTimes);
        //System.out.println("INPUT RETENTION TIME after submit: " + this.queryRetentionTimes);
        // System.out.println("INPUT COMPOSITE SPECTRUM: " + this.queryInputCompositeSpectra);
        //System.out.println("SIMPLE SEARCH");
        //System.out.println("Sign Compounds: "+ isSignifativeCompoundAux.toString() + " size: " + isSignifativeCompoundAux.size());
        // Convert RTs to seconds
        // First it is needed to transform the RT into the model unit
        int chemAlphabetInt = DataFromInterfacesUtilities.getChemAlphabetAsInt(this.getChemAlphabet());
        Integer mztoleranceModeAsInt = DataFromInterfacesUtilities.toleranceTypeToInteger(getInputModeTolerance());

        if (this.items == null) {
            //This method returns list of FeatureCCS
            this.items = this.msFacade.findCompoundsCCSPred( 
                    this.queryMasses,
                    this.queryCCSs,
                    this.queryRetentionTimes,
                    mztoleranceModeAsInt,
                    Double.parseDouble(this.inputTolerance),
                    Integer.parseInt(this.inputCCSTolerance),
                    this.adducts,
                    chemAlphabetInt,
                    this.ionMode,
                    this.databases,
                    this.metabolitesType
            );
           
        }    
        
    }



    /**
     * @return the queryInputMasses
     */
    public String getQueryInputMasses() {
        return this.queryInputMasses;
    }

    /**
     * Catches the input text in the formulary on the web of the queryMasses and
     * obtains the list of them in order.
     *
     * @param queryInputMasses the queryInputMasses to set
     */
    public void setQueryInputMasses(String queryInputMasses) {
        this.queryInputMasses = queryInputMasses;
    }

    public String getQueryInputCCS() {
        return this.queryInputCCS;
    }

    public void setQueryInputCCS(String queryInputCCS) {
        this.queryInputCCS = queryInputCCS;
    }
    
    //Added to support RT data from FrontEnd:
    public String getQueryInputRetentionTimes() {
        return queryInputRetentionTimes;
    }

    public void setQueryInputRetentionTimes(String queryInputRetentionTimes) {
        this.queryInputRetentionTimes = queryInputRetentionTimes;
    }

    /**
     * @return the inputTolerance
     */
    public String getInputTolerance() {
        return this.inputTolerance;
    }

    /**
     * @param inputTolerance the inputTolerance to set
     */
    public void setInputTolerance(String inputTolerance) {
        this.inputTolerance = inputTolerance;
    }

    /**
     * @return the inputTolerance for RT
     */
    public String getInputCCSTolerance() {
        return this.inputCCSTolerance;
    }

    /**
     * @param inputCCSTolerance the inputTolerance for RT
     */
    public void setInputCCSTolerance(String inputCCSTolerance) {
        this.inputCCSTolerance = inputCCSTolerance;
    }

    /**
     * @return the inputModeTolerance
     */
    public String getInputModeTolerance() {
        return inputModeTolerance;
    }

    /**
     * @param inputModeTolerance the inputModeTolerance to set
     */
    public void setInputModeTolerance(String inputModeTolerance) {
        this.inputModeTolerance = inputModeTolerance;
    }

    /**
     * @return the queryMasses
     */
    public List<Double> getQueryMasses() {
        return this.queryMasses;
    }

    /**
     * @param queryMasses the masses introduced to set
     */
    public void setQueryMasses(List<Double> queryMasses) {
        this.queryMasses = queryMasses;
    }

    public List<Double> getQueryCCSs() {
        return this.queryCCSs;
    }

    /**
     * @param queryCCSs the Retention Times introduced to se
     */
    public void setQueryCCSs(List<Double> queryCCSs) {
        this.queryCCSs = queryCCSs;
    }
    
    //Added to support RT data from FrontEnd:
    public List<Double> getQueryRetentionTimes() {
        return queryRetentionTimes;
    }

    public void setQueryRetentionTimes(List<Double> queryRetentionTimes) {
        this.queryRetentionTimes = queryRetentionTimes;
    }

    private MSFacade getFacade() {
        return this.msFacade;
    }

    @Override
    /**
     * Getter of Items. If Items is null, the method is going to create a new
     * instance
     *
     */
    public List<FeatureCCS> getItems() {
        return this.items;
    }

    /**
     * @param items the items to set
     */
    public void setItems(List<FeatureCCS> items) {
        this.items = items;
    }

    /**
     * @return the thereAreTheoreticalCompounds
     */
    public boolean isThereTheoreticalCompounds() {
        return getQueryMasses().size() > 0;
    }

    /**
     * @return the isThereInputMasses
     */
    public boolean isThereInputMasses() {
        if (getQueryMasses() != null) {
            return getQueryMasses().size() > 0;
        }
        return false;
    }

    public String getChemAlphabet() {
        return chemAlphabet;
    }

    public void setChemAlphabet(String chemAlphabet) {
        this.chemAlphabet = chemAlphabet;
    }

    public boolean isIncludeDeuterium() {
        return this.includeDeuterium;
    }

    public void setIncludeDeuterium(boolean includeDeuterium) {
        this.includeDeuterium = includeDeuterium;
    }

    public String getMassesMode() {
        return this.massesMode;
    }

    public void setMassesMode(String massesMode) {
        if (massesMode.equals("mz")) {
            // By default, positive
            this.ionizationModeCandidates = AdductsLists.LISTIONIZEDMODES;
            this.ionMode = 1;
            this.adductsCandidates = positiveCandidates;
            this.adducts.clear();
            this.adducts.add("M+H");
        }

}

/**
 * @return The ionization mode
 */
public int getIonMode() {
        // System.out.println("ION MODE RETURNED: " + ionMode);
        return this.ionMode;
    }

    /**
     * Set the ionization mode
     *
     * @param ionMode
     */
    public void setIonMode(int ionMode) {
        switch (ionMode) {
            case 1:
                this.ionMode = ionMode;
                this.adductsCandidates = positiveCandidates;
                this.adducts.clear();
                this.adducts.add("M+H");
                break;
            case 2:
                this.ionMode = ionMode;
                this.adductsCandidates = negativeCandidates;
                this.adducts.clear();
                this.adducts.add("M-H");
                break;

            default:
                this.ionMode = 1;
                this.adductsCandidates = positiveCandidates;
                this.adducts.clear();
                this.adducts.add("M+H");
                break;
        }
    }

    /**
     * @return the databases to search
     */
    public List<String> getDatabases() {
        return databases;
    }

    /**
     * Set the databases to the object
     *
     * @param databases the databases to search
     */
    public void setDatabases(List<String> databases) {
        this.databases = databases;
    }

    /**
     * @return the modifier to search
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * Set the modifiers to the object
     *
     * @param modifier the modifier to search
     */
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    /**
     * @return the modifiers availables
     */
    public List<SelectItem> getModifierCandidates() {
        return modifierCandidates;
    }

    /**
     * Set the modifiers to the object
     *
     * @param modifierCandidates the modifiers availables
     */
    public void setModifierCandidates(List<SelectItem> modifierCandidates) {
        this.modifierCandidates = modifierCandidates;
    }

    /**
     * @return the metabolites type to search
     */
    public String getMetabolitesType() {
        return this.metabolitesType;
    }

    /**
     * Set the metabolites types for searching to the object
     *
     * @param metabolitesType the modifier to search
     */
    public void setMetabolitesType(String metabolitesType) {
        this.metabolitesType = metabolitesType;
    }

    /**
     * @return the metabolites types availables
     */
    public List<SelectItem> getMetabolitesTypecandidates() {
        return this.metabolitesTypecandidates;
    }

    /**
     * @return the adducts
     */
    public List<String> getAdducts() {
        return adducts;
    }

    /**
     * Set the adducts to the object
     *
     * @param adducts the adducts to search
     */
    public void setAdducts(List<String> adducts) {
        this.adducts = adducts;
        /*
        for(String s : adducts)
        {
            System.out.println("\n \n ADUCTO: " + s + "\n \n");
        }
         */
    }

    /**
     *
     * @return the number of adducts
     */
    public int getNumAdducts() {
        if (adducts.contains(DataFromInterfacesUtilities.ALLADDUCTS_POSITIVE)) {
            // Double check
            if (this.ionMode != 1) {
                System.out.println("\nSomething is wrong in the search form "
                        + "Adducts contains positive and ion mode is: " + ionMode);
            }
            return AdductsLists.MAPMZPOSITIVEADDUCTS.size();
        } else if (adducts.contains(DataFromInterfacesUtilities.ALLADDUCTS_NEGATIVE)) {
            if (this.ionMode != 2) {
                System.out.println("\nSomething is wrong in the search form "
                        + "Adducts contains negative and ion mode is: " + ionMode);
            }
            return AdductsLists.MAPMZPOSITIVEADDUCTS.size();
        } else if (this.ionMode == 0) {
            return 1;
        }

        // By default, paginate with number of adducts
        return adducts.size();
    }
    
    
    public String getKeggWebPage() {
        return Constants.WEB_KEGG;
    }

    public String getHMDBWebPage() {
        return Constants.WEB_HMDB;
    }

    public String getMetlinWebPage() {
        return Constants.WEB_METLIN;
    }

    public String getLMWebPage() {
        return Constants.WEB_LIPID_MAPS;
    }

    public String getPCWebPage() {
        return Constants.WEB_PUBCHEMICHAL;
    }

    public String getChebiWebPage() {
        return Constants.WEB_CHEBI;
    }

    public String getMINEWebPage() {
        return Constants.WEB_MINE;
    }


    public List<SelectItem> getAdductsCandidates() {
        return this.adductsCandidates;
    }

    public void setAdductsCandidates(List<SelectItem> adductsCandidates) {
        this.adductsCandidates = adductsCandidates;
    }

    public List<SelectItem> getDBcandidates() {
        return this.DBcandidates;
    }

    public List<SelectItem> getIonizationModeCandidates() {
        // System.out.println("GET CANDIDATES: "+ ionizationModeCandidates);
        return ionizationModeCandidates;
    }

    public void setIonizationModeCandidates(List<SelectItem> ionizationModeCandidates) {
        this.ionizationModeCandidates = ionizationModeCandidates;
    }


    /**
     * Validates the input Tolerance to be a float between 0 and 10000
     *
     * @param arg0 FacesContext of the form
     * @param arg1 Component of the form
     * @param arg2 Input of the user in the component arg1
     *
     */
    public void validateInputTolerance(FacesContext arg0, UIComponent arg1, Object arg2)
            throws ValidatorException {
        InterfaceValidators.validateInputTolerance(arg0, arg1, arg2);
    }

    /**
     * Validates the input single mass to be a float between 0 and 10000
     *
     * @param arg0 FacesContext of the form
     * @param arg1 Component of the form
     * @param arg2 Input of the user in the component arg1
     *
     */
    public void validateInputSingleMass(FacesContext arg0, UIComponent arg1, Object arg2)
            throws ValidatorException {
        InterfaceValidators.validateInputSingleMass(arg0, arg1, arg2);
    }

    /**
     * Validates the retention Time to be a float between 0 and 1000
     *
     * @param arg0 FacesContext of the form
     * @param arg1 Component of the form
     * @param arg2 Input of the user in the component arg1
     *
     */
    public void validateSingleRT(FacesContext arg0, UIComponent arg1, Object arg2)
            throws ValidatorException {
        InterfaceValidators.validateSingleRT(arg0, arg1, arg2);
    }

    public static String roundToFourDecimals(Double doubleToRound) {
        if (doubleToRound == null) {
            return "--";
        }
        return String.format("%.4f", doubleToRound).replace(",", ".");
    }

    @Override
        public List getItemsGroupedWithoutSignificative() {
        throw new UnsupportedOperationException("getItemsGroupedWithoutSignificative Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
        public List getItemsGrouped() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
