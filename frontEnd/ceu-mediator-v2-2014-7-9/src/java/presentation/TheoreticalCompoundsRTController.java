package presentation;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import controllers.InterfaceValidators;
import facades.TheoreticalCompoundsFacade;
import exporters.CompoundExcelExporter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import persistence.theoreticalCompound.TheoreticalCompounds;
import persistence.theoreticalGroup.TheoreticalCompoundsGroup;
import utilities.Cadena;
import utilities.AdductsLists;
import static utilities.Constants.*;
import static utilities.AdductsLists.DEFAULT_ADDUCTS_POSITIVE;
import utilities.DataFromInterfacesUtilities;
import static utilities.DataFromInterfacesUtilities.MAPDATABASES;
import utilities.Constants;

/**
 * Controller (Bean) of the application
 *
 * @author alberto Gil de la Fuente. San Pablo-CEU
 * @version: 3.1, 17/02/2016
 */
@ManagedBean(name = "theoreticalCompoundsRTController")
@SessionScoped
public class TheoreticalCompoundsRTController implements Serializable, Controller {

    private static final long serialVersionUID = 1L;

    private String inputCompoundRefStds;
    private String inputRefStdsRTs;

    private String queryInputMasses;
    private String queryInputRetentionTimes;
    private String queryInputCompositeSpectra;

    private String inputTolerance;
    private String inputModeTolerance;
    private String inputRTTolerance;
    private String chemAlphabet;
    private boolean includeDeuterium;
    private String modifier;
    private List<SelectItem> modifierCandidates;
    private List<String> databases;
    private final List<SelectItem> DBcandidates;
    private String metabolitesType;
    private final List<SelectItem> metabolitesTypecandidates;

    private String massesMode;
    private int ionMode;
    private List<SelectItem> ionizationModeCandidates;
    private List<String> adducts;
    private List<SelectItem> adductsCandidates;
    // to Improve efficiency. They are assigned to adductsCandidates
    private final List<SelectItem> positiveCandidates;
    private final List<SelectItem> negativeCandidates;
    private final List<SelectItem> neutralCandidates;

    // Declared as a variable because JSF needs that even Framework marks as not used.
    private int numAdducts;

    private List<Integer> cmmidsRefStds;
    private List<Double> refStdsRTs;

    private List<Double> queryMasses;
    private List<Double> queryRetentionTimes;
    private List<Map<Double, Double>> queryCompositeSpectrum;

    private List<TheoreticalCompoundsGroup> itemsGrouped;
    private List<TheoreticalCompounds> items;


    @EJB
    private facades.TheoreticalCompoundsFacade ejbFacade;

    public TheoreticalCompoundsRTController() {
        this.inputTolerance = TOLERANCE_DEFAULT_VALUE;
        this.inputModeTolerance = TOLERANCE_MODE_DEFAULT_VALUE;
        this.inputRTTolerance = TOLERANCE_RT_PRED_SEARCH_DEFAULT_VALUE;
        //String version = FacesContext.class.getPackage().getImplementationVersion();
        //System.out.println("\n\n  VERSION DE JSF: " + version + "\n\n");
        this.items = null;
        this.itemsGrouped = new LinkedList<>();
        this.DBcandidates = new LinkedList<>();
        this.DBcandidates.add(new SelectItem("AllWM", "All"));
        MAPDATABASES.entrySet().forEach((e) -> {
            this.DBcandidates.add(new SelectItem(e.getKey(), (String) e.getKey()));
        });
        this.DBcandidates.remove(this.DBcandidates.size() - 1);
        this.databases = new LinkedList<>();
        this.databases.add("AllWM");

        // MODIFIERS
        this.modifierCandidates = new LinkedList<>();
        DataFromInterfacesUtilities.MODIFIERS.entrySet().forEach((e) -> {
            this.modifierCandidates.add(new SelectItem(e.getKey(), (String) e.getKey()));
        });
        this.modifier = "None";

        this.metabolitesTypecandidates = new LinkedList<>();
        DataFromInterfacesUtilities.METABOLITESTYPES.entrySet().forEach((e) -> {
            this.metabolitesTypecandidates.add(new SelectItem(e.getKey(), (String) e.getKey()));
        });
        this.metabolitesType = "All except peptides";

        this.positiveCandidates = new LinkedList<>();
        this.positiveCandidates.add(new SelectItem(DataFromInterfacesUtilities.ALLADDUCTS_POSITIVE, "All"));
        (AdductsLists.MAPMZPOSITIVEADDUCTS).entrySet().forEach((e) -> {
            this.positiveCandidates.add(new SelectItem((String) e.getKey(), (String) e.getKey()));
        });

        this.negativeCandidates = new LinkedList<>();
        this.negativeCandidates.add(new SelectItem(DataFromInterfacesUtilities.ALLADDUCTS_NEGATIVE, "All"));
        (AdductsLists.MAPMZNEGATIVEADDUCTS).entrySet().forEach((e) -> {
            this.negativeCandidates.add(new SelectItem((String) e.getKey(), (String) e.getKey()));
        });

        this.neutralCandidates = new LinkedList<>();
        this.neutralCandidates.add(new SelectItem(DataFromInterfacesUtilities.ALLADDUCTS_NEUTRAL, "All"));
        (AdductsLists.MAPNEUTRALADDUCTS).entrySet().forEach((e) -> {
            this.neutralCandidates.add(new SelectItem((String) e.getKey(), (String) e.getKey()));
        });
        this.massesMode = "neutral";
        this.ionizationModeCandidates = AdductsLists.LISTNEUTRALMODES;
        this.ionMode = 0;
        this.adductsCandidates = neutralCandidates;
        this.adducts = new LinkedList<>();
        this.adducts.add(DataFromInterfacesUtilities.ALLADDUCTS_NEUTRAL);

        this.inputCompoundRefStds = "";
        this.inputRefStdsRTs = "";
        this.queryInputMasses = "";
        this.queryInputRetentionTimes = "";
        this.queryInputCompositeSpectra = "";
        this.chemAlphabet = "CHNOPS";
        this.includeDeuterium = false;
    }

    /**
     * This method is used to load a list of queryMasses declared in the class
     * Constants
     */
    public void setAdvancedDemoRTMasses() {
        this.setQueryInputMasses(NEWDEMOMASSES);
        this.setQueryInputRetentionTimes(NEWDEMORETENTIONTIME_SEC);
        this.setQueryInputCompositeSpectra(NEWDEMOSPECTRUM);
        this.setInputCompoundRefStds(DEMOREFSTDSIDS);
        this.setInputRefStdsRTs(DEMOREFSTDSRTs);
        this.chemAlphabet = "CHNOPS";
        this.includeDeuterium = false;
        this.setMassesMode("mz");
    }

    /**
     * Method to clean the input formulary where the queryMasses are written
     */
    public void clearForm() {

        this.inputCompoundRefStds = "";
        this.inputRefStdsRTs = "";
        this.queryInputMasses = "";
        this.queryInputRetentionTimes = "";
        this.queryInputCompositeSpectra = "";
        this.inputTolerance = TOLERANCE_DEFAULT_VALUE;
        this.inputModeTolerance = TOLERANCE_MODE_DEFAULT_VALUE;
        this.inputRTTolerance = TOLERANCE_RT_PRED_SEARCH_DEFAULT_VALUE;

        this.chemAlphabet = "CHNOPS";
        this.includeDeuterium = false;

        this.databases.clear();
        this.databases.add("AllWM");
        /*this.massesMode = "neutral";
        this.ionizationModeCandidates = AdductsLists.LISTNEUTRALMODES;
        this.ionMode = "neutral";
        this.adductsCandidates = neutralCandidates;
        this.adducts.clear();
        this.adducts.add(DataFromInterfacesUtilities.ALLADDUCTS_NEUTRAL);
         */
        resetItems();
    }

    private void resetItems() {
        this.items = null;
        this.itemsGrouped.clear();
    }

    /**
     * Method that permits to create a excel from the current results. Flag
     * indicates if the excel generated contains RT field or not. 1 yes 0 no
     *
     * @param flag
     */
    public void exportToExcel(int flag) {
        // Only export to Excel no significative compounds
        if (this.items != null && !this.items.isEmpty()) {

            CompoundExcelExporter compoundExcelExporter = new CompoundExcelExporter(flag);
            compoundExcelExporter.generateWholeExcelCompound(this.items, flag);
        }
    }

    /**
     * Submit compounds in advanced mode.
     */
    public void submitCompoundsRTPred() {
        this.items = null;
        this.itemsGrouped.clear();
        List<Double> massesAux; // auxiliar List for input Masses
        int numInputMasses;
        List<Double> retAux; // Auxiliar List for Retention Times
        List<Map<Double, Double>> spectrumAux;  // Auxiliar List for Composite Spectra processed

        //Method returns an ArrayList because it is acceded by index
        this.queryMasses = Cadena.extractDoubles(this.queryInputMasses);
        numInputMasses = this.queryMasses.size();
        this.queryRetentionTimes = Cadena.getListOfDoubles(this.queryInputRetentionTimes, numInputMasses);
        this.queryCompositeSpectrum = getListOfCompositeSpectra(this.queryInputCompositeSpectra, numInputMasses);

        // System.out.println("INPUT: " + queryInputMasses + " \n ARRAY: " + massesAux);
        // System.out.println("INPUT RETENTION TIME: " + this.queryInputRetentionTimes);
        // System.out.println("INPUT COMPOSITE SPECTRUM: " + this.queryInputCompositeSpectra);
        //System.out.println("SIMPLE SEARCH");
        //System.out.println("Sign Compounds: "+ isSignifativeCompoundAux.toString() + " size: " + isSignifativeCompoundAux.size());
        this.cmmidsRefStds = Cadena.extractInts(this.inputCompoundRefStds);
        this.refStdsRTs = Cadena.extractDoubles(this.inputRefStdsRTs);

        // Convert RTs to seconds
        // First it is needed to transform the RT into the model unit
        transformAndProjectRTs();

    }

    /**
     * @return the queryInputCompositeSpectra
     */
    public String getQueryInputCompositeSpectra() {
        return this.queryInputCompositeSpectra;
    }

    /**
     * Catches the input text in the formulary on the web of the composite
     * Spectrum and obtains the list of them in order.
     *
     * @param queryInputCompositeSpectra the queryInputCompositeSpectra to set
     */
    public void setQueryInputCompositeSpectra(String queryInputCompositeSpectra) {
        this.queryInputCompositeSpectra = queryInputCompositeSpectra;
    }

    /**
     * @return the queryInputMasses
     */
    public String getQueryInputMasses() {
        return this.queryInputMasses;
    }

    /**
     * Catches the input text in the formulary on the web of the queryMasses and
     * obtains the list of them in order.
     *
     * @param queryInputMasses the queryInputMasses to set
     */
    public void setQueryInputMasses(String queryInputMasses) {
        this.queryInputMasses = queryInputMasses;
    }

    public String getInputCompoundRefStds() {
        return inputCompoundRefStds;
    }

    public void setInputCompoundRefStds(String inputCompoundRefStds) {
        this.inputCompoundRefStds = inputCompoundRefStds;
    }

    public String getInputRefStdsRTs() {
        return inputRefStdsRTs;
    }

    public void setInputRefStdsRTs(String inputRefStdsRTs) {
        this.inputRefStdsRTs = inputRefStdsRTs;
    }

    public String getQueryInputRetentionTimes() {
        return queryInputRetentionTimes;
    }

    public void setQueryInputRetentionTimes(String queryInputRetentionTimes) {
        this.queryInputRetentionTimes = queryInputRetentionTimes;
    }

    /**
     * @return the inputTolerance
     */
    public String getInputTolerance() {
        return this.inputTolerance;
    }

    /**
     * @param inputTolerance the inputTolerance to set
     */
    public void setInputTolerance(String inputTolerance) {
        this.inputTolerance = inputTolerance;
    }

    /**
     * @return the inputTolerance for RT
     */
    public String getInputRTTolerance() {
        return this.inputRTTolerance;
    }

    /**
     * @param inputRTTolerance the inputTolerance for RT
     */
    public void setInputRTTolerance(String inputRTTolerance) {
        this.inputRTTolerance = inputRTTolerance;
    }

    /**
     * @return the inputModeTolerance
     */
    public String getInputModeTolerance() {
        return inputModeTolerance;
    }

    /**
     * @param inputModeTolerance the inputModeTolerance to set
     */
    public void setInputModeTolerance(String inputModeTolerance) {
        this.inputModeTolerance = inputModeTolerance;
    }

    /**
     * @return the queryMasses
     */
    public List<Double> getQueryMasses() {
        return this.queryMasses;
    }

    /**
     * @param queryMasses the masses introduced to set
     */
    public void setQueryMasses(List<Double> queryMasses) {
        this.queryMasses = queryMasses;
    }

    public List<Double> getQueryRetentionTimes() {
        return queryRetentionTimes;
    }

    /**
     * @param queryRetentionTimes the Retention Times introduced to se
     */
    public void setQueryRetentionTimes(List<Double> queryRetentionTimes) {
        this.queryRetentionTimes = queryRetentionTimes;
    }

    public List<Map<Double, Double>> getQueryCompositeSpectrum() {
        return queryCompositeSpectrum;
    }

    public void setQueryCompositeSpectrum(List<Map<Double, Double>> queryCompositeSpectrum) {
        this.queryCompositeSpectrum = queryCompositeSpectrum;
    }

    private TheoreticalCompoundsFacade getFacade() {
        return this.ejbFacade;
    }

    @Override
    /**
     * Getter of Items. If Items is null, the method is going to create a new
     * instance
     *
     */
    public List<TheoreticalCompounds> getItems() {
        System.out.println("\nGET ITEMS CALLED -> \n");
        return this.items;
    }

    @Override
    /**
     * Getter of ItemsGrouped. If ItemsGrouped is null, the method is going to
     * create a new instance
     *
     */
    public List<TheoreticalCompoundsGroup> getItemsGrouped() {
        return this.itemsGrouped;
    }

    /**
     * @param items the items to set
     */
    public void setItems(List<TheoreticalCompounds> items) {
        this.items = items;
    }

    /**
     * @param itemsGrouped the items to set
     */
    public void setItemsGrouped(List<TheoreticalCompoundsGroup> itemsGrouped) {
        this.itemsGrouped = itemsGrouped;
    }

    /**
     * @return the thereAreTheoreticalCompounds
     */
    public boolean isThereTheoreticalCompounds() {
        return getQueryMasses().size() > 0;
    }

    /**
     * @return the isThereInputMasses
     */
    public boolean isThereInputMasses() {
        if (getQueryMasses() != null) {
            return getQueryMasses().size() > 0;
        }
        return false;
    }

    public String getChemAlphabet() {
        return chemAlphabet;
    }

    public void setChemAlphabet(String chemAlphabet) {
        this.chemAlphabet = chemAlphabet;
    }

    public boolean isIncludeDeuterium() {
        return this.includeDeuterium;
    }

    public void setIncludeDeuterium(boolean includeDeuterium) {
        this.includeDeuterium = includeDeuterium;
    }

    public String getMassesMode() {
        return this.massesMode;
    }

    public void setMassesMode(String massesMode) {
        switch (massesMode) {
            case "neutral":
                this.ionizationModeCandidates = AdductsLists.LISTNEUTRALMODES;
                this.ionMode = 0;
                this.adductsCandidates = neutralCandidates;
                this.adducts.clear();
                this.adducts.add(DataFromInterfacesUtilities.ALLADDUCTS_NEUTRAL);
                break;
            // If there is not any of these 3 (It should not occur) The assigned mode is neutral
            case "mz":
                // By default, positive
                this.ionizationModeCandidates = AdductsLists.LISTIONIZEDMODES;
                this.ionMode = 1;
                this.adductsCandidates = positiveCandidates;
                this.adducts.clear();
                this.adducts.addAll(DEFAULT_ADDUCTS_POSITIVE);
                break;
            /*
            case Constants.NAME_FOR_RECALCULATED:
                this.ionizationModeCandidates = AdductsLists.LISTIONIZEDMODES;
                this.ionMode = "positive";
                this.adductsCandidates = positiveCandidates;
                this.adducts.clear();
                this.adducts.addAll(DEFAULT_ADDUCTS_POSITIVE);
                break;
             */
            default:
                this.ionizationModeCandidates = AdductsLists.LISTNEUTRALMODES;
                break;
        }
        // System.out.println("CHANGED TO: "+massesMode);
        // System.out.println("IONMODE: " + ionMode);
        // System.out.println("ADDUCTS: " + adducts);
        this.massesMode = massesMode;
    }

    /**
     * @return The ionization mode
     */
    public int getIonMode() {
        // System.out.println("ION MODE RETURNED: " + ionMode);
        return this.ionMode;
    }

    /**
     * Set the ionization mode
     *
     * @param ionMode
     */
    public void setIonMode(int ionMode) {
        switch (ionMode) {
            case 1:
                this.ionMode = ionMode;
                this.adductsCandidates = positiveCandidates;
                this.adducts.clear();
                this.adducts.addAll(DEFAULT_ADDUCTS_POSITIVE);
                break;
            case 2:
                this.ionMode = ionMode;
                this.adductsCandidates = negativeCandidates;
                this.adducts.clear();
                this.adducts.addAll(AdductsLists.DEFAULT_ADDUCTS_NEGATIVE);
                break;
            // If there is not any of these 3 (It should not occur) The assigned mode is neutral
            case 0:
                this.ionMode = ionMode;
                this.adductsCandidates = neutralCandidates;
                this.adducts.clear();
                this.adducts.add(DataFromInterfacesUtilities.ALLADDUCTS_NEUTRAL);
                break;
            default:
                this.ionMode = 0;
                this.adductsCandidates = neutralCandidates;
                this.adducts.clear();
                this.adducts.add(DataFromInterfacesUtilities.ALLADDUCTS_NEUTRAL);
                break;
        }
    }

    /**
     * @return the databases to search
     */
    public List<String> getDatabases() {
        return databases;
    }

    /**
     * Set the databases to the object
     *
     * @param databases the databases to search
     */
    public void setDatabases(List<String> databases) {
        this.databases = databases;
    }

    /**
     * @return the modifier to search
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * Set the modifiers to the object
     *
     * @param modifier the modifier to search
     */
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    /**
     * @return the modifiers availables
     */
    public List<SelectItem> getModifierCandidates() {
        return modifierCandidates;
    }

    /**
     * Set the modifiers to the object
     *
     * @param modifierCandidates the modifiers availables
     */
    public void setModifierCandidates(List<SelectItem> modifierCandidates) {
        this.modifierCandidates = modifierCandidates;
    }

    /**
     * @return the metabolites type to search
     */
    public String getMetabolitesType() {
        return this.metabolitesType;
    }

    /**
     * Set the metabolites types for searching to the object
     *
     * @param metabolitesType the modifier to search
     */
    public void setMetabolitesType(String metabolitesType) {
        this.metabolitesType = metabolitesType;
    }

    /**
     * @return the metabolites types availables
     */
    public List<SelectItem> getMetabolitesTypecandidates() {
        return this.metabolitesTypecandidates;
    }

    /**
     * @return the queryMasses
     */
    public List<String> getAdducts() {
        return adducts;
    }

    /**
     * Set the adducts to the object
     *
     * @param adducts the adducts to search
     */
    public void setAdducts(List<String> adducts) {
        this.adducts = adducts;
        /*
        for(String s : adducts)
        {
            System.out.println("\n \n ADUCTO: " + s + "\n \n");
        }
         */
    }


    /**
     *
     * @return the number of adducts
     */
    public int getNumAdducts() {
        if (adducts.contains(DataFromInterfacesUtilities.ALLADDUCTS_POSITIVE)) {
            // Double check
            if (this.ionMode != 1) {
                System.out.println("\nSomething is wrong in the search form "
                        + "Adducts contains positive and ion mode is: " + ionMode);
            }
            return AdductsLists.MAPMZPOSITIVEADDUCTS.size();
        } else if (adducts.contains(DataFromInterfacesUtilities.ALLADDUCTS_NEGATIVE)) {
            if (this.ionMode != 2) {
                System.out.println("\nSomething is wrong in the search form "
                        + "Adducts contains negative and ion mode is: " + ionMode);
            }
            return AdductsLists.MAPMZPOSITIVEADDUCTS.size();
        } else if (this.ionMode == 0) {
            return 1;
        }

        // By default, paginate with number of adducts
        return adducts.size();
    }

    public List<SelectItem> getAdductsCandidates() {
        return this.adductsCandidates;
    }

    public void setAdductsCandidates(List<SelectItem> adductsCandidates) {
        this.adductsCandidates = adductsCandidates;
    }

    public List<SelectItem> getDBcandidates() {
        return this.DBcandidates;
    }

    public List<SelectItem> getIonizationModeCandidates() {
        // System.out.println("GET CANDIDATES: "+ ionizationModeCandidates);
        return ionizationModeCandidates;
    }

    public void setIonizationModeCandidates(List<SelectItem> ionizationModeCandidates) {
        this.ionizationModeCandidates = ionizationModeCandidates;
    }

    private List<Map<Double, Double>> getListOfCompositeSpectra(String input, int numInputMasses) {
        List<Map<Double, Double>> spectrumAux;
        if (!input.equals("")) {
            spectrumAux = Cadena.extractDataSpectrum(input);
            // If there is no time for all queryMasses, fill with 0
            for (int i = spectrumAux.size(); i < numInputMasses; i++) {
                spectrumAux.add(new TreeMap<Double, Double>());
            }
        } else {
            spectrumAux = new ArrayList<Map<Double, Double>>();
            // If there is no time for all queryMasses, fill with 0
            for (int i = 0; i < numInputMasses; i++) {
                spectrumAux.add(new TreeMap<Double, Double>());
            }

        }
        return spectrumAux;
    }

    public String showMessageForNeutralMasses() {
        if (this.massesMode.equals(Constants.NAME_FOR_RECALCULATED) && (this.ionMode == 1 || this.ionMode == 2)) {
            return "calculation of new m/z from neutral mass based on selected adducts";
        } else {
            return "";
        }
    }

    private void transformAndProjectRTs() {
        Client c = ClientBuilder.newClient();
        WebTarget target = c.target("http://localhost:5000/");

        Gson gson = new Gson();

        // Transform RTs from known CMM IDs
        JsonObject json = new JsonObject();
        JsonArray JSONExperimentalRTs = gson.toJsonTree(this.refStdsRTs).getAsJsonArray();
        json.add("experimental_rt", JSONExperimentalRTs);
        String jsonQuery = json.toString();
        Response response
                = target.path("transform").request(MediaType.APPLICATION_JSON).post(Entity.json(jsonQuery));
        String JSONResult = response.readEntity(String.class);
        response.close();
        JsonObject jsonObjectResult = (JsonObject) gson.fromJson(JSONResult, JsonObject.class);

        // Transform experimental RTs from unknown
        JsonObject jsonUnkRTs = new JsonObject();
        JsonArray JSONExperimentalUnkRTs = gson.toJsonTree(this.queryRetentionTimes).getAsJsonArray();
        jsonUnkRTs.add("experimental_rt", JSONExperimentalUnkRTs);
        jsonQuery = jsonUnkRTs.toString();
        response
                = target.path("transform").request(MediaType.APPLICATION_JSON).post(Entity.json(jsonQuery));
        JSONResult = response.readEntity(String.class);
        response.close();
        JsonObject jsonTransUnkRTsResult = (JsonObject) gson.fromJson(JSONResult, JsonObject.class);

        // Get modelled RTs from known CMM IDS
        List<Double> modelled_RTs = this.ejbFacade.findModelledRTs(this.cmmidsRefStds);
        JsonArray JSONModelledRTs = gson.toJsonTree(modelled_RTs).getAsJsonArray();
        jsonObjectResult.add("modelled_rt", JSONModelledRTs);
        jsonQuery = jsonObjectResult.toString();
        // Train the project
        response
                = target.path("projector").request(MediaType.APPLICATION_JSON).post(Entity.json(jsonQuery));
        String resultProjection = response.readEntity(String.class);
        Map<String, NewCookie> mapCookies = response.getCookies();
        response.close();

        /*
        Copy on the attribute queryRetentionTimes
        
        JsonArray JSONRTsTransformed = (JsonArray) jsonTransUnkRTsResult.get("experimental_rt");
        
        this.queryRetentionTimes.clear();
        for (int i = 0; i < JSONRTsTransformed.size(); i++) {
            try {
                this.queryRetentionTimes.add(Double.parseDouble(JSONRTsTransformed.get(i).toString()));
            } catch (NumberFormatException nfe) {
                System.out.println("Problem parsing a double..." + JSONRTsTransformed.get(i));
            }
        }
         */
        // Do the projection to query the database obtaining mean and std deviation
        String cookieTitle = "session";
        String cookieValue = mapCookies.get(cookieTitle).toString();

        jsonQuery = jsonTransUnkRTsResult.toString();
        response
                = target.path("do_projection").request(MediaType.APPLICATION_JSON).header("Cookie", cookieValue).post(Entity.json(jsonQuery));
        JSONResult = response.readEntity(String.class);
        response.close();
        JsonObject jsonRTsStdDevs = (JsonObject) gson.fromJson(JSONResult, JsonObject.class);

        JsonArray rtsForDBJsonArray = (JsonArray) jsonRTsStdDevs.get("mean");
        JsonArray stdDevsForDBJsonArray = (JsonArray) jsonRTsStdDevs.get("std");
        this.refStdsRTs.clear();
        List<Double> rtsForDB = new ArrayList();
        List<Double> stdDevsForDB = new ArrayList();
        for (int i = 0; i < rtsForDBJsonArray.size(); i++) {
            try {
                rtsForDB.add(Double.parseDouble(rtsForDBJsonArray.get(i).toString()));
                stdDevsForDB.add(Double.parseDouble(stdDevsForDBJsonArray.get(i).toString()));
            } catch (NumberFormatException nfe) {
                System.out.println("Problem parsing a double..." + rtsForDBJsonArray.get(i) + " WITH STD DVT" + stdDevsForDBJsonArray.get(i));
            }
        }
        
        // Start the query

        String chemAlphabetWithDeuterium = DataFromInterfacesUtilities.getChemAlphabet(this.chemAlphabet, this.includeDeuterium);
        int chemAlphabetForSearch = DataFromInterfacesUtilities.getIntChemAlphabet(chemAlphabetWithDeuterium);
        Integer RTTolerance = DataFromInterfacesUtilities.MAPRTTOLERANCES.get(this.inputRTTolerance);
        if (this.itemsGrouped == null) {
            this.itemsGrouped = new LinkedList<TheoreticalCompoundsGroup>();
        }
        if (this.items == null || this.itemsGrouped.isEmpty()) {
            this.items = this.ejbFacade.findCompoundsRTPred(
                    this.queryMasses,
                    rtsForDB,
                    stdDevsForDB,
                    this.queryCompositeSpectrum,
                    this.inputModeTolerance,
                    Double.parseDouble(this.inputTolerance),
                    RTTolerance,
                    chemAlphabetWithDeuterium,
                    this.ionMode,
                    this.massesMode,
                    this.adducts,
                    this.itemsGrouped,
                    this.databases,
                    this.metabolitesType
            );

        }
    }


    /**
     * Validates the input Tolerance to be a float between 0 and 10000
     *
     * @param arg0 FacesContext of the form
     * @param arg1 Component of the form
     * @param arg2 Input of the user in the component arg1
     *
     */
    public void validateInputTolerance(FacesContext arg0, UIComponent arg1, Object arg2)
            throws ValidatorException {
        InterfaceValidators.validateInputTolerance(arg0, arg1, arg2);
    }

    /**
     * Validates the input single mass to be a float between 0 and 10000
     *
     * @param arg0 FacesContext of the form
     * @param arg1 Component of the form
     * @param arg2 Input of the user in the component arg1
     *
     */
    public void validateInputSingleMass(FacesContext arg0, UIComponent arg1, Object arg2)
            throws ValidatorException {
        InterfaceValidators.validateInputSingleMass(arg0, arg1, arg2);
    }

    /**
     * Validates the retention Time to be a float between 0 and 1000
     *
     * @param arg0 FacesContext of the form
     * @param arg1 Component of the form
     * @param arg2 Input of the user in the component arg1
     *
     */
    public void validateSingleRT(FacesContext arg0, UIComponent arg1, Object arg2)
            throws ValidatorException {
        InterfaceValidators.validateSingleRT(arg0, arg1, arg2);
    }

    public static String roundToFourDecimals(Double doubleToRound) {
        if (doubleToRound == null) {
            return "--";
        }
        return String.format("%.4f", doubleToRound).replace(",", ".");
    }

    @Override
    public List getItemsGroupedWithoutSignificative() {
        throw new UnsupportedOperationException("getItemsGroupedWithoutSignificative Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
