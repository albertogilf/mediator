/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CEMS;

import CEMS.enums.CE_Exp_properties.CE_EXP_PROP_ENUM;
import java.util.Set;
import java.util.TreeSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alberto Gil de la Fuente
 */
public class CEMSAnnotationTest {

    public CEMSAnnotationTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of equals method, of class CEMSAnnotation.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");

        Double exp_mz = 76.0392;
        String adduct = "M+H";
        Double exp_MT = 9.015;
        Double exp_RMT = 0.759615384615385;
        Double exp_effMob = 1442.34660486998;
        Set<CEMSFragment> experimentalFragments = new TreeSet<>();
        CE_EXP_PROP_ENUM ce_exp_properties = CE_EXP_PROP_ENUM.FORMIC_20_POS_DIR;
        Double effMob = 1442.34660486998;
        Double MT = 9.015;
        Double RMT = 0.759615384615385;
        Set<CEMSFragment> productIons = new TreeSet<>();
        Integer compound_id = 1;

        Double mass = 76.0392;
        String formula = null;
        String compound_name = null;
        String cas_id = null;
        Integer formula_type = 0;
        Integer compound_type = 0;

        Integer compound_status = 0;
        Integer charge_type = 0;
        Integer charge_number = 0;
        String lm_id = null;
        String kegg_id = null;
        String hmdb_id = null;
        String metlin_id = null;
        String in_house_id = null;
        Integer pc_id = null;
        Integer chebi_id = null;
        String knapsack_id = null;
        Integer npatlas_id = null;
        Integer MINE_id = null;
        String aspergillus_id = null;
        String mesh_nomenclature = null;
        String iupac_classification = null;
        String aspergillus_web_name = null;
        Integer fahfa_id = 0;
        Integer oh_position = 0;

        CEMSAnnotation obj = new CEMSAnnotation(exp_mz, adduct, exp_effMob, exp_MT, exp_RMT, experimentalFragments, ce_exp_properties,
                effMob, MT, RMT, productIons, compound_id, mass, formula, compound_name, cas_id, formula_type, compound_type,
                compound_status, charge_type, charge_number, lm_id, kegg_id, hmdb_id, metlin_id, in_house_id, pc_id, chebi_id, MINE_id, knapsack_id, npatlas_id,
                aspergillus_id, mesh_nomenclature, iupac_classification, aspergillus_web_name, fahfa_id, oh_position,
                null, null, true);
        CEMSAnnotation instance = new CEMSAnnotation(exp_mz, adduct, exp_effMob, exp_MT, exp_RMT, experimentalFragments, ce_exp_properties,
                effMob, MT, RMT, productIons, compound_id, mass, formula, compound_name, cas_id, formula_type, compound_type,
                compound_status, charge_type, charge_number, lm_id, kegg_id, hmdb_id, metlin_id, in_house_id, pc_id, chebi_id, MINE_id, knapsack_id, npatlas_id,
                aspergillus_id, mesh_nomenclature, iupac_classification, aspergillus_web_name, fahfa_id, oh_position,
                null, null, true);
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of areThereExperimentalFragments method, of class CEMSAnnotation.
     */
    @Test
    public void testAreThereFragments() {
        System.out.println("areThereFragments");

        Double exp_mz = 76.0392;
        String adduct = "M+H";
        Double exp_MT = 9.015;
        Double exp_RMT = 0.759615384615385;
        Double exp_effMob = 1442.34660486998;
        Set<CEMSFragment> experimentalFragments = new TreeSet<>();
        CE_EXP_PROP_ENUM ce_exp_properties = CE_EXP_PROP_ENUM.FORMIC_20_POS_DIR;
        Double effMob = 1442.34660486998;
        Double MT = 9.015;
        Double RMT = 0.759615384615385;
        Set<CEMSFragment> productIons = new TreeSet<>();
        Integer compound_id = 1;

        Double mass = 76.0392;
        String formula = null;
        String compound_name = null;
        String cas_id = null;
        Integer formula_type = 0;
        Integer compound_type = 0;

        Integer compound_status = 0;
        Integer charge_type = 0;
        Integer charge_number = 0;
        String lm_id = null;
        String kegg_id = null;
        String hmdb_id = null;
        String metlin_id = null;
        String in_house_id = null;
        Integer pc_id = null;
        Integer chebi_id = null;
        String knapsack_id = null;
        Integer npatlas_id = null;
        Integer MINE_id = null;
        String aspergillus_id = null;
        String mesh_nomenclature = null;
        String iupac_classification = null;
        String aspergillus_web_name = null;
        Integer fahfa_id = 0;
        Integer oh_position = 0;
        
        CEMSAnnotation instance = new CEMSAnnotation(exp_mz, adduct, exp_effMob, exp_MT, exp_RMT, experimentalFragments, ce_exp_properties,
                effMob, MT, RMT, productIons, compound_id, mass, formula, compound_name, cas_id, formula_type, compound_type,
                compound_status, charge_type, charge_number, lm_id, kegg_id, hmdb_id, metlin_id, in_house_id, pc_id, chebi_id, MINE_id, knapsack_id, npatlas_id,
                aspergillus_id, mesh_nomenclature, iupac_classification, aspergillus_web_name, fahfa_id, oh_position,
                null, null, true);
        boolean expResult = false;
        boolean result = instance.areThereExperimentalFragments();
        assertEquals(expResult, result);

        Integer ion_source_voltage = 100;
        Double mz = 76.0392;
        Double intensity = null;
        String transformation_type = null;
        String name = null;
        CEMSCompound precursorIon = instance;

        Double effMobPI = 1442.34660486998;
        Double MTPI = null;
        Double RMTPI = null;

        CEMSFragment productIon = new CEMSFragment(effMobPI, MTPI, RMTPI, ion_source_voltage, mz, intensity, transformation_type, name, precursorIon);
        instance.addExperimentalFragment(productIon);
        expResult = true;
        result = instance.areThereExperimentalFragments();

        assertEquals(expResult, result);

        assertEquals(1, instance.getExperimentalFragments().size());

    }

    /**
     * Test of getFragmentsNotFound method, of class CEMSAnnotation.
     */
    @Test
    public void testGetFragmentsNotFound() {

        System.out.println("getFragmentsNotFound");
        Double exp_mz = 76.0392;
        String adduct = "M+H";
        Double exp_MT = 9.015;
        Double exp_RMT = 0.759615384615385;
        Double exp_effMob = 1442.34660486998;
        Set<CEMSFragment> experimentalFragments = new TreeSet<>();
        CE_EXP_PROP_ENUM ce_exp_properties = CE_EXP_PROP_ENUM.FORMIC_20_POS_DIR;
        Double effMob = 1442.34660486998;
        Double MT = 9.015;
        Double RMT = 0.759615384615385;
        Set<CEMSFragment> theoreticalProductIons = new TreeSet<>();
        Integer compound_id = 1;

        Double mass = 76.0392;
        String formula = null;
        String compound_name = null;
        String cas_id = null;
        Integer formula_type = 0;
        Integer compound_type = 0;

        Integer compound_status = 0;
        Integer charge_type = 0;
        Integer charge_number = 0;
        String lm_id = null;
        String kegg_id = null;
        String hmdb_id = null;
        String metlin_id = null;
        String in_house_id = null;
        Integer pc_id = null;
        Integer chebi_id = null;
        String knapsack_id = null;
        Integer npatlas_id = null;
        Integer MINE_id = null;
        String aspergillus_id = null;
        String mesh_nomenclature = null;
        String iupac_classification = null;
        String aspergillus_web_name = null;
        Integer fahfa_id = 0;
        Integer oh_position = 0;

        Integer ion_source_voltage = 100;
        Double mz = 76.0392;
        Double intensity = null;
        String transformation_type = null;
        String name = null;
        CEMSCompound precursorIon = null;

        Double effMobPI = 1442.34660486998;
        Double MTPI = null;
        Double RMTPI = null;

        CEMSFragment experimentalProductIon = new CEMSFragment(effMobPI, MTPI, RMTPI, ion_source_voltage, mz, intensity, transformation_type, name, precursorIon);
        CEMSFragment theoreticalProductIonPresent = new CEMSFragment(effMobPI, MTPI, RMTPI, ion_source_voltage, mz, intensity, transformation_type, name, precursorIon);

        ion_source_voltage = 100;
        mz = 77d;
        intensity = null;
        transformation_type = null;
        name = null;

        effMobPI = 1300d;

        CEMSFragment theoreticalProductIonNotPresent = new CEMSFragment(effMobPI, MTPI, RMTPI, ion_source_voltage, mz, intensity, transformation_type, name, precursorIon);

        theoreticalProductIons.add(theoreticalProductIonPresent);
        theoreticalProductIons.add(theoreticalProductIonNotPresent);
        CEMSAnnotation instance = new CEMSAnnotation(exp_mz, adduct, exp_effMob, exp_MT, exp_RMT, experimentalFragments, ce_exp_properties,
                effMob, MT, RMT, theoreticalProductIons, compound_id, mass, formula, compound_name, cas_id, formula_type, compound_type,
                compound_status, charge_type, charge_number, lm_id, kegg_id, hmdb_id, metlin_id, in_house_id, pc_id, chebi_id, MINE_id, knapsack_id, npatlas_id,
                aspergillus_id, mesh_nomenclature, iupac_classification, aspergillus_web_name, fahfa_id, oh_position,
                null, null, true);
        Set<CEMSFragment> expResult = new TreeSet<>();
        Set<CEMSFragment> result = instance.getFragmentsFound();
        assertEquals(expResult, result);

        expResult.add(theoreticalProductIonPresent);
        instance.addExperimentalFragment(experimentalProductIon);
        result = instance.getFragmentsFound();
        assertEquals(expResult, result);

        expResult = new TreeSet<>();
        expResult.add(theoreticalProductIonNotPresent);
        result = instance.getTheoreticalFragmentsNotFound();
        assertEquals(expResult, result);

    }

}
