/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import CEMS.CEMSCompound;
import CEMS.CEMSFeature;
import CEMS.CEMSFragment;
import CEMS.enums.CE_Exp_properties;
import compound.Structure;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import pathway.Pathway;

/**
 *
 * @author ceu
 */
public class CEMSFacadeInMemoryTest {

    public CEMSFacadeInMemoryTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getCECompoundsFromMassesToleranceAndMTs method, of class
     * CEMSFacadeInMemory.
     */
    @Test
    public void testGetCECompoundsFromMassesToleranceAndMTs() throws Exception {
        System.out.println("getCECompoundsFromMassesToleranceAndMTs");
        Set<CEMSCompound> dbcandidates = null;
        List<Double> mzs = null;
        Integer mzTolerance = null;
        Integer mzToleranceMode = null;
        List<Double> MTs = null;
        Integer MTTolerance = null;
        String MTToleranceMode = "";
        int buffer = 0;
        int temperature = 20;
        Integer chemAlphabet = null;
        Boolean includeDeuterium = null;
        Integer massesMode = null;
        Integer ionMode = null;
        Integer polarity = null;
        List<String> adducts = null;
        List<CEMSFeature> expResult = null;
        List<CEMSFeature> result = CEMSFacadeInMemory.getCECompoundsFromMassesToleranceAndMTs(
                dbcandidates, mzs, mzTolerance, mzToleranceMode, MTs, MTTolerance, MTToleranceMode,
                chemAlphabet, includeDeuterium, massesMode, ionMode, polarity, adducts);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of filterIntegerFormulaTypeCEMSCompounds method, of class
     * CEMSFacadeInMemory.
     */
    @Test
    public void testFilterIntegerFormulaTypeCEMSCompounds() {
        System.out.println("filterIntegerFormulaTypeCEMSCompounds");
        Set<CEMSCompound> dbCandidates = new TreeSet();
        CE_Exp_properties.CE_EXP_PROP_ENUM ce_exp_properties = CE_Exp_properties.CE_EXP_PROP_ENUM.FORMIC_25_POS_DIR;
        Set<CEMSFragment> productIons = null;
        Double effMob = 2697.16533870139;
        Double MT = 7.87;
        Double RMT = 0.50;
        Integer compound_id = 180838;
        Double mass = 181.0409;
        String formula = "C5H11NO4S";
        String compound_name = "L-Methionine sulfone";
        String cas_id = null;
        Integer formula_type = 0;
        Integer compound_type = 0;
        Integer compound_status = 0;
        Integer charge_type = 0;
        Integer charge_number = 0;
        Integer fahfa_id = 0;
        Integer oh_position = 0;
        Structure structure = null;
        List<Pathway> pathways = null;
        CEMSCompound cemscompound1 = new CEMSCompound(ce_exp_properties, effMob, MT, RMT,
                productIons, compound_id, mass, formula, compound_name, cas_id, formula_type, compound_type,
                compound_status, charge_type, charge_number, "", "", "", "", "", 0, 0, 0, "", 0, "", "", "", "", 
                fahfa_id, oh_position, structure, pathways);

        effMob = -43.86;
        MT = 25.29;
        RMT = 1.61;
        compound_id = 91854;
        mass = 179.058243159;
        formula = "C9H9NO3 ";
        compound_name = "Hippuric acid";

        CEMSCompound cemscompound2 = new CEMSCompound(ce_exp_properties, effMob, MT, RMT,
                productIons, compound_id, mass, formula, compound_name, cas_id, formula_type, compound_type,
                compound_status, charge_type, charge_number, "", "", "", "", "", 0, 0, 0, "", 0, "", "", "", "", 
                fahfa_id, oh_position, structure, pathways);

        effMob = 1669.66;
        MT = 10.89;
        RMT = 0.70;
        compound_id = 46307;
        mass = 169.085126611;
        formula = "C7H11N3O2";
        compound_name = "3-Methylhistidine";

        CEMSCompound cemscompound3 = new CEMSCompound(ce_exp_properties, effMob, MT, RMT,
                productIons, compound_id, mass, formula, compound_name, cas_id, formula_type, compound_type,
                compound_status, charge_type, charge_number, "", "", "", "", "", 0, 0, 0, "", 0, "", "", "", "", 
                fahfa_id, oh_position, structure, pathways);
        effMob = null;
        MT = null;
        RMT = null;
        compound_id = 158492;
        mass = 182.08668;
        formula = "C6H12F2N2O2";
        formula_type = 4;

        compound_name = "Eflornithine";

        CEMSCompound cemscompound4 = new CEMSCompound(ce_exp_properties, effMob, MT, RMT,
                productIons, compound_id, mass, formula, compound_name, cas_id, formula_type, compound_type,
                compound_status, charge_type, charge_number, "", "", "", "", "", 0, 0, 0, "", 0, "", "", "", "", 
                fahfa_id, oh_position, structure, pathways);

        dbCandidates.add(cemscompound1);
        dbCandidates.add(cemscompound2);
        dbCandidates.add(cemscompound4);
        dbCandidates.add(cemscompound3);
        Integer chemAlphabet = 0;
        Set<CEMSCompound> expResult = new TreeSet();
        expResult.add(cemscompound1);
        expResult.add(cemscompound2);
        expResult.add(cemscompound3);
        Set<CEMSCompound> result = CEMSFacadeInMemory.filterIntegerFormulaTypeCEMSCompounds(dbCandidates, chemAlphabet);
        assertEquals(expResult, result);
    }

    /**
     * Test of filterMTCEMSCompounds method, of class CEMSFacadeInMemory.
     */
    @Test
    public void testFilterMTCEMSCompounds() {
        System.out.println("filterMTCEMSCompounds");
        Set<CEMSCompound> dbCandidates = new TreeSet();
        CE_Exp_properties.CE_EXP_PROP_ENUM ce_exp_properties = CE_Exp_properties.CE_EXP_PROP_ENUM.FORMIC_25_POS_DIR;
        Set<CEMSFragment> productIons = null;
        Double effMob = 2697.16533870139;
        Double MT = 7.87;
        Double RMT = 0.50;
        Integer compound_id = 180838;
        Double mass = 181.0409;
        String formula = "C5H11NO4S";
        String compound_name = "L-Methionine sulfone";
        String cas_id = null;
        Integer formula_type = 0;
        Integer compound_type = 0;
        Integer compound_status = 0;
        Integer charge_type = 0;
        Integer charge_number = 0;
        Structure structure = null;
        List<Pathway> pathways = null;
        Integer fahfa_id = 0;
        Integer oh_position = 0;
        CEMSCompound cemscompound1 = new CEMSCompound(ce_exp_properties, effMob, MT, RMT,
                productIons, compound_id, mass, formula, compound_name, cas_id, formula_type, compound_type,
                compound_status, charge_type, charge_number, "", "", "", "", "", 0, 0, 0, "", 0, "", "", "", "", 
                fahfa_id, oh_position, structure, pathways);

        effMob = -43.86;
        MT = 25.29;
        RMT = 1.61;
        compound_id = 91854;
        mass = 179.058243159;
        formula = "C9H9NO3 ";
        compound_name = "Hippuric acid";

        CEMSCompound cemscompound2 = new CEMSCompound(ce_exp_properties, effMob, MT, RMT,
                productIons, compound_id, mass, formula, compound_name, cas_id, formula_type, compound_type,
                compound_status, charge_type, charge_number, "", "", "", "", "", 0, 0, 0, "", 0, "", "", "", "", 
                fahfa_id, oh_position, structure, pathways);

        effMob = 1669.66;
        MT = 10.89;
        RMT = 0.70;
        compound_id = 46307;
        mass = 169.085126611;
        formula = "C7H11N3O2";
        compound_name = "3-Methylhistidine";

        CEMSCompound cemscompound3 = new CEMSCompound(ce_exp_properties, effMob, MT, RMT,
                productIons, compound_id, mass, formula, compound_name, cas_id, formula_type, compound_type,
                compound_status, charge_type, charge_number, "", "", "", "", "", 0, 0, 0, "", 0, "", "", "", "", 
                fahfa_id, oh_position, structure, pathways);
        effMob = null;
        MT = 1d;
        RMT = null;
        compound_id = 158492;
        mass = 182.08668;
        formula = "C6H12F2N2O2";
        formula_type = 4;

        compound_name = "Eflornithine";

        CEMSCompound cemscompound4 = new CEMSCompound(ce_exp_properties, effMob, MT, RMT,
                productIons, compound_id, mass, formula, compound_name, cas_id, formula_type, compound_type,
                compound_status, charge_type, charge_number, "", "", "", "", "", 0, 0, 0, "", 0, "", "", "", "", 
                fahfa_id, oh_position, structure, pathways);

        dbCandidates.add(cemscompound1);
        dbCandidates.add(cemscompound2);
        dbCandidates.add(cemscompound4);
        dbCandidates.add(cemscompound3);
        Integer chemAlphabet = 0;
        Double expMT = 10d;
        Double MTTolerance = 10d;
        String MTToleranceMode = "percentage";
        Set<CEMSCompound> expResult = new TreeSet();
        expResult.add(cemscompound3);

        Set<CEMSCompound> result = CEMSFacadeInMemory.filterMTCEMSCompounds(dbCandidates, expMT, MTTolerance, MTToleranceMode);
        assertEquals(expResult, result);
    }

    /**
     * Test of filterMassesCEMSCompounds method, of class CEMSFacadeInMemory.
     */
    @Test
    public void testFilterMassesCEMSCompounds() {
        System.out.println("filterMassesCEMSCompounds");
        Set<CEMSCompound> dbCandidates = new TreeSet();
        CE_Exp_properties.CE_EXP_PROP_ENUM ce_exp_properties = CE_Exp_properties.CE_EXP_PROP_ENUM.FORMIC_25_POS_DIR;
        Set<CEMSFragment> productIons = null;
        Double effMob = 2697.16533870139;
        Double MT = 7.87;
        Double RMT = 0.50;
        Integer compound_id = 180838;
        Double mass = 181.0409;
        String formula = "C5H11NO4S";
        String compound_name = "L-Methionine sulfone";
        String cas_id = null;
        Integer formula_type = 0;
        Integer compound_type = 0;
        Integer compound_status = 0;
        Integer charge_type = 0;
        Integer charge_number = 0;
        Structure structure = null;
        List<Pathway> pathways = null;
        Integer fahfa_id = 0;
        Integer oh_position = 0;
        CEMSCompound cemscompound1 = new CEMSCompound(ce_exp_properties, effMob, MT, RMT,
                productIons, compound_id, mass, formula, compound_name, cas_id, formula_type, compound_type,
                compound_status, charge_type, charge_number, "", "", "", "", "", 0, 0, 0, "", 0, "", "", "", "", 
                fahfa_id, oh_position, structure, pathways);

        effMob = -43.86;
        MT = 25.29;
        RMT = 1.61;
        compound_id = 91854;
        mass = 179.058243159;
        formula = "C9H9NO3 ";
        compound_name = "Hippuric acid";

        CEMSCompound cemscompound2 = new CEMSCompound(ce_exp_properties, effMob, MT, RMT,
                productIons, compound_id, mass, formula, compound_name, cas_id, formula_type, compound_type,
                compound_status, charge_type, charge_number, "", "", "", "", "", 0, 0, 0, "", 0, "", "", "", "", 
                fahfa_id, oh_position, structure, pathways);

        effMob = 1669.66;
        MT = 10.89;
        RMT = 0.70;
        compound_id = 46307;
        mass = 169.085126611;
        formula = "C7H11N3O2";
        compound_name = "3-Methylhistidine";

        CEMSCompound cemscompound3 = new CEMSCompound(ce_exp_properties, effMob, MT, RMT,
                productIons, compound_id, mass, formula, compound_name, cas_id, formula_type, compound_type,
                compound_status, charge_type, charge_number, "", "", "", "", "", 0, 0, 0, "", 0, "", "", "", "", 
                fahfa_id, oh_position, structure, pathways);
        effMob = null;
        MT = 1d;
        RMT = null;
        compound_id = 158492;
        mass = 182.08668;
        formula = "C6H12F2N2O2";
        formula_type = 4;

        compound_name = "Eflornithine";

        CEMSCompound cemscompound4 = new CEMSCompound(ce_exp_properties, effMob, MT, RMT,
                productIons, compound_id, mass, formula, compound_name, cas_id, formula_type, compound_type,
                compound_status, charge_type, charge_number, "", "", "", "", "", 0, 0, 0, "", 0, "", "", "", "", 
                fahfa_id, oh_position, structure, pathways);

        dbCandidates.add(cemscompound1);
        dbCandidates.add(cemscompound2);
        dbCandidates.add(cemscompound4);
        dbCandidates.add(cemscompound3);
        Integer chemAlphabet = 0;
        Double massToSearch = 182.087;
        Double mzTolerance = 10d;
        Integer mzToleranceMode = 0; // ppm
        Set<CEMSCompound> expResult = new TreeSet();
        expResult.add(cemscompound4);
        Set<CEMSCompound> result = CEMSFacadeInMemory.filterMassesCEMSCompounds(dbCandidates, massToSearch, mzTolerance, mzToleranceMode);
        assertEquals(expResult, result);
    }

}
