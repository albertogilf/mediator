/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import CEMS.CEMSCompound;
import CEMS.CEMSFeature;
import CEMS.CEMSFragment;
import LCMS_FEATURE.CompoundCCS;
import LCMS_FEATURE.CompoundLCMS;
import LCMS_FEATURE.CompoundsLCMSGroupByAdduct;
import LCMS_FEATURE.Feature;
import LCMS_FEATURE.FeatureCCS;
import compound.CMMCompound;
import compound.Chain;
import compound.Classyfire_Classification;
import compound.LM_Classification;
import compound.Lipids_Classification;
import compound.Structure;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import msms.MSMSCompound;
import msms.Peak;
import static org.apache.activemq.util.ThreadTracker.result;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import pathway.Pathway;
import services.rest.api.response.ClassyfireNodeView;
import services.rest.api.response.CompoundView;
import services.rest.api.response.OrganismView;
import services.rest.api.response.ReferenceView;
import services.rest.api.response.TermView;

/**
 * 
 * @author alberto
 */
@RunWith(value = Parameterized.class)
public class MSFacadeTest {
    
    @Parameters
    public static Iterable<Object[]> getData(){
       List<Object[]> obj = new ArrayList<>();
       List<String> searchedDatabases = new LinkedList<>();
        searchedDatabases.add("All");
        
        // m/z; averag expCCS; RT; adduct; ionizationMode; DBs; compound_Id
        //Los valores de aducto e ion mode deben ser coherentes.
        //obj.add(new Object[] {400.3432d, 202.881d, 0d, "M+H", 1, searchedDatabases, 32600});//prueba
       
        // NEGATIVELY IONIZED OXYLIPINS:
        /*
		obj.add(new Object[] {343.2269d, 189.2d, 14.00d, "M-H", -1, searchedDatabases, 29349});
		obj.add(new Object[] {317.2124d, 182.1d, 13.19d, "M-H", -1, searchedDatabases, 28222});
		obj.add(new Object[] {319.2269d, 182.7d, 14.16d, "M-H", -1, searchedDatabases, 28698});
		obj.add(new Object[] {337.2384d, 185.1d, 11.44d, "M-H", -1, searchedDatabases, 28526});
		obj.add(new Object[] {335.2228d, 184.64d, 10.64d, "M-H", -1, searchedDatabases, 28605});
		obj.add(new Object[] {319.2260d, 183.8d, 12.96d, "M-H", -1, searchedDatabases, 28611});
		obj.add(new Object[] {295.2401d, 177.9d, 13.81d, "M-H", -1, searchedDatabases, 27686});
		obj.add(new Object[] {313.2376d, 181.9d, 10.52d, "M-H", -1, searchedDatabases, 27877});
		obj.add(new Object[] {317.2086d, 181.0d, 12.21d, "M-H", -1, searchedDatabases, 28658});
		obj.add(new Object[] {319.2279d, 182.2d, 13.16d, "M-H", -1, searchedDatabases, 28614});
		obj.add(new Object[] {317.2117d, 183.6d, 13.35d, "M-H", -1, searchedDatabases, 28561});
		obj.add(new Object[] {343.2243d, 189.9d, 13.94d, "M-H", -1, searchedDatabases, 29350});
		obj.add(new Object[] {295.2399d, 179.2d, 12.53d, "M-H", -1, searchedDatabases, 27875});
		obj.add(new Object[] {293.2279d, 177.9d, 11.72d, "M-H", -1, searchedDatabases, 27699});
		obj.add(new Object[] {293.2279d, 177.0d, 12.89d, "M-H", -1, searchedDatabases, 27669});
		obj.add(new Object[] {317.2107d, 183.8d, 13.12d, "M-H", -1, searchedDatabases, 28223});
		obj.add(new Object[] {319.2260d, 184.5d, 13.84d, "M-H", -1, searchedDatabases, 28678});
		obj.add(new Object[] {335.2207d, 186.0d, 10.30d, "M-H", -1, searchedDatabases, 28603});
		obj.add(new Object[] {337.2366d, 188.1d, 11.07d, "M-H", -1, searchedDatabases, 28527});
		obj.add(new Object[] {321.2421d, 186.2d, 13.30d, "M-H", -1, searchedDatabases, 28525});
		obj.add(new Object[] {315.1958d, 186.2d, 11.95d, "M-H", -1, searchedDatabases, 28253});
		obj.add(new Object[] {317.2108d, 183.6d, 12.01d, "M-H", -1, searchedDatabases, 28659});
		obj.add(new Object[] {319.2282d, 185.4d, 12.78d, "M-H", -1, searchedDatabases, 28613});
		obj.add(new Object[] {317.2105d, 183.6d, 13.08d, "M-H", -1, searchedDatabases, 28578});
		obj.add(new Object[] {343.2260d, 190.9d, 13.89d, "M-H", -1, searchedDatabases, 29351});
		obj.add(new Object[] {361.2387d, 192.7d, 11.326d, "M-H", -1, searchedDatabases, 29356});
		obj.add(new Object[] {317.2124d, 183.2d, 12.87d, "M-H", -1, searchedDatabases, 28224});
		obj.add(new Object[] {335.2218d, 184.6d, 9.98d, "M-H", -1, searchedDatabases, 28604});
		obj.add(new Object[] {343.2274d, 190.8d, 12.79d, "M-H", -1, searchedDatabases, 29346});
		obj.add(new Object[] {343.2278d, 190.4d, 13.67d, "M-H", -1, searchedDatabases, 29352});
		obj.add(new Object[] {361.2385d, 191.6d, 11.057d, "M-H", -1, searchedDatabases, 29357});
		obj.add(new Object[] {365.1936d, 187.9d, 4.71d, "M-H", -1, searchedDatabases, 28439});
		obj.add(new Object[] {319.2281d, 183.7d, 12.02d, "M-H", -1, searchedDatabases, 28553});
		obj.add(new Object[] {351.2158d, 184.2d, 5.01d, "M-H", -1, searchedDatabases, 28441});
		obj.add(new Object[] {319.2275d, 183.8d, 14.44d, "M-H", -1, searchedDatabases, 28675});
		obj.add(new Object[] {335.2217d, 187.0d, 9.86d, "M-H", -1, searchedDatabases, 28631});
		obj.add(new Object[] {337.2383d, 186.0d, 12.11d, "M-H", -1, searchedDatabases, 28522});
		obj.add(new Object[] {337.2384d, 185.3d, 12.129d, "M-H", -1, searchedDatabases, 28522});
		obj.add(new Object[] {317.2128d, 184.1d, 12.42d, "M-H", -1, searchedDatabases, 28654});
		obj.add(new Object[] {319.2276d, 184.8d, 13.42d, "M-H", -1, searchedDatabases, 28610});
		obj.add(new Object[] {317.2125d, 208.5d, 14.05d, "M-H", -1, searchedDatabases, 28555});
		obj.add(new Object[] {317.2133d, 184.8d, 14.05d, "M-H", -1, searchedDatabases, 28555});
		obj.add(new Object[] {317.2126d, 199.1d, 14.05d, "M-H", -1, searchedDatabases, 28555});
		obj.add(new Object[] {369.2255d, 194.1d, 4.72d, "M-H", -1, searchedDatabases, 28236});
		obj.add(new Object[] {335.2184d, 184.4d, 9.84d, "M-H", -1, searchedDatabases, 28436});
		obj.add(new Object[] {343.2274d, 188.7d, 14.15d, "M-H", -1, searchedDatabases, 29348});
		obj.add(new Object[] {317.2128d, 182.0d, 13.27d, "M-H", -1, searchedDatabases, 28221});
		obj.add(new Object[] {319.2275d, 182.5d, 14.29d, "M-H", -1, searchedDatabases, 28676});
		obj.add(new Object[] {335.2211d, 187.2d, 9.63d, "M-H", -1, searchedDatabases, 28633});
		obj.add(new Object[] {337.2384d, 185.5d, 11.73d, "M-H", -1, searchedDatabases, 28524});
		obj.add(new Object[] {337.2377d, 185.8d, 11.71d, "M-H", -1, searchedDatabases, 28602});
		obj.add(new Object[] {317.2122d, 183.0d, 12.13d, "M-H", -1, searchedDatabases, 28655});
		obj.add(new Object[] {319.2267d, 183.2d, 13.14d, "M-H", -1, searchedDatabases, 28612});
		obj.add(new Object[] {295.2417d, 176.9d, 14.02d, "M-H", -1, searchedDatabases, 27685});
		obj.add(new Object[] {329.2307d, 185.3d, 6.72d, "M-H", -1, searchedDatabases, 27815});
		obj.add(new Object[] {313.2388d, 178.9d, 10.75d, "M-H", -1, searchedDatabases, 27876});
		obj.add(new Object[] {329.2325d, 183.3d, 6.61d, "M-H", -1, searchedDatabases, 27667});
		obj.add(new Object[] {319.2281d, 181.4d, 13.29d, "M-H", -1, searchedDatabases, 28615});
		obj.add(new Object[] {295.2401d, 177.8d, 12.56d, "M-H", -1, searchedDatabases, 27798});
		obj.add(new Object[] {295.2393d, 191.4d, 12.56d, "M-H", -1, searchedDatabases, 27798});
		obj.add(new Object[] {293.2277d, 178.1d, 11.54d, "M-H", -1, searchedDatabases, 27676});
		obj.add(new Object[] {293.2281d, 177.1d, 13.12d, "M-H", -1, searchedDatabases, 27898});
		obj.add(new Object[] {293.2276d, 187.8d, 13.12d, "M-H", -1, searchedDatabases, 27898});
		obj.add(new Object[] {337.2363d, 188.1d, 11.11d, "M-H", -1, searchedDatabases, 301971});
		obj.add(new Object[] {335.2215d, 184.2d, 10.11d, "M-H", -1, searchedDatabases, 28426});
		obj.add(new Object[] {624.2885d, 243.0d, 8.73d, "M-H", -1, searchedDatabases, 28428});
		obj.add(new Object[] {495.2474d, 223.5d, 7.31d, "M-H", -1, searchedDatabases, 28430});
		obj.add(new Object[] {438.2272d, 211.2d, 8.65d, "M-H", -1, searchedDatabases, 28427});
		obj.add(new Object[] {351.2154d, 190.7d, 7.84d, "M-H", -1, searchedDatabases, 28510});
		obj.add(new Object[] {333.2047d, 185.2d, 9.12d, "M-H", -1, searchedDatabases, 28250});
		obj.add(new Object[] {353.2309d, 188.6d, 7.33d, "M-H", -1, searchedDatabases, 28275});
		obj.add(new Object[] {351.2150d, 186.7d, 7.36d, "M-H", -1, searchedDatabases, 28239});
		obj.add(new Object[] {349.1987d, 184.3d, 5.93d, "M-H", -1, searchedDatabases, 28349});
		obj.add(new Object[] {353.2300d, 190.2d, 7.20d, "M-H", -1, searchedDatabases, 28342});
		obj.add(new Object[] {351.2154d, 188.1d, 7.01d, "M-H", -1, searchedDatabases, 28238});
		obj.add(new Object[] {349.2006d, 184.5d, 6.10d, "M-H", -1, searchedDatabases, 28343});
		obj.add(new Object[] {353.2314d, 190.5d, 6.73d, "M-H", -1, searchedDatabases, 28237});
		obj.add(new Object[] {333.2046d, 188.2d, 9.08d, "M-H", -1, searchedDatabases, 28251});
		obj.add(new Object[] {349.2002d, 183.6d, 4.74d, "M-H", -1, searchedDatabases, 29323});
		obj.add(new Object[] {369.2240d, 191.8d, 6.04d, "M-H", -1, searchedDatabases, 28496});
		obj.add(new Object[] {271.2284d, 171.6d, 15.73d, "M-H", -1, searchedDatabases, 26726});
		obj.add(new Object[] {311.22d, 177.84d, 8.38d, "M-H", -1, searchedDatabases, 27712});
		*/
		//POSITIVELY IONIZED OXYLIPINS:
                
		obj.add(new Object[] {345.2421d, 186.2d, 14.00d, "M+H", 1, searchedDatabases, 29349});
		obj.add(new Object[] {319.2266d, 180.0d, 13.19d, "M+H", 1, searchedDatabases, 28222});
		obj.add(new Object[] {321.2423d, 185.2d, 14.16d, "M+H", 1, searchedDatabases, 28698});
		obj.add(new Object[] {337.2341d, 188.8d, 10.64d, "M+H", 1, searchedDatabases, 28605});
		obj.add(new Object[] {297.2422d, 176.2d, 13.81d, "M+H", 1, searchedDatabases, 27686});
		obj.add(new Object[] {315.2528d, 179.8d, 10.52d, "M+H", 1, searchedDatabases, 27877});
		obj.add(new Object[] {319.2264d, 190.1d, 13.35d, "M+H", 1, searchedDatabases, 28561});
		obj.add(new Object[] {345.2423d, 186.8d, 13.94d, "M+H", 1, searchedDatabases, 29350});
		obj.add(new Object[] {295.2266d, 177.0d, 12.89d, "M+H", 1, searchedDatabases, 27669});
		obj.add(new Object[] {319.2265d, 179.4d, 13.12d, "M+H", 1, searchedDatabases, 28223});
		obj.add(new Object[] {321.2424d, 180.9d, 13.84d, "M+H", 1, searchedDatabases, 28678});
		obj.add(new Object[] {337.2367d, 182.0d, 10.30d, "M+H", 1, searchedDatabases, 28603});
		obj.add(new Object[] {339.2527d, 185.2d, 11.07d, "M+H", 1, searchedDatabases, 28527});
		obj.add(new Object[] {317.2119d, 180.4d, 11.95d, "M+H", 1, searchedDatabases, 28253});
		obj.add(new Object[] {319.2255d, 181.3d, 13.08d, "M+H", 1, searchedDatabases, 28578});
		obj.add(new Object[] {345.2422d, 186.1d, 13.89d, "M+H", 1, searchedDatabases, 29351});
		obj.add(new Object[] {363.2526d, 188.6d, 11.326d, "M+H", 1, searchedDatabases, 29356});
		obj.add(new Object[] {319.2266d, 176.9d, 12.87d, "M+H", 1, searchedDatabases, 28224});
		obj.add(new Object[] {337.2368d, 180.5d, 9.98d, "M+H", 1, searchedDatabases, 28604});
		obj.add(new Object[] {345.2419d, 183.6d, 13.67d, "M+H", 1, searchedDatabases, 29352});
		obj.add(new Object[] {363.2523d, 187.5d, 11.057d, "M+H", 1, searchedDatabases, 29357});
		obj.add(new Object[] {321.2398d, 178.3d, 12.02d, "M+H", 1, searchedDatabases, 28553});
		obj.add(new Object[] {321.2419d, 183.9d, 14.44d, "M+H", 1, searchedDatabases, 28675});
		obj.add(new Object[] {339.2525d, 190.0d, 12.11d, "M+H", 1, searchedDatabases, 28522});
		obj.add(new Object[] {319.2256d, 186.8d, 14.05d, "M+H", 1, searchedDatabases, 28555});
		obj.add(new Object[] {345.2419d, 186.7d, 14.15d, "M+H", 1, searchedDatabases, 29348});
		obj.add(new Object[] {319.2262d, 180.1d, 13.27d, "M+H", 1, searchedDatabases, 28221});
		obj.add(new Object[] {321.2419d, 186.4d, 14.29d, "M+H", 1, searchedDatabases, 28676});
		obj.add(new Object[] {339.2525d, 187.0d, 11.71d, "M+H", 1, searchedDatabases, 28602});
		obj.add(new Object[] {297.2420d, 179.1d, 14.02d, "M+H", 1, searchedDatabases, 27685});
		obj.add(new Object[] {315.2523d, 181.1d, 10.75d, "M+H", 1, searchedDatabases, 27876});
		obj.add(new Object[] {295.2265d, 182.6d, 13.12d, "M+H", 1, searchedDatabases, 27898});
		obj.add(new Object[] {626.3076d, 255.0d, 8.73d, "M+H", 1, searchedDatabases, 28428});
		obj.add(new Object[] {497.2664d, 232.4d, 7.31d, "M+H", 1, searchedDatabases, 28430});
		obj.add(new Object[] {497.2675d, 224.8d, 7.31d, "M+H", 1, searchedDatabases, 28430});
		obj.add(new Object[] {497.2675d, 211.2d, 7.31d, "M+H", 1, searchedDatabases, 28430});
		obj.add(new Object[] {440.2461d, 211.5d, 8.65d, "M+H", 1, searchedDatabases, 28427});
		obj.add(new Object[] {440.2462d, 204.3d, 8.65d, "M+H", 1, searchedDatabases, 28427});
		obj.add(new Object[] {440.2460d, 221.2d, 8.65d, "M+H", 1, searchedDatabases, 28427});
		obj.add(new Object[] {335.2198d, 193.7d, 9.12d, "M+H", 1, searchedDatabases, 28250});
		obj.add(new Object[] {335.2198d, 182.8d, 9.12d, "M+H", 1, searchedDatabases, 28250});
		obj.add(new Object[] {351.2163d, 185.4d, 5.93d, "M+H", 1, searchedDatabases, 28349});
		
       
       return obj;
    }
    
    //Data of the expected compound, which concurs with the data to annotate.  
        List<Double> masses;
        List<Double> expCCSs;
        List<Double> RTs;
        List<String> adducts ;
        
        Integer toleranceMode ;
        Double tolerance;
        Integer toleranceCCS;
        Integer chemAlphabet;
        Integer ionMode;
        List<String> databases;
        
        String metabolitesType;
        
        int compound_id;
        
        Double expRT;
        
        String formula;
        String compound_name;
        String cas_id;
        int formula_type;
        int compound_type;
        int compound_status;
        int charge_type;
        int charge_number;
        String lm_id; 
        String kegg_id; 
        String hmdb_id; 
        String metlin_id; 
        String in_house_id; 
        Integer pc_id; 
        Integer chebi_id;
        Integer MINE_id;
        String knapsack_id; 
        Integer npatlas_id;
        String aspergillus_id; 
        String mesh_nomenclature; 
        String iupac_classification; 
        String aspergillus_web_name; 
        Integer fahfa_id;
        Integer oh_position;
            
        Structure structure;
        LM_Classification lm_classification;
        List<Classyfire_Classification> cclist;
        Lipids_Classification lipids_classification;
        List<Pathway> pwlist;
        
        MSFacade instance;
    
 
    /*Construrctor para compuestos con datos de RT*/
    public MSFacadeTest(Double m, Double ccs, Double rt, String adduct,
            int ionM, List<String> db,
            int compId) {
        
        //masses.add(m);
        masses= new LinkedList<>();
        masses.add(m);
        expCCSs= new LinkedList<>();
        expCCSs.add(ccs);
        RTs= new LinkedList<>();
        RTs.add(rt);
        adducts= new LinkedList<>();
        adducts.add(adduct);
        
        toleranceMode = 0;
        tolerance = 10d;
        toleranceCCS = 3;
        chemAlphabet = 0;
        ionMode = ionM;
        databases= new LinkedList<>();
        databases.addAll(db);
        
        metabolitesType = "all";
        
        compound_id = compId;
        
        expRT=rt;
        
        formula = "";
        compound_name="";
        cas_id="";
        formula_type=0;
        compound_type=0;
        compound_status=0;
        charge_type=0;
        charge_number=0;
        lm_id=""; 
        kegg_id=""; 
        hmdb_id=""; 
        metlin_id=""; 
        in_house_id=""; 
        pc_id=0; 
        chebi_id=0;
        MINE_id=0;
        knapsack_id=""; 
        npatlas_id=0;
        aspergillus_id=""; 
        mesh_nomenclature=""; 
        iupac_classification=""; 
        aspergillus_web_name=""; 
        fahfa_id=0;
        oh_position=0;
            
        structure = new Structure("","","");
        lm_classification = new LM_Classification("","","","");
        cclist= new LinkedList<>();
        Classyfire_Classification cc = new Classyfire_Classification("","","","","");
        cclist.add(cc);
        lipids_classification = new Lipids_Classification("", 0, 
            0, 0);
        pwlist= new LinkedList<>();
        Pathway pw = null;
        pwlist.add(pw);
    }
    
        
    @BeforeClass
    public static void setUpClass() {
        File archivoResultados = new File("/home/alberto/Desktop/PositiveOxylipins10ppm.txt");
        try {
            archivoResultados.createNewFile();
        
            

        } catch (IOException ex) {
            System.out.println("Error occured while creating file...");
        }
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before //Antes de realizar cada test, se ejecuta este método.
    public void setUp() {
        instance = new MSFacade();
    }
    
    @After
    public void tearDown() {
    }

    
    /**
     * Test of findCompoundsCCSPred method, of class MSFacade.
     */
    @Test
    public void testFindCompoundsCCSPred() {
        System.out.println("Test for findCompoundsCCSPred\n");
     
               
        //EXPECTED CompoundCCS: expectedCompound.-----------------------------------------
        CompoundCCS expectedCompound = new CompoundCCS(masses.get(0), expRT, new TreeMap<>(), expCCSs.get(0), adducts.get(0),
            ionMode, compound_id, masses.get(0),
            formula, compound_name, cas_id, formula_type, compound_type, compound_status,
            charge_type, charge_number, expCCSs.get(0), lm_id, 
            kegg_id, hmdb_id, metlin_id, in_house_id, pc_id, chebi_id, MINE_id,
            knapsack_id, npatlas_id,
            aspergillus_id, mesh_nomenclature, iupac_classification, aspergillus_web_name,
            fahfa_id, oh_position,
            structure,
            lm_classification, cclist,
            lipids_classification, pwlist);
        
        
        
        //OBTAINED CompoundCCS: resultCompound.-------------------------------------------
        List<FeatureCCS> resultFeature = instance.findCompoundsCCSPred(masses, expCCSs, RTs, toleranceMode, tolerance, toleranceCCS, adducts, chemAlphabet, ionMode, databases, metabolitesType);
        /** Se extraen los resultados de cada Feature (objetos "FeatureCCS") 
        de la lista de la lista de Features "resultFeature". Como solo se introdujo
        una feature, solo hay un "FeatureCCS"**/
        List<CompoundsLCMSGroupByAdduct> GroupedResultsByAdduct;
        GroupedResultsByAdduct = (resultFeature.get(0)).getAnnotationsGroupedByAdduct();
        
        int totalCompoundsRetrieved = 0;
        boolean coincidencia = false;
        float TP = 0f;//TRue positive. EStá inicializada para evitar errores.
        float FP;//False positive
        float FN = 0f;//False negative. EStá inicializada para evitar errores.
        
        /* Se recorren los compounds obtenidos*/
        for (CompoundsLCMSGroupByAdduct group : GroupedResultsByAdduct){
            totalCompoundsRetrieved = totalCompoundsRetrieved + group.getNumberAnnotations();
            TP=0f;//TRue positive
            FN=1f;//False negative
            List<CompoundCCS> listResults = group.getCompounds();
            for (CompoundCCS annotation : listResults){
                if ((annotation.getCompound_id()).equals(expectedCompound.getCompound_id())) {
                    System.out.println("Matching compound found");
                    coincidencia = true;
                    TP = 1f;
                    FN=0f;
                }
            }                
        }
        
        
        FP = totalCompoundsRetrieved - TP;
        
        float precision;
        if (totalCompoundsRetrieved==0f){
            precision = 0;
        } else {
            precision = TP/(TP+FP);
        }
        float recall = TP/(TP+FN);
        
        System.out.println("Total compounds retrieved from database for feature "+compound_id+": "+totalCompoundsRetrieved);
        try{
            FileWriter fw = new FileWriter("/home/alberto/Desktop/PositiveOxylipins10ppm.txt", true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter pw = new PrintWriter(bw);
            
            pw.println("For feature of compound with id "+compound_id+": ");
            pw.println(totalCompoundsRetrieved+" compounds retrieved.");
            if (coincidencia) {
                pw.println("Match found");
            } else {
                pw.println("Match NOT found");
            }
            
                       
            pw.println("Precision: "+precision);
            pw.println("Recall: "+recall+"\n");
            
            pw.close();
            bw.close();
            fw.close();
            
        } catch (IOException ex) {
            System.out.println("Error occured while writing file...");
        }
        
        //assertEquals(resultCompound.getCompound_id(), expectedCompound.getCompound_id());
        //assertEquals(resultCompound, expectedCompound);
        
        assertTrue(coincidencia);//--> se pasa el test si condition True
    }
       
 }

