/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

import java.util.List;
import java.util.Map;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.junit.Test;
import static org.junit.Assert.*;
import persistence.theoreticalCompound.TheoreticalCompounds;
import persistence.theoreticalGroup.TheoreticalCompoundsGroup;

/**
 *
 * @author alberto.gildelafuent
 */
public class TheoreticalCompoundsRTControllerTest {
    
    public TheoreticalCompoundsRTControllerTest() {
    }

    /**
     * Test of setAdvancedDemoRTMasses method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testSetAdvancedDemoRTMasses() {
        System.out.println("setAdvancedDemoRTMasses");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.setAdvancedDemoRTMasses();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of clearForm method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testClearForm() {
        System.out.println("clearForm");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.clearForm();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of exportToExcel method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testExportToExcel() {
        System.out.println("exportToExcel");
        int flag = 0;
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.exportToExcel(flag);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of submitCompoundsRTPred method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testSubmitCompoundsRTPred() {
        System.out.println("submitCompoundsRTPred");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.submitCompoundsRTPred();
    }

    /**
     * Test of getQueryInputCompositeSpectra method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testGetQueryInputCompositeSpectra() {
        System.out.println("getQueryInputCompositeSpectra");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        String expResult = "";
        String result = instance.getQueryInputCompositeSpectra();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setQueryInputCompositeSpectra method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testSetQueryInputCompositeSpectra() {
        System.out.println("setQueryInputCompositeSpectra");
        String queryInputCompositeSpectra = "";
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.setQueryInputCompositeSpectra(queryInputCompositeSpectra);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getQueryInputMasses method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testGetQueryInputMasses() {
        System.out.println("getQueryInputMasses");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        String expResult = "";
        String result = instance.getQueryInputMasses();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setQueryInputMasses method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testSetQueryInputMasses() {
        System.out.println("setQueryInputMasses");
        String queryInputMasses = "";
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.setQueryInputMasses(queryInputMasses);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getInputCompoundRefStds method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testGetInputCompoundRefStds() {
        System.out.println("getInputCompoundRefStds");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        String expResult = "";
        String result = instance.getInputCompoundRefStds();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setInputCompoundRefStds method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testSetInputCompoundRefStds() {
        System.out.println("setInputCompoundRefStds");
        String inputCompoundRefStds = "";
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.setInputCompoundRefStds(inputCompoundRefStds);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getInputRefStdsRTs method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testGetInputRefStdsRTs() {
        System.out.println("getInputRefStdsRTs");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        String expResult = "";
        String result = instance.getInputRefStdsRTs();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setInputRefStdsRTs method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testSetInputRefStdsRTs() {
        System.out.println("setInputRefStdsRTs");
        String inputRefStdsRTs = "";
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.setInputRefStdsRTs(inputRefStdsRTs);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getQueryInputRetentionTimes method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testGetQueryInputRetentionTimes() {
        System.out.println("getQueryInputRetentionTimes");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        String expResult = "";
        String result = instance.getQueryInputRetentionTimes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setQueryInputRetentionTimes method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testSetQueryInputRetentionTimes() {
        System.out.println("setQueryInputRetentionTimes");
        String queryInputRetentionTimes = "";
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.setQueryInputRetentionTimes(queryInputRetentionTimes);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getInputTolerance method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testGetInputTolerance() {
        System.out.println("getInputTolerance");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        String expResult = "";
        String result = instance.getInputTolerance();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setInputTolerance method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testSetInputTolerance() {
        System.out.println("setInputTolerance");
        String inputTolerance = "";
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.setInputTolerance(inputTolerance);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getInputRTTolerance method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testGetInputRTTolerance() {
        System.out.println("getInputRTTolerance");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        String expResult = "";
        String result = instance.getInputRTTolerance();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setInputRTTolerance method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testSetInputRTTolerance() {
        System.out.println("setInputRTTolerance");
        String inputRTTolerance = "";
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.setInputRTTolerance(inputRTTolerance);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getInputModeTolerance method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testGetInputModeTolerance() {
        System.out.println("getInputModeTolerance");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        String expResult = "";
        String result = instance.getInputModeTolerance();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setInputModeTolerance method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testSetInputModeTolerance() {
        System.out.println("setInputModeTolerance");
        String inputModeTolerance = "";
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.setInputModeTolerance(inputModeTolerance);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getQueryMasses method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testGetQueryMasses() {
        System.out.println("getQueryMasses");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        List<Double> expResult = null;
        List<Double> result = instance.getQueryMasses();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setQueryMasses method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testSetQueryMasses() {
        System.out.println("setQueryMasses");
        List<Double> queryMasses = null;
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.setQueryMasses(queryMasses);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getQueryRetentionTimes method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testGetQueryRetentionTimes() {
        System.out.println("getQueryRetentionTimes");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        List<Double> expResult = null;
        List<Double> result = instance.getQueryRetentionTimes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setQueryRetentionTimes method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testSetQueryRetentionTimes() {
        System.out.println("setQueryRetentionTimes");
        List<Double> queryRetentionTimes = null;
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.setQueryRetentionTimes(queryRetentionTimes);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getQueryCompositeSpectrum method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testGetQueryCompositeSpectrum() {
        System.out.println("getQueryCompositeSpectrum");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        List<Map<Double, Double>> expResult = null;
        List<Map<Double, Double>> result = instance.getQueryCompositeSpectrum();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setQueryCompositeSpectrum method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testSetQueryCompositeSpectrum() {
        System.out.println("setQueryCompositeSpectrum");
        List<Map<Double, Double>> queryCompositeSpectrum = null;
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.setQueryCompositeSpectrum(queryCompositeSpectrum);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getItems method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testGetItems() {
        System.out.println("getItems");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        List<TheoreticalCompounds> expResult = null;
        List<TheoreticalCompounds> result = instance.getItems();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getItemsGrouped method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testGetItemsGrouped() {
        System.out.println("getItemsGrouped");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        List<TheoreticalCompoundsGroup> expResult = null;
        List<TheoreticalCompoundsGroup> result = instance.getItemsGrouped();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setItems method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testSetItems() {
        System.out.println("setItems");
        List<TheoreticalCompounds> items = null;
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.setItems(items);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setItemsGrouped method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testSetItemsGrouped() {
        System.out.println("setItemsGrouped");
        List<TheoreticalCompoundsGroup> itemsGrouped = null;
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.setItemsGrouped(itemsGrouped);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of isThereTheoreticalCompounds method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testIsThereTheoreticalCompounds() {
        System.out.println("isThereTheoreticalCompounds");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        boolean expResult = false;
        boolean result = instance.isThereTheoreticalCompounds();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of isThereInputMasses method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testIsThereInputMasses() {
        System.out.println("isThereInputMasses");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        boolean expResult = false;
        boolean result = instance.isThereInputMasses();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getChemAlphabet method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testGetChemAlphabet() {
        System.out.println("getChemAlphabet");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        String expResult = "";
        String result = instance.getChemAlphabet();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setChemAlphabet method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testSetChemAlphabet() {
        System.out.println("setChemAlphabet");
        String chemAlphabet = "";
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.setChemAlphabet(chemAlphabet);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of isIncludeDeuterium method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testIsIncludeDeuterium() {
        System.out.println("isIncludeDeuterium");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        boolean expResult = false;
        boolean result = instance.isIncludeDeuterium();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setIncludeDeuterium method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testSetIncludeDeuterium() {
        System.out.println("setIncludeDeuterium");
        boolean includeDeuterium = false;
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.setIncludeDeuterium(includeDeuterium);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMassesMode method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testGetMassesMode() {
        System.out.println("getMassesMode");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        String expResult = "";
        String result = instance.getMassesMode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setMassesMode method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testSetMassesMode() {
        System.out.println("setMassesMode");
        String massesMode = "";
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.setMassesMode(massesMode);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getIonMode method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testGetIonMode() {
        System.out.println("getIonMode");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        int expResult = 0;
        int result = instance.getIonMode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setIonMode method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testSetIonMode() {
        System.out.println("setIonMode");
        int ionMode = 0;
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.setIonMode(ionMode);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDatabases method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testGetDatabases() {
        System.out.println("getDatabases");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        List<String> expResult = null;
        List<String> result = instance.getDatabases();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDatabases method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testSetDatabases() {
        System.out.println("setDatabases");
        List<String> databases = null;
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.setDatabases(databases);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getModifier method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testGetModifier() {
        System.out.println("getModifier");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        String expResult = "";
        String result = instance.getModifier();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setModifier method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testSetModifier() {
        System.out.println("setModifier");
        String modifier = "";
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.setModifier(modifier);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getModifierCandidates method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testGetModifierCandidates() {
        System.out.println("getModifierCandidates");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        List<SelectItem> expResult = null;
        List<SelectItem> result = instance.getModifierCandidates();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setModifierCandidates method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testSetModifierCandidates() {
        System.out.println("setModifierCandidates");
        List<SelectItem> modifierCandidates = null;
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.setModifierCandidates(modifierCandidates);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMetabolitesType method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testGetMetabolitesType() {
        System.out.println("getMetabolitesType");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        String expResult = "";
        String result = instance.getMetabolitesType();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setMetabolitesType method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testSetMetabolitesType() {
        System.out.println("setMetabolitesType");
        String metabolitesType = "";
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.setMetabolitesType(metabolitesType);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMetabolitesTypecandidates method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testGetMetabolitesTypecandidates() {
        System.out.println("getMetabolitesTypecandidates");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        List<SelectItem> expResult = null;
        List<SelectItem> result = instance.getMetabolitesTypecandidates();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAdducts method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testGetAdducts() {
        System.out.println("getAdducts");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        List<String> expResult = null;
        List<String> result = instance.getAdducts();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAdducts method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testSetAdducts() {
        System.out.println("setAdducts");
        List<String> adducts = null;
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.setAdducts(adducts);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNumAdducts method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testGetNumAdducts() {
        System.out.println("getNumAdducts");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        int expResult = 0;
        int result = instance.getNumAdducts();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAdductsCandidates method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testGetAdductsCandidates() {
        System.out.println("getAdductsCandidates");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        List<SelectItem> expResult = null;
        List<SelectItem> result = instance.getAdductsCandidates();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAdductsCandidates method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testSetAdductsCandidates() {
        System.out.println("setAdductsCandidates");
        List<SelectItem> adductsCandidates = null;
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.setAdductsCandidates(adductsCandidates);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDBcandidates method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testGetDBcandidates() {
        System.out.println("getDBcandidates");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        List<SelectItem> expResult = null;
        List<SelectItem> result = instance.getDBcandidates();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getIonizationModeCandidates method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testGetIonizationModeCandidates() {
        System.out.println("getIonizationModeCandidates");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        List<SelectItem> expResult = null;
        List<SelectItem> result = instance.getIonizationModeCandidates();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setIonizationModeCandidates method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testSetIonizationModeCandidates() {
        System.out.println("setIonizationModeCandidates");
        List<SelectItem> ionizationModeCandidates = null;
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.setIonizationModeCandidates(ionizationModeCandidates);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of showMessageForNeutralMasses method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testShowMessageForNeutralMasses() {
        System.out.println("showMessageForNeutralMasses");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        String expResult = "";
        String result = instance.showMessageForNeutralMasses();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }


    /**
     * Test of validateInputTolerance method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testValidateInputTolerance() {
        System.out.println("validateInputTolerance");
        FacesContext arg0 = null;
        UIComponent arg1 = null;
        Object arg2 = null;
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.validateInputTolerance(arg0, arg1, arg2);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of validateInputSingleMass method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testValidateInputSingleMass() {
        System.out.println("validateInputSingleMass");
        FacesContext arg0 = null;
        UIComponent arg1 = null;
        Object arg2 = null;
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.validateInputSingleMass(arg0, arg1, arg2);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of validateSingleRT method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testValidateSingleRT() {
        System.out.println("validateSingleRT");
        FacesContext arg0 = null;
        UIComponent arg1 = null;
        Object arg2 = null;
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        instance.validateSingleRT(arg0, arg1, arg2);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of roundToFourDecimals method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testRoundToFourDecimals() {
        System.out.println("roundToFourDecimals");
        Double doubleToRound = null;
        String expResult = "";
        String result = TheoreticalCompoundsRTController.roundToFourDecimals(doubleToRound);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getItemsGroupedWithoutSignificative method, of class TheoreticalCompoundsRTController.
     */
    @Test
    public void testGetItemsGroupedWithoutSignificative() {
        System.out.println("getItemsGroupedWithoutSignificative");
        TheoreticalCompoundsRTController instance = new TheoreticalCompoundsRTController();
        List expResult = null;
        List result = instance.getItemsGroupedWithoutSignificative();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
