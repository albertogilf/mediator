/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import CEMS.CEMSFragment;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author alberto.gildelafuent
 */
public class CadenaTest {

    public CadenaTest() {
    }

    /**
     * Test of extraerEltos method, of class Cadena.
     */
    @Test
    public void testExtraerEltos() {
        System.out.println("extraerEltos");
        String pattern = "";
        String chain = "";
        List<Float> expResult = null;
        List<Float> result = Cadena.extraerEltos(pattern, chain);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of extractFloats method, of class Cadena.
     */
    @Test
    public void testExtractFloats() {
        System.out.println("extractFloats");
        String cadena = "";
        List<Float> expResult = null;
        List<Float> result = Cadena.extractFloats(cadena);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of extractFirstDataSpectrumFloat method, of class Cadena.
     */
    @Test
    public void testExtractFirstDataSpectrumFloat() {
        System.out.println("extractFirstDataSpectrumFloat");
        String chain = "";
        List<Float> expResult = null;
        List<Float> result = Cadena.extractFirstDataSpectrumFloat(chain);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of extractFirstFloat method, of class Cadena.
     */
    @Test
    public void testExtractFirstFloat() {
        System.out.println("extractFirstFloat");
        String chain = "";
        Float expResult = null;
        Float result = Cadena.extractFirstFloat(chain);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of extractDoublesWithPatron method, of class Cadena.
     */
    @Test
    public void testExtractDoublesWithPatron() {
        System.out.println("extractDoublesWithPatron");
        String patron = "";
        String chain = "";
        List<Double> expResult = null;
        List<Double> result = Cadena.extractDoublesWithPatron(patron, chain);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of extractDoubles method, of class Cadena.
     */
    @Test
    public void testExtractDoubles() {
        System.out.println("extractDoubles");
        String chain = "";
        List<Double> expResult = null;
        List<Double> result = Cadena.extractDoubles(chain);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of extractInts method, of class Cadena.
     */
    @Test
    public void testExtractInts() {
        System.out.println("extractInts");
        String chain = "43"
                + "\n55"
                + "\n 78"
                + "\n98   "
                + "\n15678\n";
        List<Integer> expResult = new LinkedList();
        expResult.add(43);
        expResult.add(55);
        expResult.add(78);
        expResult.add(98);
        expResult.add(15678);
        List<Integer> result = Cadena.extractInts(chain);
        assertEquals(expResult, result);
    }

    /**
     * Test of extractFirstDataSpectrum method, of class Cadena.
     */
    @Test
    public void testExtractFirstDataSpectrum() {
        System.out.println("extractFirstDataSpectrum");
        String chain = "";
        List<Double> expResult = null;
        List<Double> result = Cadena.extractFirstDataSpectrum(chain);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getListOfDoubles method, of class Cadena.
     */
    @Test
    public void testGetListOfDoubles() {
        System.out.println("getListOfDoubles");
        String input = "";
        int numInputMasses = 0;
        List<Double> expResult = null;
        List<Double> result = Cadena.getListOfDoubles(input, numInputMasses);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of extractFirstDouble method, of class Cadena.
     */
    @Test
    public void testExtractFirstDouble() {
        System.out.println("extractFirstDouble");
        String chain = "";
        Double expResult = null;
        Double result = Cadena.extractFirstDouble(chain);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of extractSecondDouble method, of class Cadena.
     */
    @Test
    public void testExtractSecondDouble() {
        System.out.println("extractSecondDouble");
        String chain = "";
        Double expResult = null;
        Double result = Cadena.extractSecondDouble(chain);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of extractSecondNumberToInteger method, of class Cadena.
     */
    @Test
    public void testExtractSecondNumberToInteger() {
        System.out.println("extractSecondNumberToInteger");
        String chain = "";
        Integer expResult = null;
        Integer result = Cadena.extractSecondNumberToInteger(chain);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of generateSetOfSignificativeCompounds method, of class Cadena.
     */
    @Test
    public void testGenerateSetOfSignificativeCompounds() {
        System.out.println("generateSetOfSignificativeCompounds");
        String inputMasses = "";
        String inputRetentionTimes = "";
        Set<String> expResult = null;
        Set<String> result = Cadena.generateSetOfSignificativeCompounds(inputMasses, inputRetentionTimes);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of extractExperimentalFragments method, of class Cadena.
     */
    @Test
    public void testExtractExperimentalFragments() {
        System.out.println("extractExperimentalFragments");
        String input = "";
        Integer ionSourceVoltage = null;
        Set<CEMSFragment> expResult = null;
        Set<CEMSFragment> result = Cadena.extractExperimentalFragments(input, ionSourceVoltage);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of extractDataSpectrum method, of class Cadena.
     */
    @Test
    public void testExtractDataSpectrum() {
        System.out.println("extractDataSpectrum");
        String chain = "";
        List<Map<Double, Double>> expResult = null;
        List<Map<Double, Double>> result = Cadena.extractDataSpectrum(chain);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of extractOneSpectrum method, of class Cadena.
     */
    @Test
    public void testExtractOneSpectrum() {
        System.out.println("extractOneSpectrum");
        String chain = "";
        Map<Double, Double> expResult = null;
        Map<Double, Double> result = Cadena.extractOneSpectrum(chain);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getListOfExperimentalProductIons method, of class Cadena.
     */
    @Test
    public void testGetListOfExperimentalProductIons() {
        System.out.println("getListOfExperimentalProductIons");
        String input = "";
        Integer numInputMasses = null;
        Integer ionSourceVoltage = null;
        List<Set<CEMSFragment>> expResult = null;
        List<Set<CEMSFragment>> result = Cadena.getListOfExperimentalProductIons(input, numInputMasses, ionSourceVoltage);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getListOfCompositeSpectra method, of class Cadena.
     */
    @Test
    public void testGetListOfCompositeSpectra() {
        System.out.println("getListOfCompositeSpectra");
        String input = "";
        int numInputMasses = 0;
        List<Map<Double, Double>> expResult = null;
        List<Map<Double, Double>> result = Cadena.getListOfCompositeSpectra(input, numInputMasses);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of fillIsSignificativeCompound method, of class Cadena.
     */
    @Test
    public void testFillIsSignificativeCompound() {
        System.out.println("fillIsSignificativeCompound");
        String inputMasses = "";
        String inputRetentionTimes = "";
        int numInputMasses = 0;
        Set<String> keysSignificativeCompounds = null;
        List<Boolean> expResult = null;
        List<Boolean> result = Cadena.fillIsSignificativeCompound(inputMasses, inputRetentionTimes, numInputMasses, keysSignificativeCompounds);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}
