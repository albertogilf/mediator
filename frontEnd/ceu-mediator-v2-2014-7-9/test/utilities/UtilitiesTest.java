/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import CEMS.CEMSCompound;
import CEMS.CEMSFragment;
import CEMS.enums.CE_Exp_properties;
import compound.Structure;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import pathway.Pathway;
import persistence.NewCompounds;
import persistence.NewLMClassification;
import persistence.NewLipidsClassification;
import persistence.oxidizedTheoreticalCompound.FACompound;

/**
 *
 * @author alberto
 */
public class UtilitiesTest {

    public UtilitiesTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of calculatePPMIncrement method, of class Utilities.
     */
    @Test
    public void testCalculatePPMIncrement() {
        System.out.println("calculatePPMIncrement");
        Double measuredMass = 757.5667d;
        Double theoreticalMass = 757.56216d;
        int expResult = 6;
        int result = Utilities.calculatePPMIncrement(measuredMass, theoreticalMass);
        assertEquals(expResult, result);
    }

    /**
     * Test of calculateTheoreticalExperimentalMassOfPC method, of class
     * Utilities.
     */
    @Test
    public void testCalculateTheoreticalMassOfPC() {
        System.out.println("calculateTheoreticalMassOfPC");
        String adduct = "M-H";
        List<FACompound> FAs = new LinkedList();
        NewCompounds nc;
        nc = new NewCompounds();
        // TODO CHECK THE DB_ID_NUMBER 
        nc.setCompoundId(123205);
        NewLipidsClassification classification = new NewLipidsClassification(123205);
        nc.setLipidClassification(classification);
        NewLMClassification lmclassification = new NewLMClassification(123205);
        nc.setLMclassification(lmclassification);
        nc.setMass(256.2402d);
        nc.setCarbons(16);
        nc.setDoubleBonds(0);
        nc.setCategory("FA");
        nc.setMainClass("FA01");
        nc.setSubClass("FA010S");

        Double oxidizedFAEM = 0d;
        Double masstoSearchForOxidizedFA = 0d;
        String oxidationType = "";
        FACompound FA1 = new FACompound(nc, oxidizedFAEM, masstoSearchForOxidizedFA, oxidationType);

        FAs.add(FA1);
        Double expResult = 477.3223d;
        Double result = Utilities.calculateTheoreticalExperimentalMassOfPCJPA(FAs, adduct);
        assertEquals("TEST OUT OF DELTA", expResult, result, 0.001d);
    }

    /**
     * Test of calculateTheoreticalMassOfOxidizedFA method, of class Utilities.
     */
    @Test
    public void testCalculateTheoreticalMassOfOxidizedFA() {
        System.out.println("calculateTheoreticalMassOfOxidizedFA TEST");

        FACompound FA = get20_4OxidizedOH();

        Double expResult = 320.2351d;
        Double result = Utilities.calculateTheoreticalMassOfOxidizedFAJPA(FA);
        assertTrue(Math.abs(result - expResult) < 0.001d);
    }

    /**
     * Test of calculateTheoreticalMassOfNonOxidizedFA method, of class
     * Utilities.
     */
    @Test
    public void testCalculateTheoreticalMassOfNonOxidizedFA() {
        System.out.println("calculateTheoreticalMassOfNonOxidizedFA TEST");
        FACompound FA = get16_0NonOxidized();

        Double expResult = 256.2402d;
        Double result = Utilities.calculateTheoreticalMassOfNonOxidizedFA(FA);
        assertTrue(Math.abs(result - expResult) < 0.001d);
    }

    /**
     * Test of createNameOfPC method, of class Utilities.
     */
    @Test
    public void testCreateNameOfPC() {
        System.out.println("createNameOfPC Test");
        FACompound oxidizedFA;
        FACompound nonOxidizedFA;
        oxidizedFA = get20_4OxidizedOH();
        nonOxidizedFA = get16_0NonOxidized();
        List<FACompound> FAs;
        FAs = new LinkedList<>();
        FAs.add(nonOxidizedFA);
        FAs.add(oxidizedFA);
        String expResult = "PC(16:0/20:4[OH])";
        String result = Utilities.createNameOfPCJPA(FAs);
        assertEquals(expResult, result);
    }

    @Test
    public void testCreateNameNullOfPC() {
        System.out.println("createNameNullOfPC Test");
        FACompound oxidizedFA;
        FACompound nonOxidizedFA;
        oxidizedFA = null;
        nonOxidizedFA = get16_0NonOxidized();
        List<FACompound> FAs;
        FAs = new LinkedList<>();
        FAs.add(nonOxidizedFA);
        FAs.add(oxidizedFA);
        String expResult = "";
        String result = Utilities.createNameOfPCJPA(FAs);
        assertEquals(expResult, result);
    }

    @Test
    public void testCreateFormulaOfPC() {
        System.out.println("createFormulaOfPC Test");
        FACompound oxidizedFA;
        FACompound nonOxidizedFA;
        oxidizedFA = get20_4OxidizedOH();
        nonOxidizedFA = get16_0NonOxidized();
        List<FACompound> FAs;
        FAs = new LinkedList<>();
        FAs.add(nonOxidizedFA);
        FAs.add(oxidizedFA);
        String expResult = "C44H80NO8P";
        String result = Utilities.createFormulaOfPCJPA(FAs);
        assertEquals(expResult, result);
    }

    @Test
    public void testCreateFormula1FAOfPC() {
        System.out.println("createFormulaNullOfPC Test");
        FACompound oxidizedFA;
        FACompound nonOxidizedFA;
        oxidizedFA = null;
        nonOxidizedFA = get16_0NonOxidized();
        List<FACompound> FAs;
        FAs = new LinkedList<>();
        FAs.add(nonOxidizedFA);
        FAs.add(oxidizedFA);
        String expResult = "C24H49NO6P";
        String result = Utilities.createFormulaOfPCJPA(FAs);
        assertEquals(expResult, result);
    }

    private FACompound get16_0NonOxidized() {
        NewCompounds NC;
        NC = new NewCompounds();
        // TODO CHECK THE DB_ID_NUMBER 
        NC.setCompoundId(123205);
        NewLipidsClassification classification = new NewLipidsClassification(123205);
        NC.setLipidClassification(classification);
        NewLMClassification lmclassification = new NewLMClassification(123205);
        NC.setLMclassification(lmclassification);
        NC.setMass(256.2402d);
        NC.setCarbons(16);
        NC.setDoubleBonds(0);
        NC.setCategory("FA");
        NC.setMainClass("FA01");
        NC.setSubClass("FA010S");
        FACompound FA = new FACompound(NC, 255.233d, 256.2407, "");
        return FA;
    }

    private FACompound get20_4OxidizedOH() {
        NewCompounds NC;
        NC = new NewCompounds();
        // TODO CHECK THE DB_ID_NUMBER 
        NC.setCompoundId(123342);
        NewLipidsClassification classification = new NewLipidsClassification(123342);
        NC.setLipidClassification(classification);
        NewLMClassification lmclassification = new NewLMClassification(123342);
        NC.setLMclassification(lmclassification);
        NC.setMass(304.2402302664d);
        NC.setCarbons(20);
        NC.setDoubleBonds(4);
        NC.setCategory("FA");
        NC.setMainClass("FA01");
        NC.setSubClass("FA010U"); // PG
        FACompound FA = new FACompound(NC, 319.2274, 304.2398, "OH");
        return FA;
    }

    /**
     * Test of calculateTheoreticalExperimentalMassOfPC method, of class
     * Utilities.
     */
    @Test
    public void testCalculateTheoreticalExperimentalMassOfPC() {
        System.out.println("calculateTheoreticalExperimentalMassOfPC");
        FACompound oxidizedFA;
        FACompound nonOxidizedFA;
        oxidizedFA = get20_4OxidizedOH();
        nonOxidizedFA = get16_0NonOxidized();
        List<FACompound> FAs;
        FAs = new LinkedList<>();
        FAs.add(nonOxidizedFA);
        FAs.add(oxidizedFA);
        String adductType = "M-H";
        Double expResult = 796.5496d;
        Double result = Utilities.calculateTheoreticalExperimentalMassOfPCJPA(FAs, adductType);
        assertEquals("TEST OUT OF DELTA ALLOWED", expResult, result, 0.001d);
    }

    /**
     * Test of calculateTheoreticalNeutralMassOfPC method, of class Utilities.
     */
    @Test
    public void testCalculateTheoreticalNeutralMassOfPC() {
        System.out.println("calculateTheoreticalNeutralMassOfPC");
        FACompound oxidizedFA;
        FACompound nonOxidizedFA;
        oxidizedFA = get20_4OxidizedOH();
        nonOxidizedFA = get16_0NonOxidized();
        List<FACompound> FAs;
        FAs = new LinkedList<>();
        FAs.add(nonOxidizedFA);
        FAs.add(oxidizedFA);
        Double expResult = 797.5569d;
        Double result = Utilities.calculateTheoreticalNeutralMassOfPCJPA(FAs);
        assertEquals("TEST OUT OF DELTA ALLOWED", expResult, result, 0.001d);
    }

    /**
     * Test of calculateFAEMFromPIandOtherFAEM method, of class Utilities.
     */
    @Test
    public void testCalculateFAEMFromPIandOtherFAEM() {
        System.out.println("calculateFAEMFromPIandOtherFAEM");
        Double ParentIonNeutralMass = null;
        Double FAEM1 = null;
        Double expResult = null;
        Double result = Utilities.calculateFAEMFromPIandOtherFAEM(ParentIonNeutralMass, FAEM1);
        assertEquals(expResult, result);

    }

    /**
     * Test of calculateDeltaPPM method, of class Utilities.
     */
    @Test
    public void testCalculateDeltaPPM() {
        System.out.println("calculateDeltaPPM");
        Double massToSearch = 264.13d;
        String toleranceMode = "ppm";
        Double tolerance = 10d;
        Double expResult = 0.0026d;
        Double result = Utilities.calculateDeltaPPM(massToSearch, toleranceMode, tolerance);
        assertEquals("TEST OUT OF DELTA", expResult, result, 0.001d);
    }

    /**
     * Test of calculateMZFromNeutralMass method, of class Utilities.
     */
    @Test
    public void testCalculateMZFromNeutralMass() {
        System.out.println("calculateMZFromNeutralMass");
        Double inputMass = 100d;
        int massesMode = 0;
        int ionizationMode = 1;
        Double expResult = 100 + Constants.PROTON_WEIGHT;
        Double result = Utilities.calculateMZFromNeutralMass(inputMass, massesMode, ionizationMode);
        assertEquals("TEST OUT OF DELTA", expResult, result, 0.001d);
    }

    /**
     * Test of calculatePercentageError method, of class Utilities.
     */
    @Test
    public void testCalculatePercentageError() {
        System.out.println("calculatePercentageError");
        Double experimentalRMT = 14.5;
        Double theoreticalRMT = 14.3;
        Integer expResult = 1;
        Integer result = Utilities.calculatePercentageError(experimentalRMT, theoreticalRMT);
        assertEquals(expResult, result);
    }

    /**
     * Test of calculateTheoreticalExperimentalMassOfPCJPA method, of class
     * Utilities.
     */
    @Test
    public void testCalculateTheoreticalExperimentalMassOfPCJPA() {
        System.out.println("calculateTheoreticalExperimentalMassOfPCJPA");

        List<FACompound> FAs = new LinkedList<>();
        FAs.add(get16_0NonOxidized());
        FAs.add(get20_4OxidizedOH());
        String adductType = "M-H";
        Double expResult = 796.5496;
        Double result = Utilities.calculateTheoreticalExperimentalMassOfPCJPA(FAs, adductType);
        assertEquals("TEST OUT OF DELTA", expResult, result, 0.001d);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of calculateTheoreticalNeutralMassOfPCJPA method, of class
     * Utilities.
     */
    @Test
    public void testCalculateTheoreticalNeutralMassOfPCJPA() {
        System.out.println("calculateTheoreticalNeutralMassOfPCJPA");
        FACompound oxidizedFA;
        FACompound nonOxidizedFA;
        oxidizedFA = get20_4OxidizedOH();
        nonOxidizedFA = get16_0NonOxidized();
        List<FACompound> FAs;
        FAs = new LinkedList<>();
        FAs.add(nonOxidizedFA);
        FAs.add(oxidizedFA);
        Double expResult = 797.5569;
        Double result = Utilities.calculateTheoreticalNeutralMassOfPCJPA(FAs);
        assertEquals("TEST OUT OF DELTA", expResult, result, 0.001d);
    }

    /**
     * Test of createNameOfPCJPA method, of class Utilities.
     */
    @Test
    public void testCreateNameOfPCJPA() {
        System.out.println("createNameOfPCJPA");
        FACompound oxidizedFA;
        FACompound nonOxidizedFA;
        oxidizedFA = get20_4OxidizedOH();
        nonOxidizedFA = get16_0NonOxidized();
        List<FACompound> FAs;
        FAs = new LinkedList<>();
        FAs.add(nonOxidizedFA);
        FAs.add(oxidizedFA);
        String expResult = "PC(16:0/20:4[OH])";
        String result = Utilities.createNameOfPCJPA(FAs);
        assertEquals(expResult, result);
    }

    /**
     * Test of createFormulaOfPCJPA method, of class Utilities.
     */
    @Test
    public void testCreateFormulaOfPCJPA() {
        System.out.println("createFormulaOfPCJPA");
        FACompound oxidizedFA;
        FACompound nonOxidizedFA;
        oxidizedFA = get20_4OxidizedOH();
        nonOxidizedFA = get16_0NonOxidized();
        List<FACompound> FAs;
        FAs = new LinkedList<>();
        FAs.add(nonOxidizedFA);
        FAs.add(oxidizedFA);
        String expResult = "C44H80NO8P";
        String result = Utilities.createFormulaOfPCJPA(FAs);
        assertEquals(expResult, result);
    }

    /**
     * Test of getNumHydrogensOxidationType method, of class Utilities.
     */
    @Test
    public void testGetNumHydrogensOxidationType() throws Exception {
        System.out.println("getNumHydrogensOxidationType");
        String oxidationType = "OH-OH";
        Integer expResult = 0;
        Integer result = Utilities.getNumHydrogensOxidationType(oxidationType);
        assertEquals(expResult, result);
    }

    /**
     * Test of getNumOxigensOxidationType method, of class Utilities.
     */
    @Test
    public void testGetNumOxigensOxidationType() throws Exception {
        System.out.println("getNumOxigensOxidationType");
        String oxidationType = "OH-OH";
        Integer expResult = 2;
        Integer result = Utilities.getNumOxigensOxidationType(oxidationType);
        assertEquals(expResult, result);
    }

    /**
     * Test of calculateTheoreticalMassOfOxidizedFAJPA method, of class
     * Utilities.
     */
    @Test
    public void testCalculateTheoreticalMassOfOxidizedFAJPA() {
        System.out.println("calculateTheoreticalMassOfOxidizedFAJPA");
        FACompound FA = get20_4OxidizedOH();
        Double expResult = 320.2351d;
        Double result = Utilities.calculateTheoreticalMassOfOxidizedFAJPA(FA);
        assertEquals("TEST OUT OF DELTA ALLOWED", expResult, result, 0.001d);
    }

    /**
     * Test of calculateDeltaPPM method, of class Utilities.
     */
    @Test
    public void testCalculateDeltaPPM_3args_1() {
        System.out.println("calculateDeltaPPM");
        Double massToSearch = 100d;
        Integer toleranceMode = 0;
        Double tolerance = 10d;
        Double expResult = 0.001;
        Double result = Utilities.calculateDeltaPPM(massToSearch, toleranceMode, tolerance);
        assertEquals("TEST OUT OF DELTA ALLOWED", expResult, result, 0.001d);
    }

    /**
     * Test of calculateDeltaPPM method, of class Utilities.
     */
    @Test
    public void testCalculateDeltaPPM_3args_2() {
        System.out.println("calculateDeltaPPM");
        Double massToSearch = 100d;
        String toleranceMode = "ppm";
        Double tolerance = 10d;
        Double expResult = 0.001;
        Double result = Utilities.calculateDeltaPPM(massToSearch, toleranceMode, tolerance);
        assertEquals("TEST OUT OF DELTA ALLOWED", expResult, result, 0.001d);
    }

    /**
     * Test of calculateDeltaPercentage method, of class Utilities.
     */
    @Test
    public void testCalculateDeltaRMT() {
        System.out.println("calculateDeltaRMT");
        Double RMTToSearch = 200d;
        String RMTtoleranceMode = "percentage";
        Double RMTtolerance = 10d;
        Double expResult = 20.0d;
        Double result = Utilities.calculateDeltaPercentage(RMTToSearch, RMTtoleranceMode, RMTtolerance);
        assertEquals("TEST OUT OF DELTA ALLOWED", expResult, result, 0.001d);
    }

    /**
     * Test of generateStringFragmentsNointensity method, of class Utilities.
     */
    @Test
    public void testGenerateStringFragmentsNointensity() {
        System.out.println("generateStringFragmentsNointensity");
        CEMSFragment ceProductIon1 = new CEMSFragment(100d, null, null, 30, 100d, 60d, "fragment", "fragment of something", null);
        CEMSFragment ceProductIon2 = new CEMSFragment(100d, null, null, 30, 50d, 80d, "fragment", "fragment of something", null);
        CEMSFragment ceProductIon3 = new CEMSFragment(100d, null, null, 30, 150d, 20d, "fragment", "fragment of something", null);
        Set<CEMSFragment> ceProductIons = new TreeSet<>();
        ceProductIons.add(ceProductIon1);
        ceProductIons.add(ceProductIon2);
        ceProductIons.add(ceProductIon3);
        String expResult = "50.0, 100.0, 150.0";
        String result = Utilities.generateStringFragmentsNointensity(ceProductIons);
        assertEquals(expResult, result);
    }

    /**
     * Test of generateStringFragments method, of class Utilities.
     */
    @Test
    public void testGenerateStringFragments() {
        System.out.println("generateStringFragments");
        CEMSFragment ceProductIon1 = new CEMSFragment(100d, null, null, 30, 100d, 60d, "fragment", "fragment of something", null);
        CEMSFragment ceProductIon2 = new CEMSFragment(100d, null, null, 30, 50d, 80d, "fragment", "fragment of something", null);
        CEMSFragment ceProductIon3 = new CEMSFragment(100d, null, null, 30, 150d, 20d, "fragment", "fragment of something", null);
        Set<CEMSFragment> ceProductIons = new TreeSet<>();
        ceProductIons.add(ceProductIon1);
        ceProductIons.add(ceProductIon2);
        ceProductIons.add(ceProductIon3);
        String expResult = "(50.0, 80.0), (100.0, 60.0), (150.0, 20.0)";
        String result = Utilities.generateStringFragments(ceProductIons);
        assertEquals(expResult, result);
    }

    /**
     * Test of getDBMobilitiesFromCEMSCompounds method, of class Utilities.
     */
    @Test
    public void testGetDBMobilitiesFromCEMSCompounds() {
        System.out.println("getDBMobilitiesFromCEMSCompounds");
        Map<Integer, CEMSCompound> cemsCompoundsMap = new LinkedHashMap<>();
        CE_Exp_properties.CE_EXP_PROP_ENUM ce_exp_properties = CE_Exp_properties.CE_EXP_PROP_ENUM.FORMIC_25_POS_DIR;
        Set<CEMSFragment> productIons = null;
        Double effMob = 2697.16533870139;
        Double MT = 7.87;
        Double RMT = 0.50;
        Integer compound_id = 180838;
        Double mass = 181.0409;
        String formula = "C5H11NO4S";
        String compound_name = "L-Methionine sulfone";
        String cas_id = null;
        Integer formula_type = 0;
        Integer compound_type = 0;
        Integer compound_status = 0;
        Integer charge_type = 0;
        Integer charge_number = 0;
        Structure structure = null;
        List<Pathway> pathways = null;
        CEMSCompound cemscompound1 = new CEMSCompound(ce_exp_properties, effMob, MT, RMT,
                productIons, compound_id, mass, formula, compound_name, cas_id, formula_type, compound_type,
                compound_status, charge_type, charge_number, "", "", "", "", "", 0, 0, 0,"", 0, "", "", "", "", 
                0, 0, structure, pathways);

        
        
        effMob = -43.86;
        MT = 25.29;
        RMT = 1.61;
        compound_id = 91854;
        mass = 179.058243159;
        formula = "C9H9NO3 ";
        compound_name = "Hippuric acid";

        CEMSCompound cemscompound2 = new CEMSCompound(ce_exp_properties, effMob, MT, RMT,
                productIons, compound_id, mass, formula, compound_name, cas_id, formula_type, compound_type,
                compound_status, charge_type, charge_number, "", "", "", "", "", 0, 0, 0,"", 0, "", "", "", "", 
                0, 0, structure, pathways);

        effMob = 1669.66;
        MT = 10.89;
        RMT = 0.70;
        compound_id = 46307;
        mass = 169.085126611;
        formula = "C7H11N3O2";
        compound_name = "3-Methylhistidine";

        CEMSCompound cemscompound3 = new CEMSCompound(ce_exp_properties, effMob, MT, RMT,
                productIons, compound_id, mass, formula, compound_name, cas_id, formula_type, compound_type,
                compound_status, charge_type, charge_number, "", "", "", "", "", 0, 0, 0,"", 0, "", "", "", "", 
                0, 0, structure, pathways);

        cemsCompoundsMap.put(cemscompound1.getCompound_id(), cemscompound1);
        cemsCompoundsMap.put(cemscompound2.getCompound_id(), cemscompound2);
        cemsCompoundsMap.put(cemscompound3.getCompound_id(), cemscompound3);
        float[] dbmobilities = new float[3];
        Double[] markerMobilities = Utilities.getDBMobilitiesFromCEMSCompounds(cemsCompoundsMap, dbmobilities, compound_id, null);
        float[] dbmobilityExpected = new float[3];
        dbmobilityExpected[0] = 2697.1653f;
        dbmobilityExpected[1] = -43.86f;
        dbmobilityExpected[2] = 1669.66f;

        assertArrayEquals(dbmobilityExpected, dbmobilities, 0.1f);
        assertEquals("DELTA TOO HIGH", effMob, markerMobilities[0], 0.001d);
    }

    /**
     * Test of fillMTs method, of class Utilities.
     */
    @Test
    public void testFillMTs() {
        System.out.println("fillMTs");
        Map<Integer, CEMSCompound> cemsCompounds = null;
        float[] predictedMTs = null;
        Utilities.fillMTs(cemsCompounds, predictedMTs);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of fillRMTs method, of class Utilities.
     */
    @Test
    public void testFillRMTs() {
        System.out.println("fillRMTs");
        Map<Integer, CEMSCompound> cemsCompounds = null;
        float[] predictedMTs = null;
        Integer bge = null;
        Double expResult = null;
        Double result = Utilities.fillRMTs(cemsCompounds, predictedMTs, bge);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}
