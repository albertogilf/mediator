/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */
/* global picos */
var picos;
function graphPeaks(items,compounds) {
    
    //items son los que metemos, compounds los que nos da la db
     //alert(items); //salen como objects
     var peaks = JSON.stringify(items);
     //alert(peaks);
     picos = JSON.parse(JSON.stringify(peaks));
     
     //alert(compounds);
     var compoundsString = JSON.stringify(compounds);
    // alert(compoundsString);
     
     //alert(items.length);
     
     //hacemos que items siempre tenga una longitud de 9 para quee cuadre la gráfica
     var x  = 10;
     for(var i  = items.length + 1; i < 9; i++){
         
         items.unshift({"x": x, "y": null});
         x = x - 1;
     }
     //dejamos uno para meter el 0 y que comience en 0 la gráfica
     items.unshift({"x": 0, "y": null});

    var graph = new Rickshaw.Graph({
        unstack: true,
        element: document.querySelector("#chart"),
        width: 800,
        height: 300,
        gapSize: 0.35,
        min: -100,
        max: 100,
        renderer: 'bar',
        series: [{
                name: 'Input Spectra',
                color: 'blue',
                data: items//[ {x: 0, y: 0}, {x: 40, y: 174}, {x: 56, y: 23} ]
            }, {
                name: 'Comparison result spectra',
                color: 'tomato',
                data: compounds //[ {x: 0, y: 0}, {x: 42, y: -174}, {x: 59, y: -23} ]
            }]
    });

    var format = function (n) {

        var map = {
            0: '0',
            50: '50',
            100: '100',
            150: '150',
            200: '200'
        };

        return map[n];
    };

    graph.render();

    var y_axis = new Rickshaw.Graph.Axis.Y({
        graph: graph,
        //color: "#FFB833",
        pixelsPerTick: 35,
        orientation: 'left',
        tickFormat: Rickshaw.Fixtures.Number.formatKMBT,

        element: document.getElementById('y_axis')

    });

    var x_axis = new Rickshaw.Graph.Axis.X({
        graph: graph,
        orientation: 'bottom',
        tickSize: 6,
        ticksTreatment: 'plain',
        tickFormat: Rickshaw.Fixtures.Number.formatKMBT,
        element: document.getElementById('x_axis'),
        pixelsPerTick: 35


    });

    y_axis.render();
    x_axis.render();


    var legend = new Rickshaw.Graph.Legend({
        element: document.querySelector('#legend'),
        graph: graph
    });



    var shelving = new Rickshaw.Graph.Behavior.Series.Toggle({
        graph: graph,
        legend: legend
    });

    var order = new Rickshaw.Graph.Behavior.Series.Order({
        graph: graph,
        legend: legend
    });

    var highlight = new Rickshaw.Graph.Behavior.Series.Highlight({
        graph: graph,
        legend: legend
    });


    var hoverDetail = new Rickshaw.Graph.HoverDetail({
        graph: graph,
        formatter: function (series, x, y) {
            //var date = '<span class="date">' + new Date(x * 1000).toUTCString() + '</span>';
            //var swatch = '<span class="detail_swatch" style="background-color: ' + series.color + '"></span>';
            var content = "Intensity: " + parseFloat(y) + "<br/>" + "m/z: " + parseFloat(x);
            return content;
        }, 
        
        
    });
}



